-- MySQL dump 10.11
--
-- Host: localhost    Database: HGVbaseG2P_marker
-- ------------------------------------------------------
-- Server version	5.0.67-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Allele`
--

DROP TABLE IF EXISTS `Allele`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Allele` (
  `AlleleID` int(10) unsigned NOT NULL auto_increment,
  `Identifier` varchar(15) default NULL,
  `Status` varchar(20) NOT NULL default 'active',
  `MarkerID` int(10) unsigned NOT NULL default '0',
  `Upstream30bp` varchar(30) NOT NULL default '' COMMENT 'type & strand as in dbSNP, to contextualize alleles',
  `AlleleSeq` text NOT NULL COMMENT 'type & strand as in dbSNP',
  `AlleleSeqDigest` varchar(32) default NULL COMMENT 'MD5 digest of allele seq, to check if seqs are the same or not',
  `Downstream30bp` varchar(30) NOT NULL default '' COMMENT 'type & strand as in dbSNP, to contextualize alleles',
  `SeqType` enum('G','C') NOT NULL default 'G' COMMENT '''G'' (Genomic) or ''C'' (cDNA), as used in dbSNP',
  PRIMARY KEY  (`AlleleID`),
  UNIQUE KEY `MarkerID` (`MarkerID`,`AlleleSeqDigest`),
  CONSTRAINT `Allele_ibfk_1` FOREIGN KEY (`MarkerID`) REFERENCES `Marker` (`MarkerID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40900333 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AssayMarker`
--

DROP TABLE IF EXISTS `AssayMarker`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssayMarker` (
  `AssayMarkerID` int(10) unsigned NOT NULL default '0',
  `AssayID` int(10) unsigned NOT NULL default '0',
  `MarkerID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`AssayMarkerID`),
  KEY `MarkerID` (`MarkerID`),
  KEY `AssayID` (`AssayID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AssayMarkerGenotype`
--

DROP TABLE IF EXISTS `AssayMarkerGenotype`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssayMarkerGenotype` (
  `AssayMarkerGenotypeID` int(10) unsigned NOT NULL auto_increment,
  `AssayMarkerID` int(10) unsigned NOT NULL,
  `GenotypeID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`AssayMarkerGenotypeID`),
  UNIQUE KEY `AssayMarkerID` (`AssayMarkerID`,`GenotypeID`),
  KEY `GenotypeID` (`GenotypeID`),
  CONSTRAINT `AssayMarkerGenotype_ibfk_3` FOREIGN KEY (`AssayMarkerID`) REFERENCES `AssayMarker` (`AssayMarkerID`),
  CONSTRAINT `AssayMarkerGenotype_ibfk_4` FOREIGN KEY (`GenotypeID`) REFERENCES `Genotype` (`GenotypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Assay_MethodList`
--

DROP TABLE IF EXISTS `Assay_MethodList`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Assay_MethodList` (
  `Method` varchar(250) NOT NULL default '' COMMENT 'initial values: (''ABI 5''-Nuclease'',''Affymetrix 10k array'',''Affymetrix 100k array'',',
  PRIMARY KEY  (`Method`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Assay_TypeList`
--

DROP TABLE IF EXISTS `Assay_TypeList`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Assay_TypeList` (
  `Type` varchar(100) NOT NULL COMMENT 'initial values: (''SNP:singleplex'',',
  PRIMARY KEY  (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AssayedGenotype`
--

DROP TABLE IF EXISTS `AssayedGenotype`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssayedGenotype` (
  `AssayMarkerID` int(10) unsigned NOT NULL default '0',
  `GenotypeID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`AssayMarkerID`,`GenotypeID`),
  KEY `GenotypeID` (`GenotypeID`),
  CONSTRAINT `AssayedGenotype_ibfk_1` FOREIGN KEY (`AssayMarkerID`) REFERENCES `AssayMarker` (`AssayMarkerID`),
  CONSTRAINT `AssayedGenotype_ibfk_2` FOREIGN KEY (`GenotypeID`) REFERENCES `Genotype` (`GenotypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Build`
--

DROP TABLE IF EXISTS `Build`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Build` (
  `BuildID` int(10) unsigned NOT NULL auto_increment,
  `Database` varchar(50) NOT NULL,
  `FullName` varchar(250) default NULL,
  `URL` varchar(250) default NULL,
  `Build` varchar(10) NOT NULL default 'N/A' COMMENT 'Start at 21 for HGVbase-G2P. Latest download for dbSNP',
  `Date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`BuildID`),
  UNIQUE KEY `Database` (`Database`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Crossref`
--

DROP TABLE IF EXISTS `Crossref`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Crossref` (
  `CrossrefID` int(10) unsigned NOT NULL auto_increment,
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  `UrlID` varchar(100) default '',
  PRIMARY KEY  (`CrossrefID`),
  UNIQUE KEY `HotlinkID` (`HotlinkID`,`UrlID`),
  CONSTRAINT `Crossref_ibfk_1` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=4625 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `DataSource`
--

DROP TABLE IF EXISTS `DataSource`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DataSource` (
  `DataSourceID` int(4) unsigned NOT NULL auto_increment,
  `Name` varchar(100) NOT NULL,
  `URI` varchar(300) default NULL,
  PRIMARY KEY  (`DataSourceID`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `URI` (`URI`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Genotype`
--

DROP TABLE IF EXISTS `Genotype`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Genotype` (
  `GenotypeID` int(10) unsigned NOT NULL auto_increment,
  `MarkerID` int(10) unsigned NOT NULL,
  `GenotypeLabel` varchar(100) NOT NULL,
  PRIMARY KEY  (`GenotypeID`),
  UNIQUE KEY `MarkerID_2` (`MarkerID`,`GenotypeLabel`),
  CONSTRAINT `Genotype_ibfk_1` FOREIGN KEY (`MarkerID`) REFERENCES `Marker` (`MarkerID`)
) ENGINE=InnoDB AUTO_INCREMENT=12608861 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Haplotype`
--

DROP TABLE IF EXISTS `Haplotype`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Haplotype` (
  `HaplotypeID` int(10) unsigned NOT NULL default '0',
  `Identifier` varchar(14) NOT NULL default '' COMMENT '''HAP...''',
  `MarkersetID` int(10) unsigned default NULL,
  `LocalID` varchar(100) NOT NULL,
  PRIMARY KEY  (`HaplotypeID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `MarkersetID` (`MarkersetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `HaplotypeAllele`
--

DROP TABLE IF EXISTS `HaplotypeAllele`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `HaplotypeAllele` (
  `HaplotypeAlleleID` int(10) unsigned NOT NULL auto_increment,
  `HaplotypeID` int(10) unsigned NOT NULL default '0',
  `AlleleID` int(10) unsigned NOT NULL default '0',
  `PQOrder` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`HaplotypeAlleleID`),
  UNIQUE KEY `HaplotypeID` (`HaplotypeID`,`AlleleID`),
  KEY `AlleleID` (`AlleleID`),
  CONSTRAINT `HaplotypeAllele_ibfk_1` FOREIGN KEY (`HaplotypeID`) REFERENCES `Haplotype` (`HaplotypeID`),
  CONSTRAINT `HaplotypeAllele_ibfk_2` FOREIGN KEY (`AlleleID`) REFERENCES `Allele` (`AlleleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `HaplotypeCrossref`
--

DROP TABLE IF EXISTS `HaplotypeCrossref`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `HaplotypeCrossref` (
  `HaplotypeCrossrefID` int(10) NOT NULL auto_increment,
  `HaplotypeID` int(10) unsigned NOT NULL default '0',
  `CrossrefID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`HaplotypeCrossrefID`),
  UNIQUE KEY `HaplotypeID` (`HaplotypeID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `HaplotypeCrossref_ibfk_1` FOREIGN KEY (`HaplotypeID`) REFERENCES `Haplotype` (`HaplotypeID`),
  CONSTRAINT `HaplotypeCrossref_ibfk_2` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref` (`CrossrefID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Hotlink`
--

DROP TABLE IF EXISTS `Hotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Hotlink` (
  `HotlinkID` int(10) unsigned NOT NULL auto_increment,
  `HotlinkLabel` varchar(100) NOT NULL COMMENT 'Indicate where the URL is pointing, e.g. "dbSNP refSNP report"',
  `UrlPrefix` varchar(300) NOT NULL,
  `UrlSuffix` varchar(100) default NULL,
  PRIMARY KEY  (`HotlinkID`),
  UNIQUE KEY `HotlinkLabel` (`HotlinkLabel`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Marker`
--

DROP TABLE IF EXISTS `Marker`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Marker` (
  `MarkerID` int(10) unsigned NOT NULL auto_increment,
  `Identifier` varchar(15) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `Status` varchar(20) NOT NULL default 'active' COMMENT 'Indicates whether the marker is active, dead or withheld or other',
  `VariationType` varchar(30) NOT NULL COMMENT 'SO term for the type of the marker feature',
  `HotlinkID` int(10) unsigned default NULL,
  `Upstream30bp` varchar(30) NOT NULL,
  `AlleleSeqsShorthand` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `AlleleSeqs` text,
  `Downstream30bp` varchar(30) NOT NULL,
  `ValidationCode` text,
  `AddedFromSourceBuild` varchar(10) default NULL COMMENT 'Label of source database build (or nickname) from where we first got the marker. Example: ''b128'' for dbSNP.',
  `TimeLastTouched` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`MarkerID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  UNIQUE KEY `Accession` (`Accession`,`DataSourceID`),
  KEY `VariationType` (`VariationType`),
  KEY `HotlinkID` (`HotlinkID`),
  KEY `TimeLastTouched` (`TimeLastTouched`),
  KEY `DataSourceID` (`DataSourceID`),
  KEY `Status` (`Status`),
  CONSTRAINT `Marker_ibfk_1` FOREIGN KEY (`DataSourceID`) REFERENCES `DataSource` (`DataSourceID`),
  CONSTRAINT `Marker_ibfk_2` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=43369055 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `MarkerCoord`
--

DROP TABLE IF EXISTS `MarkerCoord`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `MarkerCoord` (
  `MarkerCoordID` int(10) unsigned NOT NULL auto_increment,
  `MarkerID` int(10) unsigned NOT NULL default '0',
  `Chr` varchar(20) NOT NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Span` varchar(10) default NULL,
  `Strand` smallint(1) NOT NULL default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `MapWeight` varchar(20) default NULL,
  `GenomeBuild` varchar(5) NOT NULL default '',
  `AssemblyType` varchar(30) NOT NULL,
  `AssemblyName` varchar(30) NOT NULL,
  `MappedGene` varchar(15) NULL,
  PRIMARY KEY  (`MarkerCoordID`),
  KEY `MarkerID` (`MarkerID`),
  KEY `Chr_2` (`Chr`,`Start`,`Stop`),
  KEY `AssemblyType` (`AssemblyType`),
  KEY `MappedGene` (`MappedGene`),
  CONSTRAINT `MarkerCoord_ibfk_1` FOREIGN KEY (`MarkerID`) REFERENCES `Marker` (`MarkerID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30113809 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `MarkerCrossref`
--

DROP TABLE IF EXISTS `MarkerCrossref`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `MarkerCrossref` (
  `MarkerCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `MarkerID` int(10) unsigned NOT NULL default '0',
  `CrossrefID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`MarkerCrossrefID`),
  UNIQUE KEY `MarkerID` (`MarkerID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `MarkerCrossref_ibfk_3` FOREIGN KEY (`MarkerID`) REFERENCES `Marker` (`MarkerID`) ON DELETE CASCADE,
  CONSTRAINT `MarkerCrossref_ibfk_4` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref` (`CrossrefID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 0 kB; (`MarkerID`) REFER `HGVbaseG2P_marker/Mar';
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `MarkerRevision`
--

DROP TABLE IF EXISTS `MarkerRevision`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `MarkerRevision` (
  `MarkerRevisionID` int(10) unsigned NOT NULL auto_increment,
  `MarkerID` int(10) unsigned NOT NULL,
  `ReplacedByMarkerID` int(10) unsigned default NULL COMMENT 'Pointer to marker that replaced this one, if applicable.',
  `StrandFlipped` varchar(5) default NULL COMMENT 'Boolean flag to indicate whether the revision resulted in a strand-flip of flanks (and alleles). Mainly happens when one rs# is merged into another rs# in dbSNP',
  `ChangeType` varchar(100) NOT NULL COMMENT 'Change type category (e.g. Deletion, Merging and so on). Can be Unknown if it is something not seen before',
  `ChangeTrigger` varchar(100) NOT NULL COMMENT 'What triggered this particular change. Usually the processing of a particular db-build, but can be something else',
  `TimeCreated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `Comment` varchar(250) default NULL,
  PRIMARY KEY  (`MarkerRevisionID`),
  UNIQUE KEY `MarkerID` (`MarkerID`,`ReplacedByMarkerID`),
  KEY `ReplacedByMarkerID` (`ReplacedByMarkerID`),
  CONSTRAINT `MarkerRevision_ibfk_3` FOREIGN KEY (`MarkerID`) REFERENCES `Marker` (`MarkerID`) ON DELETE CASCADE,
  CONSTRAINT `MarkerRevision_ibfk_4` FOREIGN KEY (`ReplacedByMarkerID`) REFERENCES `Marker` (`MarkerID`)
) ENGINE=InnoDB AUTO_INCREMENT=13469496 DEFAULT CHARSET=latin1 COMMENT='Tracks marker revision history';
SET character_set_client = @saved_cs_client;

-- Dump completed on 2011-04-08 14:39:19
