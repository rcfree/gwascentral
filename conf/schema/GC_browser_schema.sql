# Sequel Pro dump
# Version 2492
# http://code.google.com/p/sequel-pro
#
# Host: viti.gene.le.ac.uk (MySQL 5.0.67-log)
# Database: rcf8_GC_browser_test
# Generation Time: 2011-05-25 12:56:36 +0100
# ************************************************************

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table marker_binned_1mb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_binned_1mb`;

CREATE TABLE `marker_binned_1mb` (
  `Reference` varchar(10) NOT NULL,
  `Source` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `Resultset_Identifier` varchar(15) default NULL,
  `Url` text,
  `ScoreZERO` int(11) NOT NULL,
  `Score1` int(11) NOT NULL,
  `Score2` int(11) NOT NULL,
  `Score3` int(11) NOT NULL,
  `Score4` int(11) NOT NULL,
  `Score5` int(11) NOT NULL,
  `Score6` int(11) NOT NULL,
  `Score7` int(11) NOT NULL,
  `Score8` int(11) NOT NULL,
  `Score9` int(11) NOT NULL,
  `Score10` int(11) NOT NULL,
  KEY `Resultset_Identifier` (`Resultset_Identifier`),
  KEY `Reference` (`Reference`,`Start`,`Stop`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_binned_chr_3mb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_binned_chr_3mb`;

CREATE TABLE `marker_binned_chr_3mb` (
  `Reference` varchar(10) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `ResultSet_Identifier` varchar(15) default NULL,
  `ScoreListZERO` varchar(1000) NOT NULL,
  `ScoreList1` varchar(1000) NOT NULL,
  `ScoreList2` varchar(1000) NOT NULL,
  `ScoreList3` varchar(1000) NOT NULL,
  `ScoreList4` varchar(1000) NOT NULL,
  `ScoreList5` varchar(1000) NOT NULL,
  `ScoreList6` varchar(1000) NOT NULL,
  `ScoreList7` varchar(1000) NOT NULL,
  `ScoreList8` varchar(1000) NOT NULL,
  `ScoreList9` varchar(1000) NOT NULL,
  `ScoreList10` varchar(1000) NOT NULL,
  KEY `Resultset_Ident` (`ResultSet_Identifier`),
  KEY `Reference` (`Reference`,`Start`,`Stop`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_binned_ltd_1mb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_binned_ltd_1mb`;

CREATE TABLE `marker_binned_ltd_1mb` (
  `Reference` varchar(10) NOT NULL,
  `Source` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `Resultset_Identifier` varchar(15) default NULL,
  `Url` text,
  `ScoreZERO` int(11) NOT NULL,
  `Score1` int(11) NOT NULL,
  `Score2` int(11) NOT NULL,
  `Score3` int(11) NOT NULL,
  `Score4` int(11) NOT NULL,
  `Score5` int(11) NOT NULL,
  `Score6` int(11) NOT NULL,
  `Score7` int(11) NOT NULL,
  `Score8` int(11) NOT NULL,
  `Score9` int(11) NOT NULL,
  `Score10` int(11) NOT NULL,
  KEY `Resultset_Identifier` (`Resultset_Identifier`),
  KEY `Reference` (`Reference`,`Start`,`Stop`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_binned_ltd_chr_3mb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_binned_ltd_chr_3mb`;

CREATE TABLE `marker_binned_ltd_chr_3mb` (
  `Reference` varchar(10) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `ResultSet_Identifier` varchar(15) default NULL,
  `ScoreListZERO` varchar(1000) NOT NULL,
  `ScoreList1` varchar(1000) NOT NULL,
  `ScoreList2` varchar(1000) NOT NULL,
  `ScoreList3` varchar(1000) NOT NULL,
  `ScoreList4` varchar(1000) NOT NULL,
  `ScoreList5` varchar(1000) NOT NULL,
  `ScoreList6` varchar(1000) NOT NULL,
  `ScoreList7` varchar(1000) NOT NULL,
  `ScoreList8` varchar(1000) NOT NULL,
  `ScoreList9` varchar(1000) NOT NULL,
  `ScoreList10` varchar(1000) NOT NULL,
  KEY `Resultset_Ident` (`ResultSet_Identifier`),
  KEY `Reference` (`Reference`,`Start`,`Stop`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_data_chr1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr1`;

CREATE TABLE `marker_data_chr1` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr10
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr10`;

CREATE TABLE `marker_data_chr10` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr11
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr11`;

CREATE TABLE `marker_data_chr11` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr12
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr12`;

CREATE TABLE `marker_data_chr12` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr13
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr13`;

CREATE TABLE `marker_data_chr13` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr14
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr14`;

CREATE TABLE `marker_data_chr14` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr15
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr15`;

CREATE TABLE `marker_data_chr15` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr16
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr16`;

CREATE TABLE `marker_data_chr16` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
   `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr17
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr17`;

CREATE TABLE `marker_data_chr17` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr18
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr18`;

CREATE TABLE `marker_data_chr18` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


# Dump of table marker_data_chr19
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr19`;

CREATE TABLE `marker_data_chr19` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr2`;

CREATE TABLE `marker_data_chr2` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr20
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr20`;

CREATE TABLE `marker_data_chr20` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr21
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr21`;

CREATE TABLE `marker_data_chr21` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
   `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
   `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr22
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr22`;

CREATE TABLE `marker_data_chr22` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr3`;

CREATE TABLE `marker_data_chr3` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr4`;

CREATE TABLE `marker_data_chr4` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
   `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
   `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


# Dump of table marker_data_chr5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr5`;

CREATE TABLE `marker_data_chr5` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr6`;

CREATE TABLE `marker_data_chr6` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
   `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
   `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr7`;

CREATE TABLE `marker_data_chr7` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr8`;

CREATE TABLE `marker_data_chr8` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chr9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chr9`;

CREATE TABLE `marker_data_chr9` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chrX
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chrX`;

CREATE TABLE `marker_data_chrX` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_chrY
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_chrY`;

CREATE TABLE `marker_data_chrY` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  `Upstream30bp` varchar(30) NULL,
  `Downstream30bp` varchar(30) NULL,
  `Alleles` text COMMENT 'shorthand-string with active alleleseqs from the Allele table',
  `UsedmarkersetID` int(10) unsigned NOT NULL COMMENT 'Association with markerset that was tested',
  `RiskAllele` char(1) default NULL,
  `RiskAlleleFreq` double unsigned default NULL,
  `RiskAlleleComment` text,
  `ES_Type` enum('OR','BC','RR','DP') NULL COMMENT 'Type of effect measure: OR (odds ratio), RR (relative risk) or DP (difference in proportions)',
  `ES_Value` double unsigned NULL COMMENT 'Value for measured effect',
  `ES_Lower95Bound` double unsigned default NULL COMMENT 'Lower boundary of 95% confidence interval',
  `ES_Upper95Bound` double unsigned default NULL COMMENT 'Upper boundary of 95% confidence interval',
  `ES_StdError` double unsigned default NULL COMMENT 'Standard error of estimated parameter value',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr1`;

CREATE TABLE `marker_data_ltd_chr1` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr10
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr10`;

CREATE TABLE `marker_data_ltd_chr10` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr11
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr11`;

CREATE TABLE `marker_data_ltd_chr11` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr12
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr12`;

CREATE TABLE `marker_data_ltd_chr12` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr13
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr13`;

CREATE TABLE `marker_data_ltd_chr13` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr14
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr14`;

CREATE TABLE `marker_data_ltd_chr14` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr15
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr15`;

CREATE TABLE `marker_data_ltd_chr15` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr16
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr16`;

CREATE TABLE `marker_data_ltd_chr16` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr17
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr17`;

CREATE TABLE `marker_data_ltd_chr17` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr18
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr18`;

CREATE TABLE `marker_data_ltd_chr18` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr19
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr19`;

CREATE TABLE `marker_data_ltd_chr19` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr2`;

CREATE TABLE `marker_data_ltd_chr2` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr20
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr20`;

CREATE TABLE `marker_data_ltd_chr20` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr21
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr21`;

CREATE TABLE `marker_data_ltd_chr21` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr22
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr22`;

CREATE TABLE `marker_data_ltd_chr22` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr3`;

CREATE TABLE `marker_data_ltd_chr3` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr4`;

CREATE TABLE `marker_data_ltd_chr4` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr5`;

CREATE TABLE `marker_data_ltd_chr5` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr6`;

CREATE TABLE `marker_data_ltd_chr6` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr7`;

CREATE TABLE `marker_data_ltd_chr7` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr8`;

CREATE TABLE `marker_data_ltd_chr8` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chr9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chr9`;

CREATE TABLE `marker_data_ltd_chr9` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chrX
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chrX`;

CREATE TABLE `marker_data_ltd_chrX` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_data_ltd_chrY
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_data_ltd_chrY`;

CREATE TABLE `marker_data_ltd_chrY` (
  `Resultset_Identifier` varchar(15) default NULL,
  `Study_Identifier` varchar(15) default NULL,
  `SignificanceID` int(12) unsigned default '0',
  `UnadjustedPValue` double unsigned default NULL,
  `NegLogPValue` double default NULL,
  `Marker_Identifier` varchar(15) default NULL,
  `Marker_Accession` varchar(15) default NULL,
  `DataSourceID` int(4) unsigned default NULL,
  `VariationType` varchar(30) default NULL COMMENT 'SO term for the type of the marker feature',
  `ValidationCode` text COMMENT 'Validation code from dbSNP',
  `Chr` varchar(20) default NULL COMMENT 'Chromosome that marker maps to. ''MT'' for mitochondrial genome, ''Un'' if unplaced on chromosome',
  `Start` int(10) unsigned default NULL,
  `Stop` int(10) unsigned default NULL,
  `Strand` smallint(1) default '0' COMMENT 'Strand orientation relative to the reference sequence: +1 for forward strand, -1 for reverse strand and 0 if not applicable or unknown',
  KEY `Region` (`Chr`,`Start`,`Stop`),
  KEY `Resultset_Identifier` (`Resultset_Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table marker_significances_chr1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr1`;

CREATE TABLE `marker_significances_chr1` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr10
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr10`;

CREATE TABLE `marker_significances_chr10` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr11
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr11`;

CREATE TABLE `marker_significances_chr11` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr12
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr12`;

CREATE TABLE `marker_significances_chr12` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr13
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr13`;

CREATE TABLE `marker_significances_chr13` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr14
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr14`;

CREATE TABLE `marker_significances_chr14` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr15
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr15`;

CREATE TABLE `marker_significances_chr15` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr16
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr16`;

CREATE TABLE `marker_significances_chr16` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr17
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr17`;

CREATE TABLE `marker_significances_chr17` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr18
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr18`;

CREATE TABLE `marker_significances_chr18` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr19
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr19`;

CREATE TABLE `marker_significances_chr19` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr2`;

CREATE TABLE `marker_significances_chr2` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr20
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr20`;

CREATE TABLE `marker_significances_chr20` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr21
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr21`;

CREATE TABLE `marker_significances_chr21` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr22
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr22`;

CREATE TABLE `marker_significances_chr22` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr3`;

CREATE TABLE `marker_significances_chr3` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr4`;

CREATE TABLE `marker_significances_chr4` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr5`;

CREATE TABLE `marker_significances_chr5` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr6`;

CREATE TABLE `marker_significances_chr6` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr7`;

CREATE TABLE `marker_significances_chr7` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr8`;

CREATE TABLE `marker_significances_chr8` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chr9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chr9`;

CREATE TABLE `marker_significances_chr9` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chrX
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chrX`;

CREATE TABLE `marker_significances_chrX` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_chrY
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_chrY`;

CREATE TABLE `marker_significances_chrY` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr1`;

CREATE TABLE `marker_significances_ltd_chr1` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr10
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr10`;

CREATE TABLE `marker_significances_ltd_chr10` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr11
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr11`;

CREATE TABLE `marker_significances_ltd_chr11` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr12
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr12`;

CREATE TABLE `marker_significances_ltd_chr12` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr13
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr13`;

CREATE TABLE `marker_significances_ltd_chr13` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr14
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr14`;

CREATE TABLE `marker_significances_ltd_chr14` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr15
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr15`;

CREATE TABLE `marker_significances_ltd_chr15` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr16
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr16`;

CREATE TABLE `marker_significances_ltd_chr16` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr17
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr17`;

CREATE TABLE `marker_significances_ltd_chr17` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr18
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr18`;

CREATE TABLE `marker_significances_ltd_chr18` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr19
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr19`;

CREATE TABLE `marker_significances_ltd_chr19` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr2`;

CREATE TABLE `marker_significances_ltd_chr2` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr20
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr20`;

CREATE TABLE `marker_significances_ltd_chr20` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr21
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr21`;

CREATE TABLE `marker_significances_ltd_chr21` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr22
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr22`;

CREATE TABLE `marker_significances_ltd_chr22` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr3`;

CREATE TABLE `marker_significances_ltd_chr3` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr4`;

CREATE TABLE `marker_significances_ltd_chr4` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr5`;

CREATE TABLE `marker_significances_ltd_chr5` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr6`;

CREATE TABLE `marker_significances_ltd_chr6` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr7`;

CREATE TABLE `marker_significances_ltd_chr7` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr8`;

CREATE TABLE `marker_significances_ltd_chr8` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chr9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chr9`;

CREATE TABLE `marker_significances_ltd_chr9` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chrX
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chrX`;

CREATE TABLE `marker_significances_ltd_chrX` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table marker_significances_ltd_chrY
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marker_significances_ltd_chrY`;

CREATE TABLE `marker_significances_ltd_chrY` (
  `Marker_Identifier` varchar(20) NOT NULL,
  `Marker_Accession` varchar(20) NOT NULL,
  `Start` bigint(20) NOT NULL,
  `Stop` bigint(20) NOT NULL,
  `HasList` enum('0','1') NOT NULL,
  `SignificanceList0` text NOT NULL,
  `SignificanceList1` text NOT NULL,
  `SignificanceList2` text NOT NULL,
  `SignificanceList3` text NOT NULL,
  `SignificanceList4` text NOT NULL,
  `SignificanceList5` text NOT NULL,
  `SignificanceList6` text NOT NULL,
  `SignificanceList7` text NOT NULL,
  `SignificanceList8` text NOT NULL,
  `SignificanceList9` text NOT NULL,
  `SignificanceList10` text NOT NULL,
  `StudyList0` text NOT NULL,
  `StudyList1` text NOT NULL,
  `StudyList2` text NOT NULL,
  `StudyList3` text NOT NULL,
  `StudyList4` text NOT NULL,
  `StudyList5` text NOT NULL,
  `StudyList6` text NOT NULL,
  `StudyList7` text NOT NULL,
  `StudyList8` text NOT NULL,
  `StudyList9` text NOT NULL,
  `StudyList10` text NOT NULL,
  KEY `MarkerID` (`Marker_Identifier`),
  KEY `Region` (`Start`,`Stop`),
  KEY `HasList` (`HasList`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;






/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
