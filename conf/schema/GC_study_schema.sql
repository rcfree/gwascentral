-- MySQL dump 10.11
--
-- Host: localhost    Database: HGVbaseG2P_study
-- ------------------------------------------------------
-- Server version	5.0.67-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AlleleFrequency`
--

DROP TABLE IF EXISTS `AlleleFrequency`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AlleleFrequency` (
  `AlleleFrequencyID` int(10) unsigned NOT NULL auto_increment,
  `FrequencyClusterID` int(12) unsigned default NULL,
  `AlleleCombo` varchar(767) default NULL,
  `FrequencyAsProportion` decimal(4,3) unsigned NOT NULL,
  PRIMARY KEY  (`AlleleFrequencyID`),
  KEY `FrequencyClusterID` (`FrequencyClusterID`),
  CONSTRAINT `AlleleFrequency_ibfk_3` FOREIGN KEY (`FrequencyClusterID`) REFERENCES `FrequencyCluster` (`FrequencyClusterID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61230802 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AnalysisMethod`
--

DROP TABLE IF EXISTS `AnalysisMethod`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AnalysisMethod` (
  `AnalysisMethodID` int(10) unsigned NOT NULL auto_increment,
  `SubmittedInStudyIdentifier` varchar(15) default NULL,
  `Identifier` varchar(15) NOT NULL,
  `Name` varchar(100) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `ProtocolParameters` text,
  PRIMARY KEY  (`AnalysisMethodID`),
  UNIQUE KEY `Identifier` (`Identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=3697 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Assayedpanel`
--

DROP TABLE IF EXISTS `Assayedpanel`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Assayedpanel` (
  `AssayedpanelID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `Identifier` varchar(15) default NULL,
  `Name` varchar(100) default NULL,
  `Description` text,
  `TotalNumberOfIndividuals` int(6) unsigned default NULL COMMENT '*...but required for association datasets',
  `NumberOfSexMale` int(6) unsigned default NULL,
  `NumberOfSexFemale` int(6) unsigned default NULL,
  `NumberOfSexUnknown` int(6) unsigned default NULL,
  `NumberOfProbands` int(6) unsigned default NULL COMMENT 'field used only if Composition = ''Trios''',
  `NumberOfParents` int(6) unsigned default NULL COMMENT 'field used only if Composition = ''Trios''',
  PRIMARY KEY  (`AssayedpanelID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `StudyID` (`StudyID`),
  CONSTRAINT `Assayedpanel_ibfk_2` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12698 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AssayedpanelCollection`
--

DROP TABLE IF EXISTS `AssayedpanelCollection`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssayedpanelCollection` (
  `AssayedpanelCollectionID` int(10) unsigned NOT NULL auto_increment,
  `AssayedpanelID` int(10) unsigned NOT NULL,
  `ExperimentID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`AssayedpanelCollectionID`),
  UNIQUE KEY `ExperimentID` (`ExperimentID`,`AssayedpanelID`),
  KEY `AssayedpanelID` (`AssayedpanelID`),
  CONSTRAINT `AssayedpanelCollectionnew_ibfk_2` FOREIGN KEY (`AssayedpanelID`) REFERENCES `Assayedpanel` (`AssayedpanelID`) ON DELETE CASCADE,
  CONSTRAINT `AssayedpanelCollectionnew_ibfk_3` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Author`
--

DROP TABLE IF EXISTS `Author`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Author` (
  `AuthorID` int(10) unsigned NOT NULL auto_increment,
  `ResearcherID` smallint(5) unsigned NOT NULL default '0',
  `StudyID` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`AuthorID`),
  UNIQUE KEY `ResearcherID` (`ResearcherID`,`StudyID`),
  KEY `StudyID` (`StudyID`),
  CONSTRAINT `Author_ibfk_1` FOREIGN KEY (`ResearcherID`) REFERENCES `Researcher` (`ResearcherID`),
  CONSTRAINT `Author_ibfk_2` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `AuthorCommunication`
--

DROP TABLE IF EXISTS `AuthorCommunication`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AuthorCommunication` (
  `AuthorCommunicationID` mediumint(9) NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL COMMENT 'Points to study that this communication was sent in relation to',
  `ContactID` mediumint(9) NOT NULL,
  `ContactName` varchar(500) default NULL,
  `DateContacted` date default NULL,
  `DateResponse` date default NULL,
  `TypeOfResponse` enum('Public response and released additional data','Private response and released additional data','Public response','Private response','E-mail bounce') default NULL,
  `ResponseText` text,
  PRIMARY KEY  (`AuthorCommunicationID`)
) ENGINE=InnoDB AUTO_INCREMENT=896 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Build`
--

DROP TABLE IF EXISTS `Build`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Build` (
  `BuildID` int(10) unsigned NOT NULL auto_increment,
  `Database` varchar(50) NOT NULL,
  `FullName` varchar(250) default NULL,
  `URL` varchar(250) default NULL,
  `Build` varchar(10) NOT NULL default 'N/A' COMMENT 'Start at 21 for HGVbase-G2P. Latest download for dbSNP',
  `Date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`BuildID`),
  UNIQUE KEY `Database` (`Database`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `BundleLoci`
--

DROP TABLE IF EXISTS `BundleLoci`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `BundleLoci` (
  `BundleLociID` int(6) unsigned NOT NULL auto_increment,
  `BundleName` varchar(100) NOT NULL COMMENT 'Name of the standard bundle of markers',
  `Loci` text COMMENT 'Pipe (',
  PRIMARY KEY  (`BundleLociID`),
  UNIQUE KEY `BundleName` (`BundleName`)
) ENGINE=InnoDB AUTO_INCREMENT=1717 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Citation`
--

DROP TABLE IF EXISTS `Citation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Citation` (
  `CitationID` smallint(5) unsigned NOT NULL auto_increment,
  `DOI` varchar(500) default NULL,
  `PubmedID` int(10) unsigned NOT NULL,
  `Authors` text,
  `Title` varchar(250) default NULL,
  `Detail` varchar(250) default NULL,
  `MeshTerms` text,
  `IsPrimary` enum('yes','no') NOT NULL default 'no',
  PRIMARY KEY  (`CitationID`),
  UNIQUE KEY `PubmedID` (`PubmedID`),
  UNIQUE KEY `DOI` (`DOI`)
) ENGINE=InnoDB AUTO_INCREMENT=2213 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `CitationCrossref_old`
--

DROP TABLE IF EXISTS `CitationCrossref_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `CitationCrossref_old` (
  `CitationCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `CitationID` smallint(5) unsigned NOT NULL default '0',
  `CrossrefID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`CitationCrossrefID`),
  UNIQUE KEY `CitationID` (`CitationID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `CitationCrossref_old_ibfk_3` FOREIGN KEY (`CitationID`) REFERENCES `Citation` (`CitationID`) ON DELETE CASCADE,
  CONSTRAINT `CitationCrossref_old_ibfk_4` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref_old` (`CrossrefID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Composition_TypeList`
--

DROP TABLE IF EXISTS `Composition_TypeList`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Composition_TypeList` (
  `Type` varchar(100) NOT NULL COMMENT 'initial values: (''Undefined'', ''Unrelated cases'',',
  PRIMARY KEY  (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Contribution`
--

DROP TABLE IF EXISTS `Contribution`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Contribution` (
  `ContributionID` int(10) NOT NULL auto_increment,
  `ResearcherID` smallint(5) unsigned NOT NULL,
  `SubmissionID` int(10) unsigned NOT NULL,
  `IsSubmitter` enum('yes','no') NOT NULL,
  `IsAuthor` enum('yes','no') NOT NULL,
  `IsSource` enum('yes','no') NOT NULL,
  PRIMARY KEY  (`ContributionID`),
  KEY `SubmissionID` (`SubmissionID`),
  KEY `ResearcherID` (`ResearcherID`),
  CONSTRAINT `Contribution_ibfk_2` FOREIGN KEY (`ResearcherID`) REFERENCES `Researcher` (`ResearcherID`),
  CONSTRAINT `Contribution_ibfk_3` FOREIGN KEY (`SubmissionID`) REFERENCES `Submission` (`SubmissionID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6797 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Crossref_old`
--

DROP TABLE IF EXISTS `Crossref_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Crossref_old` (
  `CrossrefID` int(10) unsigned NOT NULL auto_increment,
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  `UrlID` varchar(100) default '',
  PRIMARY KEY  (`CrossrefID`)
) ENGINE=InnoDB AUTO_INCREMENT=6870 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `DiseaseCategoryBkup`
--

DROP TABLE IF EXISTS `DiseaseCategoryBkup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DiseaseCategoryBkup` (
  `DiseaseCategoryID` int(6) unsigned NOT NULL default '0',
  `ParentCategoryID` int(6) unsigned default NULL COMMENT 'PK for parent category, if this is not a root term',
  `MeSHTreeID` varchar(30) default NULL COMMENT 'MeSH tree ID, for linking back to NLM website',
  `Name` varchar(500) default NULL,
  `Description` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Experiment`
--

DROP TABLE IF EXISTS `Experiment`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Experiment` (
  `ExperimentID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `PhenotypeMethodID` int(10) unsigned default NULL,
  `Identifier` varchar(15) default NULL,
  `Name` varchar(100) default NULL,
  `Design` varchar(50) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `ExperimentType` varchar(100) NOT NULL default '' COMMENT 'In (SELECT [Type] FROM [Experiment_TypeList];)',
  `TotalMarkersTested` int(6) unsigned default NULL,
  `TotalMarkersImported` int(6) unsigned default NULL,
  `Objective` text,
  `Outcome` text,
  `Comments` text,
  `IndividualDataStatement` text,
  `FrequencySummary` text,
  `SignificanceSummary` text,
  `TimeCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `GenotypedBundle` text,
  `AccessLevel` varchar(10) default NULL,
  `ImportConfig` text,
  PRIMARY KEY  (`ExperimentID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `PhenotypeMethodID` (`PhenotypeMethodID`),
  KEY `StudyID` (`StudyID`),
  CONSTRAINT `Experiment_ibfk_2` FOREIGN KEY (`PhenotypeMethodID`) REFERENCES `PhenotypeMethod` (`PhenotypeMethodID`),
  CONSTRAINT `Experiment_ibfk_4` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4128 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ExperimentAssayedpanel`
--

DROP TABLE IF EXISTS `ExperimentAssayedpanel`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ExperimentAssayedpanel` (
  `ExperimentAssayedpanelID` int(10) unsigned NOT NULL auto_increment,
  `AssayedpanelID` int(10) unsigned NOT NULL,
  `ExperimentID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`ExperimentAssayedpanelID`),
  UNIQUE KEY `ExperimentID` (`ExperimentID`,`AssayedpanelID`),
  KEY `AssayedpanelID` (`AssayedpanelID`),
  CONSTRAINT `ExperimentAssayedpanel_ibfk_2` FOREIGN KEY (`AssayedpanelID`) REFERENCES `Assayedpanel` (`AssayedpanelID`) ON DELETE CASCADE,
  CONSTRAINT `ExperimentAssayedpanel_ibfk_3` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6961 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ExperimentHotlink`
--

DROP TABLE IF EXISTS `ExperimentHotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ExperimentHotlink` (
  `ExperimentHotlinkID` int(10) unsigned NOT NULL auto_increment,
  `ExperimentID` int(10) unsigned NOT NULL default '0',
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ExperimentHotlinkID`),
  UNIQUE KEY `ExperimentID` (`ExperimentID`,`HotlinkID`),
  KEY `HotlinkID` (`HotlinkID`),
  CONSTRAINT `experimenthotlink_ibfk_1` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE,
  CONSTRAINT `experimenthotlink_ibfk_2` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=2527 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Experiment_TypeList`
--

DROP TABLE IF EXISTS `Experiment_TypeList`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Experiment_TypeList` (
  `Type` varchar(100) NOT NULL default '' COMMENT 'initial values: (''Polymorphism frequency determination'', ''Case-control association study'',',
  PRIMARY KEY  (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `FCS`
--

DROP TABLE IF EXISTS `FCS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FCS` (
  `FCSID` int(10) unsigned NOT NULL auto_increment,
  `SignificanceID` int(12) unsigned NOT NULL,
  `FrequencyClusterID` int(12) unsigned NOT NULL,
  PRIMARY KEY  (`FCSID`),
  UNIQUE KEY `GenotypeSignificanceID` (`SignificanceID`,`FrequencyClusterID`),
  KEY `FrequencyClusterID` (`FrequencyClusterID`),
  CONSTRAINT `FCS_ibfk_2` FOREIGN KEY (`SignificanceID`) REFERENCES `Significance` (`SignificanceID`) ON DELETE CASCADE,
  CONSTRAINT `FCS_ibfk_3` FOREIGN KEY (`FrequencyClusterID`) REFERENCES `FrequencyCluster` (`FrequencyClusterID`)
) ENGINE=InnoDB AUTO_INCREMENT=50785619 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `FrequencyCluster`
--

DROP TABLE IF EXISTS `FrequencyCluster`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FrequencyCluster` (
  `FrequencyClusterID` int(12) unsigned NOT NULL auto_increment,
  `UsedmarkersetID` int(12) unsigned NOT NULL,
  `MarkerID` int(10) unsigned NOT NULL,
  `ExperimentID` int(10) unsigned default NULL,
  `AssayedpanelID` int(10) unsigned NOT NULL,
  `NumberOfGenotypedSamples` smallint(5) unsigned NOT NULL,
  `PValueHWE` decimal(6,5) unsigned default NULL,
  PRIMARY KEY  (`FrequencyClusterID`),
  KEY `AssayedpanelID` (`AssayedpanelID`),
  KEY `ExperimentID` (`ExperimentID`),
  KEY `UsedmarkersetID` (`UsedmarkersetID`),
  KEY `MarkerID` (`MarkerID`),
  CONSTRAINT `FrequencyCluster_ibfk_1` FOREIGN KEY (`AssayedpanelID`) REFERENCES `Assayedpanel` (`AssayedpanelID`) ON DELETE CASCADE,
  CONSTRAINT `FrequencyCluster_ibfk_6` FOREIGN KEY (`UsedmarkersetID`) REFERENCES `Usedmarkerset` (`UsedmarkersetID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25844166 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `GenotypeFrequency`
--

DROP TABLE IF EXISTS `GenotypeFrequency`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GenotypeFrequency` (
  `GenotypeFrequencyID` int(10) unsigned NOT NULL auto_increment,
  `FrequencyClusterID` int(12) unsigned default NULL,
  `GenotypeCombo` varchar(767) default NULL,
  `FrequencyAsProportion` decimal(4,3) unsigned NOT NULL,
  `NumberSamplesWithGenotype` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`GenotypeFrequencyID`),
  KEY `FrequencyClusterID` (`FrequencyClusterID`),
  CONSTRAINT `GenotypeFrequency_ibfk_2` FOREIGN KEY (`FrequencyClusterID`) REFERENCES `FrequencyCluster` (`FrequencyClusterID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43783435 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `GenotypedBundle`
--

DROP TABLE IF EXISTS `GenotypedBundle`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GenotypedBundle` (
  `GenotypedBundleID` int(10) unsigned NOT NULL auto_increment,
  `BundleLociID` int(6) unsigned NOT NULL,
  `ExperimentID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`GenotypedBundleID`),
  UNIQUE KEY `BundleLociID_2` (`BundleLociID`,`ExperimentID`),
  KEY `ExperimentID` (`ExperimentID`),
  CONSTRAINT `GenotypedBundle_ibfk_4` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE,
  CONSTRAINT `GenotypedBundle_ibfk_5` FOREIGN KEY (`BundleLociID`) REFERENCES `BundleLoci` (`BundleLociID`)
) ENGINE=InnoDB AUTO_INCREMENT=4203 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `GenotypedLoci`
--

DROP TABLE IF EXISTS `GenotypedLoci`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GenotypedLoci` (
  `GenotypedLociID` int(10) unsigned NOT NULL,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `GenotypedLoci` text NOT NULL COMMENT 'Pipe (',
  PRIMARY KEY  (`GenotypedLociID`),
  KEY `StudyID` (`StudyID`),
  CONSTRAINT `GenotypedLoci_ibfk_1` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Hotlink`
--

DROP TABLE IF EXISTS `Hotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Hotlink` (
  `HotlinkID` int(10) unsigned NOT NULL auto_increment,
  `HotlinkLabel` varchar(100) NOT NULL COMMENT 'Indicate where the URL is pointing, e.g. "dbSNP refSNP report"',
  `UrlPrefix` varchar(300) NOT NULL,
  `UrlSuffix` varchar(100) default NULL,
  `Identifier` varchar(15) default NULL,
  PRIMARY KEY  (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=4425 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `HotlinkCollection`
--

DROP TABLE IF EXISTS `HotlinkCollection`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `HotlinkCollection` (
  `HotlinkCollectionID` int(10) unsigned NOT NULL auto_increment,
  `Identifier` varchar(15) default NULL,
  `StudyID` int(10) NOT NULL,
  PRIMARY KEY  (`HotlinkCollectionID`),
  KEY `StudyID` (`StudyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Hotlinker`
--

DROP TABLE IF EXISTS `Hotlinker`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Hotlinker` (
  `HotlinkerID` int(10) unsigned NOT NULL auto_increment,
  `HotlinkCollectionID` int(10) unsigned NOT NULL,
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`HotlinkerID`),
  KEY `HotlinkCollectionID` (`HotlinkCollectionID`),
  CONSTRAINT `Hotlinker_ibfk_1` FOREIGN KEY (`HotlinkCollectionID`) REFERENCES `HotlinkCollection` (`HotlinkCollectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PPPA`
--

DROP TABLE IF EXISTS `PPPA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PPPA` (
  `PPPAID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypePropertyID` int(10) unsigned NOT NULL,
  `PhenotypeAnnotationID` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`PPPAID`),
  KEY `PhenotypePropertyID` (`PhenotypePropertyID`),
  KEY `PhenotypeAnnotationID` (`PhenotypeAnnotationID`),
  CONSTRAINT `PPPA_ibfk_1` FOREIGN KEY (`PhenotypePropertyID`) REFERENCES `PhenotypeProperty` (`PhenotypePropertyID`),
  CONSTRAINT `PPPA_ibfk_2` FOREIGN KEY (`PhenotypeAnnotationID`) REFERENCES `PhenotypeAnnotation` (`PhenotypeAnnotationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2557 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PVSC`
--

DROP TABLE IF EXISTS `PVSC`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PVSC` (
  `PVSCID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeValueID` int(10) unsigned NOT NULL,
  `SelectionCriteriaID` int(8) unsigned NOT NULL,
  PRIMARY KEY  (`PVSCID`),
  UNIQUE KEY `PhenotypeValueID` (`PhenotypeValueID`,`SelectionCriteriaID`),
  KEY `SelectionCriteriaID` (`SelectionCriteriaID`),
  CONSTRAINT `PVSC_ibfk_2` FOREIGN KEY (`PhenotypeValueID`) REFERENCES `PhenotypeValue` (`PhenotypeValueID`),
  CONSTRAINT `PVSC_ibfk_3` FOREIGN KEY (`SelectionCriteriaID`) REFERENCES `SelectionCriteria` (`SelectionCriteriaID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=775 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeAnnotation`
--

DROP TABLE IF EXISTS `PhenotypeAnnotation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeAnnotation` (
  `PhenotypeAnnotationID` int(6) unsigned NOT NULL auto_increment,
  `AnnotationOrigin` enum('mesh','hpo','unknown') default NULL,
  `PhenotypeIdentifier` varchar(30) default NULL,
  `ExactMatch` enum('1','0') NOT NULL default '1',
  PRIMARY KEY  (`PhenotypeAnnotationID`),
  KEY `PhenotypeIdentifier` (`PhenotypeIdentifier`),
  KEY `Origin` (`AnnotationOrigin`)
) ENGINE=InnoDB AUTO_INCREMENT=2864 DEFAULT CHARSET=latin1 COMMENT='Ontology annotation for phenotype properties (MeSH)';
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeMethod`
--

DROP TABLE IF EXISTS `PhenotypeMethod`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeMethod` (
  `PhenotypeMethodID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL COMMENT 'Points to study that this method came in',
  `PhenotypePropertyID` int(10) unsigned NOT NULL,
  `Identifier` varchar(15) default NULL,
  `Name` varchar(255) default NULL,
  `Description` text,
  `Sample` varchar(100) default NULL COMMENT 'Biological system or sample type being assessed',
  `MeasuredAttribute` varchar(100) default NULL COMMENT 'i.e., analyte/feature & property being tested, E.g., Alzheimers\n Disease state, NaCl concentration',
  `VariableType` varchar(25) default NULL COMMENT 'Kind of variable: "Continuous", "nominal" or "ordinal"',
  `Unit` varchar(25) default NULL COMMENT 'Unit of measurement (e.g. kg)',
  `Circumstance` text COMMENT 'Any particular conditions under which the test is done',
  `TimeInstant` text COMMENT 'Either; when was the assessment done',
  `TimePeriod` text COMMENT 'Or; over what period was the assessment done',
  `Details` text COMMENT 'Any other specific details of the test',
  `AccessLevel` varchar(10) default NULL,
  PRIMARY KEY  (`PhenotypeMethodID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `PhenotypePropertyID` (`PhenotypePropertyID`),
  KEY `StudyID` (`StudyID`),
  CONSTRAINT `PhenotypeMethod_ibfk_2` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `PhenotypeMethod_ibfk_3` FOREIGN KEY (`PhenotypePropertyID`) REFERENCES `PhenotypeProperty` (`PhenotypePropertyID`)
) ENGINE=InnoDB AUTO_INCREMENT=3872 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeMethodCitation`
--

DROP TABLE IF EXISTS `PhenotypeMethodCitation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeMethodCitation` (
  `PhenotypeMethodCitationID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeMethodID` int(10) unsigned NOT NULL default '0',
  `CitationID` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`PhenotypeMethodCitationID`),
  UNIQUE KEY `PhenotypeMethodID` (`PhenotypeMethodID`,`CitationID`),
  KEY `CitationID` (`CitationID`),
  CONSTRAINT `PhenotypeMethodCitation_ibfk_2` FOREIGN KEY (`PhenotypeMethodID`) REFERENCES `PhenotypeMethod` (`PhenotypeMethodID`) ON DELETE CASCADE,
  CONSTRAINT `PhenotypeMethodCitation_ibfk_3` FOREIGN KEY (`CitationID`) REFERENCES `Citation` (`CitationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeMethodCrossref_old`
--

DROP TABLE IF EXISTS `PhenotypeMethodCrossref_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeMethodCrossref_old` (
  `PhenotypeMethodCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeMethodID` int(10) unsigned NOT NULL,
  `CrossrefID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`PhenotypeMethodCrossrefID`),
  UNIQUE KEY `PhenotypeMethodID` (`PhenotypeMethodID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `PhenotypeMethodCrossref_old_ibfk_1` FOREIGN KEY (`PhenotypeMethodID`) REFERENCES `PhenotypeMethod` (`PhenotypeMethodID`) ON DELETE CASCADE,
  CONSTRAINT `PhenotypeMethodCrossref_old_ibfk_2` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref_old` (`CrossrefID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeMethodHotlink`
--

DROP TABLE IF EXISTS `PhenotypeMethodHotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeMethodHotlink` (
  `PhenotypeMethodHotlinkID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeMethodID` int(10) unsigned NOT NULL default '0',
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`PhenotypeMethodHotlinkID`),
  UNIQUE KEY `PhenotypeMethodID` (`PhenotypeMethodID`,`HotlinkID`),
  KEY `HotlinkID` (`HotlinkID`),
  CONSTRAINT `phenotypemethodhotlink_ibfk_1` FOREIGN KEY (`PhenotypeMethodID`) REFERENCES `PhenotypeMethod` (`PhenotypeMethodID`) ON DELETE CASCADE,
  CONSTRAINT `phenotypemethodhotlink_ibfk_2` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeProperty`
--

DROP TABLE IF EXISTS `PhenotypeProperty`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeProperty` (
  `PhenotypePropertyID` int(10) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Description` text,
  PRIMARY KEY  (`PhenotypePropertyID`)
) ENGINE=InnoDB AUTO_INCREMENT=2984 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypePropertyCitation`
--

DROP TABLE IF EXISTS `PhenotypePropertyCitation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypePropertyCitation` (
  `PhenotypePropertyCitationID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypePropertyID` int(10) unsigned NOT NULL default '0',
  `CitationID` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`PhenotypePropertyCitationID`),
  UNIQUE KEY `PhenotypePropertyID` (`PhenotypePropertyID`,`CitationID`),
  KEY `CitationID` (`CitationID`),
  CONSTRAINT `PhenotypePropertyCitation_ibfk_2` FOREIGN KEY (`PhenotypePropertyID`) REFERENCES `PhenotypeProperty` (`PhenotypePropertyID`),
  CONSTRAINT `PhenotypePropertyCitation_ibfk_3` FOREIGN KEY (`CitationID`) REFERENCES `Citation` (`CitationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeProperty_old`
--

DROP TABLE IF EXISTS `PhenotypeProperty_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeProperty_old` (
  `PhenotypePropertyID` int(10) unsigned NOT NULL auto_increment,
  `DiseaseCategoryID` int(6) unsigned default NULL,
  `Name` varchar(255) default NULL,
  `Description` text,
  PRIMARY KEY  (`PhenotypePropertyID`),
  UNIQUE KEY `Name` (`Name`),
  KEY `DiseaseCategoryID` (`DiseaseCategoryID`)
) ENGINE=MyISAM AUTO_INCREMENT=1172 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeValue`
--

DROP TABLE IF EXISTS `PhenotypeValue`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeValue` (
  `PhenotypeValueID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeMethodID` int(10) unsigned NOT NULL COMMENT 'Reference to the method used to generate this value',
  `SamplepanelID` int(10) unsigned default NULL COMMENT 'Reference to the Samplepanel that was measured with the method to get this value (if applicable; not all values are connected to a panel)',
  `Value` varchar(500) NOT NULL COMMENT 'The actual result from the measurement (returned by the assay)  for the individuals on the panel. Can be a mean if variable is continuous (e.g. mean weight)',
  `ValueRank` int(3) unsigned default NULL COMMENT 'Rank of this value, if this is an ordinal (or ranked) variable.',
  `ValueIsMean` varchar(3) default NULL COMMENT 'Flag indicating whether the value is a mean (for a continuous variable) or not',
  `StdDev` float unsigned default NULL COMMENT 'Standard deviation (for continuous variables)',
  `Median` float unsigned default NULL COMMENT 'Median (for continuous variables)',
  `Min` float unsigned default NULL COMMENT 'Minimum value (for continuous variables)',
  `Max` float unsigned default NULL COMMENT 'Maximum value (for continuous variables)',
  `NumberOfIndividuals` int(6) unsigned default NULL COMMENT 'Number of individuals with this value if variable is discreet, or contributing to mean (if variable is continuous)',
  `Qualifier` varchar(100) default NULL COMMENT 'Can be used for setting thresholds (e.g. weight > 80kg) when splitting up panels.',
  PRIMARY KEY  (`PhenotypeValueID`),
  UNIQUE KEY `CombinedMethodID` (`PhenotypeMethodID`,`SamplepanelID`,`Value`),
  KEY `PhenotypeMethodID` (`PhenotypeMethodID`),
  KEY `SamplepanelID` (`SamplepanelID`),
  CONSTRAINT `PhenotypeValue_ibfk_1` FOREIGN KEY (`PhenotypeMethodID`) REFERENCES `PhenotypeMethod` (`PhenotypeMethodID`) ON DELETE CASCADE,
  CONSTRAINT `PhenotypeValue_ibfk_2` FOREIGN KEY (`SamplepanelID`) REFERENCES `Samplepanel` (`SamplepanelID`)
) ENGINE=InnoDB AUTO_INCREMENT=7881 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `PhenotypeValueDetail`
--

DROP TABLE IF EXISTS `PhenotypeValueDetail`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PhenotypeValueDetail` (
  `PhenotypeValueDetailID` int(10) unsigned NOT NULL auto_increment,
  `PhenotypeValueID` int(10) unsigned NOT NULL default '0',
  `Attribute` varchar(100) NOT NULL default '',
  `PhenotypeValueDetail` varchar(150) NOT NULL,
  `UnitOfMeasurement` varchar(30) default NULL,
  `Qualifier` varchar(100) default NULL,
  PRIMARY KEY  (`PhenotypeValueDetailID`),
  UNIQUE KEY `Attribute` (`Attribute`,`PhenotypeValueID`),
  KEY `PhenotypeValueID` (`PhenotypeValueID`),
  CONSTRAINT `PhenotypeValueDetail_ibfk_1` FOREIGN KEY (`PhenotypeValueID`) REFERENCES `PhenotypeValue` (`PhenotypeValueID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Properties`
--

DROP TABLE IF EXISTS `Properties`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Properties` (
  `name` varchar(100) default NULL,
  `value` varchar(100) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Researcher`
--

DROP TABLE IF EXISTS `Researcher`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Researcher` (
  `ResearcherID` smallint(5) unsigned NOT NULL auto_increment,
  `SubmitterHandle` varchar(20) default NULL,
  `ShortName` varchar(100) NOT NULL,
  `FullName` varchar(200) default NULL,
  `Department` varchar(100) default NULL,
  `Institution` varchar(100) NOT NULL,
  `Address` varchar(250) default NULL,
  `Phone` varchar(50) default NULL,
  `Fax` varchar(30) default NULL,
  `Email` varchar(150) default NULL,
  `WWW` varchar(100) default NULL,
  PRIMARY KEY  (`ResearcherID`),
  UNIQUE KEY `SubmitterHandle` (`SubmitterHandle`)
) ENGINE=InnoDB AUTO_INCREMENT=4203 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Resultset`
--

DROP TABLE IF EXISTS `Resultset`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Resultset` (
  `ResultsetID` int(10) unsigned NOT NULL auto_increment,
  `ExperimentID` int(10) unsigned NOT NULL,
  `AnalysisMethodID` int(10) unsigned default NULL,
  `Identifier` varchar(15) default NULL,
  `Name` varchar(100) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `ProtocolParameters` text,
  `TimeCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ResultsetID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  UNIQUE KEY `Name` (`Name`,`ExperimentID`),
  KEY `ExperimentID` (`ExperimentID`),
  KEY `AnalysisMethodID` (`AnalysisMethodID`),
  CONSTRAINT `Resultset_ibfk_2` FOREIGN KEY (`AnalysisMethodID`) REFERENCES `AnalysisMethod` (`AnalysisMethodID`),
  CONSTRAINT `Resultset_ibfk_3` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4020 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ResultsetCrossref_old`
--

DROP TABLE IF EXISTS `ResultsetCrossref_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ResultsetCrossref_old` (
  `ResultsetCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `ResultsetID` int(10) unsigned NOT NULL default '0',
  `CrossrefID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ResultsetCrossrefID`),
  UNIQUE KEY `ResultsetID` (`ResultsetID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `ResultsetCrossref_old_ibfk_1` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref_old` (`CrossrefID`) ON DELETE CASCADE,
  CONSTRAINT `ResultsetCrossref_old_ibfk_2` FOREIGN KEY (`ResultsetID`) REFERENCES `Resultset` (`ResultsetID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ResultsetHotlink`
--

DROP TABLE IF EXISTS `ResultsetHotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ResultsetHotlink` (
  `ResultsetHotlinkID` int(10) unsigned NOT NULL auto_increment,
  `ResultsetID` int(10) unsigned NOT NULL default '0',
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ResultsetHotlinkID`),
  UNIQUE KEY `ResultsetID` (`ResultsetID`,`HotlinkID`),
  KEY `HotlinkID` (`HotlinkID`),
  CONSTRAINT `resultsethotlink_ibfk_1` FOREIGN KEY (`ResultsetID`) REFERENCES `Resultset` (`ResultsetID`) ON DELETE CASCADE,
  CONSTRAINT `resultsethotlink_ibfk_2` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Samplepanel`
--

DROP TABLE IF EXISTS `Samplepanel`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Samplepanel` (
  `SamplepanelID` int(10) unsigned NOT NULL auto_increment,
  `SubmittedInStudyIdentifier` varchar(15) default NULL,
  `ParentSamplepanelID` int(10) unsigned default NULL,
  `Identifier` varchar(15) default NULL,
  `Name` varchar(100) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `Composition` text,
  `TotalNumberOfIndividuals` int(6) unsigned default NULL COMMENT '*...but required for association datasets',
  `NumberOfSexMale` int(6) unsigned default NULL,
  `NumberOfSexFemale` int(6) unsigned default NULL,
  `NumberOfSexUnknown` int(6) unsigned default NULL,
  `NumberOfProbands` int(6) unsigned default NULL COMMENT 'field used only if Composition = ''Trios''',
  `NumberOfParents` int(6) unsigned default NULL COMMENT 'field used only if Composition = ''Trios''',
  `ModeOfRecruitment` text,
  `DiagnosisAgeRange` varchar(150) default NULL,
  `DiagnosisPeriod` varchar(150) default NULL,
  `SamplingAgeRange` varchar(150) default NULL,
  `SamplingPeriod` varchar(150) default NULL,
  `PopulationInfo` varchar(250) default NULL,
  `GeographicRegionInfo` varchar(250) default NULL,
  `EthnicityInfo` varchar(250) default NULL,
  `BirthPlaceInfo` varchar(250) default NULL,
  `AdmixtureInfo` varchar(250) default NULL,
  `EnvironmentInfo` text,
  `SourceOfDNA` varchar(100) default NULL COMMENT 'In (SELECT [SourceOfDNA] FROM [Sampleset_SourceOfDNAList];)',
  `DNAsArePooled` enum('Undefined','Pre-prep','Post-prep','No') NOT NULL default 'Undefined',
  `DNAsAreWGA` enum('Undefined','None','All','Some') NOT NULL default 'Undefined',
  PRIMARY KEY  (`SamplepanelID`),
  UNIQUE KEY `Name` (`Name`,`SubmittedInStudyIdentifier`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `ParentSamplepanelID` (`ParentSamplepanelID`)
) ENGINE=InnoDB AUTO_INCREMENT=9433 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SamplepanelCrossref`
--

DROP TABLE IF EXISTS `SamplepanelCrossref`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SamplepanelCrossref` (
  `SamplepanelCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `SamplepanelID` int(10) unsigned NOT NULL,
  `CrossrefID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`SamplepanelCrossrefID`),
  UNIQUE KEY `SamplepanelID` (`SamplepanelID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `SamplepanelCrossref_ibfk_5` FOREIGN KEY (`SamplepanelID`) REFERENCES `Samplepanel` (`SamplepanelID`) ON DELETE CASCADE,
  CONSTRAINT `SamplepanelCrossref_ibfk_6` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref_old` (`CrossrefID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5744 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SamplepanelHotlink`
--

DROP TABLE IF EXISTS `SamplepanelHotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SamplepanelHotlink` (
  `SamplepanelHotlinkID` int(10) unsigned NOT NULL auto_increment,
  `SamplepanelID` int(10) unsigned NOT NULL default '0',
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`SamplepanelHotlinkID`),
  UNIQUE KEY `SamplepanelID` (`SamplepanelID`,`HotlinkID`),
  KEY `HotlinkID` (`HotlinkID`),
  CONSTRAINT `SamplepanelHotlink_ibfk_2` FOREIGN KEY (`SamplepanelID`) REFERENCES `Samplepanel` (`SamplepanelID`) ON DELETE CASCADE,
  CONSTRAINT `SamplepanelHotlink_ibfk_3` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=9357 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SamplepanelOverlap`
--

DROP TABLE IF EXISTS `SamplepanelOverlap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SamplepanelOverlap` (
  `SamplepanelOverlapID` smallint(5) unsigned NOT NULL default '0',
  `SamplepanelID_New` int(10) unsigned NOT NULL default '0' COMMENT 'This ID is the current Sampleset',
  `SamplepanelID_Old` int(10) unsigned NOT NULL default '0' COMMENT 'This ID is the overlapping Sampleset',
  `OverlapDetails` varchar(250) default 'This samplepanel overlaps with another',
  PRIMARY KEY  (`SamplepanelOverlapID`),
  KEY `SamplesetID_New` (`SamplepanelID_New`),
  KEY `SamplesetID_Old` (`SamplepanelID_Old`),
  CONSTRAINT `SamplepanelOverlap_ibfk_1` FOREIGN KEY (`SamplepanelID_New`) REFERENCES `Samplepanel` (`SamplepanelID`),
  CONSTRAINT `SamplepanelOverlap_ibfk_2` FOREIGN KEY (`SamplepanelID_Old`) REFERENCES `Samplepanel` (`SamplepanelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Samplepanel_SourceOfDNAList`
--

DROP TABLE IF EXISTS `Samplepanel_SourceOfDNAList`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Samplepanel_SourceOfDNAList` (
  `SourceOfDNA` varchar(100) NOT NULL default '' COMMENT 'initial values: (''Cultured cells'',''Tissue biopsy''',
  PRIMARY KEY  (`SourceOfDNA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SelectionCriteria`
--

DROP TABLE IF EXISTS `SelectionCriteria`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SelectionCriteria` (
  `SelectionCriteriaID` int(8) unsigned NOT NULL auto_increment,
  `AssayedpanelID` int(10) unsigned NOT NULL,
  `SamplepanelID` int(10) unsigned default NULL,
  `SourceAssayedpanelID` int(10) unsigned default NULL COMMENT 'Points to assayedpanel (usually more than one) that a given apanel is made up from',
  `EnvironmentCriteria` varchar(500) default 'No selection',
  `NumberOfIndividuals` int(6) unsigned default NULL,
  PRIMARY KEY  (`SelectionCriteriaID`),
  UNIQUE KEY `AssayedpanelID` (`AssayedpanelID`,`SamplepanelID`),
  UNIQUE KEY `SourceAssayedpanelID` (`SourceAssayedpanelID`,`AssayedpanelID`),
  KEY `SamplepanelID` (`SamplepanelID`),
  CONSTRAINT `SelectionCriteria_ibfk_16` FOREIGN KEY (`AssayedpanelID`) REFERENCES `Assayedpanel` (`AssayedpanelID`) ON DELETE CASCADE,
  CONSTRAINT `SelectionCriteria_ibfk_17` FOREIGN KEY (`SamplepanelID`) REFERENCES `Samplepanel` (`SamplepanelID`) ON DELETE CASCADE,
  CONSTRAINT `SelectionCriteria_ibfk_18` FOREIGN KEY (`SourceAssayedpanelID`) REFERENCES `Assayedpanel` (`AssayedpanelID`)
) ENGINE=InnoDB AUTO_INCREMENT=12887 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Significance`
--

DROP TABLE IF EXISTS `Significance`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Significance` (
  `SignificanceID` int(12) unsigned NOT NULL auto_increment,
  `ResultsetID` int(10) unsigned default NULL,
  `UsedmarkersetID` int(12) unsigned NOT NULL,
  `HotlinkCollectionID` int(10) unsigned NOT NULL,
  `AdjustedPValue` float unsigned default NULL,
  `UnadjustedPValue` double unsigned default NULL,
  `OddsRatioStatement` varchar(250) default NULL,
  `AttributableRiskStatement` varchar(250) default NULL,
  `IsLimited` enum('yes','no') default NULL,
  PRIMARY KEY  (`SignificanceID`),
  KEY `UsedmarkersetID` (`UsedmarkersetID`),
  KEY `ResultsetID` (`ResultsetID`),
  KEY `HotlinkCollectionID` (`HotlinkCollectionID`),
  KEY `UnadjustedPValue` (`UnadjustedPValue`),
  CONSTRAINT `Significance_ibfk_23` FOREIGN KEY (`UsedmarkersetID`) REFERENCES `Usedmarkerset` (`UsedmarkersetID`) ON DELETE CASCADE,
  CONSTRAINT `Significance_ibfk_25` FOREIGN KEY (`ResultsetID`) REFERENCES `Resultset` (`ResultsetID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53241845 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Study`
--

DROP TABLE IF EXISTS `Study`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Study` (
  `StudyID` mediumint(8) unsigned NOT NULL auto_increment,
  `Identifier` varchar(15) default NULL,
  `Title` text,
  `Name` varchar(500) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `IsHidden` enum('yes','no') default NULL,
  `StudyAbstract` text NOT NULL,
  `Background` text,
  `Objectives` text,
  `KeyResults` text,
  `Conclusions` text,
  `StudyDesign` text,
  `StudySizeReason` text,
  `StudyPower` text,
  `SourcesOfBias` text,
  `Limitations` text,
  `Acknowledgements` text,
  `TimeCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `TimeUpdated` timestamp NOT NULL default '0000-00-00 00:00:00',
  `IsDisplayed` enum('yes','no') default NULL,
  `SignificanceLevel` smallint(6) default '0',
  `AccessLevel` varchar(10) default NULL,
  PRIMARY KEY  (`StudyID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `SignificanceLevel` (`SignificanceLevel`)
) ENGINE=InnoDB AUTO_INCREMENT=2239 DEFAULT CHARSET=latin1 COMMENT='A DATASET COMPRISING ALL EXPERIMENTS RELEVANT TO ONE RESEARC';
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudyAnalysisMethod`
--

DROP TABLE IF EXISTS `StudyAnalysisMethod`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudyAnalysisMethod` (
  `StudyAnalysisMethodID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `AnalysisMethodID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`StudyAnalysisMethodID`),
  UNIQUE KEY `StudyID` (`StudyID`,`AnalysisMethodID`),
  KEY `AnalysisMethodID` (`AnalysisMethodID`),
  CONSTRAINT `StudyAnalysisMethod_ibfk_1` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `StudyAnalysisMethod_ibfk_2` FOREIGN KEY (`AnalysisMethodID`) REFERENCES `AnalysisMethod` (`AnalysisMethodID`)
) ENGINE=InnoDB AUTO_INCREMENT=3980 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudyCitation`
--

DROP TABLE IF EXISTS `StudyCitation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudyCitation` (
  `StudyCitationID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL default '0',
  `CitationID` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`StudyCitationID`),
  UNIQUE KEY `StudyID` (`StudyID`,`CitationID`),
  KEY `CitationID` (`CitationID`),
  CONSTRAINT `StudyCitation_ibfk_2` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `StudyCitation_ibfk_3` FOREIGN KEY (`CitationID`) REFERENCES `Citation` (`CitationID`)
) ENGINE=InnoDB AUTO_INCREMENT=4849 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudyCrossref_old`
--

DROP TABLE IF EXISTS `StudyCrossref_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudyCrossref_old` (
  `StudyCrossrefID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL default '0',
  `CrossrefID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`StudyCrossrefID`),
  UNIQUE KEY `StudyID` (`StudyID`,`CrossrefID`),
  KEY `CrossrefID` (`CrossrefID`),
  CONSTRAINT `StudyCrossref_old_ibfk_4` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `StudyCrossref_old_ibfk_5` FOREIGN KEY (`CrossrefID`) REFERENCES `Crossref_old` (`CrossrefID`)
) ENGINE=InnoDB AUTO_INCREMENT=2302 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudyHotlink`
--

DROP TABLE IF EXISTS `StudyHotlink`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudyHotlink` (
  `StudyHotlinkID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL default '0',
  `HotlinkID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`StudyHotlinkID`),
  UNIQUE KEY `StudyID` (`StudyID`,`HotlinkID`),
  KEY `HotlinkID` (`HotlinkID`),
  CONSTRAINT `StudyHotlink_ibfk_2` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `StudyHotlink_ibfk_3` FOREIGN KEY (`HotlinkID`) REFERENCES `Hotlink` (`HotlinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=5386 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudyOverlap`
--

DROP TABLE IF EXISTS `StudyOverlap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudyOverlap` (
  `StudyOverlapID` smallint(5) unsigned NOT NULL,
  `StudyID_New` mediumint(8) unsigned NOT NULL COMMENT 'This ID is the current Study ',
  `StudyID_Old` mediumint(8) unsigned NOT NULL COMMENT 'This ID is the overlapping Study',
  `OverlapDetails` varchar(50) NOT NULL default 'This Study overlaps with another',
  PRIMARY KEY  (`StudyOverlapID`),
  UNIQUE KEY `StudyID_New` (`StudyID_New`,`StudyID_Old`),
  KEY `StudyID_Old` (`StudyID_Old`),
  CONSTRAINT `StudyOverlap_ibfk_1` FOREIGN KEY (`StudyID_New`) REFERENCES `Study` (`StudyID`),
  CONSTRAINT `StudyOverlap_ibfk_2` FOREIGN KEY (`StudyID_Old`) REFERENCES `Study` (`StudyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `StudySamplepanel`
--

DROP TABLE IF EXISTS `StudySamplepanel`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `StudySamplepanel` (
  `StudySamplepanel` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `SamplepanelID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`StudySamplepanel`),
  UNIQUE KEY `StudyID` (`StudyID`,`SamplepanelID`),
  KEY `SamplepanelID` (`SamplepanelID`),
  CONSTRAINT `StudySamplepanel_ibfk_1` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE,
  CONSTRAINT `StudySamplepanel_ibfk_2` FOREIGN KEY (`SamplepanelID`) REFERENCES `Samplepanel` (`SamplepanelID`)
) ENGINE=InnoDB AUTO_INCREMENT=7884 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Study_old`
--

DROP TABLE IF EXISTS `Study_old`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Study_old` (
  `StudyID` mediumint(8) unsigned NOT NULL default '0',
  `Identifier` varchar(15) default NULL,
  `Name` varchar(500) default NULL,
  `Label` varchar(10) default NULL,
  `Accession` varchar(15) default NULL,
  `AccessionVersion` varchar(10) default NULL,
  `Description` text,
  `IsHidden` char(5) default NULL,
  `ResearcherID` smallint(5) unsigned default NULL,
  `Abstract` text NOT NULL,
  `Background` text,
  `Objectives` text NOT NULL,
  `KeyResults` text NOT NULL,
  `Conclusions` text NOT NULL,
  `StudyDesign` text,
  `StudySizeReason` text,
  `StudyPower` text,
  `SourcesOfBias` text,
  `Limitations` text,
  `Acknowledgements` text,
  `TimeCreated` timestamp NOT NULL default '0000-00-00 00:00:00',
  `TimeUpdated` timestamp NOT NULL default '0000-00-00 00:00:00',
  `IsDisplayed` char(5) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Submission`
--

DROP TABLE IF EXISTS `Submission`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Submission` (
  `SubmissionID` int(10) unsigned NOT NULL auto_increment,
  `StudyID` mediumint(8) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Description` text,
  `TimeCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`SubmissionID`),
  UNIQUE KEY `StudyID` (`StudyID`,`Name`),
  CONSTRAINT `Submission_ibfk_1` FOREIGN KEY (`StudyID`) REFERENCES `Study` (`StudyID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2388 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Usedmarkerset`
--

DROP TABLE IF EXISTS `Usedmarkerset`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Usedmarkerset` (
  `UsedmarkersetID` int(10) unsigned NOT NULL auto_increment,
  `ExperimentID` int(10) unsigned default NULL,
  `MarkersetID` int(10) unsigned NOT NULL,
  `MarkerIdentifier` varchar(15) default NULL,
  `Name` varchar(100) default NULL,
  PRIMARY KEY  (`UsedmarkersetID`),
  UNIQUE KEY `ExperimentID` (`ExperimentID`,`MarkerIdentifier`),
  KEY `Name` (`Name`),
  KEY `MarkerIdentifier` (`MarkerIdentifier`),
  CONSTRAINT `Usedmarkerset_ibfk_1` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48328043 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-08 14:37:54
