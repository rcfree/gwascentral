create table $mart_db_name.TEMP0 as 
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chrY as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chrX as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr1 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr2 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr3 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr4 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr5 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr6 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr7 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr8 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr9 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr10 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr11 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr12 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr13 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr14 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr15 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr16 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr17 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr18 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr19 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr20 as a where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr21 as a where a.Resultset_Identifier IN  ($resultset_list);
insert into $mart_db_name.TEMP0
select Resultset_Identifier, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession, Marker_Identifier
from $browser_db_name.marker_data_chr22 as a where a.Resultset_Identifier IN  ($resultset_list);
delete from $mart_db_name.study__Association__dm where Resultset_Identifier_key IN ($resultset_list);
create index Marker_Identifier on TEMP0(Marker_Identifier);
---create table TEMP1 as select a.* from TEMP0 as a left join $browser_db_name.all_markers as b on a.Marker_Identifier = b.Identifier;
---drop table TEMP0;
insert into $mart_db_name.study__Association__dm 
select Resultset_Identifier as Resultset_Identifier_key, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession as Accession, Marker_Identifier as MarkerIdentifier
from TEMP0;
drop table TEMP0;