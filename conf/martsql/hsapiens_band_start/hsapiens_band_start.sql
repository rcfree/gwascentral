CREATE TABLE `hsapiens_band_start__band_start__main` (
  `karyotype_key` bigint(23) default NULL,
  `chromosome_id` int(10) unsigned NOT NULL default '0',
  `olook_chr_name` varchar(40) NOT NULL default '',
  `filt_chrom_start` int(10) NOT NULL default '0',
  `filt_chrom_end` int(10) NOT NULL default '0',
  `glook_band_start` varchar(40) NOT NULL default '',
  `glook_band_end` varchar(40) NOT NULL default '',
  KEY `chr_band` (`chromosome_id`,`glook_band_start`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;