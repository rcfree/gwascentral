USE $mart_db_name;
CREATE TABLE `meta_conf__dataset__main` (
  `dataset_id_key` int(11) NOT NULL,
  `dataset` varchar(100) default NULL,
  `display_name` varchar(100) default NULL,
  `description` varchar(200) default NULL,
  `type` varchar(20) default NULL,
  `visible` int(1) unsigned default NULL,
  `version` varchar(25) default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  UNIQUE KEY `dataset_id_key` (`dataset_id_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_conf__interface__dm` (
  `dataset_id_key` int(11) default NULL,
  `interface` varchar(100) default NULL,
  UNIQUE KEY `dataset_id_key` (`dataset_id_key`,`interface`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_conf__user__dm` (
  `dataset_id_key` int(11) default NULL,
  `mart_user` varchar(100) default NULL,
  UNIQUE KEY `dataset_id_key` (`dataset_id_key`,`mart_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_conf__xml__dm` (
  `dataset_id_key` int(11) NOT NULL,
  `xml` longblob,
  `compressed_xml` longblob,
  `message_digest` blob,
  UNIQUE KEY `dataset_id_key` (`dataset_id_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_template__template__main` (
  `dataset_id_key` int(11) NOT NULL,
  `template` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_template__xml__dm` (
  `template` varchar(100) default NULL,
  `compressed_xml` longblob,
  UNIQUE KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `meta_version__version__main` (
  `version` varchar(10) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
