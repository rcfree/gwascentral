create table TEMP0 as
SELECT 
    ExperimentID_key, 
    AutoStudyID, 
    Phenotype, 
    StudyID, 
    AssayedpanelCollectionID, 
    UnadjustedPValue, 
    AnalysisMethodID, 
    SignificanceID_key AS SignificanceID, 
    AssayedpanelID, 
    LocalID_Assayedpanel, 
    GenotypeFrequencyClusterID, 
    NumberOfGenotypedSamples, 
    PValueHWE, 
    AnalysisMethod, 
    FrequencyAsProportion, 
    NumberSamplesWithGenotype, 
    GenotypeFrequencyID, 
    GenotypeID, 
    AutoMarkerID, 
    GenotypeLabel, 
    MarkerCoordID, 
    UniqueID 
FROM 
    study__GenotypeAssociation__dm
where StudyID='<replace_id>';

create index I_0 on TEMP0(ExperimentID_key);

create table TEMP1 as select a.*,b.Type as Type_Experiment, b.TotalMarkersTested as TotalMarkersTested_Experiment, b.Objective as Objective_Experiment, b.Outcome as Outcome_Experiment, b.Title_Experiment
from TEMP0 as a join study__Study__main using (ExperimentID_key);
drop table TEMP0;

rename table TEMP1 to marker_ga__Experiment__dm;
