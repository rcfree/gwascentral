create table HGVmart_test.TEMP0 as select a.Upstream30bp,a.Downstream30bp,a.LocalID,a.MarkerID,a.VariationType,a.AlleleSeqsShorthand,a.Source,a.AutoMarkerID as AutoMarkerID_key from test999_rcf8.Marker as a where Status='active';
create index I_0 on HGVmart_test.TEMP0(AutoMarkerID_key);
create table HGVmart_test.TEMP1 as select a.*,b.Start,b.Stop,b.MarkerCoordID,b.Chr from HGVmart_test.TEMP0 as a inner join test999_rcf8.MarkerCoord as b on a.AutoMarkerID_key=b.AutoMarkerID and (AssemblyName='reference');
drop table HGVmart_test.TEMP0;
create index I_1 on HGVmart_test.TEMP1(AutoMarkerID_key);
create table HGVmart_test.TEMP2 as select a.*,b.CrossrefID,b.MarkerCrossrefID from HGVmart_test.TEMP1 as a left join test999_rcf8.MarkerCrossref as b on a.AutoMarkerID_key=b.AutoMarkerID;
drop table HGVmart_test.TEMP1;
create index I_2 on HGVmart_test.TEMP2(CrossrefID);
create table HGVmart_test.TEMP3 as select a.*,b.UrlID,b.HotlinkID as HotlinkID_Crossref from HGVmart_test.TEMP2 as a left join test999_rcf8.Crossref as b on a.CrossrefID=b.CrossrefID;
drop table HGVmart_test.TEMP2;
create index I_3 on HGVmart_test.TEMP3(HotlinkID_Crossref);
create table HGVmart_test.TEMP4 as select a.*,b.UrlPrefix,b.HotlinkLabel from HGVmart_test.TEMP3 as a left join test999_rcf8.Hotlink as b on a.HotlinkID_Crossref=b.HotlinkID;
drop table HGVmart_test.TEMP3;
create table HGVmart_test.TEMP5 as select Downstream30bp,Upstream30bp,CrossrefID,Stop,LocalID,VariationType,MarkerCrossrefID,AlleleSeqsShorthand,Source,HotlinkLabel,UrlPrefix,UrlID,Start,MarkerCoordID,HotlinkID_Crossref,MarkerID,Chr,AutoMarkerID_key,LEFT(Downstream30bp,10) as Downstream10bp,AlleleSeqsShorthand as AlleleSet,RIGHT(Upstream30bp,10) as Upstream10bp from HGVmart_test.TEMP4;
drop table HGVmart_test.TEMP4;
create index I_4 on HGVmart_test.TEMP5(MarkerID);
create index I_5 on HGVmart_test.TEMP5(Stop);
create index I_6 on HGVmart_test.TEMP5(LocalID);
create index I_7 on HGVmart_test.TEMP5(VariationType);
create index I_8 on HGVmart_test.TEMP5(Start);
create index I_9 on HGVmart_test.TEMP5(MarkerCoordID);
create index I_10 on HGVmart_test.TEMP5(Chr);
alter table HGVmart_test.TEMP5 drop column AlleleSeqsShorthand;
rename table HGVmart_test.TEMP5 to HGVmart_test.marker__Marker__main;
create index I_11 on HGVmart_test.marker__Marker__main(AutoMarkerID_key);
create table HGVmart_test.TEMP6 as select a.AutoMarkerID_key from HGVmart_test.marker__Marker__main as a;
create index I_12 on HGVmart_test.TEMP6(AutoMarkerID_key);
create table HGVmart_test.TEMP7 as select a.*,b.Start,b.Stop,b.MarkerCoordID,b.Chr from HGVmart_test.TEMP6 as a inner join test999_rcf8.MarkerCoord as b on a.AutoMarkerID_key=b.AutoMarkerID and (AssemblyName='reference');
drop table HGVmart_test.TEMP6;
create index I_13 on HGVmart_test.TEMP7(AutoMarkerID_key);
create table HGVmart_test.TEMP8 as select a.AutoMarkerID_key,b.Start,b.MarkerCoordID,b.Stop,b.Chr from HGVmart_test.marker__Marker__main as a left join HGVmart_test.TEMP7 as b on a.AutoMarkerID_key=b.AutoMarkerID_key;
drop table HGVmart_test.TEMP7;
rename table HGVmart_test.TEMP8 to HGVmart_test.marker__MarkerCoord__dm;
create index I_14 on HGVmart_test.marker__MarkerCoord__dm(AutoMarkerID_key);
alter table HGVmart_test.marker__Marker__main add column (MarkerCoord_count integer default 0);
update HGVmart_test.marker__Marker__main a set MarkerCoord_count=(select count(1) from HGVmart_test.marker__MarkerCoord__dm b where a.AutoMarkerID_key=b.AutoMarkerID_key and not (b.Start is null and b.MarkerCoordID is null and b.Stop is null and b.Chr is null));
