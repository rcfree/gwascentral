create table HGVmart_test.TEMP0 as select a.AlleleSeq,a.Upstream30bp,a.Downstream30bp,a.AutoAlleleID as AutoAlleleID_key,a.AutoMarkerID as AutoMarkerID_key,a.AlleleID from HGVbaseG2P_master.Allele as a;
create index I_0 on HGVmart_test.TEMP0(AutoMarkerID_key);
create table HGVmart_test.TEMP1 as select a.*,b.Status as Status_Marker from HGVmart_test.TEMP0 as a left join HGVbaseG2P_master.Marker as b on a.AutoMarkerID_key=b.AutoMarkerID;
drop table HGVmart_test.TEMP0;
create table HGVmart_test.TEMP2 as select AlleleSeq,AutoAlleleID_key,Upstream30bp,Downstream30bp,Status_Marker,AlleleID,AutoMarkerID_key,CONCAT(RIGHT(Upstream30bp,10),"(",AlleleSeq,")", Left(Downstream30bp,10)) as AlleleSeq_full from HGVmart_test.TEMP1;
drop table HGVmart_test.TEMP1;
rename table HGVmart_test.TEMP2 to HGVmart_test.marker_a__Allele__main;
create index I_1 on HGVmart_test.marker_a__Allele__main(AutoAlleleID_key);
