create table HGVmart_test.TEMP0 as select a.AutoMarkerID as AutoMarkerID_key from test999_rcf8.Marker as a;
create index I_0 on HGVmart_test.TEMP0(AutoMarkerID_key);
create table HGVmart_test.TEMP1 as select a.*,b.AlleleSeq,b.AutoAlleleID,b.AlleleID from HGVmart_test.TEMP0 as a left join test999_rcf8.Allele as b on a.AutoMarkerID_key=b.AutoMarkerID and (Status = 'active');
drop table HGVmart_test.TEMP0;
create index I_1 on HGVmart_test.TEMP1(AutoMarkerID_key);
create table HGVmart_test.TEMP2 as select a.*,b.MarkerCoordID from HGVmart_test.TEMP1 as a left join test999_rcf8.MarkerCoord as b on a.AutoMarkerID_key=b.AutoMarkerID and (AssemblyName='reference');
drop table HGVmart_test.TEMP1;
create index I_2 on HGVmart_test.TEMP2(AutoAlleleID);
create table HGVmart_test.TEMP3 as select a.*,b.AlleleFrequencyID,b.AlleleComboID from HGVmart_test.TEMP2 as a left join test999_rcf8.AlleleCombo as b on a.AutoAlleleID=b.AutoAlleleID;
drop table HGVmart_test.TEMP2;
create index I_3 on HGVmart_test.TEMP3(AlleleFrequencyID);
create table HGVmart_test.TEMP4 as select a.*,b.AlleleFrequencyClusterID,b.FrequencyAsProportion from HGVmart_test.TEMP3 as a inner join test999_rcf8.AlleleFrequency as b on a.AlleleFrequencyID=b.AlleleFrequencyID and (b.AlleleFrequencyID IS NOT NULL);
drop table HGVmart_test.TEMP3;
create index I_4 on HGVmart_test.TEMP4(AlleleFrequencyClusterID);
create table HGVmart_test.TEMP5 as select a.*,b.AssayedpanelID,b.UsedmarkersetID,b.NumberOfGenotypedSamples from HGVmart_test.TEMP4 as a left join test999_rcf8.AlleleFrequencyCluster as b on a.AlleleFrequencyClusterID=b.AlleleFrequencyClusterID;
drop table HGVmart_test.TEMP4;
create index I_5 on HGVmart_test.TEMP5(AssayedpanelID);
create table HGVmart_test.TEMP6 as select a.*,b.TotalNumberOfIndividuals,b.LocalID as LocalID_Assayedpanel,b.NumberOfSexFemale,b.NumberOfSexUnknown,b.NumberOfSexMale,b.AutoStudyID from HGVmart_test.TEMP5 as a left join test999_rcf8.Assayedpanel as b on a.AssayedpanelID=b.AssayedpanelID;
drop table HGVmart_test.TEMP5;
create table HGVmart_test.TEMP7 as select AlleleSeq,TotalNumberOfIndividuals,AssayedpanelID,NumberOfSexFemale,LocalID_Assayedpanel,FrequencyAsProportion,NumberOfGenotypedSamples,NumberOfSexMale,AlleleComboID,AutoStudyID,AlleleFrequencyClusterID,AlleleFrequencyID,MarkerCoordID,AutoAlleleID,NumberOfSexUnknown,UsedmarkersetID,AlleleID,AutoMarkerID_key,'abcdefghijklmnopqrstuvwxyz' as AlleleSeq_full from HGVmart_test.TEMP6;
drop table HGVmart_test.TEMP6;
create index I_6 on HGVmart_test.TEMP7(MarkerCoordID);
rename table HGVmart_test.TEMP7 to HGVmart_test.marker_af__AlleleFrequency__main;
create index I_7 on HGVmart_test.marker_af__AlleleFrequency__main(AutoMarkerID_key);
create table HGVmart_test.TEMP8 as select a.AutoAlleleID,a.AutoMarkerID_key from HGVmart_test.marker_af__AlleleFrequency__main as a;
create index I_8 on HGVmart_test.TEMP8(AutoAlleleID);
create table HGVmart_test.TEMP9 as select a.*,b.AlleleFrequencyID,b.AlleleComboID from HGVmart_test.TEMP8 as a left join test999_rcf8.AlleleCombo as b on a.AutoAlleleID=b.AutoAlleleID;
drop table HGVmart_test.TEMP8;
create index I_9 on HGVmart_test.TEMP9(AlleleFrequencyID);
create table HGVmart_test.TEMP10 as select a.*,b.AlleleFrequencyClusterID,b.FrequencyAsProportion from HGVmart_test.TEMP9 as a left join test999_rcf8.AlleleFrequency as b on a.AlleleFrequencyID=b.AlleleFrequencyID;
drop table HGVmart_test.TEMP9;
create index I_10 on HGVmart_test.TEMP10(AlleleFrequencyClusterID);
create table HGVmart_test.TEMP11 as select a.*,b.AssayedpanelID,b.UsedmarkersetID,b.NumberOfGenotypedSamples from HGVmart_test.TEMP10 as a left join test999_rcf8.AlleleFrequencyCluster as b on a.AlleleFrequencyClusterID=b.AlleleFrequencyClusterID;
drop table HGVmart_test.TEMP10;
create index I_11 on HGVmart_test.TEMP11(UsedmarkersetID);
create table HGVmart_test.TEMP12 as select a.*,b.LocalID,b.ExperimentID,b.AutoMarkersetID from HGVmart_test.TEMP11 as a left join test999_rcf8.Usedmarkerset as b on a.UsedmarkersetID=b.UsedmarkersetID;
drop table HGVmart_test.TEMP11;
create index I_12 on HGVmart_test.TEMP12(AssayedpanelID);
create table HGVmart_test.TEMP13 as select a.*,b.TotalNumberOfIndividuals,b.NumberOfProbands,b.NumberOfSexFemale,b.Description,b.NumberOfSexUnknown,b.NumberOfSexMale,b.NumberOfParents,b.AutoStudyID from HGVmart_test.TEMP12 as a left join test999_rcf8.Assayedpanel as b on a.AssayedpanelID=b.AssayedpanelID;
drop table HGVmart_test.TEMP12;
create index I_13 on HGVmart_test.TEMP13(AutoStudyID);
create table HGVmart_test.TEMP14 as select a.*,b.StudyPower,b.TimeCreated,b.Background,b.ResearcherID,b.SourcesOfBias,b.Abstract,b.StudyDesign,b.Limitations,b.StudyID,b.StudySizeReason,b.TimeUpdated,b.Conclusions,b.KeyResults,b.Objectives,b.Title,b.Acknowledgements from HGVmart_test.TEMP13 as a left join test999_rcf8.Study as b on a.AutoStudyID=b.AutoStudyID;
drop table HGVmart_test.TEMP13;
rename table HGVmart_test.TEMP14 to HGVmart_test.marker_af__AlleleCombo__dm;
create index I_14 on HGVmart_test.marker_af__AlleleCombo__dm(AutoMarkerID_key);
alter table HGVmart_test.marker_af__AlleleFrequency__main add column (AlleleCombo_count integer default 0);
update HGVmart_test.marker_af__AlleleFrequency__main a set AlleleCombo_count=(select count(1) from HGVmart_test.marker_af__AlleleCombo__dm b where a.AutoMarkerID_key=b.AutoMarkerID_key and not (b.NumberOfProbands is null and b.StudyPower is null and b.NumberOfSexFemale is null and b.FrequencyAsProportion is null and b.Abstract is null and b.NumberOfGenotypedSamples is null and b.NumberOfSexMale is null and b.AutoStudyID is null and b.Limitations is null and b.AlleleFrequencyID is null and b.Description is null and b.Conclusions is null and b.AutoAlleleID is null and b.Acknowledgements is null and b.KeyResults is null and b.TotalNumberOfIndividuals is null and b.AssayedpanelID is null and b.LocalID is null and b.TimeCreated is null and b.Background is null and b.ResearcherID is null and b.SourcesOfBias is null and b.StudyDesign is null and b.ExperimentID is null and b.AlleleComboID is null and b.NumberOfParents is null and b.StudyID is null and b.AlleleFrequencyClusterID is null and b.StudySizeReason is null and b.TimeUpdated is null and b.NumberOfSexUnknown is null and b.Title is null and b.Objectives is null and b.UsedmarkersetID is null));
create table HGVmart_test.TEMP15 as select a.AutoMarkerID_key from HGVmart_test.marker_af__AlleleFrequency__main as a;
create index I_15 on HGVmart_test.TEMP15(AutoMarkerID_key);
create table HGVmart_test.TEMP16 as select a.*,b.Status,b.AlleleSeq,b.Upstream30bp,b.Downstream30bp,b.SeqType,b.AutoAlleleID,b.AlleleSeqDigest,b.AlleleID from HGVmart_test.TEMP15 as a left join test999_rcf8.Allele as b on a.AutoMarkerID_key=b.AutoMarkerID;
drop table HGVmart_test.TEMP15;
rename table HGVmart_test.TEMP16 to HGVmart_test.marker_af__Allele__dm;
create index I_16 on HGVmart_test.marker_af__Allele__dm(AutoMarkerID_key);
alter table HGVmart_test.marker_af__AlleleFrequency__main add column (Allele_count integer default 0);
update HGVmart_test.marker_af__AlleleFrequency__main a set Allele_count=(select count(1) from HGVmart_test.marker_af__Allele__dm b where a.AutoMarkerID_key=b.AutoMarkerID_key and not (b.AlleleSeq is null and b.Status is null and b.Downstream30bp is null and b.Upstream30bp is null and b.SeqType is null and b.AutoAlleleID is null and b.AlleleSeqDigest is null and b.AlleleID is null));
