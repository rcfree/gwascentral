drop table if exists HGVmart_test.study__GenotypeAssociation__dm;

create table HGVmart_test.TEMP0 as select a.AutoPhenotypeMethodID,a.ExperimentID as ExperimentID_key,a.AutoStudyID from test999_rcf8.Experiment as a;
create index I_0 on TEMP0(AutoPhenotypeMethodID);
create index I_1 on TEMP0(ExperimentID_key);
create index I_2 on TEMP0(AutoStudyID);

create table HGVmart_test.TEMP1 as select a.*, b.MethodName as Phenotype
from TEMP0 as a inner join test999_rcf8.PhenotypeMethod as b using(AutoPhenotypeMethodID);
drop table HGVmart_test.TEMP0;

create table HGVmart_test.TEMP2 as select a.*,b.StudyID 
from TEMP1 as a inner join test999_rcf8.Study as b on a.AutoStudyID=b.AutoStudyID and (StudyID='<replace_id>');
drop table HGVmart_test.TEMP1;

create table HGVmart_test.TEMP3 as select a.*, b.AssayedpanelCollectionID
from TEMP2 as a join test999_rcf8.AssayedpanelCollection as b on a.ExperimentID_key = b.ExperimentID;
drop table HGVmart_test.TEMP2;
create index I_3 on TEMP3(AssayedpanelCollectionID);

create table HGVmart_test.TEMP4 as select a.*, b.UsedmarkersetID, b.UnadjustedPValue, b.AnalysisMethodID, b.SignificanceID as SignificanceID_key 
from TEMP3 as a join test999_rcf8.Significance as b using(AssayedpanelCollectionID);
drop table HGVmart_test.TEMP3;
create index I_4 on TEMP4(AnalysisMethodID);
create index I_5 on TEMP4(SignificanceID_key);
create index I_5B on TEMP4(UsedmarkersetID);

create table HGVmart_test.TEMP4A as select a.*, b.AutoMarkersetID
from TEMP4 as a join test999_rcf8.Usedmarkerset as b using(UsedmarkersetID);
drop table HGVmart_test.TEMP4;
create index I_5C on TEMP4A(AutoMarkersetID);

create table HGVmart_test.TEMP4B as select a.*, b.AutoMarkerID
from TEMP4A as a join test999_rcf8.MarkersetMarker as b using(UsedmarkersetID);
drop table HGVmart_test.TEMP4A;
create index I_5D on TEMP4B(AutoMarkerID);

create table HGVmart_test.TEMP4C as select a.*, b.MarkerID, b.LocalID, b.AlleleSeqsShorthand AS AlleleSet, b.Upstream30bp, b.Downstream30bp, b.VariationType, b.Source
from TEMP4B as a left join test999_rcf8.Marker as b on a.AutoMarkerID=b.AutoMarkerID left join test999_rcf8.MarkerCoord as c on a.AutoMarkerID=c.AutoMarkerID;
drop table HGVmart_test.TEMP4B;
create index I_5E on TEMP4C(AutoMarkerID);

create table HGVmart_test.TEMP4D as select a.*, b.Chr, b.Start, b.Stop, b.MarkerCoordID
from TEMP4C as a left join test999_rcf8.MarkerCoord as b using(AutoMarkerID);
drop table HGVmart_test.TEMP4C

create table HGVmart_test.TEMP4E as select a.*, RIGHT(b.Upstream10bp) AS Upstream10bp, LEFT(b.Downstream10bp) AS Downstream10bp, IF(a.MarkerCoordID IS NULL, CONCAT(a.SignificanceID_key,"M",a.AutoMarkerID), CONCAT(a.SignificanceID_key,"C",a.MarkerCoordID)) AS UniqueID
from TEMP4D as a;
drop table HGVmart_test.TEMP4D;

create table HGVmart_test.TEMP5 as select a.*, b.AssayedpanelID
from TEMP4E as a join test999_rcf8.APAPC as b using(AssayedpanelCollectionID);
drop table HGVmart_test.TEMP4E;
create index I_6 on TEMP5(AssayedpanelID);

create table HGVmart_test.TEMP6 as select a.*, b.LocalID as LocalID_Assayedpanel
from TEMP5 as a join test999_rcf8.Assayedpanel as b using(AssayedpanelID);
drop table HGVmart_test.TEMP5;

create table HGVmart_test.TEMP7 as select a.*, b.GenotypeFrequencyClusterID
from TEMP6 as a inner join test999_rcf8.GFCS as b on a.SignificanceID_key = b.SignificanceID;
drop table HGVmart_test.TEMP6;
create index I_7 on TEMP7(GenotypeFrequencyClusterID);

create table HGVmart_test.TEMP8 as select a.*, b.NumberOfGenotypedSamples, b.PValueHWE
from TEMP7 as a inner join test999_rcf8.GenotypeFrequencyCluster as b on a.AssayedpanelID=b.AssayedpanelID and a.GenotypeFrequencyClusterID=b.GenotypeFrequencyClusterID;
drop table HGVmart_test.TEMP7;

create table HGVmart_test.TEMP9 as select a.*, b.MethodName as AnalysisMethod
from TEMP8 as a left join test999_rcf8.AnalysisMethod as b using (AnalysisMethodID);
drop table HGVmart_test.TEMP8;

create table HGVmart_test.TEMP10 as select a.*, b.FrequencyAsProportion, b.NumberSamplesWithGenotype, b.GenotypeFrequencyID
from TEMP9 as a left join test999_rcf8.GenotypeFrequency as b using(GenotypeFrequencyClusterID);
drop table HGVmart_test.TEMP9;
create index I_8 on TEMP10(GenotypeFrequencyID);

create table HGVmart_test.TEMP11 as select a.*, b.GenotypeID
from TEMP10 as a left join test999_rcf8.GenotypeCombo as b using(GenotypeFrequencyID);
drop table HGVmart_test.TEMP10;
create index I_9 on TEMP11(GenotypeID);

create table HGVmart_test.TEMP12 as select a.*, b.AutoMarkerID, b.GenotypeLabel
from TEMP11 as a left join test999_rcf8.Genotype as b using(GenotypeID);
drop table HGVmart_test.TEMP11;

rename table TEMP12 to HGVmart_test.study_ga__Experiment__main;
