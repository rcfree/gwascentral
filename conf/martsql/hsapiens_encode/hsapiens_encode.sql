CREATE TABLE `hsapiens_encode__encode__main` (
  `encode_key` bigint(23) default NULL,
  `glook_encode_region_name` text NOT NULL,
  `filt_chr_name` varchar(40) NOT NULL default '',
  `filt_chrom_start` int(10) unsigned NOT NULL default '0',
  `filt_chrom_end` int(10) unsigned NOT NULL default '0',
  `glook_encode_region` varbinary(64) NOT NULL default '',
  `misc_feature_id` int(10) unsigned NOT NULL default '0',
  `silent_type` varchar(12) NOT NULL default '',
  `encode_description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;