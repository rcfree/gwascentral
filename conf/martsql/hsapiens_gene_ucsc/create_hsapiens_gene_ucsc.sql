CREATE TABLE `hsapiens_gene_ucsc__gene__main` (
  `gene_id_key` int(11) NOT NULL,
  `ref` varchar(6) default NULL,
  `hgnc_label` varchar(40) default NULL,
  `start` int(11) default NULL,
  `stop` int(11) default NULL,
  KEY `hgnc_label` (`hgnc_label`),
  KEY `region` (`ref`,`start`,`stop`),
  KEY `gene_id_key` (`gene_id_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `hsapiens_gene_ucsc__feature__dm` (
  `gene_id_key` int(11) NOT NULL,
  `ref` varchar(6) default NULL,
  `start` int(11) default NULL,
  `stop` int(11) default NULL,
  `feature` varchar(11) default NULL,
  KEY `gene_id_key` (`gene_id_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;