create table HGVmart_test.TEMP0 as select a.GenotypeID as GenotypeID_key,a.AutoMarkerID as AutoMarkerID_key,a.GenotypeLabel from HGVbaseG2P_master.Genotype as a;
create index I_0 on HGVmart_test.TEMP0(AutoMarkerID_key);
create table HGVmart_test.TEMP1 as select a.*,b.Upstream30bp,b.Downstream30bp from HGVmart_test.TEMP0 as a left join HGVbaseG2P_master.Marker as b on a.AutoMarkerID_key=b.AutoMarkerID;
drop table HGVmart_test.TEMP0;
create table HGVmart_test.TEMP2 as select GenotypeID_key,Upstream30bp,Downstream30bp,GenotypeLabel,AutoMarkerID_key,CONCAT(RIGHT(Upstream30bp,10),"[",GenotypeLabel,"]", Left(Downstream30bp,10)) as GenotypeLabel_full from HGVmart_test.TEMP1;
drop table HGVmart_test.TEMP1;
rename table HGVmart_test.TEMP2 to HGVmart_test.marker_g__Genotype__main;
create index I_1 on HGVmart_test.marker_g__Genotype__main(GenotypeID_key);
