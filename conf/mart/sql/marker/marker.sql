drop table if exists $mart_db_name.marker__Marker__main;
--- create table $mart_db_name.TEMP0 as 
--- select a.Chr, a.Start, a.Stop, a.VariationType, a.Accession, a.Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, a.CurrentIdentifier
--- from $browser_db_name.all_markers as a;
create table $mart_db_name.TEMP1 as
select a.*, b.Gene_Section, b.Gene_Symbol
from $browser_db_name.all_markers as a left join $browser_db_name.marker_annotation as b on a.Identifier = b.Marker_Identifier;
drop table TEMP0;
rename table TEMP1 to marker__Marker__main;
alter table marker__Marker__main
 add KEY `Region` (`Chr`,`Start`,`Stop`),
 add KEY `Accession` (`Accession`),
 add KEY `Identifier` (`Identifier`),
 add KEY `Gene` (`Gene_Symbol`,`Gene_Section`);