drop table if exists $mart_db_name.study__Association__dm;
create table $mart_db_name.TEMP0 as select a.PhenotypeMethodID,a.ExperimentID as ExperimentID_key,a.StudyID from $study_db_name.Experiment as a;
create index I_0 on TEMP0(PhenotypeMethodID);
create index I_1 on TEMP0(ExperimentID_key);
create index I_2 on TEMP0(StudyID);
create table $mart_db_name.TEMP1 as select a.*, b.Name as Phenotype
from TEMP0 as a inner join $study_db_name.PhenotypeMethod as b using(PhenotypeMethodID);
drop table $mart_db_name.TEMP0;
create table $mart_db_name.TEMP2 as select a.*,b.Identifier
from TEMP1 as a inner join $study_db_name.Study as b on a.StudyID=b.StudyID and Identifier IN ('HGVST200','HGVST201','HGVST202','HGVST203','HGVST204','HGVST205','HGVST206','HGVST207','HGVST208','HGVST209','HGVST210','HGVST211','HGVST212');
drop table $mart_db_name.TEMP1;
create table $mart_db_name.TEMP3 as select a.*, b.AssayedpanelID
from TEMP2 as a join $study_db_name.ExperimentAssayedpanel as b on a.ExperimentID_key = b.ExperimentID;
drop table $mart_db_name.TEMP2;
create table $mart_db_name.TEMP4 as select a.*, b.Name, b.ResultsetID, b.Name as Resultset
from TEMP3 as a left join $study_db_name.Resultset as b on a.ExperimentID_key = b.ExperimentID;
create index I_3 on TEMP4(ResultsetID);
drop table $mart_db_name.TEMP3;
create table $mart_db_name.TEMP5 as select a.*, b.UnadjustedPValue, b.SignificanceID as SignificanceID_key, b.UsedmarkersetID
from TEMP4 as a join $study_db_name.Significance as b using(ResultsetID);
create index I_3 on TEMP5(SignificanceID_key);
drop table $mart_db_name.TEMP4;
create table $mart_db_name.TEMP6 as select a.*, b.MarkerIdentifier
from TEMP5 as a join $study_db_name.Usedmarkerset as b using(UsedmarkersetID);
drop table $mart_db_name.TEMP5;
create table $mart_db_name.TEMP7 as select a.*, b.MarkerID, b.Accession, b.AlleleSeqsShorthand AS AlleleSet, b.Upstream30bp, b.Downstream30bp, RIGHT(b.Upstream30bp,10) AS Upstream10bp, LEFT(b.Downstream30bp,10) AS Downstream10bp, b.VariationType, b.DataSourceID
from TEMP6 as a left join $marker_db_name.Marker as b on a.MarkerIdentifier=b.Identifier;
drop table $mart_db_name.TEMP6;
create table $mart_db_name.TEMP8 as select a.*, b.Chr, b.Start, b.Stop, b.MarkerCoordID
from TEMP7 as a left join $marker_db_name.MarkerCoord as b on a.MarkerID=b.MarkerID and (AssemblyName='reference');
drop table $mart_db_name.TEMP7;
rename table TEMP8 to $mart_db_name.study__Association__dm;