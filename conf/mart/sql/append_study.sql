insert into study__AlleleFrequency__dm select * from study_af__Experiment__main;
insert into study__GenotypeFrequency__dm select * from study_gf__Experiment__main;
insert into study__AlleleAssociation__dm select * from study_aa__Experiment__main;
insert into study__GenotypeAssociation__dm select * from study_ga__Experiment__main;

drop table if exists study_af__Experiment__main;
drop table if exists study_gf__Experiment__main;
drop table if exists study_aa__Experiment__main;
drop table if exists study_ga__Experiment__main;
