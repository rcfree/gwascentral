create table $mart_db_name.TEMP0 as 
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chrY as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chrX as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr1 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr2 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles,  b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr3 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles,  b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr4 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr5 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr6 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr7 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr8 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr9 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr10 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr11 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr12 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr13 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr14 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr15 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr16 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr17 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr18 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr19 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr20 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr21 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
insert into $mart_db_name.TEMP0
select a.Resultset_Identifier, a.Study_Identifier, a.Chr, a.Start, a.Stop, a.VariationType, a.Strand, a.Unadjustedpvalue, a.NegLogPValue, a.Marker_Accession, a.Marker_Identifier, a.Upstream30bp, a.Downstream30bp, a.Alleles, b.Gene_Section, b.Gene_Symbol, a.UsedmarkersetID, a.RiskAllele, a.RiskAlleleFreq, a.RiskAlleleComment, a.ES_Type, a.ES_Value, a.ES_Lower95Bound, a.ES_Upper95Bound, a.ES_StdError
 
from $browser_db_name.marker_data_chr22 as a left join $browser_db_name.marker_annotation b on a.Marker_Identifier = b.Marker_Identifier where a.Resultset_Identifier IN ($resultset_list);
delete from $mart_db_name.study__Association__dm where Resultset_Identifier_key IN ($resultset_list);
create index Marker_Identifier on TEMP0(Marker_Identifier);
---create table TEMP1 as select a.* from TEMP0 as a left join $browser_db_name.all_markers as b on a.Marker_Identifier = b.Identifier;
---drop table TEMP0;
insert into $mart_db_name.study__Association__dm 
select Resultset_Identifier as Resultset_Identifier_key, Study_Identifier, Chr, Start, Stop, VariationType, Strand, Unadjustedpvalue, NegLogPValue, Marker_Accession as Accession, Marker_Identifier, Upstream30bp, Downstream30bp, Alleles, Gene_Section, Gene_Symbol, UsedmarkersetID, RiskAllele, RiskAlleleFreq, RiskAlleleComment, ES_Type, ES_Value, ES_Lower95Bound, ES_Upper95Bound, ES_StdError
from TEMP0;
drop table TEMP0;