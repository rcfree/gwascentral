# $Id: Samplepanel.pm 1469 2010-06-15 16:39:00Z rcf8 $

=head1 NAME

  HGVbaseG2P::Web::Controller::Samplepanel - Catalyst Controller for Samplepanels

=head1 DESCRIPTION

  This class handles retrieving data, transforming and business logic relating to 
  Samplepanel entries in the database. 

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Samplepanel;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

=head2 view

Redirects from /view to default action

=cut

sub view : Local {
    my ( $self, $c ) = @_;
    my $url = $c->req->uri->as_string;
    $url =~ s|(\w+)/view|$1|;
    $c->response->redirect($url);
}

=head2 default

Retrieves a samplepanel from the database (by identifier) and puts it in the stash for the template

=cut

sub default :Regex('^samplepanel/(HGV\w+)$') {
    my ( $self, $c ) = @_;
    my $id = $c->req->captures->[0];

    my $spanel = $c->model("StudyDB")->get_samplepanel_by_identifier($id);
    my $study = $c->model("StudyDB")->get_study_by_identifier($c->req->param("fromStudy"));
  
    $c->stash->{spanelid} = $id;
    $c->stash->{study}=$study;
    $c->stash->{template} = 'samplepanel/view.tt2';
    $c->stash->{spanel} = $spanel;
    return; 
}


=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>
  Rob Free <rcfree@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
