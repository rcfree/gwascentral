# $Id: Dialog.pm 1524 2010-08-23 09:05:57Z rcf8 $

=head1 NAME

  GwasCentral::Web::Controller::Dialog - Catalyst Controller for Dialogs displayed (usually AJAX)

=head1 DESCRIPTION

TODO

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Dialog;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

use Data::Dumper qw(Dumper);
use List::MoreUtils qw(any uniq);
use List::Compare;
use GwasCentral::Search::Query;
use GwasCentral::Tool::PubmedSearch;
use XML::Simple qw(XMLout);
use HTML::Entities;

my $home = $ENV{HGVBASEG2PWEB_HOME};

=head2 default
Action to deal with browser default. Sets up the stash with session variables and collated hashrefs for studies and resultsets

=cut

sub transcripts :Path('dialog/transcripts') {
	my ($self, $c) = @_;
	my $feature = $c->req->param('feature');
	
	my $qry = GwasCentral::Search::Query->new({conf_file => $c->config});
	$qry->feature_db($c->model('SeqFeature'));
	my @feats = $qry->get_feature_regions($feature);
	$c->stash->{features}=\@feats;
	$c->stash->{template}='dialog/transcripts.tt2';
}

#add study

sub add_study :Regex('^dialog/add_study/(HGVST\w+)$') {
    my ( $self, $c ) = @_;
	my $id = $c->req->captures->[0];
	
    # Retrieve case-control experiments for study from the database
    my $study = $c->model("Study")->get_study_by_identifier($id);
    #my @experiments = $c->model("StudyDB")->get_assoc_experiments_from_study_ident($id);
   # my %exp_levels = $acontrol->get_experiment_access_levels([$experiments->all]);
  	
#  	if (scalar(@experiments)==1) {
#  		if (scalar(@{get_field($experiments[0],"resultsets")})==1) {
#  			$c->stash->{single}=1;
#  		}
#  	}
	my @exp_data = ();
  	foreach my $exp($study->experiments) {
  		my @rsets = ();
  		foreach my $rs($exp->resultsets) {
  			my $rset_data = {$rs->get_columns};
  			$rset_data->{'id'}="gwascentral.resultset.".$rs->identifier;
  			push @rsets, $rset_data;
  		}
  		my $exp_data = {$exp->get_columns};
  		$exp_data->{'resultsets'}=\@rsets;
  		push @exp_data,$exp_data;
  	}
  	$c->stash->{entity_identifier}=$id;
    $c->stash->{entity_name}='study';
    $c->stash->{experiments}=\@exp_data;
#    $c->stash->{exp_levels}=\%exp_levels;
    $c->stash->{template}="dialog/add_entity.tt2";
}

#add phenotype
sub render : ActionClass('RenderView') { }

#sub end : Private { 
#   my ( $self, $c ) = @_;
#   $c->forward('render');
#  	if ($c->req->param('callback')) {
#	  	my $output = $c->res->output;
#	  	$output=~s/\n//g;
#	  	$output=~s/"/\\"/g;
#	  	$output = $c->req->param('callback')."({\"output\":\"".$output."\"})";
#	  	$c->res->output($output);
#  	}
#}

sub add_phenotype :Regex('^dialog/add_phenotype/(HGV\w+)$') {
    my ( $self, $c ) = @_;
	my $id = $c->req->captures->[0];
	
    # Retrieve experiments with phenotype method from the database
    my $pmethod = $c->model("Study")->get_pmethod_by_identifier($id);
    
    my @exp_data = ();
  	foreach my $exp($pmethod->experiments) {
  		my @rsets = ();
  		foreach my $rs($exp->resultsets) {
  			my $rset_data = {$rs->get_columns};
  			$rset_data->{'id'}="gwascentral.resultset.".$rs->identifier;
  			push @rsets, $rset_data;
  		}
  		my $exp_data = {$exp->get_columns};
  		$exp_data->{'resultsets'}=\@rsets;
  		push @exp_data,$exp_data;
  	}
  	
    $c->stash->{experiments}=\@exp_data;
    $c->stash->{entity_identifier}=$id;
    $c->stash->{entity_name}='phenotype';
    $c->stash->{template}="dialog/add_entity.tt2";
    return;
}

#view experiment

#set limited markers

sub select_markers : Path('dialog/select_markers') {
	my ( $self, $c ) = @_;
	$c->stash->{template} = 'dialog/select_markers.tt2';
}

sub get_article :Path('dialog/get_article') {
	my ($self, $c) = @_;
	my $pmsearch = GwasCentral::Tool::PubmedSearch->new({conf_file => $c->config });
	my $pmid = $c->req->param('pubmedid');
	my $article = $pmsearch->pubmed_search($pmid);
	$c->log->info("article:".Dumper($article));
	$c->res->content_type('text/xml');
	$c->res->body(XMLout($article, NoAttr=>1, RootName=>'article'));
}

sub add_permissions :Path('dialog/add_permissions') {
	my ($self, $c) = @_;
	
	my $acontrol = $c->get_access_control();
	my $users = $acontrol->gas->get_all_users;
	my $study_ident = $c->req->param('study_ident');
	$c->log->info("users:".Dumper($users));
	$c->stash->{study} = $c->model('StudyDB')->get_study_by_identifier($study_ident);
	$c->stash->{users}=$users;
	$c->stash->{template} = 'dialog/add_permissions.tt2';
}

1;