# $Id: Admin.pm 1458 2010-06-04 11:08:34Z rcf8 $

package GwasCentral::Web::Controller::Admin;

=head1 NAME

  GwasCentral::Web::Controller::Admin - Catalyst Controller for Admin options

=head1 DESCRIPTION

  This class handles admin pages

=head1 METHODS

=cut

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Data::Dumper qw(Dumper);
my $home = $ENV{HGVBASEG2PWEB_HOME};
use Digest::MD5 qw(md5_hex);
use GwasCentral::Browser::Creator;
use GwasCentral::Web::Update;

=head2 default
Action to deal with browser default. Sets up the stash with session variables and collated hashrefs for studies and resultsets

=cut

sub default :Path('admin') {
	my ($self, $c) = @_;
	$c->forward('admin_studies');
}

sub admin_study : Path('admin/study/(.+)') {
	my ($self, $c) = @_;
	$c->stash->{admin_template}='study';
}

sub admin_studies : Path('admin/studies') {
	my ( $self, $c ) = @_;
	$c->stash->{template} = 'admin.tt2';
	$c->stash->{admin_template}='studies';
	
	#my $ac = $c->get_AccessControl();
	
	eval {
		$self->do_search($c,'AdminStudies');
	};
	if ($@) {
		$c->log->error("AdminStudies error:$@");
		if ($c->req->param('jsonp')) {
	   		$self->output_jsonp($c,{aaData=>[["Unknown problem with AdminStudies list:".$@,"","","","","",""]], iTotalRecords => 1, iTotalDisplayRecords => 1});
	   		return;
	   	}
	   	else {
	   		$c->stash->{error} = $@;
			$c->res->status(500);
	   	}
	}
#	my $page = $c->req->param('page') || 1;
#	my $page_size = $c->req->param('page_size') || 20;
#	my @study_idents = $c->req->param('study');
#	$c->log->info("study_idents:".Dumper(\@study_idents));
#	if (scalar(@study_idents)==1 and $study_idents[0] eq "") {
#		@study_idents = ();
#	}
#	
#	if ( !$c->is_user_logged_in ) {
#		$c->stash->{error} =
#		  'You must be registered and logged-in to access this page.';
#		return;
#	}
#	
#	if ( !$acontrol->is_current_user_admin_of_any_studies ) {
#		$c->stash->{error} =
#		  'You must be an admin user to access this page.';
#		return;
#	}
#	
#	#my $requests = $acontrol->get_pending_requests;
#    
##    my ($studies,$study_levels, $exp_levels, $pager) = $acontrol->get_users_studies_with_access_levels($page, $page_size, \@study_idents);
##
##	$c->stash->{s} = {
##				query        => { recognised_as => 'none', no_filters => scalar(@study_idents)>0 ? undef : 1 },
##				dbconfig     => $c->config->{'Search'}->{'Query'}->{'AdminStudies'},
##				query => {pager=>$pager, allowed_page_sizes=>[10,20,50,100]}
##			};
#			
#	$c->stash->{studies} = $studies;
#	$c->stash->{stu_access_levels}=$study_levels;
#	$c->stash->{exp_access_levels}=$exp_levels;
#	$c->stash->{available_groups}=[];
#	$c->stash->{requests} = $requests;
}

sub store :Path('admin/store') {
	my ($self, $c) = @_;
	my $acontrol = $c->get_AccessControl;
	
	if ( !$c->is_user_logged_in ) {
		$c->res->status(200);
		$c->res->output('You must be registered and logged-in to access this page.');
		return;
	}
	
	$acontrol->store_levels($c->req->params);
	
	$c->res->output('success');
}

sub grant :Path('admin/grant') {
	my ($self, $c) = @_;
	my $acontrol = $c->get_access_control;
	
	eval {
		my $request = $acontrol->grant_request($c->req->params);
	};
	if ($@) {
		$c->stash->{error} = $@->message;
	}
	$c->res->redirect('/admin/requests');
}

sub limited :Path('admin/limited') {
	my ($self, $c) = @_;
	my $acontrol = $c->get_access_control;
	
	if ( !$c->is_user_logged_in  ) {
		$c->stash->{error} =
		  'You must be registered and logged-in to access this page.';
		return;
	}
	
	my $exp = $c->req->param('experiment');
	my $top_no = $c->req->param('top_no');
	
	my $update = $c->get_update;
	$update->recreate_limited_data_for_experiment($exp, $top_no );
	
	$c->stash->{template}="admin/limited.tt2";
}

sub requests : Path('admin/requests') {
	my ( $self, $c ) = @_;
	$c->stash->{template} = 'admin.tt2';
	$c->stash->{admin_template}='requests';
	
	my $acontrol = $c->get_access_control();

	if ( !$c->is_user_logged_in ) {
		$c->stash->{error} =
		  'You must be registered and logged-in to access this page.';
		return;
	}
	
	if ( !$acontrol->is_user_admin ) {
		$c->stash->{error} =
		  'You must be an admin user to access this page.';
		return;
	}

	my $requests = $acontrol->get_pending_requests_with_users($c->req->param('study'));
	$c->stash->{reject_reasons} = $c->config->{AccessControl}->{reject_reasons};
    $c->stash->{requests}=$requests;
}

sub users : Path('admin/users') {
	my ($self, $c) = @_;
	my $study = $c->req->param('study');
	my $add_user = $c->req->param('add_user');
	my $uri = $c->config->{WebSite}->{gas_baseuri};
	$c->stash->{template}='admin/users.tt2';
	
	my $acontrol = $c->get_access_control();
	
	eval {
		if (!$acontrol->is_user_study_admin($study)) {
			$c->stash->{error}="Sorry you do not have 'admin' permissions for this study";
			return;
		}
	};
	if ($@) {
		$c->stash->{error}="An error occurred:$@";
		return;
	}
	
#	if ($add_user) {
#		eval {
#			my $user = $acontrol->gas->get_user_by_id($add_user);
#			$acontrol->gas->set_access_level($uri."/study/$study", "none", $user->{identity});
#			$c->stash->{info}="User '".$user->{nickname}."' given access to study '$study'";
#		};
#		if ($@) { 
#			$c->stash->{error}=$@;
#			return;
#		}
#	}
	
	if ( !$c->is_user_logged_in ) {
		$c->stash->{error} =
		  'You must be registered and logged-in to access this page.';
		return;
	}
	$c->stash->{admin_user} = $acontrol->user;
	$c->log->info("admin_user:".Dumper($acontrol->user));
	my ($user_spec_perms,$dc_study, $users) = $acontrol->get_study_permissions($study);
	$c->log->info("user_spec_perms:".Dumper($user_spec_perms));
	$c->log->info("users:".Dumper($users));
	$c->stash->{users}=$users;
	$c->stash->{perms}=$user_spec_perms;
	$c->stash->{study}=$dc_study;
}

sub reject : Path('admin/reject') {
	my ( $self, $c ) = @_;
	my $request_id = $c->req->param('request');
	$c->stash->{template} = 'admin/message.tt2';
	my $acontrol = $c->get_access_control;

	eval {
		my $request = $acontrol->reject_request($c->req->params);
	};
	if ($@) {
		$c->stash->{error} = $@->message;
	}
	$c->res->redirect('/admin/requests');
}

1;
