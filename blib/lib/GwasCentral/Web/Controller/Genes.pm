# $Id: Study.pm 1537 2010-09-21 10:56:08Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Study - Catalyst Controller for a study

=head1 DESCRIPTION

  This class retrieves study-based data for a particular Study or 
  list of studies and outputs the data via a TT template, or puts it directly into the 'body' as XML, JSON or text.
  Most of the search logic is contained in the Search API.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Genes;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Env;

use Data::Dumper qw(Dumper);

sub loci :Regex('^genes$') { 
	my ( $self, $c ) = @_;
	$c->stash->{template}    = 'genes.tt2';
	eval {
		$self->do_search($c,'Genes');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

sub genes_extra :Regex('^genes/(.+)$') { 
	my ( $self, $c ) = @_;
	my $extra  = $c->req->captures->[0];
	
	$c->stash->{template}    = "genes/results.tt2";
	my $qname = 'Genes::'.ucfirst($extra);
	$c->stash->{genes_template}=$extra;
	$c->stash->{genes_qname}=$qname;
	#eval {
		$self->do_search($c,$qname);
	#};
#	if ($@) {
#		$c->stash->{error}=$@->message;
#	}
}

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
