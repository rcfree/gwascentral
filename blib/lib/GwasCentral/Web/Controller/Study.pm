# $Id: Study.pm 1537 2010-09-21 10:56:08Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Study - Catalyst Controller for a study

=head1 DESCRIPTION

  This class retrieves study-based data for a particular Study or 
  list of studies and outputs the data via a TT template, or puts it directly into the 'body' as XML, JSON or text.
  Most of the search logic is contained in the Search API.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Study;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Env;

use Data::Dumper qw(Dumper);

my $home = $ENV{HGVBASEG2PWEB_HOME};

sub study_common : Private {
	my ( $self, $c ) = @_;
	
	my $study_ident = $c->req->captures->[0];
	$c->req->params->{id}=$c->req->captures->[0];
	$c->stash->{template}    = 'study.tt2';
	$self->do_search($c,'Study');
#	$c->stash->{study_ident} = $study_ident;    #study identifier
#	$c->stash->{template}    = 'study.tt2';     #base study template
#	$c->stash->{study_template} =
#	  'summary';    #current tab template (becomes 'study/summary.tt2')
#
#	#eval {
#	my $study    = $c->model('Study')->get_study_by_identifier($study_ident);
#	#my $requests = $c->model('Study')->AccessControl->get_pending_requests($study_ident);
#	my @resultsets = map { $_->resultsets } $study->experiments;
#	$c->stash->{study_resultsets} = [map { $_->identifier } @resultsets];
#	$c->stash->{study}            = $study;
#	
#	my $rsfilter = $c->stash->{basic}->f('ResultSet');
#	my ($addable, $added) = $rsfilter->addable_study_state({identifier => $study->identifier, accesslevel=>$study->accesslevel, child_accesslevel => $study->child_accesslevel});
#	$c->stash->{addable}=$addable;
#	$c->stash->{added}=$added;
}

# deals with MeSH concept autocomplete and concept<->heading mapping
sub lookup :Regex('^study/lookup$') {
    my ( $self, $c ) = @_;
	my $value = $c->req->param('q');

  	my $lookup = GwasCentral::Search::Lookup->new({conf_file=>$c->config});
  	$lookup->populate_DS_from_context($c);
  	my @lookup_rows = $lookup->autocomplete({
  		rows => 10,
  		sorted => 1,
  		lookup_sources => ['markers','features', 'studies'],
  		value => $value,
  	});
  	
  	my $htmlstring = join("\n",@lookup_rows);
  	$c->res->content_type('text/html');
	$c->res->output($htmlstring);
}

=head2 study

  Usage      : Controller action which maps to URL /study/HGVSTxxx (used by all study report pages - see below)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study : Regex('^study/(HGV\w+)$') {
	my ( $self, $c ) = @_;

	#output results as TT template or to XML/JSON/text using 'output_report'
#	my $experiment =
#	  $c->stash->{study}
#	  ->experiments->search( {}, { order_by => 'totalmarkersimported DESC' } )
#	  ->first;
	#$c->stash->{totalmarkersimported} = $experiment->totalmarkersimported;
	#$self->output_report( $c, 'study', $c->stash->{study} );
	
	$c->stash->{study_template}="summary";
	eval {
		$c->forward('study_common');
	};
	if ($@) {
		$c->stash->{error}=$@->message;
	}
}

=head2 study_analyses

  Usage      : Controller action which maps to URL /study/HGVSTxxx/analyses (Analysis Experiments tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study_analyses : Regex('^study/(HGV\w+)/analyses$') {
	my ( $self, $c ) = @_;

	#retrieve study via 'study' action - if an error then return
	$c->forward('study_common');
	$c->stash->{error} and return;

	#get association experiments via the study
	my $ident = $c->stash->{search}->{Study}->{content}->{identifier};
	my $query = $c->stash->{search}->{Study}->{Query};
	$c->stash->{assoc_exps} = $query->get_association_experiments;
	$c->stash->{study_template} =
	  "analyses";    #current tab template (becomes 'study/summary.tt2')
}

=head2 study_genotypes

  Usage      : Controller action which maps to URL /study/HGVSTxxx/genotypes (Genotype Experiments tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study_genotypes : Regex('^study/(HGV\w+)/genotypes$') {
	my ( $self, $c ) = @_;

	#retrieve study via 'study' action - if an error then return
	$c->forward('study_common');
	$c->stash->{error} and return;
	$c->stash->{study_template} =
	  "genotypes";    #current tab template (becomes 'study/summary.tt2')
}

=head2 study_markers

  Usage      : Controller action which maps to URL /study/HGVSTxxx/markers (Markers tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study_markers : Regex('^study/(HGV\w+)/markers$') {
	my ( $self, $c ) = @_;

	#retrieve study via 'study' action - if an error then return
	$c->stash->{ignore_non_html}=1;
	$c->forward('study_common');
	$c->stash->{error} and return;
	
	$self->do_search($c,'Study');
	$self->do_search($c, 'StudyMarkers');

	$c->stash->{study_template} =
	  "markers";    #current tab template (becomes 'study/markers.tt2')
}


sub study_resultset : Regex('^study/(HGV\w+)/markers/(HGV\w+)$') {
	my ( $self, $c ) = @_;
	$c->req->params->{id}=$c->req->captures->[1];
	$self->do_search($c,'ResultsetMarkers');
}

=head2 study_panels

  Usage      : Controller action which maps to URL /study/HGVSTxxx/panels (Panels tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study_panels : Regex('^study/(HGV\w+)/panels$') {
	my ( $self, $c ) = @_;
	$c->forward('study_common');
	$c->stash->{error} and return;
	$c->stash->{study_template} =
	  "panels";    #current tab template (becomes 'study/panels.tt2')
}

=head2 study_phenotypes

  Usage      : Controller action which maps to URL /study/HGVSTxxx/phenotypes (Phenotypes tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub study_phenotypes : Regex('^study/(HGV\w+)/phenotypes$') {
	my ( $self, $c ) = @_;
	$c->forward('study_common');
	$c->stash->{error} and return;
	$c->stash->{study_template} =
	  "phenotypes";    #current tab template (becomes 'study/phenotypes.tt2')
}

=head2 studies

  Usage      : Controller action which maps to URL /studies
  Purpose    : Action related to Studies list which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub studies : Regex('^studies$') {
	my ( $self, $c ) = @_;

	#
	#do search using Studies modules with a number of parameters
	$c->stash->{template} = 'studies.tt2';
	eval {
		$self->do_search($c,'Studies');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}


=head2 experiment

  Usage      : Controller action which maps to URL /study/experiment/HGVExxx
  Purpose    : Action for Experiment report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub experiment : Regex('^study/experiment/(HGV\w+)$') {
	my ( $self, $c ) = @_;
	my $experiment_ident = $c->req->captures->[0];

	# Retrieve single experiment the database
	my $experiment =
	  $c->model("Study")->get_experiment_by_identifier($experiment_ident);

	$c->stash->{template}     = 'study/experiment.tt2';
	$c->stash->{experiment}   = $experiment;
	$c->stash->{marker_count} = $experiment->usedmarkersets->count;
	return;
}

=head2 add_all

  Usage      : Controller action which maps to URL /study/add_all
  Purpose    : Action which adds all Studies to the browser database (not available through live site)
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub add_all : Regex('^study/add_all$') {
	my ( $self, $c ) = @_;
	return if $c->config->{live_site};

	#get all resultsets in all studies and set the 'resultsets' in the session
	my @studies = $c->model("Study")->get_all_unhidden_studies;

	if (@studies) {
		$c->session->{studies} = { map { $_->identifier => 1; } @studies };
	}

	my @resultsetids = ();
	foreach my $study (@studies) {
		foreach my $exp ( $study->experiments ) {
			foreach my $rs ( $exp->resultsets ) {
				push @resultsetids, $rs->resultsetid;
			}
		}
	}

	if (@resultsetids) {
		$c->session->{resultsets} = \@resultsetids;
	}

	$c->stash->{template} = 'studies.tt2';
}

sub study_genes : Regex('^study/(HGV\w+)/genes$') {
	my ( $self, $c ) = @_;

	#retrieve study via 'study' action - if an error then return
	$c->forward('study_common');
	$c->stash->{error} and return;

	#get list of resultsets using either param 'r' or all those in study
	my $study_ident = $c->stash->{study_ident};
	$c->req->params->{study} = $study_ident;

	$c->do_search('Genes');

	$c->stash->{study_template} =
	  "genes";    #current tab template (becomes 'study/markers.tt2')
}

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>
  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
