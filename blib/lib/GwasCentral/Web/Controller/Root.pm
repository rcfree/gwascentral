# $Id: Root.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Root - Root Catalyst Controller

=head1 DESCRIPTION

This is the base controller for the HGVbase web application.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Root;

use strict;
use warnings;
use base 'Catalyst::Controller::GwasCentral';
use Data::Dumper qw(Dumper);
use Bio::Graphics;

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
__PACKAGE__->config->{namespace} = '';

my $home = $ENV{HGVBASEG2PWEB_HOME};

=head2 default

Default behaviour of the root controller is to just display a template 
corresponding to the URL path in the request. This is more or less the
same as static HTML-file handling, except provides full template functionality.

=cut

sub login : Local {
	my ( $self, $c ) = @_;
	$c->res->header( 'Access-Control-Allow-Origin' => '*' );
	my $user     = $c->req->param('user');
	my $pass     = $c->req->param('pass');
	my $login = $c->req->param('login');
	$c->stash->{template}="login.tt2";
	if ( $c->user_exists ) {
		$c->log->info( "already logged in as " . $c->user->nickname );
		$c->stash->{json} = $self->json(
			$c,
			{
				status    => 'loggedin',
				nickname  => $c->user->nickname,
				loggedin  => $c->user->email,
				sessionid => $c->sessionid
			}
		);
		return;
	}
	if ( !defined($login) ) {
		$c->log->info("not logged in");
$c->stash->{json} = $self->json( $c,
			{ status => "notloggedin", sessionid => $c->sessionid } );
		return;
	}
	if (
		$c->authenticate(
			{
				'email'    => $user,
				'password' => $pass
			},
			'members'
		)
	  )
	{
		$c->log->info("login success!");
		$c->log->info("authenticates: $user");
		$c->stash->{json} = $self->json(
			$c,
			{
				nickname  => $c->user->nickname,
				loggedin  => $c->user->email,
				sessionid => $c->sessionid,
				status => 'loggedin',
			}
		);
		return;
	}
	else {
		$c->stash->{json} = $self->json($c, { status => "loginfailed", sessionid => $c->sessionid });
		$c->log->info("login fails!");
#		$self->output_div( $c,
#			{ status => "loginfailed", sessionid => $c->sessionid } );
		return;
	}
}
#
#sub oauth : Local {
#       my ($self, $c) = @_;
#       
#       if( $c->authenticate( { provider => 'gwasconnect1.localhost' } ) ) {
#            #do something with $c->user
#       }
#}
    
sub logout : Local {
	my ( $self, $c ) = @_;
	$c->stash->{json} = $self->json( $c,
			{ status => "notloggedin", sessionid => $c->sessionid } );
	$c->logout();
	$c->stash->{template}="login.tt2";
}

sub default : Private {
    my ( $self, $c, @path ) = @_;

    my $path = join('/', @path);
    @path == 0 and $c->res->redirect('index');

    #hack to deal with 'user' path (not sure why this is happening
#    if ($path eq 'user') {
#
#    	$c->stash->{template}='info/notfound.tt2';
#    	return;
#    }
#
    # check for presence of template file in directory
    my $include_paths = $c->view('TT')->{INCLUDE_PATH};

     if (!-e  $include_paths->[0]."/".$path.'.tt2') {
     	$c->log->info("Path $path.tt2 does not exist");
     	$c->stash->{template}='info/notfound.tt2';
     	return;
     }

    # Serve template directly if available
    $c->stash->{template} = @path > 0 ?  $path.'.tt2' : 'index.tt2'; # serve /index if nothing specified
}

=head2 end

Attempt to render a view, if needed.

=cut 

sub end : ActionClass('RenderView') {
}

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>
  Rob Free <rcfree@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
