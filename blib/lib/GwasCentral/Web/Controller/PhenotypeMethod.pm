# $Id: PhenotypeMethod.pm 1553 2010-10-27 09:34:45Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::PhenotypeMethod - Catalyst Controller for phenotype methods

=head1 DESCRIPTION

  Catalyst Controller.

=head1 METHODS

=cut

package GwasCentral::Web::Controller::PhenotypeMethod;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

#use GwasCentral::Schema;
use JSON::XS;
use Data::Dumper qw(Dumper);

# Globals
my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
$json_coder->space_after(1);

=head2 view

Action which redirects to the default action

=cut

sub view : Local {
    my ( $self, $c ) = @_;
    my $url = $c->req->uri->as_string;
    $url =~ s|(\w+)/view|$1|;
    $c->response->redirect($url);
}

=head2 default

Action which retrieves a phenotype method and passes it to the template

=cut

sub default :Regex('^phenotypemethod/(HGV\w+)$') {
    my ( $self, $c) = @_;
    my $id = $c->req->captures->[0];
    $c->log->debug("Creating phenotypemethod report for $id");
    my $phenmethod = $c->model("StudyDB")->get_phenotypemethod_by_identifier($id);
	$c->stash->{phenmethodid} = $id;
    $c->stash->{phenmethod} = $phenmethod;
    $c->stash->{template} = 'phenotypemethod/view.tt2';
    return if !$phenmethod;
    	
    my $phenoannids = $c->model("StudyDB")->get_mesh_phenotypeannotations_by_phenotypemethod($id);
    my $phenoannids_hpo = $c->model("StudyDB")->get_hpo_phenotypeannotations_by_phenotypemethod($id);
    my $mhpoterms='';
	my $hpoterms='';
	my @meshidlist=();
	my @hpolist=();
	my %meshhash=();
	my %hpohash=();
	
	# find mesh from phenotypeidentifier
	while (my $row = $phenoannids->next){
		my $searchpid = $row->get_column('pid');
		push(@meshidlist, $searchpid);
		
		# find HPO terms that map to mesh term
		$mhpoterms = $c->model("HpoDB")->get_hpo_from_meshid2($searchpid);
		
		# Some HPO terms were being repeated (phenotypemethod NOT term specific)
		# points to repeats in study db.  Therefore need to check that a HPO term has not been found previously for
		# this phenotypemethod.  Can do same thing by refining queries - To do.
#		while (my $row = $mhpoterms->next){
#			my $hponame = $row->get_column('hponame');
#			if (exists $hpohash{$hponame}){
#				# if exists then this is repeat.  This line here for bug testing.
#			}
#			else{
#				$hpohash{$hponame}=1;
#				push(@hpolist, $mhpoterms->all);
#			}
#		}

		push(@hpolist, $mhpoterms->all);

	}
	
	# find hpo from phenotypeidentifier
	while (my $row = $phenoannids_hpo->next){
		my $searchpid = $row->get_column('pid');
		$hpoterms = $c->model("HpoDB")->get_name_from_id($searchpid);
		push(@hpolist, $hpoterms->first);

	}
    
    my $pidlist=\@meshidlist;
    my @meshterms = $c->model("OntologyDB")->get_mesh_headings_from_id($pidlist);
    my $meshterms=\@meshterms;
        
    my $hpotermsubmit=\@hpolist;
	
    $c->stash->{phenoannids} = $phenoannids;
    $c->stash->{meshterms} = $meshterms;
    $c->stash->{hpoterms} = $hpotermsubmit;
    return;
}


=head2 experiments

Action which gets experiments associated with a phenotypemethod and passes them to a template.

=cut

sub experiments :Regex('^phenotypemethod/experiments/(HGV\w+)$') {
    my ( $self, $c ) = @_;
	my $id = $c->req->captures->[0];
	
    # Retrieve experiments with phenotype method from the database
    my $experiments = $c->model("StudyDB")->get_experiments_with_phenotypemethod_by_identifier($id);
    
  	if ($experiments->count==1 && $experiments->single->resultsets->count==1) {
  		$c->stash->{single}=1;
  	}
  	
    $c->stash->{experiments}=$experiments;
    
    $c->stash->{template}="phenotypemethod/experiments.tt2";
    return;
}


=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>
  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
