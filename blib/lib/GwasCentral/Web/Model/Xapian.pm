# $Id: Xapian.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Web::Model::Xapian - Catalyst Xapian search engine wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper based on L<Catalyst::Model::Adaptor> Model for L<GwasCentral::Database::Xapian> database utility module using schema L<Search::Xapian>

=cut

package GwasCentral::Web::Model::Xapian;

use strict;
use base qw(Catalyst::Model::Adaptor);

use Data::Dumper qw(Dumper);
my $config = GwasCentral::Web->config;

__PACKAGE__->config( class => 'GwasCentral::DataSource::Xapian', args => { conf_file => $config } );

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>
  Rob Free <rcfree@gmail.com>


=head1 LICENSE

This library is free software . You can redistribute it and/or modify it under
the same terms as perl itself.

=cut

1;
