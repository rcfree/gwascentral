# $Id: MarkerDB.pm 1449 2010-05-26 16:26:10Z rcf8 $

=head1 NAME

GwasCentral::Web::Model::MarkerDB - Catalyst Marker database module wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper based on L<Catalyst::Model::Adaptor> Model for L<GwasCentral::Database::Marker> database utility module using schema L<GwasCentral::Schema::Marker>

=cut

package GwasCentral::Web::Model::Marker;

use strict;
use base qw(Catalyst::Model::Adaptor);

use Data::Dumper qw(Dumper);
my $config = GwasCentral::Web->config;

__PACKAGE__->config( class => 'GwasCentral::DataSource::Marker', args => { conf_file => $config } );


=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
