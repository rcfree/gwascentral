# $Id: SessionDB.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Web::Model::SessionDB - Catalyst session database module wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper for session database based on L<DBIx::Class> Model using schema L<GwasCentral::Schema::Session>

=cut

package GwasCentral::Web::Model::Session;

use strict;
use base qw(Catalyst::Model::Adaptor);

use Data::Dumper qw(Dumper);

my $config = GwasCentral::Web->config;

__PACKAGE__->config( class => 'GwasCentral::DataSource::Session', args => { conf_file => $config } );

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
