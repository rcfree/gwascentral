# $Id$

=head1 NAME

GwasCentral::Web::Model::OntologyDB - Catalyst Study database module wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper for Mesh database based on L<DBIx::Class> Model using schema L<GwasCentral::Schema::Mesh>

=cut

package GwasCentral::Web::Model::Ontology;

use strict;
use base qw(Catalyst::Model::Adaptor);

use Data::Dumper qw(Dumper);

my $config = GwasCentral::Web->config;

__PACKAGE__->config( class => 'GwasCentral::DataSource::Ontology', args => { conf_file => $config } );

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
