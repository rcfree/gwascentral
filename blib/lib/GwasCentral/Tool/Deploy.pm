# $Id: Browser.pm 1478 2010-06-19 22:09:16Z rcf8 $

=head1 NAME

GwasCentral::Database::Browser - Query Browser GwasCentral DB using DBI


=head1 SYNOPSIS

use GwasCentral::Database::Browser;
my $browser_db = GwasCentral::Database::Browser->new( { conf_file => '/conf/hgvbase.conf' } );
my $browser = $browser_db->get_markers_in_resultsets(['HGVRS1','HGVRS2','HGVRS3']);


=head1 DESCRIPTION

Provides a wrapper for common Browser database access methods. Returns single hashrefs or arrays of hashrefs

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Tool::Deploy;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../";
use Search::Xapian qw(:standard);
use FindBin;
use Config::General qw(SaveConfig);
use GwasCentral::Base qw(load_config open_infile open_outfile);
use GwasCentral::Browser::Util qw(neglog);
use Date::Simple;
use Data::Dumper qw(Dumper);
use File::Path;
use GwasCentral::DataSource::Study;

extends qw(GwasCentral::Base);
has 'databases' => ('is'=>'rw', 'default' => sub { {} });
has 'force' => ('is'=>'rw');
has 'remote_config' => ('is'=>'rw');
has 'rel_history_fh'=>('is'=>'rw');
has 'study' => ('is'=>'rw');
has 'marker' => ('is'=>'rw');
has 'browser' => ('is'=>'rw');
has 'transfer' => ('is'=>'rw');
has 'optimise' => ('is'=>'rw');
has 'calc' => ('is'=>'rw');

no Moose;

sub do {
	my ($self) = @_;
	
	if ($self->transfer) {
		$self->retrieve_remote_config;
	
		#deploy database(s)
		#update config file with new database name
		
		$self->transfer_databases();
		
		my $db = GwasCentral::Database->new(
			{ conf_file=>$self->remote_config }
		);
		$self->databases({
			study=>$db->new_instance('study'),
			marker=>$db->new_instance('marker'),
			browser=>$db->new_instance('browser')
		});
	}
	else {
		my $db = GwasCentral::Database->new(
			{ conf_file=>$self->config }
		);
		$self->databases({
			study=>GwasCentral::DataSource::Study::Database->new({conf_file=>'conf/hgvbase.conf'}),
			marker=>$db->new_instance('marker'),
			browser=>$db->new_instance('browser')
		});
	}
	
	$self->index_for_text_search(['studies','phenotypes']);
	$self->calc_markers_imported_and_levels();
	$self->populate_gt_bundles();
	
	if ($self->optimise) {
		$self->optimise_databases();
	}
	
	if ($self->transfer) {
		$self->store_remote_config;
	}
}

sub retrieve_remote_config {
	my ($self) = @_;
	
	my $depconfig = $self->config->{Deploy};
	$self->log->info("Retrieving remote config from '".$depconfig->{install_server}.":".$depconfig->{install_path}." as ".$depconfig->{install_user});
	my %rconfig;
	
	my $path = $depconfig->{tmp_path};
		
	die "Path '$path' not found!" if !-e $path;
	my $cpath = $path."/conf";
	mkdir $cpath if !-e $cpath;
	
	if ($depconfig->{install_server} eq 'localhost') {
		system "cp ".$depconfig->{install_path}."/conf/* ".$cpath."/";
		
	}
	else {
		system "scp ".$depconfig->{install_user}."@".$depconfig->{install_server}.":".$depconfig->{install_path}."/conf/* ".$cpath."/";
	}
	%rconfig = load_config($depconfig->{tmp_path}."/conf/hgvbase.conf");
	
	$self->remote_config(\%rconfig);
}

sub store_remote_config {
	my ($self) = @_;
	
	my $depconfig = $self->config->{Deploy};
	$self->log->info("Storing remote config from '".$depconfig->{install_server}.":".$depconfig->{install_path}." as ".$depconfig->{install_user});
	my %rconfig;
	
	my $path = $depconfig->{tmp_path};
		
	die "Path '$path' not found!" if !-e $path;
	my $cpath = $path."/conf";
	mkdir $cpath if !-e $cpath;
	
	if ($depconfig->{install_server} eq 'localhost') {
		system "cp $cpath/hgvbase.conf ".$depconfig->{install_path}."/conf/";
		
		if ($self->study) {
			system "cp $cpath/study_history.txt ".$depconfig->{install_path}."/conf/";
		}
		if ($self->marker) {
			system "cp $cpath/marker_history.txt ".$depconfig->{install_path}."/conf/";
		}
	}
	else {
		system "scp $cpath/hgvbase.conf ".$depconfig->{install_user}."@".$depconfig->{install_server}.":".$depconfig->{install_path}."/conf/";
		if ($self->study) {
			system "scp $cpath/study_history.txt ".$depconfig->{install_user}."@".$depconfig->{install_server}.":".$depconfig->{install_path}."/conf/";
		}
		if ($self->marker) {
			system "scp $cpath/marker_history.txt ".$depconfig->{install_user}."@".$depconfig->{install_server}.":".$depconfig->{install_path}."/conf/";
		}
	}
	%rconfig = load_config($depconfig->{tmp_path}."/conf/hgvbase.conf");
	
	$self->remote_config(\%rconfig);
}

sub dump_database {
	my ($self, $database,$version,$innodb_to_myisam) = @_;
	my $config = $self->config;
	my $depconfig = $self->config->{Deploy};
	my $path = $depconfig->{tmp_path};
		
	die "Path '$path' not found!" if !-e $path;
	my $dbpath = $path."/".lc($database);
	mkdir $dbpath if !-e $dbpath;
	my $relpath = $dbpath."/release".$version;
	mkdir $relpath if !-e $relpath;

	my $dbconfig = $config->{Database}->{$database};
	my @db_info = split(":",$dbconfig->{dsn});
	my $db_name = $db_info[2];
	if (!$self->force) {
		print "All files in the location '$relpath' will be deleted. Are you sure?\n";
		my $yn = <STDIN>;
		exit unless $yn =~ 'y';
	}
	system "rm -rf $relpath/*";
	system "chmod o+w $relpath";
	$self->log->info("Dumping tables from database ".$dbconfig->{dsn}." to '$relpath'");
	system "mysqldump $db_name -t -T $relpath -u".$dbconfig->{user}." -p".$dbconfig->{pass};
	system "mysqldump $db_name | perl -p -e 's/InnoDB/MyISAM/' > $relpath/".lc($database)."_schema.sql" if $innodb_to_myisam;
#
#	
	my $install_dbname = $depconfig->{name_prefix}.lc($database)."_live_".$version;
	$self->log->info("Creating database '$install_dbname' on '".$depconfig->{install_server}."'");
	system "mysqladmin -h ".$depconfig->{install_server}." create ".$install_dbname." -u".$depconfig->{user}." -p".$depconfig->{pass};
	system "mysql -h ".$depconfig->{install_server}." -u".$depconfig->{user}." -p".$depconfig->{pass}." $install_dbname < $relpath/".lc($database)."_schema.sql";
	system "mysqlimport -h ".$depconfig->{install_server}." $install_dbname -u".$depconfig->{user}." -p".$depconfig->{pass}." -v --local $relpath/*.txt";
	my $install_db = $self->remote_config->{Database}->{$database};
	
	$install_db->{dsn} = "dbi:mysql:".$install_dbname.":".$depconfig->{install_server};
	my $remote_user = $install_db->{user};
	my $e_text = "GRANT SELECT ON `$install_dbname`.* TO `$remote_user`\@`%`;";
	system "mysql -h ".$depconfig->{install_server}." -u".$depconfig->{user}." -p".$depconfig->{pass}." -e '".$e_text."'";
	$self->remote_config->{Database}->{$database}=$install_db;
	
	
}

sub transfer_databases {
	my ($self) = @_;
	
	my ($study,$marker) = ($self->study, $self->marker);
	if ($study) {
		$self->dump_database('Study',$self->config->{Deploy}->{study_version},1);
		$self->add_data_release('Study',$self->config->{Deploy}->{study_version});
	}
	if ($marker) {
		$self->dump_database('Marker',$self->config->{Deploy}->{marker_version});
		$self->add_data_release('Marker',$self->config->{Deploy}->{marker_version});
	}
	if ($study || $marker) {
		$self->dump_database('Browser',$self->config->{Deploy}->{study_version}."_".$self->config->{Deploy}->{marker_version});
	}
	SaveConfig($self->config->{Deploy}->{tmp_path}."/hgvbase.conf", $self->remote_config);
	
}

sub add_data_release {
	my ($self,$database,$version) = @_;
	my $depconfig = $self->config->{Deploy};
	
	my $history_file = $depconfig->{tmp_path}."/conf/".lc($database)."_history.txt";
	my $rel_history_fh = open_infile($history_file);
	my @rh_lines = $rel_history_fh->getlines;
	my $cur_release = "Release $version";
	return if ($rh_lines[0] =~ /$cur_release/);
	my $date = Date::Simple->today();
	
	$self->log->info("Adding version $version details to release history - ".$date->format("%d %b %y"));
	unshift @rh_lines, "    ".$depconfig->{lc($database)."_details"}."\n\n";
	
	unshift @rh_lines, "Release $version - ".$date->format("%d %b %Y")."\n";
	$rel_history_fh->close;
	
	$rel_history_fh = open_outfile($history_file);
	foreach(@rh_lines) { $rel_history_fh->print($_) };
	$rel_history_fh->close;
}

sub index_for_text_search {
	my ($self, $databases) = @_;
	my $config = $self->config;
	my $depconfig = $config->{Deploy};
	
	my $path = $self->transfer ? $depconfig->{tmp_path} : $config->{basedir};
	my $index_basedir = $path."/index";
	mkdir $index_basedir if !-e $index_basedir;
	
	my $searchconfig = $config->{'Search'};
	
	# Get list of databases to do. If not provided on cmdline, -a option will do all db's in configfile.
	
	$self->log->info("Index basedir:$index_basedir");
	-d $index_basedir or die "index_basedir '$index_basedir' not valid";
	my @databases = @{ $databases || [] };
	map { $_ = ucfirst($_) } @databases;
	if ( @databases == 0 ) {
			die "No dblist provided and no -a option, so aborting mission";
	}
	$self->log->info( "Creating/updating ", scalar(@databases), " databases" );
	
	DB:
	foreach my $dbname (@databases) {
		$self->_generate_xapian($dbname,$index_basedir);
		foreach my $portal(keys %{$config->{Portal}}) {
			$self->_generate_xapian($dbname, $index_basedir, $portal);
		}
	}
}

sub _generate_xapian {
	my ($self, $dbname, $index_basedir, $portal) = @_;
	my $searchconfig = $self->config->{Search};
	my $schema = $self->{databases}->{study};
	if ($schema->does("GwasCentral::DataSource::Portalable")) {
		$schema->portal($portal);
	}
	my $conf = $searchconfig->{Query}->{$dbname}
	  || die "No config available for database '$dbname', aborting";
	unless ( $conf->{type} eq 'Xapian' ) {
		$self->log->info(
"Database type '$conf->{type}' not handled by this script, skipping database '$dbname'"
		);
		next DB;
	}
	$self->log->info("Doing Xapian database $dbname: '$conf->{display_plural}'");
	
	# Configure search engine
	my $stemmer = Search::Xapian::Stem->new('english');
	my $dbdir   = $index_basedir . '/' . lc($dbname). ($portal ? "/".$portal : "");
	$self->log->info("Creating database index in $dbdir");
	-d $dbdir or mkpath( $dbdir, 0, 0775 );
	my $db =
	  Search::Xapian::WritableDatabase->new( $dbdir, DB_CREATE_OR_OVERWRITE );

	
	# Retrieve data via DBIx::Class middleware
	my $dbclass = $conf->{class};
	my $method_name =  $conf->{retrieve_method} || 'get_all_'.lc($conf->{display_plural});
	#$self->log->info("Using '$method_name' method to get data");
	my @rset = $schema->$method_name;
	my $i    = 0;
	foreach my $obj (@rset) {
		#$self->log->info( "Object identifier:".($obj->identifier ? $obj->identifier : " none"));
		$i++;
		$i % 100 == 0 and print STDERR "\r$i entries processed";
		#$self->log->info( "Got node #$i: " . $obj->identifier() . ":" . $obj->name );

		my $doc = Search::Xapian::Document->new();
		foreach my $field ( split( /\s/, $conf->{fields2index} ) ) {
			my $pos = 0;

	# Populate db-document with terms, after splitting sentences into tokens
			my $value = $obj->$field;
			
			#recursive processing of classes from database
			_process_data($value,$field,$conf,$stemmer, $doc, $pos);
		}
		
		#only store base class identifier
		$doc->set_data( $obj->identifier );
		$db->add_document($doc);
	}
	$db->flush();
	print STDERR "\n";
	undef $db;
	$self->log->info("Indexed $i entries for database $dbname");
}

sub _process_data {
	my ($value,$field,$conf,$stemmer, $doc, $pos)=@_;
	return if !$value;
	
	#print ref($value)."\n";
	if (ref($value) =~ /DBIx/ || ref($value) =~ /GwasCentral/) {
		if ($value->isa('DBIx::Class')) {
			#print "  Got related table ".$field."\n";
			if (!$conf->{$field."_fields"}) {
				die "Fields table:'$field"."_fields not found";
			}
			my @fields = split (/\ /,$conf->{$field."_fields"});
			
			if (!$value->can('next')) {
				foreach my $temp_field(@fields) {
					my $value2 = $value->$temp_field;
					_process_data($value2,$temp_field,$conf,$stemmer, $doc, $pos);
				}
			}
			else {
				while (my $related = $value->next) {
					
					foreach my $temp_field(@fields) {
						my $value2 = $related->$temp_field;
						_process_data($value2,$temp_field,$conf,$stemmer, $doc, $pos);
					}
				}
			}
		}
	}
	else {
		#print "  Got $field => $value\n";
		_parse_words($stemmer, $doc, $value ,$pos);
	}
}
	
sub _parse_words {
	my ($stemmer, $doc, $value, $pos)=@_;
	foreach my $w ( split( /[\s+,\|,\n+,\,,\),\(,-]/, $value ) )
				{
					$pos++;
					next if $w eq '' || $w =~ /(NULL|\||\s)/;
					my $term = $stemmer->stem_word( lc($w) );
					#print 
#"    Indexing term '$term' (lc+stemmed '$w'), at pos $pos\n"
					;
					$doc->add_posting( $term, $pos );
				}
}


sub calc_markers_imported_and_levels {
	my ($self) = @_;
	my $study_db = $self->{databases}->{study};
	my @studies = $study_db->dbh->resultset('Study')->all;

	foreach my $study(@studies) {
		my @rset_ids=();
		foreach my $experiment($study->experiments->all) {
			my $count = $experiment->usedmarkersets->count;
			#print $experiment->identifier.":".$count."\n";
			$experiment->update({ totalmarkersimported => $count});
			
			@rset_ids = (@rset_ids,map { $_->resultsetid } $experiment->resultsets->all);
		}
		my $top = $study_db->dbh->resultset('Significance')->search({
				resultsetid=>{-in=>\@rset_ids}, unadjustedpvalue=>{'>'=>0}
			},{ page=>1, rows=>1, order_by=>'unadjustedpvalue'})->single;
		my $neglog='0';
		if ($top) {
			#print Dumper({$top->get_columns()});
			my $neglog = neglog($top->unadjustedpvalue);
			$neglog>8 and $neglog=8;
			$neglog=substr($neglog,0,1);
			$neglog==0 and $neglog='0';
			#$self->log->info("study:".$study->identifier." = ".$neglog);
			$study->update({ significancelevel=>$neglog });
		}
		
	}
}

sub populate_gt_bundles {
	my ($self) = @_;
	$self->{databases}->{study}->dbh->storage->dbh->do("UPDATE Experiment SET GenotypedBundle = (SELECT GROUP_CONCAT(BundleLoci.BundleName SEPARATOR ';') FROM BundleLoci NATURAL JOIN GenotypedBundle WHERE Experiment.ExperimentID = GenotypedBundle.ExperimentID)");
}

sub generate_ontology_xml {
}

sub generate_das_config {
	my ($self) = @_;
	my $config = $self->config;
	my $psconfig = $config->{ProServer};
	
	my $path = $self->config->{basedir}."/conf/proserver/hgvbaseg2p.ini";
	my $fh = open_outfile($path);
	$fh->print("[general]\n");
	
	foreach my $option(keys %{$psconfig}) {
		$fh->print($option."=".$psconfig->{$option}."\n");
	}
	$fh->print("\n");
	my $study_db = $self->{databases}->{study};
	my @studies = $study_db->get_all_unhidden_studies;
	
	my %conn_info_for = %{$config->{Database}->{Browser}};
	my @dsn_options = split(":",$conn_info_for{dsn});
	
	foreach my $study(@studies) {
		my $study_ident = $study->identifier;
				
		foreach my $experiment($study->experiments->all) {

			foreach my $rset($experiment->resultsets->all) {
				my $rset_ident = $rset->identifier;
				$fh->print("[".$study_ident."_".$rset_ident."]\n");
				$fh->print("adaptor=hgvbaseg2p\n");
				$fh->print("state=on\n");
				$fh->print("transport=dbi\n");
				$fh->print("dbname=".$dsn_options[2]."\n");
				$fh->print("dbuser=".$conn_info_for{user}."\n");
				$fh->print("dbpass=".$conn_info_for{pass}."\n");
				$fh->print("description=".$study->name." (".$rset->name.")\n");
				$fh->print("doc_href=".$config->{WebSite}->{hgvbaseg2p_baseurl}."/study/".$study_ident."\n");
				$fh->print("styleshome=".$config->{basedir}."/conf/proserver/stylesheets\n");
				$fh->print("study_identifier=".$study_ident."\n");
				$fh->print("rset_identifier=".$rset_ident."\n");
				$fh->print("\n");
			}
		}
	}
	
	$fh->close;

#[DGI_Type_2_Diabetes_P-value_of_the_Z-score_test_statistic]
#adaptor=hgvbaseg2pdb
#state=on
#transport=dbi
#dbtable=das_data
#dbname=GwasCentral_das
#dbhost=viti.gene.le.ac.uk
#dbport=3306
#dbuser=gt50
#dbpass=gt50
#description=DGI Type 2 Diabetes using P-value of the Z-score test statistic from GwasCentral study HGVST3
#dsncreated=2008-07-02 15:57:36
#doc_href=http://www.hgvbaseg2p.org/hgvbaseg2p/study/view/HGVST3
#styleshome=/home/ol8/download/Bio-Das-ProServer/stylesheets/
#studyid=HGVST3
#experimentid=49
#analysismethodid=35
	
}

sub optimise_databases {
	my ($self) = @_;
	foreach my $db(keys %{$self->databases}) {
		$self->log->info("Optimise tables in database $db");
		my $dbh = $self->databases->{$db}->dbh->storage->dbh;
		my $sth = $dbh->table_info( '', '', "%", '' );
		my @tables = map { $_->[2] } @{$sth->fetchall_arrayref};
		$sth->finish;
		foreach my $table(@tables) {
			$dbh->do("optimize table $table");
		}
	}
}

sub pack_database {
	my ($self,$dblabel) = @_;
	
	my $db = $self->databases->{$dblabel};
	
	my @dbinfo = split(':',$db->dsn);
	$self->log->info("dbinfo:".Dumper(\@dbinfo));
	my $dbname = $dbinfo[2];
	my $dir = $self->config->{mysqldir}."/$dbname";
	my $tmpdir = $self->config->{tempdir};
	$self->log->info("Pack tables in database ".$db->dsn." ($dir)");
	my $dbh = $db->dbi_dbh;
	my $sth = $dbh->table_info( '', '', "%", '' );
	my @tables = map { $_->[2] } @{$sth->fetchall_arrayref};
	$sth->finish;
	foreach my $table(@tables) {
		chdir $dir;
		$self->log->info("lock table $table");
		$dbh->do("lock table $table write");
		$self->log->info("flush table $table");
		$dbh->do("flush table $table");
		$self->log->info("check table $table");
		system "myisamchk --check --fast --update-state $table";
		$self->log->info("pack table $table");
		system "myisampack -f --tmpdir=$tmpdir $table ";
		$self->log->info("reindex table $table");
		system "myisamchk -raqS --tmpdir=$tmpdir $table";
		$dbh->do("unlock tables");
	}
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Browser.pm 1478 2010-06-19 22:09:16Z rcf8 $ 

=cut

