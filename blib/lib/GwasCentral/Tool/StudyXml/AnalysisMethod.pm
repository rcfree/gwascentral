# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::AnalysisMethod


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::AnalysisMethod;


    use strict;
    use warnings;

    use FindBin;
    use lib "$FindBin::Bin/../../../";

    use strict;
    use warnings;

use Moose;
    use List::Compare;
    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);


has 'pmid' => ('is'=>'rw', default=>sub{{}});


    sub studyanalysismethod
    {
        my ($self) = @_;

        return stag_nodify( [ StudyAnalysisMethod => [] ] );
    }

    sub analysismethod
    {

        my ( $self, $analysis ) = @_;

        return
          stag_nodify([
               AnalysisMethod => [
                                   [ '@' => [ [ id => $analysis->{id} ] ] ],
                                   [ Identifier  => $analysis->{identifier} ],
                                   [SubmittedInStudyIdentifier => $analysis->{study_identifier}],
                                   [ Name        => $analysis->{name} ],
                                   [ Label       => $analysis->{label} ],
                                   [ Description => $analysis->{description}],
                                   [ ProtocolParameters => $analysis->{parameters} ],
                                   [ Accession => $analysis->{accession} ],
                                   [ AccessionVersion => $analysis->{accession_version} ]


               ]
            ]
          );

    }#sub



1;
