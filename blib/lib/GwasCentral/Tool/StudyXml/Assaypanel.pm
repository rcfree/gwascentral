# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::Assaypanels


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::Assaypanel;


    use strict;
    use warnings;

    use FindBin;
    use lib "$FindBin::Bin/../../../";

    use strict;
    use warnings;

use Moose;
    use List::Compare;
    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);



has 'pmid' => ('is'=>'rw', default=>sub{{}});



    sub assayedpanel
    {
        my ( $self, $ap) = @_;

        #
        return
          stag_nodify(
            [
               Assayedpanel => [
                   [ '@' => [ [ id => $ap->{id} ] ] ],
                   [ Identifier               => $ap->{identifier} ],
                   [ Name                     => $ap->{name} ],
                   [ TotalNumberOfIndividuals => $ap->{total} ],
                   [ Description        => $ap->{description} ],
                          [ NumberOfSexMale    => $ap->{number_male} ],
                          [ NumberOfSexFemale  => $ap->{number_female} ],
                          [ NumberOfSexUnknown => $ap->{number_unknown} ],
                          [ NumberOfProbands   => $ap->{number_probands} ],
                          [ NumberOfParents    => $ap->{number_parents} ],

               ]
            ]
          );
    }


    sub selectioncriteria{
        my ($self,$data)=@_;


        return stag_nodify ( [
                      SelectionCriteria => [
                          [ SamplepanelID => $data->{macro_id} ], ]
                   ]);


    }

    sub apc
    {
        my ( $self, $apc_id ) = @_;

        #    <AssayedpanelCollection>
        #            <AssayedpanelID>control</AssayedpanelID>
        #        </AssayedpanelCollection>
        return
          stag_nodify(
                       [
                          AssayedpanelCollection =>
                            [ [ AssayedpanelID => $apc_id ], ]
                       ],
          );
    }


1;
