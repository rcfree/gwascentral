# $Id$

=head1 NAME

GwasCentral::Tool::Study


=head1 SYNOPSIS




=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::GenerateStudyXml;


	use strict;
	use warnings;

	use FindBin;
	use lib "$FindBin::Bin/../../../";

	use strict;
	use warnings;
	use List::Compare;

use Moose;
extends qw(GwasCentral::Base);

	use GwasCentral::Tool::StudyXml::Study;
	use GwasCentral::Tool::StudyXml::Phenotype;
	use GwasCentral::Tool::StudyXml::Samplepanel;
	use GwasCentral::Tool::StudyXml::AnalysisMethod;
	use GwasCentral::Tool::StudyXml::Experiment;
	use GwasCentral::Tool::StudyXml::Assaypanel;

#	use HGVbaseG2P::DataImport::Plugin;

	use Data::Stag::XMLWriter;

	use Data::Stag qw( :all );

	use Carp qw(cluck croak confess);
	use Data::Stag::XMLWriter;
	use Data::Stag qw( :all );
	use Data::Dumper qw(Dumper);


    has 'study' => ('is'=>'rw');
	has 'phenotype' => ('is'=>'rw' );
	has 'experiment' => ('is'=>'rw' );
	has 'sp' => ('is'=>'rw' );
	has 'ap' => ('is'=>'rw');
	has 'am' => ('is'=>'rw');

sub BUILD {

    my ($self) = @_;


my $study = GwasCentral::Tool::StudyXml::Study->new();

$self->study($study);

my $phenotype =
          GwasCentral::Tool::StudyXml::Phenotype->new();

        $self->phenotype($phenotype);

        my $am = GwasCentral::Tool::StudyXml::AnalysisMethod->new();

        $self->am($am);

        my $ap =
          GwasCentral::Tool::StudyXml::Assaypanel->new();

        $self->ap($ap);

        my $sp =
          GwasCentral::Tool::StudyXml::Samplepanel->new();

        $self->sp($sp);

        my $experiment =
          GwasCentral::Tool::StudyXml::Experiment->new();

        $self->experiment($experiment);




}



sub generate_root
	{


		my ($self) = @_;

		return stag_nodify([root => []]);

	}

sub generate_study{

	my ($self) =@_;



my $study = $self->study->study;





return($study);
}

sub generate_submission{

    my ($self,$data) =@_;

my $submission = $self->study->submission($data);;

return($submission);
}

sub generate_contribution{

    my ($self,$data) =@_;

my $contribution= $self->study->contribution($data);;

return($contribution);
}


sub generate_researcher{

my ($self,$data) =@_;

my $researcher = $self->study->researcher($data);

return($researcher);
}

sub generate_studyhotlink{

	my ($self,$data)=@_;



my $studyhotlink = $self->study->studyhotlink($data);

return($studyhotlink);


}

sub generate_citation{

    my ($self,$data)=@_;

    my $study_citation = $self->study->studycitation();

my $citation = $self->study->citation($data);

$study_citation->addkid($citation);

return($study_citation);


}




sub generate_studyinfo{

    my ($self,$data) =@_;

my $study = $self->study->studyinfo($data);

return($study);
}


sub generate_phenotypemethod{

    my ($self,$data)=@_;

my $pm = $self->phenotype->phenotypemethod($data);

return($pm);


}

sub generate_phenotypeproperty{

    my ($self,$data)=@_;





my $pp = $self->phenotype->phenotypeproperty($data);

return ($pp);


}


sub generate_phenotypeannotation{
my ($self,$data)=@_;

my  $ppa = $self->phenotype->pppa();


my $pa = $self->phenotype->phenotypeannotation($data);

$ppa->addkid($pa);


return($ppa);

}


sub generate_analysismethod{
my ($self,$data)=@_;

my  $study_am = $self->am->studyanalysismethod();


my $am = $self->am->analysismethod($data);

$study_am->addkid($am);


return($study_am);

}


sub generate_studysamplepanel{


	my ($self,$data)=@_;

	my  $study_sp = $self->sp->studysamplepanel();

	return($study_sp);
}

sub generate_phenotypevalue{


    my ($self,$data)=@_;

    my  $pv = $self->phenotype->phenotypevalue($data);

    return($pv);
}

sub generate_samplepanel{
my ($self,$data)=@_;

my $sp = $self->sp->samplepanel($data);

my $study_identifier = $data->{'study_identifier'};


return($sp);

}

sub generate_assayedpanel{

my ($self,$data)=@_;

my $ap = $self->ap->assayedpanel($data);


return($ap);

}

sub generate_selectioncriteria{

	my ($self,$data)=@_;

my $sc = $self->ap->selectioncriteria($data);


return($sc);


}

sub generate_experimentinfo{

    my ($self,$data)=@_;

my $exp = $self->experiment->experimentinfo($data);


return($exp);


}

sub generate_experimentassayedpanel{

    my ($self,$data)=@_;

my $exp_ap = $self->experiment->experiment_assayedpanel($data);


return($exp_ap);


}

sub generate_experiment{

    my ($self)=@_;

my $exp = $self->experiment->experiment();


return($exp);


}

sub generate_resultset{

    my ($self,$data)=@_;

my $rset = $self->experiment->resultset($data);


return($rset);


}

sub generate_experimenthotlink{

    my ($self,$data)=@_;

my $hotlink = $self->experiment->experimenthotlink($data);


return($hotlink);


}

sub generate_platform{

    my ($self,$data)=@_;

my $bundle= $self->experiment->genotyping_platform($data);


return($bundle);


}



1;
