package GwasCentral::Search::Format::FromQuery;
use Moose;
extends qw(GwasCentral::Search::Format);
has '+format_type' => ('default' => sub { my ($self) = @_; return $self->name });
use Data::Dumper qw(Dumper);


sub export {
	my ($self)         = @_;
	my $format_type = $self->format_type;
	my $method = "_to_".$format_type;
	return $self->Query->$method;
}
1;