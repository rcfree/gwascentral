package GwasCentral::Search::Format::json;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::JSON);
has '+content_type' => ('default'=>'application/json');

sub export {
	my ($self) = @_;
	$self->Query->results;
	return $self->json->encode(  $self->Query->structured_data );
}
1;