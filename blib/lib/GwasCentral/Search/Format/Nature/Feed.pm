package GwasCentral::Search::Format::Nature::Feed;
use Moose::Role;

sub _generate_html_for_summary {
	my ( $self, $summary ) = @_;
	my $description = "";
	if ( ref($summary) eq 'ARRAY' ) {
		foreach my $structure ( @{$summary} ) {
			$description .= "<div>"
			  . $self->_html_table_from_structure($structure)
			  . "</div>";
		}
	}
	else {
		$description = $self->_html_table_from_structure($summary);
	}
	return $description;
}

sub _html_table_from_item {
	my ( $self, $item ) = @_;
	my $summary = "";
	!$item and return "";
	if ( ref($item) eq 'HASH' ) {
		$summary .= "<table><tr>";
		foreach my $key ( keys %{$item} ) {
			$summary .=
"<th style='text-align:right;border:1px solid black'><b>$key</b></th>";
		}

		$summary .= "</tr>";
		$summary .= "<tr>";
		foreach my $key ( keys %{$item} ) {
			$summary .=
			  "<td>" . $self->_html_table_from_item( $item->{$key} ) . "</td>";
		}
		$summary .= "</tr></table>";
	}
	elsif ( ref($item) eq 'ARRAY' ) {
		$summary .= "<table><tr>";
		foreach my $row ( @{$item} ) {
			$summary .= "<td>" . $self->_html_table_from_item($row) . "</td>";
		}
		$summary .= "</tr></table>";
	}
	else {
		$summary .= $item;
	}
	return $summary;
}

sub _html_table_from_structure {
	my ( $self, $structure ) = @_;

	my $summary = "<table style='border-collapse:collapse'>";
	foreach my $key ( keys %{$structure} ) {
		$summary .=
		    "<tr><th style='text-align:right;border:1px solid black'><b>" 
		  . $key
		  . ":</b></th>";
		$summary .= "<td>";

		$summary .= $self->_html_table_from_item( $structure->{$key} );
		$summary .= "</td></tr>";

	}
	$summary .= "</table>";
	return $summary;
}

sub _get_feed_as_data {
	my ($self) = @_;
	my @items = @{$self->Query->data};

	my %fieldhash = ();
	my @data;
	foreach my $item (@items) {
		my %hash = ();

		foreach my $field ( keys %{$item} ) {
			my $value = $item->{$field};
			if ( ref($value) eq 'ARRAY' ) {
				my $subcount = 1;
				foreach my $subfield ( @{$value} ) {
					if ( !$fieldhash{ $field . $subcount } ) {
						$fieldhash{ $field . $subcount } = 1;
					}

					my $subvalue;
					if ( ref($subfield) eq 'HASH' ) {
						my @subvalues =
						  map { $_ . ": " . $subfield->{$_} } keys %{$subfield};
						$subvalue = join( ";", @subvalues );
					}
					else {
						$subvalue = $subfield;
					}
					$hash{ $field . $subcount } = $subvalue;

					$subcount++;
				}
			}
			else {
				$fieldhash{$field} = 1;
				$hash{$field}      = $value;
			}
		}
		push @data, \%hash;
	}
	return ( \%fieldhash, \@data );
}

sub _get_category_info {
	my ($self)  = @_;
	my $cats = $self->_get_categories; 
	my @categories = map { "$_ = ".$cats->{$_} } keys %{$cats || {} };
	return \@categories;
}

sub _get_categories {
	my ($self)  = @_;
	my $query   = $self->Query;
	my @filters = values %{ $query->Filters };
	my %categories = ();
	foreach my $f(@filters) {
		next if !$f->is_category;
		next if !$f->value;
		$categories{$f->name}=$f->value;
	}
	$categories{"AutoDetected"} = $query->recognised_as;
	return \%categories;
}
1;