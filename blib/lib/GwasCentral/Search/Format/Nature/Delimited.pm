package GwasCentral::Search::Format::Nature::Delimited;
use Moose::Role;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;
sub _to_text {
	my ( $self, $separator ) = @_;

	$separator ||= ',';
	my ( $fieldhash, $data ) = $self->_get_feed_as_data;
	my %fieldhash = %{$fieldhash};
	my @data      = @{$data};

	my $content = join( $separator, keys %fieldhash ) . "\n";
	foreach my $datum (@data) {
		my @line = ();
		foreach my $field ( keys %fieldhash ) {
			push @line, $datum->{$field} || '';
		}
		$content .= "\"" . join( '"' . $separator . '"', @line ) . "\"\n";
	}

	return $content;
}

sub _get_feed_as_data {
	my ($self) = @_;
	my @items = @{$self->Query->results};
	tie my %fieldhash, "Tie::Hash::Indexed";
	my @data;
	foreach my $item (@items) {
		my %hash = ();

		foreach my $field ( keys %{$item} ) {
			my $value = $item->{$field};
			#$self->log->debug("value:$field = ".$item->{$field});
			if ( ref($value) eq 'ARRAY' ) {
				my $subcount = 1;
				foreach my $subfield ( @{$value} ) {
					if ( !$fieldhash{ $field . $subcount } ) {
						$fieldhash{ $field . $subcount } = 1;
					}

					my $subvalue;
					if ( ref($subfield) eq 'HASH' ) {
						my @subvalues =
						  map { $_ . ": " . $subfield->{$_} } keys %{$subfield};
						$subvalue = join( ";", @subvalues );
						$self->log->debug("subvalues:".Dumper(\@subvalues));
					}
					else {
						$subvalue = $subfield;
					}
					$hash{ $field . $subcount } = $subvalue || '';

					$subcount++;
				}
			}
			else {
				$fieldhash{$field} = 1;
				$hash{$field}      = $value || '';
			}
		}
		push @data, \%hash;
	}
	
	
	$self->log->debug("fieldhash:".Dumper(\%fieldhash));
	return ( \%fieldhash, \@data );
}
1;