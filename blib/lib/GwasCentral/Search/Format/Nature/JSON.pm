package GwasCentral::Search::Format::Nature::JSON;
use Moose::Role;
has 'json' => ('is'=>'ro','builder'=>'_build_json');
use JSON::XS;
sub _build_json {
	my ($self) = @_;
	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(0);
	return $json_coder;
}
1;