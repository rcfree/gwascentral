package GwasCentral::Search::Format::Nature::XML;
use Moose::Role;
has 'xml' => ('is'=>'rw','builder'=>'_build_xml', 'lazy' => 1);
has 'xml_root' => ('is'=>'rw','default'=>'result');

use XML::Simple;

sub _build_xml {
	my ( $self, $args ) = @_;
	my $xml = new XML::Simple( NoAttr => 1, RootName => $self->xml_root )
	  or die "unable to create XML::Simple";
	$self->xml($xml);
}

sub to_xml {
	my ($self, $data) = @_;
	return $self->xml->XMLout($data);
}

1;