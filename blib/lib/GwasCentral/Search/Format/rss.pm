package GwasCentral::Search::Format::rss;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::Feed);
has '+content_type' => ('default'=>'application/rss');
has '+is_download' => ('default'=>0);
use XML::RSS;

sub export {
	my ($self) = @_;
	# create an RSS 1.0 file (http://purl.org/rss/1.0/)
	my $rss = XML::RSS->new( version => '1.0' );
	my @categories = @{ $self->_get_category_info() || {} };
	my $category_list = join( " and ", @categories );
	$rss->channel(
		title => "GWAS Central search results (" . $category_list . ")",
		link  => $self->base_url.$self->Query->url_path,
		description =>
		  "GWAS Central database of genotype-to-phenotype information",
		dc => {
			subject   => "Search results ",
			creator   => 'gwascentral',
			publisher => 'gwascentral',
			rights    => 'Copyright 2010, GWAS Central',
			language  => 'en-uk',
		},
		syn => {
			updatePeriod    => "hourly",
			updateFrequency => "1",
			updateBase      => "1901-01-01T00:00+00:00",
		},
	);

	foreach my $item ( @{ $self->Query->results } ) {
		$rss->add_item(
			title => $item->{title},
			link  => $item->{link},
			description =>
			  $self->_generate_html_for_summary( $item->{summary} ),
			dc => {
				subject => "GWAS Central search result",
				creator => "gwascentral.org",
			},
		);
	}

	return $rss->as_string;
}
1;