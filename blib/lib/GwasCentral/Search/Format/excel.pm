package GwasCentral::Search::Format::excel;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::Delimited);
has '+content_type' => ('default'=>'application/vnd.ms-excel');
use Spreadsheet::WriteExcel;

sub fileext { 'xls' }
sub export {
	my ( $self, $query, $separator ) = @_;

	my ( $fieldhash, $data ) = $self->_get_feed_as_data;
	my %fieldhash = %{$fieldhash};
	my @data      = @{$data};

	# Create a new Excel workbook
	open my $fh, '>', \my $content or die "Failed to open Excel filehandle: $!";
	my $workbook = Spreadsheet::WriteExcel->new($fh);

	# Add a worksheet
	my $worksheet = $workbook->add_worksheet();

	my @fields = sort keys %fieldhash;

	my $col = 0;
	foreach my $field (@fields) {
		$worksheet->write( 0, $col, $field );
		$col++;
	}

	my $row = 1;
	$col = 0;

	foreach my $datum (@data) {
		my @line = ();
		foreach my $field (@fields) {
			$worksheet->write( $row, $col, $datum->{$field} );
			$col++;
		}
		$row++;
		$col = 0;
	}

	$workbook->close;
	binmode STDOUT;
	return $content;
}
1;