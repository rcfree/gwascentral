package GwasCentral::Search::Format::html;
use Moose;
extends qw(GwasCentral::Search::Format);
has '+content_type' => ('default'=>'application/x-www-form-urlencoded');
has '+is_html' => ('default' => 1);

1;