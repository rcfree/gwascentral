# $Id: Query.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

GwasCentral::Search::Query - Base class for Query modules, containing commonly used methods.


=head1 SYNOPSIS

    package GwasCentral::Search::Query::Example;
    use Moose;
    extends qw(GwasCentral::Search::Query);
    
    #override results method
    sub results {
    	my @results = ....
    	return \@results;
    }
    
    OR use as the base
    
    package GwasCentral::


=head1 DESCRIPTION

This class is a wrapper class for different types of searches. As the modules are based on Moose you can use 'before','after' and 'around' decoration upon all methods.

The main method is 'results', which prepares the 'Filter' modules specified in 'possible_filters' (prepare_filters method) and then performs the search.
Filter or Detect specific logic is contained in their respective modules (e.g. 

The type of search specified in 'performer_name' will be performed. This corresponds to a 'Performer' module name.
ATM there are the following types of search performers available:

SimpleDetect
This attempts to recognises the entire string by looping through the supplied 'Detector' modules specified in 'possible_detectors'. 
Once it detects a match, it retrieves the data using the corresponding 'Retriever' module for the search type
(e.g. if it detects an ontology annotation in the Query::Phenotype module it will use the 'PhenotypeAnnotation' module to retrieve the data
and populate the pager.

Accessors containing database modules, and the AccessControl module are also provided.

The conversion of data to a structure more akin to a feed is carried out by 'convert_data_to_entries'

The formatting to text or RSS etc. is carried out by the Search::Aggregator module.

See <L:GwasCentral::Search::Query::Studies> for an example

TODO UPDATE

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query;

use Moose;
use FindBin;
use lib "$FindBin::Bin/../../";

use Tie::Hash::Indexed;
use Carp qw(confess);
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);

has 'none_is_none' => ('is'=>'rw');
has 'total'  => ( 'is' => 'rw' );
has 'message' => ( 'is' => 'rw' );                             #message returned
has 'is_slave' => ( 'is' => 'rw' )
  ;    #flag specifying if Query module is slave of another

has 'no_results' => ( 'is' => 'rw', 'default' => undef );
has 'no_options' => ('is' => 'rw', 'default' => undef );
has 'url_path' => ('is'=>'rw');
has 'DetectType'=>('is'=>'rw');
has 'possible_DetectTypes' => ('is'=>'rw', default=>sub { [] }) ;
has 'Detector_name' => ('is'=>'rw');
has 'Filter_options' => ('is'=>'rw','default' => sub {{}});
has 'lock_DetectType'=>('is'=>'rw','default'=>undef, 'trigger'=>\&_lock_DetectType);

has 'fieldset_order' => ('is'=>'rw');
has 'DataSource_name' => ('is'=>'rw');

has 'Filters'=>('is'=>'rw', 'default' => sub { tie my %hash,"Tie::Hash::Indexed"; return \%hash; }, 'required'=>1); 
has 'params' => ('is'=>'rw', 'default' => sub { {} });
has '_pager' => ( 'is' => 'rw' );    #private accessor for pager object
has 'recognised_as' => ( 'is' => 'rw' )
  ;    #defines what query has been recognised as
has 'data' => ( 'is' => 'rw', 'default' => sub { [] } )
  ;    #arrayref of data obtained prior to conversion to 'entries'
has 'Page_filter' => ('is'=>'rw','default' => 'Page');
has 'PageSize_filter' => ('is'=>'rw','default' => 'PageSize');
has 'Format_name' => ('is'=>'rw','default' => 'ExportFormat');
has 'Format_filter' => ('is'=>'rw');
has 'Format' => ('is'=>'rw');
has 'possible_Filters' => ('is'=>'rw', 'required'=>1);
has 'jsonp_template' => ('is'=>'rw');
has 'no_Format' => ('is'=>'rw','default'=>undef);
has 'content_type' => ('is'=>'rw', 'trigger' => \&_trigger_ct);
has 'no_search_if_html' => ('is'=>'rw', 'default'=>undef);
has 'host' => ('is'=>'rw');
has 'strip' => ('is'=>'rw','default' => sub { HTML::Strip->new() });
has 'is_report' => ('is'=>'rw','default'=>undef);
use Text::ParseWords;

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Cwd;
use Digest::MD5 qw(md5);

use Env;
use Data::Pageset;
use Carp qw(cluck);
use Moose;
use Storable qw(dclone nfreeze);
use CHI;
use GwasCentral::Base qw(load_module);
use HTML::Strip;

sub BUILD {
	my ($self) = @_;
	$self->init;
}

sub _trigger_ct {
	my ($self) = @_;	
	$self->Format($self->new_Format);
}

sub text {
	my ($self) = @_;
	return $self->label;
}
 
sub text_as_plain {
	my ($self) = @_;
	return $self->strip->parse($self->text);
}

sub label {
	my ($self) = @_;
	my $dbconf = $self->dbconfig;
	my $total = $self->result_count;
	return "<b>".$total."</b> ".($total == 1 ? $dbconf->{display_single} : $dbconf->{display_plural});
}

sub result_count {
	my ($self) = @_;
	my $total;
	if ($self->pager) {
		$total = $self->pager->total_entries;
	}
	else {
		$total = "0";
	}
	return $total;
}

sub init {
	my ($self)=@_;
	tie my %filters, 'Tie::Hash::Indexed';
	
	foreach my $filter(@{$self->possible_Filters}) {
		my $filter_module = $self->new_Filter($filter,{conf_file=>$self->config});
		$filters{$filter} = $filter_module;
	}
	$self->Filters(\%filters);
}

sub add_Filter {
	my ($self, $Filter_name) = @_;
	
	my $poss = $self->possible_Filters;
	push @{$poss}, $Filter_name;
	$self->log->info("filts:".Dumper($poss));
	$self->possible_Filters($poss);
	my $filter_module = $self->new_Filter( $Filter_name,{conf_file=>$self->config});
	$self->Filters->{$Filter_name} = $filter_module;
}

sub clone {
	my ($self) = @_;
	my $clone = dclone($self);
	my $filters = dclone($self->filters);
	$clone->filters($filters);
	return $clone;
}

sub _lock_DetectType { 
	my ($self) = shift;
	$self->log->info("DetectType locked on ".$self->DetectType);
};


sub url {
	my ($self) = @_;
	return $self->url_with({});
}

sub url_with {
	my ($self,$params) = @_;
	my $url = "/".$self->url_path;
	my @params = ();
	foreach my $filter(values %{$self->Filters}) {
		next if $filter->ignore_in_url;
		my ($key,$value) = ($filter->retrieve_from, $filter->value);
		next if !$key || length($key)==0;
		$value = $params->{$key} if $params->{$key};

		if (ref($value) eq 'ARRAY') {
			push @params, map { "$key=$_" } @{$value};
		}
		else {
			my $default = $filter->default;
			$self->log->info("default is ".($default || 'undef').", value is $value");
			
			if ($default) {
				if ($value ne $default) {
					push @params, "$key=$value" if $value;
				}
			}
			else {
				push @params, "$key=$value" if $value;
			}
		}
	}
	$url.=scalar(@params)>0 ? "?".join("&",@params) : "";
	return $url;
}


sub convert_data_to_entries {
}

	sub reset {
		my ($self) = @_;
		$self->recognised_as(undef);
		$self->data(undef);
		$self->has_no_results(undef);
		$self->params(undef);
		$self->DetectType(undef);
		$self->Filters({});
		$self->init;
	}
			
	sub results {
		my ($self, $params,$do_prepare) = @_;

		if ($do_prepare) {
			$self->prepare_Filters($params);
		}
		my $d_mod = $self->new_Detector($self->Detector_name);
		my $data = $d_mod->perform;
		
		my @results = ();
		!$data and $data = [];
		if (ref($data) eq 'ARRAY') {
			if ( scalar(@{$data}) == 0) {
				$self->no_results(1);
			}
			else {
				$self->convert_data_to_entries($data);
			}
		}
		$self->data($data);

		return $data;
	}
	
	sub structured_data {
		my ($self) = @_;
		my $data = $self->data;
		if (ref($data) eq 'HASH') {
			return $data->{item}->to_hash;
		}
		else {
			return $data;
		}
	}
	
sub clone_query_state {
	my ( $self, $state ) = @_;
	$self->databases( $state->databases );
	$self->filters( $state->filters );
	$self->page( $state->page );
	$self->page_size( $state->page_size );
	$self->recognised_as( $state->recognised_as );
	$self->recognition($state->recognition);
	$self->access_control( $state->access_control );
	$self->no_sort($state->no_sort);
}

sub get_current_page {
	my ($self) = @_;
	my @data   = @{ $self->data };
	my $count  = scalar(@data);
	my $start  = ( $self->page - 1 ) * $self->page_size;
	my $end    = $start + $self->page_size - 1;
	$start + $self->page_size > $count and $end = $count - 1;
	return @data[ $start .. $end ];
}

sub get_page_of_data {
	my ( $self, $data_ref ) = @_;
	my @data      = @{$data_ref};
	my $page      = $self->pager->current_page;
	my $page_size = $self->pager->entries_per_page;
	my $start     = ( $page - 1 ) * $page_size;
	my $end       = $start + $page_size - 1;
	if ( $end > scalar(@data) - 1 ) {
		$end = scalar(@data) - 1;
	}

	my @slice = @data[ $start .. $end ];
	return \@slice;
}



sub zero_pager {
	my ($self) = @_;
	$self->new_pager(0);
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name =~ /GwasCentral::Search::Query::(.+)/;
	return $1;
}

sub format_Filter {
	my ($self) = @_;
	return $self->f($self->Format_name);
}
	
	sub build_equiv_query {
		my ($self, $name, $params) = @_;
		my $query = $self->load_module("GwasCentral::Search::Query::".$name,{conf_file=>$self->config});
		$query->databases($self->databases);
		$query->params($self->params);
		
		$query->detector($self->detector);
		$query->recognised_as($self->recognised_as);
		my %filters = %{$self->filters};
		map { $_->query(undef) } values %filters;
		my $VAR1;
		eval Dumper(\%filters);
		map { $_->query($query) } values %{$VAR1};
		$query->filters($VAR1);
		return $query;
	}

sub get_data {
	my ($self) = @_;
	my $ds = $self->DataSources->{$self->DataSource_name};
	my $get_data_method = $self->get_data_method;
	
    my ($results,$pager) = $ds->$get_data_method({Query=>$self});
    if ($pager) {
    	$self->pager($pager);
    }
    else {
    	$self->new_pager('0');
    }
	return $results;
}

sub dbconfig {
	my ($self) = @_;
	return $self->config->{Search}->{Query}->{$self->name};
}

sub prepare_Filters {
	my ($self,$params) = @_;
	$params ||= $self->params;
	
	
	foreach my $filter(@{$self->possible_Filters}) {
		my $filter_module = $self->f($filter);
		my $curr_filter_opts = $self->Filter_options->{$filter};
		if ($curr_filter_opts) {
			foreach my $option(keys %{$curr_filter_opts}) {
				
				my $value = $curr_filter_opts->{$option};
				if (ref($value) eq 'HASH') {
					tie my %filter_options, 'Tie::Hash::Indexed';
					%filter_options = %{$curr_filter_opts->{$option}};
					$filter_module->$option(\%filter_options);
				}
				else {
					if ($value && $value =~ /&(.+)/) {
						my $temp_value = $self->$1;
						$self->log->info("set $filter_module -> $option ( $temp_value )");
						$filter_module->$option($temp_value);
					}
					else {
						$self->log->info("set $filter_module -> $option ( $value )");
						$filter_module->$option($value);
					}
				}
			}
		}
		
		$filter_module->populate($params);			
		
	}
	if (!$self->no_Format) {
		my $format = $self->new_Format;
		$self->Format($format);
	}
}

sub params_from_Filters {
	my ($self, $as_querystring, $ignore_filters) = @_;
	my %params = ();
	my $filters = $self->Filters;
	my %ignore_filters = map { $_ => 1 } @{$ignore_filters || []};
	foreach my $filter(values %{$filters}) {
		next if $filter->does('GwasCentral::Search::Filter::Nature::None');
		next if $ignore_filters{$filter->name};
		if ($filter->value) {
			if ($filter->retrieve_from) { $params{$filter->retrieve_from}=$filter->value };	
		}
	}
	
	return \%params if !$as_querystring;
	my @param_list;
	foreach my $key(keys %params) {
		if (ref($params{$key}) eq 'ARRAY'){
			foreach my $value(@{$params{$key}}) {
				push @param_list, $key."=".$value;
			}
		}
		else {
			push @param_list, $key."=".$params{$key};
		}
	}
	return join("&",@param_list);
}


sub filter_value {
	my ($self, $name, $value) = @_;
	my $filter = $self->Filters->{$name};
	$self->throw("Filter '$name' does not exist in 'filters' list") if !$filter;
	
	if (defined($value)) {
		$filter->value($value);
	}
	return $filter->value;
}

sub fval {
	my $self = shift;
	$self->filter_value(@_);
}

sub f {
	my $self = shift;
	my @filters = map { $self->Filters->{$_} || $self->throw("Filter '$_' not present in Query $self") } @_;
	return wantarray ? @filters : $filters[0];
}

sub has_f {
	my $self = shift;
	my @filters = map { $self->Filters->{$_} and return 1 } @_;
	return undef;
}

sub list_value {
	my ($self, $name) = @_;
	my $filter = $self->Filters->{$name};
	$self->throw("Filter '$name' does not exist in 'filters' list") if !$filter;
	
	if ($filter->keys_as_values) {
		my %list = map { $filter->list->{$_} => $_ } keys %{$filter->list};
		#$self->log->info("valued keys:".Dumper(\%list));
		return $list{$filter->value};
	}
	else {
		return $filter->list->{$filter->value};
	}
}

sub page {
	my ($self,$value) = @_;
	return $self->fval($self->Page_filter, $value);
}

sub page_number {
	my ($self,$value) = @_;
	return $self->f($self->Page_filter)->number;
}

sub page_start_row {
	my ($self,$value) = @_;
	return $self->f($self->Page_filter)->start_row;
}

sub page_size {
	my ($self,$value) = @_;
	return $self->fval($self->PageSize_filter, $value);
}

sub new_pager {
	my ( $self, $results ) = @_;
	my $pager = Data::Page->new;
	
	if ( ref($results) eq 'ARRAY' ) {
		$pager->total_entries( scalar( @{$results} ) );
	}
	else {
		$pager->total_entries($results);
	}

	if ( $self->page_size eq 'all' ) {
		if ( $pager->total_entries > 0 ) {
			$pager->entries_per_page( $pager->total_entries );
		}
	}
	else {
		$pager->entries_per_page( $self->page_size );
	}
	
	if ($self->page>$pager->last_page) {
		$self->page(1);
		$pager->current_page(1);
	}
	else {
		$pager->current_page( $self->page);
	}
	
	$self->pager($pager);
	return $pager;
}

sub new_DetectType {
	my ($self, $name)=@_;
	my $dt = $self->load_module("GwasCentral::Search::DetectType::$name");
	$dt->Query($self);
	return $dt;
}

sub new_Filter {
	my ($self, $name, $args)=@_;
	my $filter = $self->load_module("GwasCentral::Search::Filter::$name", $args);
	$filter->Query($self);
	return $filter;
}

sub new_Detector {
	my ($self, $name)=@_;
	my $d = $self->load_module("GwasCentral::Search::Detector::$name");
	$d->Query($self);
	return $d;
}

sub new_Format {
	my ($self, $name)=@_;
	my $format_val;
	my $ef = $self->f($self->Format_name);
	if ($name) {
		$format_val = $name;
	}
	else {
		$format_val = $ef->value;
		my $ct = $self->content_type;
		if ($ct) {
			my @list = @{$ef->list};
			my @module_names = map { $_->[0] } @list;
			my %content_types = map { my $mod = load_module("GwasCentral::Search::Format::".$_,{conf_file=>$self->config}); $mod->content_type => [ $_, $mod] } @module_names; 
			$self->throw("Content-type $ct not available in Query") if !$content_types{$ct};
			$format_val = $content_types{$ct}->[0];
		}
	}
	my $d = $self->load_module("GwasCentral::Search::Format::$format_val",{conf_file=>$self->config});
	$d->Query($self);
	return $d;
}

=head2 pager

  Usage      : my $pager = Data::Page->new();
  			   $pager->total_entries(100);
  			   $self->pager($pager);
  Purpose    : Get the _pager accessor, or set it (create a Data::Pageset object from given object)
  Returns    : A Data::Pageset object
  Arguments  : None
  Throws     : 
  Status     : Public
  Comments   : 

=cut

sub pager {
	my ( $self, $pager ) = @_;
	!$pager and return $self->_pager;
	
	my $pageset;

	if (!defined($pager)) {
		$self->throw("No pager object provided");
	}
	if (ref($pager) !~ /^Data::/) {
		$self->log->info("page_size:".$self->page_size.", page:".$self->page);
		$pageset = Data::Pageset->new(
		{
			'total_entries'    => $pager,
			'entries_per_page' => $self->page_size eq 'all' ? $pager : $self->page_size,
			'current_page'     => $self->page,
			'pages_per_set'    => 10,
			'mode'             => 'fixed',
		}
	);
	}
	else {
		$pageset = Data::Pageset->new(
			{
				'total_entries'    => $pager->total_entries,
				'entries_per_page' => $pager->entries_per_page,
				'current_page'     => $pager->current_page,
				'pages_per_set'    => 10,
				'mode'             => 'fixed',
			}
		);
	}
	$self->_pager($pageset);
	return $self->_pager;
}



sub output {
	my ( $self ) = @_;
	#use Format module to output data
	return $self->Format->export;
}

sub data_row_to_jsonp {
	my ($self,$row) = @_;
	my $tmpl = $self->jsonp_template;
	my $tt = $self->tt;
	
	my $vars = { 
		row => $row,
		query => $self,
	};
	my $html="";
	#process into $html variable rather than STDOUT
	$tt->process($tmpl.".tt2", $vars,\$html) || $self->throw("Error while processing template $tmpl:".$tt->error());
	return [split(/\{end\}/,$html)];
}
	

sub shorten_author_list {
	my ( $self, $author_list, $max_length ) = @_;
	$max_length ||= 3;
	$author_list =~ s/\n//;

	#warn "start:".time;
	my @authors = split( ", ", $author_list );

	#warn "end:".time;
	if ( scalar(@authors) > $max_length ) {
		return
		  join( ", ", @authors[ 0 .. $max_length - 1 ] )
		  . '&nbsp;<i>et al.</i>';
	}
	else {
		return join( ", ", @authors );
	}
}


sub update_params {
	my ( $self, $new_hashref ) = @_;

	foreach my $new_key ( keys %{$new_hashref} ) {
		if (length($new_hashref->{$new_key})>0) {
			$self->params->{$new_key} = $new_hashref->{$new_key};
		}
	}
}

sub exclude_params {
	my ( $self, $ignore_params_ref ) = @_;

	my %ignore_params = map { $_ => 1 } @{ $ignore_params_ref || [] };
	my %sel_params = ();
	foreach my $param ( keys %{ $self->params } ) {
		next if $ignore_params{$param};
		$sel_params{$param} = $self->params->{$param};
	}
	return \%sel_params;
}

sub include_params {
	my ( $self, $include_params_ref ) = @_;

	my %include_params = map { $_ => 1 } @{ $include_params_ref || [] };
	my %sel_params = ();
	foreach my $param ( keys %{ $self->params } ) {
		next if !$include_params{$param};
		$sel_params{$param} = $self->params->{$param};
	}
	return \%sel_params;
}
	
1;

=head1 SEE ALSO

L<GwasCentral::Search::Query::Studies>, L<GwasCentral::Search::Query::Markers>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Query.pm 1560 2010-10-28 15:55:59Z rcf8 $ 

=cut

