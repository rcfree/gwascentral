# $Id: Search.pm 1547 2010-09-27 14:13:27Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Util - Basic functions for GwasCentral Search API


=head1 SYNOPSIS
	
	use GwasCentral::Search;
	 ...
	 ...
	#Catalyst action for searching studies..
   
    my $search = GwasCentral::Search->new( { conf_file => $c->config, context => $c } );
	
	my ( $results, $not_html );
	eval {
	 ( $results, $not_html ) =
	  $search->search_single( 'Studies', { 'v'=>'table' } );
	};
	if ($@) {
		$c->stash->{error}=$@;
		return;
	}
	
	#place output into the 'body' of the response unless HTML, then put it into the stash as {search}->{[query_type]}
	if ($not_html) {
		$c->res->body($results);
	}
	else {
		$c->stash->{search}->{'Studies'} = $results;
	}


=head1 DESCRIPTION

This class is a factory for the creation of Query objects (see Query.pm) for more details.
It also creates instances of other objects (which are likely to be superseded by development of V4).
It also includes utility methods for carrying out searches and returning the results to Catalyst (via the context which is passed in)

The Search API is based around Query modules. Each Query module represents a single type of search, with the exception of
Query::All which wraps the Studies, Phenotypes and Markers modules (this is used by the 'overall' search).

The key method in Query modules is 'results'. Unless over-ridden, this module will look for identifiers in $query->identifiers.
If it finds them it will use the method 'results_from_identifiers', otherwise it will use the 'results_from_database'

Query-derived modules inherit a number of methods common to all searches. These include:
 page - the current page of results
 page_size - the number of results per page
 pager - return a Data::Pageset object for simple page display in the HTML template
 options - a hash ref of key/value search properties. The most important is 'q' which contains the query
 option - retrieve/set a search property

Modules can also override default values for the following which are used internally.
Exceptions are thrown (and caught by the Web modules) if someone attempts to use a page size which is not returned by allowed_page_sizes for instance.

default_page_size - returns a single number (default 50) 
default_view - not really used anymore - views have been removed (default 'table')
use_multiple_terms - provides (shaky) support for multiple search terms
allowed_page_sizes - an array of page sizes allowed by the system (default qw(20 50 100) );
allowed_marker_ranges - a hash ref of list labels with their count ranges (default {
		'1-1,000' => [0,1000],
		'1,000-10,000' => [1001,10000],
		'10,000-100,000' => [10001,100000],
		'>100,000' => [100001,10000000],
		'All' => [0,10000000]
})

marker_range_order - the order of marker_ranges on the web-page default ('1-1,000','1,000-10,000','10,000-100,000','>100,000', 'All');
default_marker_range - the initial marker range to use (default 'All')
default_sort_field - the initial sort field to use
available_sortfields - a hashref of the possible sort fields with associated SQL
sortfield_order - a list of the order of the sort fields

The Query module has become a little convoluted, so needs to be streamlined. This is particularly the case for complex
AND/OR/NOT queries, which it will not deal with yet.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Util;
use base qw(Exporter);
use strict;
use warnings;

use Moose;

use Text::ParseWords;

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use FindBin;
use lib "$FindBin::Bin/../../";
use Cwd;
use GwasCentral::Base qw(load_module load_config);
use GwasCentral::DataSource::Util
  qw(DataSources_from_context DataSources_from_models DataSources_from_config new_AccessControl);

use Env;
our @EXPORT_OK = qw(new_Query Query_from_config new_Federate);

=head2 new_query

  Usage      : my $query = $search->new_query('Studies',{ q=>'cancer' });
  });
  Purpose    : Create a new Search::Query object for the specified query module
  Returns    : Subclass of HGVbaseG2P::Search::Query
  Arguments  : Query type (same name as module eg. Studies)
  Throws     : 
  Status     : Public

=cut

sub new_Query {
	my ( $q_module, $args ) = @_;
	my $conf_arg = { conf_file => $args->{conf_file} };
	
	my $config;
	
	if ( $args->{context} ) {
		$config = $args->{context}->config;
	}
	else {
		$config = { load_config($args->{conf_file}) };
	}
	
	$args->{ conf_file } = $config;
	
	if ($args->{AccessControl_from_config}) {
		$args->{AccessControl} = new_AccessControl($config);
	}
	
	my $ac = $args->{AccessControl};
	my $q = load_module( "GwasCentral::Search::Query::$q_module", $args );

	if ( $args->{models} ) {
		$args->{DataSources} = DataSources_from_models( $args->{models} );
	}

	if ( $args->{context} ) {
		$args->{DataSources} = DataSources_from_context( $args->{context} );
		$q->content_type($args->{context}->req->content_type);
		$q->params($args->{context}->req->params);
	}
	
	if ($args->{DataSources_from_config}) {
		$args->{DataSources} = DataSources_from_config($config, $args->{AccessControl});
	}
	
	#set databases, AccessControl and parameters in the Query object
	if ( $args->{DataSources} ) {
		$q->DataSources( $args->{DataSources} );
	}
	else {
		warn "No DataSources initialised in Query";
	}

	return $q;
}

sub new_Federate {
	my ( $q_module, $args ) = @_;

	my $conf_arg = { conf_file => $args->{conf_file} };
	my $context = $args->{context};
	if ( $context ) {
		$conf_arg = { conf_file => $context->config };
	}

	$args->{Query_name}=$q_module;
	my $ac = $args->{AccessControl};
	my $q = load_module( "GwasCentral::Search::Federate", $args );

	if ( $args->{models} ) {
		$args->{DataSources} = DataSources_from_models( $args->{models} );
	}
	my $connect = $args->{Connect};
	if ( $context ) {
		$args->{DataSources} = DataSources_from_context( $args->{context} );

		$q->content_type($context->req->content_type);
		$q->params($args->{params} || $context->req->params);
		$q->Connect($context->stash->{Connect});
	}
	
	if ($args->{DataSources_from_config}) {
		$args->{DataSources} = DataSources_from_config($q->config, $args->{AccessControl}, $connect);
	}
	
	#set databases, AccessControl and parameters in the Query object
	if ( $args->{DataSources} ) {
		$q->DataSources( $args->{DataSources} );
	}
	else {
		warn "No DataSources initialised in Query";
	}

	return $q;
}

sub Query_from_config {
	my ($args) = @_;
	my $conf_file = $args->{conf_file};
	my $query_name = $args->{name};
	my $connect = GwasCentral::Connect->new({conf_file=>$conf_file, local_url => $args->{local_url}});
	my $ac = load_module("GwasCentral::AccessControl::".$connect->config->{control_module},{conf_file=>$conf_file});
	
	$connect->db( HGVbaseG2P::DataSource::Connect->new({conf_file=>$conf_file}));
	$ac->identity($args->{identity});
	$ac->Connect($connect);

	my $ds = DataSources_from_config($conf_file,$ac,$connect);
	
	my $q = new_Query($query_name,{conf_file=>$conf_file, DataSources => $ds , Connect => $connect, content_type => $args->{content_type} });
	return $q;
}

=head2 search_single

  Usage      :  my ($search_results, $not_html) = $search->search_single('Studies',{ v=>'table' });
  				!$not_html and $c->stash->{Studies}=$search_results;
  });
  Purpose    : Carry out a search using a specific Search::Query type and deal with access control, paging and added studies etc
  Returns    : If requested content-type is text/html, it returns a hash of content, otherwise it uses Search::Aggregator/Search::DataFeed objects to produce output in the required format.
  Arguments  : Type of query (name of Search::Query subclass), extra parameters which are separate from $c->req->params
  Throws     : HGVbaseG2P::Exception if format or content type not recognised
  Status     : Public

=cut

sub search_single {
	my ( $self, $args ) = @_;

	my $qry = $args->{query} ? $args->{query} : $self->prepare_search($args);

  #set up a Search object and use the Query to get a populated Aggregator object
	my $search = HGVbaseG2P::Search->new( { conf_file => $self->config } );
	
	my $type = $args->{type};

	#return ( $filename, $qry );
}

=head2 search_single

  Usage      :  my ($search_results, $not_html) = $search->search_single('Studies',{ v=>'table' });
  				!$not_html and $c->stash->{Studies}=$search_results;
  });
  Purpose    : Carry out a search using a specific Search::Query type and deal with access control, paging and added studies etc
  Returns    : If requested content-type is text/html, it returns a hash of content, otherwise it uses Search::Aggregator/Search::DataFeed objects to produce output in the required format.
  Arguments  : Type of query (name of Search::Query subclass), extra parameters which are separate from $c->req->params
  Throws     : HGVbaseG2P::Exception if format or content type not recognised
  Status     : Public

=cut

sub prepare_search {
	my ( $self, $args ) = @_;

#Set up a search Query module and an AccessControl module
#The query type can be All, Studies, Markers, MarkerResults, MarkerFrequencies, Phenotypes, ResultsetMarkers and RegionMarkers.
	my $qry = $self->new_query( $args->{type} );
	$qry->params( $args->{params} );

	$qry->prepare_filters;

	#put content type into query
	my $content_type = $qry->content_type( $args->{content_type} );

	#use format from query which deals with validation etc.
	my $format = $qry->format;

	return $qry;
}

=head2 search_combined

  Usage      :  my ($search_results, $not_html) = $search->search_combined('Studies');
  Purpose    : Carry out a search using the Search::Query::All type which combines searches of Studies, Phenotypes and Markers.
  Returns    : Returns a hash of content for all searches. Does not provide content for types others than text/html.
  Arguments  : Current query (name of Search::Query subclass) (ie. the currently displayed tab in the search page)
  Throws     : 
  Status     : Public

=cut

sub search_combined {
	my ( $self, $current_query ) = @_;

	#Set up a search Query module and an AccessControl module
	my $qry    = $self->new_query('All');
	my $params = $self->context->req->params;

	#set databases, AccessControl and parameters in the Query object
	$qry->get_databases_from_catalyst_context( $self->context );
	my $access_control = $self->context->get_access_control;
	$qry->access_control($access_control);

	#prepare Query options, paging and 'current' displayed query
	my $page = $self->context->req->param('page') || 1;
	my $page_size = $self->context->req->param('page_size')
	  || $qry->default_page_size;
	$qry->extract_options($params);
	$qry->page($page);
	$qry->page_size($page_size);

	my $threshold = $qry->option('t');

	if ( $threshold and $threshold ne 'ZERO' ) {
		die HGVbaseG2P::Exception->new(
			{ message => "Threshold '$threshold' not recognised" } )
		  if $threshold * 1 ne $threshold;
		die HGVbaseG2P::Exception->new(
			{
				message => "Threshold must be less than "
				  . $self->config->{max_threshold}
			}
		) if $threshold > $self->config->{max_threshold};
	}

	if ( $qry->option('db') ) {
		$current_query = $qry->option('db');
		$qry->goto_report(1);
	}
	$qry->current( ucfirst($current_query) );

	my $compiled_results = {};
	my @server_error     = ();

	my $query_results = $qry->results;

	#for each Query within Search::Query::All
	foreach my $db (qw(Studies Phenotypes Markers)) {

		#prepare populated aggregator for database
		my $ag =
		  $self->prepare_aggregator_using_results( $query_results->{$db},
			'html' );

#include outputted results in results hash (along with dbconfig and other useful data)
		my ( $output, $template ) = $ag->output;
		my $lc_db = lc($db) . "_query";
		$compiled_results->{$db} = {
			content      => $output,
			server_error => \@server_error,
			query        => $qry->$lc_db,
			dbconfig     => $self->config->{'Search'}->{'Query'}->{$db}
		};
	}

	if ( $qry->option('db') ) {
		my $dbresults = $compiled_results->{ $qry->current };
		my $pager     = $dbresults->{query}->pager;
		$pager->total_entries == 1
		  and
		  $qry->goto_report( $query_results->{ $qry->current }->[0]->{link} );
	}

	$compiled_results->{all} = $qry;

	#return combined set of results
	return $compiled_results;
}



1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Search.pm 1547 2010-09-27 14:13:27Z rcf8 $ 

=cut

