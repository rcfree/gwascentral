# $Id$

=head1 NAME

GwasCentral::Search::Filter::QueryString


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::QueryString;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Text
GwasCentral::Search::Filter::Nature::GenomicRegion
);

has '+retrieve_from' => ('default'=>'q');
has '+label'=>('default'=>'Search For');
has '+fieldset'=>('default'=>'options');
has '+autocomplete_url'=>('default'=>'/search/lookup');
has 'autocomplete_width'=>('is'=>'rw','default'=>undef);
has 'width'=>('is'=>'rw','default'=>'23em');
has 'size'=>('is'=>'rw','default'=>'40');
has 'legend'=>('is'=>'rw');
has 'markers' => ('is'=>'rw');
has '+is_category' =>('default'=>1);
1;
