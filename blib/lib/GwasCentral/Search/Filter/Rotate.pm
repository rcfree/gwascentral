# $Id$

=head1 NAME

GwasCentral::Search::Filter::RestrictToAnnotation


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Rotate;
use Moose;
use GwasCentral::Base qw(load_module);

extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Checkbox);

has '+retrieve_from' => ('default'=>'rotate_multiple');
has 'value' => ('is'=>'rw');
has '+fieldset' => ('default'=>'more_options');
has '+label' => ('default'=>'Horizontal chromosomes');

1;
