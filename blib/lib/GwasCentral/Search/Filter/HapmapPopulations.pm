# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::HapmapPopulations


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::HapmapPopulations;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::CheckList);
has '+fieldset' => ('default'=>'hapmap');
has '+label' =>('default'=>'Populations');
has '+number_of_columns' => ('default'=>undef);
has '+label_above'=>('default'=>1);

sub _available_checklist {
	return [
		['CEU',"CEU (CEPH individuals from Utah, USA)"],
		['CHB','CHB (Han Chinese in Beijing, China)'],
		['JPT','JPT (Japanese in Tokyo, Japan)'],
		['YRI','YRI (Yoruba in Ibadan, Nigeria)'],
	];
}

sub _default_checklist {
	return [
		'CEU',
	];
}

sub gbrowse_qstring {
	my ($self) = @_;
	my @qstring = ();
	my $rt = $self->Query->f('RegionTracks');
	if ($rt->is_on('HapMapLDPlot')) {
		foreach my $t(keys %{$self->retrieve_from}) {
			if ($self->is_on($t)) {
				push @qstring, "PairplotPhase3Annotator.".$t."=1";
			}
			else {
				push @qstring, "PairplotPhase3Annotator.".$t."=0";
			}
		}
		return @qstring;
	}
	else {
		return undef;
	}
}
1;
