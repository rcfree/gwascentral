# $Id$

=head1 NAME

GwasCentral::Search::Filter::MarkerDisplay


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::MarkerDisplay;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'l');
has '+fieldset' => ('default'=>'options');
has '+value' => ('default'=>'asd');
has '+label' => ('default'=>'Markers displayed');

sub _list_builder { 	
	[
		['all','All markers'],
		['asd','Only with association data'],
	];
};
1;
