# $Id$

=head1 NAME

GwasCentral::Search::Filter::ExportFormat


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::ExportFormat;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'format');
has '+value' => ('default'=>'html');
has '+label' => ('default'=>'Export these results as');
has '+auto_submit' => ('default'=>1);
has '+revert_to' => ('default'=>"html");
has '+fieldset'=>('default'=>'result_opts');
has '+hide_on_none' => ('default'=>1);
tie my %list, 'Tie::Hash::Indexed';

sub _list_builder { 
	return [
		['html'=>'--choose a format --'],
		['excel'=>'Microsoft Excel'],
		['csv'=>'Comma-separated file'],
	    ['tsv'=>'Tab-separated file'],
	    ['ssv'=>'Space separated file'],
		['rss'=>'RSS feed'],
		['atom'=>'Atom feed'],
		['json'=>'JSON file'],
	];
}

no Moose;

1;
