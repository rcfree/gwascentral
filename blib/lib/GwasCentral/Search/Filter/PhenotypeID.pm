# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::PhenotypeID;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Ignore);

has '+retrieve_from' => ('default'=>'pid');
has '+fieldset' => ('default'=>'options');
has '+ignore_in_url' => ('default'=>1);
1;
