# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::HapmapLDProperty;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'ld_property');
has '+value'=>('default'=>'lod');
has '+fieldset'=>('default'=>'hapmap');
has '+label' =>('default'=>'Show LD where');

sub _list_builder { 
	return [
		['lod','lod'],
		['rsquare','rsquare'],
		['dprime','dprime'],
	];
};

1;
