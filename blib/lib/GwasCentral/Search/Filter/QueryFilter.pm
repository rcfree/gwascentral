# $Id$

=head1 NAME

GwasCentral::Search::Filter::QueryFilter


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::QueryFilter;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'qfilter');
has '+list'=>('lazy'=>1);
has '+template' => ('default'=>'qfilter');
has 'pagers'=>('is'=>'rw');
has 'dt_labels' => ('is'=>'rw', 'default' => sub { {} });
has '+fieldset' => ('default'=>'qfilter');
has 'dt_hidden' => ('is'=>'rw', 'default' => sub { {} });
has 'dt_onclick' => ('is'=>'rw', 'default' => sub { {} });
has 'texts' => ('is'=>'rw', 'default' => sub { {} });

sub _list_builder {
	my $self = shift;
	my @list = ();
	foreach my $dt(@{$self->Query->possible_DetectTypes}) {
		if (!$self->dt_hidden->{$dt}) {
			push @list, [$dt, $self->dt_labels->{$dt}, $self->dt_onclick->{$dt}];
		}
	}
	return \@list;
}

1;
