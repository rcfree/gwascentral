# $Id$

=head1 NAME

GwasCentral::Search::Filter::PublicLevel


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::ChrSize;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'cs');
has '+html_id' => ('default' => 'genomeview_size');
has '+label' => ('default' => 'Chromosome size');
has '+fieldset' => ('default' => 'more_options');

sub _list_builder { 
	return [
		['1','x1'],
		['2','x2'],
		['3','x3'],
		['4','x4'],
		['5','x5'],
	];
};

1;
