# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::DTPageSize;
use Moose;
extends qw(GwasCentral::Search::Filter::PageSize);

has '+retrieve_from' => ('default'=>'iDisplaySize');

sub _list_builder { 
	return [
		['20', '20'],
		['50' , '50'],
		['100', '100'],
		['250' , '250'],
		['all' , 'all'],
	];
};
1;
