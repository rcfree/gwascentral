# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Concept


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Page;
use Moose;
extends qw(GwasCentral::Search::Filter);
has '+retrieve_from' => ('default'=>'page');
has '+value'=>('default'=>'1');
has '+template'=>('default'=>'page');
has '+fieldset' => ('default'=>'result_opts');
has '+html_class' => ('default' => 'browser_panel');

1;
