# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::RsetIdent;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Ignore);

has '+retrieve_from' => ('default'=>'rid');
has '+fieldset' => ('default'=>'options');
has '+ignore_in_url' => ('default'=>1);
has 'resultset' => ('is'=>'rw', 'lazy'=>1,'builder'=>'_build_rset');
sub _build_rset {
	my ($self) = @_;
	my $resultset = $self->Query->DS('Study')->get_resultset_by_identifier($self->value);
	return $resultset;
}
1;
