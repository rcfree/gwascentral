# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::AddedResultsets;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Hidden);
use Data::Dumper qw(Dumper);

use Bio::Graphics::Panel;
use Tie::Hash::Indexed;

has '+retrieve_from' => ('default'=>'r[]');
has 'errors' => ('is'=>'rw','default'=>sub { {} });
has 'upload_data' => ('is'=>'rw');
has '+template' => ('default'=>'hiddencsv');
has 'resultset_list' => ('is'=>'rw');
has 'colors' => ('is'=>'rw', 'builder'=>'_build_colors', lazy=>1);
has 'html_colors' => ('is'=>'rw', 'builder'=>'_build_html_colors', lazy=>1);
has 'numbers' => ('is'=>'rw');
has '+fieldset' => ('default'=>'options');
has 'more_than_max' => ('is'=>'rw');
has 'added_studies' => ('is'=>'rw', 'builder' => '_build_added_studies', lazy=>1);
has 'added_phenotypes' => ('is'=>'rw', 'builder' => '_build_added_phenotypes', lazy=>1);
has 'is_populated' => ('is'=>'rw');
has 'resultsets' => ('is'=>'rw','builder'=>'_build_resultsets', lazy=>1);
has '+html_id' => ('default'=>'resultsets');
has 'entity' => ('is'=>'rw', 'default'=>'studies');
has 'by_position' => ('is'=>'rw');
has '+hide_on_none' => ('default'=>1);

sub _build_html_colors {
	my ($self) = @_;
	my %colors_for = ();
	my $rscounter = 0;
	my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
	my $panel = Bio::Graphics::Panel->new;
	foreach my $resultset(keys %{$self->resultsets}) {
		my @rgb = $panel->color_name_to_rgb( $colors[$rscounter] );
		$colors_for{$resultset} = join(",",@rgb);
		$rscounter++;
	}
	$self->log->info("html_colors:".Dumper(\%colors_for));
	return \%colors_for;
}

sub _build_colors {
	my ($self) = @_;
	my %colors_for = ();
	my $rscounter = 0;
	my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
	foreach my $resultset(keys %{$self->resultsets}) {
		$colors_for{$resultset} = $colors[$rscounter];
		$rscounter++;
	}
	return \%colors_for;
}

sub _build_added_studies {
	my ($self) = @_;
	my %astudies = ();
	my $rsets;
	eval {
		$rsets = $self->resultsets || {};
	};
	if($@) {
		$self->resultsets({});
		return {};
	}
	foreach my $rs(values %{$rsets}) {
		my $st_ident = $rs->study_ident;
		$astudies{$st_ident}=1;
	}

	return \%astudies;
}

sub _build_added_phenotypes {
	my ($self) = @_;
	my %aphenotypes = ();
	my $rsets;
	eval {
		$rsets = $self->resultsets || {};
	};
	if($@) {
		$self->resultsets({});
		return {};
	}
	foreach my $rs(values %{$rsets}) {
		my $pm_ident = $rs->experimentid->phenotypemethodid->identifier;
		$aphenotypes{$pm_ident}=1;
	}

	return \%aphenotypes;
}

sub _build_resultsets {
	my ($self) = @_;
	my $query = $self->Query;
	my @temp;
	return {} if !$self->value;
	if (ref($self->value) ne 'ARRAY') {
		@temp = split(",",$self->value);
		if (scalar(@temp)==1) {
			@temp = ($self->value);
		}
	}
	else {
		@temp = @{$self->value};
	}
	
	tie my %resultset_for, "Tie::Hash::Indexed";
	
	my %rs_by_server = ();
	foreach my $identifier(@temp) {
		my $db = $query->DS('Study');
		
		if ($identifier eq 'none') {
			%resultset_for=();
			%rs_by_server=();
			last;
		}
		my $resultset = $db->get_resultset_by_identifier($identifier);
		

		$resultset_for{$identifier}=$resultset;

	}
	$self->by_position([values %resultset_for]);
	return \%resultset_for;
}

sub addable_study_state {
	my ($self, $study ) = @_;
	my $addable = 0;
	my $added = 0;
	my $astudies = $self->added_studies;
	my $cal = $study->{child_accesslevel};
	my $identifier = $study->{identifier};
	if ($cal && ($cal eq 'admin' || $cal eq 'full' || $cal eq 'limited')) {
		$addable = 1;
		
		if ($astudies->{$identifier}) {
			$added = 1;
		}

	}
	else {
		$added = -1;
	}
	
	return ($addable, $added);
}

sub addable_phenotype_state {
	my ($self, $phenotype ) = @_;
	my $addable = 0;
	my $added = 0;
	my $aphenotypes = $self->added_phenotypes;
	my $cal = $phenotype->{child_accesslevel};
	my $identifier = $phenotype->{identifier};
	if ($cal && ($cal eq 'admin' || $cal eq 'full' || $cal eq 'limited')) {
		$addable = 1;
		
		if ($aphenotypes->{$identifier}) {
			$added = 1;
		}

	}
	else {
		$added = -1;
	}
	
	return ($addable, $added);
}


1;
