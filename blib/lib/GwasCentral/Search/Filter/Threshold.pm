# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Threshold;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'t');
has '+value'=>('default'=>'3');
has '+fieldset'=>('default'=>'options');
has '+label' =>('default'=>'P-value threshold');
has '+is_category' =>('default'=>1);

sub _list_builder { 
	return [
		['ZERO' , '-log p &ge; 0'],
		['1' , '-log p &ge; 1'],
		['2' , '-log p &ge; 2'],
		['3' , '-log p &ge; 3'],
		['4' , '-log p &ge; 4'],
		['5' , '-log p &ge; 5'],
		['6' , '-log p &ge; 6'],
		['7' , '-log p &ge; 7'],
		['8' , '-log p &ge; 8'],
		['9' , '-log p &ge; 9'],
		['10' , '-log p &ge; 10'],
	];
};

sub as_string {
	my ($self) = @_;
	if ($self->value) {
		return 10 ** - $self->value;
	}
}
1;
