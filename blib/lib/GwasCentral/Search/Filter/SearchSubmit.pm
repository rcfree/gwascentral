# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::SearchSubmit;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Submit);
has '+value' => ('default'=>'Search');
has '+fieldset' => ('default'=>'options');
has '+html_class' => ('default'=>'submit');
has 'onclick' => ('is'=>'rw', 'default'=>undef);
1;
