# $Id$

=head1 NAME

GwasCentral::Search::Filter::PublicLevel


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::PrivateLevel;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has 'ident' => ('is'=>'rw');
has '+html_id' => ('default' => sub {
	my ($self) = @_;
	return 'user_study' . $self->ident;
}, lazy => 1);

has '+html_onchange' => ('default' => sub {
	my ($self) = @_;
	return "requeryLevels('" . $self->ident . "','user'); storeAccess('".$self->ident . "')";
}, lazy => 1);

tie my %list, 'Tie::Hash::Indexed';
sub _list_builder { 
	return [
		['none' => 'none'],
		['request' => 'request'],
		['limited' => 'limited'],
		['full' => 'full'],
	];

};

1;
