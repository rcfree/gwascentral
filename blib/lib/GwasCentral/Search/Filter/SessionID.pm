# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::SessionID;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Hidden);

has '+retrieve_from' => ('default'=>'sessionid');
has '+fieldset' => ('default'=>'options');
has '+template' => ('default'=>'sessionid');
1;
