package GwasCentral::Search::Filter::Nature::GenomicRegion;
use Moose::Role;
has 'chr' => ('is'=>'rw');
has 'start' => ('is'=>'rw');
has 'stop' => ('is'=>'rw');
has 'region' => ('is'=>'rw', 'trigger' => \&_trigger_region);
has 'is_region' => ('is'=>'rw');
has 'sorted_features' => ('is'=>'rw');

use GwasCentral::Browser::Util qw(chr_length);

after 'populate' => sub {
	my ($self) = @_;
	my ($is_region, $chr,$start,$stop) = $self->extract_chr_coords;
	$self->is_region($is_region);
	$self->chr($chr);
	
	$start||=1;
	if (!$stop) {
		my $no = $self->chr_no;
		$no and $stop=chr_length($no);
	}
	$self->start($start);
	$self->stop($stop);
};

sub _trigger_region { 
	my ($self, $region) = @_;
	$self->chr($region->{chr});
	$self->start($region->{start});
	$self->stop($region->{stop});
}
sub chr_no {
	my ($self) = @_;
	my $chr = $self->chr;
	$chr and $chr=~s/[Cc]hr//;
	return $chr;
}

sub extract_chr_coords {
	my ( $self ) = @_;

	my ( $chr, $start, $stop, $type );
	my $keywords = $self->value;
	return undef if !$keywords;
	if ( $keywords =~ /^(chr|Chr)?([0-9|X|Y][0-9|X|Y]?)\:?/ ) {
		$chr = "chr$2";
		if ( $keywords =~ /(\w+)\D([\d|,]+)\-([\d|,]+)/ ) {
			( $chr, $start, $stop ) = ( $1, $2, $3 );
		}
		elsif ( $keywords =~ /(\w+)\D(\d+)..(\d+)/ ) {
			( $chr, $start, $stop ) = ( $1, $2, $3 );
		}
		
		$start and $start =~ s/,//g; 
		$stop and $stop =~ s/,//g;
		$chr!~/^[Cc]hr/ and $chr = "chr".$chr;
		$chr=~s/Chr/chr/;
		return ( 1, $chr, $start, $stop );
	}
	return undef;
}

1;