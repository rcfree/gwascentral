package GwasCentral::Search::Filter::Nature::Ignore;
use Moose::Role;
has 'template' => ('is'=>'rw','default'=>'none');

sub populate {
	my ($self, $params) = @_;
	my $value = $params->{$self->retrieve_from};
	defined($value) and $self->value($value);
}
1;