# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::HapmapLDGT;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'ld_gt');
has '+value'=>('default'=>'0.3');
has '+fieldset'=>('default'=>'hapmap');
has '+label' =>('default'=>'is greater than');

sub _list_builder { 
	return [
		[1.0,1.0],
		[0.9,0.9],
		[0.8,0.8],
		[0.75,0.75],
		[0.7,0.7].
		[0.5,0.5],
		[0.4,0.4],
		[0.3,0.3],
		[0.2,0.2],
		[0.1,0.1],
		[0.0,0.0]
	];
}

1;
