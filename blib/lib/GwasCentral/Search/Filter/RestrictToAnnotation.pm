# $Id$

=head1 NAME

GwasCentral::Search::Filter::RestrictToAnnotation


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::RestrictToAnnotation;
use Moose;
use GwasCentral::Base qw(load_module);

extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Checkbox);

has '+retrieve_from' => ('default'=>'rt');
has 'value' => ('is'=>'rw');
has '+fieldset' => ('default'=>'options');
has '+label' => ('default'=>'Restrict search to ontology annotations');

after 'populate' => sub {
	my ($self,$params_ref) = @_;
	my $query = $self->Query;
	return if $query->lock_DetectType;
	my $qs_filter = $query->f('QueryString');
	$self->log->info("qs_filter:$qs_filter");	
	if ($self->value && $self->value==1) {
		$self->log->info("RestrictToAnnotation set to true");
		my $detector_mod = $query->DetectType($query->new_DetectType('Annotation'));
		my $recog_as = $detector_mod->detect($qs_filter->value) || 'None';
		$recog_as and $query->recognised_as($recog_as);
		$self->log->info("Now recognised as '$recog_as'");
		$query->lock_DetectType(1);	
	}
};

1;
