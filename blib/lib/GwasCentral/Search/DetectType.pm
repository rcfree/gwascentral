# $Id$

=head1 NAME

GwasCentral::Search::Detect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType;
use Moose;
extends qw(GwasCentral::Base);
has 'Query' => ('is'=>'rw');
has 'label' => ('is'=>'rw');
has 'no_display' => ('is'=>'rw');

sub has_config {
	return undef;
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name =~ /GwasCentral::DetectType::(.+)/;
	return $name;
}

1;