# $Id$

=head1 NAME

GwasCentral::Search::Recognition::GeneRegion


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::Region;
use Data::Dumper qw(Dumper);
use Moose;
extends qw(GwasCentral::Search::DetectType);

sub detect {
	my ($self, $term) = @_;
	my $qs = $self->Query->f('QueryString');
	#if not feature attempt to extract chr coords from string (region)
	if ($qs->is_region) {
		$qs->sorted_features([]);
		return 'Region';
	}
}

1;