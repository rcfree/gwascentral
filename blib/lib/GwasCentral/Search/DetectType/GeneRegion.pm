# $Id$

=head1 NAME

GwasCentral::Search::Recognition::GeneRegion


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::GeneRegion;
use Data::Dumper qw(Dumper);
use Moose;
extends qw(GwasCentral::Search::DetectType);
has 'feature_flank_size' => ('is'=>'rw', 'default'=>20000);

sub detect {
	my ($self, $term) = @_;
	
	return undef if !$term || ($term && length($term)==0);
	my ( $is_region, $chr, $start, $stop );
	my $qs = $self->Query->f('QueryString');
	$qs->populate({q=>$term});

	#if not array then search for features matching the query_string
	my ($biggest_feature, $features_sorted_by_size) = $self->get_biggest_feature_region_for_term($term);
	
	#if features - find the biggest.. and populate 'region' accessor with reference, start and stop
	if ( $biggest_feature) {
		$qs->sorted_features($features_sorted_by_size);
		$qs->region($biggest_feature);
		return 'Feature';
	}
		
	#if not feature attempt to extract chr coords from string (region)
	if ($qs->is_region) {
		$qs->sorted_features([]);
		return 'Region';
	}
}

sub get_biggest_feature_region_for_term {
	my ($self, $term) = @_;
	my @features = $self->Query->DataSources->{Feature}->search_id($term);
	return undef if @features == 0;	

	$self->log->info("feature count:".@features);
	my @feature_sizes;
	foreach my $feature (@features) {
		next if $feature->source ne 'UCSC';
		next if $feature->ref =~ /_/;
		my $feature_size = $feature->end - $feature->start;
		push @feature_sizes,
		  { size => $feature_size, feat => $feature };
	}
	my @features_sorted_by_size =
		reverse sort { $a->{size} <=> $b->{size} } @feature_sizes;
	my $biggest_feature = $features_sorted_by_size[0]->{feat};
	my $chr   = $biggest_feature->ref;
	my $start = $biggest_feature->start - $self->feature_flank_size;
	my $stop  = $biggest_feature->end + $self->feature_flank_size;
	my $biggest_region = { chr => $chr, start => $start, stop => $stop, coords => $chr.":".$start."..".$stop };
	return ($biggest_region, \@features_sorted_by_size);
}

1;