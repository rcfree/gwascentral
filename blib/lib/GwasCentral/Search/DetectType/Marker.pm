# $Id$

=head1 NAME

GwasCentral::Search::DetectType::Marker


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::Marker;
use Moose;
extends qw(GwasCentral::Search::DetectType);
use Data::Dumper qw(Dumper);

sub detect {
	my ($self, $term) = @_;
	return undef if !$term || ($term && length($term)==0);
	
	my $marker = $self->Query->DS('Browser')->get_marker_by_id($term);
	if ( $marker ) {
		my $qs = $self->Query->f("QueryString");
		$qs->markers([$marker]);
		return 'Marker';
	}
	else {
		return undef;
	}
}

1;