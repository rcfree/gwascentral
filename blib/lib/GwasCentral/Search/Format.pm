package GwasCentral::Search::Format;
use Moose;
extends qw(GwasCentral::Base);
has 'content_type' => ('is'=>'rw');
has 'Query' => ('is'=>'rw');
has 'is_html' => ('is'=>'rw', 'default' => 0);
has 'is_redirect' => ('is'=>'rw', 'default' => 0);
has 'base_url' => ('is'=>'rw');
has 'is_download' => ('is'=>'rw','default'=>1);
has 'format_type' => ('is'=>'rw');
has 'internal_only' => ('is'=>'rw');

sub export {
	my ($self, @data) = @_;
	$self->throw("Format module ".ref($self)." has no 'export' method");
}

sub redirect_url {
	my ($self, @data) = @_;
	$self->throw("Format module ".ref($self)." has no 'redirect_url' method");
}

sub fileext {
	my ($self) = @_;
	return lc($self->name);
}

sub filename {
	my ($self) = @_;
	my $fileext = $self->fileext;
	my $filename = $fileext ? lc($self->Query->name).".".$fileext : undef;
	return $filename;
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name=~/GwasCentral::Search::Format::(.+)/;
	return $1;
}

1;