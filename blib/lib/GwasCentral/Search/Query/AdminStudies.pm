# $Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::AdminStudies - Query studies


=head1 SYNOPSIS

    use HGVbaseG2P::Search::Query::Studies;
    my $search = HGVbaseG2P::Search::Query::Studies->new;
    $search->option('q','cancer');
    my @results = $search->results;


=head1 DESCRIPTION

This class performs a search across Studies using a query string supplied as a parameter (q). It will also take into account the threshold (t) level
when searching across a region, gene or marker.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::AdminStudies;

use Moose;
extends qw(GwasCentral::Search::Query);

has '+url_path'=>('default' => 'admin/studies');
has 'jsonp_template' => ('is'=>'rw', 'default'=>'adminstudies');
has 'Page_filter' => ('is'=>'rw','default' => 'DTPage');
has 'PageSize_filter' => ('is'=>'rw','default' => 'DTPageSize');
has '+fieldset_order' => ('default'=>sub { ['options','qfilter','paging', 'browserpanel' ] });
use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Number::Format qw(:subs);
use List::MoreUtils qw(none);
use Tie::Hash::Indexed;
use GwasCentral::Search::Filter::PublicLevel;
use GwasCentral::Search::Filter::PrivateLevel;

has '+possible_Filters' => (
	default => sub {
		[qw(DTPageSize DTPage SortBy QueryString Threshold ExportFormat JSONP)
		];
	}
);
has '+Detector_name' => ('default'=>'None');

has '+Filter_options' => (
	'default' => sub {
		
		return {
			'SortBy' => {
				'list'    => [
					['none' => 'none']
				],
				'default' => 'none'
			},
			'DTPageSize' => {
				'list'    => [
					['20'  => '20'],
					['50'  => '50'],
					['100' => '100'],
					['all' => 'all'],
				],
				'default' => '50'
			},
			'DTPage'        => { 'default' => 1 },
			'Threshold' => {'default' => 'ZERO'}
		};
	}
);

sub retrieve {
	my ($self) = @_;
	
	return {};
}

sub get_data {
	my ($self, $attrs) = @_;
	my $db = $self->DataSources->{Study};
	my $ac = $db->AccessControl;
	my $info = $ac->get_studies_with_admin_level($self->f('DTPage')->number, $self->page_size, "study", []);
	$self->log->info("info:".Dumper($info));
	$self->new_pager($info->{count});
	my $data = $info->{data};
	my @records = ();
	foreach my $item(@{$data}) {
		my %record =();
		my $identifier = $item->resource;
		$record{gcid}=$identifier;
		#my ($server, $resource_type, $identifier) = split('.', $item->resource);
		#$item->{identifier}=$identifier;
		#$self->log->info("uri:".$item->{uri});
		#$self->log->info("server:$server, resource_type:$resource_type, identifier:$identifier");
		my $study = $self->DataSources->{Study}->get_study_by_identifier($item->resource);
		#$item->{server}=$server;
		next if !$study;
		$record{name}=$study->name;
		my $pul = $self->new_Filter('PublicLevel');
		$pul->value('full');
		$pul->ident($identifier);
		$record{anon_level}=$pul;
		my $prl = $self->new_Filter('PrivateLevel');
		$prl->value('full');
		$prl->ident($identifier);
		$record{user_level}=$prl;
		push @records,\%record;
	}
	
	return \@records;
#	my $method = $attrs->{internal_method};
#	my $ds = $self->DataSources->{$self->DataSource_name};
#	$ds->server(undef);
#	my $get_data_method = $self->get_data_method;
#	$self->throw("No 'get_data_method' provided in Query $self") if !$get_data_method;
#	#$self->log->info("get_data_method: ".$get_data_method." - method:$method, attrs:".Dumper($attrs));
#	my ($data,$pager) = $ds->$get_data_method($attrs);
#    $self->pager($pager);

}

sub convert_data_to_entries {
	my ($self, $data) = @_;
	my $db = $self->DataSources->{Study};
	
	foreach my $item(@{$data}) {
		#my ($server, $resource_type, $identifier) = split('/', $item->{uri});
		#$item->{identifier}=$identifier;
		#$self->log->info("uri:".$item->{uri});
		#$self->log->info("server:$server, resource_type:$resource_type, identifier:$identifier");
		#my $study = $db->get_study_by_identifier($identifier);
		#$item->{server}=$server;
		#next if !$study;
#		$item->{name}=$study->name;
#		my $pul = $self->new_Filter('PublicLevel');
#		$pul->value($item->{anon_level});
#		$pul->ident($identifier);
#		$item->{anon_level}=$pul;
#		my $prl = $self->new_Filter('PrivateLevel');
#		$prl->value($item->{user_level});
#		$prl->ident($identifier);
#		$item->{user_level}=$prl;
	}
	return $data;
}

sub populate_entries_after_cache {
	my ( $self, $entries ) = @_;
}

sub expand_cache_data {
	my ($self, $cached_data) = @_;
	return $cached_data;
}

sub collapse_cache_data {
	my ($self, $cached_item) = @_;
	return $cached_item;
}
1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

