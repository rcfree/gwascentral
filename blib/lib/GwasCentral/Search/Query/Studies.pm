# $Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Studies - Query studies


=head1 SYNOPSIS

    use GwasCentral::Search::Query::Studies;
    my $search = GwasCentral::Search::Query::Studies->new;
    $search->option('q','cancer');
    my @results = $search->results;


=head1 DESCRIPTION

This class performs a search across Studies using a query string supplied as a parameter (q). It will also take into account the threshold (t) level
when searching across a region, gene or marker.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Studies;
use strict;
use warnings;
use Moose;
extends qw(GwasCentral::Search::Query);

has '+url_path'=>('default' => 'studies');
has '+DataSource_name' => ('default'=>'Study');
has '+fieldset_order' => ('default'=>sub { ['options','qfilter','browserpanel', 'paging', 'result_opts' ] });

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Number::Format qw(:subs);
use List::MoreUtils qw(none);
use GwasCentral::Base qw(shorten_author_list);

has '+possible_Filters' => (
	default => sub {
		[qw(Page ExportFormat CannedQuery SortBy PageSize QueryString Threshold  OddsRatio MarkerCount MarkerDisplay SearchSubmit QueryFilter AddedResultsets IdentifierList TopResultsets)
		];
	}
);
#has '+fieldset_order' => ('default'=>sub { ['options','qfilter','paging' ] });
has '+Detector_name' => ('default'=>'Auto');
has '+possible_DetectTypes' => (
	'default' => sub {
		[qw(None Keyword)];
	}
);

has '+Filter_options' => (
	'default' => sub {
		return {
			'SortBy' => {
				'list'    => [
					['none' , 'Search Relevance'],
					['cast(right(me.Identifier,length(me.Identifier)-5) as SIGNED) DESC' , 'Identifier (descending)'],
					['timecreated DESC' , 'Date Created (descending)'],
					['name DESC' , 'Name (descending)'],
					['cast(right(me.Identifier,length(me.Identifier)-5) as SIGNED)' , 'Identifier (ascending)'],
					['timecreated' ,'Date Created (ascending)'],
					['name','Name (ascending)'],
					['(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported limit 1)',
					'Number of markers (ascending)'],
					['(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported limit 1) DESC' ,
				'Number of markers (descending)']
				],
				'default' => 'Identifier (descending)',
			},
			'PageSize' => {
				'list'    => [
					['20', '20'],
					['50', '50'],
					['100', '100'],
					['all', 'all'],
				],
				'default' => '50'
			},
			'Page'        => { 'default' => 1 },
			'MarkerCount' => { 'label'   => 'Markers per Study', },
			'MarkerDisplay' => { 'hidden' => 1 },
			'QueryFilter' => {
				'dt_labels' => {
					'Keyword' => 'match keywords',
					'GeneRegion' => 'matches gene feature or region',
					'Marker' => 'matches markers'
				}
			},
			'AddedResultsets' => { 
				'template' => 'browserpanel',
				'fieldset' => 'result_opts',
			 },
			 'QueryString' => {
				'autocomplete_url'=>'/studies/lookup',
				'label' => 'Enter search terms',
			},
		};
	}
);
has 'get_data_method' => ( 'is' => 'rw', 'default' => 'get_study_list' );
#
override 'text' => sub {
	my ($self) = @_;
	my $qf = $self->recognised_as;
	if ($qf eq 'None') {
		return $self->label." in the database";
	}
	else {
		return $self->label." contain the term <b>".$self->fval('QueryString')."</b>";
	}
};

sub convert_data_to_entries {
	my ( $self, $raw_studies ) = @_;
	
	my @studies = @{$raw_studies};

	my $counter        = 0;
	my $rs_counter     = 0;
	my @top_resultsets = ();
	my $rs_filter = $self->f('AddedResultsets');
	
	my $db = $self->DataSources->{Study};
	my @results = ();
	my $tr = $self->f('TopResultsets');
	foreach my $item (@studies) {

		$item->{title}="Study ".$item->{identifier}.": ".$item->{name};
		$item->{link}=$self->host."/study/".$item->{identifier};
		
		foreach my $ontology ( @{ $item->{phenotype_ontology} } ) {
			my $mesh_ident = $ontology->{mesh_identifier};
			if ($mesh_ident) {
				$ontology->{mesh_heading} = [map { $_ ? $_->name : '' }
						  $self->DataSources->{Ontology}
						  ->get_mesh_heading_from_id($mesh_ident)
					  ]
				};
		}
		foreach my $cit(@{$item->{citations} }) {
			$cit->{link} = "http://pubmed.org/".$cit->{pubmedid};
			$cit->{label} = shorten_author_list($cit->{authors},1);
			$cit->{target}="_blank";
			$cit->{image}="link";
		}
		
		my ( $is_addable, $is_added ) =
		  $rs_filter->addable_study_state( $item );
		$item->{addable}    = $is_addable;
		$item->{added} = $is_added;

		foreach my $rs ( @{ $item->{resultsets} } ) {
			if ( !$tr->more_than_max && $is_addable ) {
				$rs_counter++;
				push @top_resultsets, $rs;
				if ( $rs_counter > 16 ) {
					$tr->more_than_max(1);
				}
			}
		}
		push @results, $item;
	}
	$tr->value(\@top_resultsets);
}

sub retrieve_region {
	my ($self) = @_;
	return $self->retrieve_feature;
}

sub retrieve_gene {
	my ($self)        = @_;

	my $qs = $self->f('QueryString');

	my $b_attrs = {
		odds_ratio => $self->fval('OddsRatio'),
			threshold => $self->fval('Threshold'),
			chr       => $qs->chr_no,
			start     => $qs->start,
			stop      => $qs->stop
		};
		$self->log->info("b_attrs:".Dumper($b_attrs));
	my ( $studies, $rs_data, $m_data ) = $self->DataSources->{Browser}
	  ->get_studies_rsets_and_markers_in_region_above_threshold(
		$b_attrs
	  );

	$self->fval('IdentifierList',$studies);
}

sub retrieve_keyword {
	my ($self)        = @_;
	
	my $qs_filter_value = $self->fval('QueryString');
	my $xap       = $self->DataSources->{Xapian};
	
	# Send query with supplied query string to text search engine. This may cause an exception
	#grab list of identifiers from Xapian database and then retrieve study object for each
	my ( $total, $study_identifiers );
	eval {
		( $total, $study_identifiers ) =
		  $xap->search_all( $qs_filter_value, 'studies');
	};
	if ($@) {
		$self->throw("Search was not successful","Search");
	}
	# and then retrieve study object for each
	$study_identifiers ||= [];
	$self->log->info("number of idents:".scalar(@{$study_identifiers}));
	$self->fval('IdentifierList',$study_identifiers);
}

sub retrieve_marker {
	my ($self) = @_;
	
	my $qs_filter = $self->f('QueryString');
	my $qs_filter_value = $qs_filter->value;
	my $threshold = $self->fval('Threshold');

	#use the first marker previously retrieved by 'recognise_query_string'  to retrieve resultset identifiers
	my @marker_results = @{$qs_filter->markers || []};

	if (scalar(@marker_results)==0) {
		return;
	}

	my $identifier     = $marker_results[0]->{Identifier};
	my $chr            =  $marker_results[0]->{Chr};
	my ( $study_idents, $rset_idents, $marker_data ) =
	  $self->DataSources->{Browser}
	  ->get_studies_rsets_and_markers_by_identifier_above_threshold(
		{
			odds_ratio => $self->fval('OddsRatio'),
			threshold  =>  $self->fval('Threshold'),
			identifier => $identifier,
			chr        => "chr$chr",
			marker_display => $self->fval('MarkerDisplay'),
		}
	  );
	  
	 $self->fval('IdentifierList',$study_idents);
}

sub retrieve_none {
	my ($self)        = @_;

	$self->f('IdentifierList')->ignore(1);
}
1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

