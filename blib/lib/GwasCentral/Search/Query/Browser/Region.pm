# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Browser - Query for browser data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Browser::Region;

use Moose;
extends qw(GwasCentral::Search::Query::Browser);
use GwasCentral::Browser::Binning;
use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Bio::Graphics::FeatureFile;
use FindBin;
use lib "$FindBin::Bin/../../../../../lib";

use GwasCentral::Browser::Util qw(chr_length);
use Bio::Graphics::Panel;
use Math::Round qw(:all);
use List::Util qw(sum max);

has '+possible_Filters' => (
	default => sub {
		[qw(QueryString ExportFormat Threshold AddedResultsets ScaleType RegionTracks HapmapPopulations HapmapLDProperty HapmapLDGT HapmapLDLT)];
	}
);
has '+Filter_options' => (
	'default' => sub {
		{
			'Threshold'    => { 'default' => '3' },
			'ExportFormat' => {
				'list' => [
					['html'=>'--choose a format --'],
					['excel'=>'Microsoft Excel'],
					['csv'=>'Comma-separated file'],
				    ['tsv'=>'Tab-separated file'],
					['json'=>'JSON file'],
					['bed'=>'BED file'],
					['gff'=>'GFF3 file'],
				],
			},
			'PageSize' => { 'default' => 'all', 'list' => [ ['all'=>'all'] ] },
			'SortBy' => { 'default' => 'none', 'list' => [ ['none'=>'none'] ] },
		};
	}
);
has 'max_count' => ( 'is' => 'rw' );
has 'rs_counts' => ( 'is' => 'rw' );
has 'lowres_binsize' => ('is'=>'rw', 'default'=>1000000);
has 'highres_binsize' => ('is'=>'rw', 'default'=>50000);
has 'round_start' => ('is'=>'rw', 'builder' => '_build_round_start', lazy => 1);
has 'round_stop' => ('is'=>'rw', 'builder' => '_build_round_stop', lazy => 1);
has '+no_search_if_html' => ('default'=>1);
has '+url_path' => ('default' => '/browser/region');

sub convert_data_to_entries {
	my ( $self, $data ) = @_;
}

sub gbrowse_url {
	my ($self, $params) = @_;
	my @qstring = ();
	push @qstring,"name=".$self->f('QueryString')->value;
	
	foreach my $f(values %{$self->Filters}) {
		$self->log->info("f:".Dumper($f->retrieve_from));
		my @f_qstring = $f->gbrowse_qstring;
		@f_qstring and push @qstring, @f_qstring;
	}
	
	return $self->config->{WebSite}->{browser_rv_path}."?".join(";",@qstring);
}

sub as_features {
	my ($self) = @_;
	$self->results;
	$self->log->info("gets _to_feature");

	#use supplied list or create new features list
	!$self->features and $self->features(
		Bio::Graphics::FeatureFile->new(
			-smart_features => 1,
			-safe           => undef
		)
	);

	#$self->seg_start(1);
	my %messages = %{ $self->messages };
	if ( scalar( keys %messages ) > 0 ) {
		if ( $messages{notfound} ) {
			$self->add_textbox( "The following resultsets do not exist",
				$messages{notfound}, 'red', 'pink' );
		}

		if ( $messages{none} ) {
			$self->add_textbox(
				"No data access is provided for the following resultsets",
				$messages{none}, 'red', 'pink' );
		}

		if ( $messages{limited} ) {
			$self->add_textbox(
				"Limited data access is provided for the following resultsets",
				$messages{limited}, 'royalblue', 'lightblue'
			);
		}
		if ($messages{nodata}) {
			$self->add_textbox("The following resultsets do not have Browser data", $messages{nodata}, "red","pink");
		}
		if ($messages{server}) {
			$self->add_textbox("The following resultsets could not be accessed", $messages{server}, "red","pink");
		}
	
	}
	
	if ($self->is_high_resolution) {
		$self->_add_high_res_features;
	}
	else {
		$self->_add_low_res_features;
	}
	return $self->features;
}

sub _add_low_res_features {
	my ($self) = @_;	
	
	my $height = 30;
	
	my $rs_filter = $self->f('AddedResultsets');
	my $region = $self->f('QueryString');
	my @rs_idents = keys %{ $rs_filter->resultsets };
	my $tracks = $self->f('RegionTracks');
	
	my $data = $self->data;
	$self->log->info( "amount of data:" . scalar( @{ $self->data || [] } ) );
	
	my $rs_counts      = { map { $_ => 0 } @rs_idents };
	
	
	if ($tracks->is_on('stacked')) {
		$self->add_line();
		$self->add_sig_counts_glyph( { 
				height => 50, 
				key => "Study data: Stacked counts of -log p-values >= ".$self->fval("Threshold")." (".$self->lowres_binsize/(10**6)
				  . "-Mb regions) click a histogram bar to view region" 
			} );

		#loop through each chromosome and add stacked plot data for each bin within
		my ( $chr_max_count ) =
		  $self->add_sig_counts(
			{
				data      => $data,
				chr       => $region->chr_no,
				rs_counts => $rs_counts,
				bin_size => $self->lowres_binsize,
			}
		  );
	
		$self->features->set( 's', max_score => $chr_max_count );
	}
	
	if ($tracks->is_on('present')) {
		$self->add_line();
		$self->add_heading(
				"Study data: Marker coverage for selected studies (".$self->lowres_binsize."-Mb regions)" 
				);
	
		$self->add_bins_present_glyph();
		$self->add_bins_present(
			{
				data => $data,
			}
		);
	}
}

sub _add_high_res_features {
	my ($self) = @_;
	my $rs_filter = $self->f('AddedResultsets');
	my $region = $self->f('QueryString');
	my @rs_idents = keys %{ $rs_filter->resultsets };
	my $threshold = $self->fval('Threshold');
	my $tracks = $self->f('RegionTracks');
	
	my $data = $self->data;

	my $lowres_binsize = $self->lowres_binsize / 1000000;
	my $highres_binsize = $self->highres_binsize / 1000;

	my $binned_data = $self->_calculate_binned_data(
		{
			bin_size => $self->highres_binsize,
			data     => $data
		}
	);
	
	my (
		$max_and_counts, $sig_markers, $multi_sig_markers,
		$max_pvalue,     $max_count
	) = $self->_calculate_max_and_sig_counts( { data => $binned_data } );
	
	
	if ($tracks->is_on('stacked')) {
		$self->add_line();
	
		$self->add_sig_counts_glyph( { 
			height => 50, 
			key => "Study data: Stacked counts of -log p-values >= $threshold ($highres_binsize"
			  . "-kb regions)" 
		} );
	
		my ( $chr_max_count, $rs_counts ) = $self->add_sig_counts(
			{
				data     => $max_and_counts,
				bin_size => $self->highres_binsize,
				chr => $region->chr_no,
			}
		);
	
		$self->features->set( 's', max_score => $chr_max_count );
	}
	
	if ($tracks->is_on('present')) {
		$self->add_line();
		$self->add_heading(
			"Study data: Marker coverage for selected studies ($highres_binsize"
			  . "-kb regions)" );

		$self->add_bins_present_glyph();
		$self->add_bins_present(
			{
				data     => $max_and_counts,
				bin_size => $self->highres_binsize,
			}
		);
	}
	if ( $tracks->is_on("line_trace") ) {
		$self->add_line();

		$self->add_pvalue_traces(
			{
				height     => 50,
				data       => $max_and_counts,
				max_pvalue => $max_pvalue,
				key => "Study data: Line plot of maximum -log p-values for selected studies ($highres_binsize"
			  . "-kb regions)"
			}
		);
	}

	if ( $tracks->is_on("sig_markers_single") ) {
		$self->add_line();
		$self->add_sig_markers( { data => $sig_markers } );
	}

	if ( $tracks->is_on("sig_markers_multiple") ) {
		$self->add_line();
		$self->add_combined_sig_markers( { data => $multi_sig_markers } );
	}
}

=head2 add_sig_markers

	  Usage      : $self->add_sig_markers( { data => $data } );
	  Purpose    : For each resultset, creates a marker glyph and adds supplied markers to feature list
	  Returns    : Nowt
	  Arguments  : Hashref of:
	                data - hashref of markers with resultsetid as key (see calculate_max_and_sig_counts)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_sig_markers {
	my ( $self, $attrs ) = @_;

	my $data          = $attrs->{data};
	my $features      = $self->features;
	my $region = $self->f('QueryString');
	my $rs_filter = $self->f('AddedResultsets');
	my $colors = $rs_filter->colors;
	my $ref           = $region->chr;
	my $feature_count = 0;
	my $threshold = $self->fval('Threshold');
	my $bgcolors = $rs_filter->colors;

	my $rscounter = 0;
	my $summaries = $rs_filter->resultsets;

	#loop through each resultset in data and add a dot glyph
	foreach my $rs_ident ( keys %{ $rs_filter->resultsets } ) {
		my $rs = $rs_filter->resultsets->{$rs_ident};
		my $unique_name = $rs_ident;
		my $type = "sigmarkers".$rs_ident;
	
		$features->add_type(
			$type => {
				bgcolor => $colors->{$rs_ident},
				glyph   => 'dot',
				fgcolor => 'black',
				label   => 1,
				bump    => 1,
				title   => 1,
				height  => 8,
				clip    => 'true',
				scale   => 'none',
				'balloon hover' => $rs_ident !~/^U/ ?
'sub { my $feature = shift; return "Click to view significance" }' : 'sub { my $feature = shift; return "Significance is not in database" }',
				key => "Study data: Significant markers - ".$unique_name
			}
		);

#loop through each significant marker for the resultset and add it to the features
		my $sig_count = 0;
		
		foreach my $sig ( @{ $data->{$rs_ident} } ) {
			if ($sig) {
				my $url =
				    "goToPage('marker/"
				  . $sig->{'Marker_Identifier'}
				  . "/results?rfilter="
				  . $sig->{Resultset_Ident} . "&t="
				  . $threshold . "')";

				my $feature = Bio::Graphics::Feature->new(
					-start => $sig->{'Start'},
					-stop  => $sig->{'Stop'},
					-score => $sig->{'NegLogPValue'},
					-ref   => $ref,
					-type  => 'Marker' . $rs_ident,
					-name  => $sig->{'Marker_Accession'} . " (-log p="
					  . sprintf( "%.2f", $sig->{'NegLogPValue'} ) . ")",
					-url => ($rs_ident !~/^U/ and $url),
				);

				$features->add_feature( $feature => $type );
				$sig_count++;
			}

		}

		#if no significant markers for a resultset - highlight this
		if ( $sig_count == 0 ) {
			$self->add_heading("Study data: Significant markers - ".$unique_name." - none present in this region");
		}
		$rscounter++;
		$rscounter != scalar( keys %{ $rs_filter->resultsets } )
		  and $self->add_line(undef,1);
	}
}

=head2 add_combined_sig_markers

	  Usage      : $self->add_combined_sig_markers( { data => $data } );
	  Purpose    : For marker data provided (significant in multiple resultsets) adds markers to feature list
	  Returns    : Nowt
	  Arguments  : Hashref of:
	                data - hashref of markers with markerid as key (see calculate_max_and_sig_counts)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_combined_sig_markers {
	my ( $self, $attrs ) = @_;

	my $data = $attrs->{data};

	my $features      = $self->features;
	
	my $region = $self->f('QueryString');
	my $rs_filter = $self->f('AddedResultsets');
	my $threshold = $self->fval('Threshold');
	my $ref           = $region->chr;
	my $feature_count = 0;

	my $rscounter = 0;

	#add diamond glyph to represent multi markers
	my $type = "multisig";
	$features->add_type(
		$type => {
			bgcolor => 'black',
			glyph   => 'diamond',
			fgcolor => 'black',
			label   => 1,
			bump    => 1,
			title   => 1,
			height  => \&marker_size,
			width   => \&marker_size,
			clip    => 'true',
			scale   => 'none',
			'balloon hover' =>
'sub { my $feature = shift; return "Click to view significances" }',
			key => "Study data: Significant markers across all selected resultsets",
		}
	);

	my $sig_count = 0;

	#loop through each marker
	foreach my $marker ( keys %{$data} ) {
		my @rsids = ();

		#for each marker, add significance IDs into an array
		my @multi_sigs = @{ $data->{$marker} };
		foreach (@multi_sigs) {
			push @rsids, $_->{'Resultset_Identifier'};
		}
		my $sig      = $multi_sigs[0];
		my $rs_count = scalar(@multi_sigs);
		my $resultsets_with_sig=2;
#only add the feature if the number of resultsets they are signficant in passes the threshold set by the user
#use the array of significances in the javascript call (showSigs)
		if ( $rs_count >= $resultsets_with_sig ) {
			my $url =
			    "goToPage('marker/"
			  . $sig->{'Marker_Identifier'}
			  . "/results?rfilter="
			  . join( ",", @rsids ) . "&t="
			  . $threshold . "')";
			my $feature = Bio::Graphics::Feature->new(
				-start => $sig->{'Start'},
				-stop  => $sig->{'Stop'},
				-score => scalar(@multi_sigs),
				-ref   => $ref,
				-type  => 'MarkerAll',
				-name  => $sig->{'Marker_Accession'} . " ("
				  . $rs_count
				  . " resultsets)",
				-url => $url
			);

			$features->add_feature( $feature => $type );
			$sig_count++;
		}

	}

	#if none significant in multiple resultset highlight this
	if ( $sig_count == 0 ) {
		$self->add_heading("Study data:Significant markers across all selected resultsets - none present in this region");
	}
	$rscounter++;

}

sub add_pvalue_traces {
	my ( $self, $attrs ) = @_;

	my $max_pval = $attrs->{max_pvalue};
	my $features = $self->features;
	my $region = $self->f('QueryString');
	my $ref      = $region->chr;
	my $data     = $attrs->{data};
	my $key = $attrs->{key};
	my $threshold = $self->fval("Threshold");
	
	$features->add_type(
		maxp => {
			glyph       => 'stackplot',
			fgcolor     => 'red',
			bgcolors    => $self->config->{Browser}->{bgcolors},
			scale_color => 'black',
			graphtype   => 'line',
			height      => $attrs->{height},
			min_score   => 0,
			max_score   => (
				  $threshold > $max_pval
				? $threshold
				: $max_pval
			),
			scale           => 'left',
			bump            => 0,
			key             => 0,
			threshold       => $threshold,
			'balloon hover' => 'Maximum P values across this region',
			method          => 'trace',
			source          => 'GwasCentral',
			point_symbol    => 'disc',
			key => $key,
		}
	);

#loop through items, create a subfeature array, finally add as a segment to a feature and add this to the feature list
	my @subfeatures;
	foreach my $item ( @{$data} ) {

		my $score = $item->{bin_max};

		my $subfeature = Bio::Graphics::Feature->new(
			-start => $item->{bin_start},
			-stop  => $item->{bin_stop},
			-score => $score,
			-ref   => $ref,
		);
		push @subfeatures, $subfeature;

	}

	my $f = Bio::Graphics::Feature->new(
		-segments => \@subfeatures,
		-type     => 'maxp'
	);
	$features->add_feature($f);

}


sub _calculate_max_and_sig_counts {
	my ( $self, $attrs ) = @_;
	
	my $region = $self->f('QueryString');
	
#get marker data for all resultsets as hash with key of Start - resultsets are present in an arrayref
	my $data       = $attrs->{'data'};
	my $rs_filter = $self->f('AddedResultsets');
	my @rs_idents = keys %{ $rs_filter->resultsets };
	
	my @all_sums   = ();
	my @tot_max    = ();
	my $threshold  = $self->filter_value('Threshold');

	my %sig_markers       = ();
	my %multi_sig_markers = ();
	foreach (@rs_idents) { $sig_markers{$_} = [] }

	#loop through each binned item
	foreach my $item ( @{$data} ) {
		my %counts;
		my %allcounts;
		my %values;
		my %bin_max;

		#reset count/allcount values for resultsets for each bin
		for my $rsid (@rs_idents) {

			#set counts to 0
			$counts{$rsid}    = 0;
			$allcounts{$rsid} = 0;
			$bin_max{$rsid}   = 0;
			$values{$rsid}    = [];
		}

#loop through each marker in binned items and count number of markers for each in a bin
		foreach my $marker ( @{ $item->{bin_content} } ) {
			my $rsid     = $marker->{'Resultset_Identifier'};
			my $pvalue   = $marker->{'NegLogPValue'};
			my $markerid = $marker->{'Marker_Identifier'};

			#add to count only if significance above threshold
			if ($marker) {
				if ( $pvalue >= $threshold ) {
					push @{ $sig_markers{$rsid} },           $marker;
					push @{ $multi_sig_markers{$markerid} }, $marker;
					$counts{$rsid}++;
				}
				$allcounts{$rsid}++;  #always add to all counts if marker in bin
				push @{ $values{$rsid} }, $pvalue ? $pvalue : '0.00';
			}
		}

#get cumulative sum of counts for bin and add to all_sums (do this before replacing 0s with -1s)
#used by stack plots (top value used as max score value)
		my $bin_sum = sum( values %counts );
		push @all_sums, $bin_sum;

		#add bin counts to item and find maximum for each resultset in bin
		#if bin has no markers at all then set score to -1
		my @count_sorted = ();
		my @max_sorted   = ();
		foreach my $rsid (@rs_idents) {
			$counts{$rsid} = -1 if $allcounts{$rsid} == 0;
			push @count_sorted, $counts{$rsid};
			$bin_max{$rsid} = max( @{ $values{$rsid} } ) || 0;
			if ( $counts{$rsid} == -1 ) {
				push @max_sorted, -1;
			}
			else {
				push @max_sorted, $bin_max{$rsid};

			}
		}
		$item->{bin_counts} = join( "|", @count_sorted );
		$item->{bin_max}    = join( "|", @max_sorted );

#find bin maximum for all resultsets and add to array of all max values (used by P value trace)
		my $max_of_bin = max( values %bin_max );
		push @tot_max, $max_of_bin;
	}

	my $max_counts = max(@all_sums);
	my $max_pval   = max(@tot_max);

	return ( $data, \%sig_markers, \%multi_sig_markers, $max_pval,
		$max_counts );
}

sub _calculate_binned_data {
	my ( $self, $attrs ) = @_;

	my $data = $attrs->{data};
	my $region = $self->f('QueryString');
	#bin marker data but provide content rather than count (ie. retain elements created above)
	my $analyse = GwasCentral::Browser::Binning->new( { data => $data } );
	
	my $binned = $analyse->bin(
		{
			position_field => 'Start',
			bin_size       => $attrs->{bin_size},
			total_length   => chr_length( $region->chr_no ),
			value_field    => 'NegLogPValue',
			bin_content    => 'array',
			calc_ranges    => 1,
			start_position => $self->round_start,
			stop_position  => $self->round_stop,
		}
	);

	return $binned;
}

sub get_data {
	my ( $self, $attrs ) = @_;
	
	my $data;
	$self->log->info("attrs:".Dumper($attrs));
	if ($self->is_high_resolution) {
		$data = $self->DS('Browser')->get_marker_data({Query => $self});
	}
	else {
		$data = $self->DS('Browser')->get_binned_data({bin_size=>1, Query => $self});
	}
	
	return $data;
}

sub is_high_resolution {
	my ($self)        = @_;
	my $region = $self->f('QueryString');
	return undef if $region->chr && !$region->start && !$region->stop;
	my $region_size   = $region->stop - $region->start;
	
	my $use_minor_bin =
	  $region_size >= ( $self->lowres_binsize * 8 )
	  ? 0
	  : 1;
	 $self->log->info("is_high_resolution returns:$use_minor_bin");
	return $use_minor_bin;
}

sub _build_round_start {
	my ($self) = @_;
	my $region = $self->f('QueryString');
	my $bin_start = nlowmult( $self->lowres_binsize, $region->start );
	$bin_start == 0 and $bin_start = 1;
	return $bin_start;
}

sub _build_round_stop {
	my ($self) = @_;
	my $region = $self->f('QueryString');
	return nhimult( $self->lowres_binsize, $region->stop );
}

sub _to_gff {
	my ($self) = @_;
	my $data = $self->results;
	
	my $rs_filter = $self->f('AddedResultsets');

	my $summaries = $rs_filter->resultsets;

	#header line for BED file
	my $output="#GFF version 3\n";
	my @identifiers = keys %{$rs_filter->resultsets};
	
	my $bin_count = 1;
	if ($self->is_high_resolution) {
		foreach my $datum(@{$data}) {
			my $start = $datum->{Start};
			my $stop = $datum->{Stop};
			my $chr = $datum->{Chr};
			my $score = $datum->{NegLogPValue};
			my $rset_ident = $datum->{Resultset_Identifier};
			$output.="chr$chr\tgwascentral\tsignificance\t$start\t$stop\t$score\t.\t.\tID=".$datum->{Marker_Accession}."_$rset_ident;rset_ident=$rset_ident\n";
		}
	}
	else {
		foreach my $datum(@{$data}) {
			#$self->log->info("data:".Dumper($datum));
			my $start = $datum->{bin_start};
			my $stop = $datum->{bin_stop};
			my $chr = $datum->{Reference};
			my $value = $datum->{bin_counts};
			my $score_count = 0;
			foreach my $score(split(/\|/,$value)) {
				if ($score ne -1 && $score ne 'X') { 
					$output.="chr$chr\tgwascentral\tvariant_bin\t$start\t$stop\t$score\t.\t.\tID=".$identifiers[$score_count]."_bin".$bin_count.";rset_ident=".$identifiers[$score_count]."\n";
				}
				$score_count++;
			}
			$bin_count++;
		}
	}
	return $output;
}

=head2 marker_size

	  Usage      : height          => \&marker_size,
	  Purpose    : GBrowse callback to calculate size of glyph depending on feature score (used by add_combined_sig_markers)
	  Returns    : 4*score or 1 if no score
	  Arguments  : Dealt with by GBrowse
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub marker_size {
	my ($feature) = @_;
	my $val = $feature->score;
	if ( !$val ) {
		return 1;
	}
	else {
		return $val * 4;
	}
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

