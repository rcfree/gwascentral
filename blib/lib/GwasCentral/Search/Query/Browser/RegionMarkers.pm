# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Browser - Query for browser data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Browser::RegionMarkers;

use Moose;
extends qw(GwasCentral::Search::Query::Browser::Region);
use GwasCentral::Browser::Binning;
use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Bio::Graphics::FeatureFile;
use FindBin;
use lib "$FindBin::Bin/../../../../../lib";
has '+no_search_if_html' => ('default'=>undef);
has '+url_path' => ('default' => '/browser/rmarkers');

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

