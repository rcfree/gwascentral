# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Browser - Query for browser data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Browser::Genome;

use Moose;
extends qw(GwasCentral::Search::Query::Browser);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);

use Text::ParseWords;
use Bio::Graphics::FeatureFile;
use FindBin;
use lib "$FindBin::Bin/../../../../../lib";
use Env;
use GwasCentral::Browser::Util qw(chr_length poslog);
use Bio::Graphics::Panel;

has '+possible_Filters' => (
	default => sub {
		[qw(Page ExportFormat Threshold AddedResultsets ScaleType SessionID MultiChr  MarkerCoverage Rotate ChrSize SearchSubmit)];
	}
);
has '+Filter_options' => (
	'default' => sub {
		{
			'Threshold'    => { 'default' => '3' },
			'ExportFormat' => {
				'list' => [
					['html'=>'--choose a format --'],
					['excel'=>'Microsoft Excel'],
					['csv'=>'Comma-separated file'],
				    ['tsv'=>'Tab-separated file'],
					['json'=>'JSON file'],
					['bed'=>'BED file'],
					['gff'=>'GFF3 file'],
				],
			},
			'PageSize' => { 'default' => 'all', 'list' => [ ['all'=>'all'] ] },
			'SearchSubmit' => {
			}
		};
	}
);
#has '+Format_options' => (
#	'default' => sub {
#		'gff' => {
#			
#	}
#);
has '+fieldset_order' => ('default'=>sub { ['options'] } );
has 'max_count' => ( 'is' => 'rw' );
has 'rs_counts' => ( 'is' => 'rw' );
#has 'gbrowse_params' => ('is'=>'rw', 'trigger' => \&_trigger_gbrowse_params);
has 'gbrowse_prefix' => ('is'=>'rw', 'default' => 'GenomeView');
has '+no_search_if_html' => ('default'=>1);
#
#sub _trigger_gbrowse_params {
#	my ($self,$gb_params) = @_;
#	$self->log->info("gb_params:".Dumper($gb_params));
#	my $prefix = $self->gbrowse_prefix;
#	my @param_list = grep { $_=~ /^$prefix\:.+/ } keys %{$gb_params};
#	my %params = map { /.+\:(.+)/; $1 => $gb_params->{$_} } @param_list;
#	$self->log->info("params:".Dumper(\%params));
#	$self->params(\%params);
#}


sub get_data {
	my ($self)        = @_;
	my $results = $self->DS('Browser')->get_binned_data({bin_size=>3, Query => $self});
	return $results;
}

sub convert_data_to_entries {
	my ( $self, $data ) = @_;
}

sub as_features {
	my ($self) = @_;
	$self->results;
	my $tt_file = $self->config->{basedir} . "/htmltemplates/lib/gbrowse/sig_popup.tt2";
	
	open SIGPOPUP, $tt_file;
	my @temp = <SIGPOPUP>;
	close SIGPOPUP;
	
	my $tmpl_text = join("\n",@temp);
	
	#use supplied list or create new features list
	!$self->features and $self->features(
		Bio::Graphics::FeatureFile->new(
			-smart_features => 1,
			-safe           => undef
		)
	);

	my $height = 30;

	$self->add_sig_counts_glyph( { height => $height } );

   #if genome_present track switched on add glyph indicating presence of markers
    if ( $self->fval('MarkerCoverage')) {
   		$self->add_bins_present_glyph( { height => int( $height / 10 ) } );
   	}
	my $rs_filter = $self->f('AddedResultsets');
	my @rs_idents = keys %{$rs_filter->resultsets};

	my %chr_data     = %{$self->data};
	my $total_counts = {};
	
	
	#$self->get_marker_binned_data( { bin_size => '3' } );
	my $rs_counts      = { map { $_ => 0 } @rs_idents };

	my @max_counts = ();
	
	
	
	
	
	#loop through each chromosome and add stacked plot data for each bin within
	foreach my $chr_name ( keys %chr_data ) {
		my $chr_rs_counts = {};
		my ( $chr_max_count ) =
		  $self->add_sig_counts(
			{
				data      => $chr_data{$chr_name},
				chr       => $chr_name,
				rs_counts => $chr_rs_counts,
				popup_template => $tmpl_text,
			}
		  );
		foreach my $rs_ident ( keys %{$chr_rs_counts} ) {
			#$self->log->info("add ".$chr_rs_data_counts->{$rs_ident}." to $rs_ident");
			$rs_counts->{$rs_ident} +=
			  $chr_rs_counts->{$rs_ident};
		}

		if ( $self->fval('MarkerCoverage') ) {

			#$self->seg_stop( chr_length($chr_name) );
			$self->add_bins_present(
				{
					data             => $chr_data{$chr_name},
					bin_size         => 300000,
					bin_size_compact => 3,
					chr              => $chr_name,
				}
			);
		}
		defined($chr_max_count) and push @max_counts, $chr_max_count;
	}

	foreach my $rs (values %{$rs_filter->resultsets}) {
		my $ident = $rs->identifier;
		if ( $rs_counts->{$ident} == 0 ) {
			$rs_counts->{$ident} = "No Data";
		}
		if ($rs->{error}) {
			$rs_counts->{$ident} = "Server Error"
		} 
	}

	#find largest count in whole genome - log it if logscale flag is 1
	my @sorted = reverse sort { $a <=> $b } @max_counts;

	#reset the max_score attribute of the type to the largest count
	my $max_count = $sorted[0];
	$self->features->set( 's', max_score => $max_count );

	$self->max_count($max_count);
	$self->rs_counts($rs_counts);
	return $self->features;
}



sub _to_bed {
	my ($self) = @_;
	$self->results;
	$self->log->info("gets _to_bed");
	
	my ($rs_filter,$region) = $self->f('AddedResultsets','GenomicRegion');
	my %colors_for = %{ $rs_filter->colors };
	my $summaries = $rs_filter->resultsets;
	#retrieve data for all resultsets
#	my %data_for = $self->_get_lines_by_resultsets({ 
#		offset => 1,
#		field_order=>[qw(Chr Start Stop Marker_Accession NegLogPValue Strand)],
#		chr_prefix =>'chr',
#	}); 
	my %data_for = ();
	
	#create temporary file for BED output
	#die "Unable to find directory ".($self->config->{tempdir} || "") if !$self->config->{tempdir} or !-d $self->config->{tempdir};
	#chdir $self->config->{tempdir};
	#my ($fh,$filename) = tempfile('hgvtempXXXXXXX');
	#die "Unable to create temp file" if !$filename;
	
	#header line for BED file
	my $output="browser position\n";
	# chr".$region->chr.":".$region->start."-".$region->stop."\n";
	
	#output data for each resultset/upload including links back to GwasCentral
	my @track_names = ();
	foreach my $rs_ident(@{$rs_filter->identifiers}) {
		my ($study_ident);
		if ($rs_ident =~ /^U/) {
			$study_ident = 'UPLOAD';
		}
		else {
			$study_ident = $summaries->{$rs_ident}->study_ident;
		}
		push @track_names, $study_ident."_".$rs_ident;
	}
	$output.="browser pack ".join(' ',@track_names)."\n";

	foreach my $rs_ident(@{$rs_filter->identifiers}) {
		next if !$data_for{$rs_ident};
		my @bed_lines = @{$data_for{$rs_ident}};
		next if scalar(@bed_lines)==0;
		my ($study_ident, $study_name, $rs_name, $htmlurl, $url);
		if ($rs_ident =~ /^U/) {
			$study_ident = 'UPLOAD';
			my $udata = $self->upload_data->{$rs_ident};
			$study_name = $udata->{'study_name'};
			$rs_name = $udata->{'resultset_name'};
			$url = '#';
			$htmlurl = '#';
		}
		else {
			my $rs = $summaries->{$rs_ident};
			my $study = $rs->experimentid->studyid;
			$study_ident = $study->identifier;
			$study_name = $study->name;
			$rs_name = $rs->name;
			$url = $self->config->{WebSite}->{hgvbaseg2p_baseurl}."/marker/dbSNP:\$\$/results?rfilter=" . $rs_ident;
			$htmlurl = $self->config->{WebSite}->{hgvbaseg2p_baseurl}."/study/".$study_ident;
		}
		$output.='track name="'.$study_ident."_".$rs_ident.'" description="'.$study_name .' (Result Set '.$rs_ident.')" offset=-1 visibility=1'; 
		$output.=" color=\"".$colors_for{$rs_ident}."\" htmlUrl=\"".$htmlurl."\" ";
		$output.="url=\"".$url."\"\n";
		foreach my $line(@bed_lines) {
			$output.=join(' ',@{$line})."\n";
		}
	}
	return $output;
}

use JSON::XS;

# Globals
my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
$json_coder->space_after(0);

sub _to_gff {
	my ($self) = @_;
	my $data = $self->results;
	$self->log->info("gets _to_gff");
	
	my $rs_filter = $self->f('AddedResultsets');

	my $summaries = $rs_filter->resultsets;

	#header line for BED file
	my $output="#GFF version 3\n";
	my @identifiers = keys %{$rs_filter->resultsets};
	
	my $bin_count = 1;
	foreach my $chr(%{$data}) {
		my $chr_data = $data->{$chr};
		foreach my $datum(@{$chr_data}) {
			my $start = $datum->{bin_start};
			my $stop = $datum->{bin_stop};
			my $value = $datum->{bin_counts};
			my $score_count = 0;
			foreach my $score(split(/\|/,$value)) {
				if ($score ne -1 && $score ne 'X') { 
					$output.="chr$chr\tgwascentral\tvariant_bin\t$start\t$stop\t$score\t.\t.\tID=".$identifiers[$score_count]."_bin".$bin_count.";rset_ident=".$identifiers[$score_count]."\n";
				}
				$score_count++;
			}
			$bin_count++;
		}
	}
	
	return $output;
}


1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

