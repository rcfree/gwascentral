# $Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Results::MarkerResults - Query for marker association results


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Marker;
use Moose;
extends qw(GwasCentral::Search::Query);
has '+Detector_name' => ( 'default' => 'None' );
has '+url_path' => ( 'default' => sub { 'marker/'.$_[0]->fval('MarkerIdentifier') }, 'lazy' => 1 );
has '+is_report' => ('default'=>1);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Tool::Gene qw(separate_features);

has '+url_path' => ( 'default' => 'marker' );
has '+possible_Filters' => (
	default => sub {
		[
			qw(MarkerIdentifier ExportFormat CannedQuery)
		];
	}
);

has '+Filter_options' => ('default'=> sub {
	
	return {
		'ExportFormat' => {
			'label' => 'Export Marker as',
			'list' => [
				['html'=>'--choose a format --'],
				['yaml'=>'YAML File'],
				['xml'=>'XML File'],
				['json'=>'JSON file'],
			],
		},
	};
});

no Moose;

sub get_data {
	my ($self)    = @_;
	my $mi     = $self->f('MarkerIdentifier');
	my $marker = $mi->marker;
	!$marker and $self->throw("A Marker with identifier/accession '".$mi->value."' was not found");
	my $coord = $mi->coord;
	my $transcript_info;
	if ($coord) {
		#retrieve G2P association results where this marker is tested which pass the threshold
		my @features = $self->DS('Feature')->dbh->features(-types=>['mRNA','CDS','five_prime_utr','three_prime_utr'], -seqid=>"chr".$coord->chr, -start => $coord->start, -stop => $coord->stop);
	
		$transcript_info = separate_features($self->DS('Feature'), \@features, 1);
	}
	tie my %links, "Tie::Hash::Indexed";
	$links{'OMIM'}={'link' => "http://www.omim.org/search?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search=".$marker->accession};
	$links{'SNPedia'}={'link' => "http://www.snpedia.com/index.php/".$marker->accession };
	$links{'WAVe'}={'link' => "http://bioinformatics.ua.pt/WAVe/gene/".$marker->accession};
	$links{'dbSNP'} = {
				'link' => $marker->hotlinkid->urlprefix.$marker->accession,
				'label' => 'dbSNP',
				'target' => '_blank'
			};
	return {
		transcripts => $transcript_info,
		item => $marker,
		identifier => $marker->identifier,
		coord => $coord,
		links => \%links
	 };
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $ 

=cut

