# $Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Studies - Query studies


=head1 SYNOPSIS

    use GwasCentral::Search::Query::Studies;
    my $search = GwasCentral::Search::Query::Studies->new;
    $search->option('q','cancer');
    my @results = $search->results;


=head1 DESCRIPTION

This class performs a search across Studies using a query string supplied as a parameter (q). It will also take into account the threshold (t) level
when searching across a region, gene or marker.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Genes;
use strict;
use warnings;
use Moose;
use GwasCentral::Search::Util qw(new_Query);
use HTML::Strip;

extends qw(GwasCentral::Search::Query);

has '+url_path'=>('default' => 'genes');
has '+DataSource_name' => ('default'=>'Feature');
has '+fieldset_order' => ('default'=>sub { ['options','qfilter','browserpanel', 'paging', 'result_opts' ] });

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Number::Format qw(:subs);
use List::MoreUtils qw(none);
use Tie::Hash::Indexed;

has '+possible_Filters' => (
	default => sub {
		[qw(Page SortBy PageSize ExportFormat CannedQuery QueryFilter QueryString Threshold  OddsRatio FivePrimeFlank ThreePrimeFlank MarkerDisplay SearchSubmit AddedResultsets)
		];
	}
);
has '+Detector_name' => ('default'=>'Single');
has '+possible_DetectTypes' => (
	'default' => sub {
		[qw(Gene Region)];
	}
);

has '+Filter_options' => (
	'default' => sub {
		return {
			'SortBy' => {
				'list'    => [
					['gene_size' , 'Gene Size (descending)', 1, 1],
					['gene_size' , 'Gene Size (ascending)', 1, 0],
					['start' , 'Gene Start (descending)', 1, 1],
					['start' , 'Gene Start (ascending)', 1, 0],
					['name' , 'Gene Name (descending)', 0, 1],
					['name' , 'Gene Name (ascending)', 0, 0],
				],
				'default' => 'Gene Size (descending)',
			},
			'PageSize' => {
				'list'    => [
					['20', '20'],
					['50', '50'],
					['100', '100'],
					['all', 'all'],
				],
				'default' => '50'
			},
			'Page'        => { 'default' => 1 },
			'QueryFilter' => {
				'template' => 'geneorregion',
				'fieldset' => 'qfilter',
				'label' => 'Search for genes with markers',
				'value' => 'Gene',
				'dt_hidden' => {
					'None' => 1
				},
				'dt_labels' => {
					'Gene' => 'by HGNC gene symbol',
					'Region' => 'by genomic location'
				},
				'dt_onclick' => {
					'Gene' => 'showLociGene()',
					'Region' => 'showLociRegion()'
				}
			},
		};
	}
);
has 'get_data_method' => ( 'is' => 'rw', 'default' => 'get_gene_list' );
has '+Detector_name' => ('default' => 'Single');

override 'text' => sub {
	my ($self) = @_;
	my $qf = $self->f('QueryFilter');
	if ($qf->value eq 'Gene') {
		return $self->label." match gene symbol <b>".$self->fval('QueryString')."</b>";
	}
	else {
		return $self->label." found in genomic location <b>".$self->fval('QueryString')."</b>";
	}
};

after 'prepare_Filters' => sub {
	my ($self) = @_;
	my $qf = $self->f('QueryFilter');
	my $qs = $self->f('QueryString');
	my $label = $qf->dt_labels->{$qf->by_index(0)};
	$label=~s/by//;
	$qs->label("Enter $label");
};

sub convert_data_to_entries {
	my ( $self, $raw_genes ) = @_;

	my @genes = @{$raw_genes};

	return if scalar(@genes) == 0;
	my %queries = ();
	my @qtypes = qw(Phenotypes Markers Studies);
	foreach my $qtype(@qtypes) {
		$queries{$qtype} = new_Query("Genes::$qtype",{ DataSources=>$self->DataSources, conf_file => $self->config });
	}
	my $hs = HTML::Strip->new();
	foreach my $item (@genes) {
		my @related_data = ();
		foreach my $qtype(@qtypes) {
			my $query = $queries{$qtype};
			my %params = (
				'q'=>$qtype eq 'Studies' ? $item->{gene_symbol} : $item->{region},
				't' => $self->fval('Threshold'),
				'l'=>$self->fval('MarkerDisplay'),
				'qfilter' => $qtype eq 'Studies' ? 'Keyword' : 'Region',
				'gene' => $item->{gene_symbol}
			);
			
			$self->log->info("params:".Dumper(\%params));
			$query->params(\%params);
			$query->prepare_Filters;
			$query->results;
			
			push @related_data,{
				link => $query->result_count>0 ? $query->url : undef,
				label => $query->text_as_plain,
				title => $query->result_count>0 ? "View ".$qtype." for this gene" : undef,
				image => lc($qtype),
			};
		}
		$item->{related_data}=\@related_data;
		
		my @aliases = @{ $item->{aliases} || [] };
		tie my %links, "Tie::Hash::Indexed";
			
		foreach my $alias(@aliases) {
			if ($alias =~ /NM\_/) {
				$links{mRNA} = {id => $alias, link => "http://www.ncbi.nlm.nih.gov/nuccore/$alias" }
			}
			elsif ($alias =~ /NR\_/) {
				$links{ncRNA} = {id => $alias, link => "http://www.ncbi.nlm.nih.gov/nuccore/$alias" }
			}
		}
			
		$links{'OMIM'}={'link' => "http://www.omim.org/search?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search=".$item->{gene_symbol} };
		$links{'SNPedia'}={'link' => "http://www.snpedia.com/index.php/".$item->{gene_symbol} };
		$links{'WAVe'}={'link' => "http://bioinformatics.ua.pt/WAVe/gene/".$item->{gene_symbol} };
		$item->{links}=\%links;
	}

}

sub retrieve_region {
}

sub retrieve_gene {
}

sub retrieve_none {
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

