# $Id: All.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::All - Query studies, phenotypes and markers in one go


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::All;

	use Moose;
	extends qw(GwasCentral::Search::Query);

	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use Text::ParseWords;
	use GwasCentral::Base qw(load_module);
	
	has 'Queries' => ('is'=>'rw', 'default' => sub { {} });
	
	has '+possible_Filters' => (default => sub { 
		[qw(QueryString Concept Threshold RestrictToAnnotation ExportFormat AddedResultsets SearchSubmit)]
	});
		
	has '+possible_DetectTypes' => ('default'=>sub {
		[qw(ConceptTerm GeneRegion Marker Annotation Keyword None)];
	});
	
	has 'Query_names' => ('is'=>'rw', 'default'=>sub {
		[qw(Studies Phenotypes Markers Genes Genes::Studies Genes::Phenotypes Genes::Markers)]
	});
	
	sub BUILD {
		my ($self) = @_;
		
		foreach my $query_name(@{$self->Query_names}) {
			my $query = load_module("GwasCentral::Search::Query::$query_name", {
				conf_file => $self->config,
			});
			$self->Queries->{$query_name}=$query;
		}
	}
	
	override 'prepare_Filters' => sub { 
		my ($self) = @_;
		foreach my $query_name(@{$self->Query_names}) {
			my $query = $self->Queries->{$query_name};
			$query->DataSources($self->DataSources);
			$query->Detector_name('Multi');
			$query->params($self->params);
			$query->prepare_Filters;
			eval {
				$query->f('AddedResultsets')->template('hiddencsv');
			}
		}
		
		super();
	};
	
	sub results {
		my ($self) = @_;
		my %data_for=();
		
		foreach my $query_name(@{$self->Query_names}) {
			my $query = $self->Queries->{$query_name};
			$data_for{$query_name} = $query->results;
		}
		#my $first_query = $self->Queries->{$self->Query_names->[0]};
		#$self->recognised_as($first_query->recognised_as);
		return \%data_for;
	}


1;