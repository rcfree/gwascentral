# $Id$

=head1 NAME

HGVbaseG2P::Search::Performer::ID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Detector::None;
use Moose;
use strict;
use warnings;
extends qw(GwasCentral::Search::Detector);
use Data::Dumper qw(Dumper);

sub perform {
	my ($self) = @_;
	my $query = $self->Query;
	return $query->get_data;
}
