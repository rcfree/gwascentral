# $Id$

=head1 NAME

GwasCentral::Search::Performer::AutoDetect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Detector::Single;
use Moose;
has 'qfilter_pagers'=>('is'=>'rw', 'default' => sub { {} });

use strict;
use warnings;
extends qw(GwasCentral::Search::Detector);
use Data::Dumper qw(Dumper);

sub perform {
	my ($self) = @_;
	my $query = $self->Query;
	
	my $qs = $query->f('QueryString');
	my $qf = $query->f('QueryFilter');
	
	my $dt_mod;
	$self->throw("No 'possible_DetectTypes' defined in $self") if scalar(@{$query->possible_DetectTypes})==0;
	my %poss_dt = map { $_ => 1 } @{$query->possible_DetectTypes};
	my $dt = $qf->value;
	$self->throw("Detect type $dt not defined in 'possible_DetectTypes' for $self") if !$poss_dt{$dt};

	$dt_mod = $query->new_DetectType($dt);

	$self->log->debug("attempt to detect:$dt - '".($qs->value || "undef")."'");
	my $recog_as = $dt_mod->detect($qs->value);
	
	$query->DetectType($dt);
	$self->log->info("recognised_as:".($recog_as || 'undef').", module:".$query->DetectType.", qf_value:".($qf->value || 'undef'));
	if ($recog_as) {
		my $retrieve_method = "retrieve_".lc($recog_as);
		$query->$retrieve_method;
	}
	else {
		$query->pager(undef);
		$query->data([]);
		$query->no_results(1);
		if (!defined($qs->value)) {
			$query->no_options(1);
		}
		return [];
	}
	
	$query->recognised_as($qf->value);
	
	$self->throw("Could not detect your QueryString value (possible_detectors:".join(";",@{$query->possible_DetectTypes}).")") if !$query->recognised_as;			
	$query->data($query->get_data);
	$query->pager($query->pager);	

	return $query->data;
}
