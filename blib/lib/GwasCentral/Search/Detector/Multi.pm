# $Id$

=head1 NAME

GwasCentral::Search::Performer::AutoDetect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Detector::Multi;
use Moose;
has 'qfilter_pagers'=>('is'=>'rw', 'default' => sub { {} });

use strict;
use warnings;
extends qw(GwasCentral::Search::Detector);
use Data::Dumper qw(Dumper);

sub perform {
	my ($self) = @_;
	my $query = $self->Query;
	
	my $qs = $query->f('QueryString');
	my $qf = $query->f('QueryFilter');
	my $dt_mod;
	$self->throw("No 'possible_DetectTypes' defined in $self") if scalar(@{$query->possible_DetectTypes})==0;
	
	my %retrieved_data = ();
	my %retrieved_pager = ();
	my $is_recognised = 0;
	my $recog_as;
	$is_recognised=1 if $qf->value;
	foreach my $dt(@{$query->possible_DetectTypes}) {
		last if $query->lock_DetectType;
		$dt_mod = $query->new_DetectType($dt);
		#$recog_module->databases($self->databases);
		$self->log->debug("attempt to detect:$dt - '".($qs->value || "undef")."'");
		$recog_as = $dt_mod->detect($qs->value);
		if (!$is_recognised && $recog_as) {	
			$qf->value($dt);
			$is_recognised = 1;
		}
		$qf->value($dt);
		$query->DetectType($dt);
		$self->log->info("recognised_as:".($recog_as || 'undef').", module:".$query->DetectType.", qf_value:".($qf->value || 'undef'));
		if ($recog_as) {
			my $retrieve_method = "retrieve_".lc($recog_as);
			$query->$retrieve_method;
			$retrieved_data{$dt} = $query->get_data;
			$retrieved_pager{$dt}=$query->pager;
		}
		$qf->texts->{$dt}=$query->text;
	}
	$query->recognised_as($qf->value);
	
	$self->throw("Could not detect your QueryString value (possible_detectors:".join(";",@{$query->possible_DetectTypes}).")") if !$query->recognised_as;
	
	#$self->log->info("Pager:".Dumper([map { $_->total_entries } values %retrieved_pager]));
			
#	$query->data(\@raw_results);
	$query->pager($retrieved_pager{$qf->value});
	$query->data($retrieved_data{$qf->value});
	$qf->pagers(\%retrieved_pager);
	return $query->data;
}
