# $Id$

=head1 NAME

GwasCentral::Search::Detect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Detector;
use Moose;
extends qw(GwasCentral::Base);
has 'Query' => ('is'=>'rw');

sub has_config {
	return undef;
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name =~ /GwasCentral::Search::Detector::(.+)/;
	return $name;
}

1;