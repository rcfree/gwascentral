package GwasCentral::Search::Lookup;
use Moose;
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);

sub autocomplete {
	my ( $self, $attrs ) = @_;

	my $value         = $attrs->{value};
	my $rows          = $attrs->{rows} || 10;
	my $sorted        = $attrs->{sorted};
	my %do_lookup_for = map { $_ => 1 } @{ $attrs->{lookup_sources} || [] };

	my $querystring = "$value%";

	my $hpoactive = $self->config->{hpo};

	my @rows = ();

	if ( $do_lookup_for{ontology} ) {
		if ($hpoactive) {
			@rows = (
				@rows, $self->autocomplete_query( "HPO", $querystring, "Hpo" )
			);
			@rows = (
				@rows,
				$self->autocomplete_query( "HPO syn", $querystring, "Hpo" )
			);
		}

		@rows = (
			@rows,
			$self->autocomplete_query( "MeSH", $querystring, "Ontology" )
		);
	}

	if ( $do_lookup_for{features} ) {
		my $fdbh = $self->DS('Feature')->dbh->dbh;
		my $sth  = $fdbh->prepare(
			"select distinct name from name where name like ? limit 0,10");
		$sth->execute("$value%");
		my @data = map { $_->[0] . "|Feature" } @{ $sth->fetchall_arrayref };
		@rows = ( @rows, @data );
	}

	if ( $do_lookup_for{markers} ) {
		my $mdbh =
		  $self->DS('Marker')
		  ->dbh->resultset('Marker');
		my @markers =
		  map { $_->identifier . "|Marker" }
		  $mdbh->search( { 'Identifier' => { 'like' => "$value%" } },
			{ page => 1, rows => $rows } )->all;
		@rows = ( @rows, @markers );
		my @markers2 =
		  map { $_->accession . "|Marker" }
		  $mdbh->search( { 'Accession' => { 'like' => "$value%" } },
			{ page => 1, rows => $rows } )->all;
		@rows = ( @rows, @markers2 );
	}

#	  	if ($do_lookup_for{studies}) {
#		  	my $sdbh = $c->model('StudyDB')->dbh->resultset('Study');
#		  	my @studies = map { '"'.$_->name." (".$_->identifier.")".'"'."|Study " } $sdbh->search(-or=>[{'Identifier' => {'like' => "$value%" }},{'Name' => {'like' => "%$value%" }}], {page=>1, rows=>$rows})->all;
#		  	@rows = (@rows, @studies);
#		  	#my @studies2 = map { $_->name."|Study" } $sdbh->search({'Name' => {'like' => "$value%" }}, {page=>1, rows=>$rows})->all;
#		  	#@rows = (@rows, @studies2);
#	  	}

	my @sorted = $sorted ? reverse sort @rows : @rows;
	if ( scalar(@sorted) > $rows ) {
		return @sorted[ 0 .. $rows - 1 ];
	}
	else {
 		push @sorted, "[no suggestions] | " if (scalar(@sorted)==0);
		return @sorted;
	}

}

sub autocomplete_query {
	my ( $self, $db, $query, $usemodel ) = @_;
	my $htmlstring = '';

	# Search specific methods and column names
	my %search_method = (
		'MeSH'    => 'get_mesh_term_from_name',
		'HPO'     => 'get_term_from_name',
		'HPO syn' => 'get_synonym_from_name',
	);
	my %money_col = (
		'MeSH'    => 'termname',
		'HPO'     => 'name',
		'HPO syn' => 'synonymtext',
	);

	my $usesearch = $search_method{$db};
	my $usecol    = $money_col{$db};

	my $concept_rs = $self->DS($usemodel)->$usesearch($query);

	my @rows = map { $_->$usecol . "|" . $db } $concept_rs->all;

	return @rows;
}

sub category_to_headings {
	my ($self, $category) = @_;
	# first see if the category is in MeSH
		my $search_rs = $self->DS("Ontology")->get_mesh_headings_from_terms($category);
		my %uniquehash = ();
 		while (my $headings = $search_rs->next){
 			
 			#see if we annotate to the submitted concept
 			my $ipid= $headings->get_column('heading_meshid');
 			
 			my $initialsearch_rs = $self->DS("Study")->count_phenotypeannotations_by_phenotypeidentifier($ipid);
 			
 			if ($initialsearch_rs ne '0'){
				$uniquehash{$ipid}='1';
			}
 			
 			my $treestring=$headings->get_column('treeid').".%";
 			
 			my $search2_rs = $self->DS("Ontology")->get_mesh_children_from_treeid($treestring);

  			while (my $subterms = $search2_rs->next){
  				my $pid=$subterms->meshid;
  				
  				my $searchstudy_rs = $self->DS("Study")->count_phenotypeannotations_by_phenotypeidentifier($pid);
  				
				if ($searchstudy_rs ne '0'){
					$uniquehash{$pid}='1';
				}
  			}	
 		}
 		
 		# second see if the category is in HPO
		my $hposearch_rs = $self->DS("Ontology")->get_single_term_from_name($category);
		
 		while (my $hpoterms = $hposearch_rs->next){
 			
 			# see if we annotate to the initial term
 			my $ipid= $hpoterms->get_column('hpoid');
 			
 			my $preinitialsearch_rs = $self->DS("Study")->count_phenotypeannotations_by_phenotypeidentifier($ipid);
 			
 			if ($preinitialsearch_rs ne '0'){
					$uniquehash{$ipid}='1';
			}
 			
 			# get all chidren for the term and see if we annotate to those
 			my $search_rs2 = $self->DS("Hpo")->get_children_terms_from_id($ipid);
 			
 			while (my $allhpoterms = $search_rs2->next){
 				
 				my $ipid= $allhpoterms->get_column('hpoid');
 				
 				my $initialsearch_rs = $self->DS("Study")->count_phenotypeannotations_by_phenotypeidentifier($ipid);
 			
 				if ($initialsearch_rs ne '0'){
					$uniquehash{$ipid}='1';
				}
 			
 		
  			}	
 		}
 		return keys %uniquehash;
}

1;