# $Id: RegexFields.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::RegexFields - Rule to do regex on fields


=head1 SYNOPSIS

TODO

=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::RegexFields;
use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Rule);
no Moose;
use Data::Dumper qw(Dumper);

sub run {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $regex = $self->settings->{'regex'};

	#foreach my $panel(@{$self->settings->{panels}}) {
	foreach my $field ( @{ $self->settings->{'fields'} } ) {
		my $content = $line->{$field};
		if ( !$content ) {
			$self->log->info( "$field not present" . Dumper($line) );
		}
		$content =~ /$regex/;
		$line->{$field} = $1;
	}

}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: RegexFields.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

