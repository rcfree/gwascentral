# $Id: Custom.pm 1301 2009-08-25 12:42:40Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Rule::StrandFlip::Custom - Strand flipping depending on 'flip' flag in element


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('StrandFlip::Custom');
	

=head1 DESCRIPTION

Flips the alleles if the 'flip' flag is set to 1 in the DataElement.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::StrandFlip::Custom;

	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::DataImport::Rule::StrandFlip);
	
	use List::Compare;
	
	use Data::Dumper qw(Dumper);
	
	sub run {
		my ($self, $element) = @_;

		#do strand flip if element status set to flip
		if ($element->flip) {
			$self->strand_flip($element->line);
		}
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: Custom.pm 1301 2009-08-25 12:42:40Z rcf8 $ 

=cut