# $Id: CalcAlleleFreqsFromNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::CalcAlleleFreqsFromNumbers - PanelRule to calculate allele frequencies from allele numbers


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('CalcAlleleFreqsFromNumbers');
	

=head1 DESCRIPTION

Uses allele frequencies to calculate allele numbers and deals with multiple panels.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::CalcAlleleFreqsFromNumbers;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::PanelRule);
no Moose;
use Data::Dumper qw(Dumper);

sub run_multi_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	foreach my $panel ( @{ $self->panelnames } ) {

		my $al_no1   = $line->{"allele1_number:$panel"};
		my $al_no2   = $line->{"allele2_number:$panel"};
		my $al_total = $al_no1 + $al_no2;

		$line->{"allele1_freq:$panel"} = sprintf( "%.3f", $al_no1 / $al_total );
		$line->{"allele2_freq:$panel"} = sprintf( "%.3f", $al_no2 / $al_total );
		$line->{"allele_total:$panel"} = $al_total;
	}
}

sub run_single_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $al_freq1 = $line->{"allele1_number"};
	my $al_freq2 = $line->{"allele2_number"};

	my $al_total = $al_freq1 + $al_freq2;

	$line->{"allele1_freq"} = sprintf( "%.3f", $al_freq1 / $al_total );
	$line->{"allele2_freq"} = sprintf( "%.3f", $al_freq2 / $al_total );
	$line->{"allele_total"} = $al_total;

}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: CalcAlleleFreqsFromNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

