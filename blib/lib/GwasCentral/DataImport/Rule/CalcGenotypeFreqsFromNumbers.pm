# $Id: CalcGenotypeFreqsFromNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::CalcGenotypeFreqsFromNumbers - PanelRule to calculate genotype frequencies from genotype numbers


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('CalcGenotypeFreqsFromGenotypeNumbers');
	

=head1 DESCRIPTION

Uses genotype frequencies to calculate genotype numbers and deals with multiple panels.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::CalcGenotypeFreqsFromNumbers;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::PanelRule);
no Moose;
use Data::Dumper qw(Dumper);

sub run_multi_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	foreach my $panel ( @{ $self->panelnames } ) {

		my $gt_freq11 = $line->{"genotype11_number:$panel"};
		my $gt_freq12 = $line->{"genotype12_number:$panel"};
		my $gt_freq22 = $line->{"genotype22_number:$panel"};
		my $gt_total  = $gt_freq11 + $gt_freq12 + $gt_freq22;

		$line->{"genotype11_freq:$panel"} =
		  sprintf( "%.3f", $gt_freq11 / $gt_total );
		$line->{"genotype12_freq:$panel"} =
		  sprintf( "%.3f", $gt_freq12 / $gt_total );
		$line->{"genotype22_freq:$panel"} =
		  sprintf( "%.3f", $gt_freq22 / $gt_total );
		$line->{"genotype_total:$panel"} = $gt_total;
	}
}

sub run_single_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $gt_freq11 = $line->{"genotype11_number"};
	my $gt_freq12 = $line->{"genotype12_number"};
	my $gt_freq22 = $line->{"genotype22_number"};
	my $gt_total  = $gt_freq11 + $gt_freq12 + $gt_freq22;

	$line->{"genotype11_freq"} = sprintf( "%.3f", $gt_freq11 / $gt_total );
	$line->{"genotype12_freq"} = sprintf( "%.3f", $gt_freq12 / $gt_total );
	$line->{"genotype22_freq"} = sprintf( "%.3f", $gt_freq22 / $gt_total );
	$line->{"genotype_total"}  = $gt_total;

}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: CalcGenotypeFreqsFromNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

