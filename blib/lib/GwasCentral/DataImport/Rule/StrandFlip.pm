# $Id: StrandFlip.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Rule::StrandFlip - Base class for DataImport Strandflip Rule


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::Rule::StrandFlip);

	#overrides base Rule method
	sub run {
		my ($self,$element);
		my $line = $element->line;
		... determine whether to flip.. and
		$self->strand_flip($line);
	}


=head1 DESCRIPTION

Provides a common strand_flip method which is used to flip alleles in 'allele1' and 'allele2'. Various child modules based on this class provide differnet
functionalities. The StrandFlip child module used is determined by the LogicMaker module.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Rule::StrandFlip;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Rule);
no Moose;
use List::Compare;

use Data::Dumper qw(Dumper);

=head2 strand_flip

  Usage      : $self->strand_flip($line);
  Purpose    : Flips alleles in DataElement line
  Returns    : Nowt
  Arguments  : DataElement line
  Throws     :
  Status     :
  Comments   :

=cut

sub strand_flip {
	my ( $self, $line ) = @_;

	my $allele1   = $line->{allele1};
	my $allele2   = $line->{allele2};
	my $a1_revcom = $self->revcom($allele1);
	my $a2_revcom = $self->revcom($allele2);

	#$self->log->info( "flip: $allele1:$allele2 => $a1_revcom:$a2_revcom" );
	$line->{allele1} = $a1_revcom;
	$line->{allele2} = $a2_revcom;
}

=head2 revcom

  Usage      : my $allele_revcom = $self->revcom($a1);
  Purpose    : To reverse complement a sequence
  Returns    : a scalar string
  Arguments  : a string of DNA
  Throws     :
  Status     :
  Comments   : * To Do, add the UPAC codes *

=cut

sub revcom {
	## Reverse compliment a DNA sequence.
	my ( $self, $str ) = @_;
	$str =~ tr/acgtnACGTN/tgcanTGCAN/;
	$str = reverse $str;
	return ($str);
}

sub get_ref_strand {

	my ( $self, $strand_info ) = @_;

	if ($strand_info) {
		if ( $strand_info eq "+" || $strand_info eq "+1" ) {
			return "1";
		}
		elsif ( $strand_info eq "-" || $strand_info eq "-1" ) {
			return "-1";
		}
		else {
			return $strand_info;
		}
	}
	else {
		return undef;
	}
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: StrandFlip.pm 1515 2010-07-13 13:52:11Z rcf8 $

=cut

