# $Id: Rule.pm 1519 2010-08-02 14:42:28Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Rule - Base class for DataImport Rule


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::Rule);

	#overrides base Rule method
	sub run {
		my ($self,$element);
		my $line = $element->line;
		... do stuff with line
	}


=head1 DESCRIPTION

Rules are simple algorithms performed upon DataElements.
For example:
1) Check that a line field has the same value as a database field (e.g. marker position is same as file position)
2) Calculate missing data (e.g. allele total can be calculated from the allele1 and allele2 frequencies)

Base class providing interface for Rule modules. Generally the 'run' method is overridden to provide a single rule based on the supplied
DataElement.

Other methods may be optionally overridden.

Rules are generally created using LogicMaker->import_rule which allows the user to specify various settings etc.

Rules provide the core logic for the DataImport pipeline and are added according to complex logic in the LogicMaker module.
To extend the pipeline it is suggested that a Plugin or template-based code hook is used rather than a new Rule.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Rule;

	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::Base);

	use English qw( -no_match_vars );
	use Data::Dumper;

	has 'settings' => ( 'is' => 'rw', 'default' => sub { {} } );


	sub has_config { 0; }


=head2 run

Runs rule upon a supplied DataElement

=cut

	sub run {
		my ($self,$element)=@_;
		die "'run' method in Rule ".ref($self)." not defined";
	}

=head2 after_rules

Run after all 'run' methods have completed. Generally used to check things after all rules have run. For example that the correct fields are present.

=cut

	sub after_Rules {
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: Rule.pm 1519 2010-08-02 14:42:28Z rcf8 $

=cut