# $Id: Export.pm 1646 2011-04-05 16:14:08Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Export - Base class for DataImport Export


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::Export);

	#overrides base export method
	sub output {
		my ($self, $lines);
		my @lines = @{$lines};
		... deal with batch of lines
	}


=head1 DESCRIPTION

Base class providing interface for Export modules. Generally the 'output' module must be overridden to deal with lines of data.
Other methods may be optionally overridden.


=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Target;

	use strict;
	use warnings;

	use FindBin;
	use lib "$FindBin::Bin/../../";
	use Moose;
	extends qw(GwasCentral::Base);

	use English qw( -no_match_vars );
	use Data::Dumper;
	use GwasCentral::Base qw(load_module);
	has 'template' => ('is'=>'rw');
	has 'panelnames' => ('is'=>'rw');
	has 'frequencies' => ('is'=>'rw');
	has 'associations' => ('is'=>'rw');
	has 'settings' => ('is'=>'rw', 'default' => sub { {} });
	has 'resultset_lookup' => ('is'=>'rw');
	has 'hlc_lookup' => ('is'=>'rw');
	has 'resultset_field' => ('is'=>'rw');
	has 'resultsets' => ('is'=>'rw');
	
	sub has_config { 0; }

	sub init
	{
		my ( $self, $template, $panelnames ) = @_;
		$self->multi_panel( $panelnames ? 1 : undef );
		$panelnames and $self->panelnames($panelnames);
		$self->associations( $template->{associations} );
		$self->frequencies( $template->{frequencies} );

		#$export->settings->{'study_identifier'}      = $self->get_study;
		#$export->settings->{'experiment_identifier'} = $self->get_experimentid;
		$self->template($template);

#get resultsets or undef if supplied as a field (resultset_id or resultset_name)
		if ( $self->template->{associations} )
		{

			#try to get resultsets from fields (in form pvalue:HGVRSxxx)
			#my $resultsets = $self->get_resultsets_from_fields;
			#look for a template setting for a resultset
			my $template_resultset = $template->{resultset_identifier};

			#if is a template resultset set up export module
			#otherwise look for a field containing it
			if ($template_resultset)
			{
				$self->settings->{'resultset_identifier'} = $template_resultset;
				$self->resultset_lookup('identifier');
				$self->log->info("resultset_lookup:identifier");
			}

			#look for a template setting for a hotlinkcollection
			my $template_hlc = $template->{hlc_identifier};

			#if is a template resultset set up export module
			#otherwise look for a field containing it
			if ($template_hlc)
			{
				$self->settings->{'hlc_identifier'} = $template_hlc;
				$self->hlc_lookup('identifier');
				$self->log->info("hlc_lookup:identifier");
			}

#if multiple resultsets in single line (eg. multiple pvalues) add this to the export module
#			else {
#				$self->resultsets($resultsets);
#				$self->log->info(
#					"pvalues in multiple fields:" . Dumper($resultsets) );
		}
	}

	#standard output used by all Target module types..
	sub output
	{
		my ( $self, $elements ) = @_;
		$self->log->info("gets to output");

		my ( $study, $experiment, $resultset, $apanel );
		foreach my $element ( @{$elements} )
		{
			my $line   = $element->line;
			my $marker = $element->marker;
			my ( $sid, $eid ) =
			  $self->get_study_and_experiment_idents($line);
			my ( $study, $experiment ) =
			  $self->get_study_and_experiment( $sid, $eid );
			my $ums = $self->export_usedmarkerset( $experiment, $marker );
			next if !$ums;

			#deal with frequency based info
			my @fcs = ();
			if ( $self->frequencies )
			{


#if multiple panels in a line generate frequency cluster for each and return an array of frequencyclusterids
				if ( $self->multi_panel )
				{
					foreach my $apanel_name ( @{ $self->panelnames } )
					{
						my $apanel = $self->get_apanel( $apanel_name, $sid );
						my $freq_cluster =
						  $self->generate_frequencycluster( $element,
															$apanel_name );
						my $fc =
						  $self->export_frequencycluster( $ums, $freq_cluster,
														  $apanel );
						$self->log->debug(
										   "Created frequencycluster for "
											 . $marker->display_name,
										   " panel $apanel_name"
						);
						push @fcs,
						  { frequencyclusterid => $fc->frequencyclusterid };
					}
				} else
				{
					#check to see if same panel
					my $apanel_name = $line->{panelname};
					my $apanel = $self->get_apanel( $apanel_name, $sid );
					my $freq_cluster =
					  $self->generate_frequencycluster( $element,
														$apanel_name );
					my $fc =
					  $self->export_frequencycluster( $ums, $freq_cluster,
													  $apanel );
					$self->log->debug(
						"Created frequencycluster for " . $marker->display_name,
						" panel $apanel_name"
					);
					push @fcs,
					  { frequencyclusterid => $fc->frequencyclusterid };
				}
			}


			#deal with association based info
			if ( $self->associations )
			{	
				if ($self->resultsets) {
					foreach my $rs_ident(@{$self->resultsets}) {
						my $rs = $self->get_resultset($rs_ident);
						$self->export_significances( $element, $ums, $rs, \@fcs);
					}
				}
				else {
				#hotlinkcollection
				#my $hlc_identifier         = $self->get_hlc_identifier($line);
                #my $hlc = $self->get_hlc($hlc_identifier);
                my $resultsetid = $self->get_resultsetid($line);
					my $resultset   = $self->get_resultset($resultsetid);
					$self->export_significances( $element, $ums, $resultset, \@fcs);
				}
				#	$hlc );
			}
		}
	}    #this is end of the sub

=head2 get_resultset

	  Usage      : my ($resultset) = $self->get_resultset($element->line,$experiment,$rs_identifier);
	  Purpose    : Gets resultset from the database or the line
	  Returns    : GwasCentral::Schema::Resultset
	  Arguments  : DataElement line; GwasCentral::Schema::Experiment object; Resultset Identifier
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

	sub get_resultsetid
	{
		my ( $self, $line ) = @_;

#if multiple_resultsets then get the resultset from the experiment using the identifier
		my $resultsetid =
		    $self->resultset_field
		  ? $line->{ $self->resultset_field }
		  : $self->template->{'resultset_identifier'};
		return $resultsetid;

		#}
	}

	sub get_study_and_experiment_idents
	{
		my ( $self, $line ) = @_;

#NOTE: may need to rewrite to deal with lookups by name/title etc (see resultsets)
#get studyid from template or line
		my $studyid = $self->template->{'study_identifier'}
		  || $line->{study_identifier};

		#get experimentid from template or line
		my $experimentid = $self->template->{'experiment_identifier'}
		  || $line->{experiment_identifier};
		return ( $studyid, $experimentid );
	}

=head2 get_hlc_identifier

      Usage      : my ($hlc_identifier) = $self->get_hlc_identifier($element->line);
      Purpose    : Gets hlc_identifier from the database or the line
      Returns    : GwasCentral::Schema::HotlinkCollection
      Arguments  :
      Throws     :
      Status     : Public
      Comments   :

=cut

	sub get_hlc_identifier
	{
		my ( $self, $line ) = @_;

#NOTE: may need to rewrite to deal with lookups by name/title etc (see resultsets)
#get studyid from template or line
		my $hlc_identifier = $self->template->{'hlc_identifier'}
		  || $line->{hlc_identifier};

		return ($hlc_identifier);
	}

=head2 generate_frequencycluster

	  Usage      : my $fc = $self->store_frequencycluster($element, $panelname, $study, $ums);
	  Purpose    : Create/find frequencycluster (including allele/genotype frequencies) for a single panel
	  Returns    : GwasCentral::Schema::Frequencycluster
	  Arguments  : DataElement object; Assayedpanel name; GwasCentral::Schema::Study object; GwasCentral::Schema::Usedmarkerset object;
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

	sub generate_frequencycluster
	{
		my ( $self, $element, $apanel_name ) = @_;
		my $marker = $element->marker;
		my $line   = $element->line;

		#get flanking sequences for combos
		my @upstream30bp   = $marker->get_tag_values('upstream30bp');
		my @downstream30bp = $marker->get_tag_values('downstream30bp');
		my ( $afs, $gfs ) =
		  $self->generate_frequencies(
									   $apanel_name,     $line,
									   $upstream30bp[0], $downstream30bp[0],
									   $self->multi_panel
		  );
		my $pvaluehwe =
		  $line->{ 'pvaluehwe:'
			  . ( $self->multi_panel ? ":$apanel_name" : "" ) };
		my $total_field =
		  "genotype_total" . ( $self->multi_panel ? ":$apanel_name" : "" );
		my $freq_cluster = {
							 allelefrequencies        => $afs,
							 genotypefrequencies      => $gfs,
							 numberofgenotypedsamples => $line->{$total_field},
							 pvaluehwe                => $pvaluehwe,
		};
		return $freq_cluster;
	}

=head2 generate_frequencies

	  Usage      : my ( $afs, $gfs ) = $self->generate_frequencies(
			            'CONTROL',         $element->line, $upstream30bp[0],
			            $downstream30bp[0], $self->multi_panel
			        );
	  Purpose    : Generate hashrefs containing allele and genotype frequency data using the assayedpanel name, flanks and line
	  Returns    : Allele frequency hashref; genotype frequency hashref
	  Arguments  : Panel name; DataElement line; upstream 30bp flank;downstream 30bp flank; multipanel flag
	  Throws     :
	  Status     : Public
	  Comments   : Uses the multipanel flag to determine which field in the line to use to retrieve frequencies

=cut

	sub generate_frequencies
	{
		my ( $self, $panelname, $line, $upstream30bp, $downstream30bp,
			 $multi_panel )
		  = @_;
		my @afs = ();
		foreach my $allele ( 1 .. 2 )
		{
			my $temp_af = {
							allelecombo => substr( $upstream30bp, -10 ) . "("
							  . $line->{"allele$allele"} . ")"
							  . substr( $downstream30bp, 0, 10 ),
							frequencyasproportion => $line->{
								    "allele"
								  . $allele . "_freq"
								  . ( $multi_panel ? ":$panelname" : "" )
							  }
			};
			push @afs, $temp_af;
		}
		my @gfs = ();
		foreach my $gt (qw(11 12 22))
		{
			my $temp_gf = {
							genotypecombo => substr( $upstream30bp, -10 ) . "["
							  . $line->{"genotype$gt"} . "]"
							  . substr( $downstream30bp, 0, 10 ),
							frequencyasproportion => $line->{
								    "genotype"
								  . $gt . "_freq"
								  . ( $multi_panel ? ":$panelname" : "" )
							  },
							numbersampleswithgenotype => $line->{
								    "genotype"
								  . $gt
								  . "_number"
								  . ( $multi_panel ? ":$panelname" : "" )
							  }
			};
			push @gfs, $temp_gf;
		}
#		$self->log->info("afs:".Dumper(\@afs));
#		$self->log->info("gfs:".Dumper(\@gfs));
		return ( \@afs, \@gfs );
	}
	
	sub get_study_and_experiment {
		my ($self) = @_;
		$self->log->info("Method 'get_study_and_experiment' not implemented");
	}
	sub export_usedmarkerset {
		my ($self) = @_;
		$self->log->info("Method 'export_usedmarkerset' not implemented");
	}
	sub export_significances {
		my ($self) = @_;
		$self->log->info("Method 'export_significances' not implemented");
	}
	sub get_resultset {
		my ($self) = @_;
		$self->log->info("Method 'get_resultset' not implemented");
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: Export.pm 1646 2011-04-05 16:14:08Z rcf8 $

=cut
