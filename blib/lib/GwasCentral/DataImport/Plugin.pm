# $Id: Plugin.pm 1519 2010-08-02 14:42:28Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Plugin - Base class for DataImport Plugin


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::Plugin);

	#overrides base process_before_marker method
	sub process_before_marker {
		my ($self,$element);
		my $line = $element->line;
		... do stuff with line
	}


=head1 DESCRIPTION

Plugins provide ways of adding additional DataImport to the DataImport pipeline.
They are added to the template as a space separated list:
e.g.
to load plugins named GwasCentral::DataImport::Plugin::DoSomething and GwasCentral::DataImport::Plugin::DoSomethingElse specify:

plugins = DoSomething DoSomethingElse

Plugins allow you to process row data at four key points using four methods. This module provides an interface for these methods.
Any can be overloaded as required.

before - usually used to set up something eg. a database connection, or a list from a file

process_before_marker - run before RetrieveMarker is called (for example this could be used to lookup rs IDs which are then added to the line)

process_after_marker - run after RetrieveMarker is called, but before the rules are called

after - usually used to deinitialise something eg. a disconnect from a database, close a file handle

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Plugin;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::Base);

use English qw( -no_match_vars );
use Data::Dumper;

sub has_config { 0; }

sub before_all {

}

sub process_before_marker {

}

sub process_after_marker {

}

sub after_all {

}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: Plugin.pm 1519 2010-08-02 14:42:28Z rcf8 $

=cut

