# $Id: Core.pm 1646 2011-04-05 16:14:08Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Core - High level logic for data DataImport and import pipeline


=head1 SYNOPSIS

	#for dbGaP import of a file
	use GwasCentral::Browser::Core;

	my $validator = GwasCentral::DataImport::Core->new({
				  conf_file =>
					"/Users/robf/Projects/GwasCentral/perl/conf/hgvbase.conf",
				  argv      => \%ARGV,
				  template_file => '../conf/import/dbGaP.conf',
	  });

	  $validator->process_file($ARGV{sourcefile});
	  $logger->info("lines successful:\n".$validator->marker_success);

      my $marker_stats  = $validator->marker_failed;

=head1 DESCRIPTION

The DataImport::Core module ties together the logic of RetrieveMarker, Rulebook, Plugin::* and target::* modules.

The separation of core logic makes it easier to test the module by use of regression/unit testing. It also provides the option to
add additional Rule modules or a different target module.

See GwasCentral::DataImport for more information on the principles behind the import system and the helper classes involved.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Core;

	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::Base);
	with qw(GwasCentral::DSContainer);
	
	use English qw( -no_match_vars );
	use Data::Dumper;
	use FindBin;
    use lib "$FindBin::Bin/../../lib";
	use GwasCentral::DataSource::Study;
	use GwasCentral::DataSource::Browser;
	use GwasCentral::DataImport::RetrieveMarker;
	use GwasCentral::FileParser;
	use GwasCentral::Base qw(load_config load_module);
	use GwasCentral::DataImport::DataElement;
	use GwasCentral::DataImport::Rulebook;


	# Private variables & constants
	has 'template' => ('is'=>'rw');    #contains import config describing data file format etc.
	has 'Rules' => ('is'=>'rw'); 
	  ; #contains arrayref of 'GwasCentral::DataImport::Rule's to use on each line
	has 'Plugins' => ('is'=>'rw'); 
	has 'error_stats' => ('is'=>'rw'); 
	has 'db' => ('is'=>'rw'); 
	my $line_count;
	has 'RetrieveMarker' => ('is'=>'rw');
	has 'fields' => ('is'=>'rw');
	has 'accession_source' => ('is'=>'rw');
	has 'comment_prefix' => ('is'=>'rw');
	has 'field_separator' => ('is'=>'rw');
	has 'FileParser' => ('is'=>'rw');
	has 'Target' => ('is'=>'rw');
	has 'head' => ('is'=>'rw');
	has 'marker_failed' => ('is'=>'rw');
	has 'marker_success' => ('is'=>'rw');
	has 'line_dump' => ('is'=>'rw');
	has 'chr_limit' => ('is'=>'rw');
	has 'Rulebook' => ('is'=>'rw');
	has 'argv' => ('is'=>'rw');
	has 'template_file' => ('is'=>'rw');
	
	my $SUMMARY_NO = 1000;
	my $BATCHING = 1;
	my $BATCH_SIZE = 1000;
	my @elements=();


	sub BUILD {
		my ( $self, $args ) = @_;

		if ( $self->template_file ) {
			my %template = load_config( $self->template_file, 1 );
			$self->template( \%template );
		}
		$self->populate_DS_from_list("Study","Marker","Browser");
		my $retrieve_marker =
		  GwasCentral::DataImport::RetrieveMarker->new(
			{ conf_file => $self->config } );
		$retrieve_marker->accession_source( $self->template->{'accession_db'} || $self->template->{'accession_source'});
		
		$retrieve_marker->use_DS($self);
		$self->RetrieveMarker($retrieve_marker);

		my $file_parser =
		  GwasCentral::FileParser->new( { conf_file => $self->config } );
		$self->FileParser($file_parser);

		my $rbook =
		  GwasCentral::DataImport::Rulebook->new( { template => $self->template } );

		$self->Rulebook($rbook);
		$self->marker_failed( [] );
		$self->marker_success(0);
		$self->line_dump($self->{argv}->{l});
		$self->chr_limit($self->{argv}->{'ref'});
	}

	sub has_config { 1; }

=head2 process_file

  Usage      : $gtimport->process_file('/path/to/gtdatafile.xml')
  Purpose    : Main method for processing an input gt-data file from a particular source
  Returns    : undef if non-fatal errors are encountered during run, dies on fatal errors
  Arguments  : Path to input file.
  Throws     :
  Status     : Public
  Comments   :

=cut

	sub process_file  {
		my ( $self, $filename ) = @_;
		my @startTime  = localtime(time);
		my $line_count = 0;
		my $template   = $self->template;

		$self->setup;

		my $target = $self->Target;

		#run 'before' method for each plugin if it exists
		if ($self->Plugins) {
			foreach ( @{ $self->Plugins } ) {
				$_->can("before_all") and $_->before_all($self);
			}
		}

		$target->begin;
		$self->run_codehook('before',[$self]);

		my $line_code;
		if ( $template->{line_code} ) {
			"eval '$line_code = sub {" . $template->{line_code} . "}'";
		}

		my $handler = sub {
			my ($line) = @_;

  #process line into fields hashref and pass this to preprocess and process line
			my $fields = $self->line_to_fields(@_);

			if ($self->chr_limit) {

				if ($fields->{chromosome} ne $self->chr_limit) {
					$self->log->info("ignore ".$fields->{accession});
					return;
				}

			}

			#ignore line if header or comment
			if ( $self->comment_prefix
				and substr( $line->[0], 0, 1 ) eq $self->comment_prefix )
			{
				$self->log->info(
					"Comment line - ignored:" . substr( $line->[0], 0, 30 ) );
				return;
			}

			if ( $self->head && $self->head > 0 ) {
				my $head = $self->head;
				$self->head( --$head );
				$self->log->info("line $line_count ignored");
				return;
			}

   #run 'process' method for each plugin prior to processing each line if exists
			if ( $self->Plugins ) {
				foreach my $plugin ( @{ $self->Plugins } ) {
					$plugin->process_before_marker( $fields, $self );

				}
			}
			$self->run_codehook('process_before_marker',[$fields,$self]);
			$self->log->info("process line $line_count");
			$self->process_line($fields,$line_count);

			$line_count++;
			$line_count % $SUMMARY_NO == 0
			  and $self->log->info("$line_count entries processed");
		};
		$self->log->info("parse file");
		$self->FileParser->parse_file({
			filename  => $filename,
			filetype  => 'delimited',
			handler   => $handler,
			field_sep => $self->field_separator,
			limit     => $template->{limit},
			line_code => $line_code,
			head      => $template->{head},
		});


		if ($BATCHING && scalar(@elements)>0) {
			$self->log->info(time." Started storing final batch of ".scalar(@elements)." entries");
			$self->Target->output(\@elements);
			$self->log->info(time." Finished storing final batch of ".scalar(@elements)." entries");
			@elements=();
		}


		#run 'after' method for each plugin if it exists
		if ( $self->Plugins ) {
			foreach ( @{ $self->Plugins } ) {
				$_->can("after_all") and $_->after_all($self);
			}
		}
		$self->run_codehook("after",[$self]);

		if ($self->argv->{n}) {
			$self->log->info("Data NOT committed. -n flag set");
			$target->rollback;
			$target->after;
		}
		else {
			$self->log->info("Data committed. No -n flag set");
			$target->commit;
			$target->after;
		}

		my @endTime = localtime(time);

		#$self->log->info("Disconnect from database");
#		eval {
#			$self->db->disconnect;    # Finally explicitly close database
#		};
#		if ($@) {
#			$self->log->info("Error disconnecting database:".$@);
#		}
	}

	sub setup {
		my ($self) = @_;

		my $rbook = $self->Rulebook;
		$self->Rules( $rbook->prepare_Rules($self->template) );

		#get panels or undef if supplied as a field (panelname)
		my $panelnames = $rbook->panelnames;

		#get resultsets or undef if supplied as a global (resultset_identifier)
		my $resultsets = $rbook->resultsets;

		my $target = $rbook->prepare_Target;
		$target->setup($self);

		$self->Target($target);

		$self->Plugins($self->import_Plugins);

		#cycle through simple options and set parameters based on those found
		# -if freq_fields and assoc_fields set combined to 0 else 1
		$self->comment_prefix( $self->template->{comment_prefix} || "#" );

		my $sep = $self->template->{field_separator} || "\t";

		$sep =~ s/\\s/\ /;
		$sep =~ s/\\t/\t/;
		$self->field_separator($sep);
		$self->head( $self->template->{head} );
		$self->fields($rbook->fields);
	}

	sub process_line {
		my ( $self, $line, $line_no ) = @_;

		$self->line_dump and $self->log->info("input line:".Dumper($line));

		my ( $marker, $status ) =
		  $self->RetrieveMarker->get_validated_marker( $line->{accession} );


		my $element = GwasCentral::DataImport::DataElement->new;
		$element->marker($marker);
		$element->status($status);
		$element->line($line);
		$element->line_no($line_no);

		if ($self->validate_line($element)) {
			if ($BATCHING) {
				push @elements, $element;
				if (scalar(@elements)==$BATCH_SIZE) {
					$self->log->info(time." Started storing batch of ".scalar(@elements)." entries");
					$self->Target->output(\@elements);
					$self->log->info(time." Finished storing batch of ".scalar(@elements)." entries");#
					@elements=();
				}
			}
			else {
				$self->Target->output([$element]);
			}

			my $success = $self->marker_success;
			$self->marker_success( ++$success );
		}
	}

	sub marker_error {
		my ($self,$element) = @_;
		my $status = $element->status;
		my $line = $element->line;

		if ( $status->{r} ) {
			$self->log->error($line->{accession}. " NOT added as replacement marker dead");
			return 1;
		}

		if ( $status->{n} ) {
			$self->log->error($line->{accession}. " NOT added as marker not found");
			return 1;
		}

		if ( $status->{s} ) {
			$self->log->error($line->{accession}. " NOT added as marker strand not present");
			return 1;
		}
		if ( $status->{p} ) {
            $self->log->error($line->{accession}. " NOT added as plugin skips line");
            return 1;
        }

		return 0;
	}

	#return 0/undef for failure and 1 for success
	sub validate_line {
		my ($self, $element) = @_;

		#run 'process_after_marker' method for each plugin after getting marker
		if ( $self->Plugins ) {
			foreach my $plugin ( @{ $self->Plugins } ) {
				$plugin->process_after_marker( $element, $self );
			}
		}

		$self->run_codehook('process_after_marker',[$element,$self]);

		if ($self->marker_error($element)) {
			push @{ $self->marker_failed },$element;
			return undef;
		}

		foreach my $rule ( @{ $self->Rules } ) {
			eval { $rule->run($element); };
			if ($@) {
				$self->log->logdie( "Error running 'run' rule "
					  . ref($rule)
					  . "\n$@\nsettings:"
					  . Dumper( $rule->settings )
					  . "\nline:"
					  . Dumper($element->line) );
			}
		}

		$self->run_codehook('process_after_Rules',[$element,$self]);

		foreach my $rule ( @{ $self->Rules } ) {
			eval { $rule->after_Rules($element); };
			if ($@) {
				$self->log->logdie( "Error running 'after_Rules' rule "
					  . ref($rule)
					  . "\n$@\nsettings:"
					  . Dumper( $rule->settings )
					  . "\nline:"
					  . Dumper($element->line) );
			}
		}

		#$self->line_dump and
#		$self->log->info("output line:".Dumper($element->line));

		if ( $element->status->{a} ) {
			$self->log->error($element->marker->source_tag." NOT added as alleles invalid");
			push @{ $self->marker_failed },$element;
			return undef;
		}

		return 1;

		if ($BATCHING) {
			push @elements, $element;
			if (scalar(@elements)==$BATCH_SIZE) {
				$self->log->info(time." Started storing batch of ".scalar(@elements)." entries");
				$self->Target->output(\@elements);
				$self->log->info(time." Finished storing batch of ".scalar(@elements)." entries");#
				@elements=();
			}
		}
		else {
			$self->Target->output([$element]);
		}

		my $success = $self->marker_success;
		$self->marker_success( ++$success );
	}

	sub line_to_fields {
		my ( $self, $line ) = @_;
		my $field_counter = 0;
		my %values;
		foreach ( @{ $self->fields } ) {
			$values{$_} = $line->[$field_counter];
			$field_counter++;
		}

		return \%values;
	}

	sub import_Plugins {
		my ($self) = @_;
		my @plugin_names = split( ' ', $self->template->{plugins} );
		my @plugins = ();
		foreach my $plugin (@plugin_names) {
			$self->log->info("loading plugin $plugin");
			my $plugin_module =
			  load_module("GwasCentral::DataImport::Plugin::$plugin");
			push @plugins, $plugin_module;
		}
		return \@plugins;
	}

	sub increment_stat {
		my ( $self, $stat, $value ) = @_;
		$self->stats->{$stat}++;
		return $value;
	}

	sub run_codehook {
		my ($self, $code_label, $args) = @_;

		my $code = $self->template->{'Code'}->{$code_label}->{sub};
		if ($code) {
			my $coderef;
			eval '$coderef = sub '.$code;

			eval {
				$coderef->(@{$args});
			};
			if ($@) {
				$self->log->logdie("Error attempting to run codehook: ".$code_label."\nOffending code:".$code."\nERROR:".$@);
			}
		}
	}

	sub push_Rule {
		my ($self, $rule_name,$options) = @_;
		my $rule = import_module("GwasCentral::DataImport::Rule::$rule_name",$options);
		push @{$self->Rules},$rule;
	}

	sub unshift_Rule  {
		my ($self, $rule_name,$options) = @_;
		my $rule = import_module("GwasCentral::DataImport::Rule::$rule_name",$options);
		unshift @{$self->Rules},$rule;
	}

	sub remove_Rule {
		my ($self, $rule_name) = @_;

		my @rules = @{$self->Rules};
		my @new_rules = ();
		foreach my $rule(@rules) {
			if (ref($rule) ne "GwasCentral::DataImport::Rule::$rule_name") {
				push @new_rules,$rule;
			}
		}

		$self->Rules(\@new_rules);
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: Core.pm 1646 2011-04-05 16:14:08Z rcf8 $

=cut

