# $Id: LogicMaker.pm 1646 2011-04-05 16:14:08Z rcf8 $

=head1 NAME

GwasCentral::DataImport::LogicMaker - Deals with DataImport Logic


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::target);

	my $lmaker = GwasCentral::DataImport::LogicMaker
	my $rules = $lmaker->prepare_rules($self->template);


=head1 DESCRIPTION

Used by the DataImport::Core to get fields, decide which rules should be added, and set up the targeter


=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Rulebook;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::Base);
use Bio::SeqFeature::Generic;
use Carp qw(confess);

use English qw( -no_match_vars );
use Data::Dumper;
use List::MoreUtils qw(firstval true apply);
use GwasCentral::Base qw(load_module);

# Private variables & constants
has 'fields'     => ( 'is' => 'rw' );    #contains schema DB object
has 'panelnames' => ( 'is' => 'rw' );
has 'resultsets' => ( 'is' => 'rw' );
has 'template'   => ( 'is' => 'rw' );

sub has_config { 0; }

=head2 field_exists

	  Usage      : if ($lmaker->field_exists('name')) { #do one thing } else { #do another }
	  Purpose    : Determine whether a field exists in the list of fields (retrieved from the template)
	  Returns    : 0 if not present/1 if it is
	  Arguments  : Field name
	  Throws     :
	  Status     : Public
	  Comments   : prepare_fields must be run before calling this

=cut

sub field_exists {
	my ( $self, $field_name ) = @_;
	return firstval { $_ eq $field_name } @{ $self->fields };
}

=head2 prepare_fields

	  Usage      : $lmaker->prepare_fields;
	  Purpose    : Get fields out of the template and into an array
	  Returns    : 0 if not present/1 if it is
	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub prepare_fields {

	my ($self)   = @_;
	my $template = $self->template;
	my @fields   = ();

	#put fields into an array
	foreach ( keys %{ $template->{Fields} } ) {
		my $pos = int($_) - 1;
		$fields[$pos] = $template->{Fields}->{$_};
	}
	foreach my $field(@fields) {
		!$field and $field="ignore";
	}
	$self->fields( \@fields );
}

sub _prepare_common_Rules {
	my ( $self, $al_order, $gt_order ) = @_;

	my $template = $self->template;
	my @rules    = ();

	#add rule to compare chromosomes if field exists
	if ( $self->field_exists('chromosome') ) {
		my $pos_rule = $self->import_Rule('PositionsMatch');
		$pos_rule->settings->{"chromosome"} = 1;
		$self->log->info("Setting chromosome flag in PositionsMatch rule");
	
		#add rule to compare positions if field exists
		if ( $self->field_exists('position') ) {
			$pos_rule->settings->{"position"} = 1;
			$self->log->info("Setting position flag in PositionsMatch rule");
		}
		push @rules, $pos_rule;
	}

	#if 'alleles' field set up a split based on the allele_order
	if ( $self->field_exists('alleles') ) {
		push @rules,
		  $self->import_Rule(
			'SplitField',
			{
				order     => $al_order,
				separator => $template->{'allele_separator'},
				type      => 'allele'
			}
		  );
	}

	#if 'genotypes' field set up a split based on the genotype_order
	if ( $self->field_exists('genotypes') ) {
		push @rules,
		  $self->import_Rule(
			'SplitField',
			{
				order     => $gt_order,
				separator => $template->{'gt_separator'},
				type      => 'genotype'
			}
		  );
	}

#deal with different strand types
#if is a field then use this as comparison against DB
#if is a template setting then use this as comparison against DB
#if no field or template setting then add custom strandflip which is dealt with by a plugin
	if (   $self->field_exists('allele1')
		|| $self->field_exists('alleles') )
	{
		if ( $self->field_exists('strand') ) {
			push @rules, $self->import_Rule('StrandFlip::Field');
		}
		else {
			if ( $template->{strand} ) {
				my $rule = $self->import_Rule('StrandFlip::File');
				$rule->settings->{'strand'} = $template->{strand} || '+';
				push @rules, $rule;
			}
			else {
				my $rule = $self->import_Rule('StrandFlip::Custom');
				push @rules, $rule;
			}
		}
		push @rules, $self->import_Rule('AllelesMatch');
	}

#if no genotypes fields, but alleles fields do exist then add rule to create genotypes from alleles
#if no genotypes or alleles then logic dies
	if (   !$self->field_exists('genotype11')
		&& !$self->field_exists('genotypes') )
	{
		if (   $self->field_exists('allele1')
			|| $self->field_exists('alleles') )
		{
			$self->log->info(
"no genotypes but alleles - add rule PopulateGenotypesFromAlleles"
			);
			push @rules, $self->import_Rule('PopulateGenotypesFromAlleles');
		}
		else {
			if ( $template->{ignore_no_alleles} ) {
				$self->log->info(
"no genotypes or alleles fields provided in template - template ignores"
				);
			}
			else {
				$self->log->logdie(
					"no genotypes or alleles fields provided in template!");
			}
		}
	}
	
	if ($self->field_exists('es_value')) {
			
	}
	
	if ($self->field_exists('es_lower95')) {
			
	}
	
	if ($self->field_exists('es_upper95')) {
			
	}
	
	if ($self->field_exists('es_type')) {
			
	}
	return @rules;

}

=head2 prepare_Rules

	  Usage      : $lmaker->prepare_Rules;
	  Purpose    : Set up a list of rules based on various criteria including whether is single or multi-panel template
	  Returns    : An arrayref of GwasCentral::DataImport::Rule objects
	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub prepare_Rules {
	my ($self) = @_;

	my $template = $self->template;

	#ensure fields are prepared and set up panelnames
	$self->prepare_fields;
	my $panelnames = $self->get_preset_apanel_names;
	$self->panelnames($panelnames);

#get space separated allele and genotype orders from template and split into arrays
	my @al_order =
	  $template->{'allele_order'}
	  ? split( /\ /, $template->{'allele_order'} )
	  : [];
	my @gt_order =
	  $template->{'gt_order'}
	  ? split( /\ /, $template->{'gt_order'} )
	  : [];

	my @rules = $self->_prepare_common_Rules( \@al_order, \@gt_order );

	#if template contains frequency fields
	if ( $template->{frequencies} ) {

#if multiple panels then get multi panel specific rules, otherwise get single panel specific rules
		if ($panelnames) {
			my @more_rules = $self->_prepare_multi_panel_Rules(
				{
					template   => $template,
					al_order   => \@al_order,
					gt_order   => \@gt_order,
					panelnames => $self->panelnames
				}
			);
			@rules = ( @rules, @more_rules );
		}
		else {
			my @more_rules = $self->_prepare_single_panel_Rules(
				{
					template => $template,
					al_order => \@al_order,
					gt_order => \@gt_order
				}
			);
			@rules = ( @rules, @more_rules );
		}
	}

	return ( \@rules );
}

=head2 _prepare_single_panel_Rules

	  Usage      : $lmaker->_prepare_single_panel_Rules({ al_order => '12', gt_order => '11 12 22'});
	  Purpose    : Used internally to set up Rules for templates with single panels on a line
	  Returns    : An array of GwasCentral::DataImport::Rule objects
	  Arguments  : Hashref of:
	  				al_order => order of alleles (eg. 1 2)
	  				gt_order => order of genotypes (eg. 11 12 22)
	  				template => DataImport template as hash
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub _prepare_single_panel_Rules {
	my ( $self, $attrs ) = @_;
	$self->log->info("_prepare_single_panels_Rules");
	my $template = $attrs->{'template'};
	my @al_order = @{ $attrs->{'al_order'} };
	my @gt_order = @{ $attrs->{'gt_order'} };

	my @rules = ();

	#if genotype_freqs then split field by separator in order specified
	if ( $self->field_exists('genotype_freqs') ) {
		$self->log->info("genotype_freqs found - add rule SplitField");
		push @rules,
		  $self->import_Rule(
			'SplitField',
			{
				order     => [@gt_order],
				separator => $template->{'gt_freq_separator'},
				type      => 'genotype',
				unit      => 'freq'
			}
		  );
	}
	else {

		#if genotype_numbers then split field by separator
		if ( $self->field_exists('genotype_numbers') ) {
			$self->log->info(
				"genotype_numbers found but no freqs - add rule SplitField" );
			push @rules,
			  $self->import_Rule(
				'SplitField',
				{
					order     => [@gt_order],
					separator => $template->{'gt_number_separator'},
					type      => 'genotype',
					unit      => 'number'
				}
			  );
		}

		#if allele_numbers then split field by separator
		if ( $self->field_exists('allele_numbers') ) {
			$self->log->info("allele_numbers found - add rule SplitField");
			push @rules,
			  $self->import_Rule(
				'SplitField',
				{
					order     => [@al_order],
					separator => $template->{'allele_number_separator'},
					type      => 'allele',
					unit      => "number"
				}
			  );
		}
	}

 #if allele numbers requires regular expression to deal with it import rule here
	if ( $self->template->{'allele_number_regex'} ) {
		$self->log->info("allele_number regex found - add rule RegexFields");
		my @fields = map { $_ = "allele$_" . "_number"; } @al_order;
		push @rules,
		  $self->import_Rule(
			'RegexFields',
			{
				fields => \@fields,
				regex  => $template->{'allele_number_regex'}
			}
		  );
		$self->log->info(
			"since no freqs - add rule CalcAlleleFreqsFromNumbers");
		push @rules, $self->import_Rule('CalcAlleleFreqsFromNumbers');
	}

#if genotype numbers requires regular expression to deal with it import rule here
	if ( $self->template->{'gt_number_regex'} ) {
		$self->log->info("gt_number_regex found - add rule RegexFields");

		my @fields = map { $_ = "genotype$_" . "_number"; } @gt_order;
		my $rule = $self->import_Rule('RegexFields');

		$rule->settings->{'fields'} = [@fields];
		$rule->settings->{'regex'}  = $template->{'gt_number_regex'};
		push @rules, $rule;

		$self->log->info(
			"since no freqs - add rule CalcGenotypeFreqsFromNumbers");
		my $rule2 = $self->import_Rule('CalcGenotypeFreqsFromNumbers');
		push @rules, $rule2;
	}

	#if no allele1_freq calculate allele frequency from genotype numbers
	if ( !$self->field_exists('allele1_freq') ) {
		$self->log->info(
"No allele frequencies fields found - adding CalcAlleleFreqsFromGenotypeNumbers"
		);
		push @rules, $self->import_Rule('CalcAlleleFreqsFromGenotypeNumbers');

	}

	return @rules;
}

=head2 _prepare_multi_panel_rules

	  Usage      : $lmaker->_prepare_multi_panel_rules({ template =>$template, panelnames=>['CONTROL','CASE'], al_order => '12', gt_order => '11 12 22'});
	  Purpose    : Used internally to set up rules for templates with single panels on a line
	  Returns    : An array of GwasCentral::DataImport::Rule objects
  	  Arguments  : Hashref of:
	  				al_order => order of alleles (eg. 1 2)
	  				gt_order => order of genotypes (eg. 11 12 22)
	  				template => DataImport template as hash
	  				panelnames => arrayref containing panelnames
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub _prepare_multi_panel_Rules {
	my ( $self, $attrs ) = @_;
	$self->log->info("_prepare_multi_panel_rules");
	my $template   = $attrs->{'template'};
	my @al_order   = @{ $attrs->{'al_order'} };
	my @gt_order   = @{ $attrs->{'gt_order'} };
	my $panelnames = $attrs->{'panelnames'};
	my @rules      = ();

	#if genotype numbers for panels add SplitField rule to separate all
	if ( $self->field_exists( "genotype_numbers:" . $panelnames->[0] ) ) {
		$self->log->info( "genotype_numbers found for "
			  . $panelnames->[0]
			  . " but no freqs - add rule SplitField" );
		my $rule = $self->import_Rule('SplitField');

		$rule->panelnames($panelnames);
		$rule->settings->{'order'}     = \@gt_order;
		$rule->settings->{'separator'} = $template->{'gt_number_separator'};
		$rule->settings->{'type'}      = 'genotype';
		$rule->settings->{'unit'}      = "number";
		push @rules, $rule;

	}

	if ( $self->template->{'allele_number_regex'} ) {
		$self->log->info("allele_number regex found - add rule RegexFields");
		my @fields = map { $_ = "allele$_" . "_number"; } @al_order;
		push @rules,
		  $self->import_Rule(
			'RegexFields',
			{
				fields => \@fields,
				regex  => $template->{'allele_number_regex'}
			},
			$panelnames
		  );

		$self->log->info(
			"since no freqs - add rule CalcAlleleFreqsFromNumbers");
		push @rules,
		  $self->import_Rule( 'CalcAlleleFreqsFromNumbers', {}, $panelnames );
	}

	if ( $self->template->{'gt_number_regex'} ) {
		$self->log->info("gt_number_regex found - add rule RegexFields");
		my @fields = ();
		foreach my $gt (@gt_order) {
			foreach my $panel ( @{$panelnames} ) {
				push @fields, "genotype$gt" . "_number:$panel";
			}
		}

		my $rule = $self->import_Rule('RegexFields');

		$rule->settings->{'fields'} = [@fields];
		$rule->settings->{'regex'}  = $template->{'gt_number_regex'};
		push @rules, $rule;
	}

	if ( $self->field_exists( "genotype_numbers:" . $panelnames->[0] ) ) {
		if ( !$self->field_exists( "genotype_freqs:" . $panelnames->[0] ) ) {
			push @rules,
			  $self->import_Rule( 'CalcGenotypeFreqsFromNumbers',
				{}, $panelnames );
		}

		if ( !$self->field_exists( "allele_freqs:" . $panelnames->[0] ) ) {
			push @rules,
			  $self->import_Rule( 'CalcAlleleFreqsFromGenotypeNumbers',
				{}, $panelnames );
		}
	}

	#if genotype numbers already split add SplitField rule to separate all
	if ( $self->field_exists( "genotype11_number:" . $panelnames->[0] ) ) {
		$self->log->info( "individual genotype_numbers found for "
			  . $panelnames->[0]
			  . " no need to split field" );

		if ( !$self->field_exists( "genotype11_freqs:" . $panelnames->[0] ) ) {
			push @rules,
			  $self->import_Rule( 'CalcGenotypeFreqsFromNumbers',
				{}, $panelnames );
		}

		if ( !$self->field_exists( "allele11_freqs:" . $panelnames->[0] ) ) {
			push @rules,
			  $self->import_Rule( 'CalcAlleleFreqsFromGenotypeNumbers',
				{}, $panelnames );
		}

	}

	#if allele numbers for panels add SplitField rule to separate all
	if ( $self->field_exists( "allele_numbers:" . $panelnames->[0] ) ) {
		$self->log->info( "allele_numbers found for "
			  . $panelnames->[0]
			  . " but no freqs - add rule SplitField" );
		$self->log->info(
			"One field with allele frequencies found - adding split rule");
		my $rule = $self->import_Rule('SplitField');

		$rule->panelnames($panelnames);
		$rule->settings->{'order'}     = \@al_order;
		$rule->settings->{'separator'} = $template->{'allele_freq_separator'};
		$rule->settings->{'type'}      = 'allele';
		push @rules, $rule;
	}

	return @rules;

	#print join("\n",@rules);
}

=head2 import_Rule

	  Usage      : my $rule = $self->import_Rule('Example',undef,['CONTROL','CASE']);
	  Purpose    : Used internally to load a Rule module and set it up with defaults
	  Returns    : A GwasCentral::DataImport::Rule object
  	  Arguments  : Rule module name (eg. PositionMatch);Settings as hashref;panelnames as arrayref
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub import_Rule {
	my ( $self, $name, $settings, $panelnames ) = @_;
	my $rule = load_module("GwasCentral::DataImport::Rule::$name");
	if ($settings)   { $rule->settings($settings); }
	if ($panelnames) { $rule->panelnames($panelnames); }
	$self->log->info( "RULE added:" . ref($rule) );
	return $rule;
}

=head2 get_resultsets_from_fields

	  Usage      : my $rs = $lmaker->get_resultsets_from_fields;
	  Purpose    : Retrieve resultset identifiers from pvalue fields if more than one per line (eg. pvalue:HGVRS3 and pvalue:HGVRS4 would return HGVRS3 and HGVRS4)
	  Returns    : An arrayref containing identifiers
  	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   : Need to call prepare_fields before calling this method

=cut

sub get_resultsets_from_fields {
	my ($self) = @_;

	#get those fields which begin with 'pvalue:'
	my @pvalue_fields =
	  map { $_ =~ /(^pvalue:.*)/ } @{ $self->fields };

	#loop through each field and add the suffix to an array
	if (@pvalue_fields) {

		my @resultsets;
		foreach my $resultset_field (@pvalue_fields) {
			my @info = split( ":", $resultset_field );
			push @resultsets, $info[1];
		}
		$self->log->info( 'resultsets ('
			  . join( ",", @resultsets )
			  . ') supplied in pvalues fields' );
		return \@resultsets;
	}
	return [];
}

=head2 get_preset_panelnames

	  Usage      : my $panels = $lmaker->get_panelnames_from_fields;
	  Purpose    : Retrieve assayedpanel names from allele/genotype fields if more than one per line (eg. allele_numbers:CASE and allele_numbers:CONTROL would return CASE and CONTROL)
	  Returns    : An arrayref containing assayedpanel names
  	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   : Need to call prepare_fields before calling this method

=cut

sub get_preset_apanel_names {
	my ($self) = @_;
	$self->log->info("fields:".Dumper($self->fields));
	#get those fields which begin with 'genotype_numbers:'
	my @genotype_fields =
	  grep { $_ and $_ =~ /(^genotype_numbers:.*)/ } @{ $self->fields };

#if fields are found loop through each field add the suffix to an array and return it
	if (@genotype_fields) {

		my @panels;
		foreach my $genotype_field (@genotype_fields) {
			my @info = split( ":", $genotype_field );
			push @panels, $info[1];
		}
		$self->log->info( 'panelnames ('
			  . join( ",", @panels )
			  . ') supplied in genotype_numbers fields' );
		return \@panels;
	}

	#get those fields which begin with 'genotype11_number:'
	@genotype_fields =
	  grep { $_ and $_ =~ /(^genotype11_number:.*)/ } @{ $self->fields };

#if fields are found loop through each field add the suffix to an array and return it
	if (@genotype_fields) {

		my @panels;
		foreach my $genotype_field (@genotype_fields) {
			my @info = split( ":", $genotype_field );
			push @panels, $info[1];
		}
		$self->log->info( 'panelnames ('
			  . join( ",", @panels )
			  . ') supplied in genotypeXX_numbers fields' );
		return \@panels;
	}

	#get those fields which begin with 'allele_numbers:'
	my @al_fields =
	  grep { $_ and $_ =~ /(^allele_numbers:.*)/ } @{ $self->fields };

#if fields are found loop through each field add the suffix to an array and return it
	if (@al_fields) {

		my @panels;
		foreach my $al_field (@al_fields) {
			my @info = split( ":", $al_field );
			push @panels, $info[1];
		}
		$self->log->info('panelnames supplied in allele_numbers fields');
		return \@panels;
	}

	if ( $self->field_exists('panelname') ) {
		$self->log->info('panelname defined in field');
		return undef;
	}

	if ( $self->template->{apanels} ) {
		my @panels = split( ' ', $self->template->{apanels} );
		$self->log->info( "template apanels:" . Dumper( \@panels ) );
		return \@panels;
	}
}

=head2 prepare_target

	  Usage      : my $target = $lmaker->prepare_target;
	  Purpose    : Load the appropriate target module (MultiPanel or OnePanel) and prepare it
	  Returns    : An GwasCentral::DataImport::target:: module
  	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub prepare_Target {
	my ($self) = @_;
	my $panelnames = $self->panelnames;

	#setup target with general settings
	my $target =
	  load_module( 'GwasCentral::DataImport::Target::'
		  . ( $self->template->{'target_module'} || 'Database' ) );

	$target->init( $self->template, $panelnames );

	if ( $self->template->{associations} ) {
		if ( !$target->settings->{resultset_identifier} ) {
			my $found = 0;
			foreach my $fld_suffix (qw(name identifier label)) {
				if ( $self->field_exists( 'resultset_' . $fld_suffix ) ) {
					$target->resultset_lookup($fld_suffix);
					$target->resultset_field( 'resultset_' . $fld_suffix );
					$found = 1;
				}
			}

			#die if no field containing resultset found
			if (!$found) {
				my $resultsets = $self->get_resultsets_from_fields();
				$self->log->info("resultsets:".Dumper($resultsets));
				if (scalar(@{$resultsets})==0) {
					$self->throw(
"No resultset template field (resultset_identifier) or file field (resultset_name, resulltset_identifier or resultset_label) defined. Are you sure you want to have associations=1 in the template?"
);
				}
				else {
					$target->resultsets($resultsets);
				}
			}
 
		}
	}

	return $target;
}

sub get_study {
	my ($self) = @_;
	my $study_id = $self->template->{study_identifier};
	if ( !$study_id ) {
		$self->log->info(
			"Study Identifier not provided in template - looking for field" );
	}

	return $study_id;
}

sub get_accession_db {
	my ($self) = @_;
	my $datasource = $self->template->{accession_db};
	if ( !$datasource ) {
		$self->log->fatal("No DataSource name provided");
	}
	else {
		$self->log->info("DataSource name provided $datasource");
	}
	my $ds = $self->db->get_datasource($datasource);
	return $ds->datasourceid;
}

sub get_freq_experiment {
	my ($self) = @_;
	my $freq_exp_id = $self->template->{freq_exp_id};
	if ( !$freq_exp_id ) {
		$self->log->fatal("No Frequency Experiment ID provided");
	}
	else {
		$self->log->info("Frequency Experiment ID provided $freq_exp_id");
	}
	my $freq_exp = $self->db->get_experiment($freq_exp_id);

	return $freq_exp;
}

sub get_resultsetid {
	my ($self) = @_;
	my $resultset = $self->template->{resultset_identifier};
	if ( !$resultset ) {
		$self->log->fatal("No ResultSet Identifier provided");
	}
	else {
		$self->log->info("ResultSet Identifier provided $resultset");
	}

	return $resultset;
}

sub get_experimentid {
	my ($self) = @_;
	my $exp_id = $self->template->{experiment_identifier};
	if ( !$exp_id ) {
		$self->log->fatal("No Experiment Identifier provided");
	}
	else {
		$self->log->info("Experiment ID provided $exp_id");
	}

	return $exp_id;
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: LogicMaker.pm 1646 2011-04-05 16:14:08Z rcf8 $

=cut

