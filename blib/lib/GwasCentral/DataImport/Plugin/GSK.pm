# $Id: AffymetrixLookup.pm 1192 2009-06-18 11:26:43Z rcf8 $

=head1 NAME

HGVbaseG2P::Validation::Plugin::AffymetrixLookup - Validation Plugin dealing with affymetrix IDs


=head1 SYNOPSIS

	#in Validation template file
	plugins = AffymetrixLookup

	#the plugin is called by the Validation pipeline

=head1 DESCRIPTION

Plugin to deal with Affymetrix ID lookups.

Does the following:
Gets affymetrix->rsid data from CSV files provided by Affymetrix. Then for each line it:
Gets the RSid using the Affymetrix ID
Compares the strand in the DB with the strand in the Affymetrix file -> if they are different does a strand flip

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Plugin::GSK;

	use strict;
	use warnings;

	# Use OIO inside-out object framework for our OO hierarachy
	use Moose;
	extends qw(GwasCentral::DataImport::Plugin);
	use Data::Dumper qw(Dumper);


	sub after_setup {
		my ($self, $core) = @_;
		$core->remove_rule('SplitField');
	}

	sub process_before_marker {
		my ($self, $fields,$core) = @_;
		my @temp = split("\:\ ",$fields->{chr_position});
		$fields->{chromosome}=$temp[0];
		$fields->{position}=int($temp[1]);
		$fields->{accession}=lc($fields->{accession});
	}

	sub process_after_marker {
		my ($self, $element, $core) = @_;
		my $line = $element->line;
		use Data::Dumper qw(Dumper);

		my ($gt1,$gt2);

		foreach my $panel(qw(Case Control)) {
		    my @temp = split(/\)\ /,$line->{"genotype_numbers:$panel"});
		    $self->log->info("temp $panel:".Dumper(\@temp));

			$gt1 = substr($temp[1],0,1);
			$gt2 = substr($temp[1],2,1);

			$temp[0]=~/\((\d+)/;
			$self->log->info("gt:$1");
			$line->{"genotype11_number:$panel"}=$1;
			$temp[1]=~/\((\d+)/;
			$self->log->info("gt:$1");
			$line->{"genotype12_number:$panel"}=$1;
			$temp[2]=~/\((\d+)/;
			$self->log->info("gt:$1");
			$line->{"genotype22_number:$panel"}=$1;
		}

		$line->{'allele1'}=$gt1;
		$line->{'allele2'}=$gt2;
		$line->{'genotype11'}="(".$gt1.")";
		$line->{'genotype12'}="($gt1+$gt2)";
		$line->{'genotype22'}="(".$gt2.")";
		#$self->log->info("rules:".Dumper(join("\n",@{$core->rules})));
	}


1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: AffymetrixLookup.pm 1192 2009-06-18 11:26:43Z rcf8 $

=cut