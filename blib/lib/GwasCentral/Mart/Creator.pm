#!perl

# $Id: MartCreator.pm 782 2008-06-26 13:39:21Z rcf8 $

=head1 NAME

GwasCentral::Mart::MartCreator - Class for creating a mart based using
details in a supplied config file


=head1 SYNOPSIS

    use HGVbaseG2Pmart::MartCreator;
    my $creator=MartCreator->new($config_filename, $verbose_flag);
    $creator->create_mart;

=head1 DESCRIPTION

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Mart::Creator;
use Moose;
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);
	use strict;
	use warnings;
	use FindBin;
	use lib "$FindBin::Bin/../../";
	use GwasCentral::Base qw(load_config init_logger open_infile load_module split_number_list);
	use Digest::MD5;
	use Compress::Zlib;
	use GwasCentral::DataSource::Mart;
	use GwasCentral::DataSource::Util qw(DataSources_from_list new_AccessControl);
	use Data::Dumper qw(Dumper);
	use Log::Log4perl::Level;
	
	has 'init' => ('is'=>'rw');
	has 'studies' => ('is'=>'rw');
	has 'verbose' => ('is'=>'rw');
	has 'dataset' => ('is'=>'rw');
	has 'refreshconfig' => ('is'=>'rw');
	has 'noconfirm' => ('is'=>'rw');
	#has 'ignorebase' => ('is'=>'rw');
	#has 'killbase' => ('is'=>'rw');
	
	has 'user'=> ('is'=>'rw');
	has 'password' => ('is'=>'rw');
	has 'argv' => ('is'=>'rw');
	
=head2 create_mart
	
	  Usage      : $creator->create_mart;
	  Purpose    : Creates the mart using the properties of the MartCreator object
	  Returns    : Nothing
	  Arguments  : None
	  Throws     : 
	  Status     : 
	  Comments   : 
	
=cut
	
	sub generate {
		my ($self)=@_;
		!$self->config->{Mart} and $self->throw("No <Mart> section defined in config file(s)");
		my %config = %{$self->config->{Mart}};
		#setup variables with config info
		my $sql_location=$config{sql_location};
		
		#set up source DB object as this is used by both update and install
		$self->populate_DS_from_list(qw(Study Marker Mart Ontology Browser Feature));
		
		my $martdb = $self->DS('Mart');
		my $studydb = $self->DS('Study');
		$studydb->AccessControl(new_AccessControl($self->config));
		$self->studies([split_number_list($self->studies,'HGVST')]);
		if ($self->init) {
			if (!$config{noconfirm}) {
				system("echo This will truncate the following database: ". $martdb->dsn);
	            system("echo Are you sure you want to do this?");
	            my $yn=<STDIN>;
	            if ($yn =~ /^n/) { exit; };
			}
			$self->create_mart;
			 
			$self->log->info("Dropped and created mart database ".$martdb->name);
			
			#create dataset config tables
			my $script = $self->_get_sql_from_script("$sql_location/create_config.sql");
			$martdb->execute_script($script);
			$self->log->info("Created config tables");
		}
		else {
			$self->log->info("Using current mart database ".$martdb->name);
		}
			
			
		#add datasets
		my $dataset_cfg = $config{Dataset};
		foreach my $dataset_name(keys %{$dataset_cfg}) {
				my $dataset = $dataset_cfg->{$dataset_name};
				next if $dataset->{ignore};
				next if $dataset->{init_only} and !$self->init;
				$dataset->{dataset}=$dataset_name;
				$self->_add_dataset($dataset);	
		}
	}
	
	
	sub create_mart {
		my ($self) = @_;
	
		#need to use existing db (source db) as 'handle' in order to create mart db
		my $martdb=$self->DataSources->{Mart};
		$martdb->drop_and_create();
		$martdb=GwasCentral::DataSource::Mart->new({conf_file=>$self->config});
		$self->DataSources->{Mart}=$martdb;

	}
=head2 _add_dataset
	
	  Usage      : $self->_add_dataset({
	  				dataset=>$dataset_name,
					description=>$description,
					visible=>$visible,
					importtable=>$importtable
	  			   });
	  Purpose    : Private method used to add a dataset to the mart including data and structure
	  Returns    : Nothing
	  Arguments  : Hash reference containing:
	                dataset => dataset name
	                description => description displayed for dataset
	                visible => dataset visible in martview?
	                importtable => import table using FTP?
	  Throws     : 
	  Status     : 
	  Comments   : 
	
=cut
	
	sub _add_dataset {
		my ($self,$attrs) = @_;
		my %config = %{$self->config->{Mart}};
		my $dataset = $attrs->{dataset};
		my $ds_type = $attrs->{source} || "script";
		
		my $martdb = $self->DS('Mart');
		my $studydb = $self->DS('Study');
		
		my $sql_location = $config{sql_location};
		my $xml_location=$config{xml_location};
		
		if ($self->init) {
			my $scriptfile = "$sql_location/$dataset/create_$dataset.sql";
			my $script = $self->_get_sql_from_script($scriptfile);
			$self->log->info("Executing '$scriptfile' SQL file to create dataset");
			$martdb->execute_script($script);
		}
		
		#obtain list of dataset SQL files
		$self->log->info("Dataset $dataset has source $ds_type");
		if (!$self->refreshconfig) {
			if ($ds_type eq "script") {
				my $suffix = "_*.sql";
				my @scriptfiles = <$sql_location/$dataset/$dataset$suffix>;
				
				my $scriptfile = "$sql_location/$dataset/$dataset.sql";
				unshift @scriptfiles, $scriptfile;
	
				$scriptfile = "$sql_location/$dataset/before_$dataset.sql";
				$self->log->info("Executing '$scriptfile' SQL file");
				if (-e $scriptfile) {
					my $script = $self->_get_sql_from_script("$scriptfile");
					$martdb->execute_script($script);
				}
					
				#for each SQL script generate script ref and execute each one
				foreach my $study_ident(@{$self->studies}) {
					foreach my $scriptfile(@scriptfiles) {
						if (-e $scriptfile) {
							$self->log->info("Executing '$scriptfile' SQL file for study $study_ident");
							my $script = $self->_get_sql_from_script("$scriptfile",$study_ident);
							
							$martdb->execute_script($script);
						}
					}
				}
				$scriptfile = "$sql_location/$dataset/after_$dataset.sql";
				if (-e $scriptfile) {
					$self->log->info("Executing '$scriptfile' SQL file");
					my $script = $self->_get_sql_from_script("$scriptfile");
					$martdb->execute_script($script);
				}
			}
			elsif ($ds_type eq "import") {
				my $importtable = $attrs->{import_table};
				my @importtables = ref($importtable) ne 'ARRAY' ? ($importtable) : @{$importtable};
				foreach my $it(@importtables) {
					$self->_get_table_via_ftp($it);
					$martdb->import_tsv_file($config{temp_dir}."/".$it.".txt", $it);
				}
			}
			elsif ($ds_type eq "plugin") {
				my $plugin_mod = load_module("GwasCentral::Mart::Plugin::".$attrs->{plugin_module},{conf_file => $self->config});
				$plugin_mod->Creator($self);
				$plugin_mod->populate;
			}
			else {
				$self->throw("Source type in config ($ds_type) not recognised!");
			}
		}
			
		#add config records for dataset and insert XML

		$self->update_config_xml($attrs);
	}
	

	
=head2 _get_table_via_ftp
	
	  Usage      : $self->_get_table_via_ftp($tablename)
	  Purpose    : Private method used to get text file containing table from FTP site
	  Returns    : Nothing
	  Arguments  : Table name (file downloaded is <tablename>.txt.gz
	  Throws     : 
	  Status     : 
	  Comments   : 
	
=cut
	
	sub _get_table_via_ftp {
		my ($self, $tablename) = @_;
		my %config = %{$self->config->{Mart}};
		my $ftp_host = $config{ftp_host};
		my $temp_dir = $config{temp_dir};
		my $file = "$tablename.txt";
	 
	#    my $ftp = Net::FTP->new($ftp_host, Debug => 0)
	#      or die "Unable to connect to $ftp_host: $@";
	#
	#    $ftp->login("anonymous",'-anonymous@')
	#      or die "Unable to login to $ftp_host: ", $ftp->message;
	#
	#	$ftp->cwd($ftp_dir);
	#	$ftp->get($file,"$temp_dir/$file") or warn $ftp->message;
	#
	#    $ftp->quit;
	    
	    if (!$config{use_local}) {
	    	my $failed = system "curl $ftp_host/$file.gz > /$temp_dir/$file.gz";
	    	!$failed ? $self->log->info("File '$file' obtained from $ftp_host successfully") : $self->throw("Unable to obtain '$file' from $ftp_host!");
	    }
	    else {
	        if (-e "$temp_dir/$file.gz") {
	                $self->log->info("Using local file '$temp_dir/$file.gz'");
	        }
	        else {
	                $self->throw("DIED - Local file '$temp_dir/$file.gz' not found!");
	        }
	    }
	    system "gunzip -c $temp_dir/$file.gz > $temp_dir/$file";
	
	}
	
	sub update_config_xml {
    my ($self, $attrs)=@_;
    my $location = $self->config->{Mart}->{xml_location};
    my $dataset = $attrs->{dataset};
	#retrieve and update data using GwasCentral::Mart::Database object to 
	my $martdb = $self->DataSources->{Mart};
	my $dataset_id=$martdb->get_dataset_id($dataset);
    if (!$dataset_id) {
    	$self->log->info("Adding '$dataset' to config tables with xml config at '$location'");
    	 $dataset_id = $martdb->add_dataset_config($dataset,$attrs->{desc},$attrs->{visible});
    	 $self->log->info("dataset_id of added is:$dataset_id");
    }
	#retrieve uncompressed XML from file and compress 
	my $xml_template_file=$location."/".$dataset."_template.template.xml";
	my $xml_file=$location."/".$dataset.".xml";
	$self->log->info("XML file name:$xml_file, dataset ID:$dataset_id");
	my $config_xml = $self->_get_config_xml($xml_file,$dataset_id);
	
	$self->log->info("XML file name:$xml_template_file, dataset ID:$dataset_id");
	my $template_xml = $self->_get_config_xml($xml_template_file,$dataset_id);
	
	my $compressed_xml= $self->_compress_data($config_xml);
	my $compressed_template_xml = $self->_compress_data($template_xml);

	
	#update config and template with compressed XML  	
	$martdb->update_conf_xml($compressed_xml,$dataset_id);
	$martdb->update_template_xml($compressed_template_xml,$dataset);
}

=head2 _get_config_file

  Usage      :  $temp = _get_config_file ($filename, $dataset_id)

  Description:  Retrieves data from a file, and replaces any dataset ID with the new dataset ID.

  Return type:  A scalar containing the contents of the config file.

  Exceptions :  

  Caller     :

=cut

sub _get_config_xml {
	my ($self, $file, $dataset_id) = @_;
	
	my $fh = open_infile($file);
	
	my $config="";
	
	while (<$fh>) {
		my $line = $_;
		if ($line =~ /\<Replacer module=\"(.+)\"\/>/) {

			my $replacer = load_module("GwasCentral::Mart::Replacer::$1",{Creator => $self, conf_file=>$self->config});
			$self->log->info("Replacing <Replacer /> in $file using $1");
			$line = $replacer->replace_text;
		}
		$config.=$line;
		
	}

	$fh->close;
	
	$config =~ s/(datasetID=".+?")/datasetID=\"$dataset_id\"/;
	return $config;
}
1;

=head2 _compress_data

  Usage      :  $compressref = _compress_data ($data)

  Description:  Compresses data in a scalar it using Gzip and creates an MD5
  digest of the uncompressed data.

  Return type:  A hash-ref containing uncompressed data, compressed data and the digest

  Exceptions :  

  Caller     :

=cut

sub _compress_data {
	my ($self, $data) = @_;
	
	my $digest = Digest::MD5->new;
  	$digest->md5($data);
	
	my $ret=();
	$ret->{uncompressed}=$data;
	$ret->{compressed}=Compress::Zlib::memGzip($data);
	$ret->{digest}=$digest;
	return $ret;
}


=head2 get_script

  Usage      : $db->get_script($script_file)
  Purpose    : Open SQL script file and generate array of individual SQL statements.
               Process SQL to replace robmart and HGVbaseG2P with output and source dbs
               respectively.
  Returns    : Array of SQL statements
  Arguments  : Script file name
               Output db name
               Source db name
  Throws     : HGVbaseG2Pmart::Exception::Config if no script file found
  Status     : 
  Comments   : 

=cut

sub _get_sql_from_script {
	my ($self,$script_file,$study_ident)=@_;
	my $ds = $self->DataSources;
	#get script and separate into individual SQL statements
	open SCRIPT,"<",$script_file or $self->throw("Unable to open script file ($script_file)");
	my $temp="";
	while (<SCRIPT>) { $temp.=$_; }
	
	my @sql_statements = split(";\n",$temp);
	my @rsets = ();
	if ($study_ident) {
	    my $study = $self->DataSources->{Study}->get_study_by_identifier($study_ident);
		
		foreach my $exp($study->experiments) {
			foreach my $rset($exp->resultsets) {
				push @rsets, $rset->identifier;
			}
		}
	}
	
    #replace $study_db_name, $marker_db_name and $mart_db_name placeholders in the SQL scripts
    my @evaled_statements;
    my $study_db_name = $ds->{Study}->name;
	my $mart_db_name = $ds->{Mart}->name;
	my $browser_db_name = $ds->{Browser}->name;
	my $marker_db_name = $ds->{Marker}->name;
	my $ontology_db_name = $ds->{Ontology}->name;
	my $resultset_list = "'".join("','",@rsets)."'";
	foreach my $sql_statement(@sql_statements) {
		my $evaled_statement;
		eval "\$evaled_statement = \"".$sql_statement."\";";
		if ($@) { $self->log->error("Error evaling SQL:".$sql_statement."-\n".$@); }
		push @evaled_statements, $evaled_statement;
	}
	
	return \@evaled_statements;
}
1;
