package GwasCentral::Mart::Plugin::UCSCGenes;
use FindBin;
use lib "$FindBin::Bin/../lib";

use Data::Dumper qw(Dumper);
use Moose;
extends qw(GwasCentral::Base);
use GwasCentral::Tool::Gene qw(separate_features);
has 'Creator' => ('is'=>'rw');

sub populate {
	my ($self) = @_;
	my $chr = "6";
	$self->log->info("Creator:".$self->Creator);
	my $feat_ds = $self->Creator->DS('Feature');
	$self->log->info("feat_ds:".$feat_ds);
	my @features = $feat_ds->dbh->features(
				-seqid => "chr$chr",
		    	-type => ['mRNA','CDS','three_prime_utr','five_prime_utr']
		    );
	$self->log->info("Adding ". scalar(@features)." features from chr$chr");
	my ($feature_list, $gene_list, $gene_aliases) = separate_features($feat_ds, @features);

	
	my $mart_ds = $self->Creator->DS('Mart');
	$mart_ds->dbh->do("truncate table hsapiens_gene_ucsc__gene__main");
	$mart_ds->dbh->do("alter table hsapiens_gene_ucsc__gene__main disable keys");
	my $sql = "insert into hsapiens_gene_ucsc__gene__main(gene_id_key, ref, hgnc_label, start, stop) values(?,?,?,?,?)";
	my $sth = $mart_ds->dbh->prepare($sql);
	foreach my $g(@{$gene_list}) {
		$sth->execute(@{$g});
	}
	$mart_ds->dbh->do("alter table hsapiens_gene_ucsc__gene__main enable keys");
	
	$mart_ds->dbh->do("truncate table hsapiens_gene_ucsc__feature__dm");
	$sql = "insert into hsapiens_gene_ucsc__feature__dm(gene_id_key, ref, start, stop, feature) values(?,?,?,?,?)";
	$sth = $mart_ds->dbh->prepare($sql);
	foreach my $f(@{$feature_list}) {
		$sth->execute(@{$f});
	}
}
1;
