#!perl

# $Id: Replacer.pm 497 2008-04-15 14:51:03Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Replacer - Superclass for Replacer modules 

=head1 SYNOPSIS
	
	package HGVbaseG2Pmart::Replacer::ReplaceTest;
	
    use base qw(HGVbaseG2Pmart::Replacer)
    
    #over-ride the new and replace_text methods
    sub new {
		my $class = shift;
		my $self=HGVbaseG2Pmart::Replacer->new;
		bless($self, $class);
		return $self;
	}
	
	sub replace_text {
		return "text to replace data";
	}

=head1 DESCRIPTION

This class contains method definitions which are overridden in a superclass.
Ultimately it is used to replace a <Replacer module="<modulename>"/> XML tag in mart XML config files.

The common Replacer interface provides a way for ConfigUploader to do the replacement without worrying about which
module it is using.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Mart::Replacer;

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../";
use Moose;
extends qw(GwasCentral::Base);
has 'Creator' => ('is'=>'rw');
has 'xml_file' => ('is'=>'rw');

use GwasCentral::Base qw(open_infile);

=head2 replace_text

  Usage      :  $new_text = $replacer->replace_text;

  Description:  Returns replacement text for a called Replacer object

  Return type:  A scalar containing a string of replacement text

  Exceptions :  

  Caller     :

=cut

sub replace_text {
	die "No replace_text method implemented in ".__PACKAGE__;
}


1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Replacer.pm 497 2008-04-15 14:51:03Z rcf8 $

=cut