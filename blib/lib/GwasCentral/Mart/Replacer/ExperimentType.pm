#!perl

# $Id: ExperimentType.pm 772 2008-06-23 17:37:28Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Replacer - Superclass for Replacer modules 

=head1 SYNOPSIS

    use HGVbaseG2Pmart::Replacer::StudyList;
    $replacer = HGVbaseG2Pmart::Replacer::StudyList->new;
    $new_text = replacer->replace_text;

=head1 DESCRIPTION

This class contains methods which replace a <Replacer/> XML tag in a mart XML config file.
The replace_text method here returns an Option list containing Study Titles and IDs as values.

=head1 SUBROUTINES/METHODS 

=cut

package HGVbaseG2Pmart::Replacer::ExperimentType;

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../";
use base qw(HGVbaseG2Pmart::Replacer);
use Log::Log4perl qw( :easy );

sub new {
	my $class = shift;
	my $self=HGVbaseG2Pmart::Replacer->new;
	bless($self, $class);
	return $self;
}

=head2 replace_text

  Usage      :  $text = $replacer->replace_text;

  Description:  Generates the data used to replace a <Replacer/> element in mart XML config.
  				It uses the DB to retrieve StudyIDs and Titles and generates Option elements

  Return type:  A scalar containing the generated text

  Exceptions :  

  Caller     :

=cut

sub replace_text {
	my ($self) = @_;
	my $stag_template="hgvbase-experiment";
	my $stag_nesting="Experiment";

	my $dbh =  $self->get_database; 
	
	my $stag_set = $dbh->selectall_stag(-template => $stag_template);
	my $text="";
	foreach($stag_set->find($stag_nesting)) {
		my $type = $_->get("Type");
		$text .= '<Option displayName="'.$type.'" internalName="'. $type . '" isSelectable="true" value="'.$type.'"/>'."\n";
	}
	$dbh->disconnect;
	return $text;
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: ExperimentType.pm 772 2008-06-23 17:37:28Z rcf8 $

=cut