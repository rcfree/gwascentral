package GwasCentral::Mart::QueryXML;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../lib";
extends qw(GwasCentral::Base);
use GwasCentral::Base qw(open_infile);
use HTML::Entities;
has 'Filters' => ('is'=>'rw');
has 'xml' => ('is'=>'rw');

sub mart_redirect_url {
	my ($self) = @_;
	my $xml = $self->xml;
	my $url="http://localhost:8000/biomart/martview";
	return { query => encode_entities($xml) };
}

sub retrieve_xml_file  {
	my ($self, $query_xml_file) = @_;
	
	my $xml;
	my $xml_fh = open_infile($query_xml_file);
	
	while (<$xml_fh>) {
		$xml.=$_;
	}
	$xml_fh->close;
	$xml=~s/\"/\\"/g;
	$self->log->info("Using XML query file:$query_xml_file");
	
	my $eval_str;
	foreach my $filt(values %{$self->Filters}) {
		$eval_str.= "my \$".$filt->name." = '".($filt->value || '')."';\n";
	}
	$eval_str.="\$evaled_xml = \"".$xml ."\"";
	
	my $evaled_xml;
	eval $eval_str;
	$self->log->info("$evaled_xml");
	$self->xml($evaled_xml);
	return $xml;
	
}

sub get_data_using_api {
		my ($self, $output_fh) = @_;
		my $xml = $self->xml;
		my $registry;
		my $query = BioMart::Query->new(
			 'registry' => $registry,
			 'virtualSchemaName' => 'default',
			 'xml'      => $xml
		);
    
    	my $runner = BioMart::QueryRunner->new();
    		
    	$runner->execute($query);
		$self->log->info("Executed query");
        if ($query->header && $query->header eq '1') {
		    $runner->printHeader($output_fh);
		}
		
		my $formatter = $runner->get('formatter');

		my $counter;
		no warnings 'uninitialized';
		while (my $row = $formatter->nextRow)
		{
		    next if ($row eq "\n");
			$counter++;
			$counter%1000==0 && $self->log->info("Read $counter rows");
			print $output_fh $row;
		}
		$self->log->info("Got $counter rows");
		$runner->printFooter($output_fh);
	}
	
	sub get_data_using_martservice {
		my ($self, $output_fh) = @_;
		my $xml = $self->xml;
		my $url=$self->config->{martservice_url} || $self->log->logdie("No 'martservice_url' value was provided!");
			
		my $request = HTTP::Request->new("POST",$url,HTTP::Headers->new,'query='.$xml."\n");
		my $ua = LWP::UserAgent->new;
		
		my $response;
		
		$ua->request($request, 
		     sub{   
				 my($data, $response) = @_;
				 if ($response->is_success) {
					$output_fh->print($data);
				 }
				 else {
				    $self->log->logwarn("Problems with the web server: ".$response->status_line);
				 }
				 
		     },1000
		 );
	}
	1;