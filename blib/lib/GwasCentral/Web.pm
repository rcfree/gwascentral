# $Id: Web.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

GwasCentral::Web - Catalyst based web application

=head1 SYNOPSIS

    # To start up a a standalone server
    perl script/hgvbaseg2pweb_server.pl

    # To start up a standalone server which automatically restarts when modules (but not config files) are changed, and on a different port (3001)
	perl script/hgvbaseg2pweb_server.pl -restart -port 3001
	
=head1 DESCRIPTION

This is the base class for the Catalyst-based GwasCentral web application. This class has now evolved and contain common methods which can be used via 
the Catalyst context ($c) variable used by other inheriting classes.

The most important methods are the 'get_browser' and 'get_access_control' methods which retrieve new Browser and AccessControl modules.
There are also output methods 'do_search' and 'do_output_html' which return search and individual object.

The other role of this module is to bootstrap the app from parameters in the hgvbase.conf config file in conf/. 
Other data retrieval, presentation and business logic resides in class modules in Model, View and Controller, respectively.

For newbies to the project and/or MVC-frameworks, please see the intro at http://search.cpan.org/~jrockway/Catalyst-Manual/lib/Catalyst/Manual/Intro.pod
to get started.

A series of HTML-based templates are used to generate web pages. These are stored in /htmltemplates/src and /htmltemplates/lib.
The Template Toolkit system is used to generate HTML from these which Catalyst outputs to the browser via the GwasCentral::Web::View::TT module.
Information on how this works can be found at http://template-toolkit.org.

=head1 SEE ALSO

L<GwasCentral::Web::Controller::Root>, L<GwasCentral::Web::View::TT>, L<Catalyst>, L<Template>

=cut

package GwasCentral::Web;

use strict;
use warnings;
use FindBin;
use Catalyst::Runtime;
use Catalyst::Log::Log4perl;
use Env;

our $VERSION = '4.0';

# Load Catalyst itself, with various plugins & options
use Catalyst qw[
  -Log=info
  -Stats=1
  ConfigLoader
  Session::Store::File
  Session
  Session::State::Cookie
  Authentication
  Browser
  Unicode::Encoding
  -Debug
  StackTrace
  UploadProgressSizeLimit
  Cache::FastMmap
  Static::Simple
];
 

# Then configure our application


# if no environment variable, sets this using the current Bin path
!$ENV{GWASCENTRAL_HOME} and $ENV{GWASCENTRAL_HOME} = $FindBin::Bin . "/../"; #IMPORTANT - base directory for web application

my $home = $ENV{GWASCENTRAL_HOME}
  || die "FATAL ERROR:GWASCENTRAL_HOME environment variable not set!";
warn "home dir:$home";

__PACKAGE__->config(
	name                   => 'GwasCentral::Web',
	'Plugin::ConfigLoader' => { file => $home . "/conf/main.conf",
		driver => { 'General' => { -InterPolateVars => 1, -IncludeDirectories => 1, -IncludeRelative=>1, UseApacheInclude=>1 } }
		 },
);
__PACKAGE__->config->{recaptcha}->{pub_key} =
                          '6LdvzAoAAAAAAE6qhKU9lw4ElRyozgIDwf5nul6S';
__PACKAGE__->config->{recaptcha}->{priv_key} =
                          '6LdvzAoAAAAAAOK--j0cMX3h4sLMgkxpwdPlGklK';

#set DBIx::Class debug trace using the config
$ENV{DBIC_TRACE}         = 0;
$ENV{CATALYST_TRACE} = 1;

#set up session to store in DB
__PACKAGE__->config->{'session'} = {
        expires   => 3600,
        dbi_dbh   => 'Session',
        dbi_table => 'sessions',
        cookie_name => 'gwascentral',
    };
    
# Set up more configurable logger, replacing stock Catalyst one. Log::Log4perl
# can be configured entirely from a master config-file
__PACKAGE__->log(
	Catalyst::Log::Log4perl->new( $home . "/conf/log4perl.conf" ) );

# Set up Static::Simple, for serving static files from the application's root
__PACKAGE__->config->{static} = {
	no_logs           => 1,
	include_path      => [ $home . '/static' ],
	ignore_extensions => [qw/tmpl tt tt2/]
};

# Finally start the application
__PACKAGE__->setup();


 
=head1 AUTHOR
  
  Rob Free <rcfree@gmail.com>
  Gudmundur A. Thorisson <gthorisson@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
1;
