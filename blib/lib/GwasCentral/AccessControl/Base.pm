#base class for AccessControl modules
package GwasCentral::AccessControl::Base;
use Moose;
extends qw(GwasCentral::Base);
has 'identity' => ( 'is' => 'rw', 'trigger' => \&_trigger_identity );
has 'user'=>('is'=>'rw');
has 'access_state' => ('is'=>'rw');
has 'sessionid' => ('is'=>'rw', 'trigger' => \&_get_webapp_session );
has 'session_data' => ('is'=>'rw');
has 'Connect' => ('is'=>'rw');
has 'identity_hash' => ('is'=>'rw');

use Data::Dumper qw(Dumper);
use GwasCentral::Schema::Session;
use MIME::Base64;
use Storable qw/nfreeze thaw/;

no Moose;

sub _trigger_identity {
	my ($self, $new_val, $old_val) = @_;
	$self->update_identity($new_val);
}

sub _set_user_and_access_level {
	my ( $self ) = @_;
	$self->throw("_set_user_and_access_level not implemented");
}

sub _get_webapp_session {
	my ($self, $sessionid) = @_;
	
	#unable to access session data from a completely different Apache instance
	#this method gets the binary representation of a session from the Session database, decodes it and returns it as a standard data structure
	my $sconfig = $self->config->{'Model::SessionDB'};
	my $sess_db = HGVbaseG2P::Schema::Session->connect(@{$sconfig->{connect_info}});
	my $session = $sess_db->resultset('Sessions')->search({"id"=>"session:".$sessionid})->single;
	!$session and $self->throw("Session ID '$sessionid' not found in Session DB");
	my $data = thaw( decode_base64($session->session_data) );
	my $user = $data->{user} || $data->{__user};
	$self->session_data($data);
	$self->log->info("Got session data from session '$sessionid'");
	$self->identity($user ? $user->{email} : undef);
}

sub get_study_access_levels {
	my ( $self ) = @_;
	$self->throw("get_study_access_levels not implemented");
}


sub get_phenotype_and_experiment_access_levels {
	my ( $self ) = @_;
	$self->throw("get_phenotype_and_experiment_access_levels not implemented");
}

sub get_study_and_experiment_access_levels {
	my ( $self ) = @_;
	$self->throw("get_study_and_experiment_access_levels not implemented");
}

sub get_resultset_access_level {
	my ( $self ) = @_;
	$self->throw("get_resultset_access_levels not implemented");
}

#sub extract_admin_access_studies {
#	my ($self, $identifiers) = @_;
#	#return ['HGVST156','HGVST200','HGVST300'];
#	return $identifiers;
#}


sub is_current_user_admin_of_any_studies {
	my ( $self ) = @_;
	$self->throw("is_current_user_admin_of_any_studies not implemented");
}

sub store_levels {
	my ($self) = @_;
	$self->throw("store_levels not implemented");
}

sub get_studies_with_admin_level {
	my ( $self ) = @_;
	$self->throw("get_studies_with_admin_level");
}
1;