package GwasCentral::DataSource::Study::PhenotypeList;
use Moose;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;
use FindBin;
use lib "$FindBin::Bin/../../../../lib";
use GwasCentral::Base qw(shorten_author_list);
use List::Util qw(sum);
sub get_pmethod_list {
	my ( $self, $args ) = @_;
	$self->log->info("get local pmethods");
	my ($opts, $params) = $self->_prepare_pmethod_list_options($args);
	return ([],0) if !$opts;
	$self->log->info("final params:".Dumper($params)." and opts:".Dumper($opts));
	
	my $pmethod_rs         = $self->dbh->resultset("Phenotypemethod")->search($params, $opts);
	return ( [], 0, undef) if !$pmethod_rs;
	my $pager = $args->{Query}->page_size eq 'all' ? undef : $pmethod_rs->pager;
	my @pmethods          =  $pmethod_rs->all;

	$self->populate_pmethod_access_levels( \@pmethods );
	
	my @results = map { $self->pmethod_to_result($_) } @pmethods;
		
	return (\@results,$pager);
}

sub _prepare_pmethod_list_options {
	my ($self, $args) = @_;
	my $q = $args->{Query};
	return (undef,undef) if $q->no_results;
	my $page_size = $q->page_size;
	my $opts = {};
	if ($page_size ne 'all') {
		$opts = {
			rows => int($page_size),
			page => int($q->page),
		};
	}
	my $params = {'studyid.ishidden'=>'no'};
	$q->fval('Threshold') and $params->{'studyid.significancelevel'}={'>='=>$q->fval('Threshold') };
	#$q->fval(low_count and $params->{'(select experimentid2.totalmarkersimported from Experiment experimentid2 where experimentid2.studyid = me.studyid order by experimentid2.totalmarkersimported desc limit 0,1)'} = {'>'=>$low_count,'<'=>$high_count};
	
	my $ident = $q->f('IdentifierList');
	
	if (!$ident->ignore) {
		my $ident_list = $ident->as_array;
		
		if ($ident->class eq 'rs') {
			 defined($ident_list) and $params->{'resultsets.identifier'} = {-in => $ident_list };
			 $opts->{join}={'experiments'=>['resultsets','studyid'], 'phenotypepropertyid'=>{'pppas'=>'phenotypeannotationid'}};
		}
		else {
			 defined($ident_list) and $params->{'me.identifier'} = {-in => $ident_list };
			 $opts->{join}=['experiments','studyid',{'phenotypepropertyid'=>{'pppas'=>'phenotypeannotationid'}}];
		}
	
	}
	
	$q->fval('PhenotypeID') and $params->{'phenotypeannotationid.phenotypeidentifier'}={-in=>$q->fval('PhenotypeID')};
	
	my $sort_by = $q->f('SortBy')->by_index(0);
	$opts->{order_by}=$sort_by;
	$opts->{distinct}=1;
	#$self->log->info("opts:".Dumper($opts)."\nparams:".Dumper($params));
	return ($opts, $params);
}

sub pmethod_to_result {
	my ($self, $pmethod) = @_;
	my @results = ();
	my $counter = 0;
	my $pm_identifier   = $pmethod->identifier;
	my @resultsets         = ();
	my %gtbundles_flag_for = ();
	my @marker_counts      = ();

	foreach my $experiment ( $pmethod->experiments ) {

		foreach my $resultset ( $experiment->resultsets ) {
			push @resultsets, $resultset->identifier;
		}

		push @marker_counts, int( $experiment->totalmarkersimported );

	}

	my %ontology_identifiers = ();
	my @pannotations = $pmethod->phenotypepropertyid->phenotypeannotations;
	foreach my $pannotation (@pannotations) {
		my $pidentifier = $pannotation->phenotypeidentifier;
		$ontology_identifiers{$pidentifier}=1;
	}
		
	tie my %item, "Tie::Hash::Indexed";
	%item = (
		'identifier'              => $pm_identifier,
		'number'                  => $pmethod->number,
		'name'                    => $pmethod->name,
		'description'             => $pmethod->description,
		'phenotype_property'       => $pmethod->phenotypepropertyid->name,
		'accesslevel'      => $pmethod->accesslevel,
		'request_response'        => $pmethod->response_status,
		'child_accesslevel' => $pmethod->child_accesslevel,
		'resultsets'              => \@resultsets,
		'phenotype_annotations' => [map { { identifier => $_ } } keys %ontology_identifiers],
		'study_name' => $pmethod->studyid->name,
		'study_identifier' => $pmethod->studyid->identifier,
		'no_pvalues' => sum(@marker_counts),
		#'genotyping_platform'     => [ keys %gtbundles_flag_for ],
	);
	return \%item;
}

1;