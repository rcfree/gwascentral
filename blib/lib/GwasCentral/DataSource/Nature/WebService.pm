package GwasCentral::DataSource::Nature::WebService;
use FindBin;
use lib "$FindBin::Bin/../../";
use Moose::Role;
use Data::Dumper qw(Dumper);
use JSON::XS;

has 'Connect' => ('is'=>'rw');
has '_lwp' => ('is'=>'rw');
has '_json' => ('is'=>'rw');

after 'init' => sub {
	my ($self) = @_;
	my $lwp = LWP::UserAgent->new(
		cookie_jar => { file => "/var/www/gwascentral/tmp/cookies.dat", autosave => 1 },
		agent => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
	);
	$self->_lwp($lwp);
	
	my $json = JSON::XS->new->ascii->pretty->allow_nonref;
	$json->space_after(1);
	$self->_json($json);
};

sub get_json_data_from_url {
	my ($self, $path, $params_ref) = @_;
	my $lwp = $self->_lwp;
	my %params = %{$params_ref || {}};
	$params{'format'}="json";
	
	my @qstrings =();
	foreach my $key(keys %params) {
		if (ref($params{$key}) ne 'ARRAY') {
			push @qstrings, $key."=".$params{$key};
		}
		else {
			foreach my $value(@{$params{$key}}) {
				push @qstrings, $key."=".$value;
			}
		}
	}
	
	my $url = $path.(scalar(@qstrings)>0 and "?".join("&",@qstrings));
	$self->log->info("url:$url");
	my $response = $lwp->get($url);
	$self->throw("Unable to get data from external source ".$url." (".$response->status_line . ")","DataSource") if !$response->is_success;
	return $self->_json->decode($response->content);
}

sub get_url_and_ident_from_connect_id {
	my ($self, $connect_id, $entity) = @_;
	my ($url,$ident) = $self->Connect->get_url_and_ident_from_connectid($connect_id, $entity);
	return ($url,$ident);
}

1;