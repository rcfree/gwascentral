package GwasCentral::DataSource::Nature::Controlled;
use FindBin;
use lib "$FindBin::Bin/../../";
use Moose::Role;
use Data::Dumper qw(Dumper);

has 'AccessControl' => ('is'=>'rw');

#sub _trigger_AccessControl {
#	my ($self,$ac, $old_ac) = @_;
#
#	foreach my $source (values %{$self->DataSources}) {
#		if ($source->does("HGVbaseG2P::DataSource::ControlledSource")) {
#			$source->AccessControl($ac);
#		}
#	}	
#}

1;