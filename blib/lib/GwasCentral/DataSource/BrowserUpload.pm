# $Id: Browser.pm 1478 2010-06-19 22:09:16Z rcf8 $

=head1 NAME

HGVbaseG2P::Database::BrowserUpload - Query Browser HGVbaseG2P DB using DBI


=head1 SYNOPSIS

use HGVbaseG2P::Database::BrowserUpload;
my $browser_db = HGVbaseG2P::Database::BrowserUpload->new( { conf_file => '/conf/hgvbase.conf' } );
my $browser = $browser_db->get_markers_in_resultsets(['HGVRS1','HGVRS2','HGVRS3']);


=head1 DESCRIPTION

Provides a wrapper for common Browser database access methods. Returns single hashrefs or arrays of hashrefs

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Database::BrowserUpload;
use Moose;

use FindBin;
use lib "$FindBin::Bin/../../";

extends qw(GwasCentral::DataSource::Base);
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'BrowserUpload' );

use GwasCentral::Browser::Util qw(chr_list);

no Moose;

sub insert_upload_data {
	my ($self , $args ) = @_;
	my $dbh = $self->dbh;
	my $sql = "insert into HGVbaseG2P_upload.upload_data (Study_Identifier, Study_Name, Resultset_Name, PhenotypeProperty_Name, SessionID) values (?,?,?,?,?)";
	my $sth = $dbh->prepare($sql);
	$self->log->info("sql:$sql");
	$sth->execute("UPLOAD", $args->{'study_name'}, $args->{'resultset_name'}, $args->{'pproperty_name'}, $args->{'sessionid'});

	my $latest_id = $dbh->storage->dbh->{mysql_insertid};
	$sql = "update upload_data set Resultset_Identifier = ? where UploadID=?"; 
	my $rs_ident = "U".$latest_id;
	$dbh->do($sql, undef, $rs_ident, $latest_id);
	return $rs_ident;
}

sub get_upload_data_for_resultset_ident {
	my ($self , $rs_ident ) = @_;
	my $dbh = $self->dbh;
	my $sql = "select Study_Name, Resultset_Name, SessionID from upload_data where Resultset_Identifier=?";
	my $sth = $dbh->prepare($sql);
	$sth->execute($rs_ident);
	my $row = $sth->fetchrow_hashref;
	return $row;
}

sub delete_old_uploads {
	my ($self, $days) = @_;
	!$days and $days = 2;
	my $dbh = $self->dbh;
	my $sql = "select Resultset_Identifier from upload_data where TimeStamp <= SUBDATE(CURRENT_TIMESTAMP,$days);";
	my $sth = $dbh->prepare($sql);
	$sth->execute();
	while (my $row = $sth->fetchrow_arrayref) {
		my $rs_ident = $row->[0];
		$self->log->info("delete upload $rs_ident");
		$self->delete_marker_binned_chr_by_resultset(
		{ resultset => $rs_ident, binsize => 3 } );
		$self->delete_marker_binned_by_resultset(
		{ resultset => $rs_ident, binsize => 1 } );
		foreach my $chr ( keys %{ chr_list() } ) {
			$self->delete_marker_data_by_resultset_and_chr(
				{ resultset => $rs_ident, chr => $chr } );
		}

	}
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Browser.pm 1478 2010-06-19 22:09:16Z rcf8 $ 

=cut

