package GwasCentral::DataSource::Browser::BinSearch;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;
use GwasCentral::Base qw(shorten_author_list);
use strict;
use warnings;

sub get_binned_data {
	my ( $self, $args ) = @_;
	my $q = $args->{Query};

	my $rs_filter = $q->f('AddedResultsets');

	#obtain resultset access levels and take into account uploads

	my %binned_data_for;

	#get browser data for all resultsets on a particular server
	my $threshold = $q->fval('Threshold');
	my $start;
	my $stop;
	my $chr;
	if (ref($q)=~/Region/) {
		my $qs = $q->f('QueryString');
		$chr = $qs->chr_no;
		$start = $qs->start;
		$stop = $qs->stop;
	}
	else {
		$chr = $q->fval('MultiChr');
		$chr eq 'ALL' and $chr = undef;
	}
	
	
	$self->log->info("chr:$chr, threshold:$threshold");
	
	my $method;
	if ( $chr && length( $chr ) > 0 ) {
		if ( $start && $stop ) {
			$method = '_get_marker_binned_by_resultsets_range_and_threshold';
		}
		else {
			$method = '_get_marker_binned_by_resultsets_chr_and_threshold';
		}
	}
	else {
		$method = '_get_marker_binned_by_resultsets_and_threshold';
	}
	my $attrs = {
		chr => $chr,
		start => $start,
		stop => $stop,
		resultsets => $rs_filter->resultsets,
		threshold => $threshold,
		bin_size => $args->{bin_size}
	};
	$self->log->info("attrs:".Dumper($attrs));
	$self->log->info("method:$method");
	return $self->$method($attrs);
	
}

sub _get_marker_binned_by_resultsets_and_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{threshold};
	my $statements = {
		full=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb",
	  admin=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb",
	    limited=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM marker_binned_ltd_chr_"
	  . $attrs->{bin_size}
	  . "mb",
	   none=>"SELECT Reference, Resultset_Identifier, '-1' as Scores"
	  . "  FROM marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb",
	  upload=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM `GC_upload_4-2`.marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb",
	};
	
	my ($sth,$rset_idents) = $self->_execute_union_sql_for_access_levels($attrs,$statements);
	!$sth and return {};
	
	my %bin_count_for = ();
	my %marker_bins_for=();
	
	while ( my $row = $sth->fetchrow_hashref ) {
		my $chr = $row->{Reference};
		my @scores = split(";",$row->{Scores});
		push @{$marker_bins_for{$chr}},\@scores;
		$bin_count_for{$chr}=scalar(@scores);
	}
	$sth->finish;

	return $self->_output_marker_binned_by_chr($rset_idents, \%marker_bins_for, \%bin_count_for);
}
#
sub _output_marker_binned_by_chr {
	my ($self, $rset_idents_aref, $marker_bins_for_href, $bin_count_for_href) = @_;
	my %bin_count_for = %{$bin_count_for_href};
	my @rset_idents = @{$rset_idents_aref};
	my %marker_bins_for = %{$marker_bins_for_href};
	
	my %bin_data_for=();
	
	foreach my $chr(keys %marker_bins_for) {
		my $curr_chr_bin_count = $bin_count_for{$chr};
		for(my $bin_no=0;$bin_no<$curr_chr_bin_count;$bin_no++) {
			my @chr_marker_bins=();
			foreach(my $rscount=0;$rscount<scalar(@rset_idents);$rscount++) {
				push @chr_marker_bins,$marker_bins_for{$chr}->[$rscount]->[$bin_no] || 'X';
			}
			my $bin_start = $bin_no * 3000000;
			push @{$bin_data_for{$chr}}, {
				Reference=>$chr,
				bin_start=>$bin_start + 1,
				bin_stop=>$bin_start + 3000000,
				bin_counts=>join("|",@chr_marker_bins)
			};
		}
	}
	$self->log->info("bin_data_for:".Dumper(\%bin_data_for));
	return \%bin_data_for;
}

sub _get_marker_binned_by_resultsets_chr_and_threshold {
	my ( $self, $attrs ) = @_;
	
	my $chrs = $attrs->{chr};

	$chrs =~ s/\s/','/g;
	
	my $threshold = $attrs->{threshold};
	
	my $statements = {
	limited => "SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
		  . "  FROM marker_binned_ltd_chr_"
		  . $attrs->{bin_size}
		  . "mb ",
	full=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb ",
	  admin=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb ",
	none=>"SELECT Reference, Resultset_Identifier, '-1' as Scores"
		  . "  FROM marker_binned_chr_"
		  . $attrs->{bin_size}
		  . "mb ",
	upload=>"SELECT Reference, Resultset_Identifier, ScoreList$threshold as Scores"
	  . "  FROM `GC_upload_4-2`.marker_binned_chr_"
	  . $attrs->{bin_size}
	  . "mb ",
	};
	my $where = "Reference IN ('$chrs')";
	my ($sth,$rset_idents) = $self->_execute_union_sql_for_access_levels($attrs,$statements, $where);
	
	!$sth and return {};
	
	my %bin_count_for = ();
	my %marker_bins_for=();
	
	while ( my $row = $sth->fetchrow_hashref ) {
		my $chr = $row->{Reference};
		my @scores = split(";",$row->{Scores});
		push @{$marker_bins_for{$chr}},\@scores;
		$bin_count_for{$chr}=scalar(@scores);
	}
	$sth->finish;

	return $self->_output_marker_binned_by_chr($rset_idents, \%marker_bins_for, \%bin_count_for);
}

sub _get_marker_binned_by_resultsets_range_and_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{threshold};
	my $bin_size = $attrs->{bin_size} * (10**6);
	
	my $statements = {
	limited => "SELECT Start,Score$threshold, Resultset_Identifier FROM marker_binned_ltd_1mb",
	full=>"SELECT Start,Score$threshold, Resultset_Identifier FROM marker_binned_1mb",
	admin=>"SELECT Start,Score$threshold, Resultset_Identifier FROM marker_binned_1mb",
	none=>"SELECT Start,'0', Resultset_Identifier FROM marker_binned_1mb",
	upload=>"SELECT Start,Score$threshold, Resultset_Identifier FROM `GC_upload_4-2`.marker_binned_1mb"
	};
	my $where = "Reference=? and Start>=? and Stop<=?";
	my ($sth,$rs_idents) = $self->_execute_union_sql_for_access_levels($attrs,$statements, $where, ['chr','start','stop']);
	!$sth and return [];
	
	my %bin_counts_for=();
	while (my $row = $sth->fetchrow_arrayref) {
		my $range = $row->[0];
		push @{$bin_counts_for{$range}},$row->[1];
	}
	$sth->finish;
	my @results;
	foreach my $start_pos(sort keys %bin_counts_for) {
		
		my $counts_list = join("|",@{$bin_counts_for{$start_pos}});
		push @results, { Reference=>$attrs->{chr}, bin_start=>$start_pos, bin_stop=>($start_pos + ($bin_size-1)), bin_counts=>$counts_list };
	}
	return \@results;
}




1;