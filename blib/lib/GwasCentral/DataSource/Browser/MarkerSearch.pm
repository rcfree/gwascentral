package GwasCentral::DataSource::Browser::MarkerSearch;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;
#use GwasCentral::Base qw(shorten_author_list);
use strict;
use warnings;

sub get_marker_data {
	my ($self,$attrs) = @_;
	my $q = $attrs->{Query};
	my $qs = $q->f('QueryString');
	my $rs_filter = $q->f('AddedResultsets');
	
	if ( $qs->chr ) {
		if ( $qs->start && $qs->stop ) {
			return $self->_get_marker_data_by_range_and_resultsets(
				{
					chr        => $qs->chr_no,
					start      => $qs->start,
					stop       => $qs->stop,
					resultsets => $rs_filter->resultsets,
				}
			);
		}
		else {
			return $self->_get_marker_data_by_chr_and_resultsets(
				{
					chr => $qs->chr_no,
					resultsets => $rs_filter->resultsets,
				}
			);
		}
	}
}

sub _get_marker_data_by_chr_and_resultsets {
	my ( $self, $attrs ) = @_;
	my @data = ();
	my $statements = {
	limited => "SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, UnadjustedPValue, NegLogPValue, Strand, SignificanceID, Resultset_Identifier FROM marker_data_ltd_chr"
		  . $attrs->{chr},
	full=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, NegLogPValue, UnadjustedPValue, Strand, SignificanceID, Resultset_Identifier FROM marker_data_chr"
		  . $attrs->{chr},
  admin=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, NegLogPValue, UnadjustedPValue, Strand, SignificanceID, Resultset_Identifier FROM marker_data_chr"
  . $attrs->{chr},
	none=>undef,
	upload=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, NegLogPValue, UnadjustedPValue, Strand, SignificanceID, Resultset_Identifier FROM `GC_upload_4-2`.marker_data_chr"
		  . $attrs->{chr},
	};

	my ($sth,$rs_idents) = $self->_execute_union_sql_for_access_levels($attrs,$statements);
	!$sth and return \@data;

	while ( my $row = $sth->fetchrow_hashref ) {
		push @data, $row;
	}
	$sth->finish;
	
	return \@data;
}

sub _get_marker_data_by_range_and_resultsets {
	my ( $self, $attrs ) = @_;
	my $statements = {
	limited => "SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, UnadjustedPValue, NegLogPValue, Strand, VariationType, Resultset_Identifier, ValidationCode FROM marker_data_ltd_chr"
		  . $attrs->{chr},
	full=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, UnadjustedPValue, NegLogPValue, Strand, VariationType, Resultset_Identifier, ValidationCode FROM marker_data_chr"
		  . $attrs->{chr},
		 admin=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, UnadjustedPValue, NegLogPValue, Strand, VariationType, Resultset_Identifier, ValidationCode FROM marker_data_chr"
		  . $attrs->{chr},
	none=>undef,
	upload=>"SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, UnadjustedPValue, NegLogPValue, Strand, VariationType, Resultset_Identifier, ValidationCode FROM `GC_upload_4-2`.marker_data_chr" . $attrs->{chr},
	};
	my $where = "Start>=? and Stop<=?";
	my ($sth,$rs_idents) = $self->_execute_union_sql_for_access_levels($attrs,$statements, $where, ['start','stop']);
	
	my @data = ();
	return [] if !$sth;
	
	while ( my $row = $sth->fetchrow_hashref ) {
		push @data, $row;
	}

	$sth->finish;

	return \@data;
}
1;