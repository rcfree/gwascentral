 # $Id: Mesh.pm 1324 2009-09-17 14:33:32Z rcf8 $

=head1 NAME

HGVbaseG2P::Database::Mesh - Query Mesh HGVbaseG2P DB using Class::DBIx ORM


=head1 SYNOPSIS

use HGVbaseG2P::Database::Mesh;
my $mesh_db = HGVbaseG2P::Database::Mesh->new( { conf_file => '/conf/hgvbase.conf' } );
my @headings = $mesh_db->get_headings_from_terms('cancer');

=head1 DESCRIPTION

Provides a wrapper for common Mesh database access methods. Returns single or arrays of HGVbaseG2P::Schema objects (derived from DBIx::Class) or
a DBIx::Class ResultSet object

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataSource::Ontology;
use Moose;
extends qw(GwasCentral::DataSource::Base);
with qw(GwasCentral::DataSource::Nature::DBIC);
has '+source_name' => ( 'default' => 'Ontology' );
	no Moose;
	
	use English qw( -no_match_vars );
	use Data::Dumper;
	
	use Carp qw(croak cluck);

=head2 get_headings_from_concepts

	  Usage      : my @headings = $mesh_db->get_headings_from_concepts('breast cancer');
	  Purpose    : Gets concept, heading and treeid by concept name from Mesh DB
	  Returns    : DBIx::Class::Resultset containing HGVbaseG2P::Schema::Mesh::Concept objects
	  Arguments  : A MeSH concept name
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut
	
	sub get_mesh_headings_from_concepts {
		my ($self, $category) = @_;
		return $self->dbh->resultset('MeshConcept')->search({'conceptname' => $category,},
    		{ join      =>{'mesh_termtree', 'mesh_heading'},
     		 '+select' => ['mesh_termtree.treeID', 'mesh_heading.meshid'],
     		 '+as'     => ['treeid', 'heading_meshid'],
     		 order_by  => ['id'], 
    		});
	}
	
	
=head2 get_headings_from_terms

	  Usage      : my @headings = $mesh_db->get_headings_from_terms('cancer of the breast');
	  Purpose    : Gets term, heading and treeid by term name from Mesh DB
	  Returns    : DBIx::Class::Resultset containing HGVbaseG2P::Schema::Mesh::Term objects
	  Arguments  : A MeSH term name
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut
	
	
	sub get_mesh_headings_from_terms {
		my ($self, $category) = @_;
		return $self->dbh->resultset('MeshTerm')->search({'termname' => $category,},
    		{ join      =>{'mesh_termtree', 'mesh_heading'},
     		 '+select' => ['mesh_termtree.treeID', 'mesh_heading.meshid'],
     		 '+as'     => ['treeid', 'heading_meshid'],
     		 order_by  => ['id'], 
    		});
	}

	
	
=head2 get_children_from_treeid

	  Usage      : my @children = $mesh_db->get_children_from_treeid('C04.588.180');
	  Purpose    : Gets term tree by tree id from Mesh DB
	  Returns    : DBIx::Class::Resultset containing HGVbaseG2P::Schema::Mesh::Termtree objects
	  Arguments  : A MeSH tree id / tree number
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut	
	
	
	sub get_mesh_children_from_treeid {
		my ($self, $treestring) = @_;
		return $self->dbh->resultset('MeshTermtree')->search({ 'treeid' => { -like => $treestring}});
	}
	


=head2 get_term_from_name

	  Usage      : my @terms = $mesh_db->get_term_from_name('cancer of');
	  Purpose    : Gets term by (partial) term name from Mesh DB
	  Returns    : DBIx::Class::Resultset containing HGVbaseG2P::Schema::Mesh::Term objects
	  Arguments  : A MeSH term name / partial MeSH term name
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

	
	sub get_mesh_term_from_name {
		my ($self, $querystring) = @_;
		return $self->dbh->resultset('MeshTerm')->search({ 'me.termname' => { 'like' => $querystring }, 'me.meshused' => '1'},   
 			{ order_by => 'termname',
  			rows => 15 }
		);
	}
	
	
	
=head2 get_heading_from_id

	  Usage      : my $id = $mesh_db->get_heading_from_id('D001943');
	  Purpose    : Get heading in MeshDB using heading id
	  Returns    : HGVbaseG2P::Schema::Mesh::Heading object 
	  Arguments  : MeSH heading id
	  Throws     : 
	  Status     : Public
	  Comments   : 

=cut
	
	
	sub get_mesh_heading_from_id {
		my ($self, $queryid) = @_;
		return $self->dbh->resultset('MeshHeading')->search({ 'meshid' => $queryid })->first;
	}
	
	sub get_mesh_headings_from_id {
		my ($self, $queryids) = @_;
		my @rs = $self->dbh->resultset('MeshHeading')->search({meshid => {-in => $queryids }})->all;
		return @rs;
	}
	
	
	# moved from Hpo.pm
	
	sub get_hpoTerm_from_meshid {
		my ($self, $queryid) = @_;
		return $self->dbh->resultset('Hpo2mesh')->search({ 'meshid' => $queryid },
			{ join      =>{'hpo_term'},
     		 '+select' => ['hpo_term.name'],
     		 '+as'     => ['hponame'],
     		 group_by => 'hpoID',
			});
	}
	
	sub get_name_from_id {
		my ($self, $queryid) = @_;
		return $self->dbh->resultset('HpoTerm')->search({ 'hpoid' => $queryid },
		{'+select' => ['me.name'],
     		 '+as'     => ['hponame'],
     		 group_by => 'hpoID',
		});
	}
	
	sub get_single_term_from_name {
		my ($self, $category) = @_;
		return $self->dbh->resultset('HpoTerm')->search({'me.name' => $category });
	}
	
	sub get_single_synonym_from_name {
		my ($self, $category) = @_;
		return $self->dbh->resultset('HpoSynonym')->search({'me.synonymText' => $category });
	}
	
	sub count_terms_by_name {
		my ($self, $qstring) = @_;
		return $self->dbh->resultset('HpoTerm')->search({ name => $qstring })->count;
	}
	
	sub count_synonyms_by_name {
		my ($self, $qstring) = @_;
		return $self->dbh->resultset('HpoSynonym')->search({ synonymtext => $qstring })->count;
	}
	
	sub get_meshid_from_hpoid {
		my ($self, $queryid) = @_;
		return $self->dbh->resultset('Hpo2mesh')->search({ 'hpoid' => $queryid });
	}
	
	sub get_children_terms_from_id {
		my ($self, $ipid) = @_;
		return $self->dbh->resultset('HpoTermPath')->search({'me.ancestor_hpoid' => $ipid });
	}
	
	sub get_term_from_name {
		my ($self, $querystring) = @_;
		return $self->dbh->resultset('HpoTerm')->search({ 'me.name' => { 'like' => $querystring }, 'me.hgvbaseuse' => '1'},   
 			{ order_by => 'name',
  			rows => 8 }
		);
	}
	
	# gets synonyms for terms that are used in hgvbase annotations
	sub get_synonym_from_name {
		my ($self, $querystring) = @_;
		return $self->dbh->resultset('HpoSynonym')->search({ 'me.synonymtext' => { 'like' => $querystring }, 'hpo_term.hgvbaseuse' => '1'},   
 			{ join => {'hpo_term'},
 			order_by => 'synonymtext',
  			rows => 6 }
		);
	}

1;


=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Tim Beck <tb143@le.ac.uk>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: Mesh.pm 1324 2009-09-17 14:33:32Z rcf8 $

=cut
1;
