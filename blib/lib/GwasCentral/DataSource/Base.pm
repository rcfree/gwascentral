package GwasCentral::DataSource::Base;
use Moose;
extends qw(GwasCentral::Base);
use Data::Dumper qw(Dumper);
has 'dsn' => ('is'=>'rw');
has 'user' => ('is'=>'rw');
has 'pass' => ('is'=>'rw');
has 'options' => ('is'=>'rw');
has 'name' => ('is'=>'rw');
has 'host' => ('is'=>'rw');
has 'type' => ('is'=>'rw');

no Moose;

sub options_from_config {
	my ($self) = @_;
	my $source_name = $self->source_name;
	my $sconfig = $self->config->{Database}->{$source_name};
	my $dbconfig = $self->config->{Database};
	# Connect to database using either info from config OR parameters provided as
    # arguments to the constructor-call. Arguments will override configfile
    my $dsn = $self->dsn || $sconfig->{dsn} || $dbconfig->{dsn};
	my $host;
	my $type;
	my $name;
	
	if (!$dsn) {
		$name = $sconfig->{name};
		$type =  $sconfig->{type} || $dbconfig->{type};
		$host =  $sconfig->{host} || $dbconfig->{host};
		$dsn = "dbi:".$type.":".($name || $self->throw("No DSN provided or not able to construct it from the database config:".Dumper($dbconfig)." or source config:".Dumper($sconfig)));
		$dsn.=($host and ":".$host);
	}
	else {
		my @temp = split(':',$dsn);
		$type = $temp[1];
		$name = $temp[2];
	 	$host = $temp[3];
	}
	$self->name($name);
	$self->type($type);
	$self->host($host);

	my $dbuser = $self->user || $sconfig->{user} || $dbconfig->{user} || getlogin();
	my $dbpassw = $self->pass || $sconfig->{pass} || $dbconfig->{pass};
	my $dboptions = $self->options || $sconfig->{Options} || $dbconfig->{Options} || {};
	$self->user($dbuser);
	$self->pass($dbpassw);
	$self->options($dboptions);
	return {dsn=>$dsn, user=> $dbuser, pass=>$dbpassw, options=>$dboptions};
	
}

sub mysql_options {
	my ($self, $prefix, $suffix) = @_;
	my $sys = $prefix." -h ".$self->host." -u".$self->user." ".($self->pass ne "" and " -p".$self->pass." ").$suffix;
	$self->log->info("sys:$sys");
	return $sys;
}

sub drop_database {
	my ($self) = @_;
	system $self->mysql_options("mysql -e 'drop database if exists ".$self->name."'","");
}

sub create_database {
	my ($self) = @_;
	system $self->mysql_options("mysql -e 'create database if not exists ".$self->name."'","");
}

sub build_schema {
	my ($self) = @_;
	my $source_name = $self->source_name;
	my $sconfig = $self->config->{Database}->{$source_name};
	my $basedir = $self->config->{basedir};
	my $schema_file = $sconfig->{schema_file} || "GC_".lc($source_name)."_schema.sql";
	my $schema_loc = $basedir."/conf/schema/".$schema_file;
	system $self->mysql_options("cat $schema_loc | mysql",$self->name);
}


sub BUILD {
	my ($self) = @_;
	$self->can('init') and $self->init;
}

sub make_sql_placeholders {
	my ($self, $number) = @_;

	if (ref($number) eq 'ARRAY') {
		$number = scalar(@{$number});
	}
	my $temp = "?," x $number;
	my $placeholders = substr($temp, 0, length($temp)-1);
	return $placeholders;
}

1;