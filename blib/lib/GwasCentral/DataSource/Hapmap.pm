package GwasCentral::DataSource::Hapmap;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use Data::Dumper qw(Dumper);
extends qw(GwasCentral::DataSource::Base);
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'Hapmap' );

sub create_ld_table {
	my ($self, $chr) = @_;
	$self->dbh->do("CREATE TABLE IF NOT EXISTS `$chr"."_ldscore` (
  `fstart` int(11) DEFAULT NULL,
  `fstop` int(11) DEFAULT NULL,
  `pop_code` varchar(5) DEFAULT NULL,
  `marker1` varchar(20) DEFAULT NULL,
  `marker2` varchar(20) DEFAULT NULL,
  `dprime` double DEFAULT NULL,
  `rsquare` double DEFAULT NULL,
  `lod` double DEFAULT NULL,
  `fbin` int(11) DEFAULT NULL,
  KEY `fstartlod` (`fstart`,`pop_code`,`lod`),
  KEY `fstartrsquare` (`fstart`,`pop_code`,`rsquare`),
  KEY `fstartdprime` (`fstart`,`pop_code`,`dprime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1");
}

sub disable_ld_table_keys {
	my ($self, $chr) = @_;
	my $table = $chr."_ldscore";
	$self->dbh->do("ALTER TABLE $table DISABLE KEYS");
}

sub enable_ld_table_keys {
	my ($self, $chr) = @_;
	my $table = $chr."_ldscore";
	$self->dbh->do("ALTER TABLE $table ENABLE KEYS");
}

sub import_ld_file {
	my ($self, $filename, $chr) = @_;
	my $table = $chr."_ldscore";
	my $sql = "LOAD DATA INFILE '$filename' IGNORE INTO TABLE $table FIELDS TERMINATED BY ' ' (fstart,fstop,pop_code,marker1,marker2,dprime,rsquare,lod,fbin);";
	$self->dbh->do($sql);
}

sub get_table_status {
	my ($self, $table) = @_;
	my $sth = $self->dbh->storage->dbh->prepare("show table status where Name=?");
	$sth->execute($table);
	return $sth->fetchrow_hashref;
}

sub create_temp_ldtable {
	my($self, $chr) = @_;
	$self->dbh->storage->dbh->do("CREATE TABLE IF NOT EXISTS `TEMP_chr".$chr."_ldscore` (
  `fstart` int(11) default NULL,
  `fstop` int(11) default NULL,
  `pop_code` varchar(5) default NULL,
  `marker1` varchar(20) default NULL,
  `marker2` varchar(20) default NULL,
  `dprime` double default NULL,
  `rsquare` double default NULL,
  `lod` double default NULL,
  `fbin` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;");

}

sub count_records {
	my($self, $table) = @_;
	my $sth = $self->dbh->storage->dbh->prepare("select count(*) from $table");
	$sth->execute();
	my $row = $sth->fetchrow_arrayref;
	return $row->[0];
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Database.pm 782 2008-06-26 13:39:21Z rcf8 $

=cut


1;
