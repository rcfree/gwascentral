package GwasCentral::DataSource::Feature::GeneList;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;
#use GwasCentral::Base qw(shorten_author_list);

sub get_gene_list {
	my ( $self, $args ) = @_;
	$self->log->info("get local studies");
	#my ($opts, $params) = $self->_prepare_study_list_options($args);
	my $q = $args->{Query};
	my $qs = $q->f('QueryString');
	my @features = ();
	if ($qs->is_region) {
		$self->log->info("chr:".$qs->chr.", start:".$qs->start.", stop:".$qs->stop);
		@features = $self->dbh->features(
			-ref => $qs->chr,
			-start => $qs->start,
			-stop => $qs->stop,
			-types => [qw(mRNA five_prime_UTR three_prime_UTR CDS)]
		);
	}
	else {
		my @sorted = @{$qs->sorted_features};
		@features = map { $_->{feat} } @sorted;
	}
	
	
	my $fpf = $q->fval('FivePrimeFlank');
	my $tpf = $q->fval('ThreePrimeFlank');
	my %temp_results = ();
	for my $f(@features) {
		if ($f->display_name) {
			
			my $type = $f->type;
			$type=~s/\:UCSC//;
			tie my %hash, "Tie::Hash::Indexed";
			%hash = (
				id => $f->primary_id,
				gene_symbol => $f->display_name,
				chr => $f->ref,
				start => $f->start,
				stop => $f->stop,
				gene_size => $f->stop - $f->start,
				type => $type,
				region => $f->ref.":".($f->start - $fpf)."..".($f->stop + $tpf),
				aliases => [ $f->get_tag_values('Alias') ]
			);
			$temp_results{$hash{region}}=\%hash;
		}
	}
	my @results = values %temp_results;
	
	my $sb = $q->f('SortBy');
	my @sorted;
	my $pager = $q->new_pager(scalar(@results));
	my $is_ascending = $sb->by_index(3);
	my $is_numeric = $sb->by_index(2);
	my $sort_key = $sb->by_index(0);
	if ($is_numeric) {
		if ($is_ascending) {
			@sorted = reverse sort { $a->{$sort_key} <=> $b->{$sort_key} } @results;
		}
		else {
			@sorted = sort { $a->{$sort_key} <=> $b->{$sort_key} } @results;
		}
	}
	else {
		if ($is_ascending) {
			@sorted = reverse sort { $a->{$sort_key} cmp $b->{$sort_key} } @results;
		}
		else {
			@sorted = sort { $a->{$sort_key} cmp $b->{$sort_key} } @results;
		}
	}
	return ($q->get_page_of_data(\@sorted),$pager);
}

1;