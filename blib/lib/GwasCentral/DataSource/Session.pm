package GwasCentral::DataSource::Session;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(GwasCentral::DataSource::Base);
# HGVbaseG2P::DataSource::Study::StudyItem
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'Session' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

sub get_study_by_identifier {
	my ($self, $ident)=@_;
	$self->log->debug("get study by identifier $ident");
	
	$self->throw("Study identifier not supplied","DataSource") if !$ident;

	my $study = $self->dbh->resultset("Study")->search({ Identifier => $ident })->first;
	$self->throw("Study $ident was not found","DataSource") if !$study;
	
	return $study;
}

#sub get_study_by_identifier {
#	my ($self, $connectid)=@_;
#	$self->log->info("get study by identifier $connectid");
#	
#	$self->throw("Study identifier not supplied","DataSource") if !$connectid;
#	
#	my ($url, $ident) = $self->get_url_and_ident_from_connect_id($connectid,"study");
#	
#	my $study;
#	
#	if ($url) {
#		$self->log->info("identity:".$self->AccessControl->identity_hash);
#		$self->Connect->do($self->AccessControl->identity_hash);
#		$study = $self->get_json_data_from_url($url);
#	}
#	else {
#		$study = $self->dbh->resultset("Study")->search({ Identifier => $ident })->first;
#		$self->throw("Study $ident was not found","DataSource") if !$study;
#		$study->gcid($self->Connect->local_instance->{label}.".study.".$ident);
#		$self->_populate_study_access_levels($study);
#	}
#	
#	return $study;
#}

sub get_resultset_by_identifier {
	my ($self, $connectid)=@_;
	$self->log->info("get resultset by identifier $connectid");
	
	$self->throw("Resultset identifier not supplied","DataSource") if !$connectid;
	
	my ($url, $ident) = $self->get_url_and_ident_from_connect_id($connectid,"resultset");
	
	my $study;
	
	if ($url) {
		$self->log->info("identity:".$self->AccessControl->identity_hash);
		$self->Connect->do($self->AccessControl->identity_hash);
		$study = $self->get_json_data_from_url($url);
	}
	else {
		$study = $self->dbh->resultset("Resultset")->search({ Identifier => $ident })->first;
		$self->throw("Resultset $ident was not found","DataSource") if !$study;
		$study->gcid($self->Connect->local_instance->{label}.".resultset.".$ident);
		#$self->_populate_study_access_levels($study);
	}
	
	return $study;
}
	
sub get_all_unhidden_studies {
	my ($self) = @_;
#	my @studies = inner(@_);
#	
#	#populate access levels from AccessControl module for each study
#	$self->_populate_study_access_levels(\@studies);	
#	
	my $params = {ishidden => 'no' };
	#$self->portal and $params->{'portal'}=$self->portal;
	return $self->dbh->resultset('Study')->search($params)->all;
}

sub get_all_phenotypemethods {
	my ($self)=@_;
	my @pmethods = inner(@_);
	my $params = {'studyid.ishidden' => 'no' };
	#$self->portal and $params->{'studyid.portal'}=$self->portal;
	return $self->dbh->resultset("Phenotypemethod")->search($params,{join=>'studyid'})->all();
}

#sub get_resultset_idents_from_study {
#	my ($self,$study)=@_;
#	!$study and $self->throw("Study object not supplied");
#	my @rs_idents = inner(@_);
#	return @rs_idents;
#};
#
#sub get_assoc_experiments_from_study_ident {
#	my ($self,$study_ident)=@_;
#	!$study_ident and $self->throw("Study identifier not supplied");
#	my @exps = inner(@_);
#	return @exps;
#};

#sub get_paged_phenotypemethods_by_identifiers_with_uniqueids {
#	my ($self,$args) = @_;		
#	my ($pmethods,$count,$pager) = inner(@_);
#	    
#	#populate access levels from AccessControl module for each phenotype method
#	$self->_populate_pmethod_access_levels($pmethods);	
#	
#	return ($pmethods,$count,$pager);
#}
#	
#sub get_paged_phenotypemethods_by_resultset_identifiers {
#	my ($self,$args) = @_;		
#    
#    my ($pmethods,$count,$pager) = inner(@_);
#    
#    #populate access levels from AccessControl module for each phenotype method
#	$self->_populate_pmethod_access_levels($pmethods);	
#    return ($pmethods,$count,$pager);
#}

sub _populate_study_access_levels {
	my ($self, $studies_ref) = @_;
	
	#only bother getting access levels if is_controlled
	if (ref($studies_ref) ne 'ARRAY') { 
		$studies_ref=[$studies_ref];
	}
	my %levels = $self->AccessControl->get_study_and_experiment_access_levels($studies_ref);

	foreach my $study(@{$studies_ref}) {
			my $study_ident = $study->identifier;
			$study->accesslevel($levels{$study_ident}->{accesslevel});
			$study->response_status($levels{$study_ident}->{response});
			$study->child_accesslevel($levels{$study_ident}->{all_experiments});
	}
}

#sub _populate_study_list_access_levels {
#	my ($self, $studies_ref) = @_;
#	
#	#only bother getting access levels if is_controlled
#	if (ref($studies_ref) ne 'ARRAY') { 
#		$studies_ref=[$studies_ref];
#	}
#	my %levels = $self->AccessControl->get_study_and_experiment_access_levels($studies_ref);
#
#	foreach my $study(@{$studies_ref}) {
#			my $study_ident = $study->{identifier};
#			$study->accesslevel($levels{$study_ident}->{accesslevel});
#			$study->response_status($levels{$study_ident}->{response});
#			$study->child_accesslevel($levels{$study_ident}->{all_experiments});
#	}
#}

sub _populate_pmethod_access_levels {
	my ($self, $pmethods_ref) = @_;
	
	#only bother getting access levels if is_controlled
	if (ref($pmethods_ref) ne 'ARRAY') { $pmethods_ref=[$pmethods_ref]; }
	my %levels = $self->AccessControl->get_phenotype_and_experiment_access_levels($pmethods_ref);
	
	foreach my $pmethod(@{$pmethods_ref}) {
		my $study_ident = $pmethod->studyid->identifier;
		$pmethod->accesslevel($levels{$study_ident}->{study});
		$pmethod->response_status($levels{$study_ident}->{response});
		$pmethod->child_accesslevel($levels{$study_ident}->{all_experiments});
	}
}
	
#sub get_paged_phenotypemethods_rs {
#	my ($self,$args) = @_;		
#	
#	#do subclass method
#    my ($pmethods,$count,$pager) = inner(@_);
#	
#	#obtain access levels from AccessControl module for each phenotype method
#	$self->_populate_pmethod_access_levels($pmethods);	
#	
#	return ($pmethods,$count,$pager);
#}

#sub count_phenotypeannotations_by_phenotypeidentifier {
#	return inner(@_);
#}
#
#sub get_resultsets_by_identifiers {
#	return inner(@_);
#}
#
#
#sub count_resultsets_in_study_by_identifier {
#	return inner(@_);
#}
#
#sub get_resultset_by_identifier {
#	my ($self) = @_;
#	$self->throw("No 'get_resultset_by_identifier' method");
#}



1;
