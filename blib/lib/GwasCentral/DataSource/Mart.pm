package GwasCentral::DataSource::Mart;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use Data::Dumper qw(Dumper);
extends qw(GwasCentral::DataSource::Base);
with qw(
GwasCentral::DataSource::Nature::DBI
);

has '+source_name' => ( 'default' => 'Mart' );

after 'init' => sub {
	my ($self) = @_;
	$self->{curr_dataset_id}=1;
};

=head2 get_dataset_id

  Usage      : $dataset_id=$db->get_dataset_id("study");
  Purpose    : Get dataset id from database when given dataset name
  Returns    : Scalar containing dataset id
  Arguments  : A dataset name
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_dataset_id {
	my ($self, $dataset) = @_;
	
	#get dataset id key from dataset name
    my $sql = "SELECT dataset_id_key FROM meta_conf__dataset__main WHERE dataset=?";
    my @params = ();
    push @params, lc($dataset);
    my $dataset_id=$self->get_scalar_value($sql,\@params);
    return $dataset_id; 
}

=head2 get_dataset_id

  Usage      : $newdb=$db->drop_and_create($db_cfg);
  Purpose    : Drop and create new db based on supplied database config hash (see new)
  Returns    : New database object based on config
  Arguments  : Database config hash (see new)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub drop_and_create {
	my ($self) = @_;
	my $dbname = $self->name;
	$self->log->info("Dropping $dbname");
	$self->execute_statement("DROP DATABASE IF EXISTS ".$self->name);
	$self->execute_statement("CREATE DATABASE IF NOT EXISTS ".$self->name);
}

=head2 update_conf_xml

  Usage      : $db->update_conf_xml($compressed,$dataset_id);
  Purpose    : Update config XML in mart for specific dataset
  Returns    : Nothing
  Arguments  : A scalar containing compressed XML data
  			   A current dataset id
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub update_conf_xml {
	my ($self,$compressed,$dataset_id) = @_;
	
	#update table with compressed XML
    my $sql = "UPDATE meta_conf__xml__dm SET xml=?, compressed_xml=?, message_digest=? WHERE dataset_id_key = ?";
 	my @params = ();
 	
 	push @params, $compressed->{uncompressed};
 	push @params, $compressed->{compressed};
 	push @params, $compressed->{digest};
 	push @params, $dataset_id;

	$self->execute_statement($sql,\@params);
}

=head2 update_template_xml

  Usage      : $db->update_template_xml($compressed,$dataset);
  Purpose    : Update template config XML in mart for specific dataset
  Returns    : Nothing
  Arguments  : A scalar containing compressed template XML data
  			   A current dataset name
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub update_template_xml {
	my ($self, $compressed,$dataset) = @_;
	my $sql = "UPDATE meta_template__xml__dm SET compressed_xml=? WHERE template = ?";
 	my @params = ();
 	
 	push @params, $compressed->{compressed};
 	push @params, $dataset;
 	
	$self->execute_statement($sql,\@params);
}

=head2 DESTROY

  Usage      : $db->DESTROY;
  Purpose    : Disconnects database and removes associated properties
  Returns    : Nothing
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub DESTROY {
	my $self = shift;
	if ($self->dbh) {
		$self->dbh->disconnect;
	}
}

=head2 get_scalar_value

  Usage      : $scalar = $db->get_scalar_value($sql,$parameters)
  Purpose    : Get scalar result from supplied SQL statement and parameters
  Returns    : Scalar value
  Arguments  : Select based SQL statement
               Arrayref of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_scalar_value {
	my ($self,$sql,$parameters) = @_;

	my $sth=$self->prepare_statement($sql,@{$parameters});	
	my @row=$sth->fetchrow_array;
	$sth->finish;
	
	return $row[0];
}

=head2 is_greater_than_zero

  Usage      : $boolean = $db->is_greater_than_zero($sql,$parameters)
  Purpose    : Determine if value obtained from supplied SQL statement and parameters is greater than zero 
  Returns    : 0 if not greater than zero and 1 if it is
  Arguments  : Select based SQL statement
               Arrayref of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub is_greater_than_zero {
	my ($self,$sql,$parameters) = @_;
	
	#fetch scalar of count
	my $rec_count = $self->get_scalar_value($sql, @{$parameters});

	#return true or false depending on count
	if ($rec_count>0) { return 1; }
	else { return 0; }
}

=head2 get_row_as_array

  Usage      : @array = $db->get_row_as_array($sql,$parameters)
  Purpose    : Get array of first record from supplied SQL statement and parameters
  Returns    : Array of first record
  Arguments  : Select based SQL statement
               Arrayref/scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_row_as_array {
	my ($self,$sql,@parameters)=@_;
	
	my $dbh = $self->{db_handle};
	
	my $sth=$dbh->prepare($sql);

	$sth->execute(@parameters) or die;
	
	my @row=$sth->fetchrow_array;
	return @row;
}

=head2 execute_statement

  Usage      : $db->execute_statement($sql,$parameters)
  Purpose    : Cleanly execute SQL (update or insert) with parameters
  Returns    : Nothing
  Arguments  : Select based SQL statement
               Arrayref/scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub execute_statement {
	my ($self,$sql,$parameters)=@_;
	
	my $dbh = $self->dbh;
	
	$self->log->info("execute_statement: $sql\n");

	
	#get db handle and prepare SQL
	my $sth=$dbh->prepare($sql) or $self->throw("Unable to prepare $sql");
	
	$sth->{PrintError}  = 0;
	$sth->{HandleError} = sub {
	    $self->throw("Error executing statement:$sql");
	};

	if (!$parameters) {
		$sth->execute;
	}
	elsif (ref($parameters) eq "SCALAR") {
		$sth->execute($parameters);
	}
	elsif (ref($parameters) eq "ARRAY") {
		$sth->execute(@{$parameters});
	}
	
	$sth->finish;
}

=head2 prepare_statement

  Usage      : $db->prepare_statement($sql,$parameters)
  Purpose    : Prepare SQL with parameters
  Returns    : Statement handle for row retrieval etc.
  Arguments  : Select based SQL statement
               Arrayref or scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub prepare_statement {
	my ($self,$sql,$parameters)=@_;
	
	my $dbh = $self->dbh;
	
	$self->log->info("prepare_statement: $sql\n");
		
	#get db handle and prepare SQL
	my $sth=$dbh->prepare($sql) or die;

	$sth->{HandleError} = sub {
		$self->throw("Error preparing statement");
	};
	
	if (scalar($parameters)) {
		
		$sth->execute($parameters);
	}
	else {
		$sth->execute(@{$parameters});
	}
	return $sth;
}

=head2 execute_script

  Usage      : $db->execute_script($script)
  Purpose    : Execute SQL script line by line
  Returns    : Nothing
  Arguments  : Array of SQL statements (see get_script)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub execute_script {
	my ($self,$script)=@_;
	my $logger = Log::Log4perl->get_logger();
	
	my $dbh = $self->dbh;
	my $line = 0;
	#execute each statement
	foreach my $query(@{$script}) {
		next if !$query;
		next if $query=~/^\-\-\-/;
		$line++;
		if ($query=~/^[alter|update].+_count/) {
 #       	$self->log->info("execute_script: $line IGNORED - ignoring remaining lines $query");
        	last;
        }
        else {
			if (length(chomp($query)) != 0){ 
#				$self->log->info("execute_script: $line - $query"); 
				$dbh->do($query) or $self->throw("Error executing script (line $line)");
			}
        }
	}
}



=head2 add_dataset_config

  Usage      : $db->add_dataset_confiupdate_conf_xmlg($dataset,$description,$visible);
  Purpose    : Create new dataset in mart config tables
  Returns    : Nothing
  Arguments  : Dataset name
               Description of dataset (appears in MartView)
               Whether dataset appears as visible (0 or 1)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub add_dataset_config {
	my ($self,$dataset,$description,$visible)=@_;
	my $sth  = $self->dbh->prepare("select dataset_id_key from `meta_conf__dataset__main` order by dataset_id_key DESC");
	$sth->execute();
	my $row = $sth->fetchrow_arrayref;
	
	my $dataset_id=++$row->[0];
	
	#create config in interface table
	my $sql="INSERT INTO `meta_conf__interface__dm` (dataset_id_key,interface) VALUES(?,?)";
	my @params = ( $dataset_id, "default");
	$self->execute_statement($sql,\@params);
	
	#create config in user table
	$sql="INSERT INTO `meta_conf__user__dm` (dataset_id_key,mart_user) VALUES(?,?)";
	@params = ( $dataset_id, "default");
	$self->execute_statement($sql,\@params);
	
	#create config in main table
	$sql="INSERT INTO `meta_conf__dataset__main` (dataset_id_key,dataset,display_name,type,visible,description,version) VALUES(?,?,?,?,?,'','')";
	@params = ( $dataset_id, $dataset, $description, "TableSet",$visible);
	$self->execute_statement($sql,\@params);
	
	#create config in xml table
	$sql="INSERT INTO `meta_conf__xml__dm` (dataset_id_key) VALUES(?)";
	@params = ($dataset_id);
	$self->execute_statement($sql,\@params);
	
	#create config in template table
	$sql="INSERT INTO `meta_template__template__main` (dataset_id_key,template) VALUES(?,?)";
	@params = ($dataset_id,$dataset);
	$self->execute_statement($sql,\@params);
	
	#create config in template xml table
	$sql="INSERT INTO `meta_template__xml__dm` (template) VALUES(?)";
	@params = ($dataset);
	$self->execute_statement($sql,\@params);
	
	#create config in template xml table
	$sql="INSERT INTO `meta_version__version__main` (version) VALUES(0.7)";
	$self->execute_statement($sql);
	return $dataset_id;
}

#=head2 add_dataset_config
#
#  Usage      : $db->add_dataset_confiupdate_conf_xmlg($dataset,$description,$visible);
#  Purpose    : Create new dataset in mart config tables
#  Returns    : Nothing
#  Arguments  : Dataset name
#               Description of dataset (appears in MartView)
#               Whether dataset appears as visible (0 or 1)
#  Throws     : 
#  Status     : 
#  Comments   : 
#
#=cut
#
#sub clear_configs {
#	my ($self,$dataset,$description,$visible)=@_;
#	
#	my @script = ("TRUNCATE `meta_conf__interface__dm`",
#	"TRUNCATE `meta_conf__user__dm`",
#	"TRUNCATE `meta_conf__dataset__main`",
#	"TRUNCATE `meta_conf__xml__dm`",
#	"TRUNCATE `meta_template__template__main`",
#	"TRUNCATE `meta_template__xml__dm`",
#	"TRUNCATE `meta_version__version__main`");
#	$self->execute_script(\@script);
#}

=head2 import_tsv_file

  Usage      : $db->import_tsv_file($importfile,$importtable);
  Purpose    : Import TSV (tab separated values) file into a table
  Returns    : Nothing
  Arguments  : Import TSV file name 
               Table to import TSV file into
  Throws     : 
  Status     : 
  Comments   : Order of columns must be identical in both db table and TSV file 

=cut

sub import_tsv_file {
	my ($self, $importfile, $importtable) = @_;
	my $sql = "LOAD DATA LOCAL INFILE '$importfile' INTO TABLE $importtable FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n'";
	$self->execute_statement($sql);
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Database.pm 782 2008-06-26 13:39:21Z rcf8 $

=cut


1;
