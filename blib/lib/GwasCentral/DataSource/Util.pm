#DataSource util module -> for creating multiple DataSources from a list, a config or extracting from Catalyst
package GwasCentral::DataSource::Util;
use base qw(Exporter);

use Moose;
use Data::Dumper qw(Dumper);
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use GwasCentral::Base qw(load_config load_module);
use Exporter;
our @EXPORT_OK=qw(DataSources_from_config DataSources_from_context DataSources_from_models DataSources_from_list new_AccessControl new_DataSource);


sub new_DataSources {
	my $self = shift;
	my @ds = map { load_module("GwasCentral::DataSource::$_",{conf_file=>$self->config, AccessControl=>$self->AccessControl}) } @_;
	return wantarray ? @ds : $ds[0];
}

#create DataSources in a hash using the 'datasources' option in a config
sub DataSources_from_config {
	my ($config,$ac, $connect) = @_;
	my %config = load_config($config);
	
	my @datasources = split(/\ /,$config{datasources});
	

	return DataSources_from_list(\@datasources, $config,$ac,$connect);
}

#create DataSources in a hash using the 'datasources' option in a config
sub DataSources_from_list {
	my ($ds_list, $config,$ac, $connect) = @_;
	
	my @datasources = @{$ds_list};
	my %ds;
	my %config = load_config($config);
	foreach my $ds_name(@datasources) {
		my $ds_mod = load_module("GwasCentral::DataSource::$ds_name",{conf_file=>\%config});

		if ($ds_mod->does('GwasCentral::DataSource::Nature::Controlled')) {
			$ds_mod->AccessControl($ac);
			
		}
		
		my @temp = split('::',$ds_name);
		$ds{$temp[0]}=$ds_mod;
	}

	return \%ds;
}

sub new_AccessControl {
	my ($config, $host, $identity) = @_;
	my %config = load_config($config);
	my $ac_module = $config{control_module} || 'None';
    
	my $acontrol = load_module("GwasCentral::AccessControl::$ac_module", { 
		conf_file => \%config, 
		local_url => $host,
	});
	warn "ac_module:$ac_module";
	$acontrol->identity_hash( $identity );
	return $acontrol;
}


sub new_DataSource {
	my ($ds_name,$config,$args) = @_;
	my $opts = $args;
	$opts->{ conf_file } =$config; 
	my $ds = load_module("GwasCentral::DataSource::$ds_name",$opts);
	return $ds;
}

sub DataSources_from_models {
	my ( $models ) = @_;

	my %required_db = (
		'Study'    => 'Study',
		'Xapian'     => 'Xapian',
		'Browser'  => 'Browser',
		'Feature' => 'Feature',
		'Marker'   => 'Marker',
		'Ontology'  => 'Ontology',
	);
	my %databases = ();
	foreach my $db ( keys %required_db ) {
		my $model = $models->{"$db"} or warn "$db -> ".$required_db{$db}." not found in Catalyst models";	
		$databases{$required_db{$db}}=$model;
	}
	return \%databases;
}

sub models_from_context {
	my ( $context ) = @_;
	return { map { $_ => $context->model($_) } $context->models };
}

sub DataSources_from_context {
	my ( $context ) = @_;
	my $models = models_from_context($context);
	return DataSources_from_models($models);
}
1;