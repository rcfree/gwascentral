package GwasCentral::DataSource::Browser;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(GwasCentral::DataSource::Base GwasCentral::DataSource::Browser::BinSearch GwasCentral::DataSource::Browser::MarkerSearch);
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'Browser' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

sub get_marker_by_id {
	my ($self,$id) = @_;
	my $sth = $self->dbh->prepare("select * from all_markers where Identifier = ? or Accession = ?");
	$sth->execute($id,$id);
	return $sth->fetchrow_hashref;
}

sub count_markers_in_region_above_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{'threshold'};
	my $chr       = lcfirst($attrs->{'chr'});
	my $start = $attrs->{'start'};
	my $stop      = $attrs->{stop};
	my $pager = $attrs->{pager};
	my $has_list = $attrs->{display} eq 'asd' ? 1 : 0;
	
	$threshold eq 'ZERO' and $threshold = '0';
	my $sql =
"select count(*) from marker_significances_"
	  . $chr
	  . " where Start>? and Start<? ";
	  $has_list and $sql.=" and HasList='1'";
	 if ($threshold && $has_list) {
	 	$sql.=" and length(SignificanceList$threshold)>0";
	 }
	my $sth = $self->dbh->prepare($sql);
	$sth->execute( $start, $stop );
	my $row = $sth->fetchrow_arrayref;
	return $row->[0];
}

sub get_markers_in_region_above_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{'threshold'};
	my $chr       = lcfirst($attrs->{'chr'});
	my $start = $attrs->{'start'};
	my $stop      = $attrs->{stop};
	my $pager = $attrs->{pager};
	my $has_list = $attrs->{display} eq 'asd' ? 1 : 0;
	my $first = $pager->first - 1;
	my $page_size = $pager->entries_per_page;
	my $sort_by = $attrs->{sort_by};
	$threshold eq 'ZERO' and $threshold = '0';
	my $sql =
"select Marker_Identifier, HasList, SignificanceList$threshold from marker_significances_"
	  . $chr
	  . " where Start>=? and Start<=? ";
	  $has_list and $sql.=" and HasList='1' ";
	  if ($threshold && $has_list) {
	 	$sql.=" and length(SignificanceList$threshold)>0 ";
	 }
	  $sql.=" order by $sort_by limit $first,$page_size";
	  $self->log->info("sql:$sql");
		my $sth = $self->dbh->storage->dbh->prepare($sql);
	$sth->execute( $start, $stop );
	my %marker_data;
	my %no_assoc_data;
	my %pvalue_count;
	while ( my $row = $sth->fetchrow_arrayref ) {
		my $mid = $row->[0];
		$marker_data{$mid} = 1;
		my @resultsets = $row->[2] ? split(";",$row->[2]) : ();
		$row->[1] ne '1' and $no_assoc_data{$mid}=1;
		$pvalue_count{$mid} = @resultsets; 
	}
	return ([ keys %marker_data ],\%no_assoc_data, \%pvalue_count );
}

sub get_studies_rsets_and_markers_in_region_above_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{'threshold'};
	my $chr       = $attrs->{'chr'};
	my $start = $attrs->{'start'};
	my $stop      = $attrs->{stop};
	
	$threshold eq 'ZERO' and $threshold = '0';
	my $sql =
"select Marker_Identifier, StudyList$threshold, SignificanceList$threshold from marker_significances_chr"
	  . $chr
	  . " where Start>=? and Start<=? and SignificanceList$threshold is not null";
	return $self->_fetch_studies_rsets_and_markers_from_sigificances($sql,[$start,$stop]);
}

sub get_studies_rsets_and_markers_by_identifier_above_threshold {
	my ( $self, $attrs ) = @_;
	my $threshold = $attrs->{'threshold'};
	my $identifier       = $attrs->{'identifier'};
	#my $id_list = "'".join ("','",@{$identifier});
	my $chr = $attrs->{'chr'};
	$threshold eq 'ZERO' and $threshold = '0';
	
	my $sql =
"select Marker_Identifier, StudyList$threshold, SignificanceList$threshold from marker_significances_"
	  . $chr
	  . " where Marker_Identifier =?";
	return $self->_fetch_studies_rsets_and_markers_from_sigificances($sql,[$identifier]);
}

sub _fetch_studies_rsets_and_markers_from_sigificances {
	my ($self, $sql, $parameters) = @_;
	my $sth = $self->dbh->storage->sth($sql);
	$self->log->info("sql:$sql");
	$sth->execute( @{$parameters} ) or die;
	
	my %marker_data;
	my %study_data;
	my %resultset_data;
	
	while ( my $row = $sth->fetchrow_arrayref ) {
		my $mid = $row->[0];
		length($row->[1])>0  and $marker_data{$mid} = 1;
		foreach my $study (split( ";", $row->[1] )) {
			$study_data{$study}++;
		}

		foreach my $rs_sig (split( ";", $row->[2] )) {
			my ( $rs, $sig ) = split( /\|/, $rs_sig );
			$resultset_data{$rs}++;
		}
	}

	return ( [ keys %study_data] , [ keys %resultset_data], [keys %marker_data] );
}

sub delete_gene_resultsets_by_identifier {
	my ( $self, $attrs ) = @_;
	my $sql =
	    "DELETE FROM gene_resultsets WHERE Resultset_Identifier=?";

	my @pars = ();

	$self->dbh->do( $sql, undef, $attrs->{resultset} );
}

sub delete_marker_binned_by_resultset {
	my ( $self, $attrs ) = @_;
	my $sql =
	    "DELETE FROM marker_binned_"
	  . $attrs->{binsize}
	  . "mb WHERE Resultset_Identifier=?";

	$self->dbh->do( $sql, undef, $attrs->{resultset} );
}

sub delete_marker_binned_ltd_by_resultset {
	my ( $self, $attrs ) = @_;
	my $sql =
	    "DELETE FROM marker_binned_ltd_"
	  . $attrs->{binsize}
	  . "mb WHERE Resultset_Identifier=?";

	my @pars = ();

	$self->dbh->do( $sql, undef, $attrs->{resultset} );
}

sub delete_marker_binned_chr_by_resultset {
	my ( $self, $attrs ) = @_;
	my $sql =
	    "DELETE FROM marker_binned_chr_"
	  . $attrs->{binsize}
	  . "mb WHERE ResultSet_Identifier=?";
		$self->log->debug("delete rs:".$attrs->{resultset});

	$self->dbh->do( $sql, undef, $attrs->{resultset} );
}
#
sub delete_marker_binned_ltd_chr_by_resultset {
	my ( $self, $attrs ) = @_;
	my $sql =
	    "DELETE FROM marker_binned_ltd_chr_"
	  . $attrs->{binsize}
	  . "mb WHERE ResultSet_Identifier=?";
	my @pars = ();
	$self->dbh->do($sql, undef, $attrs->{resultset} );

}
#
sub delete_marker_data_by_resultset_and_chr {
	my ( $self, $attrs ) = @_;
	my $sql =
	  "DELETE FROM marker_data_chr" . $attrs->{chr} . " WHERE Resultset_Identifier=?";

	$self->dbh->do( $sql, undef, $attrs->{resultset} );
}
#
#sub truncate_marker_significances {
#	my ( $self, $chr ) = @_;
#	check_chr($chr);
#	$self->dbh->do("TRUNCATE marker_significances_chr$chr");
#}
#
#sub truncate_marker_significances_ltd {
#	my ( $self, $chr ) = @_;
#	check_chr($chr);
#	$self->dbh->do("TRUNCATE marker_significances_ltd_chr$chr");
#}
sub delete_marker_data_ltd_by_resultset_and_chr {
	my ( $self, $attrs ) = @_;
	my $sql =
	  "DELETE FROM marker_data_ltd_chr" . $attrs->{chr} . " WHERE Resultset_Identifier=?";

	$self->dbi_delete( $sql, [ $attrs->{resultset}, ], $attrs );
}

sub _execute_union_sql_for_access_levels {
	my ($self, $attrs, $sql_statements_by, $where, $extra_parameters) = @_;
	my %resultsets = %{$attrs->{resultsets} || {}};
	my %by_access_level=();
	foreach my $rset_ident (keys %resultsets) {
		my $rset = $resultsets{$rset_ident};
		$by_access_level{$rset->accesslevel}||=[];
		push @{$by_access_level{$rset->accesslevel}},$rset;
	}

	my $rset_ident_field = $attrs->{rset_ident_field} || 'Resultset_Identifier';
	$where = $where ? $where.=" and " : " ";
	my @combined_resultsets = ();
	my @sql_statements=();
	my @parameters = ();

	foreach my $level(keys %by_access_level) {
		my @rsets_by_curr_level = @{$by_access_level{$level}};
		next if scalar(@rsets_by_curr_level)==0;
			@combined_resultsets = (@combined_resultsets, @rsets_by_curr_level);
		  if ($extra_parameters) {
		  	my @temp_pars = map { $attrs->{$_} } @{$extra_parameters};
		  	@parameters = (@parameters,@temp_pars, map { $_->identifier } @rsets_by_curr_level);
		  }
		  else {
		  	@parameters = (@parameters,map { $_->identifier } @rsets_by_curr_level);
		  }
		  $sql_statements_by->{$level} and push @sql_statements,$sql_statements_by->{$level}." WHERE $where $rset_ident_field IN (".$self->make_sql_placeholders(scalar(@rsets_by_curr_level)).")";		  
	} 
	
	scalar(@sql_statements)==0 and return (undef, \@combined_resultsets);
	
	my $union_sql=join(" UNION ",@sql_statements)." ORDER BY Resultset_Identifier";
	my $dbh = $self->dbh->storage->dbh;
	my $sth = $dbh->prepare($union_sql);
	$self->log->info("union_sql:".$union_sql);
	$sth->execute(@parameters);
	$self->log->info("parameters:'".join("','",@parameters)."'");
	return ($sth,\@combined_resultsets);
}

1;
