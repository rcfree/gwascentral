# $Id: Genome.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

GwasCentral::Browser::Genome - Generate features for Browser genome view


=head1 SYNOPSIS
		
	my $browser = GwasCentral::Browser::Genome->new(
		{
			conf_file =>
			  $home."/conf/hgvbase.conf"
		}
	);

	$browser->autodb(1);
	my $threshold   = $pars->{threshold} || 3;
	my $resultsets = $pars->{resultsets};
	my $stackheight = $pars->{stackheight} || 10;
	my $settings = $pars->{settings};
	my $tracks = $pars->{tracks};
	my $sessionid = $pars->{sessionid};
	my $logscale    = 0;
    
    my $feature_data;

	$feature_data = $browser->generate_features(
		{
			feature_list => $self->{feature_list},
			threshold   => $threshold,
			resultsets => $resultsets,
			chr => $chr,
			stackheight => $stackheight,
			tracks => $tracks,
			settings => $settings,
			sessionid => $sessionid
		}
	);
	  
=head1 DESCRIPTION

Deals with the generation of GFF features for the GenomeView 'plugin' which runs in the modified hgvbrowse_karyotype CGI script

=head1 SUBROUTINES/METHODS

TODO

=cut

package GwasCentral::Browser::Genome;

use Moose;
extends qw(GwasCentral::Browser::Core);
has 'max_count' => ( 'is' => 'rw' );
has 'rs_counts' => ( 'is' => 'rw' );
no Moose;
use GwasCentral::Browser::Util qw(chr_length);
use Carp qw(cluck);
use constant GENOME_MB => 3;          #genome bin size default (megabases)
use constant GENOME_BP => 3000000;    #genome bin size default (base pairs)
use Data::Dumper qw(Dumper);
	
=head2 generate_features

	  Usage      : my ($attrs) = $browser->generate_features({
    				bin_size=>3000000,
    				threshold=>$threshold,
    				resultsetid=>$resultsetid,
    				logscale=>$logscale,

    			   });
    			   
	  Purpose    : Main method to generate feature data for use by RegionView.pm gbrowse plugin.
	  Returns    : $attrs - attributes passed in from RegionView plugin
	  				
	  Arguments  : In hash ref
	  				 threshold - -log of P value threshold
	  				 resultsets - comma-separated list of resultsetids to generate genome data for
	  				 logscale - flag indicating whether log (1) or linear (0) scale should be used
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub generate_features {
	my ( $self, $attrs ) = @_;
	my @max_counts = ();
	my $config     = $self->config;
	#cluck "prepare_browser\n".Dumper(\@INC);
	my %messages = $self->prepare_browser($attrs);
	
	my $chr_data = $self->get_marker_binned_data( { bin_size => GENOME_MB } );

	$self->seg_start(1);
	
	my $height =
	  ( $self->chr && length( $self->chr ) < 3 )
	  ? $self->setting('region_stacked_height')
	  : $self->setting('genome_stacked_height');

	#add sig counts glyph
	$self->add_sig_counts_glyph( { height => $height } );

   #if genome_present track switched on add glyph indicating presence of markers
   if ( $self->track('genome_present') ) {
		$self->add_bins_present_glyph( { height => int( $height / 10 ) } );
	}
	
	my $rs_counts = { map { $_=>0 } @{ $self->resultsets_and_uploads }};

	#loop through each chromosome and add stacked plot data for each bin within
	foreach my $chr_name ( keys %{$chr_data} ) {
		my ($chr_max_count);
		( $chr_max_count, $rs_counts ) = $self->add_sig_counts(
			{
				data      => $chr_data->{$chr_name},
				chr       => $chr_name,
				rs_counts => $rs_counts
			}
		);

		if ( $self->track('genome_present') ) {

			$self->seg_stop( chr_length($chr_name) );
			$self->add_bins_present(
				{
					data             => $chr_data->{$chr_name},
					bin_size         => GENOME_BP,
					bin_size_compact => GENOME_MB,
					chr              => $chr_name,
				}
			);
		}
		defined($chr_max_count) and push @max_counts, $chr_max_count;
	}

	#find largest count in whole genome - log it if logscale flag is 1
	my @sorted = reverse sort { $a <=> $b } @max_counts;

	#reset the max_score attribute of the type to the largest count
	my $max_count = $sorted[0];
	$self->features->set( 's', max_score => $max_count );

	$self->max_count($max_count);
	$self->rs_counts($rs_counts);
	$self->messages( \%messages );

	#return the feature list
	return $self->features;
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: Genome.pm 1526 2010-09-16 13:48:27Z rcf8 $ 

=cut
