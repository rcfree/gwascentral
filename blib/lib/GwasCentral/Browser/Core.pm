# $Id: Core.pm 1540 2010-09-22 08:35:36Z rcf8 $

=head1 NAME

HGVbaseG2P::Browser::Core - High level logic for G2P genome and region view browsers


=head1 SYNOPSIS

e.g. an example sub class
	package HGVbaseG2P::Browser::Example;

	use Moose;
	extends qw(HGVbaseG2P::Browser::Core);
	  
=head1 DESCRIPTION

The Browser modules make use of the Database::Browser and Browser::Binning modules to generate feature data for GBrowse plugins.
This Core module contains the feature generation logic used by the separate Genome.pm and Region.pm subclasses.

The separation of core logic makes it easier to test the module by use of regression/unit testing. It also reduces
the amount of code in the GBrowse config file which is useful for error checking and debugging etc.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Browser::Core;

use Moose;
extends qw(GwasCentral::Base);
has 'autodb' => ('is'=>'rw');

use English qw( -no_match_vars );
use Data::Dumper;
use Math::Round qw(:all);
use FindBin;
use lib "$FindBin::Bin/../lib";
use Bio::Graphics::Feature;
use Bio::Graphics::FeatureFile;
use Bio::Graphics::Panel;
use File::Temp qw(tempfile);
#use HGVbaseG2P::AccessControl;
use GwasCentral::DataSource::Browser;
use GwasCentral::Browser::Binning;
use GwasCentral::Base qw(load_config init_logger new_pager);
use GwasCentral::Browser::Util
  qw(chr_list chr_length split_data_by_field format_bases poslog);
use GwasCentral::DataSource::Study;

use List::Util qw(sum max);
use List::Compare;
use Carp qw(croak cluck confess);

my $VERSION = "2.00";
use MIME::Base64;
use Storable qw/freeze thaw/;

# Private variables & constants
has 'DataSources' => ( 'is' => 'rw' );    #contains browser DB object
has 'seg_start'   => ( 'is' => 'rw' );    #contains actual segment start
has 'seg_stop'    => ( 'is' => 'rw' );    #contains actual segment stop
has 'round_start' => ( 'is' => 'rw' );    #contains rounded-down start
has 'round_stop'  => ( 'is' => 'rw' );    #contains rounded-up stop
has 'chr'         => ( 'is' => 'rw' );    #contains chr no (eg. 5)
has 'ref'         => ( 'is' => 'rw' );    #contains full chr (eg. Chr5)
has 'features'    => ( 'is' => 'rw' );
;    #reference to all features whether genome or region based

has '_resultsets' => ( 'is' => 'rw', 'default' => sub { [] } );
;    #arrayref containing resultset identifiers
has 'threshold' => ( 'is' => 'rw' );    #significance threshold
has 'autodb'    => ( 'is' => 'rw' );    #flag for auto create DB object
has 'logscale' => ( 'is' => 'rw' );
;    #flag to specify if 'count' graphs have log scale

has 'featureno' => ( 'is' => 'rw' );
;    #specifies incrementable count for features
has 'summaries' => ( 'is' => 'rw' );    #access to resultset summary records
has '_tracks'   => ( 'is' => 'rw' );    #private _tracks property containing details on activated tracks
has '_settings' => ( 'is' => 'rw', 'default' => sub { {} } )
  ;                                     #private _settings property containing details on current settings
has 'messages' => ( 'is' => 'rw', 'default' => sub { [] } )
  ;                                     #contains error/warn messages
has 'access_control'   => ( 'is' => 'rw' ); #accessor to AccessControl object
has 'resultset_levels' => ( 'is' => 'rw', 'default' => sub { {} } ); #hashref of resultset identifiers and access levels (none, limited, full, admin)
has 'upload_data' => ('is' => 'rw', 'default' => sub { {} } ); #hashref of uploaded data (from session), based on upload ID as key

no Moose;

#Moose init method - set up features with empty FeatureFile
sub BUILD {
	my ( $self, $args ) = @_;

	#use supplied list or create new features list
	$self->features(
		$args->{feature_list} || Bio::Graphics::FeatureFile->new(
			-smart_features => 1,
			-safe           => undef
		)
	);
}

=head2 is_high_resolution

	  Usage      : my $is_hr = $self->is_high_resolution;
	  Purpose    : Check if need to display low or high resolution
	  Returns    : 0 for low resolution and 1 for high resolution
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

		sub is_high_resolution {
			my ($self)        = @_;
			
			#if the size of current region is >= 8 x the low res bin size (generally 1Mb) then is low resolution
			if (!$self->seg_start && !$self->seg_stop) {
				return 0;
			}
			my $region_size   = $self->seg_stop - $self->seg_start;
			
			my $use_minor_bin =
			  $region_size >= ( $self->setting("lowres_binsize") * 8 )
			  ? 0
			  : 1;
			 $self->log->debug("is high resolution returns:$use_minor_bin");
			return $use_minor_bin;
		}


#increments the feature number - used internally
sub next_featureno {
	my ($self) = @_;
	my $featureno = $self->featureno;
	if ( $self->featureno ) {
		$featureno++;
	}
	else {
		$featureno = 1;
	}
	$self->featureno($featureno);
	return $featureno;
}

=head2 resultsets

	  Usage      : my $resultsets = $self->resultsets;
	  Purpose    : Set/get list of resultset identifiers
	  Returns    : Sorted arrayref of resultset identifiers
	  Arguments  : Optional $value - arrayref of rs identifiers 
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub resultsets {
	my ( $self, $value ) = @_;
	#if $value then set the _resultsets accessor otherwise return a sorted list from the _resultsets accessor
	if ($value) {
		$self->_resultsets($value);
	}
	else {
		if ( !$self->_resultsets ) {
			return undef;
		}
		my @resultsets = sort { $a cmp $b } @{ $self->_resultsets };
		return \@resultsets;
	}
}

=head2 resultsets_and_uploads

	  Usage      : my $resultsets = $self->resultsets_and_uploads;
	  Purpose    : To get list of resultset identifiers combined with upload identifiers
	  Returns    : Sorted arrayref of resultset identifiers combined with upload identifiers
	  Arguments  :  
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub resultsets_and_uploads {
	my ( $self ) = @_;
	#merge resultsets arrayref and upload_data hashref keys, sort and return them
	my @resultsets = $self->_resultsets ? @{$self->_resultsets} : ();
	my %upload_data = $self->upload_data ? %{$self->upload_data} : ();
	my @combined = (@{$self->_resultsets}, keys %upload_data);
	my @sorted_combined = sort { $a cmp $b } @combined;
	return \@sorted_combined;
}

=head2 get_webapp_session

	  Usage      : my $session = $browser->get_webapp_session('dfgd363tegsd');
	  Purpose    : To retrieve session data for the HGVbaseG2P web application
	  Returns    : A data-structure corresponding to the Catalyst session
	  Arguments  :  
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_webapp_session {
	my ($self, $sessionid) = @_;
	
	#unable to access session data from a completely different Apache instance
	#this method gets the binary representation of a session from the Session database, decodes it and returns it as a standard data structure
	my $sconfig = $self->config->{'Model::SessionDB'};
	my $sess_db = HGVbaseG2P::Schema::Session->connect(@{$sconfig->{connect_info}});
	my $session = $sess_db->resultset('Sessions')->search({"id"=>"session:".$sessionid})->single;
	
	return $session ? thaw( decode_base64($session->session_data) ) : undef;
}

=head2 set_webapp_session

	  Usage      : my $session = $browser->get_webapp_session('dfgd363tegsd');
	  			   $session->{additional_data}=1;
	  			   $browser->set_webapp_session('dfgd363tegsd', $session);
	  Purpose    : To store session data for the HGVbaseG2P web application
	  Returns    : Nothing
	  Arguments  :  
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub set_webapp_session {
	my ($self, $sessionid, $session_data) = @_;
	my $sconfig = $self->config->{'Model::SessionDB'};
	my $sess_db = HGVbaseG2P::Schema::Session->connect(@{$sconfig->{connect_info}});
	my $session = $sess_db->resultset('Sessions')->search({"id"=>"session:".$sessionid});

	my $encoded_data = encode_base64(freeze($session_data));
	$session->update({session_data=>$encoded_data });
}

=head2 prepare_browser

	  Usage      : package HGVbaseG2P::Browser::Example
	  			   use Moose;
	               extends qw(HGVbaseG2P::Browser::
	               
	               sub generate_features {
	               	my ($self,$attrs) = @_;
	                my %messages = $self->prepare_browser($attrs);
	                ...
	  			   
	  Purpose    : Key method used by derived classes to setup browser with threshold, chr, genomic coords, current web app session, upload data and resultset access levels etc.
	               If there are problems highlights where these are (particularly with data access etc).
	  Returns    : A hash containing the messages related to data access
	  Arguments  : $attrs - a hash of setup attributes including tracks, settings, threshold, resultsets, logscale, sessionid
	  Throws     : 
	  Status     : Private
	  Comments   : 
	
=cut

sub prepare_browser {
	my ($self, $attrs) = @_;
	
	my @resultsets = split( /,/, $attrs->{resultsets} );
	my $threshold  = $attrs->{threshold} || die "No threshold provided";
	my $logscale   = $attrs->{logscale} || 1;
	my $chr        = $attrs->{chr} || $attrs->{ref};
	my $sessionid = $attrs->{sessionid};
	$self->tracks( $attrs->{tracks} || [] );
	$self->settings( $attrs->{settings} || {});

	#set browser accessors with attributes
	$chr        and $self->chr($chr);
	$threshold  and $self->threshold($threshold);
	@resultsets and $self->resultsets( \@resultsets );
	$logscale   and $self->logscale($logscale);
	
	my $webapp_data = $self->get_webapp_session($sessionid);
    if ($webapp_data->{upload_data}) {
    	$self->upload_data($webapp_data->{upload_data});
    }
   
	#get precalculated counts for analysis and threshold
	my $summaries = $self->get_resultset_summaries;
	$self->summaries($summaries);
	
	my %messages = $self->determine_resultset_access( $sessionid );
	return %messages;
}


=head2 determine_resultset_access

	  Usage      : my %messages = $browser->determine_resultset_access($sessionid);   
	  Purpose    : Used internally to determine access levels to the resultsets in $self->resultsets
	  Returns    : A hash containing the messages related to data access
	  Arguments  : $attrs - a hash of setup attributes including tracks, settings, threshold, resultsets, logscale, sessionid
	  Throws     : 
	  Status     : Private
	  Comments   : 
	
=cut

sub determine_resultset_access {
	my ( $self, $sessionid ) = @_;
	
	my @rs_idents = @{$self->resultsets};
	
	my $access_control =
	  HGVbaseG2P::AccessControl->new( { conf_file => $self->config } );
	
	#if access control on - get the identity from the session
	if (!$self->config->{no_access_control}) {
		$access_control->use_identity_from_session($sessionid);
	}
	
	$self->access_control($access_control);

	my %messages     = ();
	my @available_rs = ();
	my %experiment_by = ();
	my $summaries = $self->summaries;
	
	#get experiments which are related to each resultset, keyed by resultset identifier
	foreach my $rs_ident ( @rs_idents ) {
		next if !$summaries->{$rs_ident};
		
		my $rset_experiment = $summaries->{$rs_ident}->experiment;
		if ( !$experiment_by{ $rset_experiment->identifier } ) {
			$experiment_by{$rs_ident} = $rset_experiment;
		}
	}
	my @rset_experiments = values %experiment_by;
	
	#get access levels for experiments (if no access control - gets this value from study -> 'isdisplayed'
	my %exp_access_levels_by =
	  $access_control->get_experiment_access_levels( \@rset_experiments );
	
	#loop through resultsets and obtain access levels
	#depending on access level, make it available or not by adding it to the 'available_rs' list
	#add a message to the correct type of message (ie. data not available, limited data etc).
	foreach my $rs_ident ( @rs_idents ) {
		my $summary = $summaries->{$rs_ident};
		if ($summary) {
			my $exp_access_level =  $exp_access_levels_by{ $summary->experiment->identifier };
			$self->resultset_levels->{$rs_ident} = $exp_access_level;
			
			if ( $exp_access_level eq 'full' || $exp_access_level eq 'admin' ) {
				push @available_rs, $rs_ident;
			}
			elsif ( $exp_access_level eq 'limited' ) {
				push @{ $messages{'limited'} },
				  $summary->name . " (" . $summary->identifier . ")";
				push @available_rs, $rs_ident;
			}
			elsif ( $exp_access_level eq 'none' ) {
				push @{ $messages{'none'} },
				  $summary->name. " (". $summary->identifier . ")";
				  push @available_rs, $rs_ident;
			}
			else {
				$self->log->info("data from $rs_ident not available");
				push @{ $messages{'none'} },
				  $summary->name . " (" . $summary->identifier . ")";
			}
		}
		else {
			$self->log->info("Resultset $rs_ident not found in database");
			push @{ $messages{'notfound'} },
			  "$rs_ident";
		}
	}
	
	$self->resultsets( \@available_rs );
	return %messages;
}

=head2 get_resultset_summaries

	  Usage      : $browser->resultsets('HGVRS12,HGVRS13');
	               my ($summaries) = $hgvbrowser->get_resultset_summaries;
	               
	  Purpose    : Get summary data for all resultsets in the Browser object
	  Returns    : Hashref with key of Resultset identifier and value of Resultset description
	  Arguments  : None   
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_resultset_summaries {
	my ($self)    = @_;
	my $summaries = {};
	my $studies   = {};
	
	foreach my $rs_ident ( @{ $self->resultsets } ) {
		my $rs = $self->DataSources->{Study}->get_resultset_by_identifier($rs_ident);
		$summaries->{$rs_ident} = $rs;
	}
	return $summaries;
}

=head2 get_db

	  Usage      : my $db = $self->get_db;
	  Purpose    : Retrieve HGVbaseG2P::Database::Browser object. If autodb = 1 then auto create it
	  Returns    : HGVbaseG2P::Database::Browser object from $self->db
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : Not required to the same extent as DBIx::Class hack deals with reconnection etc.
	
=cut

sub get_db {
	my ( $self, $db ) = @_;
	
		if ( !$self->db ) {
			my $db = GwasCentral::DataSource::Browser->new(
				{ conf_file => $self->config } );
			$self->DataSources->{Browser}=$db;
		}
	return $self->DataSources->{Browser};
}

=head2 get_chr_max_count

	  Usage      : my ($max_count) = $browser->get_chr_max_count({
    				data=>/@data,
    			   });
    			   
	  Purpose    : Determine maximum 'bin_counts' value for supplied bin data
	  Returns    : Maximum count in data
	  Arguments  : In hash ref
	  				 data - arrayref containing data
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_chr_max_count {
	my ( $self, $attrs ) = @_;
	my $data    = $attrs->{data};
	my $analyse = GwasCentral::Browser::Analyse->new(
		{ data => $data, conf_file => $self->config } );
	my $chr_max_count = $analyse->statistic(
		{ field => 'bin_counts', type => 'Maximum', sum => 1, log => 1 } );
	return $chr_max_count;
}



=head2 get_marker_data

	  Usage      : $browser->ref('chr9');
	               my $data = $browser->get_marker_data;
    			   
	  Purpose    : Wrapper method to get different aspects of marker significance data from the Browser database. The data returned depends on the attributes for chr, round_start and round_stop.
	               If chr is set gets just data for that chromosome.
	               If a rounded start and stop range are also set, gets just data for that range.
	  Returns    : Arrayref of data
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_marker_data {
	my ( $self, $attrs ) = @_;

	my $db = $self->get_db;
	my $data;

	my %rs_levels;
	
	if ( $attrs->{level} ) {
		$self->log->debug("level:".$attrs->{level}." with ".join(",",@{$self->resultsets}));
		%rs_levels = ( $attrs->{level} => $self->resultsets );
	}
	else {
		%rs_levels =
		  $self->get_resultset_access_by_level;
	}
	$self->upload_data and $rs_levels{upload}=[keys %{$self->upload_data}];
	
	if ( $self->chr ) {
		if ( $self->round_start && $self->round_stop ) {
			$data = $db->get_marker_data_by_range_and_resultsets(
				{
					chr        => $self->chr,
					start      => $self->round_start,
					stop       => $self->round_stop,
					rset_access_levels => \%rs_levels
				}
			);
		}
		else {
			$data = $db->get_marker_data_by_chr_and_resultsets(
				{
					chr        => $self->chr,
					rset_access_levels => \%rs_levels
				}
			);
		}
	}
	else {
		die "Can't get marker data for all chromosomes in one go!";
	}
	
	return $data;
}

=head2 get_resultset_access_by_level

	  Usage      : my %resultsets_by_level = $browser->get_resultset_access_by_level;
    			   
	  Purpose    : Retrieve access levels for resultsets in $self->resultsets
	  Returns    : Hash of level as key containing an array of resultset identifiers
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_resultset_access_by_level {
	my ($self) = @_;
	my %by_level=();
	
	foreach my $rs_id(@{$self->resultsets}) {
		my $level = $self->resultset_levels->{$rs_id};
		push @{$by_level{$level}}, $rs_id;
	}
	
	return %by_level;
}

=head2 get_marker_binned_data

	  Usage      : $browser->ref('chr9');
	               my $data = $browser->get_marker_binned_data;
    			   
	  Purpose    : Wrapper method to get different aspects of binned marker data from the Browser database. The data returned depends on the attributes for chr, round_start and round_stop.
	               If resultsets and no chr are set, gets all binned data for the resultsets.
	               If chr is set gets just binned data for that chromosome.
	               If a rounded start and stop range are also set, gets just data for that range.
	  Returns    : Hashref with chr as key and lists of data as value.
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_marker_binned_data {
	my ( $self, $attrs ) = @_;

	my $db = $self->get_db;
	my $data;
	
	#obtain resultset access levels and take into account uploads
	my %rs_levels =
	  $self->get_resultset_access_by_level;
	$self->upload_data and $rs_levels{upload}=[keys %{$self->upload_data}];
	
	my $resultset_idents = $self->resultsets;
	if ( !$resultset_idents || scalar( @{$resultset_idents} ) == 0 ) {
		return {} if !$self->upload_data;
	}
	$self->chr eq 'ALL' and $self->chr(undef);
	
	#get only single chr data if only one chr present (ie. if chr info length is less than 3)
	if ( $self->chr && length( $self->chr ) > 0 ) {
		if ( $self->round_start && $self->round_stop ) {
			$data = $db->get_marker_binned_by_resultsets_range_and_threshold(
				{
					bin_size   => $attrs->{bin_size},
					chr        => $self->chr,
					start      => $self->round_start,
					stop       => $self->round_stop,
					threshold  => $self->threshold,
					rset_access_levels => \%rs_levels
				}
			);
		}
		else {
			$data = $db->get_marker_binned_by_resultsets_chr_and_threshold(
				{
					bin_size   => $attrs->{bin_size},
					chr        => $self->chr,
					threshold => $self->threshold || 'ZERO',
					rset_access_levels => \%rs_levels

				}
			);
			return $data;
		}
	}
	else {
		$data = $db->get_marker_binned_by_resultsets_and_threshold(
			{
				bin_size   => $attrs->{bin_size},
				threshold => $self->threshold || 'ZERO',
				rset_access_levels => \%rs_levels
			}
		);
		return $data;
	}

	return split_data_by_field( $data, "Reference", 1 );
}

=head2 is_no_features

	  Usage      : my $no_features = $self->is_no_features;
	  Purpose    : Determines if no tracks or resultset identifiers have been added. Adds features to feature list to show this.
	  Returns    : 1 if no features
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub is_no_features {
	my ($self) = @_;
	my $features = $self->features;
	if ( $self->resultsets eq 'NONE' ) {
		$self->add_heading(
"No Analysis Results Sets were specified. Please return to the linking page and try again."
		);
		$self->add_heading(
" If this does not work please report the error to the HGVbaseG2P team."
		);

		return 1;
	}

	if (    !$self->maxpval_low
		and !$self->maxpval_high
		and !$self->sigmarkers
		and !$self->sigmarkers_count )
	{
		$self->add_heading("No tracks selected in the configuration settings");
		return 1;
	}
}

=head2 format_chr

	  Usage      : $self->format_chr("ChrX");
	  Purpose    : Sets $self->ref with unformatted chr (eg. ChrX) and $self->chr with removed chr (eg. X)
	  Returns    : None
	  Arguments  : Full chromosome name (eg. ChrY)
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub format_chr {
	my ( $self, $ref ) = @_;
	$self->ref($ref);
	$ref =~ /^Chr(.+)/;
	my $chr = $1;
	$self->chr($chr);
}

=head2 round_positions

	  Usage      : my ($start, $stop) = $self->round_positions
	  Purpose    : Rounds down the start and up the stop position to the nearest multiple of $self->major_bin ands sets accessors
	  Returns    : Rounded start and stop
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub round_positions {
	my ($self) = @_;
	!$self->setting("lowres_binsize")
	  and $self->log->logdie(
		"You must set the 'lowres_binsize' setting before using this method" );
	!$self->seg_start
	  and $self->log->logdie(
		"You must set the 'seg_start' accessor before using this method" );
	!$self->seg_stop
	  and $self->log->logdie(
		"You must set the 'seg_stop' accessor before using this method" );

	my $bin_start =
	  nlowmult( $self->setting("lowres_binsize"), $self->seg_start );
	if ( $bin_start == 0 ) {
		$bin_start = 1;
	}
	my $bin_stop = nhimult( $self->setting("lowres_binsize"), $self->seg_stop );
	$self->round_start($bin_start);
	$self->round_stop($bin_stop);
	return ( $bin_start, $bin_stop );
}

=head2 tracks

	  Usage      : $browser->tracks('track1,track2');
	  Purpose    : Populates the _tracks accessor by creating a hash (with all values = 1) from a comma-separated string. Generally populated with CGI parameters
	  Returns    : Nothing
	  Arguments  : $value - contains a comma-separated string
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub tracks {
	my ( $self, $value ) = @_;
	if ($value) {
		my %track_hash;

		foreach ( split( ',', $value ) ) {
			$track_hash{$_} = 1;
		}
		$self->_tracks( \%track_hash );
	}
	return $self->_tracks;
}

=head2 settings

	  Usage      : $browser->settings('setting1|test1,setting2|test2);
	  Purpose    : Populates the _settings accessor by creating a hash from a comma-separated and pipe-separated string. Generally populated with CGI parameters
	  Returns    : Nothing
	  Arguments  : $value - contains a comma/pipe-separated string
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub settings {
	my ( $self, $value ) = @_;
	if ($value) {
		my %settings_hash;
		foreach ( split( ',', $value ) ) {
			my @temp = split( /\|/, $_ );
			$settings_hash{ $temp[0] } = $temp[1];
		}
		$self->_settings( \%settings_hash );
	}
	return $self->_settings;
}

=head2 setting

	  Usage      : my $value = $browser->setting('setting1');
	  Purpose    : Get the value for a setting in the browser object
	  Returns    : The value corresponding to the provided key from the _setting accessor
	  Arguments  : $key - key to look for in settings
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub setting {
	my ( $self, $key, $value ) = @_;
	$value and $self->_settings->{$key} = $value;
	return $self->_settings->{$key};
}

=head2 track

	  Usage      : if ($browser->track('track1')) { #do something if present; } else { #do something else if not present;}
	  Purpose    : Determine if a track has been added in the browser object
	  Returns    : 1 if present, undef if not
	  Arguments  : $key - key to look for in tracks
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub track {
	my ( $self, $key ) = @_;
	my $track;
	eval { $track = $self->_tracks->{$key} };
	$@ and return undef;
	return $track;
}

=head2 search_for_markers

	  Usage      : my $features = $browser->generate_region_features(\%attrs);
	  Purpose    : Core method for searching for significant markers by ID in the current resultsets - called by MarkerFinder plugin
	  Returns    : \@features - list of matching features
	  			   $ref - chromosome
	  			   $start - start position
	  			   $stop - stop position
	  Arguments  : Hash ref containing:
					searchterms - ID to search for (can be either HGVM id or rsid)
	  Throws     : 
	  Status     : Public
	  Comments   : Need to set resultset identifiers and threshold in the Browser object before calling this method.
	
=cut

sub search_for_markers {
	my ( $self, $attrs ) = @_;
	$self->summaries( $self->get_resultset_summaries );
	my %messages = $self->determine_resultset_access($attrs->{sessionid});
	
	my ( $marker, $coords ) =
	  $self->DataSources->{Marker}->get_marker_and_refcoords_by_identifier(
		$attrs->{searchterms} );
	(!$marker || !$coords) and return undef;
	my %rs_levels =
		  $self->get_resultset_access_by_level;
	
	my $markers = $self->get_db->get_markers_by_identifier_on_chr_above_threshold(
		{
			identifier => $marker->identifier,
			rset_access_levels => \%rs_levels,
			threshold  => $self->threshold,
			chr        => $coords->chr
		}
	);

	my @features = ();
	my $start;
	my $stop;
	my $ref;
	if ( scalar( @{$markers} ) > 0 ) {
		my $marker = $markers->[0];
		$start = $marker->{Start} - 1000000;
		$stop  = $marker->{Stop} + 1000000;
		$ref   = "Chr" . $marker->{Chr};

		my $f = Bio::Graphics::Feature->new(
			-ref   => $ref,
			-type  => 'Significant Markers',
			-name  => $marker->{Marker_Accession},
			-start => $marker->{Start},
			-score => $marker->{NegLogPValue},
			-end   => $marker->{Stop},
		);
		push @features, $f;
	}

	return ( \@features, $ref, $start, $stop );
}

=head2 get_markers_in_resultsets

	  Usage      : my $sigs = $browser->get_markers_in_resultsets(1);
	  Purpose    : Produce significance data for each marker
	  Returns    : A hashref with key of marker identifier
	  Arguments  : 0/1 to specify whether to populate the significances or not
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub get_markers_in_resultsets {
	my ($self, $populate_significances) = @_;
	my $marker_significance_of = $self->get_db->get_markers_in_resultsets(
		{
			chr        => $self->chr,
			start      => $self->seg_start,
			stop       => $self->seg_stop,
			threshold  => $self->threshold,
			resultsets => $self->resultsets
		}
	);
	$self->log->info("marker_significance_of:".Dumper($marker_significance_of));
	
	return $marker_significance_of unless $populate_significances;
	
	my $running_count_of_significances = 0;
	
	foreach my $marker_ident ( keys %{$marker_significance_of} ) {
		my $marker = $marker_significance_of->{$marker_ident};

		#go through each significance
		foreach my $significance_id ( keys %{ $marker->{significances} } ) {
			next if !$significance_id;
			my $sig = $self->DataSources->{Study}->get_limited_significance($significance_id);
			
			next if !$sig;
			$self->log->info("sig:".Dumper({$sig->get_columns}));
			$marker->{resultsets}->{ $sig->resultsetid->identifier } =
			  {$sig->get_columns()};
		}
		$running_count_of_significances+=scalar(keys %{ $marker->{significances} });
	
	#get array of resultsetIDs, compare against the 'base_resultsets' and remove this element if none
		my @marker_resultsets = keys %{ $marker->{resultsets} };
		my $lc =
		  List::Compare->new( \@marker_resultsets, $self->resultsets );
		if ( $lc->get_intersection == 0 ) {
			delete $marker_significance_of->{$marker_ident};
		}
	}
	return $marker_significance_of;
}

=head2 calculate_binned_data

	  Usage      : my $binned_data = $browser->calculate_binned_data({
	  				data => \@data,
	  				bin_size => 3000000
	  });    			   
	  Purpose    : Calculates binned data using the data and bin_size provided
	  Returns    : An arrayref of binned data 
	  Arguments  : In hash ref
	  				 binsize - size of bins to add in bp
	  				 data - arrayref of binned data (see HGVbaseG2P::Browser::Binning)
	  Throws     : 
	  Status     : Public
	  Comments   : Data format is an array of hashes (and the Start and NegLogPValue values act as position and value respectively)
	
=cut

sub calculate_binned_data {
	my ( $self, $attrs ) = @_;

	my $data = $attrs->{data};

#bin marker data but provide content rather than count (ie. retain elements created above)
	my $analyse = GwasCentral::Browser::Binning->new( { data => $data } );

	my $binned = $analyse->bin(
		{
			position_field => 'Start',
			bin_size       => $attrs->{bin_size},
			total_length   => chr_length( $self->chr ),
			value_field    => 'NegLogPValue',
			bin_content    => 'array',
			calc_ranges    => 1,
			start_position => $self->round_start,
			stop_position  => $self->round_stop,
		}
	);

	return $binned;
}

=head2 calculate_max_and_sig_counts

	  Usage      : my ($data, $sig_markers, $multi_sig_markers, $max_pval, $max_counts ) = $self->calculate_max_and_sig_counts;

	  Purpose    : Core logic to calculate counts, maximums and significant markers (in single and multiple studies) for high res view
	  Returns    : 1) Processed data
	  			   2) A hashref of single significant markers with a key of resultset id (used by add_sig_markers)
	  			   3) A hashref of an array of significant markers in different resultsets with a key of marker ID (used by add_combined_sig_markers)
	  			   4) Max pvalue found across all data (used by add_pvalue_traces)
	  			   5) Max counts found across all data (used by add_sig_counts)
	  Arguments  : Hashref of:
	                data - arrayref of binned data (as hash including actual markers in 'bin_content' key)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub calculate_max_and_sig_counts {
	my ( $self, $attrs ) = @_;

#get marker data for all resultsets as hash with key of Start - resultsets are present in an arrayref
	my $data       = $attrs->{'data'};
	my @resultsets = @{ $self->resultsets_and_uploads };
	my @all_sums   = ();
	my @tot_max    = ();
	my $threshold  = $self->threshold eq 'ZERO' ? '0' : $self->threshold;

	my %sig_markers       = ();
	my %multi_sig_markers = ();
	foreach (@resultsets) { $sig_markers{$_} = [] }

	#loop through each binned item
	foreach my $item ( @{$data} ) {
		my %counts;
		my %allcounts;
		my %values;
		my %bin_max;

		#reset count/allcount values for resultsets for each bin
		for my $rsid (@resultsets) {

			#set counts to 0
			$counts{$rsid}    = 0;
			$allcounts{$rsid} = 0;
			$bin_max{$rsid}   = 0;
			$values{$rsid}    = [];
		}

#loop through each marker in binned items and count number of markers for each in a bin
		foreach my $marker ( @{ $item->{bin_content} } ) {
			my $rsid     = $marker->{'Resultset_Identifier'};
			my $pvalue   = $marker->{'NegLogPValue'};
			my $markerid = $marker->{'Marker_Identifier'};

			#add to count only if significance above threshold
			if ($marker) {
				if ( $pvalue >= $threshold ) {
					push @{ $sig_markers{$rsid} },           $marker;
					push @{ $multi_sig_markers{$markerid} }, $marker;
					$counts{$rsid}++;
				}
				$allcounts{$rsid}++;  #always add to all counts if marker in bin
				push @{ $values{$rsid} }, $pvalue ? $pvalue : '0.00';
			}
		}

#get cumulative sum of counts for bin and add to all_sums (do this before replacing 0s with -1s)
#used by stack plots (top value used as max score value)
		my $bin_sum = sum( values %counts );
		push @all_sums, $bin_sum;

		#add bin counts to item and find maximum for each resultset in bin
		#if bin has no markers at all then set score to -1
		my @count_sorted = ();
		my @max_sorted   = ();
		foreach my $rsid (@resultsets) {
			$counts{$rsid} = -1 if $allcounts{$rsid} == 0;
			push @count_sorted, $counts{$rsid};
			$bin_max{$rsid} = max( @{ $values{$rsid} } ) || 0;
			if ( $counts{$rsid} == -1 ) {
				push @max_sorted, -1;
			}
			else {
				push @max_sorted, $bin_max{$rsid};

			}
		}
		$item->{bin_counts} = join( "|", @count_sorted );
		$item->{bin_max}    = join( "|", @max_sorted );

#find bin maximum for all resultsets and add to array of all max values (used by P value trace)
		my $max_of_bin = max( values %bin_max );
		push @tot_max, $max_of_bin;
	}

	my $max_counts = max(@all_sums);
	my $max_pval   = max(@tot_max);

	return ( $data, \%sig_markers, \%multi_sig_markers, $max_pval,
		$max_counts );
}

=head2 calculate_max_pvalues

	  Usage      : my ($binned, $max_pval) = $self->calculate_max_pvalues({
					 bin_size => $attrs->{bin_size}
				   });
	  Purpose    : Calculate max pvalues for specified bin size (therefore used by low and high resolution views)
	  Returns    : Arrayref of hash bin data (see HGVbaseG2P::Browser::Binning) and the max value found in the analysis
	  Arguments  : Hashref of:
	                bin_size - size of bin to use
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub calculate_max_pvalues {
	my ( $self, $attrs ) = @_;
	my $binned     = $self->all_binned;
	my @resultsets = split( ',', $self->resultsets_and_uploads );
	my @tot_max    = ();

	#loop through each binned item
	foreach my $item ( @{$binned} ) {
		my @max = [];

		#loop through contents each binned item
		foreach my $content ( @{ $item->{bin_content} } ) {

#loop through each resultset of binned items and get max P value for each in a bin

			for (
				my $rscounter = 0 ;
				$rscounter < scalar(@resultsets) ;
				$rscounter++
			  )
			{

#print "content:".Dumper($content->[2])." and ".($content->[1]->[$rscounter] or 'none')."\n";
				!$max[$rscounter] and $max[$rscounter] = [];
				if ( $content->[1]->[$rscounter] ) {
					push @{ $max[$rscounter] },
					  $content->[1]->[$rscounter]->{'NegLogPValue'};
				}
			}
		}

		#add calculated counts to bin data
		foreach my $max (@max) {
			$max = max( @{$max} );
		}
		$item->{bin_max} = \@max;
		my $all_max = max(@max);
		$all_max and push @tot_max, $all_max;
	}

	my @sorted = reverse sort { $a <=> $b } @tot_max;
	my $max_pval = $sorted[0];

	return ( $binned, $max_pval );
}

=head2 genomic_region

	  Usage      : my $region = $self->genomic_region('chr12:1..234235234');
	  Purpose    : Populate Browser with chr, start and stop from genomic coordinates.
	  Returns    : Genomic coordinates in form "ChrN:1..23423432"
	  Arguments  : Genomic coordinates in UCSC or ENSEMBL form
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub genomic_region {

	my ( $self, $value ) = @_;
	if ($value) {
		my ( $chr, $start, $stop ) = $self->extract_genomic_region($value);
		$self->chr($chr);
		$self->seg_start($start);
		$self->seg_stop($stop);
	}
	if ( $self->seg_start && $self->seg_stop ) {
		return
		    "Chr"
		  . $self->chr . ":"
		  . $self->seg_start . ".."
		  . $self->seg_stop;
	}
	else {
		if ( $self->chr ) {
			return "Chr" . $self->chr;
		}
		else {
			return undef;
		}
	}
}

=head2 extract_genomic_region

	  Usage      : my ($chr,$start,$stop) = $self->extract_genomic_region('chr12:1..234235234');
	  Purpose    : Utility method to extract chr, start and stop
	  Returns    : List of chr, start and stop or just chr
	  Arguments  : Genomic coordinates in UCSC or ENSEMBL form
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub extract_genomic_region {

	my ( $self, $region ) = @_;
	my ( $chr, $start, $stop, $type );
	if ( $region =~ /[Chr|chr]*(\w+)/ ) {
		my $chr = $1;
		if ( $region =~ /[Chr|chr]*(\w+)\D(\d+)..(\d+)/ ) {
			( $chr, $start, $stop ) = ( $1, $2, $3 );
			return ( $chr, $start, $stop );
		}
		else {
			return ($chr);
		}
	}

}

=head2 _get_lines_by_resultsets

	  Usage      : $browser->_get_lines_by_resultsets({ 
	  				offset=>1, 
	  				field_order=>[qw(Chr Start Stop NegLogPValue)], 
	  				default_strand => '-',
	  				chr_prefix => 'chr',
	  			});
	  Purpose    : To produce data for upload to UCSC and ENSEMBL
	  Returns    : Hash with key or resultset identifier and value of list of field data (corresponding to the field_order)
	  Arguments  : Hashref of attributes:
	                offset = number to add to stop position
	                field_order = order of table fields for output
	                default_strand = default DNA strand to use
	                chr_prefix = added to front of chr in Chr field
	  Throws     : 
	  Status     : Private
	  Comments   : 
=cut

sub _get_lines_by_resultsets {
	my ($self, $attrs) = @_;
	my $offset = $attrs->{offset} || 0;
	my @field_order = @{$attrs->{field_order}};
	my $chr_prefix = $attrs->{chr_prefix};
	my $default_strand = $attrs->{default_strand} || '+';
	my %data_for = ();
	$self->summaries( $self->get_resultset_summaries );
	$self->determine_resultset_access;
	my ( $bin_start, $bin_stop ) = $self->round_positions;
	
	my $marker_data = $self->get_marker_data;
	
	
	foreach my $item(@{$marker_data}) {
		my $counter = 0;
		my @bed_line = ();
		next if $item->{'NegLogPValue'}<$self->threshold;
		foreach my $field(@field_order)  {
			my $value = $item->{$field};
			
			$field eq 'Chr' and $value=$chr_prefix.$value;
			if ($field eq 'Strand' && !$value) { $value=$default_strand; }
			if ($field eq 'Stop') { $value++ ;}
			$bed_line[$counter]=$value;
			$counter++;
		}
		push @{$data_for{$item->{'Resultset_Identifier'}}}, \@bed_line;
	}
	return %data_for;
}

=head2 generate_bed_file

	  Usage      : $browser->generate_bed_file();
	  Purpose    : Use state of current browser object to generate BED file of marker data
	  Returns    : Temporary filename of the BED file
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub generate_bed_file {
	my ($self) = @_;
	die "BED file can only be generated for high-resolution view" if !$self->is_high_resolution;
	
	my %colors_for = $self->_get_colors_by_resultsets;
	
	#retrieve data for all resultsets
	my %data_for = $self->_get_lines_by_resultsets({ 
		offset => 1,
		field_order=>[qw(Chr Start Stop Marker_Accession NegLogPValue Strand)],
		chr_prefix =>'chr',
	}); 
	
	#create temporary file for BED output
	die "Unable to find directory ".($self->config->{tempdir} || "") if !$self->config->{tempdir} or !-d $self->config->{tempdir};
	chdir $self->config->{tempdir};
	my ($fh,$filename) = tempfile('hgvtempXXXXXXX');
	die "Unable to create temp file" if !$filename;
	
	#header line for BED file
	$fh->print("browser position chr".$self->chr.":".$self->seg_start."-".$self->seg_stop."\n");
	
	#output data for each resultset/upload including links back to HGVbaseG2P
	my @track_names = ();
	foreach my $rs_ident(@{$self->resultsets_and_uploads}) {
		my ($study_ident);
		if ($rs_ident =~ /^U/) {
			$study_ident = 'UPLOAD';
		}
		else {
			$study_ident = $self->summaries->{$rs_ident}->experimentid->studyid->identifier;
		}
		push @track_names, $study_ident."_".$rs_ident;
	}
	$fh->print("browser pack ".join(' ',@track_names)."\n");

	foreach my $rs_ident(@{$self->resultsets_and_uploads}) {
		next if !$data_for{$rs_ident};
		my @bed_lines = @{$data_for{$rs_ident}};
		next if scalar(@bed_lines)==0;
		my ($study_ident, $study_name, $rs_name, $htmlurl, $url);
		if ($rs_ident =~ /^U/) {
			$study_ident = 'UPLOAD';
			my $udata = $self->upload_data->{$rs_ident};
			$study_name = $udata->{'study_name'};
			$rs_name = $udata->{'resultset_name'};
			$url = '#';
			$htmlurl = '#';
		}
		else {
			my $rs = $self->summaries->{$rs_ident};
			my $study = $rs->experimentid->studyid;
			$study_ident = $study->identifier;
			$study_name = $study->name;
			$rs_name = $rs->name;
			$url = $self->config->{WebSite}->{hgvbaseg2p_baseurl}."/marker/dbSNP:\$\$/results?rfilter=" . $rs_ident;
			$htmlurl = $self->config->{WebSite}->{hgvbaseg2p_baseurl}."/study/".$study_ident;
		}
		$fh->print('track name="'.$study_ident."_".$rs_ident.'" description="'.$study_name .' (Result Set '.$rs_ident.')" offset=-1 visibility=1'); 
		$fh->print(" color=\"".$colors_for{$rs_ident}."\" htmlUrl=\"".$htmlurl."\" ");
		$fh->print("url=\"".$url."\"\n");
		foreach my $line(@bed_lines) {
			$fh->print(join(' ',@{$line})."\n");
		}
	}
	
	$fh->close;
	#change to 0704
	chmod 0777, $self->config->{tempdir}."/".$filename;
	return $filename;
}

=head2 output_to

	  Usage      : $browser->output_to("ucsc");
	  Purpose    : Output text file (e.g. BED, GFF) for export/upload to another genome browser and return the URL to forward to
	  Returns    : URL to redirect to (e.g. genome browser or specific temporary file)
	  Arguments  : String of 'ucsc' or 'ensembl' to return a URL for a genome browser or 'bed' for a temp file
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub output_to {
	my ($self, $output) = @_;
	my $redirect_url;
	return if !$output;
	my $tempfilename = $self->generate_bed_file;
	if ($output eq 'ucsc') {
		my $url = "http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&db=hg18&position=chr".$self->chr."&hgt.customText=";
		$redirect_url = $url.$self->config->{WebSite}->{temp_url}."/".$tempfilename;
	}
	elsif ($output eq 'ensembl') {
		my $url = "http://www.ensembl.org/Homo_sapiens/Location/View?r=".$self->chr.":".$self->seg_start."-".$self->seg_stop.";contigviewbottom=url:";
		$redirect_url = $url.$self->config->{WebSite}->{temp_url}."/".$tempfilename;
	}
	elsif ($output eq 'bed') {
		$redirect_url = $self->config->{WebSite}->{temp_url}."/".$tempfilename;
	}
	else {
		die HGVbaseG2P::Exception->new({message=>"Output format '$output' not recognised"});
	}
	return $redirect_url;
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: Core.pm 1540 2010-09-22 08:35:36Z rcf8 $ 

=cut

