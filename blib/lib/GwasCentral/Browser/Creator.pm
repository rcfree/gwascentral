# $Id: Creator.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

HGVbaseG2P::Browser::Creator - Create Browser database and generate binned data/result set marker lists from Study/Marker database


=head1 SYNOPSIS

TODO
	  
=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

TODO

=cut

package GwasCentral::Browser::Creator;

use Moose;
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);
use Data::Dumper qw(Dumper);
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use GwasCentral::DataSource::Browser;
use GwasCentral::Browser::Util
  qw(neglog chr_list chr_length split_data_by_field);
use GwasCentral::Browser::Core;
use GwasCentral::AccessControl::None;
use GwasCentral::DataSource::Util qw(DataSources_from_list);
use GwasCentral::Base qw(split_number_list new_pager);
has 'no_init' => ( 'is' => 'rw' );

has 'Browser'  => ( 'is' => 'rw' );
has 'studies'  => ( 'is' => 'rw' );
has 'genome'   => ( 'is' => 'rw' );
has 'region'   => ( 'is' => 'rw' );
has 'list'     => ( 'is' => 'rw' );
has 'markers'  => ( 'is' => 'rw' );
has 'populate' => ( 'is' => 'rw' );
has 'all'      => ( 'is' => 'rw' );
has 'init'     => ( 'is' => 'rw' );
has 'noaction' => ( 'is' => 'rw' );

has 'resultsets' => ( 'is' => 'rw' );

has 'prepped_sth'    => ( 'is' => 'rw' );
has 'study_list'     => ( 'is' => 'rw', 'default' => sub { [] } );
has 'resultset_list' => ( 'is' => 'rw', 'default' => sub { [] } );

has 'sth_genome'          => ( 'is' => 'rw' );
has 'sth_region'          => ( 'is' => 'rw' );
has 'sth_genome_ltd'      => ( 'is' => 'rw' );
has 'sth_region_ltd'      => ( 'is' => 'rw' );
has 'sth_markers'         => ( 'is' => 'rw', 'default' => sub { {} } );
has 'sth_ltd_markers'     => ( 'is' => 'rw', 'default' => sub { {} } );
has 'changed_markers'     => ( 'is' => 'rw', 'default' => sub { {} } );
has 'ltd_changed_markers' => ( 'is' => 'rw', 'default' => sub { {} } );

my $BATCH_SIZE = 10000;

sub BUILD {
	my ($self) = @_;

	$self->_init_db_and_browser;
	$self->studies    and $self->_get_studies;
	$self->resultsets and $self->_get_resultsets;

	if ( $self->init ) {
		$self->_init_tables;
	}

	return if $self->genome || $self->region || $self->list || $self->markers;
	$self->genome(1);
	$self->region(1);
	$self->list(1);
	$self->markers(1);
	if ( $self->noaction ) {
		$self->log->info(
			"-n flag set so no data will be written to databases etc");
	}
}

sub create {
	my ($self) = @_;

	my $no_access_control = $self->config->{no_access_control};

	if ( $self->populate ) {
		$self->populate_marker_significances();

		#!$no_access_control and $self->populate_marker_significances(1);
	}

	if ( $self->region ) {
		$self->prepare_calc_region;
	}

	if ( $self->genome ) {
		$self->prepare_calc_genome;
	}

	if ( $self->resultsets && !$self->studies ) {
		foreach my $rset ( @{ $self->resultset_list } ) {
			$self->log->info( "Processing resultset " . $rset->identifier );

			$self->create_resultset_data( $rset->experimentid->studyid,
				$rset->experimentid, $rset );

		}
	}

	if ( $self->studies ) {
		foreach my $study ( @{ $self->study_list } ) {
			my $rscount = 0;
			foreach my $exp ( $study->experiments->all ) {
				$rscount = $exp->resultsets->count;
			}
			$self->log->info( "Processing study "
				  . $study->identifier . " ("
				  . $rscount
				  . " resultsets)" );

			foreach my $exp ( $study->experiments->all ) {
				foreach my $rs ( $exp->resultsets->all ) {
					$self->create_resultset_data($rs);
				}
			}
		}
	}

	if ( $self->list && ( $self->studies || $self->resultsets || $self->all ) )
	{
		$self->log->info("Calculating marker significance lists");
		$self->generate_marker_lists(0);

#!$no_access_control and $self->delete_marker_lists( 1, $self->ltd_changed_markers );
		!$no_access_control and $self->generate_marker_lists(1);
	}

}

sub create_resultset_data {
	my ( $self, $rs ) = @_;
	my $db                = $self->DS('Browser');
	my $no_access_control = $self->config->{no_access_control};
	if ( $self->markers ) {
		$self->copy_marker_data($rs);
		!$no_access_control and $self->copy_marker_data( $rs, 1 );
	}

	if ( $self->genome ) {
		$self->log->info( "Removing any previous genome data for resultset "
			  . $rs->identifier
			  . " and binsize 3" );
		$db->delete_marker_binned_chr_by_resultset(
			{ resultset => $rs->identifier, binsize => 3 } );
		!$no_access_control
		  and $self->browser->get_db->delete_marker_binned_ltd_chr_by_resultset(
			{ resultset => $rs->identifier, binsize => 3 } );
		$self->calc_genome_binned($rs);
		!$no_access_control and $self->calc_genome_binned( $rs, 1 );
	}
	if ( $self->region ) {
		$self->log->info( "Removing any previous region data for resultset "
			  . $rs->identifier
			  . " and binsize 1" );
		$db->delete_marker_binned_by_resultset(
			{ resultset => $rs->identifier, binsize => 1 } );
		!$no_access_control
		  and $db->delete_marker_binned_ltd_by_resultset(
			{ resultset => $rs->identifier, binsize => 1 } );
		$self->calc_region_binned($rs);
		!$no_access_control and $self->calc_region_binned( $rs, 1 );
	}
}

sub prepare_calc_genome {
	my ($self) = @_;
	my ( @fields, @pars ) = ();
	my $db = $self->DS('Browser');
	for ( my $t = 0 ; $t <= $self->config->{max_threshold} ; $t++ ) {
		push @fields, "ScoreList" . ( $t == 0 ? 'ZERO' : $t );
		push @pars, "?";
	}
	$self->sth_genome(
		$db->dbh->prepare(
"INSERT INTO marker_binned_chr_3mb (Reference, Start, Stop, Resultset_Identifier, "
			  . join( ",", @fields ) . ")
				VALUES (?,?,?,?," . join( ",", @pars ) . ")"
		)
	);
	$self->sth_genome_ltd(
		$db->dbh->prepare(
"INSERT INTO marker_binned_ltd_chr_3mb (Reference, Start, Stop, Resultset_Identifier, "
			  . join( ",", @fields ) . ") 
				VALUES (?,?,?,?," . join( ",", @pars ) . ")"
		)
	);
}

sub prepare_calc_region {
	my ($self) = @_;
	my ( @fields, @pars ) = ();
	for ( my $t = 0 ; $t <= $self->config->{max_threshold} ; $t++ ) {
		push @fields, "Score" . ( $t == 0 ? 'ZERO' : $t );
		push @pars, "?";
	}
	my $db = $self->DS('Browser');
	$self->sth_region(
		$db->dbh->prepare(
"INSERT INTO marker_binned_1mb (Reference, Start, Stop, Resultset_Identifier, "
			  . join( ",", @fields )
			  . ") VALUES (?,?,?,?,"
			  . join( ",", @pars ) . ")"
		)
	);
	$self->sth_region_ltd(
		$db->dbh->prepare(
"INSERT INTO marker_binned_ltd_1mb (Reference, Start, Stop, Resultset_Identifier, "
			  . join( ",", @fields )
			  . ") VALUES (?,?,?,?,"
			  . join( ",", @pars ) . ")"
		)
	);
}

sub _init_db_and_browser {
	my ($self) = @_;
	$self->populate_DS_from_list(qw(Study Marker Ontology Browser Feature));

	my $browser =
	  GwasCentral::Browser::Core->new( { conf_file => $self->config } );
	$browser->DataSources( $self->DataSources );
	my $acontrol =
	  GwasCentral::AccessControl::None->new( { conf_file => $self->config } );

	#	$acontrol->study_db($study_db);
	#$browser->access_control($acontrol);

	$self->Browser($browser);
}

=head2 get_script

  Usage      : $db->get_script($script_file)
  Purpose    : Open SQL script file and generate array of individual SQL statements.
               Process SQL to replace robmart and HGVbaseG2P with output and source dbs
               respectively.
  Returns    : Array of SQL statements
  Arguments  : Script file name
               Output db name
               Source db name
  Throws     : HGVbaseG2Pmart::Exception::Config if no script file found
  Status     : 
  Comments   : 

=cut

sub _get_sql_from_script {
	my ( $self, $script_file ) = @_;

	#get script and separate into individual SQL statements
	open SCRIPT, $script_file
	  or $self->throw("Unable to open script file ($script_file)");
	my $temp = "";
	while (<SCRIPT>) { $temp .= $_; }

	my @sql_statements = split( ";\n", $temp );
	my @rsets = ();

	return @sql_statements;
}

sub _init_tables {
	my ($self) = @_;
	my $db = $self->DS('Browser');
	$self->log->info("Initialising Browser database and tables");
	my @sql = $self->_get_sql_from_script('conf/schema/GC_browser_schema.sql');
	foreach (@sql) {
		$db->dbh->do($_);
	}

	$db->dbh->do("set foreign_key_checks=0");
}

sub _get_studies {
	my ($self) = @_;
	my @study_idents = $self->_get_identifiers( $self->studies, "HGVST" );

	my @temp_studies =
	  $self->DS('Study')->get_study_by_identifier( \@study_idents );
	my @studies = grep { $_->ishidden eq 'no' and $_ } @temp_studies;
	$self->log->info(
		"Using " . scalar(@studies) . " study/ies (" . $self->studies . ")" );

	$self->study_list( \@studies );
}

sub _get_identifiers {
	my ( $self, $numbers, $prefix ) = @_;
	my @blocks = split( ",", $numbers );
	my @ids = ();

	foreach my $block (@blocks) {
		$self->log->info("block:$block");
		my @range = split( "-", $block );
		if ( @range == 0 || @range > 2 ) {
			$self->log->error(
				"Too many identifiers in range: '$block' - ignored");
			next;
		}
		if ( $range[1] && $range[0] > $range[1] ) {
			$self->log->error(
				"To value greater than from value in range: '$block' - ignored"
			);
			next;
		}
		@ids =
		  @range == 2 ? ( @ids, $range[0] .. $range[1] ) : ( @ids, $block );
	}
	return map { $prefix . $_ } @ids;
}

sub _get_resultsets {
	my ($self) = @_;

	my @rset_idents = $self->_get_identifiers( $self->resultsets, "HGVRS" );
	$self->log->info( "Using "
		  . scalar(@rset_idents)
		  . " result sets ("
		  . $self->resultsets
		  . ")" );
	my @resultsets =
	  map { $self->DS('Study')->get_resultset_by_identifier($_) } @rset_idents;
	$self->resultset_list( \@resultsets );
}

sub get_sth_markers {
	my ( $self, $chr, $limited_only ) = @_;
	my $sth_markers;
	if ($limited_only) {
		my $sth = $self->sth_ltd_markers->{$chr}
		  || $self->get_insert_ltd_sig($chr);
		!$self->sth_ltd_markers->{$chr}
		  and $self->sth_ltd_markers->{$chr} = $sth;
		return $sth;
	}
	else {
		my $sth = $self->sth_markers->{$chr} || $self->get_insert_sig($chr);
		!$self->sth_markers->{$chr} and $self->sth_markers->{$chr} = $sth;
		return $sth;
	}
}

sub _copy_marker_data_for_resultset {
	my ( $self, $rset, $limited_only ) = @_;
	my @data;
	my $db = $self->DS('Browser');
	my $opts = $limited_only ? { 'islimited' => 'yes' } : undef;

	my $sigs =
	  $rset->significances->search( $opts, { join => 'usedmarkersetid' } );

	my $count = $sigs->count;

	$self->log->info( "Copy "
		  . ( $limited_only ? 'limited' : 'full' )
		  . " marker data from "
		  . $rset->identifier
		  . " ($count significances)\n" );
	my $line_count = 0;

	my $sql =
"select Identifier, Accession, Chr, Start, Stop, GeneSymbol from all_markers where Identifier = ?";
	my $marker_sth = $db->dbh->prepare($sql);
	while ( my $sig = $sigs->next ) {

		#		my ( $marker, $coord ) =
		#		  $self->marker_db->get_marker_and_refcoords_by_identifier(
		#			$sig->usedmarkerset->markeridentifier );
		if ( !$self->noaction ) {
			$marker_sth->execute( $sig->usedmarkerset->markeridentifier );
		}
		my $marker = $marker_sth->fetchrow_hashref;
		next if !$marker;
		next if $marker && $marker->{Chr} eq 'MT';
		next if $marker && !$marker->{Chr};
		my $mid = $marker->{Identifier};

		#$self->log->info("chr:".$coord->chr);
		my $sth = $self->get_sth_markers( $marker->{Chr}, $limited_only );
		my $record = {
			Study_Identifier     => $rset->experimentid->studyid->identifier,
			Resultset_Identifier => $rset->identifier,
			UnadjustedPValue     => $sig->unadjustedpvalue,
			NegLogPValue => sprintf( "%.3f", neglog( $sig->unadjustedpvalue ) ),
			Marker_Identifier => $mid,
			Marker_Accession  => $marker->{Accession},
			VariationType     => "SNP",
			Chr               => $marker->{Chr},
			Start             => $marker->{Start},
			Stop              => $marker->{Stop},
			Strand            => "",
			SignificanceID    => $sig->id,
			GeneSymbol        => $marker->{GeneSymbol}
		};
		if ( !$self->noaction ) {
			$sth->execute( values %{$record} );
		}
		$limited_only
		  ? $self->ltd_changed_markers->{ $marker->{Chr} }->{$mid} = 1
		  : $self->changed_markers->{ $marker->{Chr} }->{$mid} = 1;
		$line_count++;

		$line_count % $BATCH_SIZE == 0
		  and $self->log->info("$line_count marker data records copied\n");
	}
}

sub copy_marker_data {
	my ( $self, $rs, $limited_only ) = @_;
	my $db = $self->DS('Browser');
	foreach my $chr ( keys %{ chr_list() } ) {
		if ($limited_only) {
			$db->delete_marker_data_ltd_by_resultset_and_chr(
				{ resultset => $rs->identifier, chr => $chr } );
		}
		else {
			$db->delete_marker_data_by_resultset_and_chr(
				{ resultset => $rs->identifier, chr => $chr } );
		}

	}
	$self->_copy_marker_data_for_resultset( $rs, $limited_only );
}

sub execute_statements {
	my ( $self, $sql_statements ) = @_;
	my $dbh = $self->DS('Browser')->dbh;

	foreach my $sql_statement ( @{$sql_statements} ) {
		$self->log->info( "sql:" . $sql_statement );
		if ( !$self->noaction ) {
			$dbh->do($sql_statement);
		}
	}
}

sub populate_marker_significances {
	my ( $self, $only_limited ) = @_;
	my $mstablename =
	  $only_limited
	  ? "marker_significances_ltd_chr"
	  : "marker_significances_chr";

	my %sth = ();

	my $markerdb_name = $self->DS('Marker')->name;
	my $dbh           = $self->DS('Browser')->dbh;

	#$self->_generate_all_markers;

	#$self->_update_replaced_markers($mstablename);
	
	#$self->_insert_marker_chr_data( $mstablename );

	#$self->_determine_genes($marker_count);

#	my $sql = "create index Identifier on all_markers(Identifier)";
#	$dbh->do($sql);

	my $sql = "create index Accession on all_markers(Accession)";
	$dbh->do($sql);

	$sql = "drop table TEMP0";
	$dbh->do($sql);
	$sql = "drop table TEMP1";
	$dbh->do($sql);
	$sql = "drop table TEMP2";
	$dbh->do($sql);
}

sub _generate_all_markers {
	my ($self)         = @_;
	my $browser_db     = $self->DS('Browser');
	my $dbh            = $browser_db->dbh;
	my $browserdb_name = $browser_db->name;
	my $markerdb_name  = $self->DS('Marker')->name;
	my $assembly       = $self->config->{assembly_name};
	my @sql            = (
		"drop table if exists TEMP2",
		"drop table if exists TEMP0",
		"drop table if exists all_markers",
"create table $browserdb_name.TEMP0 as select a.MarkerID, a.Identifier, a.Accession, a.Status, a.VariationType, a.ValidationCode, a.Upstream30bp, a.AlleleSeqsShorthand as Alleles, a.Downstream30bp from $markerdb_name.Marker as a",
		"create index MarkerID on $browserdb_name.TEMP0(MarkerID)",
"create table $browserdb_name.all_markers as select a.*,b.Start,b.Stop,b.MarkerCoordID,b.Chr, b.AssemblyName from $browserdb_name.TEMP0 as a left join $markerdb_name.MarkerCoord as b on a.MarkerID=b.MarkerID and (AssemblyName='$assembly')",
"alter table all_markers add column CurrentIdentifier VARCHAR(15) NULL DEFAULT NULL",
		"drop table if exists TEMP1",
		"create table TEMP1 as select a.* from $markerdb_name.MarkerRevision a",
		"create index Ordered on TEMP1(ReplacedByMarkerID, TimeCreated);",
		"create index MarkerID on all_markers(MarkerID);",
		"create index Status on all_markers(Status);",
"create table TEMP2 as select CAST(SUBSTRING_INDEX(GROUP_CONCAT(ReplacedByMarkerID ORDER BY TimeCreated DESC),',',1) AS UNSIGNED) as ReplacedByMarkerID, MarkerID from TEMP1 where ReplacedByMarkerID is not null group by MarkerID",
		"create index MarkerID on TEMP2(MarkerID);",
		"create index ReplacedByMarkerID on TEMP2(ReplacedByMarkerID);",
	);

	if ( !$self->noaction ) {
		$self->execute_statements( \@sql );
	}
}

sub _determine_genes {
	my ( $self, $marker_count ) = @_;
	my $browser_db = $self->DS('Browser');
	my $dbh        = $browser_db->dbh;
	my $update_current_sql =
	  "update all_markers set GeneSymbol=? where Identifier=?";
	my $update_current_sth = $dbh->prepare($update_current_sql);
	my $pager = new_pager( $marker_count, $BATCH_SIZE );

	for ( my $page = 1 ; $page <= $pager->last_page ; $page++ ) {
		$pager->current_page($page);
		my @all_markers = $self->_get_page_from_sql(
"select Identifier, Accession, Chr, Start, Stop from all_markers where Status='active'",
			$pager
		);
		my @update_data = ();
		for my $marker (@all_markers) {
			next if !$marker->[3];
			my $opts = {
				-seq_id => "chr" . $marker->{Chr},
				-start  => $marker->{Start},
				-stop   => $marker->{Stop},
				-types  => ['mRNA'],
			};
			my $feature = $self->DS('Feature')->get_biggest_feature($opts);

			#$self->log->info("feature:".Dumper($feature));
			$feature
			  and push @update_data, [ $feature->display_name, $marker->[0] ];
		}

		foreach my $item (@update_data) {
			if ( !$self->noaction ) {
				$update_current_sth->execute( @{$item} );
			}
		}

		$self->log->info( "$page/"
			  . $pager->last_page
			  . " marker info pages ("
			  . scalar(@update_data)
			  . " rows) genes updated\n" );
	}
}

sub _insert_marker_chr_data {
	my ( $self, $mstablename ) = @_;
	my $dbh       = $self->DS('Browser')->dbh;
	my %sth       = ();
	my $count_sql = "select count(*) from all_markers where Chr=?";
	my $count_sth = $dbh->prepare($count_sql);
	  foreach my $chr ( keys %{ chr_list() } ){
	  	$self->log->info("clear table $mstablename$chr");
	  	$dbh->do("truncate table $mstablename$chr");
		$count_sth->execute($chr);
		  my $marker_count = $count_sth->fetchrow_arrayref->[0];
		  my $insert_sql =
"insert into $mstablename$chr (Marker_Identifier, Marker_Accession, Start, Stop) values(?,?,?,?)";
		  my $insert_sth = $dbh->prepare($insert_sql);

		  my $page_size = 10000;

		  my $pager = Data::Page->new;
		  $pager->total_entries($marker_count);
		  $pager->entries_per_page($BATCH_SIZE);
		  $self->log->info("chr marker count:$marker_count");
		  if ( !$self->noaction ) {
			for ( my $page = 1 ; $page <= $pager->last_page ; $page++ ) {
				$pager->current_page($page);
				my @data = $self->_get_page_from_sql("select Identifier, Accession, Chr, Start, Stop from all_markers where Chr=?",$pager,[$chr]);

				foreach my $marker(@data) {
					
					my $chr   = $marker->{Chr};
					my $start = $marker->{Start};
					my $stop  = $marker->{Stop};
					next if !$chr;
					next if $marker && $chr eq 'MT';
					next if $marker && !$chr;
					my $mid = $marker->{Identifier};
					next if !$mid;
					my @pars = ( $mid, $marker->{Accession}, $start, $stop );

					if ( !$self->noaction ) {
						$insert_sth->execute(@pars);
					}
				}
				$self->log->info( "$page/"
					  . $pager->last_page
					  . " marker info pages ($page_size rows) copied for chr $chr\n" );
			}
		}
	  };
}

sub _get_page_from_sql {
	my ( $self, $sql, $pager, $params ) = @_;
	return () if $pager->total_entries == 0;
	my $final_sql =
	    $sql
	  . " limit "
	  . ( $pager->first - 1 ) . ","
	  . $pager->entries_on_this_page;
	my $temp_sth = $self->DS('Browser')->dbh->prepare($final_sql);
	$self->log->info("final_sql:$final_sql");
	$temp_sth->execute(@{$params});
	my @results = ();
	while(my $row = $temp_sth->fetchrow_hashref) {
		push @results, $row;
	}
	
	return @results;
}

sub _update_replaced_markers {
	my ($self) = @_;
	my $dbh    = $self->DS('Browser')->dbh;
	my $sth    = $dbh->prepare("select count(*) from TEMP2");

	$sth->execute();

	my $row            = $sth->fetchrow_arrayref;
	my $inactive_count = $row->[0];
	$self->log->info("Updating $inactive_count inactive markers");
	my $pager = new_pager( $inactive_count, $BATCH_SIZE, 1 );
	$self->log->info( "pager:" . Dumper($pager) );
	my $get_marker_sql =
"select Identifier from all_markers where MarkerID = ?";

	my $get_marker_sth = $dbh->prepare($get_marker_sql);

	for ( my $page = 1 ; $page <= $pager->last_page ; $page++ ) {
		$pager->current_page($page);
		my @replaced_markers = $self->_get_page_from_sql(
			"select ReplacedByMarkerID, MarkerID from TEMP2", $pager );
		$self->log->info(
			"Get inactive markers page " . $page . "/" . $pager->last_page );

		my @updated_markers = ();
		foreach my $rep_marker (@replaced_markers) {

			$get_marker_sth->execute( $rep_marker->{ReplacedByMarkerID} );

			my $marker_row = $get_marker_sth->fetchrow_hashref;
			if ($marker_row) {
				if ( $marker_row->[0] ) {

#$self->log->info("added marker:".$marker_row->[0]." = ".$rep_marker->[1]);
#$self->log->info("marker:".$marker->[0]." has new accession:".$curr_accession);
					push @updated_markers,
					  [ $marker_row->{Identifier}, $rep_marker->{MarkerID} ];
				}
			}
		}

		my $update_current_sql =
		  "update all_markers set CurrentIdentifier=? where MarkerID=?";
		my $update_current_sth = $dbh->prepare($update_current_sql);

		$self->log->info(
			    scalar(@updated_markers) . "/"
			  . $pager->entries_on_this_page()
			  . " markers have new accession(s)" );
		if ( !$self->noaction ) {
			foreach my $updated_marker (@updated_markers) {

				$update_current_sth->execute( @{$updated_marker} );
			}
		}

	}
}

sub prepare_precalculate_bins {
	my ( $self, $rs, $bin_size ) = @_;

	#get data via browser
	my $long_bin_size = $bin_size * 1000000;

	$self->browser->_settings->{"lowres_binsize"} = $long_bin_size;
	$self->browser->resultsets( [ $rs->identifier ] );
}

sub precalculate_bins {
	my ( $self, $chr, $bin_size, $level ) = @_;
	$self->browser->chr($chr);
	$self->browser->round_start(undef);
	$self->browser->round_stop(undef);

	my $data = $self->browser->get_marker_data( { level => $level } );
	$self->browser->seg_start(1);
	$self->browser->seg_stop( chr_length($chr) );
	$self->browser->round_positions;

	my ($binned_data) = $self->browser->calculate_binned_data(
		{
			bin_size => $bin_size * 1000000,
			data     => $data
		}
	);

	#retrieve bins with no P-values in at all and add to hash
	my %scores = ();
	foreach my $threshold ( 0 .. $self->config->{max_threshold} ) {
		$threshold == 0 and $threshold = 'ZERO';
		$self->browser->threshold($threshold);

		my ( $max_and_counts, $sig_markers, $max_pvalue, $max_count ) =
		  $self->browser->calculate_max_and_sig_counts(
			{ data => $binned_data } );

		foreach my $item ( @{$max_and_counts} ) {
			my $bin_start = $item->{bin_start};
			if ( $threshold eq 'ZERO' ) {
				$scores{$bin_start} = $item;
			}
			push @{ $scores{$bin_start}->{bin_scores} }, $item->{bin_counts};
		}
	}

	return %scores;
}

sub calc_region_binned {
	my ( $self, $rset, $limited_only ) = @_;
	my $rs_identifier = $rset->identifier;
	my $level = $limited_only ? 'limited' : 'admin';

	$self->log->info(
		"Precalculating $level region data for resultset: $rs_identifier");
	$self->prepare_precalculate_bins( $rset, 1 );

	foreach my $chr ( keys %{ chr_list() } ) {
		my %scores = $self->precalculate_bins( $chr, 1, $level );

		my $counter = 0;

		foreach my $item ( values %scores ) {

			my @pre_pars =
			  ( $chr, $item->{bin_start}, $item->{bin_stop}, $rs_identifier, );

			my @pars = ( @pre_pars, @{ $item->{bin_scores} } );
			if ( !$self->noaction ) {
				$limited_only
				  ? $self->sth_region_ltd->execute(@pars)
				  : $self->sth_region->execute(@pars);
			}
			$counter++;
		}
	}
}

sub calc_genome_binned {
	my ( $self, $rset, $limited_only ) = @_;
	my $rs_identifier = $rset->identifier;
	my $level = $limited_only ? 'limited' : 'admin';

	$self->log->info(
		"Precalculating $level genome data for resultset: $rs_identifier");
	$self->prepare_precalculate_bins( $rset, 3 );

	my $counter = 0;
	foreach my $chr ( keys %{ chr_list() } ) {
		my %scores = $self->precalculate_bins( $chr, 3, $level );
		my @pars = $self->_calc_bin_scores( \%scores, $chr, $rs_identifier );
		if ( !$self->noaction ) {
			$limited_only
			  ? $self->sth_genome_ltd->execute(@pars)
			  : $self->sth_genome->execute(@pars);
		}
		$counter++;
	}
}

sub _calc_bin_scores {
	my ( $self, $scores, $chr, $rs_identifier ) = @_;
	my %scores        = %{$scores};
	my @chr_scores    = ();
	my @sorted_scores = ();
	for (
		my $threshold = 0 ;
		$threshold <= $self->config->{max_threshold} ;
		$threshold++
	  )
	{
		@sorted_scores =
		  sort { $a->{bin_start} <=> $b->{bin_start} } values %scores;
		my @thres_scores =
		  map { $_->{bin_scores}->[$threshold] } @sorted_scores;
		push @chr_scores, join( ";", @thres_scores );
	}

	my @pre_pars = (
		$chr,
		$sorted_scores[0]->{bin_start},
		$sorted_scores[ scalar(@sorted_scores) - 1 ]->{bin_stop},
		$rs_identifier,
	);
	my @pars = ( @pre_pars, @chr_scores );
	return @pars;
}

sub get_insert_sig {
	my ( $self, $chr ) = @_;

	tie my %fields, "Tie::Hash::Indexed";
	%fields = (
		Study_Identifier     => 1,
		Resultset_Identifier => 1,
		UnadjustedPValue     => 1,
		NegLogPValue         => 1,
		Marker_Identifier    => 1,
		Marker_Accession     => 1,
		VariationType        => 1,
		Chr                  => 1,
		Start                => 1,
		Stop                 => 1,
		Strand               => 1,
		SignificanceID       => 1,
	);

	my $length = scalar( keys %fields );

	my $pholders = "?," x $length;
	$pholders = substr( $pholders, 0, length($pholders) - 1 );
	my $sql =
	    "INSERT INTO marker_data_chr" 
	  . $chr . " ("
	  . join( ",", keys %fields )
	  . ") VALUES("
	  . $pholders . ")";
	my $sth = $self->Browser->DataSource->dbh->prepare($sql);
	return $sth;
}

sub get_insert_ltd_sig {
	my ( $self, $chr ) = @_;
	tie my %fields, "Tie::Hash::Indexed";
	%fields = (
		Study_Identifier     => 1,
		Resultset_Identifier => 1,
		UnadjustedPValue     => 1,
		NegLogPValue         => 1,
		Marker_Identifier    => 1,
		Marker_Accession     => 1,
		VariationType        => 1,
		Chr                  => 1,
		Start                => 1,
		Stop                 => 1,
		Strand               => 1,
		SignificanceID       => 1
	);

	my $length = scalar( keys %fields );

	my $pholders = "?," x $length;
	$pholders = substr( $pholders, 0, length($pholders) - 1 );
	my $sql =
	    "INSERT INTO marker_data_ltd_chr" 
	  . $chr . " ("
	  . join( ",", keys %fields )
	  . ") VALUES("
	  . $pholders . ")";
	my $sth = $self->Browser->DataSource->dbh->prepare($sql);
	return $sth;

}

sub delete_marker_lists {
	my ( $self, $only_limited, $changed_markers ) = @_;
	my $tablename =
	  $only_limited
	  ? "marker_significances_ltd_chr"
	  : "marker_significances_chr";
	foreach my $chr ( keys %{ chr_list() } ) {
		if ($changed_markers) {
			my $sql = "delete from $tablename$chr";
			my @cms = keys %{ $changed_markers->{$chr} };
			next if !@cms or scalar(@cms) == 0;
			my $pars = "?," x scalar(@cms);
			$pars = substr( $pars, 0, length($pars) - 1 );
			$sql .= " where Marker_Identifier IN ($pars)";

			my $sth = $self->browser->get_db->dbh->prepare($sql);
			if ( !$self->noaction ) {
				$sth->execute(@cms);
			}
		}
		else {
			$self->Browser->DataSource->dbh->do("TRUNCATE $tablename$chr");
		}
	}
}

sub _determine_changed_chr_markers {
	my ( $self, $chr, $limited_only ) = @_;
	my $sth;
	my $data_tablename =
	  $limited_only ? "marker_data_ltd_chr" . $chr : "marker_data_chr" . $chr;

	my @pars = ();
	my $sql;
	my $number;
	if ( $self->all ) {
		my $sql =
"select SignificanceID, Marker_Accession, Marker_Identifier,Start, Stop, Resultset_Identifier, NegLogPValue, Study_Identifier from $data_tablename where Resultset_Identifier is not null and Resultset_Identifier not like 'U%'";
	}
	else {
		my @cms =
		  $limited_only
		  ? keys %{ $self->ltd_changed_markers->{$chr} }
		  : keys %{ $self->changed_markers->{$chr} };
		return undef if !@cms or scalar(@cms) == 0;
		$sql =
"select SignificanceID, Marker_Accession, Marker_Identifier,Start, Stop, Resultset_Identifier, NegLogPValue, Study_Identifier from $data_tablename$chr";
		$number = scalar(@cms);
		my $par_list = "?," x $number;
		$par_list = substr( $par_list, 0, length($par_list) - 1 );
		$sql .= " where Marker_Identifier IN ($par_list)";
		@pars = @cms;
	}

	$sth = $self->Browser->DataSource->dbh->prepare($sql);
	if ( !$self->noaction ) {
		$sth->execute(@pars);
	}
	$self->log->info(
		$number . " markers updated in significance list for chr $chr" );

	my %markers = ();

	while ( my $row = $sth->fetchrow_hashref ) {
		my $mid = $row->{'Marker_Identifier'};
		$markers{$mid} = {
			accession => $row->{Marker_Accession},
			start     => $row->{Start},
			stop      => $row->{Stop},
			data      => []
		  }
		  if !exists( $markers{$mid} );
		push @{ $markers{$mid}->{data} }, $row;
	}
	return %markers;
}

sub generate_marker_lists {
	my ( $self, $limited_only ) = @_;

	foreach my $chr ( keys %{ chr_list() } ) {

		my %markers =
		  $self->_determine_changed_chr_markers( $chr, $limited_only );
		$self->_generate_chr_markers_list( $chr, \%markers );
	}
}

sub _generate_chr_markers_list {
	my ( $self, $chr, $markers_ref, $only_limited ) = @_;
	my %markers = %{ $markers_ref || {} };
	my @fields = "HasList='1'";
	my $sigs_tablename =
	  $only_limited
	  ? "marker_significances_ltd_chr"
	  : "marker_significances_chr";

	for ( my $t = 0 ; $t <= $self->config->{max_threshold} ; $t++ ) {
		push @fields, "SignificanceList$t=?";
	}
	for ( my $t = 0 ; $t <= $self->config->{max_threshold} ; $t++ ) {
		push @fields, "StudyList$t=?";
	}

	my $sql =
	    "update $sigs_tablename$chr set "
	  . join( ",", @fields )
	  . " where Marker_Identifier = ?";

	my $sth = $self->DS('Browser')->dbh->prepare($sql);
	my $i   = 0;
	foreach my $mid ( keys %markers ) {
		my @resultsetlists = ();
		my @studylists     = ();
		foreach my $marker ( @{ $markers{$mid}->{data} } ) {

			for (
				my $threshold = $self->config->{max_threshold} ;
				$threshold >= 0 ;
				$threshold--
			  )
			{
				if ( $marker->{NegLogPValue} >= $threshold ) {
					for ( my $loc = 0 ; $loc <= $threshold ; $loc++ ) {
						my $resultset =
						    $marker->{Resultset_Identifier} . "|"
						  . $marker->{SignificanceID};
						my $study = $marker->{Study_Identifier};

						$resultsetlists[$loc]->{$resultset} = 1;
						$studylists[$loc]->{$study}         = 1;
					}
					last;
				}
			}
		}
		my @study;
		my @resultset;
		foreach my $counter ( 0 .. $self->config->{max_threshold} ) {
			my $resultset = $resultsetlists[$counter];
			my $study     = $studylists[$counter];
			$resultset[$counter] = join( ";", keys %{$resultset} );
			$study[$counter]     = join( ";", keys %{$study} );
		}
		my @pars = ( @resultset, @study, $mid );
		if ( !$self->noaction ) {
			$sth->execute(@pars);
		}
		$i++;
		$i % $BATCH_SIZE == 0
		  and print STDERR "\rChr $chr - $i entries processed";
	}
}

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: Creator.pm 1560 2010-10-28 15:55:59Z rcf8 $ 

=cut
