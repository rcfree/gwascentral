# $Id: Genome.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

GwasCentral::Browser::Display - Create common GFF features in browser


=head1 SYNOPSIS

package GwasCentral::Browser::Core;

use Moose;
extends qw(GwasCentral::ConfigLogger GwasCentral::Browser::Display);
	  
=head1 DESCRIPTION

Used by the Browser::Core module to provide general Browser display functions such as adding lines, headings and bin-based significance counts

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Browser::Display;

use Moose;
use GwasCentral::Browser::Util
  qw(chr_list chr_length split_data_by_field format_bases poslog);

=head2 _get_colors_by_resultsets

	  Usage      : my %colors = $browser->_get_colours_by_resultset();
    			   
	  Purpose    : For each resultset identifier/upload identifier, converts their GD colour names to RGB values for use by HTML pages
	  Returns    : A hashref with key of resultset/upload ident and value a decimal RGB colour (eg. '255,255,0')
	  			   $max_count - max count found in data set
	  Arguments  : None
	  Throws     : 
	  Status     : Protected
	  Comments   : 
	
=cut

sub _get_colors_by_resultsets {
	my ($self) = @_;
	my %colors_for = ();
	my $rscounter = 0;
	my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
	my $panel = Bio::Graphics::Panel->new;
	foreach my $resultset(@{$self->resultsets_and_uploads}) {
		my @rgb = $panel->color_name_to_rgb( $colors[$rscounter] );
		$colors_for{$resultset} = join(",",@rgb);
		$rscounter++;
	}
	return %colors_for;
}


=head2 add_sig_counts

	  Usage      : my ($max_count) = $browser->add_sig_counts({
    				chr=>'9',
    				bin_size=>3000000,
    				data=>\@data,
   			   });
    			   
	  Purpose    : Add binned significant count features to specific chromosome Main method to generate feature data for use by modified hgvbrowse_karyotype CGI script. Also provides the max value for stacked plot for any chromosome
	  Returns    : $max_value - highest value for bin_count found in data set
	  Arguments  : In hash ref
	  				 chr - Chromosome number (automatically adds 'Chr' prefix if region view [optional]
	  				 bin_size - bin size used to generate data
	  				 data - arrayref of binned data (see GwasCentral::Browser::Binning)
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub add_sig_counts {
	my ( $self, $attrs ) = @_;
	my $chr       = $attrs->{chr}       || $self->chr;
	my $rs_counts = $attrs->{rs_counts} || {};
	my $logscale  = $self->logscale;
	my $chr_length = chr_length($chr);

	my $features = $self->features;
	my $data     = $attrs->{data};
	my $bin_size = $attrs->{bin_size} || 3000000;

	my $counter    = 1;
	my @sum_scores = ();

	#define expected value if totally empty (eg. -1|-1|...)
	my $empty_score = "-1|" x scalar( @{ $self->resultsets_and_uploads } );
	$empty_score = substr( $empty_score, 0, length($empty_score) - 1 );

	#define expected value if no markers in the region (eg. 0|0|..)
	my $zero_score = "0|" x scalar( @{ $self->resultsets_and_uploads } );
	$zero_score = substr( $zero_score, 0, length($zero_score) - 1 );

	my $chr_prefix =
	  ref($self) =~ /Region/ ? 'Chr' : '';    #add Chr prefix if region view
	
	#loop through each item in data
	for my $item ( @{$data} ) {
		my $actual_score = $item->{'bin_counts'} || '0';
		my $score = 0;
		my $sum_score=0;
		my @temp = ();

		#get array of actual scores for data item and loop through them
		my @scores = split( /\|/, $actual_score );
		foreach (@scores) {

		  #if score is not empty or zero process for scale-type and add to array
		  #otherwise add a '0'
			if ( $_ ne -1 && $_ ne 0 && $_ ne 'X') {

				#log count if logscale flag is 1
				if ( $self->setting('scale_type') eq 'Log10' ) {
					my $value = sprintf( "%.3f", poslog( $_ + 1 ) );
					push @temp, $value;
					$sum_score+=$value;
				}
				else {
					push @temp, $_;
					$sum_score+=$_;
				}
			}
			else {
				push @temp, '0';
			}

			#join the scores together with | and get a total for the bin
			$score = join( '|', @temp );
		}

		#prevent start and stop going off ends of chromosome
		my $range_start =
		    $item->{'bin_start'} >= $bin_size
		  ? $item->{'bin_start'}
		  : 1;
		my $range_stop =
		    $item->{'bin_stop'} < $chr_length
		  ? $item->{'bin_stop'}
		  : $chr_length;
		my $name = "s$chr" . "_" . $counter;

		my $short_start = sprintf( "%03d", int( $range_start / 1000000 ) );

#add final data to list of feature data chr and position are represented as Chr.Pos (ie. 4.351)
		my $url =
		    "submitSearch('region','"
		  . $chr_prefix
		  . $chr . ":"
		  . $item->{bin_start} . ".."
		  . $item->{bin_stop} . "')";
		my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
		my $panel = Bio::Graphics::Panel->new;

		#deal with chrX and Y (passed in by JS as 23 and 24)
		my $js_chr = $chr eq 'X' ? '23' : $chr eq 'Y' ? '24' : $chr;

#| separated score is dealt with by custom Bio::Graphics::Glyph::stackplot in GBrowse
#include hack with onclick to call JS function
		my $feature = Bio::Graphics::Feature->new(
			-start      => $item->{bin_start},
			-stop       => $item->{bin_stop},
			-score      => $score,
			-ref        => $chr_prefix . $chr,
			-type       => 's',
			-name       => "Count:$actual_score",
			-attributes => { 'onClick' => ["gvToRV($js_chr.$short_start)"], },
			-url        => ( ref($self) =~ /Region/ ? $url : '' )
		);

	 #store actual score in feature as well (used to sort out axes in stackplot)
		$feature->{actualscore} = $actual_score;

		#sort out HTML popup for each bin and store this in the feature as well
		if ( $actual_score eq $empty_score ) {
			$feature->{popup} = "No markers in bin";
			$feature->{empty} = 1;
		}
		else {
			my $html = "<b>Click to view region in detail</b><br/><br/><u>Number of significant markers in bin</u>";
			$html .= "<table>";
			my $rscounter = 0;
			my $rows      = 0;
			foreach (@scores) {
				my $rsid = $self->resultsets_and_uploads->[$rscounter];
				
				if ( $_ && $_ ne 'X' && $_ > 0 ) {
					$rs_counts->{$rsid} += $_;
					my $summary = $self->summaries->{$rsid};
					my @rgb = $panel->color_name_to_rgb( $colors[$rscounter] );
					$html .= "<tr><td style='vertical-align:middle'>$_</td>";
					$html .=
					    "<td style='background-color:rgb("
					  . join( ",", @rgb )
					  . "); width:16px; height:12px;border:solid 1px black'>&nbsp;</td>";
					  my $study_name; 
					  my $study_ident; 
					  my $rs_name;
					  
					  if ($rsid =~ /^U/) {
					  	$study_name = $self->upload_data->{$rsid}->{study_name};
					  	$study_ident = 'UPLOAD';
					  	$rs_name = $self->upload_data->{$rsid}->{resultset_name};
					  }
					  else {
						  $study_name = $summary->experiment->study->name;
						  $study_ident = $summary->experiment->study->identifier;
						  $rs_name = $summary->name;
					  }
					$self->summaries
					  and $html .=
					    "<td><b>"
					  . $study_name . " ("
					  . $study_ident
					  . ")</b><br><i>"
					  . $rs_name . " ("
					  . ( $rsid || 'unknown' )
					  . ")</i></td>";
					$html .= "</tr>";
					$rows++;
				}
				$rscounter++;
			}

			if ( $rows > 0 ) {
				$html .= "</table>";
			}
			else {
				$html = "No significant markers in bin";
			}

			$feature->{popup} = $html;
		}

		#add the feature to the feature list
		$features->add_feature( $feature => 's' );
		
		#add the total score to an array of total scores
		push @sum_scores, $sum_score;
		$counter++;
	}

#once finished looping through the data items get the highest score in the array of total scores
	my @sorted = reverse sort { $a <=> $b } @sum_scores;
	return ( $sorted[0], $rs_counts );
}

=head2 add_bins_present

	  Usage      : $browser->add_bins_present({
    				chr=>'9',
    				bin_size=>3000000,
    				data=>\@data,
   			   });
    			   
	  Purpose    : Add binned significant count features to specific chromosome Main method to generate feature data for use by modified hgvbrowse_karyotype CGI script. Also provides the max value for stacked plot for any chromosome
	  Returns    : Nothing
	  Arguments  : In hash ref
	  				 chr - Chromosome number (automatically adds 'Chr' prefix if region view
	  				 binsize - size of bins to add in base pairs
	  				 data - arrayref of binned data (see GwasCentral::Browser::Binning)
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub add_bins_present {
	my ( $self, $attrs ) = @_;
	my $chr      = $attrs->{chr} || $self->chr;
	my $data     = $attrs->{data};
	my $features = $self->features;

	my $counter   = 1;
	my %start_pos = ();
	my %end_pos   = ();
	my %prev_stop = ();
	my %within    = ();

#more elegant and speeds up rendering to draw lines as required, rather than on or off feature for each bin
	foreach my $rsid ( @{ $self->resultsets_and_uploads } ) {
		$start_pos{$rsid} = $self->seg_start;
		$end_pos{$rsid}   = $self->seg_start;
	}

#uses a hash based flag system to determine whether it is 'in' or 'out' of a coloured bar
#loop through each item in the data array
	for my $item ( @{$data} ) {
		my $rscounter  = 0;
		my $score_list = $item->{'bin_counts'};

#get scores for each resultset by splitting by | and loop through these
#draw a coloured box if score is not -1 (colouring of box is determined in 'add_bins_present_glyph')

		#get the score for this item in resultset_ident order
		my @scores = split( /\|/, $score_list );

		#loop through each resultset id
		foreach my $rsid ( @{ $self->resultsets_and_uploads } ) {

			#if within a coloured bar for this resultset..
			if ( $within{$rsid} ) {

#if the bin is empty then make this bin the end the coloured bar and add the feature
				if ( $scores[$rscounter] eq '-1' ) {

					$end_pos{$rsid} = $prev_stop{$rsid};
					$within{$rsid}  = 0;
					my $feature = Bio::Graphics::Feature->new(
						-start => $start_pos{$rsid},
						-stop  => $end_pos{$rsid},
						-ref   => $chr,
						-type  => "l$rsid",
						-score => 1,
					);
					$self->log->info("f start:".$start_pos{$rsid}." stop:".$end_pos{$rsid});
					$features->add_feature( $feature => "l$rsid" );
				}
			}

			#if not within a coloured bar for this resultset..
			else {

#if the bin is not empty then make this bin the end of the coloured bar with a start determined by the previous end
				if ( $scores[$rscounter] and $scores[$rscounter] ne '-1' ) {
					if ($item->{'bin_start'} != $end_pos{$rsid}) {
						my $feature = Bio::Graphics::Feature->new(
							-start => $end_pos{$rsid},
							-stop  => $item->{'bin_start'},
							-ref   => $chr,
							-type  => "l$rsid",
							-score => 0,
						);
						$self->log->info("f start:".$end_pos{$rsid}." stop:".$item->{'bin_start'});
						$features->add_feature( $feature => "l$rsid" );
					}
					
					$start_pos{$rsid} = $item->{'bin_start'};
					$within{$rsid}    = 1;
				}
			}

			#set the previous stop to be the end of this bin
			$prev_stop{$rsid} = $item->{'bin_stop'};
			$rscounter++;
		}

		$counter++;
	}

#ensure that any bars that any resultsets that were still within a bin have features created to the end of the region
	foreach my $rsid ( @{ $self->resultsets_and_uploads } ) {
		if ( $within{$rsid} ) {
			my $feature = Bio::Graphics::Feature->new(
				-start => $start_pos{$rsid},
				-stop  => $self->seg_stop,
				-ref   => $chr,
				-type  => "l$rsid",
				-score => 1,
			);
			$features->add_feature($feature);
		}
		else {
			my $feature = Bio::Graphics::Feature->new(
				-start => $end_pos{$rsid},
				-stop  => $self->seg_stop,
				-ref   => $chr,
				-type  => "l$rsid",
				-score => 0,
			);
			$features->add_feature($feature);
		}
	}
}


=head2 add_heading

	  Usage      : $self->add_heading("Nothing inparticular");
	  Purpose    : Add a heading 'feature' to Browser feature list
	  Returns    : Nowt
	  Arguments  : Heading text
	  Throws     : 
	  Status     : Public
	  Comments   : This is a bit of a hack. It adds a heading by creating a white line which has a label (ie. the text)
	               So far this seems to have worked ok. May need to expand on this anyway - will probably create a specific glyph
				   N.B. Don't use () or [] in the text as this breaks GBrowse!
=cut

sub add_heading {
	my ( $self, $heading ) = @_;
	$self->add_line($heading);
}

=head2 add_textbox

	  Usage      : $self->add_textbox("Nothing inparticular");
	  Purpose    : Add a heading 'feature' to Browser feature list
	  Returns    : Nowt
	  Arguments  : Heading text
	  Throws     : 
	  Status     : Public
	  Comments   : This is a bit of a hack. It adds a heading by creating a white line which has a label (ie. the text)
	               So far this seems to have worked ok. May need to expand on this anyway - will probably create a specific glyph
				   N.B. Don't use () or [] in the text as this breaks GBrowse!
=cut

sub add_textbox {
	my ( $self, $title, $text, $outline, $background ) = @_;

	$self->features->add_type(
		$title => {
			glyph        => 'multiline_text',
			fgcolor      => $outline,
			bgcolor      => $background,
			fontcolor    => 'black',
			label        => 0,
			text         => join( ";", @{$text} ),
			text_bgcolor => $background,
			separator    => ";",
			text_pad     => 5,
			bump         => 1,
			height       => 24,
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => $self->seg_start,
		-stop  => $self->seg_stop,
		-ref   => $self->ref,
	);

	$self->features->add_feature( $feature => $title );
}

=head2 add_dashed_line

	  Usage      : $self->add_dashed_line;
	  Purpose    : Add a dashed line which goes from 'start of region to end' to Browser feature list
	  Returns    : Nowt
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_dashed_line {
	my ($self) = @_;

	my $type = "line" . $self->next_featureno;
	$self->features->add_type(
		$type => {
			glyph       => 'dashed_line',
			fgcolor     => 'lightgrey',
			textcolor   => 'white',
			label       => 0,
			title       => 0,
			pad_bottom  => 0,
			pad_top     => 0,
			label       => 0,
			description => 0,
			label       => 0,
			key         => 0,
			linewidth   => 0.5,
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => (
			  $self->seg_start == $self->seg_stop ? $self->seg_start - 10
			: $self->seg_start
		),
		-stop => (
			  $self->seg_start == $self->seg_stop ? $self->seg_stop + 10
			: $self->seg_stop
		),
		-ref   => $self->ref,
		-score => 0
	);
	$self->features->add_feature( $feature => $type );
}

=head2 add_line
	  Usage      : $self->add_line;
	  Purpose    : Add a divider line of full detail width to Browser feature list
	  Returns    : Nowt
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_line {
	my ($self,$key) = @_;

	my $type = "line" . $self->next_featureno;
	my $fgcolor = $key ? 'white' : 'lightgrey';
	$self->features->add_type(
		$type => {
			glyph       => 'line',
			fgcolor     => $fgcolor,
			textcolor   => 'white',
			label       => 0,
			title       => 0,
			pad_bottom  => 0,
			pad_top     => 0,
			label       => 0,
			description => 0,
			key         => 0,
			linewidth   => 2,
			height      => 2,
			key => $key
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => (
			  $self->seg_start == $self->seg_stop ? $self->seg_start - 10
			: $self->seg_start
		),
		-stop => (
			  $self->seg_start == $self->seg_stop ? $self->seg_stop + 10
			: $self->seg_stop
		),
		-ref   => $self->ref,
		-score => 0
	);
	$self->features->add_feature( $feature => $type );
}

=head2 add_link

	  Usage      : $self->add_link("Nothing inparticular");
	  Purpose    : Add a link 'feature' to Browser feature list
	  Returns    : Nowt
	  Arguments  : Link text and address
	  Throws     : 
	  Status     : Public
	  Comments   : This uses text_in_box glyph coloured blue to simulate a link
=cut

sub add_link {
	my ( $self, $link_text, $link ) = @_;

	my $heading = " ";
	$self->features->add_type(
		$heading => {
			glyph     => 'text_in_box',
			fgcolor   => 'blue',
			bgcolor   => 'black',
			fontcolor => 'blue',
			label     => 0,
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => $self->seg_start,
		-stop  => $self->seg_stop,
		-ref   => $self->ref,
		-score => 0,
		-type  => $heading,
		-name  => ' ',
		-url   => $link,

	);
	$feature->{text} = $link_text;
	$self->features->add_feature( $feature => $heading );
}


=head2 add_bins_present_glyph

	  Usage      : $browser->add_bins_present_glyph({
    				height=>'20'
   			   });
    			   
	  Purpose    : Add glyphs of marker coverage for each resultset in Browser object
	  Returns    : Nothing
	  Arguments  : In hash ref
	  				 height - Height of boxes to draw
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub add_bins_present_glyph {
	my ( $self, $attrs ) = @_;
	my $panel = Bio::Graphics::Panel->new;
	my @colors =
	  split( ' ', $self->config->{Browser}->{bgcolors} );

	my $rscounter = 0;

	foreach my $rs_ident ( @{ $self->resultsets_and_uploads } ) {
		my $html = "<u>Marker coverage for:</u></br>";

		my $summary = $self->summaries->{$rs_ident};
		my @rgb     = $panel->color_name_to_rgb( $colors[$rscounter] );
		$html .= "<table><tr><td style='vertical-align:middle'></td>";
		$html .=
		    "<td style='background-color:rgb("
		  . join( ",", @rgb )
		  . "); width:16px; height:12px;border:solid 1px black'>&nbsp;</td>";
		  my $study_name; 
					  my $study_ident; 
					  my $rs_name;
		  if ($rs_ident =~ /^U/) {
					  	$study_name = $self->upload_data->{$rs_ident}->{study_name} || 'name';
					  	$study_ident = $self->upload_data->{$rs_ident}->{study_identifier} || 'ident';
					  	$rs_name = $self->upload_data->{$rs_ident}->{resultset_name} || 'ident';
					  }
					  else {
						  $study_name = $summary->experiment->study->name;
						  $study_ident = $summary->experiment->study->identifier;
						  $rs_name = $summary->name;
					  }
		$self->summaries
		  and $html .=
		    "<td><b>"
		  . $study_name . " ("
		  . $study_ident
		  . ")</b><br><i>"
		  . $rs_name . " ("
		  . $rs_ident
		  . ")</i></td>";
		$html .= "</tr>";
		$html .= "</table>";

		$self->features->add_type(
			"l$rs_ident" => {
				glyph           => 'graded_segments',
				fgcolor         => 'lightgrey',
				bgcolor         => $colors[$rscounter],
				bump            => 0,
				label           => 0,
				description     => 0,
				title           => 0,
				key             => 0,
				height          => $attrs->{height},
				method          => 'presence',
				source          => 'GwasCentral',
				max_score       => 1,
				min_score       => 0,
				'balloon width' => 200,
				'balloon hover' => 'sub { return "' . $html . '";}'
			}
		);
		$rscounter++;
	}
}

=head2 add_sig_counts_glyph

	  Usage      : $browser->add_bins_present_glyph({
    				height=>'20'
   			   });
    			   
	  Purpose    : Add binned significant count glyphs for each resultset in Browser object
	  Returns    : Nothing
	  Arguments  : In hash ref
	  				 max_count - Maximum count in stackplot graph
	  				 height - Height in pixels of stackplot graph
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub add_sig_counts_glyph {
	my ( $self, $attrs ) = @_;

	my $max_count = $attrs->{max_count} || 20;
	my $key = $attrs->{key};
	
	$self->features->add_type(
		's' => {
			glyph      => 'stackplot',
			graph_type => 'boxes',
			fgcolor    => sub {
				my $f = shift;
				return $f->{empty} ? "lightgrey" : "black";
			},
			bgcolors        => $self->config->{Browser}->{bgcolors},
			height          => $attrs->{height} || 20,
			min_score       => 0,
			max_score       => 10,
			scale           => 'none',
			bump            => 0,
			key             => 0,
			label           => 0,
			'balloon width' => 300,
			'balloon hover' =>
			  'sub { my $feature = shift; return $feature->{"popup"}; }',
			method => 'sig_counts',
			source => 'GwasCentral',
			key=>$key,
		}
	);
}
1;