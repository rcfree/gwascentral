# $Id: Util.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Browser::Util - Utility methods used by the Browser system


=head1 SYNOPSIS

	  
=head1 DESCRIPTION

The Util module provides static methods for simple things like poslog calculations and chr_length retrieval.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Browser::Util;

use strict;
use warnings;

use Carp qw(confess);
use English qw( -no_match_vars );
use Data::Dumper;
use Exporter qw(import);
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($DEBUG);

BEGIN {

	# Export no subroutines by default, only this list on demand
	our @EXPORT_OK = qw( chr_list
	  chr_length
	  hwe
	  poslog
	  split_data_by_field
	  format_bases
	  neglog
	  inv_neglog
	  chr_list
	);
}

my $VERSION = "1.00";

#chr length data
my $chr_data = {
	'1'  => 247249719,
	'10' => 135374737,
	'11' => 134452384,
	'12' => 132349534,
	'13' => 114142980,
	'14' => 106368585,
	'15' => 100338915,
	'16' => 88827254,
	'17' => 78774742,
	'18' => 76117153,
	'19' => 63811651,
	'2'  => 242951149,
	'20' => 62435964,
	'21' => 46944323,
	'22' => 49691432,
	'3'  => 199501827,
	'4'  => 191273063,
	'5'  => 180857866,
	'6'  => 170899992,
	'7'  => 158821424,
	'8'  => 146274826,
	'9'  => 140273252,
	'X'  => 154913754,
	'Y'  => 57772954
};

=head2 poslog

	  Usage      : my $log10 = poslog(5,10);
	  Purpose    : Get positive log of number
	  Returns    : positive logarithm of base specified
	  Arguments  : Number to return log of. Logarithm base to use
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub poslog {
	my ( $value, $base ) = @_;
	!$base and $base = 10;
	$value == 0  and return '0.00';
	$value == -1 and confess "-1 passed to poslog";
	my $logged = log($value) / log($base);
	return $logged;
}

=head2 neglog

	  Usage      : my $log10 = neglog(5,10);
	  Purpose    : Get positive log of number
	  Returns    : positive logarithm of base specified
	  Arguments  : Number to return log of. Logarithm base to use
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub neglog {
	my ( $value, $base ) = @_;
	!$base and $base = 10;
	$value == 0  and return '0.00';
	$value == -1 and confess "-1 passed to poslog";
	my $logged = -log($value) / log($base);
	return $logged;
}

=head2 inv_neglog
=cut

sub inv_neglog {
	my ($value) = @_;
	return 10**-$value;
}

=head2 chr_length

	  Usage      : my $length = $self->chr_length('12');
	  Purpose    : Get length of chromosome
	  Returns    : length of chr specified retrieve from the $chr_data hash ref
	  Arguments  : chromosome no
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub chr_length {
	my ($chr) = @_;

	#INFO("chr_length for ".$chr.":".$chr_data->{ $chr });
	if ( ref($chr) eq 'GwasCentral::Browser::Core' ) {
		confess;
	}
	return $chr_data->{$chr};
}

=head2 chr_list

	  Usage      : my $list = $self->chr_list;
	  Purpose    : Get chromosome list (ie. hash with lengths)
	  Returns    : Chromosome list
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub chr_list {
	my ($chr) = @_;
	return $chr_data;
}

=head2 format_bases

	  Usage      : print format_bases(100000);
	  Purpose    : Format bases as Kb or Mb depending on length
	  Returns    : Formatted text
	  Arguments  : Bases - number of bases 
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub format_bases {
	my ($bin_size) = @_;
	my $bin_info;
	if ( length($bin_size) >= 4 and length($bin_size) < 7 ) {
		$bin_info = substr( $bin_size, 0, length($bin_size) - 3 ) . "Kb";
	}

	if ( length($bin_size) >= 7 and length($bin_size) < 9 ) {
		$bin_info = substr( $bin_size, 0, length($bin_size) - 6 ) . "Mb";
	}
	return $bin_info;
}

=head2 hwe

	  Usage      : my ($shortchi2, $chisprob, $fval) = hwe(30,50,20);
	  Purpose    : Calculate Hardy Weinberg statistics for given genotype numbers
	  Returns    : Chi-squared value, chi-squared P-value and F value
	  Arguments  :  p2 - number of P homozygous individuals
	  				q2 - number of Q homozygous individuals
	  				pq - number of PQ heterozygous individuals
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub hwe {
	my ( $p2, $q2, $pq2 ) = @_;

	my $total_inds = $p2 + $q2 + $pq2;
	TRACE "values: p2=$p2, q2=$q2, pq2=$pq2, total_inds:$total_inds";

	# Calculate allele freqs
	my $allele1freq = ( ( $p2 * 2 ) + $pq2 ) / ( $total_inds * 2 );
	my $allele2freq = ( ( $q2 * 2 ) + $pq2 ) / ( $total_inds * 2 );

	#print "Allele1: $allele1freq\tAllele2: $allele2freq\n";

	my $exp_p2 = ( ( $allele1freq * $allele1freq ) * $total_inds );
	my $exp_q2 = ( ( $allele2freq * $allele2freq ) * $total_inds );
	my $exp_pq2 = ( $allele1freq * $allele2freq * 2 ) * $total_inds;

#print "p2:$p2\tq2:$q2\t2pq:$pq2\tp:$allele1freq\tq:$allele2freq\ttotalinds:$total_inds\texp.p2:$exp_p2\texp.q2:$exp_q2\texp_2pq:$exp_pq2\n";

	if ( $exp_p2 == 0 || $exp_q2 == 0 || $exp_pq2 == 0 ) {
		return "NONSNP";
	}

	my $p_chi2   = ( ( $p2 - $exp_p2 )**2 ) / $exp_p2;
	my $q_chi2   = ( ( $q2 - $exp_q2 )**2 ) / $exp_q2;
	my $pq2_chi2 = ( ( $pq2 - $exp_pq2 )**2 ) / $exp_pq2;

	my $chisq = ( $p_chi2 + $q_chi2 + $pq2_chi2 );

	#print "Chi2: $chisq\n";

	my $shortchi2 = sprintf( "%.3f", $chisq );

	my $chisprob = Statistics::Distributions::chisqrprob( 1, $chisq );

	my $fval = $pq2 / $exp_pq2;
	return ( $shortchi2, $chisprob, $fval );
}

=head2 split_data_by_field

	  Usage      : my ($data,$field,$sort_type) = hwe(30,50,20);
	  Purpose    : Splits array containing hash elements/array elements into hash containing arrays split on a field 
	               (eg. keys could be 'chromosome', each key-value would contain an array of items only from a single chromosome)
	  Returns    : Processed hash containing split data 
	  Arguments  :  data - arrayref containing hash elements
	  				field - field to split data on
	  				sort_type - determines the type of field which is required for splitting data (1 for cmp comparison and 0 for <=>)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub split_data_by_field {
	my ( $data, $field, $sort_type ) = @_;

	my %chr_data = ();
	my @sorted   = ();

	if ( ref $data->[0] eq 'ARRAY' ) {
		if ($sort_type) {
			@sorted = sort { $a->[$field] cmp $b->[$field] } @{$data};
		}
		else {
			@sorted = sort { $a->[$field] <=> $b->[$field] } @{$data};
		}

		foreach my $item ( @{$data} ) {
			my $chr = $item->[$field];
			!$chr_data{$chr} and $chr_data{$chr} = [];
			push @{ $chr_data{$chr} }, $item;
		}
	}
	else {
		if ($sort_type) {
			@sorted = sort { $a->{$field} cmp $b->{$field} } @{$data};
		}
		else {
			@sorted = sort { $a->{$field} <=> $b->{$field} } @{$data};
		}

		foreach my $item ( @{$data} ) {
			my $chr = $item->{$field};
			!$chr_data{$chr} and $chr_data{$chr} = [];
			push @{ $chr_data{$chr} }, $item;
		}
	}

	return \%chr_data;
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2008> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Util.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut
