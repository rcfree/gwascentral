package GwasCentral::Schema::User::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::User::User

=cut

__PACKAGE__->table("user");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 password

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 roles

  data_type: 'text'
  is_nullable: 1

=head2 nickname

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 first_name

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 last_name

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 institution

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 is_emailed_on_request

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 1

=head2 is_emailed_on_response

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 1

=head2 is_emailed_on_upgrade

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 1

=head2 activation_code

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 time_created

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "user_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "password",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "roles",
  { data_type => "text", is_nullable => 1 },
  "nickname",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "first_name",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "last_name",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "institution",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "is_emailed_on_request",
  { data_type => "tinyint", default_value => 1, is_nullable => 1 },
  "is_emailed_on_response",
  { data_type => "tinyint", default_value => 1, is_nullable => 1 },
  "is_emailed_on_upgrade",
  { data_type => "tinyint", default_value => 1, is_nullable => 1 },
  "activation_code",
  { data_type => "char", is_nullable => 1, size => 20 },
  "time_created",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
);
__PACKAGE__->set_primary_key("user_id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 15:13:58
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SGW+qlZ4La+c0OFF28RTTQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
