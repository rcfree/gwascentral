package GwasCentral::Schema::Study::Phenotypeproperty;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypeproperty

=cut

__PACKAGE__->table("PhenotypeProperty");

=head1 ACCESSORS

=head2 phenotypepropertyid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 description

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "phenotypepropertyid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "description",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("phenotypepropertyid");

=head1 RELATIONS

=head2 pppas

Type: has_many

Related object: L<GwasCentral::Schema::Study::Pppa>

=cut

__PACKAGE__->has_many(
  "pppas",
  "GwasCentral::Schema::Study::Pppa",
  { "foreign.phenotypepropertyid" => "self.phenotypepropertyid" },
  {},
);

=head2 phenotypemethods

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->has_many(
  "phenotypemethods",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { "foreign.phenotypepropertyid" => "self.phenotypepropertyid" },
  {},
);

=head2 phenotypepropertycitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypepropertycitation>

=cut

__PACKAGE__->has_many(
  "phenotypepropertycitations",
  "GwasCentral::Schema::Study::Phenotypepropertycitation",
  { "foreign.phenotypepropertyid" => "self.phenotypepropertyid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CNTqYIV9ggYsXC+NDXZoVA

__PACKAGE__->many_to_many(phenotypeannotations => 'pppas', 'phenotypeannotationid' );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
