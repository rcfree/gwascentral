package GwasCentral::Schema::Study::CrossrefOld;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::CrossrefOld

=cut

__PACKAGE__->table("Crossref_old");

=head1 ACCESSORS

=head2 crossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 hotlinkid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 urlid

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "crossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "hotlinkid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "urlid",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("crossrefid");

=head1 RELATIONS

=head2 citationcrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::CitationcrossrefOld>

=cut

__PACKAGE__->has_many(
  "citationcrossref_olds",
  "GwasCentral::Schema::Study::CitationcrossrefOld",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);

=head2 phenotypemethodcrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::PhenotypemethodcrossrefOld>

=cut

__PACKAGE__->has_many(
  "phenotypemethodcrossref_olds",
  "GwasCentral::Schema::Study::PhenotypemethodcrossrefOld",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);

=head2 resultsetcrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::ResultsetcrossrefOld>

=cut

__PACKAGE__->has_many(
  "resultsetcrossref_olds",
  "GwasCentral::Schema::Study::ResultsetcrossrefOld",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);

=head2 samplepanelcrossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Study::Samplepanelcrossref>

=cut

__PACKAGE__->has_many(
  "samplepanelcrossrefs",
  "GwasCentral::Schema::Study::Samplepanelcrossref",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);

=head2 studycrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::StudycrossrefOld>

=cut

__PACKAGE__->has_many(
  "studycrossref_olds",
  "GwasCentral::Schema::Study::StudycrossrefOld",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:C3+6R5hJhfQFf7siRB+uuQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
