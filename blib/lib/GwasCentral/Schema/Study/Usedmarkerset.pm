package GwasCentral::Schema::Study::Usedmarkerset;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Usedmarkerset

=cut

__PACKAGE__->table("Usedmarkerset");

=head1 ACCESSORS

=head2 usedmarkersetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 markeridentifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 newmarkeridentifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=cut

__PACKAGE__->add_columns(
  "usedmarkersetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "markeridentifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "newmarkeridentifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
);
__PACKAGE__->set_primary_key("usedmarkersetid");
__PACKAGE__->add_unique_constraint("ExperimentID", ["experimentid", "markeridentifier"]);

=head1 RELATIONS

=head2 effectsizes

Type: has_many

Related object: L<GwasCentral::Schema::Study::Effectsize>

=cut

__PACKAGE__->has_many(
  "effectsizes",
  "GwasCentral::Schema::Study::Effectsize",
  { "foreign.usedmarkersetid" => "self.usedmarkersetid" },
  {},
);

=head2 frequencyclusters

Type: has_many

Related object: L<GwasCentral::Schema::Study::Frequencycluster>

=cut

__PACKAGE__->has_many(
  "frequencyclusters",
  "GwasCentral::Schema::Study::Frequencycluster",
  { "foreign.usedmarkersetid" => "self.usedmarkersetid" },
  {},
);

=head2 significances

Type: has_many

Related object: L<GwasCentral::Schema::Study::Significance>

=cut

__PACKAGE__->has_many(
  "significances",
  "GwasCentral::Schema::Study::Significance",
  { "foreign.usedmarkersetid" => "self.usedmarkersetid" },
  {},
);

=head2 experimentid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->belongs_to(
  "experimentid",
  "GwasCentral::Schema::Study::Experiment",
  { experimentid => "experimentid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RqYERKz7O4uctqluZQglmQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
