package GwasCentral::Schema::Study::Selectioncriteria;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Selectioncriteria

=cut

__PACKAGE__->table("SelectionCriteria");

=head1 ACCESSORS

=head2 selectioncriteriaid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 assayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 samplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 sourceassayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 environmentcriteria

  data_type: 'varchar'
  default_value: 'No selection'
  is_nullable: 1
  size: 500

=head2 numberofindividuals

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "selectioncriteriaid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "assayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "samplepanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "sourceassayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "environmentcriteria",
  {
    data_type => "varchar",
    default_value => "No selection",
    is_nullable => 1,
    size => 500,
  },
  "numberofindividuals",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("selectioncriteriaid");
__PACKAGE__->add_unique_constraint("AssayedpanelID", ["assayedpanelid", "samplepanelid"]);
__PACKAGE__->add_unique_constraint(
  "SourceAssayedpanelID",
  ["sourceassayedpanelid", "assayedpanelid"],
);

=head1 RELATIONS

=head2 pvscs

Type: has_many

Related object: L<GwasCentral::Schema::Study::Pvsc>

=cut

__PACKAGE__->has_many(
  "pvscs",
  "GwasCentral::Schema::Study::Pvsc",
  { "foreign.selectioncriteriaid" => "self.selectioncriteriaid" },
  {},
);

=head2 assayedpanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Assayedpanel>

=cut

__PACKAGE__->belongs_to(
  "assayedpanelid",
  "GwasCentral::Schema::Study::Assayedpanel",
  { assayedpanelid => "assayedpanelid" },
);

=head2 samplepanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid" },
);

=head2 sourceassayedpanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Assayedpanel>

=cut

__PACKAGE__->belongs_to(
  "sourceassayedpanelid",
  "GwasCentral::Schema::Study::Assayedpanel",
  { assayedpanelid => "sourceassayedpanelid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:sXYtNNm59W2LTEfbByx1hw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
