package GwasCentral::Schema::Study::Analysismethod;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Analysismethod

=cut

__PACKAGE__->table("AnalysisMethod");

=head1 ACCESSORS

=head2 analysismethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 submittedinstudyidentifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 identifier

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 protocolparameters

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "analysismethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "submittedinstudyidentifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "identifier",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "protocolparameters",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("analysismethodid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 resultsets

Type: has_many

Related object: L<GwasCentral::Schema::Study::Resultset>

=cut

__PACKAGE__->has_many(
  "resultsets",
  "GwasCentral::Schema::Study::Resultset",
  { "foreign.analysismethodid" => "self.analysismethodid" },
  {},
);

=head2 studyanalysismethods

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyanalysismethod>

=cut

__PACKAGE__->has_many(
  "studyanalysismethods",
  "GwasCentral::Schema::Study::Studyanalysismethod",
  { "foreign.analysismethodid" => "self.analysismethodid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5xHtSiz3CJRRnwA/7wlsNg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
