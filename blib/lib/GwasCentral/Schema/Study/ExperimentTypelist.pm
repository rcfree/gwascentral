package GwasCentral::Schema::Study::ExperimentTypelist;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::ExperimentTypelist

=cut

__PACKAGE__->table("Experiment_TypeList");

=head1 ACCESSORS

=head2 type

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "type",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
);
__PACKAGE__->set_primary_key("type");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bGcIl4wJ8M39yDKegkbGZw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
