package GwasCentral::Schema::Study::Phenotypemethod;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypemethod

=cut

__PACKAGE__->table("PhenotypeMethod");

=head1 ACCESSORS

=head2 phenotypemethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 phenotypepropertyid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 sample

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 measuredattribute

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 variabletype

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 unit

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 circumstance

  data_type: 'text'
  is_nullable: 1

=head2 timeinstant

  data_type: 'text'
  is_nullable: 1

=head2 timeperiod

  data_type: 'text'
  is_nullable: 1

=head2 details

  data_type: 'text'
  is_nullable: 1

=head2 accesslevel

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=cut

__PACKAGE__->add_columns(
  "phenotypemethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "phenotypepropertyid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "sample",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "measuredattribute",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "variabletype",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "unit",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "circumstance",
  { data_type => "text", is_nullable => 1 },
  "timeinstant",
  { data_type => "text", is_nullable => 1 },
  "timeperiod",
  { data_type => "text", is_nullable => 1 },
  "details",
  { data_type => "text", is_nullable => 1 },
  "accesslevel",
  { data_type => "varchar", is_nullable => 1, size => 10 },
);
__PACKAGE__->set_primary_key("phenotypemethodid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 experiments

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->has_many(
  "experiments",
  "GwasCentral::Schema::Study::Experiment",
  { "foreign.phenotypemethodid" => "self.phenotypemethodid" },
  {},
);

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);

=head2 phenotypepropertyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypeproperty>

=cut

__PACKAGE__->belongs_to(
  "phenotypepropertyid",
  "GwasCentral::Schema::Study::Phenotypeproperty",
  { phenotypepropertyid => "phenotypepropertyid" },
);

=head2 phenotypemethodcitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethodcitation>

=cut

__PACKAGE__->has_many(
  "phenotypemethodcitations",
  "GwasCentral::Schema::Study::Phenotypemethodcitation",
  { "foreign.phenotypemethodid" => "self.phenotypemethodid" },
  {},
);

=head2 phenotypemethodcrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::PhenotypemethodcrossrefOld>

=cut

__PACKAGE__->has_many(
  "phenotypemethodcrossref_olds",
  "GwasCentral::Schema::Study::PhenotypemethodcrossrefOld",
  { "foreign.phenotypemethodid" => "self.phenotypemethodid" },
  {},
);

=head2 phenotypemethodhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethodhotlink>

=cut

__PACKAGE__->has_many(
  "phenotypemethodhotlinks",
  "GwasCentral::Schema::Study::Phenotypemethodhotlink",
  { "foreign.phenotypemethodid" => "self.phenotypemethodid" },
  {},
);

=head2 phenotypevalues

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypevalue>

=cut

__PACKAGE__->has_many(
  "phenotypevalues",
  "GwasCentral::Schema::Study::Phenotypevalue",
  { "foreign.phenotypemethodid" => "self.phenotypemethodid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KFr++KMwbwRerv6TAMAuWQ


*study = \&studyid;
*phenotypeproperty = \&phenotypepropertyid;
__PACKAGE__->many_to_many(citations => 'phenotypemethodcitations', 'citationid' );
__PACKAGE__->many_to_many(hotlinks => 'phenotypemethodhotlinks', 'hotlinkid' );

sub to_hash {
	my ($self) = @_;
	my %pmethod = $self->get_columns;
	my @pvalues = $self->phenotypevalues;
	delete $pmethod{phenotypepropertyid};
	my @pvalues_in_pmethod=();
	foreach my $pvalue (@pvalues) {
		my %pvalue_data = $pvalue->get_columns;
		push @pvalues_in_pmethod, \%pvalue_data;
	}
	$pmethod{phenotypevalue}=\@pvalues_in_pmethod;
	$pmethod{phenotypeproperty}={$self->phenotypeproperty->get_columns};
	return \%pmethod;
}


sub accesslevel {
	my ($self,$value)=@_;
	if (defined($value)) {
		$self->{_accesslevel}=$value;
		return $value;
	}
	return $self->{_accesslevel};
}

sub response_status {
	my ($self,$value)=@_;
	if (defined($value)) {
		$self->{_response_status}=$value;
		return $value;
	}
	return $self->{_response_status};
}

sub child_accesslevel {
	my ($self,$value)=@_;
	if (defined($value)) {
		$self->{_child_accesslevel}=$value;
		return $value;
	}
	return $self->{_child_accesslevel};
}

sub number {
	my ($self) = @_;
	$self->identifier =~ /HGVPM(\w+)/;
	return $1;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
