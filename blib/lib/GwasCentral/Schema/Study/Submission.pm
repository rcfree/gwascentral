package GwasCentral::Schema::Study::Submission;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Submission

=cut

__PACKAGE__->table("Submission");

=head1 ACCESSORS

=head2 submissionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 timecreated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "submissionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "timecreated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
  },
);
__PACKAGE__->set_primary_key("submissionid");
__PACKAGE__->add_unique_constraint("StudyID", ["studyid", "name"]);

=head1 RELATIONS

=head2 contributions

Type: has_many

Related object: L<GwasCentral::Schema::Study::Contribution>

=cut

__PACKAGE__->has_many(
  "contributions",
  "GwasCentral::Schema::Study::Contribution",
  { "foreign.submissionid" => "self.submissionid" },
  {},
);

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 12:49:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zyDGoftL4r/Hpu1CHdFgCg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
