package GwasCentral::Schema::Study::Study;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Study

=cut

__PACKAGE__->table("Study");

=head1 ACCESSORS

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 whochecked

  data_type: 'enum'
  extra: {list => ["sg303","rkh7","tb143","rcf8"]}
  is_nullable: 1

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 ishidden

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 1

=head2 studyabstract

  data_type: 'text'
  is_nullable: 0

=head2 background

  data_type: 'text'
  is_nullable: 1

=head2 objectives

  data_type: 'text'
  is_nullable: 1

=head2 keyresults

  data_type: 'text'
  is_nullable: 1

=head2 conclusions

  data_type: 'text'
  is_nullable: 1

=head2 studydesign

  data_type: 'text'
  is_nullable: 1

=head2 studysizereason

  data_type: 'text'
  is_nullable: 1

=head2 studypower

  data_type: 'text'
  is_nullable: 1

=head2 sourcesofbias

  data_type: 'text'
  is_nullable: 1

=head2 limitations

  data_type: 'text'
  is_nullable: 1

=head2 acknowledgements

  data_type: 'text'
  is_nullable: 1

=head2 timecreated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0

=head2 timeupdated

  data_type: 'timestamp'
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 isdisplayed

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 1

=head2 significancelevel

  data_type: 'smallint'
  default_value: 0
  is_nullable: 1

=head2 accesslevel

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=cut

__PACKAGE__->add_columns(
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "whochecked",
  {
    data_type => "enum",
    extra => { list => ["sg303", "rkh7", "tb143", "rcf8"] },
    is_nullable => 1,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "ishidden",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 1,
  },
  "studyabstract",
  { data_type => "text", is_nullable => 0 },
  "background",
  { data_type => "text", is_nullable => 1 },
  "objectives",
  { data_type => "text", is_nullable => 1 },
  "keyresults",
  { data_type => "text", is_nullable => 1 },
  "conclusions",
  { data_type => "text", is_nullable => 1 },
  "studydesign",
  { data_type => "text", is_nullable => 1 },
  "studysizereason",
  { data_type => "text", is_nullable => 1 },
  "studypower",
  { data_type => "text", is_nullable => 1 },
  "sourcesofbias",
  { data_type => "text", is_nullable => 1 },
  "limitations",
  { data_type => "text", is_nullable => 1 },
  "acknowledgements",
  { data_type => "text", is_nullable => 1 },
  "timecreated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
  },
  "timeupdated",
  {
    data_type     => "timestamp",
    default_value => "0000-00-00 00:00:00",
    is_nullable   => 0,
  },
  "isdisplayed",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 1,
  },
  "significancelevel",
  { data_type => "smallint", default_value => 0, is_nullable => 1 },
  "accesslevel",
  { data_type => "varchar", is_nullable => 1, size => 10 },
);
__PACKAGE__->set_primary_key("studyid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 assayedpanels

Type: has_many

Related object: L<GwasCentral::Schema::Study::Assayedpanel>

=cut

__PACKAGE__->has_many(
  "assayedpanels",
  "GwasCentral::Schema::Study::Assayedpanel",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 authors

Type: has_many

Related object: L<GwasCentral::Schema::Study::Author>

=cut

__PACKAGE__->has_many(
  "authors",
  "GwasCentral::Schema::Study::Author",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 authorcommunications

Type: has_many

Related object: L<GwasCentral::Schema::Study::Authorcommunication>

=cut

__PACKAGE__->has_many(
  "authorcommunications",
  "GwasCentral::Schema::Study::Authorcommunication",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 experiments

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->has_many(
  "experiments",
  "GwasCentral::Schema::Study::Experiment",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 genotypedlocis

Type: has_many

Related object: L<GwasCentral::Schema::Study::Genotypedloci>

=cut

__PACKAGE__->has_many(
  "genotypedlocis",
  "GwasCentral::Schema::Study::Genotypedloci",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 phenotypemethods

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->has_many(
  "phenotypemethods",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 studyanalysismethods

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyanalysismethod>

=cut

__PACKAGE__->has_many(
  "studyanalysismethods",
  "GwasCentral::Schema::Study::Studyanalysismethod",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 studycitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studycitation>

=cut

__PACKAGE__->has_many(
  "studycitations",
  "GwasCentral::Schema::Study::Studycitation",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 studycrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::StudycrossrefOld>

=cut

__PACKAGE__->has_many(
  "studycrossref_olds",
  "GwasCentral::Schema::Study::StudycrossrefOld",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 studyhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyhotlink>

=cut

__PACKAGE__->has_many(
  "studyhotlinks",
  "GwasCentral::Schema::Study::Studyhotlink",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 studyoverlap_studyid_news

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyoverlap>

=cut

__PACKAGE__->has_many(
  "studyoverlap_studyid_news",
  "GwasCentral::Schema::Study::Studyoverlap",
  { "foreign.studyid_new" => "self.studyid" },
  {},
);

=head2 studyoverlap_studyid_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyoverlap>

=cut

__PACKAGE__->has_many(
  "studyoverlap_studyid_olds",
  "GwasCentral::Schema::Study::Studyoverlap",
  { "foreign.studyid_old" => "self.studyid" },
  {},
);

=head2 studysamplepanels

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studysamplepanel>

=cut

__PACKAGE__->has_many(
  "studysamplepanels",
  "GwasCentral::Schema::Study::Studysamplepanel",
  { "foreign.studyid" => "self.studyid" },
  {},
);

=head2 submissions

Type: has_many

Related object: L<GwasCentral::Schema::Study::Submission>

=cut

__PACKAGE__->has_many(
  "submissions",
  "GwasCentral::Schema::Study::Submission",
  { "foreign.studyid" => "self.studyid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 12:49:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TDvX/pVunFsvt6N+515Y8A


*submittedsamplepanels = \*samplepanels;
*submittedanalysismethods = \*analysismethods;
__PACKAGE__->many_to_many(usedsamplepanels => 'studysamplepanels', 'samplepanelid' );
__PACKAGE__->many_to_many(usedanalysismethods => 'studyanalysismethods', 'analysismethodid' );
__PACKAGE__->many_to_many(citations => 'studycitations', 'citationid' );
__PACKAGE__->many_to_many(hotlinks => 'studyhotlinks', 'hotlinkid' );

sub to_hash {
	my ($self) = @_;
	my %study = $self->get_columns;
	my @dc_exps = $self->experiments;
	my @exps=();
	foreach my $dc_exp (@dc_exps) {
		my %exp = $dc_exp->get_columns;
		delete $exp{studyid};
		
		my @resultsets=();
		foreach my $dc_rs($dc_exp->resultsets) {
			my %rs = $dc_rs->get_columns;
			delete $rs{experimentid};
			my %am = $dc_rs->analysismethodid->get_columns;
			$rs{analysismethod}=\%am;
			$rs{accesslevel}=$self->accesslevel;
			push @resultsets, \%rs;
			
		}
		$exp{resultset}=\@resultsets;
		
		push @exps, \%exp;
	}
	$study{experiments}=\@exps;
	$study{accesslevel}=$self->accesslevel;
	$study{child_accesslevel}=$self->child_accesslevel;
	$study{gcid}=$self->gcid;
	return \%study;
}

sub uri {
	my ($self,$value)=@_;
	
	if ($value) {
		$self->{_uri}=$value;
	}
	return $self->{_uri};
}

sub response_status {
	my ($self,$value)=@_;
	
	if ($value) {
		$self->{_response_status}=$value;
	}
	return $self->{_response_status};
}

sub gcid {
	my ($self,$value)=@_;
	
	if ($value) {
		$self->{_gcid}=$value;
	}
	return $self->{_gcid};
}

sub child_accesslevel {
	my ($self,$value)=@_;
	
	if ($value) {
		$self->{_child_accesslevel}=$value;
	}
	return $self->{_child_accesslevel};
}

sub accesslevel {
	my ($self,$value)=@_;
	
	if (defined($value)) {
		$self->{_accesslevel}=$value;
	}
	return $self->{_accesslevel};
}

sub number {
	my ($self) = @_;
	$self->identifier =~ /HGVST(\w+)/;
	return $1;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
