package GwasCentral::Schema::Study::Genotypedbundle;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Genotypedbundle

=cut

__PACKAGE__->table("GenotypedBundle");

=head1 ACCESSORS

=head2 genotypedbundleid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 bundlelociid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "genotypedbundleid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "bundlelociid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("genotypedbundleid");
__PACKAGE__->add_unique_constraint("BundleLociID_2", ["bundlelociid", "experimentid"]);

=head1 RELATIONS

=head2 experimentid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->belongs_to(
  "experimentid",
  "GwasCentral::Schema::Study::Experiment",
  { experimentid => "experimentid" },
);

=head2 bundlelociid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Bundleloci>

=cut

__PACKAGE__->belongs_to(
  "bundlelociid",
  "GwasCentral::Schema::Study::Bundleloci",
  { bundlelociid => "bundlelociid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vRdbKmjSwg5v0NejNyGmDA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
