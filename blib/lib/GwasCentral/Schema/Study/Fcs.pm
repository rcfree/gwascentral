package GwasCentral::Schema::Study::Fcs;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Fcs

=cut

__PACKAGE__->table("FCS");

=head1 ACCESSORS

=head2 fcsid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 significanceid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 frequencyclusterid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "fcsid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "significanceid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "frequencyclusterid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("fcsid");
__PACKAGE__->add_unique_constraint(
  "GenotypeSignificanceID",
  ["significanceid", "frequencyclusterid"],
);

=head1 RELATIONS

=head2 significanceid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Significance>

=cut

__PACKAGE__->belongs_to(
  "significanceid",
  "GwasCentral::Schema::Study::Significance",
  { significanceid => "significanceid" },
);

=head2 frequencyclusterid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Frequencycluster>

=cut

__PACKAGE__->belongs_to(
  "frequencyclusterid",
  "GwasCentral::Schema::Study::Frequencycluster",
  { frequencyclusterid => "frequencyclusterid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GnCRAIRYCjnMjgEYopjM6w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
