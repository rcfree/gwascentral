package GwasCentral::Schema::Study::PhenotypemethodcrossrefOld;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::PhenotypemethodcrossrefOld

=cut

__PACKAGE__->table("PhenotypeMethodCrossref_old");

=head1 ACCESSORS

=head2 phenotypemethodcrossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypemethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "phenotypemethodcrossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypemethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("phenotypemethodcrossrefid");
__PACKAGE__->add_unique_constraint("PhenotypeMethodID", ["phenotypemethodid", "crossrefid"]);

=head1 RELATIONS

=head2 phenotypemethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->belongs_to(
  "phenotypemethodid",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { phenotypemethodid => "phenotypemethodid" },
);

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::CrossrefOld>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Study::CrossrefOld",
  { crossrefid => "crossrefid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:r7tpOBTwg3f10OAtqv0zGw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
