package GwasCentral::Schema::Study::Citation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Citation

=cut

__PACKAGE__->table("Citation");

=head1 ACCESSORS

=head2 citationid

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 doi

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pubmedid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 authors

  data_type: 'text'
  is_nullable: 1

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 detail

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 meshterms

  data_type: 'text'
  is_nullable: 1

=head2 isprimary

  data_type: 'enum'
  default_value: 'no'
  extra: {list => ["yes","no"]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "citationid",
  {
    data_type => "smallint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "doi",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pubmedid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
  "authors",
  { data_type => "text", is_nullable => 1 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "detail",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "meshterms",
  { data_type => "text", is_nullable => 1 },
  "isprimary",
  {
    data_type => "enum",
    default_value => "no",
    extra => { list => ["yes", "no"] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("citationid");
__PACKAGE__->add_unique_constraint("PubmedID", ["pubmedid"]);
__PACKAGE__->add_unique_constraint("DOI", ["doi"]);

=head1 RELATIONS

=head2 citationcrossref_olds

Type: has_many

Related object: L<GwasCentral::Schema::Study::CitationcrossrefOld>

=cut

__PACKAGE__->has_many(
  "citationcrossref_olds",
  "GwasCentral::Schema::Study::CitationcrossrefOld",
  { "foreign.citationid" => "self.citationid" },
  {},
);

=head2 phenotypemethodcitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethodcitation>

=cut

__PACKAGE__->has_many(
  "phenotypemethodcitations",
  "GwasCentral::Schema::Study::Phenotypemethodcitation",
  { "foreign.citationid" => "self.citationid" },
  {},
);

=head2 phenotypepropertycitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypepropertycitation>

=cut

__PACKAGE__->has_many(
  "phenotypepropertycitations",
  "GwasCentral::Schema::Study::Phenotypepropertycitation",
  { "foreign.citationid" => "self.citationid" },
  {},
);

=head2 studycitations

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studycitation>

=cut

__PACKAGE__->has_many(
  "studycitations",
  "GwasCentral::Schema::Study::Studycitation",
  { "foreign.citationid" => "self.citationid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KmTy07JXGtyCzO4s0VGp9Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
