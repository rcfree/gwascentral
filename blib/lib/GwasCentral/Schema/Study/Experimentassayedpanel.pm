package GwasCentral::Schema::Study::Experimentassayedpanel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Experimentassayedpanel

=cut

__PACKAGE__->table("ExperimentAssayedpanel");

=head1 ACCESSORS

=head2 experimentassayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 assayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "experimentassayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "assayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("experimentassayedpanelid");
__PACKAGE__->add_unique_constraint("ExperimentID", ["experimentid", "assayedpanelid"]);

=head1 RELATIONS

=head2 assayedpanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Assayedpanel>

=cut

__PACKAGE__->belongs_to(
  "assayedpanelid",
  "GwasCentral::Schema::Study::Assayedpanel",
  { assayedpanelid => "assayedpanelid" },
);

=head2 experimentid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->belongs_to(
  "experimentid",
  "GwasCentral::Schema::Study::Experiment",
  { experimentid => "experimentid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oB1ur5uA0bn5LAhtN1wxdg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
