package GwasCentral::Schema::Study::Researcher;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Researcher

=cut

__PACKAGE__->table("Researcher");

=head1 ACCESSORS

=head2 researcherid

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 submitterhandle

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 shortname

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 fullname

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 department

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 institution

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 address

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 phone

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 fax

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 www

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "researcherid",
  {
    data_type => "smallint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "submitterhandle",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "shortname",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "fullname",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "department",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "institution",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "address",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "phone",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "fax",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "www",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("researcherid");
__PACKAGE__->add_unique_constraint("SubmitterHandle", ["submitterhandle"]);

=head1 RELATIONS

=head2 authors

Type: has_many

Related object: L<GwasCentral::Schema::Study::Author>

=cut

__PACKAGE__->has_many(
  "authors",
  "GwasCentral::Schema::Study::Author",
  { "foreign.researcherid" => "self.researcherid" },
  {},
);

=head2 contributions

Type: has_many

Related object: L<GwasCentral::Schema::Study::Contribution>

=cut

__PACKAGE__->has_many(
  "contributions",
  "GwasCentral::Schema::Study::Contribution",
  { "foreign.researcherid" => "self.researcherid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:HvUYeovikHVYxNVQiRZVrg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
