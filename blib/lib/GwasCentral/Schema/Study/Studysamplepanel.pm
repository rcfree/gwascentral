package GwasCentral::Schema::Study::Studysamplepanel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Studysamplepanel

=cut

__PACKAGE__->table("StudySamplepanel");

=head1 ACCESSORS

=head2 studysamplepanel

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 samplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "studysamplepanel",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "samplepanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("studysamplepanel");
__PACKAGE__->add_unique_constraint("StudyID", ["studyid", "samplepanelid"]);

=head1 RELATIONS

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);

=head2 samplepanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EPZZ/BFQUsl3+jzvxdPrCQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
