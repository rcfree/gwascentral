package GwasCentral::Schema::Study::Samplepanelcrossref;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Samplepanelcrossref

=cut

__PACKAGE__->table("SamplepanelCrossref");

=head1 ACCESSORS

=head2 samplepanelcrossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 samplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "samplepanelcrossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "samplepanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("samplepanelcrossrefid");
__PACKAGE__->add_unique_constraint("SamplepanelID", ["samplepanelid", "crossrefid"]);

=head1 RELATIONS

=head2 samplepanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid" },
);

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::CrossrefOld>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Study::CrossrefOld",
  { crossrefid => "crossrefid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zdynJ2EJqIUNTSfyZvh4Rg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
