package GwasCentral::Schema::Study::Hotlinkcollection;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Hotlinkcollection

=cut

__PACKAGE__->table("HotlinkCollection");

=head1 ACCESSORS

=head2 hotlinkcollectionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 studyid

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "hotlinkcollectionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "studyid",
  { data_type => "integer", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("hotlinkcollectionid");

=head1 RELATIONS

=head2 hotlinkers

Type: has_many

Related object: L<GwasCentral::Schema::Study::Hotlinker>

=cut

__PACKAGE__->has_many(
  "hotlinkers",
  "GwasCentral::Schema::Study::Hotlinker",
  { "foreign.hotlinkcollectionid" => "self.hotlinkcollectionid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vnYQum4pK/DHpbmUw2Id1w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
