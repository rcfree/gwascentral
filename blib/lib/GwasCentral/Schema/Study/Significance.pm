package GwasCentral::Schema::Study::Significance;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Significance

=cut

__PACKAGE__->table("Significance");

=head1 ACCESSORS

=head2 significanceid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 resultsetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 usedmarkersetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 hotlinkcollectionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 adjustedpvalue

  data_type: 'float'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 unadjustedpvalue

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 oddsratiostatement

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 attributableriskstatement

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 islimited

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 1

=head2 sourcedetails

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "significanceid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "resultsetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "usedmarkersetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "hotlinkcollectionid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
  "adjustedpvalue",
  { data_type => "float", extra => { unsigned => 1 }, is_nullable => 1 },
  "unadjustedpvalue",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "oddsratiostatement",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "attributableriskstatement",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "islimited",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 1,
  },
  "sourcedetails",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("significanceid");

=head1 RELATIONS

=head2 fcs

Type: has_many

Related object: L<GwasCentral::Schema::Study::Fcs>

=cut

__PACKAGE__->has_many(
  "fcs",
  "GwasCentral::Schema::Study::Fcs",
  { "foreign.significanceid" => "self.significanceid" },
  {},
);

=head2 usedmarkersetid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Usedmarkerset>

=cut

__PACKAGE__->belongs_to(
  "usedmarkersetid",
  "GwasCentral::Schema::Study::Usedmarkerset",
  { usedmarkersetid => "usedmarkersetid" },
);

=head2 resultsetid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Resultset>

=cut

__PACKAGE__->belongs_to(
  "resultsetid",
  "GwasCentral::Schema::Study::Resultset",
  { resultsetid => "resultsetid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gbHSxu1H/z1pXFoz2MPa2Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
