package GwasCentral::Schema::Study::Studyanalysismethod;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Studyanalysismethod

=cut

__PACKAGE__->table("StudyAnalysisMethod");

=head1 ACCESSORS

=head2 studyanalysismethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 analysismethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "studyanalysismethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "analysismethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("studyanalysismethodid");
__PACKAGE__->add_unique_constraint("StudyID", ["studyid", "analysismethodid"]);

=head1 RELATIONS

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);

=head2 analysismethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Analysismethod>

=cut

__PACKAGE__->belongs_to(
  "analysismethodid",
  "GwasCentral::Schema::Study::Analysismethod",
  { analysismethodid => "analysismethodid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Gsfd43ArPPi02dc6Jtvgqw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
