package GwasCentral::Schema::Study::Studyoverlap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Studyoverlap

=cut

__PACKAGE__->table("StudyOverlap");

=head1 ACCESSORS

=head2 studyoverlapid

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 studyid_new

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 studyid_old

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 overlapdetails

  data_type: 'varchar'
  default_value: 'This Study overlaps with another'
  is_nullable: 0
  size: 50

=cut

__PACKAGE__->add_columns(
  "studyoverlapid",
  { data_type => "smallint", extra => { unsigned => 1 }, is_nullable => 0 },
  "studyid_new",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "studyid_old",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "overlapdetails",
  {
    data_type => "varchar",
    default_value => "This Study overlaps with another",
    is_nullable => 0,
    size => 50,
  },
);
__PACKAGE__->set_primary_key("studyoverlapid");
__PACKAGE__->add_unique_constraint("StudyID_New", ["studyid_new", "studyid_old"]);

=head1 RELATIONS

=head2 studyid_new

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid_new",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid_new" },
);

=head2 studyid_old

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid_old",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid_old" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1oRzUzcDTaVIl2SqS3/8BA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
