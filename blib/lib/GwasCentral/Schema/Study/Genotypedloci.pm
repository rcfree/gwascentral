package GwasCentral::Schema::Study::Genotypedloci;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Genotypedloci

=cut

__PACKAGE__->table("GenotypedLoci");

=head1 ACCESSORS

=head2 genotypedlociid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 genotypedloci

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "genotypedlociid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "genotypedloci",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("genotypedlociid");

=head1 RELATIONS

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:35A89f5ni50ziff77Inzcw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
