package GwasCentral::Schema::Study::Pppa;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Pppa

=cut

__PACKAGE__->table("PPPA");

=head1 ACCESSORS

=head2 pppaid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypepropertyid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 phenotypeannotationid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "pppaid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypepropertyid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "phenotypeannotationid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("pppaid");

=head1 RELATIONS

=head2 phenotypepropertyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypeproperty>

=cut

__PACKAGE__->belongs_to(
  "phenotypepropertyid",
  "GwasCentral::Schema::Study::Phenotypeproperty",
  { phenotypepropertyid => "phenotypepropertyid" },
);

=head2 phenotypeannotationid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypeannotation>

=cut

__PACKAGE__->belongs_to(
  "phenotypeannotationid",
  "GwasCentral::Schema::Study::Phenotypeannotation",
  { "phenotypeannotationid" => "phenotypeannotationid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 12:49:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bInrpNsyXjSijLGISO8aqA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
