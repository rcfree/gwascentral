package GwasCentral::Schema::Study::StudyOld;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::StudyOld

=cut

__PACKAGE__->table("Study_old");

=head1 ACCESSORS

=head2 studyid

  data_type: 'mediumint'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 ishidden

  data_type: 'char'
  is_nullable: 1
  size: 5

=head2 researcherid

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 abstract

  data_type: 'text'
  is_nullable: 0

=head2 background

  data_type: 'text'
  is_nullable: 1

=head2 objectives

  data_type: 'text'
  is_nullable: 0

=head2 keyresults

  data_type: 'text'
  is_nullable: 0

=head2 conclusions

  data_type: 'text'
  is_nullable: 0

=head2 studydesign

  data_type: 'text'
  is_nullable: 1

=head2 studysizereason

  data_type: 'text'
  is_nullable: 1

=head2 studypower

  data_type: 'text'
  is_nullable: 1

=head2 sourcesofbias

  data_type: 'text'
  is_nullable: 1

=head2 limitations

  data_type: 'text'
  is_nullable: 1

=head2 acknowledgements

  data_type: 'text'
  is_nullable: 1

=head2 timecreated

  data_type: 'timestamp'
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 timeupdated

  data_type: 'timestamp'
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 isdisplayed

  data_type: 'char'
  is_nullable: 1
  size: 5

=cut

__PACKAGE__->add_columns(
  "studyid",
  {
    data_type => "mediumint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "ishidden",
  { data_type => "char", is_nullable => 1, size => 5 },
  "researcherid",
  { data_type => "smallint", extra => { unsigned => 1 }, is_nullable => 1 },
  "abstract",
  { data_type => "text", is_nullable => 0 },
  "background",
  { data_type => "text", is_nullable => 1 },
  "objectives",
  { data_type => "text", is_nullable => 0 },
  "keyresults",
  { data_type => "text", is_nullable => 0 },
  "conclusions",
  { data_type => "text", is_nullable => 0 },
  "studydesign",
  { data_type => "text", is_nullable => 1 },
  "studysizereason",
  { data_type => "text", is_nullable => 1 },
  "studypower",
  { data_type => "text", is_nullable => 1 },
  "sourcesofbias",
  { data_type => "text", is_nullable => 1 },
  "limitations",
  { data_type => "text", is_nullable => 1 },
  "acknowledgements",
  { data_type => "text", is_nullable => 1 },
  "timecreated",
  {
    data_type     => "timestamp",
    default_value => "0000-00-00 00:00:00",
    is_nullable   => 0,
  },
  "timeupdated",
  {
    data_type     => "timestamp",
    default_value => "0000-00-00 00:00:00",
    is_nullable   => 0,
  },
  "isdisplayed",
  { data_type => "char", is_nullable => 1, size => 5 },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 12:49:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xwtKmksNf/fQ4VQtvHJcGQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
