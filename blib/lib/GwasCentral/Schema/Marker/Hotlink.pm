package GwasCentral::Schema::Marker::Hotlink;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Hotlink

=cut

__PACKAGE__->table("Hotlink");

=head1 ACCESSORS

=head2 hotlinkid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 hotlinklabel

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 urlprefix

  data_type: 'varchar'
  is_nullable: 0
  size: 300

=head2 urlsuffix

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "hotlinkid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "hotlinklabel",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "urlprefix",
  { data_type => "varchar", is_nullable => 0, size => 300 },
  "urlsuffix",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("hotlinkid");
__PACKAGE__->add_unique_constraint("HotlinkLabel", ["hotlinklabel"]);

=head1 RELATIONS

=head2 crossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Crossref>

=cut

__PACKAGE__->has_many(
  "crossrefs",
  "GwasCentral::Schema::Marker::Crossref",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);

=head2 markers

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->has_many(
  "markers",
  "GwasCentral::Schema::Marker::Marker",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ze2RabnkD56LV/ZnAs2nnw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
