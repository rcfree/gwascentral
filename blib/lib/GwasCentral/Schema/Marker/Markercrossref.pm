package GwasCentral::Schema::Marker::Markercrossref;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Markercrossref

=cut

__PACKAGE__->table("MarkerCrossref");

=head1 ACCESSORS

=head2 markercrossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "markercrossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("markercrossrefid");
__PACKAGE__->add_unique_constraint("MarkerID", ["markerid", "crossrefid"]);

=head1 RELATIONS

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Crossref>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Marker::Crossref",
  { crossrefid => "crossrefid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cBQ4bfdN3UDN8Ur9ubcfVA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
