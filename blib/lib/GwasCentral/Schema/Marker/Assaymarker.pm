package GwasCentral::Schema::Marker::Assaymarker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Assaymarker

=cut

__PACKAGE__->table("AssayMarker");

=head1 ACCESSORS

=head2 assaymarkerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 assayid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "assaymarkerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "assayid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("assaymarkerid");

=head1 RELATIONS

=head2 assaymarkergenotypes

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Assaymarkergenotype>

=cut

__PACKAGE__->has_many(
  "assaymarkergenotypes",
  "GwasCentral::Schema::Marker::Assaymarkergenotype",
  { "foreign.assaymarkerid" => "self.assaymarkerid" },
  {},
);

=head2 assayedgenotypes

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Assayedgenotype>

=cut

__PACKAGE__->has_many(
  "assayedgenotypes",
  "GwasCentral::Schema::Marker::Assayedgenotype",
  { "foreign.assaymarkerid" => "self.assaymarkerid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1KM8MN7dzwmP8LZLT3WFzQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
