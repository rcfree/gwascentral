package GwasCentral::Schema::Marker::Build;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Build

=cut

__PACKAGE__->table("Build");

=head1 ACCESSORS

=head2 buildid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 database

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 fullname

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 build

  data_type: 'varchar'
  default_value: 'N/A'
  is_nullable: 0
  size: 10

=head2 date

  data_type: 'date'
  datetime_undef_if_invalid: 1
  default_value: '0000-00-00'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "buildid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "database",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "fullname",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "build",
  {
    data_type => "varchar",
    default_value => "N/A",
    is_nullable => 0,
    size => 10,
  },
  "date",
  {
    data_type => "date",
    datetime_undef_if_invalid => 1,
    default_value => "0000-00-00",
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("buildid");
__PACKAGE__->add_unique_constraint("Database", ["database"]);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EMUeIPq3WkXN/9Y6EKp2lg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
