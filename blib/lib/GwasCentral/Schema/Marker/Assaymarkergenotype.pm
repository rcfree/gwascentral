package GwasCentral::Schema::Marker::Assaymarkergenotype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Assaymarkergenotype

=cut

__PACKAGE__->table("AssayMarkerGenotype");

=head1 ACCESSORS

=head2 assaymarkergenotypeid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 assaymarkerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 genotypeid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "assaymarkergenotypeid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "assaymarkerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "genotypeid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("assaymarkergenotypeid");
__PACKAGE__->add_unique_constraint("AssayMarkerID", ["assaymarkerid", "genotypeid"]);

=head1 RELATIONS

=head2 assaymarkerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Assaymarker>

=cut

__PACKAGE__->belongs_to(
  "assaymarkerid",
  "GwasCentral::Schema::Marker::Assaymarker",
  { assaymarkerid => "assaymarkerid" },
);

=head2 genotypeid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Genotype>

=cut

__PACKAGE__->belongs_to(
  "genotypeid",
  "GwasCentral::Schema::Marker::Genotype",
  { genotypeid => "genotypeid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fKBgS5BXOH2P07nJ8iU15A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
