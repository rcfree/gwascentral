package GwasCentral::Schema::Marker::Haplotypeallele;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Haplotypeallele

=cut

__PACKAGE__->table("HaplotypeAllele");

=head1 ACCESSORS

=head2 haplotypealleleid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 haplotypeid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 alleleid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 pqorder

  data_type: 'tinyint'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "haplotypealleleid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "haplotypeid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "alleleid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "pqorder",
  {
    data_type => "tinyint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("haplotypealleleid");
__PACKAGE__->add_unique_constraint("HaplotypeID", ["haplotypeid", "alleleid"]);

=head1 RELATIONS

=head2 haplotypeid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Haplotype>

=cut

__PACKAGE__->belongs_to(
  "haplotypeid",
  "GwasCentral::Schema::Marker::Haplotype",
  { haplotypeid => "haplotypeid" },
);

=head2 alleleid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Allele>

=cut

__PACKAGE__->belongs_to(
  "alleleid",
  "GwasCentral::Schema::Marker::Allele",
  { alleleid => "alleleid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:F42pa46/WiYkLn7Rss2ZGA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
