package GwasCentral::Schema::Marker::Allele;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Allele

=cut

__PACKAGE__->table("Allele");

=head1 ACCESSORS

=head2 alleleid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 status

  data_type: 'varchar'
  default_value: 'active'
  is_nullable: 0
  size: 20

=head2 markerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 upstream30bp

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 30

=head2 alleleseq

  data_type: 'text'
  is_nullable: 0

=head2 alleleseqdigest

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 downstream30bp

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 30

=head2 seqtype

  data_type: 'enum'
  default_value: 'G'
  extra: {list => ["G","C"]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "alleleid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "status",
  {
    data_type => "varchar",
    default_value => "active",
    is_nullable => 0,
    size => 20,
  },
  "markerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "upstream30bp",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 30 },
  "alleleseq",
  { data_type => "text", is_nullable => 0 },
  "alleleseqdigest",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "downstream30bp",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 30 },
  "seqtype",
  {
    data_type => "enum",
    default_value => "G",
    extra => { list => ["G", "C"] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("alleleid");
__PACKAGE__->add_unique_constraint("MarkerID", ["markerid", "alleleseqdigest"]);

=head1 RELATIONS

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);

=head2 haplotypealleles

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Haplotypeallele>

=cut

__PACKAGE__->has_many(
  "haplotypealleles",
  "GwasCentral::Schema::Marker::Haplotypeallele",
  { "foreign.alleleid" => "self.alleleid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xyHMCmZMyW+bnfgx7IjbgQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
