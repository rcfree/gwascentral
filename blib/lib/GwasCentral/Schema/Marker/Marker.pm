package GwasCentral::Schema::Marker::Marker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Marker

=cut

__PACKAGE__->table("Marker");

=head1 ACCESSORS

=head2 markerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 datasourceid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 status

  data_type: 'varchar'
  default_value: 'active'
  is_nullable: 0
  size: 20

=head2 variationtype

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 hotlinkid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 upstream30bp

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 alleleseqsshorthand

  data_type: 'text'
  is_nullable: 1

=head2 alleleseqs

  data_type: 'text'
  is_nullable: 1

=head2 downstream30bp

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 validationcode

  data_type: 'text'
  is_nullable: 1

=head2 addedfromsourcebuild

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 timelasttouched

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "markerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "datasourceid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "status",
  {
    data_type => "varchar",
    default_value => "active",
    is_nullable => 0,
    size => 20,
  },
  "variationtype",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "hotlinkid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "upstream30bp",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "alleleseqsshorthand",
  { data_type => "text", is_nullable => 1 },
  "alleleseqs",
  { data_type => "text", is_nullable => 1 },
  "downstream30bp",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "validationcode",
  { data_type => "text", is_nullable => 1 },
  "addedfromsourcebuild",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "timelasttouched",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("markerid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);
__PACKAGE__->add_unique_constraint("Accession", ["accession", "datasourceid"]);

=head1 RELATIONS

=head2 alleles

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Allele>

=cut

__PACKAGE__->has_many(
  "alleles",
  "GwasCentral::Schema::Marker::Allele",
  { "foreign.markerid" => "self.markerid" },
  {},
);

=head2 genotypes

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Genotype>

=cut

__PACKAGE__->has_many(
  "genotypes",
  "GwasCentral::Schema::Marker::Genotype",
  { "foreign.markerid" => "self.markerid" },
  {},
);

=head2 datasourceid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Datasource>

=cut

__PACKAGE__->belongs_to(
  "datasourceid",
  "GwasCentral::Schema::Marker::Datasource",
  { datasourceid => "datasourceid" },
);

=head2 hotlinkid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Hotlink>

=cut

__PACKAGE__->belongs_to(
  "hotlinkid",
  "GwasCentral::Schema::Marker::Hotlink",
  { hotlinkid => "hotlinkid" },
);

=head2 markercoords

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Markercoord>

=cut

__PACKAGE__->has_many(
  "markercoords",
  "GwasCentral::Schema::Marker::Markercoord",
  { "foreign.markerid" => "self.markerid" },
  {},
);

=head2 markercrossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Markercrossref>

=cut

__PACKAGE__->has_many(
  "markercrossrefs",
  "GwasCentral::Schema::Marker::Markercrossref",
  { "foreign.markerid" => "self.markerid" },
  {},
);

=head2 markerrevision_markerids

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Markerrevision>

=cut

__PACKAGE__->has_many(
  "markerrevision_markerids",
  "GwasCentral::Schema::Marker::Markerrevision",
  { "foreign.markerid" => "self.markerid" },
  {},
);

=head2 markerrevision_replacedbymarkerids

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Markerrevision>

=cut

__PACKAGE__->has_many(
  "markerrevision_replacedbymarkerids",
  "GwasCentral::Schema::Marker::Markerrevision",
  { "foreign.replacedbymarkerid" => "self.markerid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:sy/4VjhpQ6a/KIlEIPcOwg

sub to_hash {
	my ($self) = @_;
	my %marker = $self->get_columns;
	my @coords=();
	foreach($self->markercoords) {
		my %coord = $_->get_columns;
		delete $coord{markerid};
		push @coords, \%coord;
	}
	
	my @alleles=();
	foreach($self->alleles) {
		my %allele = $_->get_columns;
		delete $allele{markerid};
		push @alleles, \%allele;
	}
	
	my @gts=();
	foreach($self->genotypes) {
		my %gt = $_->get_columns;
		delete $gt{markerid};
		push @gts, \%gt;
	}
	
	$marker{markercoord}=\@coords;
	$marker{allele}=\@alleles;
	$marker{genotype}=\@gts;
	return \%marker;
}

sub downstream10bp {
	my ($self) = @_;
	my $ds30 = $self->downstream30bp;
	return substr($ds30,0,10);
}

sub upstream10bp {
	my ($self) = @_;
	my $us30 = $self->upstream30bp;
	return substr($us30,20);
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
