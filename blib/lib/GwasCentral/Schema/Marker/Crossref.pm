package GwasCentral::Schema::Marker::Crossref;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Crossref

=cut

__PACKAGE__->table("Crossref");

=head1 ACCESSORS

=head2 crossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 hotlinkid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 urlid

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "crossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "hotlinkid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "urlid",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("crossrefid");
__PACKAGE__->add_unique_constraint("HotlinkID", ["hotlinkid", "urlid"]);

=head1 RELATIONS

=head2 hotlinkid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Hotlink>

=cut

__PACKAGE__->belongs_to(
  "hotlinkid",
  "GwasCentral::Schema::Marker::Hotlink",
  { hotlinkid => "hotlinkid" },
);

=head2 haplotypecrossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Haplotypecrossref>

=cut

__PACKAGE__->has_many(
  "haplotypecrossrefs",
  "GwasCentral::Schema::Marker::Haplotypecrossref",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);

=head2 markercrossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Markercrossref>

=cut

__PACKAGE__->has_many(
  "markercrossrefs",
  "GwasCentral::Schema::Marker::Markercrossref",
  { "foreign.crossrefid" => "self.crossrefid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nRyyyvk6Makdw9ESP+OSqw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
