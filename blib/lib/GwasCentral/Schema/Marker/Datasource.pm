package GwasCentral::Schema::Marker::Datasource;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Datasource

=cut

__PACKAGE__->table("DataSource");

=head1 ACCESSORS

=head2 datasourceid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 uri

  data_type: 'varchar'
  is_nullable: 1
  size: 300

=cut

__PACKAGE__->add_columns(
  "datasourceid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "uri",
  { data_type => "varchar", is_nullable => 1, size => 300 },
);
__PACKAGE__->set_primary_key("datasourceid");
__PACKAGE__->add_unique_constraint("URI", ["uri"]);
__PACKAGE__->add_unique_constraint("Name", ["name"]);

=head1 RELATIONS

=head2 markers

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->has_many(
  "markers",
  "GwasCentral::Schema::Marker::Marker",
  { "foreign.datasourceid" => "self.datasourceid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jriama/FCADWVbJddiyS1g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
