package GwasCentral::Schema::Marker::Genotype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Genotype

=cut

__PACKAGE__->table("Genotype");

=head1 ACCESSORS

=head2 genotypeid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 genotypelabel

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "genotypeid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "genotypelabel",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);
__PACKAGE__->set_primary_key("genotypeid");
__PACKAGE__->add_unique_constraint("MarkerID_2", ["markerid", "genotypelabel"]);

=head1 RELATIONS

=head2 assaymarkergenotypes

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Assaymarkergenotype>

=cut

__PACKAGE__->has_many(
  "assaymarkergenotypes",
  "GwasCentral::Schema::Marker::Assaymarkergenotype",
  { "foreign.genotypeid" => "self.genotypeid" },
  {},
);

=head2 assayedgenotypes

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Assayedgenotype>

=cut

__PACKAGE__->has_many(
  "assayedgenotypes",
  "GwasCentral::Schema::Marker::Assayedgenotype",
  { "foreign.genotypeid" => "self.genotypeid" },
  {},
);

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JPRQQAtVMCb/yYOj1HIUZA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
