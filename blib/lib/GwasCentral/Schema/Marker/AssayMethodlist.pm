package GwasCentral::Schema::Marker::AssayMethodlist;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::AssayMethodlist

=cut

__PACKAGE__->table("Assay_MethodList");

=head1 ACCESSORS

=head2 method

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 250

=cut

__PACKAGE__->add_columns(
  "method",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 250 },
);
__PACKAGE__->set_primary_key("method");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RTMHPNt+zJWueFSsApVUbg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
