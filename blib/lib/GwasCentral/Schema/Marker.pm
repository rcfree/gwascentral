package GwasCentral::Schema::Marker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7IWVmVxSC1/kWGWrZSb4Cg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
