package GwasCentral::Schema::Ontology::HpoTermPath;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::HpoTermPath

=cut

__PACKAGE__->table("hpo_term_path");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 ancestor_hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "ancestor_hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:DyRTZX33h0sOBa4nJqA20A

__PACKAGE__->belongs_to(hpo_term => 'GwasCentral::Schema::Ontology::HpoTerm', {'hpoid' => 'hpoid'} );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
