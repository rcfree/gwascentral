package GwasCentral::Schema::Ontology::HpoTerm;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::HpoTerm

=cut

__PACKAGE__->table("hpo_term");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 300

=head2 is_obsolete

  data_type: 'enum'
  default_value: 0
  extra: {list => [0,1]}
  is_nullable: 0

=head2 hgvbaseuse

  data_type: 'enum'
  default_value: 0
  extra: {list => [0,1]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 300 },
  "is_obsolete",
  {
    data_type => "enum",
    default_value => 0,
    extra => { list => [0, 1] },
    is_nullable => 0,
  },
  "hgvbaseuse",
  {
    data_type => "enum",
    default_value => 0,
    extra => { list => [0, 1] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8NWTA4+Jvy7zu9QrhYCM9A

__PACKAGE__->belongs_to(hpo2mesh => 'GwasCentral::Schema::Ontology::Hpo2mesh', {'hpoid' => 'hpoid'} );
 __PACKAGE__->belongs_to(hpo_synonym => 'GwasCentral::Schema::Ontology::HpoSynonym', {'hpoid' => 'hpoid'} );
 
# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
