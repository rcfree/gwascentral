package GwasCentral::Schema::Ontology::HpoVersion;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::HpoVersion

=cut

__PACKAGE__->table("hpo_version");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 releaseid

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 retired

  data_type: 'enum'
  default_value: 'no'
  extra: {list => ["no","yes"]}
  is_nullable: 0

=head2 retiredid

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 retireddate

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "releaseid",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "retired",
  {
    data_type => "enum",
    default_value => "no",
    extra => { list => ["no", "yes"] },
    is_nullable => 0,
  },
  "retiredid",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "retireddate",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pUMwjyWsSzCDEb/uCjmGcg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
