package GwasCentral::Schema::Ontology::Hpo2uml;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::Hpo2uml

=cut

__PACKAGE__->table("hpo2umls");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 umlsid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "umlsid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kxFuvuISIG8FGpp3SI7n6Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
