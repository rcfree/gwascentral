package GwasCentral::Schema::Ontology::MeshTermtree;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::MeshTermtree

=cut

__PACKAGE__->table("mesh_termTree");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 meshid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 treeid

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "meshid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "treeid",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Is5wpeuwifRLdqEiRHejmA

__PACKAGE__->belongs_to(mesh_heading => 'GwasCentral::Schema::Ontology::MeshHeading', {'meshid' => 'meshid'} );
__PACKAGE__->belongs_to(mesh_concept => 'GwasCentral::Schema::Ontology::MeshConcept', {'meshid' => 'meshid'} );
__PACKAGE__->belongs_to(mesh_term => 'GwasCentral::Schema::Ontology::MeshTerm', {'meshid' => 'meshid'} );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
