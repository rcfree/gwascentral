package GwasCentral::Test::MockDB;
use Moose;
#use GwasCentral::Browser::Creator;
#use GwasCentral::XMLStore;
use FindBin;
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);
has 'AccessControl' => ('is'=>'rw');

sub BUILD {
	my ($self) = @_;
	$self->populate_DS_from_config($self->config);
}

sub create_study_db {
	my ($self) = @_;
	my $study_ds = $self->DS('Study');

	$study_ds->drop_database;
	$study_ds->create_database;
	$study_ds->build_schema;
}

sub populate_study_db {
	my ($self) = @_;
	$self->xml_to_study_db("$FindBin::Bin/../../t/data/study/HGVST1.xml");

}

sub xml_to_study_db {
	my ($self,$xml_file) = @_;
	my $study_ds = $self->DS('Study');
	my $xmldb;
	eval {
	    $xmldb = GwasCentral::XMLStore->new({
	    	conf_file => $self->conf_file,
			dsn       => $study_ds->dsn,
			user      => $study_ds->user, 
			passw     => $study_ds->pass,
	})
	};
	if($@) {
	    $self->log->fatal("Caught exception: $@");
	}

	# Attempt to store XML-file
	eval {
	    $xmldb->store_file($xml_file);
	};
	# Catch exception and only commit if either no errors or -force x
	if($@) {
		$self->throw("Encountered errors during load so NOT committing transaction! $@");
	}
	else {
		$self->log->info("No errors encountered during load, committing clean transaction");
		$xmldb->dbstag->commit();
	}
}

sub create_marker_db {
	my ($self) = @_;
	my $marker_ds = $self->DS('Marker');
	
	$marker_ds->drop_database;
	$marker_ds->create_database;
	$marker_ds->build_schema;
	
	my $m1 = $self->create_marker("1","active");
	my $m2 = $self->create_marker("2","dead");
	my $m3 = $self->create_marker("3","dead");
	my $m4 = $self->create_marker("4","active");
	my $m5 = $self->create_marker("5","dead");
	my $m6 = $self->create_marker("6","dead");
	my $m7 = $self->create_marker("7","active");
	my $m8 = $self->create_marker("8","dead");
	my $m9 = $self->create_marker("9","dead");
	my $m10 = $self->create_marker("10","active");
	my $m11 = $self->create_marker("11","dead");
	my $m12 = $self->create_marker("12","dead");
	my $m13 = $self->create_marker("13","dead");
	my $m14 = $self->create_marker("14","dead");
	
	$m2->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m3->id,
	});
	sleep 1;
	$m2->create_related('markerrevision_markerids',{
		changetype => 'StrandFlip'
	});
	sleep 1;
	$m2->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m4->id,
	});
	$m5->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m6->id,
	});
	sleep 1;
	$m5->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m4->id,
	});
	sleep 1;
	$m6->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m7->id,
	});
	sleep 1;
	$m7->create_related('markerrevision_markerids',{
		changetype => 'StrandFlip'
	});
	sleep 1;
	$m8->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m9->id,
	});
	sleep 1;
	$m9->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m10->id,
	});
	$m11->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m10->id,
	});
	$m12->create_related('markerrevision_markerids',{
		replacedbymarkerid => $m13->id,
	});
	
my $mrs = $marker_ds->dbh->resultset('Marker');

my $m = $mrs->create({
	identifier => "HGVM999",
	accession => "rs1111",
	variationtype => 'SNP',
	status => "active",
	upstream30bp => "CATCTGGAGGATAAAGAGATCCCTCTGGGG",
	alleleseqsshorthand => "(C):(A)",
	downstream30bp => "CTGTGTCAGGGCAGGACTGACTCAAGGCCT",
	validationcode => "byFrequency,byCluster,byHapMap",
	
});

my $mappedgene = $self->DS('Feature')->get_mapped_gene("chr8",3792876,3792876);

$m->create_related('markercoords',{
	chr => "8",
	start => "3792876",
	stop => "3792876",
	span => "\@",
	strand => "+",
	assemblyname => "reference",
	assemblytype => "ref_assembly",
	genomebuild => "36.3",
	mappedgene => $mappedgene,
});

$m->create_related('markercoords',{
	chr => "8",
	start => "5467561",
	stop => "5467561",
	span => "\@",
	strand => "+",
	assemblyname => "GRhg19",
	assemblytype => "ref_assembly",
	genomebuild => "37.1",
});

$m->create_related('markercoords',{
	chr => "8",
	start => "5467560",
	stop => "5467560",
	span => "\@",
	strand => "+",
	assemblyname => "celera",
	assemblytype => "celera",
	genomebuild => "37.1",
});

	
}

sub populate_browser_markers {
	my ($self) = @_;
	my $browser_ds = $self->DS('Browser');
	$browser_ds->drop_database;
	$browser_ds->create_database;
	
	my $bc = GwasCentral::Browser::Creator->new({
		populate => 1,
		init => 1,
		conf_file => $self->config
	});
	$bc->create;
}

sub create_marker {
	my ($self, $no,$status) = @_;
	my $marker_ds = $self->DS('Marker');
	my $mrs = $marker_ds->dbh->resultset('Marker');

	my $m = $mrs->create({
		identifier => "HGVM$no",
		accession => "rs50$no",
		variationtype => 'SNP',
		status => $status,
	});
	$m->create_related('markercoords',{
		chr => "$no",
		start => "$no"."000000",
		stop => "$no"."000000",
		span => "\@",
		strand => "+",
		assemblyname => $self->config->{assembly_name},
		assemblytype => "ref_assembly",
		genomebuild => "36.3",
	});
	return $m;
}
1;