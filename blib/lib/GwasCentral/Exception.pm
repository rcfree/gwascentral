package GwasCentral::Exception;

use Exception::Class (
	  GwasCentral::Exception => {
	  	 fields => ['message'],
	  },
      GwasCentral::Exception::DataSource => { 
      	  isa         => 'GwasCentral::Exception',
          description => 'database' },

      GwasCentral::Exception::Search => {
          isa         => 'GwasCentral::Exception',
          description => 'search'
      },
      GwasCentral::Exception::AccessControl => {
          isa         => 'GwasCentral::Exception',
          description => 'accesscontrol'
      },
      GwasCentral::Exception::DataImport => {
          isa         => 'GwasCentral::Exception',
          description => 'dataimport',
          fields => ['marker', 'reason']
      },
     
);
1;