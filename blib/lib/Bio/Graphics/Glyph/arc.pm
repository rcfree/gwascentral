package Bio::Graphics::Glyph::arc;
# DAS-compatible package to use for drawing an arc
#ROBF adaptation of 'dot' glyph

use strict;
use base qw(Bio::Graphics::Glyph::generic);

sub draw {
  my $self = shift;
  my $gd = shift;
  my $fg = $self->fgcolor;

  # now draw a square
  my ($left,$top) = @_;
  my ($x1,$y1,$x2,$y2) = $self->calculate_boundaries(@_);
  my $xmid   = (($x1+$x2)/2);  my $width  = abs($x2-$x1);
  my $ymid   = (($y1+$y2)/2);  my $height = abs($y2-$y1);

  my $r = $self->height;

  # Code to maintain compliancy with gd 1.8.4 
  # gd 1.8.4 does not support the ellipse() or filledEllipse() methods.
  # Let's maintain the filledEllipse approach for installations
  # using gd2 or for drawing images with GD::SVG
  # Otherwise, we will use fill as before.
  # The can() method fails with GD::SVG. Why?
  my $bg = $self->bgcolor;
  if ($gd->can('filledArc') || $gd =~ /SVG/ ) {
    $gd->filledArc($xmid,$ymid,$r,$r,0, 180, $bg) if ($bg);
    # Draw the border (or unfilled square)
    $gd->arc($xmid,$ymid,$r,$r,0, 180,$fg);
    my $rad = $r/2;
    $gd->line($xmid - $rad, $ymid, $xmid + $rad, $ymid,$fg);
  }

  $self->draw_label($gd,@_) if $self->option('label');
}

1;

__END__

=head1 NAME

Bio::Graphics::Glyph::square - The "square" glyph

=head1 SYNOPSIS

  See L<Bio::Graphics::Panel> and L<Bio::Graphics::Glyph>.

=head1 DESCRIPTION

This glyph draws an ellipse the width of the scaled feature passed,
and height a possibly configured height (See Bio::Graphics::Glyph).

=head2 OPTIONS

The following options are standard among all Glyphs.  See
L<Bio::Graphics::Glyph> for a full explanation.

  Option      Description                      Default
  ------      -----------                      -------

  -fgcolor      Foreground color	       black

  -outlinecolor	Synonym for -fgcolor

  -bgcolor      Background color               turquoise

  -fillcolor    Synonym for -bgcolor

  -linewidth    Line width                     1

  -height       Height of glyph		       10

  -font         Glyph font		       gdSmallFont

  -connector    Connector type                 0 (false)

  -connector_color
                Connector color                black

  -label        Whether to draw a label	       0 (false)

  -description  Whether to draw a description  0 (false)

  -hilite       Highlight color                undef (no color)

In addition to the common options, the following glyph-specific
options are recognized:

  Option      Description                  Default
  ------      -----------                  -------

  -point      Whether to draw an ellipse   feature width
              the scaled width of the
              feature or with radius
              point.

=head1 BUGS

Please report them.

=head1 SEE ALSO

L<Bio::Graphics::Panel>,
L<Bio::Graphics::Glyph>,
L<Bio::Graphics::Glyph::arrow>,
L<Bio::Graphics::Glyph::cds>,
L<Bio::Graphics::Glyph::crossbox>,
L<Bio::Graphics::Glyph::diamond>,
L<Bio::Graphics::Glyph::dna>,
L<Bio::Graphics::Glyph::dot>,
L<Bio::Graphics::Glyph::ellipse>,
L<Bio::Graphics::Glyph::extending_arrow>,
L<Bio::Graphics::Glyph::generic>,
L<Bio::Graphics::Glyph::graded_segments>,
L<Bio::Graphics::Glyph::heterogeneous_segments>,
L<Bio::Graphics::Glyph::line>,
L<Bio::Graphics::Glyph::pinsertion>,
L<Bio::Graphics::Glyph::primers>,
L<Bio::Graphics::Glyph::rndrect>,
L<Bio::Graphics::Glyph::segments>,
L<Bio::Graphics::Glyph::ruler_arrow>,
L<Bio::Graphics::Glyph::toomany>,
L<Bio::Graphics::Glyph::transcript>,
L<Bio::Graphics::Glyph::transcript2>,
L<Bio::Graphics::Glyph::translation>,
L<Bio::Graphics::Glyph::triangle>,
L<Bio::DB::GFF>,
L<Bio::SeqI>,
L<Bio::SeqFeatureI>,
L<Bio::Das>,
L<GD>

=head1 AUTHOR

Allen Day E<lt>day@cshl.orgE<gt>.

Copyright (c) 2001 Cold Spring Harbor Laboratory

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.  See DISCLAIMER.txt for
disclaimers of warranty.

=cut
