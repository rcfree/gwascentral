# $Id: Region.pm 1540 2010-09-22 08:35:36Z rcf8 $

=head1 NAME

GenomeView - GenomeView 'plugin' for hgvbrowse_karyotype which wraps Browser::Genome.

=head1 SYNOPSIS
		
	  Provide options as CGI parameters for hgvbrowse_karyotype/genome
	  fp=GenomeView (Feature plugin = GenomeView)
	  
	  Then provide GenomeView parameters joined with ~
	  
	  threshold=ZERO
	  resultsets=HGVRS1117
	  settings=scale_type|Linear,genomeview_size|1
	  tracks=
	  sessionid=cb042fd447f4e683d437c34c6e912fd34da538cc
	  
	  e.g. /cgi-perl/hgvbrowse_karyotype/genome_v4?band_labels=0&c=ALL&h=350&w=11&e=1&rows=2&rotate=0&fp=GenomeView~threshold%3DZERO~resultsets%3DHGVRS1117~settings%3Dscale_type%7CLinear%2Cgenomeview_size%7C1~tracks%3D~sessionid%3Dcb042fd447f4e683d437c34c6e912fd34da538cc

=head1 DESCRIPTION

Karyotype feature plugin built to work with hacked version of gbrowse_karyotype (hgvbrowse_karyotype).
Uses the Browser::Genome module to retrieve features from the Browser database and return them along with header HTML
(this contains some hidden fields with counts etc.) 

=head1 SUBROUTINES/METHODS 

=cut


package GenomeView;

use Env;
use strict;
use warnings;
my $home = $ENV{GWASCENTRAL_HOME} || die "FATAL ERROR:Need to set GWASCENTRAL_HOME env variable"; 

use lib $home."/lib";
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw(new_AccessControl);
use Template;
use Bio::Graphics;

#Perl init routine
sub new {
	my ($class,$attrs) = @_;
	my $self  = {};
	$self->{feature_list}=$attrs->{feature_list};
	bless $self, $class;
	return $self;
}

=head2 retrieve_features

	  Usage      : ( $feature_file, $header_html ) = $browser->retrieve_features( $chr_list, \%parameters );
	  Purpose    : Retrieve features for the specified chromosome list and parameters
	  Returns    : The feature data as a Bio::Graphics::FeatureFile
	               An HTML heading information
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub retrieve_features {
	my ( $self, $chr, $pars) = @_;
	
	my $conf = $home."/conf/main.conf";
	#set up browser object and options
	my $browser = new_Query( "Browser::Genome", { conf_file =>
			  $home."/conf/main.conf", features => $self->{feature_list},
			  params => $pars,
			  sessionid => $pars->{sessionid},
			  DataSources_from_config=>1,
			  AccessControl_from_config => 1
	});
			  

	$browser->prepare_Filters();
	my $feature_data = $browser->as_features;
	warn "feature_data:".$feature_data;
    
    #create HTML to return using the template lib/browser_messages.tt2 this can include 'messages' created as a result of data not being available etc.
    #template also contains hidden fields containing counts and access levels these are copied to visible controls on the genome view web-page
		
	my $header_html;
	
	my $tt = Template->new({
   	 INCLUDE_PATH => $home."/htmltemplates/gbrowse",
    	INTERPOLATE  => 1,
	}) || die "$Template::ERROR\n";
	
	my $rs = $browser->f('ResultSet');
	my $vars = { 
		messages => $browser->messages,
		rs_counts => $browser->rs_counts,
		identifiers=>$rs->identifiers,
		rs_filter=>$rs
	};
	
	#process into $header_html variable rather than STDOUT
	$tt->process('genome_messages.tt2', $vars,\$header_html)
	
    || die $tt->error(), "\n";

	return ( $feature_data, $header_html );
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are probably bugs in this module but we don't know about them.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: Genome.pm 1526 2010-09-16 13:48:27Z rcf8 $ 

=cut
