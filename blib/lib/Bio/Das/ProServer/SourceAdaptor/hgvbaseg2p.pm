package Bio::Das::ProServer::SourceAdaptor::hgvbaseg2p;
use strict;
use warnings;
use base qw(Bio::Das::ProServer::SourceAdaptor);
use Data::Dumper qw(Dumper);

our $VERSION = do { my @r = (q$Revision: 453 $ =~ /\d+/mxg); sprintf '%d.'.'%03d' x $#r, @r };

sub capabilities {
  return {
	  features => '1.0',
	 };
}

sub build_features {
  my ($self, $opts) = @_;
  my $segment          = $opts->{segment};
  my $start            = $opts->{start};
  my $end              = $opts->{end};
  my $dsn              = $self->{dsn};
  #my $dbtable          = $self->config->{dbtable} || $dsn;
  my $rset_identifier     = $self->config->{rset_identifier};
  my $note             = $self->config->{description};
#  #########
#  # if this is a hydra-based source the table name contains the hydra name and needs to be switched out
#  #
#  my $hydraname     = $self->config->{hydraname};
#
#  if($hydraname) {
#    my $basename = $self->config->{basename} || q();
#    $dbtable     =~ s/$hydraname/$basename/mx;
#  }
my $chr = $segment;

$chr =~ s/chr//;
$chr =~ s/Chr//;

  my $dbtable = "marker_data_chr".$chr;
  my @bound      = ($rset_identifier);

  my $query      = qq(SELECT Marker_Identifier,Start,Stop,VariationType,Marker_Accession,Strand,NegLogPValue FROM $dbtable WHERE Resultset_Identifier = ? );
 warn "chr:$chr, start:$start, end:$end";
  if($start && $end) {
    $query .= q(AND Start >= ? AND Stop <= ?);
    push @bound, ($start, $end);
  }
  warn "starting query";
  warn "sql:$query";
  warn "parameters:".Dumper(\@bound);
  my $ref      = $self->transport->query($query, @bound);
  warn "ending query";
  my @features;

  for my $row (@{$ref}) {
    my ($start, $end) = ($row->{Start}, $row->{Stop});
	my $rsid = $row->{Marker_Accession};
	warn "$rsid";
	my $note_with_rs = "Marker dbSNP:$rsid genotyped in " . $note;
	
	my $link = "http://www.hgvbaseg2p.org/marker/" . $row->{Marker_Identifier} . "/results?rfilter=".$rset_identifier;
    if($start > $end) {
      ($start, $end) = ($end, $start);
    }
    push @features, {
                     id     => $row->{Marker_Identifier},
                     type   => $row->{VariationType} || 'hgvbaseg2p',
                     method => 'hgvbaseg2p',
                     start  => $start,
                     end    => $end,
		             note   => $note_with_rs,
		             link   => $link,
		             ori    => $row->{Strand},
		             score  => $row->{NegLogPValue},
                    };
  }
  return @features;
}

1;
__END__

=head1 NAME

Bio::Das::ProServer::SourceAdaptor::hgvbaseg2pdb - Builds DAS features from HGVbaseG2P das mysql database

=head1 VERSION

1.0

=head1 SYNOPSIS

  Build simple segment:start:stop features from a basic database table structure:
  
  Chr,MarkerID,Start,Stop,VariationType,LocalID,Source,Strand,UnadjustedPValue,LogPValue

=head1 DESCRIPTION

=head1 SUBROUTINES/METHODS

=head2 capabilities

=head2 build_features - Return an array of features based on a query given in the config file

  my @aFeatures = $oSourceAdaptor->build_features({
                                                   segment => $sSegmentId,
                                                   start   => $iSegmentStart, # Optional
                                                   end     => $iSegmentEnd,   # Optional
                                                   dsn     => $sDSN,          # if used as part of a hydra
                                                  });
=head1 DIAGNOSTICS

=head1 CONFIGURATION AND ENVIRONMENT

[general]
port=9000
response_port=80
response_host=www.hgvbaseg2p.org
response_protocol=http
response_baseuri=/das
hostname=www.hgvbaseg2p.org
maintainer=help@hgvbaseg2p.org
prefork=5
maxclients=10
styleshome=/home/ol8/Bio-Das-ProServer/stylesheets/

[CGEMS_Breast_Cancer_Unadjusted]
adaptor=hgvbaseg2pdb
state=on
transport=dbi
dbtable=das_data
dbname=HGVbaseG2P_das
dbhost=askja.gene.le.ac.uk
dbport=3306
dbuser=x
dbpass=x
description=CGEMS Breast Cancer using Unadjusted score test from HGVbaseG2P study HGVST2
dsncreated=2008-06-02 16:09:15
doc_href=http://www.hgvbaseg2p.org/study/view/HGVST2
studyid=HGVST2
experimentid=58
coordinates=NCBI_36,Chromosome,Homo sapiens => 7:140079754,140272033
analysismethodid=44

  [mysource]
  adaptor   = hgvbaseg2pdb
  transport = dbi
  dbhost    = viti.gene.le.ac.uk
  dbport    = 3308
  dbname    = HGVbaseG2P_das
  dbuser    = x
  dbpass    = x
  dbtable   = das_data

  Or for SourceHydra use:
  [mysimplehydra]
  adaptor   = hgvbaseg2pdb 
  hydra     = dbi          
  transport = dbi
  basename  = das_data 
  dbname    = HGVbaseG2P_das
  dbhost    = viti.gene.le.ac.uk
  dbuser    = x
  dbpass    = x

=head1 DEPENDENCIES

 Bio::Das::ProServer::SourceAdaptor

=head1 INCOMPATIBILITIES

=head1 BUGS AND LIMITATIONS

=head1 AUTHOR

$Author: Owen Lancaster$

=head1 LICENSE AND COPYRIGHT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut
