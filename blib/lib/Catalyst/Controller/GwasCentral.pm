package Catalyst::Controller::GwasCentral;
use base qw(Catalyst::Controller);
use FindBin;
use lib "$FindBin::Bin/../../../lib";

use GwasCentral::Search::Util qw(new_Query);
use File::Basename;
use GwasCentral::Search::Query::Basic;
use GwasCentral::AccessControl::None;

#use HGVbaseG2P::Web::Util qw(add_uri_parameters);
#use HGVbaseG2P::AccessControl;
use Carp qw(cluck);
use Moose;
use GwasCentral::DataSource::Util
  qw(DataSources_from_context new_AccessControl);
use GwasCentral::Base qw(load_module);
use JSON::XS;
use URI;
use GwasCentral::Web::Output;
use Data::Dumper qw(Dumper);

=head2 begin

Begin is run prior to default. Here core variables are set up for use by templates.
The most important is initialisation of the session if it has not been populated already.
P3P is also added to the header so that it plays nicely in IE.
The 'w3c/p3p.xml' file contains this policy information for the usage of data etc.

Also set up are base_ values for resultsets, studies and phenotypes
and a GUID which can be used to prevent file caching in browsers.

=cut

sub get_base_url {
	my ( $self, $c ) = @_;
	my @url = split( /,/, $c->req->base );
	$c->log->info("url:$url[0]");
	$url[0] =~ s/\/$//;
	return $url[0];
}

sub begin : Private {
	my ( $self, $c ) = @_;
	$c->log->info( "params:" . Dumper( $c->req->params ) );
	eval {
		
		$c->stash->{base_url} = $self->get_base_url($c);
	
		#P3P added to header for IE6 cookies
		$c->res->header(
			P3P => 'policyref="' . $self->get_base_url($c) . '/w3c/p3p.xml"' );
	
		if ( !$c->session ) {
			$c->log->debug("session not populated!");
			$c->log->info("initialising session");
			$self->init_session($c);
		}
		
		tie my %tabs, "Tie::Hash::Indexed";
		%tabs = (
		'Phenotypes' => '/phenotypes',
		'Genes' => '/genes',
		'Markers' => '/markers',
		'Studies' => '/studies',
		'Browser' => '/browser',
		'GWAS Mart' => '/biomart/martview'
		);
		$c->stash->{tabs}=\%tabs;
		
		my $ac = $self->get_AccessControl($c);
	
		$c->model("Study")->AccessControl($ac);
	
		$self->store_settings($c);
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

sub store_settings {
	my ( $self, $c ) = @_;
	my $basic =
	  GwasCentral::Search::Query::Basic->new( { conf_file => $c->config } );

	#populate browser params with browser settings session data first
	$basic->params( $c->session->{browser_settings} || {} );

	#update changes using URL params
	$basic->update_params( $c->req->params );
	$c->session->{browser_settings} = $basic->exclude_params( ['export'] );
	$c->log->info(
		"browser_settings:" . Dumper( $c->session->{browser_settings} ) );
	$basic->DataSources( DataSources_from_context($c) );
	$basic->prepare_Filters();
	$c->log->info( "session id after begin:" . $c->sessionid );
	$c->stash->{basic} = $basic;
}

sub get_host {
	my ( $self, $c ) = @_;
	return $c->req->uri->scheme . "://" . $c->req->uri->authority . "/";
}

sub get_AccessControl {
	my ( $self, $c ) = @_;
	my $ac_module = $c->config->{control_module} || 'None';

	my $acontrol = new_AccessControl(
		$c->config,
		{
			local_url     => $self->get_host($c),
			identity_hash => $c->session->{identity_hash}
		}
	);

	return $acontrol;
}

sub get_Connect {
	my ( $self, $c ) = @_;
	my $conn_module = $c->config->{connect_module} || 'Node';

	my $conn = load_module(
		"GwasCentral::Connect::$conn_module",
		{
			conf_file => $c->config,
			local_url => $self->get_host($c),
		}
	);

	return $conn;
}

sub is_user_logged_in {
	my ( $self, $c ) = @_;
	return 1 if $c->user_exists and !$c->user->{unregistered};
}

sub do_search {
	my ( $self, $c, $type, $ignore_non_html ) = @_;
	my $attrs = { context => $c, };

	my $q = new_Query( $type, $attrs );
	$q->params( $c->session->{browser_settings} );
	$q->update_params( $c->req->params );
	$c->session->{browser_settings} = $q->include_params( ['r[]'] );

	$q->prepare_Filters;
	
	$c->log->info("prepares filters");
	$c->log->info( "do_search-params:" . Dumper( $q->params ) );
	#eval {
		#place output into the 'body' of the response unless HTML, then put it into the stash as {search}->{[query_type]}
		if ( $q->Format->is_html ) {
			$c->res->content_type('text/html');
			if ($q->no_search_if_html) {
				$c->stash->{search}->{ $q->name } = {
					Query   => $q,
				};
			}
			else {
				$c->stash->{search}->{ $q->name } = {
					content => $q->results,
					Query   => $q,
				};
			}
		}
		else {
			return if $ignore_non_html;
			if ( $q->Format->is_redirect ) {
				$c->res->redirect( $q->Format->redirect_url );
			}
			else {
				$c->res->content_type( $q->Format->content_type );
				$q->Format->base_url( $self->get_base_url($c) );
				if ( $q->Format->is_download ) {
					my $filename = $q->Format->filename;
					if ($filename) {
						$c->res->headers->header(
							"Content-Disposition" => "attachment; filename="
							  . $filename );
					}
				}
				$c->res->headers->header( "Content-Transfer-Encoding" => "binary" );
				$c->res->body( $q->output );
			}
	
		}
#	};
#	if ($@) {
#		$q->throw($@);
#	}
	return $q;
}

#sub error :Regex('^error$') {
#	my ($self, $c) = @_;
#	$c->log->info("error was thrown:".$c->stash->{error}->{info});
#	$c->res->content_type("text/html");
#	#$c->stash->{template}="error.tt2";
#	$c->res->status(400);
#	$c->res->output("blah");
#}

=head2 init_session

Initialises session with default values

=cut

sub init_session {
	my ( $self, $c ) = @_;

	my $config = $c->config->{Browser};
	my @colors = ();
	my $panel  = Bio::Graphics::Panel->new;

	my @bgcolors = split( ' ', $config->{bgcolors} );
	for ( my $counter = 0 ; $counter < scalar(@bgcolors) ; $counter++ ) {
		my @rgb = $panel->color_name_to_rgb( $bgcolors[$counter] );
		push @colors, "rgb(" . join( ",", @rgb ) . ")";
	}

	$c->session->{colors}   = \@colors;
	$c->session->{settings} = $config->{Settings};
	my %track_default_for = ();
	my %track_label_for   = ();
	my %track_type_for    = ();
	my @all_tracks        = ();

	foreach my $track_type ( keys %{ $config->{Tracks} } ) {
		my %tracks = %{ $config->{Tracks}->{$track_type} || {} };
		foreach my $track ( keys %tracks ) {
			my @temp    = split( ";", $tracks{$track} );
			my $id      = $track;
			my $default = $temp[0];
			my $label   = $temp[1];
			$track_default_for{$id} = $default;
			$track_label_for{$id}   = $label;
			$track_type_for{$id}    = $track_type;
			push @all_tracks, $track;
		}
	}

	$c->session->{tracks}          = \%track_default_for;
	$c->session->{track_labels}    = \%track_label_for;
	$c->session->{track_types}     = \%track_type_for;
	$c->session->{all_tracks}      = \@all_tracks;
	$c->session->{resultsets}      = [];
	$c->session->{studies}         = {};
	$c->session->{phenotypes}      = {};
	$c->session->{threshold}       = 'ZERO';
	$c->session->{chrs_to_display} = ['ALL'];
	$c->session->{populated}       = 1;
	$c->session->{user}            = undef;

	my $b = Bio::Graphics::Browser->new;
	my ( $filename, $directory ) =
	  fileparse( $c->config->{Browser}->{region_config} );
	$b->read_configuration($directory);
	$b->source($filename);
	my $current_source = $b->source;
	my @pop_list =
	  split( ' ', $b->config->setting( 'general' => 'hapmap_populations' ) );
	$c->session->{pop_list} = \@pop_list;

	my %pop_status_for = ();
	foreach my $pop (@pop_list) {
		$pop_status_for{$pop} = 1;
	}
	$c->session->{selected_pops} = \%pop_status_for;
	$c->session->{ld_property}   = 'rsquare';
	$c->session->{ld_gt}         = '0.3';
	$c->session->{ld_lt}         = '1';

	my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
	  localtime(time);

	$c->session->{timestamp} = sprintf(
		"%4d-%02d-%02d %02d:%02d:%02d\n",
		$year + 1900,
		$mon + 1, $mday, $hour, $min, $sec
	);

}

sub output_report {
	my ( $self, $c, $item_name, $item ) = @_;

	my $format = $c->req->param('format');
	return if !$format || $format eq 'html';

	eval {
		my $wlogic =
		  GwasCentral::Web::Output->new( { conf_file => $c->config } );
		my ( $output, $content_type ) =
		  $wlogic->output_item( $item, $item_name, $format );

		$c->res->body($output);
		$c->res->content_type($content_type);
	};
	$@ and $c->stash->{error} = $@;
}

sub output_json {
	my ( $self, $c, $jsonp_data, $prefix, $suffix ) = @_;

	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(0);
	my $json = $json_coder->encode($jsonp_data);
	$c->res->output( ( $prefix || '' ) . $json . ( $suffix || '' ) );
}

sub json {
	my ( $self, $c, $json_data ) = @_;

	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(0);
	my $json = $json_coder->encode($json_data);
	return $json;
}

sub output_div {
	my ( $self, $c, $div_data, $prefix, $suffix ) = @_;
	my @divs = ();
	foreach my $div_key ( keys %{ $div_data || {} } ) {
		push @divs, $div_key . "=" . $div_data->{$div_key};

#$divs.="<div name='".$div_key."' id='".$div_key."'>".$div_data->{$div_key}."</div>";
	}
	$c->res->output(
		"<html><head><title>" . join( ";", @divs ) . "</title></head></html>" );
}

1;
