package Template::Plugin::XML::RSS::URL;
use base qw( Template::Plugin::XML::RSS );
use Template::Plugin;
use strict;
use warnings;
use Data::Dumper qw(Dumper);
use LWP::UserAgent;
my $curr_level;

sub new {
	my $class   = shift;
	my $context = shift;
	my $url_or_file = shift;
	
	my $data = $url_or_file;
	
	!$url_or_file and die "No URL, file or string supplied";
	warn "URL:$url_or_file";
	
	if ($url_or_file =~ /^(http|fttp|https)/) {
		my $ua = LWP::UserAgent->new;
  		my $req = HTTP::Request->new(GET => $url_or_file);
  		my $res = $ua->request($req);
  		if (!$res->is_success) {
	        warn "not successful:".$res->status_line;
	    }

  		$data = $res->content;
  		!$data and die "URL($url_or_file) failed".$@;
	}

	return Template::Plugin::XML::RSS->new($context, $data);
}

1;