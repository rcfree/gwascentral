
package Bio::Graphics::Glyph::myreverseplot ;

# Triangle plot for showing pairwise quantitative relationships.
# Developed for drawing LD.  Might be useful for something else.
# To work, must be passed a feature that contains multiple subfeatures.
# The parent feature must have a pair() method, which given two subfeatures
# returns an intensity value between 0 (off) and 1 (saturated)

# There needs to be an option for point features, so that the
# plot is drawn to the center of the interval between subfeatures.

use strict;
use Math::Trig;
use vars '@ISA';
use Bio::Graphics::Glyph::generic;
use Data::Dumper;
use GD::Polyline;
use constant DEBUG=> 1 ;

@ISA = 'Bio::Graphics::Glyph::generic';
$Data::Dumper::Purity =1 ;
my $SIZE=99;
# return angle in radians
sub pad_top{
my $self = shift;
 my $top = $self->SUPER::pad_top;
 $top +=  10 ;
 
  return $top ;
}




sub angle {
  my $self  = shift;
  my $angle = $self->{angle} ||= $self->option('angle') || 45;
  $self->{angle} = shift if @_;
  deg2rad($angle);
}

sub slope {
  my $self = shift;
  return $self->{slope} if exists $self->{slope};
  return $self->{slope} = tan($self->angle);
}

sub x2y {
  my $self = shift;
  shift() * $self->slope;
}

sub intercept {
  my $self = shift;
  my ($x1,$x2) = @_;
  my $mid = $self->x2y(($x1+$x2)/2);
  my $y = 0;
 $y   = $mid-$x1;
return (int($mid),int($y));

}

# height calculated from width
sub layout_height {
  my $self = shift;
  return $self->{height} if exists $self->{height};
#  return $self->{height} = $self->x2y($self->width)/2;
  return $self->{height} = ($self->width)/2;
}

sub calculate_color
 {
  my $self = shift;
  my ($s,$dprime,$lod_fl,$rgb) = @_;
  my ($blgr,$e,$lod) = 1 ;
  my @h_color = ("255","224","224");
  my $score = $s; 
  warn "score:$s";
if ($lod_fl)
  { warn"$score is the lod and $dprime is the Dprime ";
     $dprime =0 if ($dprime == 99.99);   ## missing data 
      # $blgr = int((255-244)*2*(1-$dprime));
       $blgr = int((255-32)*2*(1-$dprime));
       $blgr = 0 if ($blgr < 0);  
      #if ($score > $dprime) ;
      if ($score >= 2.0)
      { 
         # High LOD,Low D'
         @h_color= ("255","224","224")  if ($dprime < 0.5);
          #High LOD, high D' shades of red 
         @h_color =("255",$blgr,$blgr) if ($dprime > 0.5);    
      }
     if ($score < 2.0) 
     { 
       @h_color = ("192","192","240") if($dprime >0.99); 
       @h_color = ("255","255","255") if ($dprime < 0.99)  ;
      }
      #my $rgb = Number::RGB->new(rgb=>\@h_color);
 	#warn "translate_color:".Dumper();
 	#warn "hex color:".$rgb->hex();
    $self->{colors} = $self->panel->translate_color($h_color[0],$h_color[1],$h_color[2]);
   warn "this is the haplo color: @h_color, this is e:$e";
   warn "colors:".Dumper($self->{colors});
   return $self->{colors};
}

if ($lod_fl ==0)
  {
 $e = 1;
 switch: { 
  if($score == 99.99){ $e=1; last switch;}
  if($score == 1.0){$e=1; last switch;}
  if($score >= 0.9) {$e=0.9;last switch;}
  if($score >= 0.8) {$e= 0.85; last switch;}
  if($score >= 0.7) {$e = 0.75;  last switch;}
  if($score >= 0.6)  {$e = 0.65; last switch;}
  if($score >= 0.5) {$e = 0.55; last switch;}
  if($score >= 0.4 ){ $e = 0.45; last switch;}
  if($score >= 0.3 ) {$e = 0.35; last switch;}
  if($score >= 0.2 ) {$e =0.25; last switch;}
   $e = 0;
  }
 # return $self->{colors}{$e} if exists $self->{colors}{$e};
   return $self->{colors}{$e} =$self->panel->translate_color(map { 255 - (255-$_) * ($e)} @$rgb);

 }

}



sub draw {

  my $self = shift;
  my $gd   = shift;

  my ($left,$top,$partno,$total_parts) = @_;
  my $num_of_snps = $self->option('numsnp');
  my $med_score = $self->option('med_score');
  my $ld_fl = $self->option('f_type'); # values:CEU HCB YRI or JPT 
  my $box= $self->option('box_size'); # uniform or proportionate box;
  my $pop_code = $self->option('f_type'); # values:CEU HCB YRI or JPT 
  my $blackcolor = 127;
  my $ld_property =$self->option('ld_property'); 
  my $orient = $self->option('glyph_orient');
  my $fgcolor = $self->fgcolor;
  my $mdcolor = $self->panel->translate_color($self->option("mdcolor")); #missing data color ;
  my ($red,$green,$blue) = $self->panel->rgb($self->bgcolor);
  #print "<pre>color:",$red,$green,$blue, "and forground is :",$fgcolor,"</pre>\n";
  my ($missingdatared,$missingdatagreen,$missingdatablue)
            =$self->panel->rgb($mdcolor);
 
  #my $tkcolor = $self->option("mdcolor");
  # lod_flag used for the different color scheme function used for diff plots;
  my $lod_flag = 0;
  $lod_flag=1 if ($ld_property eq "lod");
  my $right = $self->right;
  my $l = $self->left ;
  my $boxes = 0;

  my @points = $self->get_points();
 
# my @positions = sort {$a<=>$b} keys %$points; 
  my $points = $self->option('point');
  #print "<pre>\$self=",Dumper($self),"</pre>\n";
  #my @parts  = map {$self->map_pt($_)} @positions; 
my @parts = sort{$a->left<=>$b->left} $self->parts;
  my $e = @parts ;
  #warn "Total parts: $e,size: $SIZE box size is : $box  top :$top left :$left\n";
  $boxes = int(($self->right-$self->left)/$e) if ($e > 0);
  $boxes = 40 if ($boxes > 40);
  $boxes= 2 if ($boxes < 2);
  $SIZE= $boxes if ($boxes < $SIZE);
  my $size = $SIZE;
 
   
#$top =$top+272;
 

  # assumption: parts are not overlapping
  #my @points;
  if ($points)
    {
     @points = map { int ((($parts[$_]->right+$parts[$_+1]->left)/2)+0.5)} (0..$#parts-1);
      unshift @points,int($parts[0]->left);
      push @points,int($parts[-1]->right);
  
    }
if ($orient eq "I"){
  my ($hcx,$hcy) = $self->intercept($points[0],$points[@parts]);
  $top = $top+$hcy;
}
else
{

 $gd->line($self->left+$left, $top+3,
	    $self->right+$left,$top+3,
	    $fgcolor);
 $_->draw_component($gd,$left,$top-8) foreach @parts;
$top=$top+5;
}
#warn "@points";
  for (my $ia=0;$ia<@parts-1;$ia++)
 {
    for (my $ib= $ia+1;$ib<@parts;$ib++)
    {
      my ($l1,$r1,$l2,$r2) = 0;
      if (@points)
	{
	
         #($l1,$r1) = ($points[$ia]+1,$points[$ia+1]-1);
	 # ($l2,$r2) = ($points[$ib]+1,$points[$ib+1]-1);
        
         ($l1,$r1) = ($points[$ia],$points[$ia+1]);
	  ($l2,$r2) = ($points[$ib],$points[$ib+1]);
           $r1=$r1+1 if (($points[$ia+1]%2) == 0);
           $l1=$l1+1 if (($points[$ia]%2) == 0) ;
           $r2=$r2+1 if (($points[$ib+1]%2) == 0) ;
          
        } 
      else {
     ($l1,$r1) = ($parts[$ia]->left+1,$parts[$ib]->right-1);
	    ($l2,$r2) = ($parts[$ib]->left+1,$parts[$ib+1]->right-1);
           }
   
      my $intensity = 0.0;
      my $c = 0;
   
  
    $intensity = eval{$self->feature->pair_score($parts[$ia],$parts[$ib],$ld_fl)};
  
 # warn "$intensity " if ($intensity ==2.0);
     #warn $@ if $@;
  # need the dprime score for the haplo/lod plot
     my $dprime = 0.0;
      
  if ($lod_flag){
     my $dpr_fl=$ld_fl."-dprime";
      $dprime = eval{$self->feature->pair_score($parts[$ia],$parts[$ib],
					    $dpr_fl)} ;
      }
      else {$dprime =0.0;}
 
next if ($intensity == 99.99); # missing data get out now


    $c   = $self->calculate_color($intensity,$dprime,$lod_flag,[$red,$green,$blue]) if ($intensity != 99.99) ;
 
  
 #$c =$self->mycolor_code($intensity,$med_score,[$red,$green,$blue]);
 
warn "calculate code returned: $c";
      # left corner
    my ($lcx,$lcy,$tcy,$tcx,$rcx,$rcy,$bcx,$bcy)=0;
    if ($box == 0)
      {
       ($lcx,$lcy) = $self->intercept($l1,$l2);
       ($tcx,$tcy) = $self->intercept($r1,$l2);
       ($rcx,$rcy) = $self->intercept($r1,$r2);
       ($bcx,$bcy) = $self->intercept($l1,$r2);
     
      }
  if ($box == 1)
      {
      ($tcx,$tcy) = $self->intercept($r1,$l2);
        $tcy = 1 if ($tcy < 1);
       ($lcx,$lcy) = ($tcx-$size,$tcy+$size);
  
       ($rcx,$rcy) = ($tcx+$size,$tcy+$size);
       ($bcx,$bcy) = ($tcx,$tcy+(2*$size));
  
   }


 # warn "$l1 $l2"; 
#    warn "$hcy: $l1,$l2,$r1,$r2,L1 L2:$lcx,$lcy, R1 L2:$tcx,$tcy, R1 R2:$rcx,$rcy, L1 R2:$bcx,$bcy"; 
      my $poly = GD::Polyline->new();
      if ($orient eq "I")
      {
      	$poly->addPt($lcx+$left,$top-$lcy);
      	$poly->addPt($tcx+$left,$top- $tcy);
      	$poly->addPt($rcx+$left,$top-$rcy);
     	 $poly->addPt($bcx+$left,$top-$bcy);
	}
 else
    
      {
      	$poly->addPt($lcx+$left,$top+$lcy);
      	$poly->addPt($tcx+$left,$top+ $tcy);
      	$poly->addPt($rcx+$left,$top+$rcy);
     	 $poly->addPt($bcx+$left,$top+$bcy);
	}
  $gd->filledPolygon($poly,$c);
    }
  #last;
  }
if ($orient eq "I"){ 
$gd->line($self->left+$left, $top+3,
	    $self->right+$left,$top+3,
	    $fgcolor);
 $_->draw_component($gd,$left,$top-8) foreach @parts;
  }
#warn "this is the value of size: $size";
}

sub get_points {
    #warn "In get points";
  my $self = shift;
  my @points;
  my @parts = $self->parts;
  my ($l1,$r2)=0;
  return unless @parts;
  
  for my $g (@parts)
  {
    $r2= $g->right;
    if ($r2-$l1 > 0)
    {
      $SIZE = $r2-$l1 if ($r2-$l1 < $SIZE);
      push (@points,$g->left);
      push (@points,$g->right);
    }
    $l1= $g->left;
   }
  
  $SIZE= 3 if ($SIZE < 3);
  @points;
}
sub mycolor_code{
my $self = shift;
my ($s,$med_score,$rgb)=@_;
my $new_rgb=$rgb;
my $red_color ="";
 switch: { 
  if($s == 2.0){ $red_color="#999999"; last switch;}
  if($s == 1.0){$red_color= "#dd0000"; last switch;}
  if($s >= 0.9) {$red_color="#ff0000";last switch;}
  if($s >= 0.8) {$red_color= "#ff3333"; last switch;}
  if($s >= 0.7) {$red_color= "#ff9999";  last switch;}
  if($s >= 0.6)  {$red_color="#ee99cc"; last switch;}
  if($s >= 0.5) {$red_color="#ffaa99"; last switch;}
  if($s >= 0.4 ){ $red_color="#ffcccc"; last switch;}
  if($s >= 0.3 ) {$red_color= "#eecccc"; last switch;}
  if($s >= 0.2 ) {$red_color="#eedddd"; last switch;}
   $red_color="#ffffff";
}
 my $intensity = $self->panel->translate_color($red_color);
 my ($red,$green,$blue) =$self->panel->rgb($intensity);
#warn " red_color : $red_color, $intensity , $red, $green, $blue ";  
#return $self->{colors}{$intensity} if exist $self->{colors}{$intensity};
#return $self->{colors}{$intensity} = $intensity;
return $intensity ;
# $self->panel->translate_color(map { 255 - (255-$_) * min(max( ($score-$min_score)/$span, 0), 1)} @$rgb);

sub min { $_[0] < $_[1] ? $_[0] : $_[1] }
sub max { $_[0] > $_[1] ? $_[0] : $_[1] }
#if ($score == 2.0)
 # {
 # warn "this is $@rgb";
 #$e=1.0;
 # return $self->{colors}{$e} =$self->panel->translate_color(map { 255 - (255-$_) * ($e)} @$rgb);

 # }
# my $min_score = 0.4;
# my $span =0.6; 
#my ($e,$score, $median) =0.00;

#$median = $dprime ;
#my $e = 0 if ($score == 0.0);
#my $e = $score+ (log($score)/log(10)) if ($score > 0.0) ;
#$e = 0.0 if ($e < 0);

#my $e = $s * $s; 
#if ($score <= $median)
#  {
#    $e = (0.5 - ($median -$score));
#  }
#else
#  {
#    $e = ($score - $median) +0.5 ;
#  }

}

# never allow our internal parts to bump;
sub bump { 0 }

1;

__END__

=head1 NAME

Bio::Graphics::Glyph::pairwise_plot - The "pairwise plot" glyph

=head1 SYNOPSIS

 use Bio::Graphics;

 # create the panel, etc.  See Bio::Graphics::Panel
 # for the synopsis

 # Create one big feature using the PairFeature
 # glyph (see end of synopsis for an implementation)
 my $block = PairFeature->new(-start=>  2001,
 			      -end  => 10000);

 # It will contain a series of subfeatures.
 my $start = 2001;
 while ($start < 10000) {
   my $end = $start+120;
   $block->add_SeqFeature($bsg->new(-start=>$start,
				    -end  =>$end
				   ),'EXPAND');
   $start += 200;
 }

 $panel->add_track($block,
 		   -glyph => 'pairwise_plot',
		   -angle => 45,
		   -bgcolor => 'red',
		   -point => 1,
		  );

 print $panel->png;

 package PairFeature;
 use base 'Bio::SeqFeature::Generic';

 sub pair_score {
   my $self = shift;
   my ($sf1,$sf2) = @_;
   # simple distance function
   my $dist  = $sf2->end    - $sf1->start;
   my $total = $self->end   - $self->start;
   return sprintf('%2.2f',1-$dist/$total);
 }

=head1 DESCRIPTION

This glyph draws a "triangle plot" similar to the ones used to show
linkage disequilibrium between a series of genetic markers.  It is
basically a dotplot drawn at a 45 degree angle, with each
diamond-shaped region colored with an intensity proportional to an
arbitrary scoring value relating one feature to another (typically a
D' value in LD studies).

This glyph requires more preparation than other glyphs.  First, you
must create a subclass of Bio::SeqFeature::Generic (or
Bio::Graphics::Feature, if you prefer) that has a pair_score() method.
The pair_score() method will take two features and return a numeric
value between 0.0 and 1.0, where higher values mean more intense.

You should then create a feature of this new type and use
add_SeqFeature() to add to it all the genomic features that you wish
to compare.

Then add this feature to a track using the pairwise_plot glyph.  When
the glyph renders the feature, it will interrogate the pair_score()
method for each pair of subfeatures.

=head2 OPTIONS

In addition to the common options, the following glyph-specific
options are recognized:

  Option      Description                  Default
  ------      -----------                  -------

  -point      If true, the plot will be         0
              drawn relative to the
              midpoint between each adjacent
              subfeature.  This is appropriate
              for point-like subfeatures, such
              as SNPs.

  -angle      Angle to draw the plot.  Values   45
              between 1 degree and 89 degrees
              are valid.  Higher angles give
              a more vertical plot.

  -bgcolor    The color of the plot.            cyan

=head1 BUGS

Please report them.

=head1 SEE ALSO

L<Bio::Graphics::Panel>,
L<Bio::Graphics::Glyph>,
L<Bio::Graphics::Glyph::arrow>,
L<Bio::Graphics::Glyph::cds>,
L<Bio::Graphics::Glyph::crossbox>,
L<Bio::Graphics::Glyph::diamond>,
L<Bio::Graphics::Glyph::dna>,
L<Bio::Graphics::Glyph::dot>,
L<Bio::Graphics::Glyph::ellipse>,
L<Bio::Graphics::Glyph::extending_arrow>,
L<Bio::Graphics::Glyph::generic>,
L<Bio::Graphics::Glyph::graded_segments>,
L<Bio::Graphics::Glyph::heterogeneous_segments>,
L<Bio::Graphics::Glyph::line>,
L<Bio::Graphics::Glyph::pinsertion>,
L<Bio::Graphics::Glyph::primers>,
L<Bio::Graphics::Glyph::rndrect>,
L<Bio::Graphics::Glyph::segments>,
L<Bio::Graphics::Glyph::ruler_arrow>,
L<Bio::Graphics::Glyph::toomany>,
L<Bio::Graphics::Glyph::transcript>,
L<Bio::Graphics::Glyph::transcript2>,
L<Bio::Graphics::Glyph::translation>,
L<Bio::Graphics::Glyph::triangle>,
L<Bio::Graphics::Glyph::xyplot>,
L<Bio::DB::GFF>,
L<Bio::SeqI>,
L<Bio::SeqFeatureI>,
L<Bio::Das>,
L<GD>

=head1 AUTHOR

Lincoln Stein E<lt>lstein@cshl.edu<gt>.

Copyright (c) 2004 Cold Spring Harbor Laboratory

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.  See DISCLAIMER.txt for
disclaimers of warranty.

=cut
