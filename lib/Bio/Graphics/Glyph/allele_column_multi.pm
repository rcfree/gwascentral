package Bio::Graphics::Glyph::allele_column_multi;

# $Id: allele_column_multi.pm,v 1.1 2008/02/16 13:11:49 Zhenyuan Exp $
# Glyph for drawing a column chart with s slices representing allele frequency.

use strict;
use vars '@ISA';
@ISA = 'Bio::Graphics::Glyph::generic';
use Bio::Graphics::Glyph::generic;
use Data::Dumper qw(Dumper);
my $width=4;
my %abbr=qw(
CEU C
CHB H
JPT J
YRI Y
ASW A
MEX M
GIH G
TSI T
CHD D
LWK L
MKK K
);
sub height {
    my $self   = shift;
    my $height = $self->SUPER::height;
    if ( defined $self->option('stacked') ) {
        $height = 19 if $height < 19;
        my $freq =
          defined( $self->option('freq') ) ? $self->option('freq') : 'NO';
        my @pops = split /;/, $freq;
        return $height * scalar(@pops);
    }
    else {
        return $height > 19 ? $height : 19;
    }
}

# Need to make room for the allele pies if there is room
sub pad_right {
    my $self   = shift;
    my $right  = $self->SUPER::pad_right;
    my $height = $self->height;
    my $freq = defined( $self->option('freq') ) ? $self->option('freq') : "NO";
    my @pops = split /;/, $freq;

    if ( defined $self->option('stacked') ) {
        return $right > $height + 7 ? $right : $height + 7 if $self->label;
    }
    else {
        return $right > $height * scalar(@pops) + 6
          ? $right
          : $height * scalar(@pops) + 6
          if $self->label;
        return 6;
    }
}

sub majorcolor {
    my $self = shift;
    my $color = $self->option('majorcolor') || '#0033FF';
    $self->factory->translate_color($color);
}

sub minorcolor {
    my $self = shift;
    my $color = $self->option('minorcolor') || '#FF0000';
    $self->factory->translate_color($color);
}
sub get_description {
    my $self    = shift;
    my $feature = shift;
    my $freq = defined( $self->option('freq') ) ? $self->option('freq') : undef;
    if ( $freq && $freq =~ /:/ ) {
		#return join( ' ', map( /(.*):/, map { split /;/ } $freq ) );
		return join( ' ', map { $abbr{$_} || substr($_, 0, 1) } map { /(.*):/ } map { split /;/ } $freq );
    }
    else {
        return join '; ', eval { $feature->notes };
    }
}

sub pad_bottom {
    my $self = shift;
    if ( defined $self->option('stacked') ) {
        return 0;
    }
    else {
        return $self->SUPER::pad_bottom;
    }
}

sub draw_description {
    my $self = shift;
    my ( $gd, $left, $top, $partno, $total_parts ) = @_;
    my $label = $self->description or return;
    my @items = split /\s+/, $label;
    my $x = $self->left + $left;
    $x = $self->panel->left + 1 if $x <= $self->panel->left;
    my $pie_size;
    if ( defined $self->option('stacked') ) {
        my $freq =
          defined( $self->option('freq') ) ? $self->option('freq') : 'NO';
        my @pops = split /;/, $freq;
        $pie_size = ( $self->height ) / scalar(@pops);
    }
    else {
        $pie_size = $self->height;
    }
    for ( my $i = 0 ; $i < @items ; $i++ ) {
        if ( $self->option('stacked') ) {
            $gd->string(
                $self->font,
                ( $x + $pie_size + 7 ),
                (
                    ( ( $pie_size + 1 ) * $i ) - 1 + $pie_size / 2 +
                      $self->font->height / 2 + $self->top + $top
                ),
                $items[$i],
                $self->font2color
            );
        }
        else {
			my $d=10+$i*($width+2);
			#my $d = 10 + ( $pie_size * ( 0.5 + $i ) ) -
			#  ( length( $items[$i] ) * ( $self->font->width / 2 ) );
            $gd->string(
                $self->font,
                ( $x + $d ),
                $self->bottom - $self->pad_bottom + $top,
                $items[$i], $self->font2color
            );
        }
    }
}

sub draw_component {
    my $self = shift;
    my $gd   = shift;

    # find the center and vertices
    my ( $x1, $y1, $x2, $y2 ) = $self->calculate_boundaries(@_);
	
    my $feature = $self->feature;

    my @alleles = split /\//, $self->option('alleles');

    if (@alleles) {

        # If it is on the minus strand
        if ( my $strand = $self->option('ref_strand') < 0 ) {
            foreach (@alleles) {
                tr/ACTG/TGAC/ if $self->option('complement');
            }
        }
        my $height = $self->height;

      #	my $maf =  defined ($self->option('maf')) ? $self->option('maf') : "NO";
        my $freq =
          defined( $self->option('freq') ) ? $self->option('freq') : "NO";
        my @pop_freqs = split (/;/, $freq);
		
        # write the alleles
        for ( my $i = 0 ; $i < @alleles ; $i++ ) {
            if ( my $ref_allele = $self->option('ref_allele') ) {

                if ( $alleles[$i] ne $ref_allele ) {
                    $gd->string(
                        $self->image_class->gdSmallFont,
                        $x1 - 1, -2 + 10 + $y1,
                        $alleles[$i], $self->minorcolor
                    );
                }
                else {
                    my $c = $self->majorcolor;
                    $gd->string( $self->image_class->gdMediumBoldFont,
                        $x1 - 1, -2 + $y1, $alleles[$i], $c );
                }
            }
        }

		if ( $self->label ) {
			next if $self->option('stacked');
			for ( my $i = 0 ; $i < scalar(@pop_freqs) ; $i++ ) {
				my $freq;
                if ( $pop_freqs[$i] =~ /:/ ) {
                    my @s = split /:/, $pop_freqs[$i];
					$freq = $s[1];
                }

                $freq = 0.499 if $freq == 0.5;
                $freq = 'NO' unless $freq =~ /^[0-9.]+$/;

                #		my $ph = $height -1 ;
				my $xt=$x1+10+$i*($width+2);
				my $yt=$y1;
				my $xb=$xt+$width;
				my $yb=$yt+$height;
				if ( $freq eq 'NO' ) {
					$gd->rectangle($xt, $yt, $xb, $yb ,$self->majorcolor);
				} elsif ( 1 == $freq ) {
					$gd->filledRectangle($xt, $yt, $xb, $yb ,$self->majorcolor);
				} elsif ( 0 == $freq ) {
					$gd->filledRectangle($xt, $yt, $xb, $yb ,$self->minorcolor);
				} else {
					my $ym=int($height*$freq);
					if (0 == $ym) {
						$ym=1;
					} elsif ($height == $ym) {
						$ym=$height-1;
					}
					$gd->filledRectangle($xt, $yt, $xb, $yt+$ym ,$self->majorcolor);
					$gd->filledRectangle($xt, $yt+$ym+1, $xb, $yb, $self->minorcolor);
				}
			}

		}
    }
}

1

  ;

__END__

=head1 NAME

Bio::Graphics::Glyph::allele_column_multi

=head1 SYNOPSIS

  See <Bio::Graphics::Panel> and <Bio::Graphics::Glyph>.

=head1 DESCRIPTION

This glyph draws column for each population found at a SNP position, one above the other (i.e. in a column).

See also http://www.hapmap.org/cgi-perl/gbrowse/gbrowse 'genotyped SNPs' for an example.

The common options are available (except height which is calculated
based on the number of alleles).  In addition, if you give the glyph
the minor allele frequency (MAF) and indicate which is the minor
allele, the glyph will display these differences.


=head2 GETTING THE ALLELES

To specify the alleles, you can pass an "alleles" callback to the appropriate
section of the config file.  This option should return the two alleles such as G/T.

=head2 OPTIONS

 . Glyph Colour
 . Major allele shown in bold
 . Vertical columns to show allele frequency

=head3 GLYPH COLOR

The glyph color can be configured to be different.  Use majorcolor to define the glyph color for the reference allele and minorcolor for non-reference allele.  For example:

   majorcolor     = blue
   minorcolor     = red

For this option to work, you must also set ref_strand to return the strand of the feature:

ref_strand        = sub {shift->strand}

=head3 ALLELE FREQUENCY COLUMNS

Use the 'freq' option to return the reference allele frequency for the SNP. e.g. ASW:0.3;CEU:0.6;CHB:0.4;CHD:0.5

=head1 BUGS

Please report them.

=head1 SEE ALSO

L<Bio::Graphics::Panel>,
L<Bio::Graphics::Glyph>,
L<Bio::Graphics::Glyph::arrow>,
L<Bio::Graphics::Glyph::cds>,
L<Bio::Graphics::Glyph::crossbox>,
L<Bio::Graphics::Glyph::diamond>,
L<Bio::Graphics::Glyph::dna>,
L<Bio::Graphics::Glyph::dot>,
L<Bio::Graphics::Glyph::ellipse>,
L<Bio::Graphics::Glyph::extending_arrow>,
L<Bio::Graphics::Glyph::generic>,
L<Bio::Graphics::Glyph::graded_segments>,
L<Bio::Graphics::Glyph::heterogeneous_segments>,
L<Bio::Graphics::Glyph::line>,
L<Bio::Graphics::Glyph::pinsertion>,
L<Bio::Graphics::Glyph::primers>,
L<Bio::Graphics::Glyph::rndrect>,
L<Bio::Graphics::Glyph::segments>,
L<Bio::Graphics::Glyph::ruler_arrow>,
L<Bio::Graphics::Glyph::toomany>,
L<Bio::Graphics::Glyph::transcript>,
L<Bio::Graphics::Glyph::transcript2>,
L<Bio::Graphics::Glyph::translation>,
L<Bio::Graphics::Glyph::allele_tower>,
L<Bio::DB::GFF>,
L<Bio::SeqI>,
L<Bio::SeqFeatureI>,
L<Bio::Das>,
L<GD>

=head1 AUTHOR

Zhenyuan Lu E<lt>luj@cshl.eduE<gt> in Lincoln Stein's lab E<lt>steinl@cshl.eduE<gt>.

Copyright (c) 2008 Cold Spring Harbor Laboratory

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.  See DISCLAIMER.txt for
disclaimers of warranty.

=cut
