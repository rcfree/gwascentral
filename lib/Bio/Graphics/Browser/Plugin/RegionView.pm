# $Id: RegionView.pm 1515 2010-07-13 13:52:11Z rcf8 $

#ROB's RegionView plugin for hgvbrowse which calls logic in GwasCentral::Browser::Core to draw significance glyphs

package Bio::Graphics::Browser::Plugin::RegionView;

use Env;
my $home = $ENV{GWASCENTRAL_HOME} || die "FATAL ERROR:Need to set GWASCENTRAL_HOME env variable"; 

use lib $home."/lib";

use GwasCentral::Search::Query::Browser::Region;
use GwasCentral::AccessControl::None;
use GwasCentral::DataSource::Study;
use GwasCentral::DataSource::Browser;

use strict;
use warnings;

use Bio::Graphics::Browser::Plugin;
use CGI qw(:standard *table);
use Log::Log4perl qw(:easy);
use Data::Dumper qw(Dumper);

use vars '@ISA';

@ISA = qw(Bio::Graphics::Browser::Plugin);

my %SITES;
my $percentid;
my @COLORS = qw(red green blue orange cyan black
  turquoise brown indigo wheat yellow emerald);
  
sub name { return "RegionView"; }

sub description {
	p(
"This plugin displays data related to -log P-values including the maximum and count of significant values as a histogram."
	);
}

sub type { 'annotator' }

sub init {
	my $self = shift;
	my $conf = $self->browser_config;
}

sub config_defaults {

	my $config = {
		t  => -1,
		r=>-1,
		tracks=>'',
		settings=>'',
		sessionid=>'',
	};
	return $config;
}

sub reconfigure {
	my $self = shift;
	my $current_config = $self->configuration;
	$current_config->{'t'}  = $self->config_param('t');
	$current_config->{'r'} = $self->config_param('r');
	$current_config->{'settings'} = $self->config_param('settings');
	$current_config->{'tracks'}=$self->config_param('tracks');
	$current_config->{'sessionid'}=$self->config_param('sessionid');
}

sub configure_form {
	my $self           = shift;
	my $current_config = $self->configuration;
          return table({-style=>'width:'},
           TR({-class=>'searchtitle'},
            th(
                { -style => 'text-align:right', -style => 'display:none' },
                "Hidden Fields:",
                td(hidden(
                    -name => $self->config_name('r'),
                    -default => $current_config->{'resultsets'},
                ),
                hidden(
                    -name => $self->config_name('settings'),
                    -default => $current_config->{'settings'},
                ),
                hidden(
                    -name => $self->config_name('tracks'),
                    -default => $current_config->{'tracks'},
                ),
                hidden(
                    -name => $self->config_name('sessionid'),
                    -default => $current_config->{'sessionid'},
                )))));

}    

#bare minimum logic in annotate method
sub annotate {
	  my ($self,$segment)    = @_;
	  #logic for feature generation is present in GwasCentral::Browser
 
		my $ac = GwasCentral::AccessControl::None->new({conf_file=>$home.'/conf/main.conf'});
		my $sdb = GwasCentral::DataSource::Study->new({conf_file=>$home.'/conf/main.conf'});
		$sdb->AccessControl($ac);
		
		my $bdb = GwasCentral::DataSource::Browser->new({conf_file=>$home.'/conf/main.conf'});
		$bdb->AccessControl($ac);
		
		 my %attrs = %{$self->configuration};

		 $attrs{q}=$segment->seq_id.":".$segment->start."..".$segment->stop;
	  $attrs{format}="feature";
	  
		my $browser = GwasCentral::Search::Query::Browser::Region->new({
				  conf_file =>
					$home."/conf/main.conf",
					features=>$self->new_feature_list,
					params => \%attrs
	  });
	  
		$browser->databases({
			Study => $sdb,
			Browser => $bdb
		});
	
	  #my $formatter = GwasCentral::Search::Format->new({conf_file=>$home.'/conf/hgvbase.conf'});
	  #$browser->Formatter($browser);
	  $browser->prepare_Filters();
	  warn "attrs:".Dumper(\%attrs);
	  my $features;
	  eval {
	  	$features =  $browser->output;
	  };
	  if ($@) {
	  	die "error:".$@;
	  }
	  
	  warn "features:".$features;
	  return $features;
	
}

1;

__END__

=head1 NAME

Bio::Graphics::Browser::Plugin::RegionView - Calculate variety of P value feature data including count of significant markers, max significance etc.

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 OPTIONS


=head1 BUGS

None known yet.

=head1 SEE ALSO

L<Bio::Graphics::Browser::Plugin>

=head1 AUTHOR

Copyright (c) 2007 University of Leicester.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
