#!perl

# $Id: DateTo.pm 497 2008-04-15 14:51:03Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Replacer - Superclass for Replacer modules 

=head1 SYNOPSIS

    use HGVbaseG2Pmart::Replacer::DateTo;
    $replacer = HGVbaseG2Pmart::Replacer::DateTo->new;
    $new_text = replacer->replace_text;

=head1 DESCRIPTION

This class contains methods which replace a <Replacer/> XML tag in a mart XML config file.
The replace_text method here returns an Option list of years which when selected produce a list of months
which pass the correct date as a filter.

=head1 SUBROUTINES/METHODS 

=cut
package GwasCentral::Mart::Replacer::DateTo;

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../";
use Moose;
extends qw(GwasCentral::Mart::Replacer);

=head2 replace_text

  Usage      :  $text = $replacer->replace_text;

  Description:  Generates the data used to replace a <Replacer/> element in mart XML config.
  				It generate a list of years with PushAction elements containing month Option elements.
  				Ultimately it provides a list of years which when selected produce a list of months
  				which pass the correct date as a filter.

  Return type:  A scalar containing the generated text

  Exceptions :  

  Caller     :

=cut

sub replace_text {
	my ($self) = @_;
	my $type="to";
	my @months=qw(jan feb mar apr may jun jul aug sep oct nov dec);
	my $day_no = '31';
	my $text="";
	for (my $year=2011;$year>1990;$year--) {
		my $counter=1;
		$text.="<Option displayName=\"$year\" internalName=\"$type$year\" isSelectable=\"true\" value=\"$year\">\n";
		$text.="\t<PushAction internalName=\"push"."_"."$type"."_"."$year\" ref=\"submissiondate_month_$type\">\n";
		my $month_no=1;
		foreach my $month(@months) {
			$month_no = sprintf("%02d", $month_no);
			$text.="\t\t<Option displayName=\"".uc($month)."\" internalName=\"$year-$month_no-$day_no\" isSelectable=\"true\" value=\"$year-$month_no-$day_no\"/>\n";
			$month_no++;
		}
		$text.="\t</PushAction>\n";
		$text.= "</Option>\n";
	}
	return $text;
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: DateTo.pm 497 2008-04-15 14:51:03Z rcf8 $

=cut