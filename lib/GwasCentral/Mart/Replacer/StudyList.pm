#!perl

# $Id: StudyList.pm 772 2008-06-23 17:37:28Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Replacer - Superclass for Replacer modules 

=head1 SYNOPSIS

    use HGVbaseG2Pmart::Replacer::StudyList;
    $replacer = HGVbaseG2Pmart::Replacer::StudyList->new;
    $new_text = replacer->replace_text;

=head1 DESCRIPTION

This class contains methods which replace a <Replacer/> XML tag in a mart XML config file.
The replace_text method here returns an Option list containing Study Titles and IDs as values.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Mart::Replacer::StudyList;

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../";
use Data::Dumper qw(Dumper);
use Moose;
extends qw(GwasCentral::Mart::Replacer);

=head2 replace_text

  Usage      :  $text = $replacer->replace_text;

  Description:  Generates the data used to replace a <Replacer/> element in mart XML config.
  				It uses the DB to retrieve StudyIDs and Titles and generates Option elements

  Return type:  A scalar containing the generated text

  Exceptions :  

  Caller     :

=cut

sub replace_text {
	my ($self) = @_;

	my $dbh =  $self->Creator->DataSources->{Study}; 
	
	my @studies = $dbh->get_all_unhidden_studies();
	my $text="";
	foreach(@studies) {
		my $title = $_->name;
		my $id = $_->identifier;
		$text .= '<Option displayName="'.$title.'" internalName="'. $id . '" isSelectable="true" value="'.$id.'"/>'."\n";
	}
	
	return $text;
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: StudyList.pm 772 2008-06-23 17:37:28Z rcf8 $

=cut