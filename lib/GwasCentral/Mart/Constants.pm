#!perl

# $Id: Constants.pm 782 2008-06-26 13:39:21Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Constants - Mart constants file


=head1 SYNOPSIS

	use HGVbaseG2Pmart::Constants;
	$current_config_file = $HGVbaseG2Pmart::Constants::TEST_CONFIG_FILE;
	
=head1 DESCRIPTION

This class contains global constants which are used by the Test suite.

=head1 SUBROUTINES/METHODS 

=cut

package HGVbaseG2Pmart::Constants;

use strict;
use warnings;

use FindBin;

our $TEST_CONFIG_FILE='./conf/test/hgvmart.conf';