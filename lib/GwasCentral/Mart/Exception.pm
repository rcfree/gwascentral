#!perl

# $Id: Exception.pm 268 2008-01-09 16:01:16Z rcf8 $

=head1 NAME

HGVbaseG2Pmart::Exception - Exceptions thrown by HGVbaseG2Pmart modules

=head1 SYNOPSIS

   # In library code
   use HGVbaseG2Pmart::Exception;
   if('something goes wrong in database-query') {
      HGVbaseG2Pmart::Exception::Database->throw('this query is busted');
   }

   # In calling code
   use HGVbaseG2Pmart::Exception;
   eval{ 'attempt to execute prepared Mart-query' };
   my $e;
   if($e = Exception::Class->caught('HGVbaseG2Pmart::Exception::Database')) {
      print "caught db-error";
      # do something about error, if possible
   }

=head1 DESCRIPTION

HGVbaseG2Pmart::Exception defines exceptions thrown by the HGVbaseG2Pmart
modules. This is done using the Exception::Class module
(see POD at http://search.cpan.org/~drolsky/Exception-Class/).

=head1 EXCEPTION CLASSES

=head2 HGVbaseG2Pmart::Exception::Configuration

Error related to Mart setup configuration, such as failure to read a config file. 

=head2 HGVbaseG2Pmart::Exception::Database

Error related to a Mart database connection, such as incorrect db-connection
parameters or bad SQL-statement.

=cut

package HGVbaseG2Pmart::Exception;

use Exception::Class (
  'HGVbaseG2Pmart::Exception',
   HGVbaseG2Pmart::Exception::Config => {
        isa         => 'HGVbaseG2Pmart::Exception',
        description => 'Error in configuration'
   },
   HGVbaseG2Pmart::Exception::Db => {
        isa         => 'HGVbaseG2Pmart::Exception',
        description => 'Error in database connection or statement',
		fields      => ['dbh','sth'],
   },	
);

Exception::Class::Base->Trace(1);

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Exception.pm 268 2008-01-09 16:01:16Z rcf8 $

=cut