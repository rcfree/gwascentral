#!perl

# $Id: Database.pm 782 2008-06-26 13:39:21Z rcf8 $

=head1 NAME

GwasCentral::Mart::Database - Mart database access


=head1 SYNOPSIS

    use GwasCentral::Mart::Database;
    use HGVbaseG2P::Util qw(loadConfig);
    
    my %config = loadConfig($configfile);

	my $db_cfg=$config{Database};
	
    my $db = GwasCentral::Mart::Database->new($db_cfg);
    
    $db->add_dataset_config("study","STUDY",1);
    
    $db->DESTROY;


=head1 DESCRIPTION

This class handles data access for the HGVBase-G2P mart. It separates database
access logic from the rest of the script. 

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Mart::Database;
use Moose;
extends qw(GwasCentral::DataSource::Mart);
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";

use DBI;
use DBD::mysql;
use Carp qw(croak cluck);

use Data::Dumper qw(Dumper);

use GwasCentral::Exception;

=head2 new

  Usage      : $db = GwasCentral::Mart::Database->new($db_cfg)
  Purpose    : To initialise an GwasCentral::Mart::Database object 
  Returns    : GwasCentral::Mart::Database object
  Arguments  : $db_cfg is passed as hash from config file:
					dsn = dbi:mysql:db_name:host:port
					user = uid
					pwd = pword
  Throws     : GwasCentral::Mart::Exception::Db if unable to connect
  Status     : 
  Comments   : 

=cut

sub new {
	my ($class,$db_cfg)=@_;
	
	my $dbh = DBI->connect($db_cfg->{dsn},$db_cfg->{user},$db_cfg->{pwd}, {'PrintError' => 1, 'RaiseError'=>1}) or GwasCentral::Mart::Exception::Db->throw("Error connecting to database");
	# stops AE XML too long bug
    $dbh->{'LongTruncOk'} = 1;
    $dbh->{'LongReadLen'} = 20000;
	
	my $self={};
	$self=$db_cfg;
	$self->{db_handle}=$dbh;
	$self->{curr_dataset_id}=1;
	bless($self, $class);
	return $self;
}

=head2 get_name

  Usage      : $db_name = $db_obj->get_name
  Purpose    : Get name of database
  Returns    : Scalar containing name of database
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_name {
	my $self = shift;
	return $self->{name};
}

=head2 get_dataset_id

  Usage      : $dataset_id=$db->get_dataset_id("study");
  Purpose    : Get dataset id from database when given dataset name
  Returns    : Scalar containing dataset id
  Arguments  : A dataset name
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_dataset_id {
	my ($self, $dataset) = @_;
	
	#get dataset id key from dataset name
    my $sql = "SELECT dataset_id_key FROM meta_conf__dataset__main WHERE dataset=?";
    my @params = ();
    push @params, lc($dataset);
    my $dataset_id=$self->get_scalar_value($sql,\@params);
    return $dataset_id; 
}

=head2 get_dataset_id

  Usage      : $newdb=$db->drop_and_create($db_cfg);
  Purpose    : Drop and create new db based on supplied database config hash (see new)
  Returns    : New database object based on config
  Arguments  : Database config hash (see new)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub drop_and_create {
	my ($self,$db_cfg) = @_;
	my $dbname = $db_cfg->{name};
	#get dataset id key from dataset name
	DEBUG("Dropping $dbname");
	$self->execute_statement("DROP DATABASE IF EXISTS $dbname");
	$self->execute_statement("CREATE DATABASE IF NOT EXISTS $dbname");
	my $newdb = GwasCentral::Mart::Database->new($db_cfg);
	DEBUG("newdb:".Dumper($newdb));
	return $newdb;
}

=head2 update_conf_xml

  Usage      : $db->update_conf_xml($compressed,$dataset_id);
  Purpose    : Update config XML in mart for specific dataset
  Returns    : Nothing
  Arguments  : A scalar containing compressed XML data
  			   A current dataset id
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub update_conf_xml {
	my ($self,$compressed,$dataset_id) = @_;
	
	#update table with compressed XML
    my $sql = "UPDATE meta_conf__xml__dm SET xml=?, compressed_xml=?, message_digest=? WHERE dataset_id_key = ?";
 	my @params = ();
 	
 	push @params, $compressed->{uncompressed};
 	push @params, $compressed->{compressed};
 	push @params, $compressed->{digest};
 	push @params, $dataset_id;

	$self->execute_statement($sql,\@params);
}

=head2 update_template_xml

  Usage      : $db->update_template_xml($compressed,$dataset);
  Purpose    : Update template config XML in mart for specific dataset
  Returns    : Nothing
  Arguments  : A scalar containing compressed template XML data
  			   A current dataset name
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub update_template_xml {
	my ($self, $compressed,$dataset) = @_;
	my $sql = "UPDATE meta_template__xml__dm SET compressed_xml=? WHERE template = ?";
 	my @params = ();
 	
 	push @params, $compressed->{compressed};
 	push @params, $dataset;
 	
	$self->execute_statement($sql,\@params);
}

=head2 DESTROY

  Usage      : $db->DESTROY;
  Purpose    : Disconnects database and removes associated properties
  Returns    : Nothing
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub DESTROY {
	my $self = shift;
	if ($self->{db_handle}) {
		$self->{db_handle}->disconnect;
	}
}

=head2 get_scalar_value

  Usage      : $scalar = $db->get_scalar_value($sql,$parameters)
  Purpose    : Get scalar result from supplied SQL statement and parameters
  Returns    : Scalar value
  Arguments  : Select based SQL statement
               Arrayref of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_scalar_value {
	my ($self,$sql,$parameters) = @_;

	my $sth=$self->prepare_statement($sql,@{$parameters});	
	my @row=$sth->fetchrow_array;
	$sth->finish;
	
	return $row[0];
}

=head2 is_greater_than_zero

  Usage      : $boolean = $db->is_greater_than_zero($sql,$parameters)
  Purpose    : Determine if value obtained from supplied SQL statement and parameters is greater than zero 
  Returns    : 0 if not greater than zero and 1 if it is
  Arguments  : Select based SQL statement
               Arrayref of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub is_greater_than_zero {
	my ($self,$sql,$parameters) = @_;
	
	#fetch scalar of count
	my $rec_count = $self->get_scalar_value($sql, @{$parameters});

	#return true or false depending on count
	if ($rec_count>0) { return 1; }
	else { return 0; }
}

=head2 get_row_as_array

  Usage      : @array = $db->get_row_as_array($sql,$parameters)
  Purpose    : Get array of first record from supplied SQL statement and parameters
  Returns    : Array of first record
  Arguments  : Select based SQL statement
               Arrayref/scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub get_row_as_array {
	my ($self,$sql,@parameters)=@_;
	
	my $dbh = $self->{db_handle};
	
	my $sth=$dbh->prepare($sql);

	$sth->execute(@parameters) or die;
	
	my @row=$sth->fetchrow_array;
	return @row;
}

=head2 execute_statement

  Usage      : $db->execute_statement($sql,$parameters)
  Purpose    : Cleanly execute SQL (update or insert) with parameters
  Returns    : Nothing
  Arguments  : Select based SQL statement
               Arrayref/scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub execute_statement {
	my ($self,$sql,$parameters)=@_;
	
	my $dbh = $self->{db_handle};
	
	INFO("execute_statement: $sql\n");

	
	#get db handle and prepare SQL
	my $sth=$dbh->prepare($sql) or croak;
	
	$sth->{PrintError}  = 0;
	$sth->{HandleError} = sub {
	    GwasCentral::Mart::Exception::Db->throw("Error executing statement:$sql");
	};

	if (!$parameters) {
		$sth->execute or GwasCentral::Mart::Exception::Db->throw("Error executing statement:$sql");
	}
	elsif (ref($parameters) eq "SCALAR") {
		$sth->execute($parameters) or GwasCentral::Mart::Exception::Db->throw("Error executing statement:$sql");
	}
	elsif (ref($parameters) eq "ARRAY") {
		$sth->execute(@{$parameters}) or GwasCentral::Mart::Exception::Db->throw("Error executing statement:$sql");
	}
	
	$sth->finish;
}

=head2 prepare_statement

  Usage      : $db->prepare_statement($sql,$parameters)
  Purpose    : Prepare SQL with parameters
  Returns    : Statement handle for row retrieval etc.
  Arguments  : Select based SQL statement
               Arrayref or scalar of parameters
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub prepare_statement {
	my ($self,$sql,$parameters)=@_;
	
	my $dbh = $self->{db_handle};
	
	DEBUG("prepare_statement: $sql\n");
		
	#get db handle and prepare SQL
	my $sth=$dbh->prepare($sql) or die;

	$sth->{HandleError} = sub {
		GwasCentral::Mart::Exception::Db->throw("Error preparing statement");
	};
	
	if (scalar($parameters)) {
		
		$sth->execute($parameters);
	}
	else {
		$sth->execute(@{$parameters});
	}
	return $sth;
}

=head2 execute_script

  Usage      : $db->execute_script($script)
  Purpose    : Execute SQL script line by line
  Returns    : Nothing
  Arguments  : Array of SQL statements (see get_script)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub execute_script {
	my ($self,$script)=@_;
	my $logger = Log::Log4perl->get_logger();
	
	my $dbh = $self->{db_handle};
	my $line = 0;
	#execute each statement
	foreach my $query(@{$script}) {
		next if !$query;
		$line++;
		if ($query=~/^[alter|update].+_count/) {
        	DEBUG("execute_script: $line IGNORED - ignoring remaining lines $query");
        	last;
        }
        else {
			if (length(chomp($query)) != 0){ 
				warn "execute_script: $line - $query"; 
				$dbh->do($query) or GwasCentral::Mart::Exception::Db->throw("Error executing script (line $line)");
			}
        }
	}
}

=head2 get_script

  Usage      : $db->get_script($script_file)
  Purpose    : Open SQL script file and generate array of individual SQL statements.
               Process SQL to replace robmart and HGVbaseG2P with output and source dbs
               respectively.
  Returns    : Array of SQL statements
  Arguments  : Script file name
               Output db name
               Source db name
  Throws     : GwasCentral::Mart::Exception::Config if no script file found
  Status     : 
  Comments   : 

=cut

sub get_script {
	my ($self,$script_file,$outputdb_name,$sourcedb_name,$replaceid)=@_;
	my $logger = Log::Log4perl->get_logger();
	warn "get script:$script_file";
	#get script and separate into individual SQL statements
	open SCRIPT,"<",$script_file or GwasCentral::Mart::Exception::Config->throw("Unable to open script file ($script_file)");
	my $temp="";
	while (<SCRIPT>) { $temp.=$_; }
	
	my @sql = split(";\n",$temp);
	DEBUG("SourceDB:$sourcedb_name");
	
	#replace robmart with the outputdb
	my $src_replace_regex = $self->{src_replace_regex};
	my $mart_replace_regex = $self->{mart_replace_regex};
    if ($replaceid) {
    	INFO("Replacing <replace_id> with $replaceid in SQL");
    }
    
	foreach(@sql) {
		$_ =~ s/$mart_replace_regex/$outputdb_name/g;
		$_ =~ s/$src_replace_regex/$sourcedb_name/g;
		if ($replaceid) {
		    
			$_ =~ s/\<replace_id\>/$replaceid/g;
		}
	}
	
	return \@sql;
}

=head2 add_dataset_config

  Usage      : $db->add_dataset_confiupdate_conf_xmlg($dataset,$description,$visible);
  Purpose    : Create new dataset in mart config tables
  Returns    : Nothing
  Arguments  : Dataset name
               Description of dataset (appears in MartView)
               Whether dataset appears as visible (0 or 1)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub add_dataset_config {
	my ($self,$dataset,$description,$visible)=@_;
	my $dataset_id=$self->{curr_dataset_id}++;
	
	#create config in interface table
	my $sql="INSERT INTO `meta_conf__interface__dm` (dataset_id_key,interface) VALUES(?,?)";
	my @params = ( $dataset_id, "default");
	$self->execute_statement($sql,\@params);
	
	#create config in user table
	$sql="INSERT INTO `meta_conf__user__dm` (dataset_id_key,mart_user) VALUES(?,?)";
	@params = ( $dataset_id, "default");
	$self->execute_statement($sql,\@params);
	
	#create config in main table
	$sql="INSERT INTO `meta_conf__dataset__main` (dataset_id_key,dataset,display_name,type,visible,description,version) VALUES(?,?,?,?,?,'','')";
	@params = ( $dataset_id, $dataset, $description, "TableSet",$visible);
	$self->execute_statement($sql,\@params);
	
	#create config in xml table
	$sql="INSERT INTO `meta_conf__xml__dm` (dataset_id_key) VALUES(?)";
	@params = ($dataset_id);
	$self->execute_statement($sql,\@params);
	
	#create config in template table
	$sql="INSERT INTO `meta_template__template__main` (dataset_id_key,template) VALUES(?,?)";
	@params = ($dataset_id,$dataset);
	$self->execute_statement($sql,\@params);
	
	#create config in template xml table
	$sql="INSERT INTO `meta_template__xml__dm` (template) VALUES(?)";
	@params = ($dataset);
	$self->execute_statement($sql,\@params);
	
	#create config in template xml table
	$sql="INSERT INTO `meta_version__version__main` (version) VALUES(0.6)";
	$self->execute_statement($sql);
}

=head2 add_dataset_config

  Usage      : $db->add_dataset_confiupdate_conf_xmlg($dataset,$description,$visible);
  Purpose    : Create new dataset in mart config tables
  Returns    : Nothing
  Arguments  : Dataset name
               Description of dataset (appears in MartView)
               Whether dataset appears as visible (0 or 1)
  Throws     : 
  Status     : 
  Comments   : 

=cut

sub clear_configs {
	my ($self,$dataset,$description,$visible)=@_;
	
	my @script = ("TRUNCATE `meta_conf__interface__dm`",
	"TRUNCATE `meta_conf__user__dm`",
	"TRUNCATE `meta_conf__dataset__main`",
	"TRUNCATE `meta_conf__xml__dm`",
	"TRUNCATE `meta_template__template__main`",
	"TRUNCATE `meta_template__xml__dm`",
	"TRUNCATE `meta_version__version__main`");
	$self->execute_script(\@script);
}

=head2 import_tsv_file

  Usage      : $db->import_tsv_file($importfile,$importtable);
  Purpose    : Import TSV (tab separated values) file into a table
  Returns    : Nothing
  Arguments  : Import TSV file name 
               Table to import TSV file into
  Throws     : 
  Status     : 
  Comments   : Order of columns must be identical in both db table and TSV file 

=cut

sub import_tsv_file {
	my ($self, $importfile, $importtable) = @_;
	my $sql = "LOAD DATA LOCAL INFILE '$importfile' INTO TABLE $importtable FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n'";
	$self->execute_statement($sql);
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Database.pm 782 2008-06-26 13:39:21Z rcf8 $

=cut
