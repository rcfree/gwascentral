package GwasCentral::DSContainer;
use Moose::Role;
has 'DataSources' => ('is'=>'rw', 'default' => sub { {} });
use GwasCentral::DataSource::Util qw(DataSources_from_list);
sub DS {
	my ($self, $ds_name, $ds_object) = @_;
	if (defined($ds_object)) {
		$self->DataSources->{$ds_name}=$ds_object;
	}
	return $self->DataSources->{$ds_name};
}

sub populate_DS_from_list {
	my ($self, @ds_list) = @_;
	my $ds = DataSources_from_list(\@ds_list, $self->config);
	$self->DataSources($ds);
	return $ds;
}

sub populate_DS_from_context {
	my ($self, $context) = @_;
	my %ds = map { $_ => $context->model($_) } $context->models;
	$self->DataSources(\%ds);
	return \%ds;
}

sub populate_DS_from_config {
	my ($self) = @_;
	my @ds_list = split(/\ /,$self->config->{datasources});
	my $ds = DataSources_from_list(\@ds_list, $self->config, $self->AccessControl);
	$self->DataSources($ds);
	return $ds;
}

sub use_DS {
	my ($self,$ds_container) = @_;
	if ($ds_container->does("GwasCentral::DSContainer")) {
		$self->DataSources($ds_container->DataSources);
	}
	else {
		$self->throw("Object does not have the GwasCentral::DSContainer role");
	}
}

1;