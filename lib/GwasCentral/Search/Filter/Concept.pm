# $Id$

=head1 NAME

GwasCentral::Search::Filter::Concept


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Concept;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Text);
has '+retrieve_from' => ('default'=>'concept');
1;
