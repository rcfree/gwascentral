# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::JSONP;


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::JSONP;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Ignore);
has '+retrieve_from' => ('default'=>'jsonp');

after 'populate' => sub {
	my ($self) = @_;
	return if !$self->value;
	my $format = $self->Query->new_Format('jsonp');
	$self->Query->Format($format);
};
1;
