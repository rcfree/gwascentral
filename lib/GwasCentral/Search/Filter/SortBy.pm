# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::SortBy;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'o');
has '+label' => ('default' => 'Order results by');
has '+auto_submit' => ('default'=>1);
has '+fieldset'=>('default'=>'result_opts');
has '+hide_on_none' => ('default'=>1);

sub _list_builder { 
	return ();
};
1;
