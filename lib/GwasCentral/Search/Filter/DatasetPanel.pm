# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::DatasetPanel;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::None);
use Data::Dumper qw(Dumper);

has '+template' => ('default'=>'browserpanel');
has '+fieldset' => ('default'=>'browser_opts');
1;
