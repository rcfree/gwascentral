# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::SessionID;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Ignore);
use MIME::Base64;
use Storable qw/freeze thaw/;
use Data::Dumper qw(Dumper);
has '+retrieve_from' => ('default'=>'sessionid');
has '+fieldset' => ('default'=>'options');
has 'session_data' => ('is'=>'rw','builder' => '_build_session_data','lazy'=>1);

sub _build_session_data {
	my ($self) = @_;
	my $sessionid = $self->value;
	return undef if !$sessionid;
	my $sess_db = $self->Query->DS('Session');
	
	#unable to access session data from a completely different Apache instance
	#this method gets the binary representation of a session from the Session database, decodes it and returns it as a standard data structure
	my $session;
	if ($sess_db->can('dbh')) {
		$session = $sess_db->dbh->resultset('Sessions')->search({"id"=>"session:".$sessionid})->single;
	}
	else {
		$session = $sess_db->resultset('Sessions')->search({"id"=>"session:".$sessionid})->single;
	}
	if (!$session) {
		$self->log->info("session ".$sessionid." not found!"); return undef;
	}
	my $data = thaw( decode_base64($session->session_data) );
	$self->log->info("session_data:".Dumper($data));
	return $data;
}
1;
