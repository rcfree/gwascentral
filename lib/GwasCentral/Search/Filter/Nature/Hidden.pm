package GwasCentral::Search::Filter::Nature::Hidden;
use Moose::Role;
has 'template' => ('is'=>'rw','default'=>'hidden');

sub populate {
	my ($self, $params) = @_;
	
	if (ref($self->retrieve_from) =~ /ARRAY/) {
		foreach my $key(@{$self->retrieve_from}) {
			my $value = $params->{$key};
			defined($value) and $self->value($value);
		}
	}
	else {
		my $value = $params->{$self->retrieve_from};
		defined($value) and $self->value($value);
	}
}
1;