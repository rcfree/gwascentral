package GwasCentral::Search::Filter::Nature::CheckList;
use Moose::Role;
use strict;
use warnings;
has 'template' => ('is'=>'rw','default'=>'checklist');
has 'retrieve_from' => ('is'=>'rw', 'builder'=>'_build_available');
has 'value'=>('is'=>'rw', 'default' => sub { {} });
has 'number_of_columns' => ('is'=>'rw', 'default'=>'1');
has 'defaults' => ('is'=>'rw','builder' => '_build_defaults');
has '_indexed_hash' => ('is'=>'rw', 'default'=>sub { tie my %tracks, "Tie::Hash::Indexed"; return \%tracks });
has 'label_above' => ('is'=>'rw');

use Data::Dumper qw(Dumper);

sub populate {
	my ($self, $params) = @_;
	
	my @rf_keys = ref($self->retrieve_from) eq 'HASH' ? keys %{$self->retrieve_from} : @{$self->retrieve_from};
	$self->log->info("params before:".Dumper($params));
	foreach my $rf_key(@rf_keys) {
		$self->value->{$rf_key}=defined($params->{$rf_key}) ? $params->{$rf_key} : $self->defaults->{$rf_key};
	}
	$self->log->info("values in check_list: of ".Dumper($self->value));
}

sub _build_available {
	my ($self) = @_;
	tie my %hash, "Tie::Hash::Indexed";
	foreach my $checkitem(@{$self->_available_checklist}) {
		$hash{$checkitem->[0]}=$checkitem;
	}
	return \%hash;
}

sub _build_defaults {
	my ($self) = @_;
	return { map { $_ => 1 } @{ $self->_default_checklist() } };
}

sub is_on {
	my ($self, $track) = @_;
	return $self->value->{$track} == 1 ? 1 : undef; 
}

sub item_label {
	my ($self, $track) = @_;
	return $self->retrieve_from->{$track}->[1]; 
}

1;