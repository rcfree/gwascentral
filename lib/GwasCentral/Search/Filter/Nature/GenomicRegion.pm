package GwasCentral::Search::Filter::Nature::GenomicRegion;
use Moose::Role;
has 'chr'             => ( 'is' => 'rw' );
has 'start'           => ( 'is' => 'rw' );
has 'stop'            => ( 'is' => 'rw' );
has 'region'          => ( 'is' => 'rw', 'trigger' => \&_trigger_region );
has 'is_region'       => ( 'is' => 'rw' );
has 'sorted_features' => ( 'is' => 'rw' );
use Data::Dumper qw(Dumper);
use GwasCentral::Browser::Util qw(chr_length);

after 'populate' => sub {
	my ($self) = @_;
	my ( $is_region, $chr, $start, $stop ) = $self->extract_chr_coords;
	$self->is_region($is_region);
	$self->chr($chr);

	if ( !$stop ) {
		my $no = $self->chr_no;
		$no and $stop = chr_length($no);
	}
	$self->start($start);
	$self->stop($stop);
};

sub _trigger_region {
	my ( $self, $region ) = @_;
	$self->chr( $region->{chr} );
	$self->start( $region->{start} );
	$self->stop( $region->{stop} );
}

sub chr_no {
	my ($self) = @_;
	my $chr = $self->chr;
	$chr and $chr =~ s/[Cc]hr//;
	return $chr;
}

sub extract_chr_coords {
	my ($self) = @_;

	my ( $chr, $start, $stop, $type );
	my $keywords = $self->value;
	return undef if !$keywords;
	my @temp = split( /\:/, $keywords );
	$temp[0] =~ /^(chr|Chr)?([0-9|X|Y][0-9|X|Y]?)$/;
	my $length = chr_length($2);
	!$length and return undef;
	$chr = "chr$2";
	if ( @temp == 1 ) {
		#check for chr
		return ( 1, $chr, 1,  );
	}
	elsif ( @temp == 2 ) {
	
		#attempt to extract coordinates
		$temp[1] =~ /([\d|,]+)(\-|\.\.)([\d|,]+)/;
		return ( 1, $chr, $1, $3 );

	}
}
	1;
