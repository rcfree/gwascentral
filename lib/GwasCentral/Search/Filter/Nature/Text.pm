package GwasCentral::Search::Filter::Nature::Text;
use Moose::Role;
has 'template' => ('is'=>'rw','default'=>'text');
has 'autocomplete_url'=>('is'=>'rw'); #URL to autocomplete data (see 'lookup' method in Search.pm) (if not present does not set up autocomplete)

sub populate {
	my ($self, $params) = @_;
	my $value = $params->{$self->retrieve_from};
	$self->throw("Text/checkbox filter '".$self->name."' value cannot be an array (Items:".join(";",@{$value}).")") if ref($value) eq 'ARRAY';
	$self->value($value);
}
1;