package GwasCentral::Search::Filter::Nature::Checkbox;
use Moose::Role;
has 'template' => ('is'=>'rw','default'=>'checkbox');

sub populate {
	my ($self, $params) = @_;
	my $value = $params->{$self->retrieve_from};
	$self->throw("Text/checkbox filter '".$self->name."' value cannot be an array (Items:".join(";",@{$value}).")") if ref($value) eq 'ARRAY';
	$self->log->info("value of ".$self->name." is '".($value || "undef")."'");
	$self->value($value);
}
1;