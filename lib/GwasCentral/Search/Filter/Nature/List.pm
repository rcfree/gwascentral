package GwasCentral::Search::Filter::Nature::List;
use Moose::Role;
has 'template' => ('is'=>'rw','default'=>'list');
has 'list' => ('is'=>'rw', 'builder' => '_list_builder', 'lazy'=>1);
has 'key_index'=>('is'=>'rw', 'default' => 0);
has 'value_index'=>('is'=>'rw', 'default' => 1);
has 'html_onchange' => ('is'=>'rw');
has 'auto_submit'=>('is'=>'rw','default'=>undef); #specify whether to submit form upon change
has 'revert_to'=>('is'=>'rw','default'=>undef); #specify whether revert to a string upon submission
has 'submit_target' => ('is'=>'rw');
has 'lookup' => ('is'=>'rw');

use Data::Dumper qw(Dumper);

sub populate {
	my ($self, $params) = @_;
	my $value = $params->{$self->retrieve_from} || $self->default || $self->value;
	if (!defined($value)) {
		$self->value(undef);
		return;
	}
	
	$self->throw("List filter '".$self->name."' value cannot be an array (Items:".join(";",@{$value}).")") if ref($value) eq 'ARRAY';
	
	$self->throw("List '".$self->name."' not defined as array") if ref($self->list) ne 'ARRAY';
	my @list = @{$self->list || []};
	my %values = map { ref($_) eq 'ARRAY' ? $_->[$self->key_index] : $_ => $_ } @list;
	$self->lookup(\%values);
	
	$self->throw("List filter '".$self->name."' value '".$value."' is not in the available list:".Dumper($self->lookup)) if !$self->lookup->{$value};
	$self->value($value);
}

sub by_index {
	my ($self, $index) = @_;
	!$self->lookup and $self->throw("No lookup list provided in filter ".$self->name);
	my $tmp_value = $self->lookup->{$self->value};
	return $tmp_value->[$index];
}

sub list_value {
	my ($self,$value) = @_;
	my $tmp_value = $self->lookup->{$value};
	$tmp_value && return $tmp_value->[$self->value_index];
}
1;