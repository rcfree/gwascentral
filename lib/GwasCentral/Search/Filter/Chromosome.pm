# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Chromosome;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'chr');
has '+value'=>('default'=>'all');
has '+fieldset' => ('default'=>'options');
has '+label' =>('default'=>'Chromosome(s)');

sub _list_builder { 
	return [
		['all','All'],
		['1' , '1'],
		['2' , '2'],
		['3' , '3'],
		['4' , '4'],
		['5' , '5'],
		['6' , '6'],
		['7' , '7'],
		['8' , '8'],
		['9' , '9'],
		['10' , '10'],
		['11' , '11'],
		['12' , '12'],
		['13' , '13'],
		['14' , '14'],
		['15' , '15'],
		['16' , '16'],
		['17' , '17'],
		['18' , '18'],
		['19' , '19'],
		['20' , '20'],
		['21' , '21'],
		['22' , '22'],
		['X' , 'X'],
		['Y' , 'Y'],
	];
};

1;
