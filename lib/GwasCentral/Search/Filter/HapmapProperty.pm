# $Id$

=head1 NAME

GwasCentral::Search::Filter::HapmapPopulations


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::HapmapProperty;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'ld_property');
has '+value'=>('default'=>'all');
has '+fieldset' => ('default'=>'hapmap');
has '+label' =>('default'=>'Property');

tie my %list, 'Tie::Hash::Indexed';
sub _list_builder { 
	%list = (
		'd-prime' => 'd-prime',
		'rsquare' => "rsquare",
		'lod' => "lod",
	);
	return \%list;
};

1;
