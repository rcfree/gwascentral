# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::FivePrimeFlank;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'fpf');
has '+value'=>('default'=>'0');
has '+fieldset'=>('default'=>'options');
has '+label' =>('default'=>'5&prime; flank');
sub _list_builder { 
	return [
		['0' , 'none'],
		['1000' , '1Kb'],
		['5000' , '5Kb'],
		['10000' , '10Kb'],
		['50000' , '50Kb'],
		['100000' , '100Kb'],
	];
};
1;
