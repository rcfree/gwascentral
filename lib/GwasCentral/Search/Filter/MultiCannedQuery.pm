# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::MultiCannedQuery;
use Moose;
extends qw(GwasCentral::Search::Filter::CannedQuery);
use Data::Dumper qw(Dumper);
sub _list_builder { 
	my ($self) = @_;
	my $dbconfig;
	my @final_list;
	eval {
		$dbconfig = $self->Query->dbconfig;
	};
	if ($@) {
		$self->log->error("error:".$@);
		@final_list = ();
	}
	else {
		my %cq = %{$dbconfig->{CannedQueries} || {}};
		foreach my $cq_key(keys %cq) {
			my $curr_cq = $cq{$cq_key};
			
			my @temp = ($cq_key , $curr_cq->{label});
			my $cq_count = 1;
			while(my $curr_query = $curr_cq->{"query".$cq_count}) {
				push @temp, $curr_query;
				$cq_count++;
			}
			push @final_list, \@temp;
		}	
	}
	unshift @final_list,["","--choose a canned query--"];
	$self->log->info("canned_query:".Dumper(\@final_list));
	return \@final_list;
};

after 'populate' => sub {
	my ($self,$params_ref) = @_;
	my $query = $self->Query;
	
	my $qf = $query->fval('QueryFilter');
	my $cq_number = $query->CannedQueries->{$qf};
	if ($self->value) {
		$query->Format($query->new_Format('mart'));
		$query->Format->querystring($self->by_index($cq_number+1));
		$query->no_Format(1);
	}
};

1;