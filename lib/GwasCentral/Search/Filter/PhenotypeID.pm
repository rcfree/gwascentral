# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::PhenotypeID;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Ignore);

has '+retrieve_from' => ('default'=>'pid');
has '+fieldset' => ('default'=>'options');
has '+ignore_in_url' => ('default'=>1);

sub count {
	my ($self) = @_;
	my $val = $self->value;
	return '0' if !$val;
	if (ref($val) eq 'ARRAY') {
		return scalar(@{$self->value});
	}
	elsif (ref($val) eq 'HASH') {
		return scalar(keys %{$self->value});
	}
	else {
		return 1;
	}
}

1;
