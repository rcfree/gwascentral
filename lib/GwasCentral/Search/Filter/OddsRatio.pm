# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::OddsRatio;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Text);
has '+retrieve_from' => ('default'=>'or');
has '+value'=>('default'=>'3');
has '+fieldset'=>('default'=>'options');
has '+label' =>('default'=>'Odds ratio &ge;');

1;
