# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::MultiChr;
use Moose;
extends qw(GwasCentral::Search::Filter::Chromosome);
with qw(GwasCentral::Search::Filter::Nature::Hidden);
has '+retrieve_from' => ('default'=>'c');
has '+value'=>('default'=>'ALL');
has '+fieldset' => ('default'=>'more_options');
has '+label' =>('default'=>'Chromosome(s) to Display');
has '+template' => ('default'=>'multichr');
has 'chrs' => ('is'=>'rw');
use Data::Dumper qw(Dumper);
after 'populate' => sub {
	my ($self) = @_;
	if ($self->value eq 'ALL') {
		$self->chrs({map { $_->[0] => 1 } @{$self->list} });
		$self->log->info("chrs:".Dumper($self->chrs));
		return;
	}
	my @values = ref($self->value) eq 'ARRAY' ? @{$self->value} : ($self->value);
	my %selected_chrs = map { $_ => 1 } @values;
	$self->chrs(\%selected_chrs);
};

1;
