# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::RegionTracks;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::CheckList);

has '+fieldset' => ('default'=>'tracks');
has '+label' =>('default'=>'Tracks');
has '+number_of_columns' => ('default'=>'4');
has '+label_above' => ('default'=>1);
has '+ignore_in_url' => ('default'=>1);
sub _available_checklist {
	return [
		['Ideogram:overview','Ideogram','GBrowse'],
		['sig_markers_single','Significant markers for single studies ', 'RegionView'],
		['stacked','Stacked counts of significant markers','RegionView'],
		['line_trace','Line plot of maximum -log P values','RegionView'],
		['HGMD','HGMD Variants','GBrowse'],
		['gtsh','Hapmap SNPs', 'GBrowse'],
		['Genes','UCSC RefSeq Genes','GBrowse'],
		['sig_markers_multiple','Significant markers across all Result Sets','RegionView'],
		['present','Marker coverage for selected studies','RegionView'],
		['HapMapLDPlot','Hapmap LD plot','Plugin'],
	];
}

sub _default_checklist {
	return [
		'Ideogram:overview',
		'sig_markers_single',
		'stacked',
		'line_trace',
		'HGMD',
		'Genes'
	];
}

sub ttype {
	my ($self, $track) = @_;
	return $self->retrieve_from->{$track}->[2]; 
}

sub gbrowse_qstring {
	my ($self) = @_;
	my @qstring = ();
	push @qstring,"label=plugin:RegionView";
	foreach my $t(keys %{$self->retrieve_from}) {
		next if !$self->is_on($t);
		if ($self->ttype($t) eq 'GBrowse') {
			push @qstring,"label=".$t;
		}
		elsif ($self->ttype($t) eq 'Plugin') {
			push @qstring, "label=plugin:".$t;
		}
		else {
			push @qstring, $self->ttype($t).".".$t."=1";
		}
	}
	return @qstring;
}
1;
