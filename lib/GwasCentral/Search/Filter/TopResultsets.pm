# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::TopResultsets;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Hidden);
has '+html_id' => ('default'=>'top_resultsets');
has '+fieldset' => ('default'=>'options');
has '+template' => ('default'=>'topresultsets');
has 'more_than_max' => ('is'=>'rw');
sub populate {
}

sub push_resultsets {
	my ($self, $resultsets) = @_;
	return if $self->more_than_max;
	foreach my $rs ( @{$resultsets || [] }  ) {
		push @{$self->value}, $rs;
		if ( scalar(@{$self->value}) > 16 ) {
			$self->more_than_max(1);
		}
	}	
}
1;
