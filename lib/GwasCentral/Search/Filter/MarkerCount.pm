# $Id$

=head1 NAME

GwasCentral::Search::Filter::MarkerCount


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::MarkerCount;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'m');
has 'from' => ('is'=>'rw', 'default'=>'1');
has 'to' =>('is'=>'rw', 'default'=>'1000000');
has '+fieldset' => ('default'=>'options');
has '+value' => ('default'=>'all');

sub _list_builder { 
	[
		['0,1000','1-1,000'],
		['1001,10000','1,000-10,000'],
		['10001,10000','10,000-100,000'],
		['100001,1000000','>100,000'],
		['all','All']
	];
};
1;
