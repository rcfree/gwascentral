# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::CannedQuery;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'cq');
has '+fieldset' => ('default'=>'result_opts');
has '+label' => ('default'=>'Get data from GWAS Mart');
has '+template' => ('default' => 'cannedquery');
has '+revert_to' => ('default'=>"");
has '+auto_submit' => ('default'=>1);
has '+submit_target' => ('default'=>undef);
has '+hide_on_none' => ('default'=>1);

use Data::Dumper qw(Dumper);
sub _list_builder { 
	my ($self) = @_;
	my $dbconfig;
	my @final_list;
	eval {
		$dbconfig = $self->Query->dbconfig;
	};
	if ($@) {
		@final_list = ();
	}
	else {
		my %cq = %{$dbconfig->{CannedQueries} || {}};
		@final_list = map { [ $_ , $cq{$_}->{label}, $cq{$_}->{query}  ] } keys %cq;
	}
	unshift @final_list,["","--choose a canned query--"];
	return \@final_list;
};

after 'populate' => sub {
	my ($self,$params_ref) = @_;
	my $query = $self->Query;
	
	if ($self->value) {
		$query->Format($query->new_Format('mart'));
		$query->Format->querystring($self->by_index(2));
		$query->no_Format(1);
	}
};

1;
