# $Id$

=head1 NAME

GwasCentral::Search::Filter::Concept


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Study;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'study');


after 'populate' => sub {
	my ($self,$params) = @_;
	my $query = $self->Query;
	return if $query->lock_DetectType;
	if ($self->value) {
		$self->log->info("Study set to ".$self->value);
		$query->detector($query->new_DetectType('Keyword'));
	
		my $recog_as = 'Keyword';
		$recog_as and $query->recognised_as($recog_as);
		$self->log->info("Now recognised as '$recog_as'");
		$query->lock_DetectType(1);
	}
};
1;
