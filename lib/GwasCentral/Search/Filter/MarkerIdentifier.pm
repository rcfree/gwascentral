# $Id$

=head1 NAME

GwasCentral::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::MarkerIdentifier;
use Moose;
extends qw(GwasCentral::Search::Filter::SingleIdentifier);
with qw(GwasCentral::Search::Filter::Nature::Ignore);

has '+fieldset' => ('default'=>'options');
has '+ignore_in_url' => ('default'=>1);
has 'marker' => ('is'=>'rw', 'lazy'=>1,'builder'=>'_build_marker');
has 'coord' => ('is'=>'rw');
has '+retrieve_from' => ('default'=>'mid');

sub _build_marker {
	my ($self) = @_;
	my $id = $self->value;
	my ($marker, $coords);
	
	if ($id=~/^(.+)\:(.+)$/) {
		my $source = $1; my $accession = $2;
		($marker, $coords) = $self->Query->DS('Marker')->get_marker_and_refcoords_by_identifier($accession);
	}
	else {
		($marker, $coords) = $self->Query->DS('Marker')->get_marker_and_refcoords_by_identifier($id);
	}
	$self->coord($coords);
	$self->log->info("marker:".$marker);
	return $marker;
}

1;
