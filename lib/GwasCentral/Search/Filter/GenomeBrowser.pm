# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::GenomeBrowser;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'gb');
has '+fieldset' => ('default'=>'result_opts');
has '+label' => ('default'=>'View region in');
has '+revert_to' => ('default'=>"");
has '+auto_submit' => ('default'=>1);
has '+submit_target' => ('default'=>"_blank");
has '+hide_on_none' => ('default'=>1);

use Data::Dumper qw(Dumper);
sub _list_builder { 
	my ($self) = @_;
	my $q = $self->Query;
	my $qs = $q->f("QueryString");
	return [
		["","--choose a genome browser--",undef],
		["ucsc","UCSC Genome Browser","http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&db=hg19&position=chr".$qs->chr_no."&hgt.customText=".$q->host.$q->url_with({'format' => 'bed'})],
		["ensembl","ENSEMBL Genome Browser"]
	];
};

after 'populate' => sub {
	my ($self,$params_ref) = @_;
	my $query = $self->Query;
	
	if ($self->value) {
		$query->Format($query->new_Format('redirect'));
		$query->Format->querystring($self->by_index(2));
		$query->no_Format(1);
	}
};

1;
