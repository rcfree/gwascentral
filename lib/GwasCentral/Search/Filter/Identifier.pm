# $Id$

=head1 NAME

GwasCentral::Search::Filter::Concept


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Identifier;
use Moose;

extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::None);
has 'ignore' => ('is'=>'rw','default'=>undef);
has 'class' => ('is'=>'rw','default'=>undef);
1;
