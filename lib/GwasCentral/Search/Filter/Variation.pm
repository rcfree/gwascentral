# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Variation;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'vt');
has '+value'=>('default'=>'all');
has '+fieldset' => ('default'=>'options');
has '+label' =>('default'=>'Variation');

sub _list_builder { 
	return [
		['all' , 'All'],
		['indel','Indel'],
		['snp' , 'SNP'],
	];
};

1;
