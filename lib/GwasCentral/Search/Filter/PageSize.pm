# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::PageSize;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'page_size');
has '+value'=>('default'=>'50');
has '+label'=>('default'=>'No. per page');
has '+auto_submit' => ('default'=>1);
has '+fieldset'=>('default'=>'result_opts');
has '+hide_on_none' => ('default'=>1);

1;
