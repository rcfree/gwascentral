# $Id$

=head1 NAME

GwasCentral::Search::Filter::Concept


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::DTPage;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Hidden);
has '+retrieve_from' => ('default'=>'iDisplayStart');
has '+value'=>('default'=>'0');
has '+template'=>('default'=>undef);
has 'start_row' => ('is'=>'rw');
has 'number' => ('is'=>'rw');

sub populate {
	my ($self, $params) = @_;
	
	my $start = $params->{$self->retrieve_from} || $self->value;
	$self->value($start);
	if ($self->Query->page_size eq 'all') {
		$self->value('0');
		$self->number(1);
	}
	else {
		my $ps = $self->Query->page_size;
		if ($ps eq 'all') {
			
			$self->number(1);
		}
		else {
			my $page = ($start/$self->Query->page_size)+1;
			$self->log->info("page no is $page");
			$self->number($page);
		}
	}
	$self->start_row($start);
}


1;
