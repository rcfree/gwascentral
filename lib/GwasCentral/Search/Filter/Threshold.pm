# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::Threshold;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'t');
has '+value'=>('default'=>'3');
has '+fieldset'=>('default'=>'options');
has '+label' =>('default'=>'P-value threshold');
has '+is_category' =>('default'=>1);

sub _list_builder { 
	return [
		['ZERO' , '-log p &ge; 0', '0'],
		['1' , '-log p &ge; 1', '0.1'],
		['2' , '-log p &ge; 2', '0.01'],
		['3' , '-log p &ge; 3', '0.001'],
		['4' , '-log p &ge; 4', '0.0001'],
		['5' , '-log p &ge; 5', '0.00001'],
		['6' , '-log p &ge; 6', '0.000001'],
		['7' , '-log p &ge; 7', '0.0000001'],
		['8' , '-log p &ge; 8', '0.00000001'],
		['9' , '-log p &ge; 9', '0.000000001'],
		['10' , '-log p &ge; 10', '0.0000000001'],
	];
};

sub as_string {
	my ($self) = @_;
	if ($self->value) {
		return $self->by_index(2);
	}
}
1;
