# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::ScaleType;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'sc');
has '+value'=>('default'=>'linear');
has '+label'=>('default'=>'Stacked-plot scale type');
has '+fieldset'=>('default'=>'options');

sub _list_builder { 
	return [
		['linear', 'linear'],
		['log10', 'log10'],
	]
};

1;
