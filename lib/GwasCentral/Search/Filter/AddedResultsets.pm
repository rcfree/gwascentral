# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::AddedResultsets;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::Hidden);
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=2;
use GwasCentral::Browser::CustomResultset;
use Bio::Graphics::Panel;
use Tie::Hash::Sorted;

has '+retrieve_from' => ('default'=>'r[]');
has 'errors' => ('is'=>'rw','default'=>sub { {} });
has 'upload_data' => ('is'=>'rw');
has '+template' => ('default'=>'hiddencsv');
has 'resultset_list' => ('is'=>'rw', 'default' => sub { [] });
has 'colors' => ('is'=>'rw', 'builder'=>'_build_colors', lazy=>1);
has 'html_colors' => ('is'=>'rw', 'builder'=>'_build_html_colors', lazy=>1);
has 'numbers' => ('is'=>'rw');
has '+fieldset' => ('default'=>'options');
has 'more_than_max' => ('is'=>'rw');
has 'added_studies' => ('is'=>'rw', 'builder' => '_build_added_studies', lazy=>1);
has 'added_phenotypes' => ('is'=>'rw', 'builder' => '_build_added_phenotypes', lazy=>1);
has 'is_populated' => ('is'=>'rw');
has 'resultsets' => ('is'=>'rw', 'default' => sub { {} });
has '+html_id' => ('default'=>'resultsets');
has 'entity' => ('is'=>'rw', 'default'=>'studies');
has 'by_position' => ('is'=>'rw');
has '+hide_on_none' => ('default'=>1);
has 'resultsets_by_study' => ('is'=>'rw', 'default' => sub { {} });
has 'resultsets_by_phenotype' => ('is'=>'rw', 'default' => sub { {} });


sub _build_html_colors {
	my ($self) = @_;
	my %colors_for = ();
	my $rscounter = 0;
	my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
	my $panel = Bio::Graphics::Panel->new;
	foreach my $resultset(@{$self->resultset_list}) {
		my @rgb = $panel->color_name_to_rgb( $colors[$rscounter] );
		$colors_for{$resultset->identifier} = join(",",@rgb);
		$rscounter++;
	}
	return \%colors_for;
}

sub _build_colors {
	my ($self) = @_;
	tie my %colors_for, "Tie::Hash::Indexed";
	my $rscounter = 0;
	my @colors = split( /\ /, $self->config->{Browser}->{bgcolors} );
	foreach my $resultset(@{$self->resultset_list}) {
		$colors_for{$resultset->identifier} = $colors[$rscounter];
		$rscounter++;
	}
	return \%colors_for;
}

sub _build_added_studies {
	my ($self) = @_;
	my %astudies = ();
	my $rsets;
	eval {
		$rsets = $self->resultsets_without_uploads || {};
	};
	if($@) {
		$self->resultsets({});
		return {};
	}
	foreach my $rs(values %{$rsets}) {
		my $st_ident = $rs->study_ident;
		$astudies{$st_ident}=1;
	}

	return \%astudies;
}

sub _build_added_phenotypes {
	my ($self) = @_;
	my %aphenotypes = ();
	my $rsets;
	eval {
		$rsets = $self->resultsets_without_uploads || {};
	};
	if($@) {
		$self->resultsets({});
		return {};
	}
	foreach my $rs(values %{$rsets}) {
		next if $rs->accesslevel eq 'upload';
		my $pm_ident = $rs->experimentid->phenotypemethodid->identifier;
		$aphenotypes{$pm_ident}=1;
	}

	return \%aphenotypes;
}

sub update {
	my ($self) = @_;
	my $query = $self->Query;
	my @temp = ();
	$self->log->info("resultsets:".Dumper($self->value));
	if ($self->value) {
		if (ref($self->value) ne 'ARRAY') {
			@temp = split(",",$self->value);
			if (scalar(@temp)==1) {
				@temp = ($self->value);
			}
		}
		else {
			@temp = @{$self->value};
		}
	}
	tie my %resultset_for, "Tie::Hash::Sorted";
	
	my %rs_by_study = ();
	my %rs_by_phenotype = ();
	
	foreach my $identifier(@temp) {
		next if !$identifier || $identifier eq "";
		my $db = $query->DS('Study');
		
		if ($identifier eq 'none') {
			%resultset_for=();
			%rs_by_study=();
			last;
		}
		my $resultset = $db->get_resultset_by_identifier($identifier);
		my $study_ident = $resultset->study_ident;
		my $phenotype_ident = $resultset->phenotype_ident;
		
		$resultset_for{$identifier}=$resultset;
	
		$rs_by_study{$study_ident}||=[];
		push @{$rs_by_study{$study_ident}},$identifier;
		$rs_by_phenotype{$phenotype_ident}||=[];
		push @{$rs_by_phenotype{$phenotype_ident}},$identifier;
	}
	$self->resultsets_by_study(\%rs_by_study);
	$self->resultsets_by_phenotype(\%rs_by_phenotype);

	my $upload_data;
	
	if ($self->Query->has_f("SessionID")) {
		$self->log->info("sess id:".Dumper($self->Query->f("SessionID")));
		$self->log->info("has sessionid ".$self->Query->fval("SessionID"));
		my $sess = $self->Query->f('SessionID');
		my $sess_data = $sess->session_data;
		$upload_data = $sess_data->{upload_data};
	}
	
	if (!$upload_data) { $upload_data = $self->upload_data; }
	
	$self->log->info("upload_data:".Dumper($upload_data));
	if ($upload_data) {
		
		foreach my $uid (keys %{$upload_data}) {
			my $upload = GwasCentral::Browser::CustomResultset->new($upload_data->{$uid});
			$resultset_for{$uid}=$upload;
		}		
	}
	$self->by_position([values %resultset_for]);
	$self->resultset_list([values %resultset_for]);
	$self->resultsets(\%resultset_for);
}

after 'populate' => sub {
	my ($self) = @_;
	$self->update;
};

sub addable_study_state {
	my ($self, $study ) = @_;
	my $addable = 0;
	my $added = 0;
	my $astudies = $self->added_studies;
	my $cal = $study->{child_accesslevel};
	my $identifier = $study->{identifier};
	if ($cal && ($cal eq 'admin' || $cal eq 'full' || $cal eq 'limited')) {
		$addable = 1;
		
		if ($astudies->{$identifier}) {
			$added = 1;
		}

	}
	else {
		$added = -1;
	}
	
	return ($addable, $added);
}

sub addable_phenotype_state {
	my ($self, $phenotype ) = @_;
	my $addable = 0;
	my $added = 0;
	my $aphenotypes = $self->added_phenotypes;
	my $cal = $phenotype->{child_accesslevel};
	my $identifier = $phenotype->{identifier};
	if ($cal && ($cal eq 'admin' || $cal eq 'full' || $cal eq 'limited')) {
		$addable = 1;
		
		if ($aphenotypes->{$identifier}) {
			$added = 1;
		}

	}
	else {
		$added = -1;
	}
	
	return ($addable, $added);
}

sub resultsets_without_uploads {
	my ($self) = @_;
	my %resultsets = %{ $self->resultsets || {} };
	my @wout_uploads = grep { $resultsets{$_}->accesslevel ne 'upload' } keys %resultsets;
	return {map { $_ => $resultsets{$_} } @wout_uploads };
}

sub resultsets_with_uploads {
	my ($self) = @_;
	my %resultsets = %{ $self->resultsets || {} };
	my @wout_uploads = grep { $resultsets{$_}->accesslevel eq 'upload' } keys %resultsets;
	return {map { $_ => $resultsets{$_} } @wout_uploads };
}

sub resultsets_in_study {
	my ($self,$study) = @_;
	my $study_ident = $study->identifier;
	my $al = $study->accesslevel;
	
	my $arsets = $self->resultsets;
	my @all_rsets = map { $_->resultsets } $study->experiments;
	my %rs_data = ();
	foreach my $rset(@all_rsets) {
		my $identifier = $rset->identifier;
		
		my $addable = 0;
		my $added = 0;
		
		if ($al && ($al eq 'admin' || $al eq 'full' || $al eq 'limited')) {
			$addable = 1;
			
			if ($arsets->{$identifier}) {
				$added = 1;
			}
	
		}
		else {
			$added = -1;
		}
		my $temp = 
		$rs_data{$identifier}={
			identifier => $identifier,
			added => $added,
			addable => $addable,
			resultsets => [map { $_->identifier } @all_rsets]
		};
	}
	return (\%rs_data,[map { $_->identifier } @all_rsets]);
}



1;
