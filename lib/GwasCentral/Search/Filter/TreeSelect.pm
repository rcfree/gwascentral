# $Id$

=head1 NAME

GwasCentral::Search::Filter::TreeSelect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::TreeSelect;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);

has '+retrieve_from' => ('default'=>'treeselect');
has '+template' => ('default'=>'radio');

sub _list_builder {
	my $self = shift;
	return [
	 ['mesh', 'Medical Subject Headings (MeSH)', 'gettree(this.value)'],
	 ['hpo', 'Human Phenotype Ontology (HPO)','gettree(this.value)']
	];
}

1;
