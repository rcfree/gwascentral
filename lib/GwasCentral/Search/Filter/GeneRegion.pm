# $Id$

=head1 NAME

GwasCentral::Search::Filter::Threshold


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter::GeneRegion;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::List);
has '+retrieve_from' => ('default'=>'g');
has '+value'=>('default'=>'all');
has '+fieldset' => ('default'=>'options');
has '+label' =>('default'=>'Gene Region(s)');

tie my %list, 'Tie::Hash::Indexed';
sub _list_builder { 
	%list = (
		'all' => 'All',
		'1' => "5-prime UTR",
		'2' => "3-prime UTR",
		'3' => 'intron',
		'4' => 'exon',
	);
	return \%list;
};

1;
