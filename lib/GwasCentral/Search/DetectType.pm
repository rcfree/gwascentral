# $Id$

=head1 NAME

GwasCentral::Search::Detect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType;
use Moose;
extends qw(GwasCentral::Base);
has 'Query' => ('is'=>'rw');
has 'label' => ('is'=>'rw');
has 'no_display' => ('is'=>'rw');

sub has_config {
	return undef;
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name =~ /GwasCentral::DetectType::(.+)/;
	return $name;
}

sub get_biggest_feature_region_for_term {
	my ($self, $term, $search_method) = @_;
	my @features = $self->Query->DS('Feature')->$search_method($term);
	return undef if @features == 0;	

	$self->log->info("feature count:".@features);
	my @feature_sizes;
	foreach my $feature (@features) {
		next if $feature->source ne 'UCSC';
		next if $feature->ref =~ /_/;
		my $feature_size = $feature->end - $feature->start;
		push @feature_sizes,
		  { size => $feature_size, feat => $feature };
	}
	my @features_sorted_by_size =
		reverse sort { $a->{size} <=> $b->{size} } @feature_sizes;
	my $biggest_feature = $features_sorted_by_size[0]->{feat};
	my $chr   = $biggest_feature->ref;
	$chr!~m/(chr|Chr)/ and $chr="chr$chr";
	my $start = $biggest_feature->start;
	my $stop  = $biggest_feature->end;
	my $biggest_region = { chr => $chr, start => $start, stop => $stop, coords => $chr.":".$start."..".$stop };
	return ($biggest_region, \@features_sorted_by_size);
}

1;