# $Id$

=head1 NAME

GwasCentral::Search::Performer::AutoDetect


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Detector::Auto;
use Moose;
use strict;
use warnings;
extends qw(GwasCentral::Search::Detector);
	
sub perform {
	my ($self) = @_;
	my $query = $self->Query;
	
	if (!$query->DetectType && !$query->lock_DetectType) {
		my $qs_filter = $query->f('QueryString');
		
		my $dt_mod;
		my $recog_as;
		$self->throw("No 'possible_detectors' defined in $self") if scalar(@{$query->possible_DetectTypes})==0;
		
		foreach my $detector(@{$query->possible_DetectTypes}) {
			$dt_mod = $query->new_DetectType($detector);
			$self->log->info("attempt to detect:$detector - '".($qs_filter->value || "undef")."'");
			$recog_as = $dt_mod->detect($qs_filter->value);
			$self->log->info("Detected as $recog_as") if $recog_as;
			last if $recog_as;
		}
		$self->throw("Could not detect your QueryString value (possible_detectors:".join(";",@{$query->possible_DetectTypes}).")") if !$recog_as;
		$query->DetectType($dt_mod);
		$query->recognised_as($recog_as);
	}
			
	my $retrieve_method = "retrieve_".lc($query->recognised_as);
	my $attrs = $query->$retrieve_method;
	return $query->get_data($attrs); 

}
