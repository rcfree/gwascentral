# $Id: RegionMarkers.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::RegionMarkers - Query marker results in a specific region for selected resultsets

=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::RegionMarkers;
use Moose;
extends qw(GwasCentral::Search::Query);
has 'resultset_data' => ( 'is' => 'rw' );
has 'resultset_level'=>('is'=>'rw');
has 'markers'=>('is'=>'rw');
no Moose;

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Browser::Util qw(inv_neglog);

sub _possible_options {
	return (
		browser=>'browser object'
	);
}

sub default_page_size {
	return 'all';
}

sub allowed_page_sizes {
	return qw(all);
}

sub no_filters {
	return 1;
}

sub required_db {
	return {
		'Xapian'     => 'xapian_db',
		'BrowserDB'  => 'browser_db',
		'SeqFeature' => 'feature_db',
		'MarkerDB'   => 'marker_db',
	};
}

sub results_from_database {
	my ($self) = @_;
	my $browser = $self->option('browser');
	my $threshold = $browser->threshold || '0';    #default to zero threshold
	my $rsidentifier = $browser->resultsets;
	
	my $sigids = $browser->get_markers_in_resultsets(1);
	my $marker_count = keys %{$sigids};
	my $pager = $self->new_pager($marker_count);
	$self->pager($pager);
	$self->markers($sigids);
	return $sigids;
}

sub _convert_data_to_entries {
	my ($self) = @_;
	my %data     = %{ $self->markers };
	my $results  = [];
	my $base_url = $self->config->{WebSite}->{hgvbaseg2p_baseurl};
	my $counter  = 1;
	my $browser = $self->option('browser');
	my $rsets = $browser->get_resultset_summaries;
		
	foreach my $marker_ident (keys %data) {
		my $sigs = $data{$marker_ident};
		
		my $marker_hash = {
			accession => $sigs->{accession},
			identifier => $marker_ident,
			start => $sigs->{start},
			stop => $sigs->{stop},
			chr => $browser->chr
		};
		
		foreach my $rs_ident (keys %{$sigs->{resultsets}}) {
			my %sigs_for = %{$sigs->{resultsets}};
			my $rset = $rsets->{$rs_ident};
			my $exp = $rset->experimentid;
			my $study = $exp->studyid;
			
			my %sig_data_for = %{$marker_hash};
			$sig_data_for{'pvalue'}=$sigs_for{$rs_ident}->{unadjustedpvalue};
			$sig_data_for{'study'}=$study->name . "(". $study->identifier . ")";
			$sig_data_for{'experiment'}=$exp->name . "(" . $exp->identifier . ")";
			$sig_data_for{'resultset'}=$rset->name . "(" . $rset->identifier . ")";

		push @{$results},
		  {
			id      => $base_url."/marker/" . $marker_ident ."/results?rfilter=".$rset->identifier,
			title   => "Marker " . $marker_ident  ."(".$sigs->{accession} . ") used in Result Set " . $rset->name ." (" . $rset->identifier . ")",
			summary => \%sig_data_for,
			  link  => $base_url."/marker/" . $marker_ident ."/results?rfilter=".$rset->identifier,
			content => \%sig_data_for,
			updated => '2009-01-01'
		  };
			
		}
	}
	return $results;
}
	

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: RegionMarkers.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut

