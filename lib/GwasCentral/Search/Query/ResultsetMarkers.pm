# $Id: ResultsetMarkers.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::ResultsetMarkers - Query top markers in supplied resultset

=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::ResultsetMarkers;
use Moose;
extends qw(GwasCentral::Search::Query);

has '+Detector_name' => ( 'default' => 'None' );

use Moose;
extends qw(GwasCentral::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Browser::Util qw(neglog);

has '+url_path' => ( 'default' => 'resultset' );
has '+possible_Filters' => (
	default => sub {
		[
			qw(Page ExportFormat ResultsetIdentifier PageSize)
		];
	}
);
has 'resultset' => ('is'=>'rw');
has '+Filter_options' => (
	'default' => sub {
		return {
			'PageSize' => {
				'list' => [
					[ '20',  '20' ],
					[ '50',  '50' ],
					[ '100', '100' ],
					[ 'all', 'all' ],
				],
				'default' => '50',
				'fieldset' => 'none',
			},
			'Page' => {
				'template' => 'markerspage',
				'fieldset' => 'dataset_opts',
			},
			'ExportFormat' => {
				'fieldset'=>'dataset_opts'
			}
		};
	}
);

sub name {
	my ($self) = @_;
	return "DatasetMarkers_".$self->fval('ResultsetIdentifier');
}

sub get_data {
	my ($self) = @_;
	if ($self->fval('Page') != 1) {
		$self->throw("Only top markers can be accessed");
	}
	$self->fval( 'Page', 1 );
	
	my $rs_filter = $self->f('ResultsetIdentifier');
	my $rank = 1;
	my $rset = $rs_filter->resultset;
	my @data;
	my $sigs_rs = $rset->significances->search( {},
	{ page => 1, rows => $self->page_size, order_by => 'unadjustedpvalue', prefetch => 'usedmarkersetid'} );
	next if !$sigs_rs;
	my @sigdata = ();
	my @hotlinks = $rset->hotlinks->all;
	my $rset_ident = $rset->identifier;
	while ( my $sig=$sigs_rs->next) {
		my $mid = $sig->usedmarkersetid->markeridentifier;
		my $marker = $self->DS('Browser')->get_marker_by_id($mid);
		
		my $markerstripped = $marker->{Accession};
		$markerstripped =~ s/rs//;
		
		my @ext_links = map {
			{ 
				label => $_->hotlinklabel,
				link => ($_->urlprefix ? $_->urlprefix : "") . $markerstripped . ($_->urlsuffix ? $_->urlsuffix : "")
			}
		} @hotlinks;
		tie my %content, "Tie::Hash::Indexed";
		%content = (
			rank => $rank,
			identifier => $mid,
			resultset => $rset_ident,
			accession => $marker->{Accession},
			chr => $marker->{Chr},
			start => $marker->{Start},
			stop => $marker->{Stop},
			pvalue => $sig->unadjustedpvalue,
			neglog_pvalue => sprintf("%.3f",neglog($sig->unadjustedpvalue)),
			hotlinks => \@ext_links,
			link => $self->host."/markers/".$mid."/results/".$rset_ident,
			title => "Association Result for Marker ".$mid." (dbSNP:".$marker->{Accession}.") in data set ".$rset_ident,
		);
		$rank++;
		push @data, \%content;
	}
	$self->pager($sigs_rs->pager);
	return \@data;
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: ResultsetMarkers.pm 1560 2010-10-28 15:55:59Z rcf8 $ 

=cut

