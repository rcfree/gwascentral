# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::Phenotype - Query for phenotype data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Phenotypes::Term;

	use Moose;
	extends qw(GwasCentral::Search::Query::Phenotypes);

	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use Text::ParseWords;
	use GwasCentral::Base qw(pluralize);
	has '+url_path'=>('default' => 'phenotypes/term');
	has '+possible_Filters' => (default => sub { [
		qw(QueryString Threshold PhenotypeID SearchSubmit Page SortBy PageSize CannedQuery ExportFormat IdentifierList AddedResultsets TopResultsets DatasetPanel)]
	});
	has '+possible_DetectTypes' => ('is'=>'rw','default' => sub { ['Annotation'] });
	has '+Filter_options' => ('default'=> sub {
		 return {
			'SortBy' => {
				'list' => [
					 [1, 'Phenotype (ascending)', 'phenotypepropertyid.name'],
					[2, 'Phenotype (descending)', 'phenotypepropertyid.name'],
					[3, 'Identifier (ascending)', 'me.identifier'],
					[4, 'Study Name (ascending)', 'studyid.name'],
					[5, 'Identifier (descending)','me.identifier DESC'],
					[6, 'Study Name (descending)', 'studyid.name DESC'],
					[7,'Number of markers (ascending)', '(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported DESC limit 1)'],
					[8,'Number of markers (descending)',  '(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported DESC limit 1) desc'],
				],
				'default' => 1
			},
			'PageSize' => {
				'list' => [
					['20','20'],
					['50','50'],
					['100','100'],
					['all','all'],
				],
				'default' => '50'
			},
			'MarkerCount' => {
				'template'=>'hidden'
			},
			'QueryString' => {
				'autocomplete_url'=>'/phenotypes/meshconcept',
				'legend'=>'&_default_legend',
				'label' => 'Enter a phenotype term',
			},
			'Threshold' => {
				'default'=>'3'
			},
			'AddedResultsets' => { 
				'template' => 'addedresultsets',
				'fieldset' => 'result_opts',
				'entity' => 'phenotypes',
			 },
		}
	});
	
	sub _default_legend {
		my ($self) = @_;
		my $legend = qq^Autocompletes with <a href="javascript:void(0)" onclick="showHelp('what_are_mesh_terms')" title="[%- tt.phenotypes_mesh -%]">MeSH<img src="/images/options/infolink.png" alt="info"/></a> ^;
		if ($self->config->{hpo}) {
			 $legend .= qq^and <a href="javascript:void(0)" onclick="showHelp('what_are_hpo_terms')" title="[%- tt.phenotypes_hpo -%]">HPO<img src="/images/options/infolink.png" alt="info"/></a> ^;
		}
		$legend .= "terms used in annotations";
		return $legend;
	}
	
	override 'text' => sub {
		my ($self) = @_;
		return $self->label." match MeSH/HPO term <b>".$self->fval('QueryString')."</b> and contain data with <b>".$self->f('Threshold')->by_index(1)."</b>";
	};

	
sub retrieve_annotation {
}

sub retrieve_concept {
}

sub retrieve_region {
	my ($self) = @_;
	$self->retrieve_gene;
}

sub retrieve_gene {
	my ($self)        = @_;
	my $qs   = $self->f('QueryString');
	my $threshold = $self->fval('Threshold');    #default to zero threshold
	
	my ( $studies, $rs_data, $m_data ) = $self->DS('Browser')
	  ->get_studies_rsets_and_markers_in_region_above_threshold(
		{
			threshold => $threshold,
			chr       => $qs->chr_no,
			start     => $qs->start,
			stop      => $qs->stop,
		}
	  );

	 my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('rs');
	 $id_filter->value($rs_data);
}

sub retrieve_keyword {
	my ($self)        = @_;
	
	my $qs_value = $self->fval('QueryString');
	my $xap       = $self->DS('Xapian');
	
# Send query with supplied query string to text search engine. This may cause an exception
# to be thrown (e.g. invalid query syntax), so let's catch that
#grab list of identifiers from Xapian database and then retrieve pmethod object for each

	my ( $total, $pmethod_identifiers ) =
	  $xap->search_all( $qs_value, 'phenotypes');
	$self->log->info("pmethods:",join(",",@{$pmethod_identifiers}));
	# and then retrieve phenotype object for each
	$pmethod_identifiers ||= [];
	my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('pm');
	 $id_filter->value($pmethod_identifiers);
}

sub retrieve_marker {
	my ($self) = @_;
	my $qs = $self->fval('QueryString');
	my $qs_filter_value = $qs->value;
	my $threshold = $self->fval('Threshold');    #default to zero threshold
	my $marker = $qs->marker;
	!$marker and return ();
	#markers already retrieved by 'recognise_qstring' - but need identifier and chr to do lookup
	my $identifier = $marker->{marker}->identifier;
	my $chr            =  $marker->{coords} ? $marker->{coords}->chr : 'Y';

	my ( $studies, $rs_data, $m_data ) =
			  $self->DS('Browser')->get_studies_rsets_and_markers_by_identifier_above_threshold(
				{
					threshold  => $threshold,
					identifier => $identifier,
					chr        => "chr$chr"
				}
			 );
	warn "rs_data:".Dumper($rs_data);
	my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('rs');
	 $id_filter->value($rs_data);
}

sub retrieve_none {
	my ($self)        = @_;
	$self->no_results(1);
	$self->no_options(1);
}
1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

