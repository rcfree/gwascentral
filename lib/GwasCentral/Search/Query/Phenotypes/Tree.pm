# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::Phenotype - Query for phenotype data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Phenotypes::Tree;

	use Moose;
	extends qw(GwasCentral::Search::Query::Phenotypes);

	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use GwasCentral::Base qw(pluralize);
	has '+url_path'=>('default' => 'phenotypes');
	has '+possible_Filters' => (default => sub { [
		qw(Concept Threshold PhenotypeID Page SortBy PageSize CannedQuery ExportFormat IdentifierList AddedResultsets TopResultsets DatasetPanel TreeSelect QueryString)]
	});
	has '+possible_DetectTypes' => ('is'=>'rw','default' => sub { ['ConceptTerm'] });	
	has '+Filter_options' => ('default'=> sub {
		 return {
			'SortBy' => {
				'list' => [
					 [1, 'Phenotype (ascending)', 'phenotypepropertyid.name'],
					[2, 'Phenotype (descending)', 'phenotypepropertyid.name'],
					[3, 'Identifier (ascending)', 'me.identifier'],
					[4, 'Study Name (ascending)', 'studyid.name'],
					[5, 'Identifier (descending)','me.identifier DESC'],
					[6, 'Study Name (descending)', 'studyid.name DESC'],
					[7,'Number of markers (ascending)', '(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported DESC limit 1)'],
					[8,'Number of markers (descending)',  '(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported DESC limit 1) desc'],
				],
				'default' => 1
			},
			'PageSize' => {
				'list' => [
					['20','20'],
					['50','50'],
					['100','100'],
					['all','all'],
				],
				'default' => '50'
			},
			'MarkerCount' => {
				'template'=>'hidden'
			},
			'Threshold' => {
				'default'=>'ZERO'
			},
			'AddedResultsets' => { 
				'template' => 'addedresultsets',
				'fieldset' => 'result_opts',
				'entity' => 'phenotypes',
			 },
			'TreeSelect' => {
				'fieldset' => 'treeselect',
				'label' => undef,
				'value' => 'mesh',
			},
		}
	});
		
	override 'text' => sub {
	my ($self) = @_;
	my $count = $self->f('PhenotypeID')->count;
	return $self->label." match ".pluralize($count,"MeSH tree category" , "MeSH tree categories");
};

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

