# $Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Results::MarkerResults - Query for marker association results


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Browser::TopMarkers;
use Moose;
extends qw(GwasCentral::Search::Query);
has '+Detector_name' => ( 'default' => 'None' );

use Moose;
extends qw(GwasCentral::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Search::Util qw(new_Query);

has '+url_path' => ( 'default' => 'browser/tmarkers' );
has '+possible_Filters' => (
	default => sub {
		[
			qw(PageSize ExportFormat Page AddedResultsets)
		];
	}
);

has '+Filter_options' => (
	'default' => sub {
		return {
			'PageSize' => {
				'list' => [
					[ '20',  '20' ],
					[ '50',  '50' ],
					[ '100', '100' ],
					[ '500', '500' ],
					[ '1000', '1000' ],
				],
				'default' => '50',
				'label' => 'Up to top markers in Study'
			},
			'ExportFormat' => {
				'fieldset' => 'none',
			},
			'Page' => {
				'template' => 'none'
			}
		};
	}
);

has 'Queries' => ('is'=>'rw', 'default' => sub { tie my %hash, "Tie::Hash::Indexed"; return \%hash; });


no Moose;

sub get_data {
	my ($self)    = @_;
	$self->fval( 'Page', 1 );


	my @results = ();
	my $rs_filter = $self->f('AddedResultsets');
		
	my $rsets = $rs_filter->resultsets;
	$self->data({});
	my @rset_idents = keys %{$rsets || {} };
	foreach my $rset_ident(@rset_idents) {
		my $rset = $rsets->{$rset_ident};
		my $q = new_Query( 'ResultsetMarkers', { DataSources => $self->DataSources, conf_file => $self->config } );
		$q->Filter_options->{PageSize}=$self->Filter_options->{PageSize};
		my $params = $self->params;
		$params->{rid} = $rset_ident;
		$q->params($params);
		$q->prepare_Filters();
		$q->results;
		$self->Queries->{$rset_ident} = $q;
	}
	if ($rset_idents[0]) {
		return $self->Queries->{$rset_idents[0]}->data;
	}
	else {
		return [];
	}
}

sub convert_data_to_entries {
	my ( $self, $data ) = @_;

	
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $ 

=cut

