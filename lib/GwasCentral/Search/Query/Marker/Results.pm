# $Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Results::MarkerResults - Query for marker association results


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Marker::Results;
use Moose;
extends qw(GwasCentral::Search::Query);
has '+Detector_name' => ( 'default' => 'None' );

use Moose;
extends qw(GwasCentral::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Browser::Util qw(neglog);

has '+url_path' => ( 'lazy' => 1, 'default' => sub { my ($self) = @_; return 'marker/' . $self->fval('MarkerIdentifier')."/results"; } );

has '+possible_Filters' => (
	default => sub {
		[
			qw(Threshold Page PageSize MarkerIdentifier ExportFormat ResultsetsFilter SearchSubmit TopResultsets AddedResultsets DatasetPanel)
		];
	}
);

has '+Filter_options' => (
	'default' => sub {
		return {
			'PageSize' => {
				'list' => [
					[ '20',  '20' ],
					[ '50',  '50' ],
					[ '100', '100' ],
					[ 'all', 'all' ],
				],
				'default' => '50'
			},
			'Threshold' => { 'default' => '3' },
			'AddedResultsets' => { 
				'template' => 'addedresultsets',
				'fieldset' => 'result_opts',
			 },
		};
	}
);
has 'region'  => ( 'is' => 'rw' );
has 'markers' => ( 'is' => 'rw' );

override 'text' => sub {
	my ($self) = @_;
	return $self->label." with <b>".$self->f('Threshold')->by_index(1)."</b>";
};

no Moose;

sub get_data {
	my ($self)    = @_;
	my $mi     = $self->f('MarkerIdentifier');
	my $threshold = $self->fval('Threshold');         #default to zero threshold
	my $rfilter   = $self->fval('ResultsetsFilter');
	my $ar_filter = $self->f('AddedResultsets');
	my %resultsets = $rfilter ? map { $_ => 1 } split( ",", $rfilter ) : ();

	$self->pager(undef);

	my @results = ();

#retrieve G2P association results where this marker is tested which pass the threshold
	my $marker = $mi->marker;
	my $coord = $mi->coord;

	my @marker_results = ( { marker => $marker, coords => $coord } );
	my $start = $coord ? $coord->start - 20000 : undef;
	my $stop  = $coord ? $coord->stop + 20000  : undef;
	$coord and $self->region(
		{
			chr    => "chr" . $coord->chr,
			start  => $start,
			stop   => $stop,
			coords => "chr" . $coord->chr . ":" . $start . ".." . $stop
		}
	);
	$self->markers( \@marker_results );

	if ( !$coord ) {
		return;
	}
	my @studies = ();

#loop through each significance and work out which study, experiment and resultset they are present in
	my @sigs =
	  $self->DS('Study')->get_marker_sigs_gt_threshold( $marker->identifier, $threshold );

	#	$self->log->info("studies:".Dumper(\@sigs));
	#	my $studies = {};
	my @data = ();
	my $rs_counter = 0;
	my ( $start_row, $end_row ) = $self->calc_row_start_and_end;
	foreach my $sig (@sigs) {
		my $rset  = $sig->resultsetid;
		my $exp   = $rset->experimentid;
		my $study = $exp->studyid;
		$self->DS('Study')->populate_study_access_levels($study);
		if ( $study->accesslevel eq 'limited' ) {
			next if !$sig->islimited;
		}

		$rs_counter++;

		next if $rs_counter < $start_row;
		next if $rs_counter > $end_row;
		
		my @rs_in_study =
		  map { $_->identifier } map { $_->resultsets } $study->experiments;
		my $mid = $self->markers->[0]->{marker}->identifier;
		
		my $pvalue = $sig->unadjustedpvalue;
		my $efsz   = $sig->usedmarkersetid->effectsizes->search({'type' => 'OR'});
		my $or     = $efsz->first;
		
		my $item = {
			'study_identifier'      => $study->identifier,
			'study_name'            => $study->name,
			'experiment_identifier' => $exp->identifier,
			'experiment_name'       => $exp->name,
			'phenotype' => $exp->phenotypemethodid->phenotypepropertyid->name,
			'phenotype_identifier' => $exp->phenotypemethodid->identifier,
			resultset   => $rset->name,
			identifier => $rset->identifier,
			analysis_method      => $rset->analysismethod->name,
			'accesslevel'        => $study->accesslevel,
			'child_accesslevel' => $study->child_accesslevel,
			'resultsets' => join( ",", @rs_in_study ),
			'pvalue' => $pvalue,
			'neglog_pvalue' => sprintf("%.3f",neglog($pvalue)),
			'marker_identifier' => $mid,
			or_value => $or ? $or->value : "Not supplied",
			or_lower95 => $or ? $or->lower95bound : "Not supplied",
			or_upper95 => $or ? $or->upper95bound : "Not supplied",
			or_description => $or ? $or->description : "Not supplied",
			risk_allele => $or ? $or->riskallele : "NS",
			risk_allele_freq => $or ? $or->riskallelefreq : "NS",
		};

		push @data, $item;
	
	}
	$self->new_pager($rs_counter);

	return \@data
}

sub convert_data_to_entries {
	my ( $self, $data ) = @_;

	my $entries;
	my @results  = ();
	my $base_url = $self->config->{WebSite}->{hgvbaseg2p_baseurl};
	my $counter  = 0;

	my $studies;

	#my $tr             = $self->f('TopResultsets');
	my $rs_filter      = $self->f('AddedResultsets');

	foreach my $item (@{$data}) {
		
		my ( $is_addable, $is_added ) = $rs_filter->addable_study_state(
			{
				identifier        => $item->{study_identifier},
				accesslevel       => $item->{accesslevel},
				child_accesslevel => $item->{accesslevel},
			}
		);
		$item->{addable}=$is_addable;
		$item->{added}=$is_added;
		my @results = ();
		#$is_addable and $tr->push_resultsets( $item->{resultsets} );
		if (!$is_addable) {
			$item->{pvalue}="Not available";
			$item->{neglog_pvalue}="Not available";
		}
	}
	$counter++;
}

sub calc_row_start_and_end {
	my ($self)    = @_;
	my $start_row = ( $self->page * $self->page_size ) - $self->page_size + 1;
	my $end_row   = $start_row + $self->page_size - 1;
	return ( $start_row, $end_row );
}


1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $ 

=cut

