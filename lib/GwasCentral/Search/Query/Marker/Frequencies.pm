# $Id: MarkerFrequencies.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::MarkerFrequencies - Query for marker frequencies

=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::MarkerFrequencies;
use Moose;
extends qw(GwasCentral::Search::Query);
has 'marker'             => ( 'is' => 'rw' );
has 'reported_alleles'   => ( 'is' => 'rw' );
has 'reported_genotypes' => ( 'is' => 'rw' );
no Moose;

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;

sub _possible_options {
	return (
		id => 'marker identifier',
		t => 'threshold',
		v => 'view',
		rfilter => 'resultset identifier',
	);
}

sub accepted_parameters {
	return qw(q);
}

sub required_db {
	return {
		'Xapian'     => 'xapian_db',
		'BrowserDB'  => 'browser_db',
		'SeqFeature' => 'feature_db',
		'MarkerDB'   => 'marker_db',
	};
}

sub results_from_database {
	my ($self) = @_;
	my $id = $self->option('id');
	my $threshold = $self->option('t') || '0';    #default to zero threshold
	$self->pager(undef);

	my @results = ();

#retrieve G2P association results where this marker is tested which pass the threshold
	my ( $marker, $coord ) =
	  $self->marker_db->get_marker_and_refcoords_by_identifier($id);
	$self->marker($marker);

	if ( !$coord ) {
		$self->log->info("no coord returned");
		return;
	}

	my @studies = ();

#loop through each significance and work out which study, experiment and resultset they are present in

	my @freqclusters =
	  $self->study_db->get_frequencyclusters_by_markeridentifier($id);
	$self->log->info(
		"freqclusters for " . $id . ": " . scalar(@freqclusters) );

	my $studies            = {};
	my %reported_alleles   = ();
	my %reported_genotypes = ();

	foreach my $fc (@freqclusters) {
		my $experiment   = $fc->usedmarkersetid->experiment;
		my $experimentid = $experiment->identifier;
		my $study        = $experiment->studyid;
		my $studyid      = $study->identifier;

		$studies->{$studyid}->{'study'} = $study;
		$studies->{$studyid}->{$experimentid}->{'experiment'} = $experiment;
		$studies->{$studyid}->{$experimentid}->{ $fc->frequencyclusterid } =
		  $fc;

		#determine reported alleles/genotypes
		foreach my $af ( $fc->allelefrequencies ) {
			$reported_alleles{ $af->allelecombo } = 1;
		}
		foreach my $gf ( $fc->genotypefrequencies ) {
			$reported_genotypes{ $gf->genotypecombo } = 1;
		}
	}

	$self->reported_alleles(   [ keys %reported_alleles ] );
	$self->reported_genotypes( [ keys %reported_genotypes ] );
	@studies = map { $studies->{$_} } keys %{$studies};

	return @studies;
}

sub _convert_data_to_entries {
	my ($self) = @_;
	my $entries;
	if ( $self->option('v') eq 'report' ) {
		$entries = $self->_study_based_entries();
	}
	else {
		$entries = $self->_resultset_based_entries();
	}
	$self->pager($self->new_pager($entries));
	return $entries;
}

sub _study_based_entries {
	my ($self)   = @_;
	my @data     = @{ $self->data };
	my $results  = [];
	my $base_url = $self->config->{GwasCentral_baseurl};
	my $counter  = 0;

	my $studies;
	foreach my $panel (@data) {
		my $study = $panel->{'study'};
		my $levels = $self->access_control->get_study_and_experiment_access_levels([$study]);
		my $slevel = $levels->{$study->identifier}->{study};
		
		my $item  = {};
		$item->{identifier} = $study->identifier;
		$item->{name}       = $study->name;
		my @exps = ();
		my @studies;
		my $mid = $self->marker->identifier;
		
		foreach my $exp_id ( keys %{$panel} ) {
			next if $exp_id eq 'study';
			my $elevel = $levels->{$study->identifier}->{experiments}->{$exp_id};
			my $exp = $panel->{$exp_id}->{'experiment'};
			#my $markers_ctrl = $self->access_control->get_controlled_markers_for_experiment($exp_id);
			
			my $freq_protected;
#			if ($elevel eq 'none' || ($markers_ctrl->{$mid} && $markers_ctrl->{$mid}->freqs)) {
#				$self->log->info("freq protected: elevel = $elevel, marker->freqs = ".($markers_ctrl->{$mid} ? $markers_ctrl->{$mid}->freqs : 'none set'));
#				$freq_protected = 1;
#			}
				
			my @results = ();
			
			foreach my $fc_id ( keys %{ $panel->{$exp_id} } ) {
				next if $fc_id eq 'experiment';
				
				
				my $fc      = $panel->{$exp_id}->{$fc_id};
				my $cluster = {
					assayed_panel        => $fc->assayedpanel->name,
					no_individuals_typed => $fc->numberofgenotypedsamples,
				};
				
				my (@afs_array,@gfs_array);

					
					
					
					foreach my $allele ( @{ $self->reported_alleles } ) {
						if ($freq_protected) {
							$cluster->{allele_frequencies}->{$allele}='protected';
						}
						else {
							my @afs = $fc->allelefrequencies;
							foreach my $freq (@afs) {
								$freq->allelecombo eq $allele
								  and $cluster->{allele_frequencies}->{$allele} =
								 $freq->frequencyasproportion;
							}
						}
					}
					
					foreach my $gt ( @{ $self->reported_genotypes } ) {
						if ($freq_protected) {
							$cluster->{genotype_frequencies}->{$gt}='protected';
						}
						else {
							my @gfs = $fc->genotypefrequencies;
							foreach my $freq (@gfs) {
								$freq->genotypecombo eq $gt
								  and $cluster->{genotype_frequencies}->{$gt} = $freq->frequencyasproportion;
							}
						}
					}
					@afs_array =
					  map { $_ . "(" . $cluster->{allele_frequencies}->{$_} . ")" }
					  keys %{ $cluster->{allele_frequencies} };
					@gfs_array = map {
						$_ . "(" . $cluster->{genotype_frequencies}->{$_} . ")"
					} keys %{ $cluster->{genotype_frequencies} };

				my $study = {
					'Study' => $study->name . " (" . $study->identifier . ")",
					'Experiment' => $exp->name . " (" . $exp->identifier . ")",
					'Allele Frequencies' => ($freq_protected ? "protected" : join( ",", @afs_array ) ),
					'Genotype Frequencies' => ($freq_protected ? "protected" :  join( ",", @gfs_array )),
					'Study Access Level' => $slevel,
					'Experiment Access Level' => $elevel,
				};
				push @studies, $study;
				
				
				push @results, $cluster;
			}

			my $exp_item = {
				'identifier' => $exp->identifier,
				'name'       => $exp->name,
				'results'    => \@results,
				'experiment_access_level'=>$elevel,
			};

			push @exps, $exp_item;
		}

		$item->{experiments} = \@exps;
		$item->{study_access_level} = $slevel;
		push @{$results},
		  {
			id      => "study/" . $study->identifier,
			title   => $study->name . " (" . $study->identifier . ")",
			summary => \@studies,
			link    => "study/" . $study->identifier,
			content => $item,
			updated => $study->timecreated
		  };
		$counter++;

	}

	return $results;
}

sub _resultset_based_entries {
	my ($self)   = @_;
	my @data     = @{ $self->data };
	my $results  = [];
	my $base_url = $self->config->{GwasCentral_baseurl};
	my $counter  = 0;

	my $studies;
	foreach my $panel (@data) {
		my $study = $panel->{'study'};
		my $item  = {};

		my @exps = ();
		my @studies;
		foreach my $exp_id ( keys %{$panel} ) {
			next if $exp_id eq 'study';

			my $exp = $panel->{$exp_id}->{'experiment'};

			my @results = ();

			foreach my $rs_id ( keys %{ $panel->{$exp_id} } ) {
				next if $rs_id eq 'experiment';
				my $sig     = $panel->{$exp_id}->{$rs_id};
				my $rs      = $panel->{$exp_id}->{$rs_id}->{'resultset'};
				my $content = {
					'study_identifier'      => $study->identifier,
					'study_name'            => $study->name,
					'experiment_identifier' => $exp->identifier,
					'experiment_name'       => $exp->name,
				};
				my $summary = {
					'Study' => $study->name . " (" . $study->identifier . ")",
					'Experiment' => $exp->name . " (" . $exp->identifier . ")",
				};
				push @{$results},
				  {
					id      => "study/" . $study->identifier,
					title   => $study->name . " (" . $study->identifier . ")",
					summary => $summary,
					link    => "study/" . $study->identifier,
					content => $content,
					updated => $study->timecreated,
				  };
			}
			my $exp_item = {

			};

			push @exps, $exp_item;
		}

		$item->{Experiments} = \@exps;

		$counter++;

	}
	$self->log->info( "results:" . Dumper($results) );
	return $results;
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerFrequencies.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut

