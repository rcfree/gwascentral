# $Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Results::MarkerResults - Query for marker association results


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Study;
use Moose;
extends qw(GwasCentral::Search::Query);
has '+Detector_name' => ( 'default' => 'None' );

use Moose;
extends qw(GwasCentral::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use List::Util qw(sum);

has '+url_path' => ( 'default' => sub { 'study/'.$_[0]->fval('SingleIdentifier') }, 'lazy' => 1 );
has '+is_report' => ('default'=>1);
has '+possible_Filters' => (
	default => sub {
		[
			qw(SingleIdentifier ExportFormat CannedQuery AddedResultsets)
		];
	}
);

has '+Filter_options' => ('default'=> sub {
	
	return {
		'AddedResultsets' => {
			'template' => 'reportbpanel',
			'fieldset' => 'result_opts',
		},
		'ExportFormat' => {
			'label' => 'Export Study as',
			'list' => [
				['html'=>'--choose a format --'],
				['yaml'=>'YAML File'],
				['xml'=>'XML File'],
				['json'=>'JSON file'],
			],
		},
	};
});

no Moose;

sub get_data {
	my ($self)    = @_;
	my $si    = $self->f('SingleIdentifier');

#retrieve G2P association results where this marker is tested which pass the threshold
	my $study =
	  $self->DS('Study')
	  ->get_study_by_identifier( $si->value );
	!$study and $self->throw("A Study with identifier/accession '".$si->value."' was not found");
	my $entries;
	my @results  = ();
	my $base_url = $self->config->{WebSite}->{hgvbaseg2p_baseurl};
	my $counter  = 0;

	my $rs_filter      = $self->f('AddedResultsets');
	
	my ( $is_addable, $is_added ) = $rs_filter->addable_study_state(
		{
			identifier        => $study->identifier,
			accesslevel       => $study->accesslevel,
			child_accesslevel => $study->child_accesslevel,
		}
	);
	
	my ($rsets,$rsets_in_study) = $rs_filter->resultsets_in_study($study);

	my $tmi = sum(map { $_->totalmarkersimported } $study->experiments);

	return {
		study => $study,
		identifier => $study->identifier,
		addable=>$is_addable,
		added=>$is_added,
		rs_data => $rsets,
		resultsets => $rsets_in_study,
		totalmarkersimported => $tmi,
		association_experiments => [ $study->experiments->search({'experimenttype'=>{'!='=>'Frequency determination'}}) ],
	 };
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $ 

=cut

