# $Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Studies - Query studies


=head1 SYNOPSIS

    use GwasCentral::Search::Query::Studies;
    my $search = GwasCentral::Search::Query::Studies->new;
    $search->option('q','cancer');
    my @results = $search->results;


=head1 DESCRIPTION

This class performs a search across Studies using a query string supplied as a parameter (q). It will also take into account the threshold (t) level
when searching across a region, gene or marker.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Genes;
use strict;
use warnings;
use Moose;
use GwasCentral::Search::Util qw(new_Query);
use HTML::Strip;

extends qw(GwasCentral::Search::Query);

has '+url_path'        => ( 'default' => 'genes' );
has '+DataSource_name' => ( 'default' => 'Feature' );
has '+fieldset_order'  => ( 'default' =>
	  sub { [ 'options', 'qfilter', 'browserpanel', 'paging', 'result_opts' ] }
);
has 'Queries' => ( 'is' => 'rw' );
has 'subquery' => ('is'=>'rw');
has 'is_subquery' => ('is'=>'rw', 'default' => 1);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Number::Format qw(:subs);
use List::MoreUtils qw(none);
use Tie::Hash::Indexed;

has '+possible_Filters' => (
	default => sub {
		[
			qw(QueryFilter QueryString Threshold FivePrimeFlank ThreePrimeFlank MarkerDisplay SearchSubmit ExportFormat Page)
		];
	}
);

has '+Detector_name' => ( 'default' => 'Single' );
has '+possible_DetectTypes' => (
	'default' => sub {
		[qw(Gene Region)];
	}
);

has '+Filter_options' => (
	'default' => sub {
		return {
			'ExportFormat' => { 'fieldset' => 'none' },
			'QueryFilter'  => {
				'template'  => 'geneorregion',
				'fieldset'  => 'qfilter',
				'label'     => 'Search for genes with markers',
				'value'     => 'Gene',
				'dt_hidden' => { 'None' => 1 },
				'dt_labels' => {
					'Gene'   => 'by HGNC gene symbol',
					'Region' => 'by genomic location'
				},
				'dt_onclick' => {
					'Gene'   => 'showLociGene()',
					'Region' => 'showLociRegion()'
				}
			},
			
		};
	}
);
has 'get_data_method' => ( 'is' => 'rw', 'default' => 'get_gene_list' );
has '+Detector_name' => ( 'default' => 'Single' );

after 'prepare_Filters' => sub {
	my ($self) = @_;
	my $sq = $self->params->{subquery};
	$self->subquery("Genes_".ucfirst($sq));
	my $fmt = $self->Format;
	my $fileext = $fmt->fileext;
	my $filename = $fileext ? lc($self->name)."_".$sq.".".$fileext : undef;
	$self->Format->filename($filename);
};

override 'text' => sub {
	my ($self) = @_;
	my $qf = $self->f('QueryFilter');
	if ($self->no_results) {
		return "<b>".$self->fval("QueryString")."</b> is not recognised as a ".lc($qf->value);
	}
	if ( $qf->value eq 'Gene' ) {
		return
		    $self->label
		  . " match gene symbol <b>"
		  . $self->fval('QueryString') . "</b>";
	}
	else {
		return
		    $self->label
		  . " found in genomic location <b>"
		  . $self->fval('QueryString') . "</b>";
	}
};

has '+heading' => ('default' => sub {
		my ($self) = @_;
		my $qf_val = $self->fval('QueryFilter');
		return $qf_val . " search results:";
	}
);

override 'structured_data' => sub {
	my ($self) = @_;
	$self->log->info("subquery:". $self->subquery);
	my $sq = $self->Queries->{$self->subquery};
	my $data = $sq->data;
	return $data;
};
#override 'heading' => sub {
#	my ($self) = @_;
#	my $qf = $self->f('QueryFilter');
#	my $qs = $self->f('QueryString');
#	if ( $qf->value eq 'Gene' ) {
#		if ($self->no_results) {
#			return "Search term <b>".$self->fval("QueryString")."</b> is not recognised as a gene";
#		}
#		else {
#			return "Search term <b>".$self->fval("QueryString")."</b> is a gene (".$qs->chr.":".$qs->start."..".$qs->stop.")";
#		}
#	}
#	else {
#		if ($self->no_results) {
#			return "Search term <b>".$self->fval("QueryString")."</b> is not recognised as a region";
#		}
#		else {
#			return "Search term <b>".$self->fval("QueryString")."</b> is a region (".$qs->chr.":".$qs->start."..".$qs->stop.")";
#		}
#	}
#};

after 'prepare_Filters' => sub {
	my ($self) = @_;
	my $qf     = $self->f('QueryFilter');
	my $qs     = $self->f('QueryString');
	my $label  = $qf->dt_labels->{ $qf->by_index(0) };
	$label =~ s/by//;
	$qs->label("Enter $label");
	$qs->autocomplete_url($qf->value eq 'Gene' ? '/genes/lookup/symbols' : '/genes/lookup/region');
			
	tie my %queries, "Tie::Hash::Indexed";
	my @qtypes = qw(Phenotypes Markers Studies);
	
	foreach my $qtype (@qtypes) {
		my $query = new_Query( "Genes::$qtype",
			{ DataSources => $self->DataSources, conf_file => $self->config, params => $self->params } );
		$query->f("ExportFormat")->html_id("format_".$self->name."_".$qtype."_".$qf->value);
		$queries{ "Genes_" . $qtype } = $query;
	}
	$self->Queries( \%queries );
};

after 'results' => sub {
	my $self = shift;
	$self->Detector_name("Multi");
};

sub retrieve_gene {
	my ($self) = @_;
	$self->log->info("retrieve_gene");
	#my @genes = @{$raw_genes};
	my $qs = $self->f('QueryString');
	my $f  = $qs->sorted_features()->[0]->{feat};
	return undef if !$f;
	my $item = {};

	$item->{gene_symbol} = $f->display_name;
	$item->{region}      = $qs->region;
	$item->{region_size} = $qs->sorted_features()->[0]->{size};
	$item->{aliases}     = [ $f->get_tag_values('Alias') ];

	my $hs = HTML::Strip->new();

	foreach my $qtype ( keys %{ $self->Queries } ) {
		my $query = $self->Queries->{$qtype};

		$query->params( $self->params );
		$query->params->{qfilter} = "Gene";
		$query->prepare_Filters;
		$query->results;
	}

	my @aliases = @{ $item->{aliases} || [] };
	tie my %links, "Tie::Hash::Indexed";

	foreach my $alias (@aliases) {
		if ( $alias =~ /NM\_/ ) {
			$links{mRNA} = {
				id   => $alias,
				link => "http://www.ncbi.nlm.nih.gov/nuccore/$alias"
			};
		}
		elsif ( $alias =~ /NR\_/ ) {
			$links{ncRNA} = {
				id   => $alias,
				link => "http://www.ncbi.nlm.nih.gov/nuccore/$alias"
			};
		}
	}

	$links{'OMIM'} =
	  { 'link' =>
"http://www.omim.org/search?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search="
		  . $item->{gene_symbol} };
	$links{'SNPedia'} =
	  { 'link' => "http://www.snpedia.com/index.php/" . $item->{gene_symbol} };
	$links{'WAVe'} =
	  { 'link' => "http://bioinformatics.ua.pt/WAVe/gene/"
		  . $item->{gene_symbol} };
	$item->{links} = \%links;

	$self->data($item);
}

sub get_data {
	my ($self) = @_;
	$self->log->info( "data:" . Dumper( $self->data ) );
	return $self->data;
}

sub convert_data_to_entries {

}

sub retrieve_region {
	my ($self) = @_;
	$self->log->info("is region?");
	#my @genes = @{$raw_genes};
	my $qs = $self->f('QueryString');

	my $item = {};

	$item->{region}      = $qs->region;
	$item->{region_size} = $qs->sorted_features()->[0]->{size};

	my $hs = HTML::Strip->new();

	foreach my $qtype ( keys %{ $self->Queries } ) {
		my $query = $self->Queries->{$qtype};

		$query->params( $self->params );
		$query->params->{qfilter} = "Region";
		$query->prepare_Filters;
		$query->results;
	}
	tie my %links, "Tie::Hash::Indexed";
	if (!$qs->is_region) {
	
		$links{'OMIM'} =
		  { 'link' =>
	"http://www.omim.org/search?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search="
			  . $qs->value };
	}
	$item->{links} = \%links;

	$self->data($item);
}
1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

