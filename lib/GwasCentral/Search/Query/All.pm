# $Id: All.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::All - Query studies, phenotypes and markers in one go


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::All;

	use Moose;
	extends qw(GwasCentral::Search::Query);

	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use Text::ParseWords;
	use GwasCentral::Base qw(load_module);
	
	has 'Queries' => ('is'=>'rw', 'default' => sub { {} });
	
	has '+possible_Filters' => (default => sub { 
		[qw(QueryFilter QueryString Threshold ExportFormat SearchSubmit AddedResultsets)]
	});
		
	has '+possible_DetectTypes' => ('default'=>sub {
		[qw(Annotation Region Gene Marker Keyword)];
	});
	
	has 'Query_info' => ('is'=>'rw', 'default'=>sub {
		tie my %qinfo,"Tie::Hash::Indexed";
		%qinfo = (
			'Phenotypes'=> ['Annotation'],	
			'Genes'=> ['Gene','Region'],
			'Studies'=>['Keyword'],
			'Markers'=> ['Marker']
		);
		return \%qinfo;
	});
	
	has '+Filter_options' => (
	'default' => sub {
		return {
			'SortBy' => {
				'list'    => [
					['none' , 'Search Relevance'],
					['cast(right(me.Identifier,length(me.Identifier)-5) as SIGNED) DESC' , 'Identifier (descending)'],
					['timecreated DESC' , 'Date Created (descending)'],
					['name DESC' , 'Name (descending)'],
					['cast(right(me.Identifier,length(me.Identifier)-5) as SIGNED)' , 'Identifier (ascending)'],
					['timecreated' ,'Date Created (ascending)'],
					['name','Name (ascending)'],
					['(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported limit 1)',
					'Number of markers (ascending)'],
					['(select TotalMarkersImported from Experiment where StudyID = me.StudyID order by TotalMarkersImported limit 1) DESC' ,
				'Number of markers (descending)']
				],
				'default' => 'Identifier (descending)',
			},
			'QueryFilter' => {
				'dt_labels' => {
					'Keyword' => 'for keywords',
					'Gene' => 'for gene symbol',
					'Region' => 'for genomic region',
					'Marker' => 'for marker IDs',
					'Annotation' => 'for phenotype MeSH IDs',
				}
			},
			'AddedResultsets' => { 
				'template' => 'browserpanel',
				'fieldset' => 'result_opts',
			 },
			 'QueryString' => {
				'autocomplete_url'=>'/search/lookup',
				'label' => 'Enter search terms',
			},
		};
	}
);

	
	
	override 'prepare_Filters' => sub { 
		my ($self) = @_;
		foreach my $qname(keys %{$self->Query_info}) {
			
			foreach my $qfilter(@{$self->Query_info->{$qname}}) {
				my $query = load_module("GwasCentral::Search::Query::".$qname, {
					conf_file => $self->config,
				});
				
				$query->DataSources($self->DataSources);
				$query->Detector_name('Single');
				$query->params($self->params);
				$query->params->{qfilter}=$qfilter;
				
				$query->prepare_Filters;
				$query->f('QueryFilter')->template("hidden");
				$query->f('Page')->template("hidden");
				
				if ($query->has_f('DatasetPanel')) {
					$self->log->info("has DatasetPanel");
					$query->f('DatasetPanel')->template('none');
				}
				
				if ($query->can('Queries')) {
					foreach my $subquery(values %{$query->Queries}) {
						$subquery->f('QueryFilter')->template("hidden");
						$subquery->f('Page')->template("hidden");
						if ($subquery->has_f('DatasetPanel')) {
							$self->log->info("has DatasetPanel ".$self);
							$subquery->f('DatasetPanel')->template('none');
						}
					}
				}
				$self->Queries->{$qname}->{$qfilter}=$query;
			}
		}
		
		super();
		
	};
	
	sub results {
		my ($self) = @_;
		my %data_for=();
		
		foreach my $qname(keys %{$self->Query_info}) {
			$self->log->info("query:$qname");
			foreach my $qfilter(@{$self->Query_info->{$qname}}) {
				$self->log->info("qfilter:$qfilter");
				my $query = $self->Queries->{$qname}->{$qfilter};
				$query->has_f('AddedResultsets') and $self->log->info("template:".$query->f('AddedResultsets')->template);
				$data_for{$qname}->{$qfilter} = $query->results;
			}
		}
		return \%data_for;
	}


1;