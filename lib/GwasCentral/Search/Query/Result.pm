# $Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Results::MarkerResults - Query for marker association results


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Significance;
use Moose;
extends qw(GwasCentral::Search::Query);
has '+Detector_name' => ( 'default' => 'None' );

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use GwasCentral::Tool::Gene qw(separate_features);

has '+url_path' => ( 'default' => 'marker' );
has '+possible_Filters' => (
	default => sub {
		[
			qw(MarkerIdentifier ResultsetIdentifier ExportFormat)
		];
	}
);
has '+Filter_options' => ('default'=> sub {
	
	return {
		'ExportFormat' => {
			'template' => 'none',
			'list' => [
				['html'=>'--choose a format --'],
			],
			'value' => 'html',
		},
	};
});

no Moose;

sub get_data {
	my ($self)    = @_;
	my $mi     = $self->f('MarkerIdentifier');
	my $marker = $mi->marker;
	my $coord = $mi->coord;
	my $annot = $self->DS('Browser')->get_annotation_by_identifier($marker->identifier);
	my $ri = $self->f('ResultsetIdentifier');
	my $rset = $ri->resultset;
	
#retrieve G2P association results where this marker is tested which pass the threshold
	#my @features = $self->DS('Feature')->dbh->features(-types=>['mRNA','CDS','five_prime_utr','three_prime_utr'], -seqid=>"chr".$coord->chr, -start => $coord->start, -stop => $coord->stop);

	#my ($transcript_info) = separate_features($self->DS('Feature'), \@features, 1);
	my $sig = $self->DS('Study')->dbh->resultset('Significance')->search({'usedmarkersetid.markeridentifier'=>$marker->identifier, 'resultsetid.identifier'=>$rset->identifier},{'join' => ['usedmarkersetid', 'resultsetid']})->single;
	
	my @related_data = ();
		push @related_data,{
			link => "/marker/".$marker->identifier."/results",
			label => "All association results for this marker",
			title => "All association results for this marker",
			image => "grid",
		};
	
		
		$marker->{related_data}=\@related_data;
	return {
		marker => $marker,
		coord => $coord,
		significance => $sig,
		resultset => $rset,
		effectsizes => $sig->usedmarkersetid->effectsizes,
		annotation => $annot,
		related_data => \@related_data
	 };
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: MarkerResults.pm 1641 2011-03-16 14:22:54Z rcf8 $ 

=cut

