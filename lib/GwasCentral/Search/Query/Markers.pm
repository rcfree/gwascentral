# $Id: Markers.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Marker - Query for markers


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Markers;
use Moose;
extends qw(GwasCentral::Search::Query);
use GwasCentral::Base qw(pluralize);
use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
has '+url_path'=>('default' => 'markers');

has '+possible_Filters' => (default => sub { [
		qw(QueryFilter QueryString Threshold Variation MarkerDisplay SearchSubmit Page ExportFormat SortBy PageSize IdentifierList)]
	});
		
	has '+possible_DetectTypes' => ('default'=>sub {
		[qw(Marker Region Gene)];
	});
has '+Filter_options' => ('default'=> sub {
		
		return {
			'SortBy' => {
				'list' => [
				[1, 'Identifier (descending)','cast(right(Marker_Identifier,length(Marker_Identifier)-4) as SIGNED) DESC'], 
					[2, 'Identifier (ascending)','cast(right(Marker_Identifier,length(Marker_Identifier)-4) as SIGNED)']
					], 
				'default' => 1
			},
			'PageSize' => {
				'list' => [
					['20','20'],
					['50','50'],
					['100','100'],
					['250','250'],
				],
				'default' => '50'
			},
			'Page' => {
				'default' => 1
			},
			'QueryFilter' => {
				'template' => 'geneorregion',
				'fieldset' => 'qfilter',
				'label' => 'Search for markers',
				'value' => 'Marker',
				'dt_hidden' => {
					'None' => 1
				},
				'dt_labels' => {
					'Marker' => 'by dbSNP rsid/GWAS Central id',
					'Region' => 'by genomic location',
					'Gene' => 'by HGNC gene symbol'
				},
				'dt_onclick' => {
					'Marker' => 'showMarkersMarker()',
					'Region' => 'showMarkersRegion()',
					'Gene' => 'showMarkersGene()'
				}
			},
		};
	});
has '+Detector_name' => ('default'=>'Single');
has 'get_data_method' => ('is'=>'rw','default'=>'get_marker_list');
has '+DataSource_name' => ('default'=>'Marker');
has 'no_data_markers' => ('is'=>'rw','default' => sub { {} });
has 'pvalue_count' => ('is'=>'rw', 'default'=>sub { {} });
override 'text' => sub {
	my ($self) = @_;
	my $qf = $self->f('QueryFilter');
	if ($qf->value eq 'Marker') {
		return $self->label." match marker identifier <b>".$self->fval('QueryString')."</b>";
	}
	elsif ($qf->value eq 'Gene') {
		return $self->label." found in gene <b>".$self->fval('QueryString')."</b> (largest transcript)";
	}
	else {
		return $self->label." found in genomic location <b>".$self->fval('QueryString')."</b>";
	}
};

after 'prepare_Filters' => sub {
	my ($self) = @_;
	my $qf = $self->f('QueryFilter');
	my $qs = $self->f('QueryString');
	my $label = $qf->dt_labels->{$qf->by_index(0)};
	$label=~s/by//;
	$qs->label("Enter $label");
};

sub retrieve_region {
	my ($self,$term) = @_;
	my $qs = $self->f('QueryString');
	my $threshold = $self->fval('Threshold');
	my $region  = $qs->region;
	my $marker_count = $self->DS('Browser')->count_markers_in_region_above_threshold(
	{
				threshold => $threshold,
				chr       => $qs->chr,
				start     => $qs->start,
				stop      => $qs->stop,
				display => $self->fval('MarkerDisplay')
	});
	$self->new_pager($marker_count);
	return [] if $marker_count == 0;
	$self->log->info("marker_count:$marker_count");
	

	my ($markers, $no_data_markers, $pvalue_count) = $self->DS('Browser')
		  ->get_markers_in_region_above_threshold(
			{
				threshold => $threshold,
				chr       => $qs->chr,
				start     => $qs->start,
				stop      => $qs->stop,
				pager      => $self->pager,
				display => $self->fval('MarkerDisplay'),
				sort_by => $self->f('SortBy')->by_index(0)
			}
	);
	$self->log->info("markers:".Dumper($markers));
	$self->no_data_markers($no_data_markers);
	$self->pvalue_count($pvalue_count);
	$self->fval('IdentifierList',$markers);
	#$self->markers($markers);
}

sub retrieve_gene {
	return $_[0]->retrieve_region;
}

sub retrieve_marker {
	my($self) = @_;
	$self->log->info("retrieve_marker");
	$self->log->info("markers:".Dumper($self->f('QueryString')->markers));
	my @midents = map { $_->{Identifier} } @{$self->f('QueryString')->markers || [] };
	$self->fval('IdentifierList',\@midents);
	my $pager = $self->new_pager(scalar(@midents));
	my ($markers, $no_data_markers, $pvalue_count) = $self->DS('Browser')
		  ->get_markers_by_ids_above_threshold(
			{
				threshold => $self->fval("Threshold"),
				ids => \@midents,
				display => $self->fval('MarkerDisplay'),
				pager => $pager,
				sort_by => $self->f('SortBy')->by_index(0)
			}
	);
	$self->log->info("markers:".Dumper($markers));
	$self->no_data_markers($no_data_markers);
	$self->pvalue_count($pvalue_count);
}

sub convert_data_to_entries {
	my ($self, $raw_markers) = @_;
	my @data = ();
	my @markers = @{ $raw_markers };
	return if scalar(@markers) == 0;
	$self->log->info("no_data_markers:".Dumper($self->no_data_markers));
	$self->log->info("pager:".Dumper($self->pager));
	my $threshold = $self->fval('Threshold');
	foreach my $marker(@markers) {
		my @related_data;
		my $ident = $marker->{identifier};
		$marker->{no_data} = $self->no_data_markers->{$ident};	
		my $result_label = $self->no_data_markers->{$marker->{accession}}." Results";
		my $pvalue_count = $self->pvalue_count->{$ident};
		
		if ($pvalue_count>0) {
			my $results = pluralize($pvalue_count,"Result") ." with -log p>= ".$threshold;
			push @related_data,{
				link => "/marker/".$marker->{identifier}."/results?t=".$threshold,
				label => $results,
				title => $results,
				image => "grid",
			};
		}
		
		$marker->{related_data}=\@related_data;
		tie my %links, "Tie::Hash::Indexed";
		$links{'OMIM'}={'link' => "http://www.omim.org/search?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search=".$marker->{accession}, 'target' => '_blank' };
		$links{'SNPedia'}={'link' => "http://www.snpedia.com/index.php/".$marker->{accession}, 'target' => '_blank' };
		$links{'dbSNP'}={'link' => "http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs=".$marker->{accession}, 'target' => '_blank' };
		$marker->{links}=\%links;
	}
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Markers.pm 1560 2010-10-28 15:55:59Z rcf8 $ 

=cut

