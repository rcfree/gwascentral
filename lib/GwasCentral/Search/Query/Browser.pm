# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Browser - Query for browser data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Browser;

use Moose;
extends qw(GwasCentral::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use Bio::Graphics::FeatureFile;
use FindBin;
use lib "$FindBin::Bin/../../../../lib";

use GwasCentral::Browser::Util qw(chr_length poslog);
has '+Detector_name' => ( 'default' => 'None' );
has 'server_issues' => ( 'is' => 'rw', 'default' => sub { {} } );

has '+possible_Filters' => (
	default => sub {
		[qw(ExportFormat Threshold MultiChr AddedResultsets ScaleType JSONP)];
	}
);

has '+Filter_options' => (
	'default' => sub {
		{
			'Threshold'    => { 'default' => '3' },
			'ExportFormat' => {
				'list' => [
					[ 'html'    => '--choose a format --' ],
					[ 'excel'   => 'Microsoft Excel' ],
					[ 'csv'     => 'Comma-separated file' ],
					[ 'tsv'     => 'Tab-separated file' ],
					[ 'rss'     => 'RSS feed' ],
					[ 'atom'    => 'Atom feed' ],
					[ 'json'    => 'JSON file' ],
					[ 'jsonp'   => 'JSONP file' ],
					[ 'feature' => 'GBrowse features' ],
					[ 'bed'     => 'BED file' ],
					[ 'gff'     => 'GFF3 file' ],
				],
			},
			'PageSize' => { 'default' => 'all' }
		};
	}
);

has 'features'   => ( 'is' => 'rw' );
has 'max_count'  => ( 'is' => 'rw' );
has 'rs_counts'  => ( 'is' => 'rw' );
has '_featureno' => ( 'is' => 'rw', 'default' => 1 );
has 'bin_data' => ('is'=>'rw', 'default' => sub { [] });

sub convert_data_to_entries {
	my ( $self, $data ) = @_;
}

sub add_sig_counts_glyph {
	my ( $self, $attrs ) = @_;

	my $max_count = $attrs->{max_count} || 20;
	my $key = $attrs->{key};
	$self->log->info("add_sig_counts_glyph");
	$self->features->add_type(
		's' => {
			glyph      => 'stackplot',
			graph_type => 'boxes',
			fgcolor    => sub {
				my $f = shift;
				return $f->{empty} ? "lightgrey" : "black";
			},
			bgcolors        => $self->config->{Browser}->{bgcolors},
			height          => $attrs->{height} || 20,
			min_score       => 0,
			max_score       => 10,
			scale           => 'none',
			bump            => 0,
			key             => 0,
			label           => 0,
			'balloon width' => 300,
			'balloon hover' =>
			  'sub { my $feature = shift; return $feature->{"popup"}; }',
			method => 'sig_counts',
			source => 'GwasCentral',
			key    => $key,
		}
	);
}

sub add_sig_counts {
	my ( $self, $attrs ) = @_;

	my $region = ref($self) =~ /Region/ ? $self->f('QueryString') : undef;

	my $rs_filter  = $self->f('AddedResultsets');
	my $chr        = $attrs->{chr} || $region->chr;
	my $rs_counts  = $attrs->{rs_counts} || {};
	my $logscale   = $self->f('ScaleType')->value eq 'log10' ? 1 : undef;
	my $chr_length = chr_length($chr);

	my $features = $self->features;
	my $data     = $attrs->{data};
	my $bin_size = $attrs->{bin_size} || 3000000;

	#my %has_data_count = ();
	my $counter    = 1;
	my @sum_scores = ();

	my @identifiers = keys %{ $rs_filter->resultsets };

	#define expected value if totally empty (eg. -1|-1|...)
	my $empty_score = "-1|" x scalar(@identifiers);

	#my $empty_score = "-1|" x scalar( @{ $self->resultsets_and_uploads } );
	$empty_score = substr( $empty_score, 0, length($empty_score) - 1 );

	#define expected value if no markers in the region (eg. 0|0|..)
	my $zero_score = "0|" x scalar(@identifiers);
	$zero_score = substr( $zero_score, 0, length($zero_score) - 1 );

	my $chr_prefix =
	  ref($self) =~ /Region/ ? 'Chr' : '';    #add Chr prefix if region view
	my $summaries   = $rs_filter->resultsets;
	my $colors      = $rs_filter->colors;
	my $html_colors = $rs_filter->html_colors;
	my $tmpl_text   = $attrs->{popup_template};
	my $tt          = Template->new(
		{
			INCLUDE_PATH => $self->config->{basedir}
			  . "/htmltemplates/lib/gbrowse",
			INTERPOLATE => 1,
		}
	) || die "$Template::ERROR\n";

	my @rs_idents = keys %{ $rs_filter->resultsets };

	#deal with chrX and Y (passed in by JS as 23 and 24)
	my $js_chr = $chr eq 'X' ? '23' : $chr eq 'Y' ? '24' : $chr;
	
	#loop through each item in data
	for my $item ( @{$data} ) {
		$self->log->info("item:".Dumper($item));
		my $actual_score = $item->{'bin_counts'} || '0';
		my $score        = 0;
		my $sum_score    = 0;
		my @temp         = ();

		#prevent start and stop going off ends of chromosome
		my $range_start =
		    $item->{'bin_start'} >= $bin_size
		  ? $item->{'bin_start'}
		  : 1;
		my $range_stop =
		    $item->{'bin_stop'} < $chr_length
		  ? $item->{'bin_stop'}
		  : $chr_length;
		my $name = "s$chr" . "_" . $counter;

		my $short_start = sprintf( "%03d", int( $range_start / 1000000 ) );

		my $popup;
		my $empty;
		my $url;


		if ( $actual_score eq $empty_score ) {
			$url   = undef;
			$popup = "No markers in bin";
			$empty = 1;
		}
		else {

			#get array of actual scores for data item and loop through them
			my @actual_scores = split( /\|/, $actual_score );
			my $pos = 0;
			
			my $rs_scores = {};
			foreach my $val (@actual_scores) {
		  #if score is not empty or zero process for scale-type and add to array
		  #otherwise add a '0'
		  		my $count = $val;
				if ( $val ne -1 && $val ne 0 && $val ne 'X' ) {
					#log count if logscale flag is 1
					if ($logscale) {
						$val = sprintf( "%.3f", poslog( $val + 1 ) );
					}
					$rs_scores->{$rs_idents[$pos] } = $count;
				}
				else {
					$val = '0';
					$count = 0;
				}
				push @temp, $val;
				$sum_score += $val;
				$rs_counts->{ $rs_idents[$pos] } += $count;
				
				$pos++;
			}
			$self->log->info("rs_scores:".Dumper($rs_scores));
			#join the scores together with | and get a total for the bin
			$score = join( '|', @temp );

			

#add final data to list of feature data chr and position are represented as Chr.Pos (ie. 4.351)
			$url =
			    "submitSearch('region','"
			  . $chr_prefix
			  . $chr . ":"
			  . $item->{bin_start} . ".."
			  . $item->{bin_stop} . "')";

#| separated score is dealt with by custom Bio::Graphics::Glyph::stackplot in GBrowse
#include hack with onclick to call JS function

		 #sort out HTML popup for each bin and store this in the feature as well
			
			my $vars = {
				rs_scores    => $rs_scores,
				rs_filter => $self->f('AddedResultsets'),
			};
			$popup = $self->_add_sig_popup($rs_scores);
			
			$self->log->info("$popup");
			#add the total score to an array of total scores
			push @sum_scores, $sum_score;
			$counter++;
		}

		my $feature = Bio::Graphics::Feature->new(
			-start      => $item->{bin_start},
			-stop       => $item->{bin_stop},
			-score      => $score,
			-ref        => $chr_prefix . $chr,
			-type       => 's',
			-name       => "Count:$actual_score",
			-attributes => { 'onClick' => ["gvToRV($js_chr.$short_start)"], },
			-url        => ( ref($self) =~ /Region/ ? $url : '' )
		);

	 #store actual score in feature as well (used to sort out axes in stackplot)
 	$feature->{actualscore} = $actual_score;
	$feature->{popup}       = $popup;
	$feature->{empty}       = $empty;

		#add the feature to the feature list
		$features->add_feature( $feature => 's' );
	}

#once finished looping through the data items get the highest score in the array of total scores
	my @sorted = reverse sort { $a <=> $b } @sum_scores;
	return ( $sorted[0] );
}

#sub _get_binned_data {
#	my ( $self, $attrs ) = @_;
#
#	my %data   = ();
#	my ( $binned_data, $total_counts ) = $ds->$method($attrs);
#	return $binned_data;
#
#}

sub _add_sig_popup {
	my ($self, $rs_scores_ref) = @_;
	my %rs_scores = %{$rs_scores_ref};
	if (scalar(keys %rs_scores) > 0) {
		my $html = "<b>Click to view region in detail</b><br/><br/><u>Number of significant markers in bin</u>";
		$html .= "<table>";
		$self->log->info("rs_scores:".Dumper($rs_scores_ref));
		foreach my $rs_ident (keys %rs_scores) {
			my $rs = $self->f('AddedResultsets')->resultsets->{$rs_ident};
			$html .= "<tr><td style='vertical-align:middle'>$rs_scores{$rs_ident}</td>";
			$html .= "<td style='background-color:rgb(" . $self->f('AddedResultsets')->html_colors->{$rs_ident}
				  . "); width:16px; height:12px;border:solid 1px black'>&nbsp;</td>";

			$html .=
			    "<td><b>"
			  . $rs->study_name . " ("
			  . $rs->study_ident
			  . ")</b><br><i>"
			  . $rs->name . " ("
			  . ( $rs_ident )
			  . ")</i></td>";
			$html .= "</tr>";
		}
		$html .= "</table>";
		return $html;
	}
	else {
		return "No significant markers in bin";
	}
}


sub add_bins_present_glyph {
	my ( $self, $attrs ) = @_;
	my $panel       = Bio::Graphics::Panel->new;
	my $rs_filter   = $self->f('AddedResultsets');
	my $html_colors = $rs_filter->html_colors;
	my $colors      = $rs_filter->colors;
	my $summaries   = $rs_filter->resultsets;

	#my $numbers = $rs_filter->numbers;
	my $rscounter = 0;

	foreach my $rs_ident ( keys %{ $rs_filter->resultsets } ) {
		my $html = "<u>Marker coverage for:</u></br>";

		my $summary = $summaries->{$rs_ident};
		$html .= "<table><tr><td style='vertical-align:middle'></td>";
		$html .=
		    "<td style='background-color:"
		  . $html_colors->{$rs_ident}
		  . "; width:16px; height:12px;border:solid 1px black'>&nbsp;</td>";
		my $study_name  = $summary->study_name;
		my $study_ident = $summary->study_ident;
		my $rs_name     = $summary->name;
	
		$summaries
		  and $html .=
		    "<td><b>"
		  . $study_name . " ("
		  . $study_ident
		  . ")</b><br><i>"
		  . $rs_name . " ("
		  . $rs_ident
		  . ")</i></td>";
		$html .= "</tr>";
		$html .= "</table>";

		$self->features->add_type(
			"l$rs_ident" => {
				glyph           => 'graded_segments',
				fgcolor         => 'lightgrey',
				bgcolor         => $colors->{$rs_ident},
				bump            => 0,
				label           => 0,
				description     => 0,
				title           => 0,
				key             => 0,
				height          => $attrs->{height},
				method          => 'presence',
				source          => 'GwasCentral',
				max_score       => 1,
				min_score       => 0,
				'balloon width' => 200,
				'balloon hover' => 'sub { return "' . $html . '";}'
			}
		);
		$self->log->info("add_type:l$rs_ident");
		$rscounter++;
	}
}

sub add_bins_present {
	my ( $self, $attrs ) = @_;
	my $region;
	eval { $region = $self->f('QueryString'); };
	my $rs_filter  = $self->f('AddedResultsets');
	my $chr        = $attrs->{chr} || $region->chr;
	my $chr_length = chr_length($chr);
	my $data       = $attrs->{data};
	my $features   = $self->features;

	my $counter   = 1;
	my %start_pos = ();
	my %end_pos   = ();
	my %prev_stop = ();
	my %within    = ();

#more elegant and speeds up rendering to draw lines as required, rather than on or off feature for each bin
	foreach my $rset_ident ( keys %{ $rs_filter->resultsets } ) {
		$start_pos{$rset_ident} = $region ? $region->start : 1;
		$end_pos{$rset_ident}   = $region ? $region->start : 1;
	}

#uses a hash based flag system to determine whether it is 'in' or 'out' of a coloured bar
#loop through each item in the data array
	for my $item ( @{$data} ) {
		my $rscounter  = 0;
		my $score_list = $item->{'bin_counts'};

#get scores for each resultset by splitting by | and loop through these
#draw a coloured box if score is not -1 (colouring of box is determined in 'add_bins_present_glyph')

		#get the score for this item in resultset_ident order
		my @scores = split( /\|/, $score_list );

		#loop through each resultset id
		foreach my $rset_ident ( keys %{ $rs_filter->resultsets } ) {

			#if within a coloured bar for this resultset..
			if ( $within{$rset_ident} ) {

#if the bin is empty then make this bin the end the coloured bar and add the feature
				if ( $scores[$rscounter] eq '-1' ) {

					$end_pos{$rset_ident} = $prev_stop{$rset_ident};
					$within{$rset_ident}  = 0;
					my $feature = Bio::Graphics::Feature->new(
						-start => $start_pos{$rset_ident},
						-stop  => $end_pos{$rset_ident},
						-ref   => $chr,
						-type  => "l$rset_ident",
						-score => 1,
					);
					$self->log->info( "f start:"
						  . $start_pos{$rset_ident}
						  . " stop:"
						  . $end_pos{$rset_ident} );
					$features->add_feature( $feature => "l$rset_ident" );
				}
			}

			#if not within a coloured bar for this resultset..
			else {

#if the bin is not empty then make this bin the end of the coloured bar with a start determined by the previous end
				if ( $scores[$rscounter] and $scores[$rscounter] ne '-1' ) {
					if ( $item->{'bin_start'} != $end_pos{$rset_ident} ) {
						my $feature = Bio::Graphics::Feature->new(
							-start => $end_pos{$rset_ident},
							-stop  => $item->{'bin_start'},
							-ref   => $chr,
							-type  => "l" . $rset_ident,
							-score => 0,
						);
						$self->log->info( "f start:"
							  . $end_pos{$rset_ident}
							  . " stop:"
							  . $item->{'bin_start'} );
						$features->add_feature( $feature => "l" . $rset_ident );
					}

					$start_pos{$rset_ident} = $item->{'bin_start'};
					$within{$rset_ident}    = 1;
				}
			}

			#set the previous stop to be the end of this bin
			$prev_stop{$rset_ident} = $item->{'bin_stop'};
			$rscounter++;
		}

		$counter++;
	}

#ensure that any bars that any resultsets that were still within a bin have features created to the end of the region
	foreach my $rset_ident ( keys %{ $rs_filter->resultsets } ) {
		if ( $within{$rset_ident} ) {
			my $feature = Bio::Graphics::Feature->new(
				-start => $start_pos{$rset_ident},
				-stop  => $region ? $region->stop : $chr_length,
				-ref   => $chr,
				-type  => "l" . $rset_ident,
				-score => 1,
			);
			$features->add_feature($feature);
		}
		else {
			my $feature = Bio::Graphics::Feature->new(
				-start => $end_pos{$rset_ident},
				-stop  => $region ? $region->stop : $chr_length,
				-ref   => $chr,
				-type  => "l" . $rset_ident,
				-score => 0,
			);
			$features->add_feature($feature);
		}
	}
}

sub add_heading {
	my ( $self, $heading ) = @_;
	$self->add_line($heading);
}

sub add_line {
	my ( $self, $key, $is_dashed ) = @_;
	my $region  = $self->f('QueryString');
	my $type    = "line" . $self->next_featureno;
	my $fgcolor = $key ? 'white' : 'lightgrey';
	$self->features->add_type(
		$type => {
			glyph => $is_dashed ? 'dashed_line' : 'line',
			fgcolor     => $fgcolor,
			textcolor   => 'white',
			label       => 0,
			title       => 0,
			pad_bottom  => 0,
			pad_top     => 0,
			label       => 0,
			description => 0,
			key         => 0,
			linewidth   => 2,
			height      => 2,
			key         => $key
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => (
			  $region->start == $region->stop ? $region->start - 10
			: $region->start
		),
		-stop => (
			  $region->start == $region->stop ? $region->stop + 10
			: $region->stop
		),
		-ref   => $region->chr,
		-score => 0
	);
	$self->features->add_feature( $feature => $type );
}

#increments the feature number - used internally
sub next_featureno {
	my ($self) = @_;
	my $featureno = $self->_featureno;
	if ( $self->_featureno ) {
		$featureno++;
	}
	else {
		$featureno = 1;
	}
	$self->_featureno($featureno);
	return $featureno;
}

sub add_textbox {
	my ( $self, $title, $text, $outline, $background ) = @_;
	my $region = $self->f('QueryString');
	$self->features->add_type(
		$title => {
			glyph     => 'multiline_text',
			fgcolor   => $outline,
			bgcolor   => $background,
			fontcolor => 'black',
			label     => 0,
			text      => "test",

			#text         => join( ";", @{$text} ),
			separator => ";",
			text_pad  => 5,
			bump      => 1,
			height    => 24,
		}
	);

	my $feature = Bio::Graphics::Feature->new(
		-start => $region->start,
		-stop  => $region->stop,
		-ref   => $region->chr,
	);

	$self->features->add_feature( $feature => $title );
}

sub messages {
	my ($self) = @_;
	my $rs_filter = $self->f('AddedResultsets');
	return $rs_filter->errors;
}

sub retrieve {
	my ($self) = @_;

	my $chr       = $self->fval('MultiChr');
	my $threshold = $self->fval('Threshold');
	my $start;
	my $stop;
	$chr eq 'all' and $chr = undef;
	$self->log->info("chr:$chr, threshold:$threshold");

#get only single chr data if only one chr present (ie. if chr info length is less than 3)
	my %returned = (
		bin_size  => 3,
		threshold => $threshold,
	);
	if ( $chr && length($chr) > 0 ) {
		if ( $start && $stop ) {

			%returned = (
				method => 'get_marker_binned_by_resultsets_range_and_threshold',
				chr    => $chr,
				start  => $start,
				stop   => $stop,
			);
		}
		else {
			$returned{method} =
			  'get_marker_binned_by_resultsets_chr_and_threshold';
			$returned{chr} = $chr;
		}
	}
	else {
		$returned{method} = 'get_marker_binned_by_resultsets_and_threshold';
	}
	return \%returned;
}


sub _bed_tracks {
	my ($self, $rs_data_for, $url, $htmlurl) = @_; 
	my $output="browser position\n";
	my @track_names = ();
	my $summaries =  $self->f('AddedResultsets')->resultsets;
	my %colors_for = %{ $self->f('AddedResultsets')->colors };
	
	my @identifiers = keys %{$summaries};
	foreach my $rs_ident(@identifiers) {
		my $study_ident = $summaries->{$rs_ident}->study_ident;
		push @track_names, $study_ident."_".$rs_ident;
	}
	$output.="browser pack ".join(' ',@track_names)."\n";

	foreach my $rs_ident(@identifiers) {
		next if !$rs_data_for->{$rs_ident};
		my @bed_lines = @{$rs_data_for->{$rs_ident}};
		next if scalar(@bed_lines)==0;
		my $rs = $summaries->{$rs_ident};
		my $study_ident = $rs->study_ident;
		my $study_name = $rs->study_name;
		my $rs_name = $rs->name;
		$url.=$rs_ident;
		$htmlurl.=$study_ident;
		
		$output.='track name="'.$study_ident."_".$rs_ident.'" description="'.$study_name .' (Result Set '.$rs_ident.')" offset=-1 visibility=1'; 
		$output.=" color=\"".$colors_for{$rs_ident}."\" ";
		$url and $output.="url=\"".$url."\" ";
		$htmlurl and $output.="htmlUrl=\"".$htmlurl."\" ";
		$output.="\n";
		foreach my $line(@bed_lines) {
			$output.=join(' ',@{$line})."\n";
		}
	}
	return 
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

