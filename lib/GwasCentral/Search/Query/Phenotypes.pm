# $Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

HGVbaseG2P::Search::Query::Phenotype - Query for phenotype data


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Phenotypes;

	use Moose;
	extends qw(GwasCentral::Search::Query);

	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use Text::ParseWords;
	use GwasCentral::Base qw(pluralize);
	has '+DataSource_name' => ('default'=>'Study');
	
	has 'get_data_method' => ( 'is' => 'rw', 'default' => 'get_pmethod_list' );
	has '+Detector_name' => ('default' => 'Single');
	
	sub convert_data_to_entries {
		my ( $self, $raw_phenotypes ) = @_;
		my $base_url = $self->config->{WebSite}->{hgvbaseg2p_baseurl};

		my @phenotypes = @{$raw_phenotypes};
    	my $rs_filter = $self->f('AddedResultsets');
    	
    	return if scalar(@phenotypes)==0;
		my $counter = 0;
		my $rs_counter        = 0;
		my @top_resultsets = ();
		my @results = ();
		my $tr = $self->f('TopResultsets');
		
		#obtain access levels for the studies and experiments from the integrated AccessControl module
		#my %access_levels_for = %{$self->access_control->get_phenotype_and_experiment_access_levels( \@phenotypes )};
		foreach my $item ( @phenotypes ) {
			#$self->log->info("item before:".Dumper($item));
			
			my ( $is_addable, $is_added ) =
			  $rs_filter->addable_phenotype_state( $item );
			$item->{addable}    = $is_addable;
			$item->{added} = $is_added;
	
			foreach my $rs ( @{ $item->{resultsets} } ) {
				if ( !$tr->more_than_max && $is_addable ) {
					$rs_counter++;
					push @top_resultsets, $rs;
					if ( $rs_counter > 16 ) {
						$tr->more_than_max(1);
					}
				}
			}
			foreach my $pa (@{$item->{phenotype_annotations} || [] }) {
				my $pannotationtext;
				my $patype;
				my $paident = $pa->{identifier};
				if ($paident =~ /^D/){
					$pannotationtext = $self->DS('Ontology')->get_mesh_heading_from_id($pa->{identifier})->name;
					$patype = "MeSH";			
				}
				elsif($paident =~ /^HP/){
					my $hponame = $self->DS('Ontology')->get_name_from_id($pa->{identifier})->first;
					$pannotationtext = $hponame ? $hponame->name : undef;
					$patype = "HPO";
								
				}
				$pa->{text}=$pannotationtext;
				$pa->{type}=$patype;
			}	
		}
		$tr->value(\@top_resultsets);
	}
	
sub retrieve_annotation {
}

sub retrieve_concept {
}

sub retrieve_region {
	my ($self) = @_;
	$self->retrieve_gene;
}

sub retrieve_gene {
	my ($self)        = @_;
	my $qs   = $self->f('QueryString');
	my $threshold = $self->fval('Threshold');    #default to zero threshold
	
	my ( $studies, $rs_data, $m_data ) = $self->DS('Browser')
	  ->get_studies_rsets_and_markers_in_region_above_threshold(
		{
			threshold => $threshold,
			chr       => $qs->chr_no,
			start     => $qs->start,
			stop      => $qs->stop,
		}
	  );

	 my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('rs');
	 $id_filter->value($rs_data);
}

sub retrieve_keyword {
	my ($self)        = @_;
	
	my $qs_value = $self->fval('QueryString');
	my $xap       = $self->DS('Xapian');
	
# Send query with supplied query string to text search engine. This may cause an exception
# to be thrown (e.g. invalid query syntax), so let's catch that
#grab list of identifiers from Xapian database and then retrieve pmethod object for each

	my ( $total, $pmethod_identifiers ) =
	  $xap->search_all( $qs_value, 'phenotypes');
	$self->log->info("pmethods:",join(",",@{$pmethod_identifiers}));
	# and then retrieve phenotype object for each
	$pmethod_identifiers ||= [];
	my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('pm');
	 $id_filter->value($pmethod_identifiers);
}

sub retrieve_marker {
	my ($self) = @_;
	my $qs = $self->fval('QueryString');
	my $qs_filter_value = $qs->value;
	my $threshold = $self->fval('Threshold');    #default to zero threshold
	my $marker = $qs->marker;
	!$marker and return ();
	#markers already retrieved by 'recognise_qstring' - but need identifier and chr to do lookup
	my $identifier = $marker->{marker}->identifier;
	my $chr            =  $marker->{coords} ? $marker->{coords}->chr : 'Y';

	my ( $studies, $rs_data, $m_data ) =
			  $self->DS('Browser')->get_studies_rsets_and_markers_by_identifier_above_threshold(
				{
					threshold  => $threshold,
					identifier => $identifier,
					chr        => "chr$chr"
				}
			 );
	warn "rs_data:".Dumper($rs_data);
	my $id_filter = $self->f('IdentifierList');
	 $id_filter->class('rs');
	 $id_filter->value($rs_data);
}

sub retrieve_none {
	my ($self)        = @_;
	$self->no_results(1);
	$self->no_options(1);
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Phenotypes.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

