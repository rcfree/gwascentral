# $Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $

=head1 NAME

GwasCentral::Search::Query::Studies - Query studies


=head1 SYNOPSIS

    use GwasCentral::Search::Query::Studies;
    my $search = GwasCentral::Search::Query::Studies->new;
    $search->option('q','cancer');
    my @results = $search->results;


=head1 DESCRIPTION

This class performs a search across Studies using a query string supplied as a parameter (q). It will also take into account the threshold (t) level
when searching across a region, gene or marker.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Query::Basic;
use strict;
use warnings;
use Moose;
use Data::Dumper qw(Dumper);
extends qw(GwasCentral::Search::Query);

has '+no_Format' => ('default'=>1);
has '+possible_Filters' => (
	default => sub {
		[qw(AddedResultsets QueryString SessionID)
		];
	}
);

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Studies.pm 1552 2010-10-19 02:14:24Z rcf8 $ 

=cut

