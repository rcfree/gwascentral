# $Id$

=head1 NAME

GwasCentral::Search::Filter


=head1 SYNOPSIS

	package GwasCentral::Search::Filter::Example;
	use Moose;
	extends qw(GwasCentral::Search::Filter);
	
	has '+retrieve_from' => ('default'=>'t');
	has '+filter_type'=>('default'=>'list');
	has 'list'=>('is'=>'rw','builder'=>'_list_builder');
	has '+value'=>('default'=>'3');
	has '+panel' => ('default'=>'options');
	has '+label' =>('default'=>'P-value threshold');

=head1 DESCRIPTION

A filter is just that, a filter on the results returned by a search query.
Filters are set up in a 'Query' module.

The base filter provides accessors for both web/database operations AND template operations.

The 'populate' subroutine by default fills 'value' with the CGI parameter name in 'retrieve_from'
When the 'filter_type' is a list it will throw an exception if the 'value' is not in the list.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::Filter;
use Moose;
extends qw(GwasCentral::Base);
has 'retrieve_from' => ('is'=>'rw');  #CGI parameter to retrieve value from
has 'value'=>('is'=>'rw', 'default' => undef); #current value of the filter
has 'Query'=>('is'=>'rw'); #access to Search::Query based module
has 'default' => ('is'=>'rw', 'default'=>undef); #default value if none initially
has 'label'=>('is'=>'rw'); #label displayed on page

has 'ignore_in_url' => ('is'=>'rw');
has 'html_id' => ('is'=>'rw', 'default' => sub { my ($self) = @_; $self->retrieve_from }, 'lazy' => 1 );
has 'html_class' => ('is'=>'rw', 'default' => 'filter');
has 'value' => ('is'=>'rw');
has 'template' => ('is'=>'rw');
has 'fieldset' => ('is'=>'rw');
has 'hidden' => ('is'=>'rw');
has 'is_category' => ('is'=>'rw','default'=>undef);
has 'hide_on_none' => ('is'=>'rw','default'=>undef);
has 'template_on_none' => ('is'=>'rw','default'=>undef);

use Data::Dumper qw(Dumper);
use Template;
use Env;
use HTML::Entities;

sub has_config {
	return undef;
}

sub gbrowse_qstring {
	my ($self) = @_;
	return "RegionView.".$self->retrieve_from."=".$self->value;
}

sub as_string {
	my ($self) = @_;
	if ($self->value) {
		if (ref($self->value) eq 'ARRAY') {
			return join(",",@{$self->value});
		}
		else {
			return $self->value;
		}
	}
	else {
		return "";
	}
}

sub name {
	my ($self) = @_;
	my $name = ref($self);
	$name =~ /GwasCentral::Search::Filter::(.+)/;
	return $1;
}

sub category_info {
	my ($self) = @_;
	my $value = $self->value;
	return undef if !$value;
	if (ref($value)=~/ARRAY/) {
		$value = join(" or ",@{$self->value});
	}
	$self->log->info("value:$value");
	return $self->name . "=" . $value;
}

sub to_html {
	my ($self) = @_;
	my $tmpl = $self->template;
	my $tt =Template->new({
   	 INCLUDE_PATH => $ENV{GWASCENTRAL_HOME}."/htmltemplates/lib",
    	INTERPOLATE  => 1,
	}) || die "$Template::ERROR\n";
	
	my $vars = { 
		curr_filter => $self
	};
	
	my $html="";
	#process into $html variable rather than STDOUT
	$tt->process("macros/filters/".$tmpl.".tt2", $vars,\$html) or $self->throw("Unknown error while generating filter ".$self->name."\n".$tt->error);
	return $html;
}

sub populate {
	my ($self, $params) = @_;
	my $value = $params->{$self->retrieve_from};
	defined($value) and $self->value($value);
}

sub html_value {
	my ($self) = @_;
	return undef if !$self->value;
	return decode_entities($self->value);
}

sub update {
}

sub as_array {
	my ($self) = @_;
	my @array;
	if (ref($self->value) eq 'ARRAY') {
		if (defined($self->value)) {
			@array = @{$self->value};
		}
		else {
			return undef;
		}	
	}
	else {
		if (defined($self->value)) {
			@array = ($self->value);
		}
		else {
			return undef;
		}
	}
	return \@array;
}

1;
