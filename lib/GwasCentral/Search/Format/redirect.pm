package GwasCentral::Search::Format::redirect;
use Moose;
extends qw(GwasCentral::Search::Format);
#use GwasCentral::Base qw(open_infile);
use HTML::Entities;

has '+is_redirect' => ('default'=>1);
has 'querystring' => ('is'=>'rw');
sub redirect_url {
	my ($self) = @_;
	$self->Query->results;
	my $qs = $self->retrieve_evaled_file($self->querystring);
	$self->log->info("evaled_qs:$qs");
	return $qs;
}

sub retrieve_evaled_file  {
	my ($self, $file) = @_;

#	my $fh = open_infile($query_file);
#	
#	while (<$fh>) {
#		$file.=$_;
#	}
#	$fh->close;
	$file=~s/\"/\\"/g;
	$self->log->info("Using query file:$file");
	
	my $eval_str;
	foreach my $filt(values %{$self->Query->Filters}) {
		$eval_str.= "my \$".$filt->name." = '".$filt->as_string."';\n";
	}
	$eval_str.="\$evaled = \"".$file ."\"";
	
	my $evaled;
	eval $eval_str;
	if ($@) {
		$self->throw("Error:".$@);
	}
	$self->log->info("$evaled");
	#$self->file_content($evaled);
	return $evaled;
}

1;