package GwasCentral::Search::Format::csv;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::Delimited);
has '+content_type' => ('default'=>'text/csv');

sub export {
	my ( $self, $data ) = @_;
	$self->_to_text(',');
}
1;