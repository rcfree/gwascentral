package GwasCentral::Search::Format::yaml;
use Moose;
extends qw(GwasCentral::Search::Format);
has '+content_type' => ('default'=>'application/yaml');
use YAML qw(Dump);

sub export {
	my ($self) = @_;
	$self->Query->results;
	return Dump(  $self->Query->structured_data );
}
1;