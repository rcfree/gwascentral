package GwasCentral::Search::Format::xml;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(
GwasCentral::Search::Format::Nature::XML
);
has '+content_type' => ('default'=>'text/xml');
has '+is_download' => ('default'=>1);

use XML::Atom::SimpleFeedOS;

use Data::Dumper qw(Dumper);

sub export {
	my ($self) = @_;
	$self->xml_root(lc($self->Query->name));
	$self->Query->results;
	return $self->to_xml( $self->Query->structured_data );
}
1;