package GwasCentral::Search::Format::jsonp;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::JSON);
has '+content_type' => ('default'=>'application/json');
has 'tt'=>('is'=>'rw');
use Template;
use Data::Dumper qw(Dumper);
use Env;
sub export {
	my ($self) = @_;
	my @rows = ();
	my $tt = Template->new({
   	 INCLUDE_PATH => $ENV{HGVBASEG2PWEB_HOME}."/htmltemplates/lib",
    	INTERPOLATE  => 1,
	}) || die "$Template::ERROR\n";
	$self->tt($tt);
	my $callback;
	my $filters = $self->Query->params->{'filters'};
	my %json_data = ();
	if ($filters) {
		$callback = $self->Query->params->{'callback'};
		tie my %fieldsets,"Tie::Hash::Sorted";
		foreach my $filt_name(@{$self->Query->possible_Filters}) {
			my $f = $self->Query->f($filt_name);
			
			my $fs = $f->fieldset || 'none';
			$self->log->info("filter $filt_name, fs:$fs");
			$f->html_id($f->retrieve_from.$filters);
			$fieldsets{$fs}||=[];
			push @{$fieldsets{$fs}}, $f;
		}
		foreach my $fs(@{$self->Query->fieldset_order}) {
			push @rows,"<fieldset name='".$fs."'>";
			foreach my $f(@{$fieldsets{$fs}}) {
				my $html = $f->to_html;
				$self->log->info("filter". $f->name.":".$html);
				push @rows, $html;
			}
			push @rows,"</fieldset>";
		}
	}
	else {
		$self->Query->results;
		foreach my $datum(@{$self->Query->data || []}) {
			push @rows, $self->data_row_to_jsonp($datum);
		}
		$callback = $self->Query->params->{'callback'};
		$json_data{'iTotalRecords'} = $self->Query->pager->total_entries;
		$json_data{'iTotalDisplayRecords'} = $self->Query->pager->total_entries;
	}
	$json_data{'aaData'} = \@rows,
	return $callback."(".$self->json->encode(\%json_data).")";
}

sub data_row_to_jsonp {
	my ($self,$row) = @_;
	my $tmpl = $self->Query->jsonp_template;
	my $tt = $self->tt;
	
	my $vars = { 
		row => $row,
		Query => $self->Query,
	};
	my $html="";
	#process into $html variable rather than STDOUT
	$tt->process("jsonp/".$tmpl.".tt2", $vars,\$html) || $self->throw("Error while processing template $tmpl:".$tt->error());
	return [split(/\{end\}/,$html)];
}
1;