package GwasCentral::Search::Format::atom;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(
GwasCentral::Search::Format::Nature::Feed
GwasCentral::Search::Format::Nature::XML
);
has '+content_type' => ('default'=>'application/atom');
has '+is_download' => ('default'=>0);
use XML::Atom::SimpleFeedOS;

use Data::Dumper qw(Dumper);


sub export {
	my ($self)         = @_;
	my @category_attrs = ();
	my %categories     = %{ $self->_get_categories };
	foreach my $category_term ( keys %categories ) {
		push @category_attrs,
		  (
			"category",
			{ term => $category_term, scheme => $categories{$category_term} }
		  );
	}
	my $query = $self->Query;
	my $data = $query->results;
	my $pager = $query->pager;

	my @feed_attrs = (
		title                     => 'GWAS Central search results',
		link                      => $self->Query->host."/".$self->Query->url_path,
		id                        => $self->Query->host."/".$self->Query->url_path,
		author                    => 'gwascentral',
		generator                 => 'gwascentral',
		'opensearch:totalResults' => $pager ? $pager->total_entries : undef,
		'opensearch:itemsPerPage' => $pager ? $pager->entries_per_page : undef,
		'opensearch:startIndex'   => $pager ? $pager->first : undef,
		@category_attrs,
	);
	
	my $feed = XML::Atom::SimpleFeedOS->new(@feed_attrs);

	foreach my $item ( @{ $data } ) {
		my @entry = (
			id      => $item->{identifier} . " ",
			title   => $item->{title},
			summary => $self->_generate_html_for_summary($item),
			link    => $item->{link},
			content => {
				type    => 'application/xml',
				content => $self->to_xml($item)
			},
			updated => $item->{date_updated},
		);
		#$self->log->info("entry:".Dumper({@entry}));
		$feed->add_entry(
			id      => $item->{identifier} . " ",
			title   => $item->{title},
			summary => $self->_generate_html_for_summary($item),
			link    => $item->{link},
			content => {
				type    => 'application/xml',
				content => $self->to_xml($item)
			},
			updated => $item->{updated},
		);
	}
	return $feed->as_string;
}
1;