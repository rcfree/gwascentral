package GwasCentral::Search::Format::tsv;
use Moose;
extends qw(GwasCentral::Search::Format);
with qw(GwasCentral::Search::Format::Nature::Delimited);
has '+content_type' => ('default'=>'text/tsv');

sub export {
	my ( $self, $data ) = @_;
	$self->_to_text("\t");
}
1;