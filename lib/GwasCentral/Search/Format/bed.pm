package GwasCentral::Search::Format::bed;
use Moose;
extends qw(GwasCentral::Search::Format::FromQuery);

has '+content_type' => ('default'=>'text/plain');
has '+is_download' => ('default'=>1);
use XML::Atom::SimpleFeedOS;

use Data::Dumper qw(Dumper);


1;