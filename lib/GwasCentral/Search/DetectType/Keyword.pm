# $Id$

=head1 NAME

GwasCentral::Search::Recognition::Keyword


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::Keyword;
use Moose;
extends qw(GwasCentral::Search::DetectType);

sub detect {
	my ($self, $term) = @_;
	return 'Keyword';
}

1;