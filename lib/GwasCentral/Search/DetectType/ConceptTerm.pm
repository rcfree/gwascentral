# $Id$

=head1 NAME

GwasCentral::Search::Recognition::Annotation


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::ConceptTerm;
use Moose;
extends qw(GwasCentral::Search::DetectType);
#has 'biggest_feature' => ('is'=>'rw');
#has 'phenotype_ids' =>('is'=>'rw');

sub detect {
	my ($self, $term) = @_;

	my $query = $self->Query;
	my $concept = $query->f('Concept');
	if ($query->fval('PhenotypeID')) {
		$query->fval('QueryString',$query->fval('Concept'));
		return 'Concept';
	}
}
	1;