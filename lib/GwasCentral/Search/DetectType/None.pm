# $Id$

=head1 NAME

GwasCentral::Search::Recognition::Keyword


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::None;
use Moose;
extends qw(GwasCentral::Search::DetectType);
has '+no_display' => ('default'=>1);

sub detect {
	my ($self, $term) = @_;
	if (!defined($term) || length($term)==0) {
		$self->log->info("locking DetectType");
		$self->Query->lock_DetectType(1);
		return 'None';
	}
}

1;