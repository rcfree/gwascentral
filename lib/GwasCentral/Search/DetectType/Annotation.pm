# $Id$

=head1 NAME

GwasCentral::Search::Recognition::Annotation


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::Annotation;
use Moose;
extends qw(GwasCentral::Search::DetectType);
use Data::Dumper qw(Dumper);

sub detect {
	my ($self, $term) = @_;
	!$term and return 'None';
			
	if ($self->Query->fval("PhenotypeID")) {
		return 'Annotation';
	}
	my @phenotype_ids = $self->try_to_get_phenotype_ids($term);
	
	if (scalar(@phenotype_ids) > 0) {
	 	$self->Query->fval("PhenotypeID",\@phenotype_ids);
	 	return 'Annotation';
	}
	else {
		return undef;
	}
}

sub try_to_get_phenotype_ids {
		my ($self, $qstring) = @_;

		my $ontology_db = $self->Query->DataSources->{Ontology};
		my $study_db = $self->Query->DataSources->{Study};
		my %uniquehash = ();
		my $search_rs = $ontology_db->get_mesh_headings_from_terms($qstring);
		my $hposearch_rs = $ontology_db->get_single_term_from_name($qstring);
		my $hpotermcount_rs = $ontology_db->count_terms_by_name($qstring);
		my $hposyncount_rs = $ontology_db->count_synonyms_by_name($qstring);

		# first; look at MeSH
		while (my $headings = $search_rs->next){
 			
 			#see if we annotate to the submitted concept
 			my $ipid= $headings->get_column('heading_meshid');
 			my $initialsearch_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($ipid);
 			if ($initialsearch_rs ne '0'){
				$uniquehash{$ipid}='1';
			}
 			
 			# get child terms and see if we annotate to those
 			my $treestring=$headings->get_column('treeid').".%";
 			my $search2_rs = $ontology_db->get_mesh_children_from_treeid($treestring);
			while (my $subterms = $search2_rs->next){
  				my $pid=$subterms->meshid;
  				my $searchstudy_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($pid);
  				if ($searchstudy_rs ne '0'){
					$uniquehash{$pid}='1';
				}
  			}	
 		}
    	
 		my $row = $search_rs->first;
 		
 		
		# second; look at HPO
		# does string match terms?
		if ($hpotermcount_rs ne '0'){
			$hposearch_rs = $ontology_db->get_single_term_from_name($qstring);
		}
		
		# does string match synonyms?
		elsif ($hposyncount_rs ne '0'){
			$hposearch_rs = $ontology_db->get_single_synonym_from_name($qstring);
		}
		
 		while (my $hpoterms = $hposearch_rs->next){
 			
 			# see if we annotate to the initial term
 			my $ipid= $hpoterms->get_column('hpoid');
 			my $preinitialsearch_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($ipid);
 			if ($preinitialsearch_rs ne '0'){
					$uniquehash{$ipid}='1';
			}
			
			# get MeSH mapping for the term and see if we annotate to that
			my $meshmap_rs = $ontology_db->get_meshid_from_hpoid($ipid);
			while (my $meshmapterms = $meshmap_rs->next){
				my $mmid= $meshmapterms->get_column('meshid');
				my $meshmapsearch_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($mmid);
 				if ($meshmapsearch_rs ne '0'){
					$uniquehash{$mmid}='1';
				}
			}
			
 			# get all chidren for the term and see if we annotate to those
 			my $search_rs2 = $ontology_db->get_children_terms_from_id($ipid);
 			while (my $allhpoterms = $search_rs2->next){
 				my $cpid= $allhpoterms->get_column('hpoid');
 				my $initialsearch_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($cpid);
 				if ($initialsearch_rs ne '0'){
					$uniquehash{$cpid}='1';
				}
				
				# get mesh mappings for all chidren for the term and see if we annotate to those
 			    my $cmeshmap_rs = $ontology_db->get_meshid_from_hpoid($cpid);
				while (my $cmeshmapterms = $cmeshmap_rs->next){
					my $cmmid= $cmeshmapterms->get_column('meshid');
					my $cmeshmapsearch_rs = $study_db->count_phenotypeannotations_by_phenotypeidentifier($cmmid);
 					if ($cmeshmapsearch_rs ne '0'){
						$uniquehash{$cmmid}='1';
					}
				}
   			}
  		}
 		return keys %uniquehash;
	}
	1;