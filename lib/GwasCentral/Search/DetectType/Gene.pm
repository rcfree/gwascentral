# $Id$

=head1 NAME

GwasCentral::Search::Recognition::GeneRegion


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Search::DetectType::Gene;
use Data::Dumper qw(Dumper);
use Moose;
extends qw(GwasCentral::Search::DetectType);

sub detect {
	my ($self, $term) = @_;
	return undef if !$term || ($term && length($term)==0);
	my ( $is_region, $chr, $start, $stop );
	my $qs = $self->Query->f('QueryString');
	$qs->populate({q=>$term});

	#if not array then search for features matching the query_string
	my ($biggest_feature, $features_sorted_by_size) = $self->get_biggest_feature_region_for_term($term,"search_gene");
	
	#if features - find the biggest.. and populate 'region' accessor with reference, start and stop
	if ( $biggest_feature) {
		$qs->sorted_features($features_sorted_by_size);
		$qs->region($biggest_feature);
		return 'Gene';
	}
}



1;