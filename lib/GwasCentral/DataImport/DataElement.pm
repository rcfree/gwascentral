# $Id: DataElement.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::DataElement - Class which stores various data for a single 'line' in a file


=head1 SYNOPSIS
	...
	my $element = GwasCentral::DataElement->new;
	my ($marker,$status) = $rmarker->get_validated_marker('rs699');
	$element->marker($marker);
	$element->line($line);
	$element->status($status);


=head1 DESCRIPTION

A container class representing a single line in a file.
Contains accessors for marker (Bio::SeqFeature), line (arrayref of values), status (hashref), line_no (scalar), flip (0/1 flag)


=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::DataElement;

use strict;
use warnings;

use Moose;
use English qw( -no_match_vars );

# Private variables & constants
has 'marker'  => ( 'is' => 'rw' );    #marker data as BioSeqFeature
has 'line'    => ( 'is' => 'rw' );    #arrayref of line data
has 'line_no' => ( 'is' => 'rw' );    #line number in file
has 'status' => ( 'is' => 'rw', 'default' => sub { {} } )
  ;                                   #contains RetrieveMarker object
has 'flip' => ( 'is' => 'rw' );    #carry out strand-flip (1) or not (0/undef)

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: DataElement.pm 1515 2010-07-13 13:52:11Z rcf8 $

=cut

