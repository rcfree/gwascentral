# $Id: Database.pm 1646 2011-04-05 16:14:08Z rcf8 $

=head1 NAME

GwasCentral::DataImport::Database - Base database class for DataImport Export via Database Modules


=head1 SYNOPSIS

	package GwasCentral::DataImport::Export::Database::Test
	use base qw(GwasCentral::DataImport::Export::Database);

	#overrides base export method
	sub output {
		my ($self, $lines);
		my @lines = @{$lines};

		#use methods from the base class eg.
		my $db     = $self->DS('Study');

        my ( $study, $experiment ) = $self->get_study_and_experiment($line);
	}

=head1 DESCRIPTION

Base class for exporting to database (using GwasCentral::Database modules) containing commmon methods used by MultiPanel and OnePanel modules.
In inheriting classes the 'output' method is defined and calls these methods.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Target::Database;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Target);
with qw(GwasCentral::DSContainer);
use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);;
use Carp qw(confess);
$Data::Dumper::Maxdepth=1;
has 'study' => ( 'is' => 'rw', 'default' => sub { {} } )
  ;    #hash of DBIx::Class study objects with definable key
has 'hotlinkcollection' => ( 'is' => 'rw' );    #hash of DBIx::Class hlc objects
has 'resultset' => ( 'is' => 'rw', 'default' => sub { {} } )
  ;    #hash of DBIx::Class resultset objects with definable key
has 'experiment' => ( 'is' => 'rw', 'default' => sub { {} } )
  ;    #hash of DBIx::Class experiment objects with definable key
has 'apanel' => ( 'is' => 'rw', 'default' => sub { {} } )
  ; #hash of hash of DBIx::Class assayedpanel objects with definable key of study then panel name
has 'multi_panel' => ( 'is' => 'rw' )
  ;    #multi panel flag (ie. all panels on one line or not)

sub has_config { 0; }

#sets up database for use by child modules
sub setup {
	my ( $self, $core ) = @_;
	$self->log->info( "DataSources added to Target::Database from core" );
	$self->use_DS($core);
}

=head2 get_data_item

	  Usage      : my $experiment = $self->get_data_item( 'experiment', $experimentid, 'identifier' );
	  Purpose    : Get database item. Builds up a method name based on the type and field.
	               e.g. the example above becomes 'get_experiment_by_identifier' and this is called against the GwasCentral::Database::Study object
	               For speed, items are cached in a hash (e.g. studies, experiments etc.)
	  Returns    : GwasCentral::Schema object
	  Arguments  : Type of object;key to retrieve;field name
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub _get_data_item {
	my ( $self, $type, $key, $field ) = @_;

	!$key and confess "No key provided";
	my $items = $self->$type;
	my $item  = $items->{$key};
	if ( !$item ) {

		#my $get_method = "get_$type" . "_by_$field";
		$item =
		  $self->DS('Study')->dbh->resultset( ucfirst($type) )
		  ->search( { $field => $key },
			{ columns => [ 'identifier', $type . "id" ] } )->single;

		#$get_method($key);
		if ( !$item ) {
			$self->throw( "Unable to find $type by " . $field
				  . " '$key' - have you loaded metadata?" );
		}
		$items->{$key} = $item;
	}
	else {
	}
	return $item;
}

=head2 get_data_item_from_other

	  Usage      : my $experiment = $self->get_data_item_from_other( 'study','identifier', 'experiment', $experimentid);
	  Purpose    : Get database item based on another item. Builds up a method name based on the types and fields.
	               e.g. the example above becomes 'get_experiment_by_identifier_and_study' and this is called against the GwasCentral::Database::Study object
	               For speed, items are cached in a hash of hashes (e.g. experiments within studies etc.)
	  Returns    : GwasCentral::Schema object
	  Arguments  : Type of object;key to retrieve;field name;other type of object;other key to retrieve
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub _get_data_item_from_other {
	my ( $self, $type, $key, $field, $other, $otherid ) = @_;

#        $self->log->info("type:$type, key:$key, field:$field, other:$other, otherid:$otherid");
	my $items = $self->$type;
	!$items->{$otherid} and $items->{$otherid} = {};
	my $item = $items->{$otherid}->{$key};
	if ( !$item ) {
		my $get_method = "get_$type" . "_by_$field" . "_and_$other";
		$item = $self->DS('Study')->$get_method( $key, $otherid );
		if ( !$item ) {
			$self->throw( "Unable to find $type by " . $field
				  . " '$key' in $other '$otherid' - have you loaded correct metadata?"
			);
		}
		else {
		}
		$items->{$otherid}->{$key} = $item;
	}

	return $item;
}

=head2 get_study_and_experiment

	  Usage      : my ($study,$experiment) = $self->get_study_and_experiment($element->line);
	  Purpose    : Gets study and experiment from the database or the line
	  Returns    : GwasCentral::Schema::Study;GwasCentral::Schema;Experiment
	  Arguments  : DataElement line
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub get_study_and_experiment {
	my ( $self, $studyid, $experimentid ) = @_;

	#retrieve study object and cache
	my $study = $self->_get_data_item( 'study', $studyid, 'identifier' );

	#retrieve experiment object and cache it
	my $experiment =
	  $self->_get_data_item( 'experiment', $experimentid, 'identifier' );

	return ( $study, $experiment );
}

# sub get_study_by_pubmedid {
#        my ( $self, $pubmedid ) = @_;
#
#        #retrieve study object and cache
#        my $study = $self->_get_data_item( 'study', $studyid, 'identifier' );
#
#        #retrieve experiment object and cache it
#        my $experiment =
#          $self->_get_data_item( 'experiment', $experimentid, 'identifier' );
#
#        return ( $study, $experiment );
#    }

=head2 get_hotlink_collection

      Usage      : my ($experiment) = $self->get_hotlinkcollection($element->line);
      Purpose    : Gets study and experiment from the database or the line
      Returns    : GwasCentral::Schema::hotlinkcollection;
      Arguments  : DataElement line
      Throws     :
      Status     : Public
      Comments   :

=cut

sub get_hotlinkcollection {
	my ( $self, $hlc_identifier ) = @_;

	#retrieve study object and cache
	my $hotlinkcollection =
	  $self->_get_data_item( 'holinkcollection', $hlc_identifier,
		'identifier' );

	return ($hotlinkcollection);
}

sub get_apanel {
	my ( $self, $apanel_name, $studyid ) = @_;
	return $self->_get_data_item_from_other( 'apanel', $apanel_name, 'name',
		'study', $studyid );
}

=head2 get_resultset

	  Usage      : my ($resultset) = $self->get_resultset($element->line,$experiment,$rs_identifier);
	  Purpose    : Gets resultset from the database or the line
	  Returns    : GwasCentral::Schema::Resultset
	  Arguments  : DataElement line; GwasCentral::Schema::Experiment object; Resultset Identifier
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub get_resultset {
	my ( $self, $resultsetid ) = @_;

#if multiple_resultsets then get the resultset from the experiment using the identifier
        if ($self->resultsets) {
		        my $resultset =
		          $self->_get_data_item( 'resultset', $resultsetid,
		            $self->resultset_lookup || 'identifier');
	        return $resultset;
        }
    else {
#$self, $type, $key, $field, $other, $otherid
#get resultset lookup field and retrieve new or cached resultset object based on this
	!$self->resultset_lookup and die "No resultset_lookup field set";
	my $resultset =
	  $self->_get_data_item( 'resultset', $resultsetid,
		$self->resultset_lookup );

	#	            $self->log->info("get_resultset");
	return $resultset;

	       }
}

sub get_hlc {
	my ( $self, $hlc_identifier ) = @_;

#if multiple_resultsets then get the resultset from the experiment using the identifier
#        if ($self->resultsets) {
#               my $resultset =
#                 $self->get_data_item( 'resultset', $rs_identifier,
#                   $self->resultset_lookup || 'identifier');
#           return $resultset;
#        }
#    else {
#$self, $type, $key, $field, $other, $otherid
#get resultset lookup field and retrieve new or cached resultset object based on this
	!$self->hlc_lookup and die "No hlc_lookup field set";

	$self->log->info( $self->hlc_lookup );

	my $hlc =
	  $self->_get_data_item( 'hotlinkcollection', $hlc_identifier,
		$self->hlc_lookup );

	#                $self->log->info("get_hlc");
	#
	#              $self->log->info("got hlc_object");
	return $hlc;

	#       }
}

=head2 store_usedmarkerset

	  Usage      : my $ums = $self->store_usedmarkerset( $experiment, $marker );
	  Purpose    : Create/find a usedmarkerset for an experiment and marker
	  Returns    : GwasCentral::Schema::Usedmarkerset
	  Arguments  : GwasCentral::Schema::Experiment object; DataElement marker (BioSeqFeature)
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub export_usedmarkerset {
	my ( $self, $experiment, $marker ) = @_;

	my $template = $self->template;

	#check if exists already in this experiment
	my $mid = $marker->display_name;
	my $ums = $experiment->search_related(
		'usedmarkersets',
		{ markeridentifier => $mid },
		{ columns          => [ 'usedmarkersetid', 'experimentid' ] }
	)->single;

	if ( !$ums ) {

		#$self->log->info("create ums for marker ".$marker->display_name);
		$ums =
		  $experiment->create_related( 'usedmarkersets',
			{ markeridentifier => $marker->display_name } );
	}

	return $ums;
}

sub export_frequencycluster {
	my ( $self, $ums, $freq_cluster, $apanel ) = @_;
	$freq_cluster->{assayedpanelid} = $apanel->assayedpanelid;

	#		$self->log->info("freqcluster:".Dumper($freq_cluster));
	my $frequencyclusters =
	  $ums->find_or_create_related( 'frequencyclusters', $freq_cluster );
	return $frequencyclusters;
}

=head2 store_significances

	  Usage      : $self->store_significances($element,$ums, $experiment,\@frequencyclusters)
	  Purpose    : Create/find significances (including fcs) for a single line (if multiple pvalues creates multiple significances)
	  Returns    : GwasCentral::Schema::Frequencycluster
	  Arguments  : DataElement object; GwasCentral::Schema::Usedmarkerset object; GwasCentral::Schema::Experiment object; Arrayref of GwasCentral::Schema::Frequencycluster objects or undef
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub export_significances {
	my ( $self, $element, $ums, $resultset, $fcs, $hlc ) = @_;
	my @significances;
	my $field = 'pvalue:'.$resultset->identifier;
	my $pvalue = $element->line->{'pvalue'} || $element->line->{$field};

	
	#check for current significance if so - compare pvalues
	#if does not exist create it
	my $sig = $resultset->search_related(
		'significances',
		{ usedmarkersetid => $ums->usedmarkersetid },
		{
			columns =>
			  [ 'significanceid', 'usedmarkersetid', 'unadjustedpvalue' ]
		}
	)->single;
	
	if ($sig && $pvalue) {
		$self->log->info("sig:".Dumper({$sig->get_columns}));
		$self->log->info("pvalue:$pvalue");
		if ( $sig->unadjustedpvalue == $pvalue ) {
			$self->log->info( "SAME p-value for marker "
				  . $element->marker->display_name
				  . " in resultset "
				  . $resultset->identifier . " ("
				  . $sig->unadjustedpvalue
				  . ")" );
		}
		else {
			$self->log->error( "DIFF p-value for marker "
				  . $element->marker->display_name
				  . " in resultset "
				  . $resultset->identifier . " (db="
				  . $sig->unadjustedpvalue
				  . " & file=$pvalue)" );
			$element->status->{t}=1;
			return;
		}
	}
	else {
		my $significance = {
			unadjustedpvalue => $pvalue,
			usedmarkersetid  => $ums->usedmarkersetid,

		};

		if ($hlc) {
			$significance->{hotlinkcollectionid} = $hlc->hotlinkcollectionid;
		}

		$sig = $resultset->create_related( 'significances', $significance );
	}

	#find_or_create_related( 'significances', $significance );
	if ( $self->frequencies ) {
		my @fcs;
		if ( !$fcs ) {
			my @frequencyclusters = $ums->frequencyclusters->all;
			@fcs =
			  map { { frequencyclusterid => $_->frequencyclusterid } }
			  @frequencyclusters;
		}
		else {
			@fcs = @{$fcs};
		}

		#	        $self->log->info("fcs:".Dumper(\@fcs));

		foreach my $fc (@fcs) {
			$sig->find_or_create_related( 'fcs', $fc );
		}
	}

	#frequency cluster will be the same for each marker in an experiment
	#if no frequencyclusters passed to method look them up in the experiment

	#create fcs for the individual significance
	#

#set up a single significance for a single resultset
#        if ($self->resultsets) {
#        	foreach my $resultset(@{$self->resultsets}) {
#
#		        my $significance = {
#			        unadjustedpvalue => $element->line->{'pvalue:'.$resultset},
#		            usedmarkersetid  => $ums->usedmarkersetid,
#		        };
#
#				my $info = {
#					marker_identifier => $ums->markeridentifier,
#					#experiment => $ums->experimentid->identifier,
#				};
#
#				foreach my $fcluster($ums->frequencyclusters->all) {
#					my ($af,$gf);
#					foreach my $afrequency($fcluster->allelefrequencies->all) {
#						$af->{$afrequency->allelecombo}=$afrequency->frequencyasproportion;
#					}
#					foreach my $gfrequency($fcluster->genotypefrequencies->all) {
#						$gf->{$gfrequency->genotypecombo}=$gfrequency->frequencyasproportion;
#					}
#					$info->{$fcluster->assayedpanel->name}={
#						frequencyclusterid=>$fcluster->frequencyclusterid,
#						allelefrequencies=>$af,
#						genotypefrequencies=>$gf,
#					};
#
#				}
#
#				#create the significance record using the resultset
#        		my $sig = $self->get_resultset($resultset)->find_or_create_related( 'significances', $significance );
#        		$sig->find_or_create_related('fcs',@fcs);
#        	}
#
#        }
#else {

	# }
}

=head2 begin

	  Usage      : $export->begin;
	  Purpose    : Begin Export database 'transaction'
	  Returns    : Nowt
	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub begin {
	my ($self) = @_;
	$self->DS('Study')->dbh->txn_begin;
}

=head2 commit

	  Usage      : $export->commit;
	  Purpose    : Commit Export database 'transaction' changes
	  Returns    : Nowt
	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub commit {
	my ($self) = @_;
	$self->log->info("commit");
	$self->DS('Study')->dbh->txn_commit;

	#$self->DS('Study')->dbh->disconnect;
}

=head2 rollback

	  Usage      : $export->rollback;
	  Purpose    : Rollback Export database 'transaction' changes
	  Returns    : Nowt
	  Arguments  : None
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub rollback {
	my ($self) = @_;
	$self->log->info("rollback");
	$self->DS('Study')->dbh->txn_rollback;
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVN

$Id: Database.pm 1646 2011-04-05 16:14:08Z rcf8 $

=cut

