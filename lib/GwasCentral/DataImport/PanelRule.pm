# $Id: PanelRule.pm 1519 2010-08-02 14:42:28Z rcf8 $

=head1 NAME

GwasCentral::DataImport::PanelRule - Base class for DataImport PanelRule


=head1 SYNOPSIS

	use base qw(GwasCentral::DataImport::Rule::StrandFlip);

	#overrides base Rule method
	sub run_multi_panel {
		my ($self,$element);
		my $line = $element->line;
		... do stuff with line
	}

	#overrides base Rule method
	sub run_single_panel {
		my ($self,$element);
		my $line = $element->line;
		... do stuff with line
	}


=head1 DESCRIPTION

PanelRules are extended versions of Rules which perform differently for single or multiple panels.
The 'run' method of a PanelRule determines whether one panel or multiple panels are present in the fields and calls the appropriate method.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::PanelRule;

use strict;
use warnings;

# Use OIO inside-out object framework for our OO hierarachy
use Moose;
extends qw(GwasCentral::DataImport::Rule);

use English qw( -no_match_vars );
use Data::Dumper;

has 'panelnames' => ( 'is' => 'rw' );

=head2 run

Runs appropriate method if the template has single or multiple panels

=cut

sub run {
	my ( $self, $element ) = @_;

	if ( $self->panelnames ) {
		$self->run_multi_panel($element);
	}
	else {
		$self->run_single_panel($element);
	}

}

sub after_rules {
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: PanelRule.pm 1519 2010-08-02 14:42:28Z rcf8 $

=cut

