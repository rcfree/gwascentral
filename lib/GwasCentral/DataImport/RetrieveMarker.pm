# $Id: RetrieveMarker.pm 1646 2011-04-05 16:14:08Z rcf8 $

=head1 NAME

GwasCentral::DataImport::RetrieveMarker - Retrieve validated marker from the MarkerDB and set status (used by DataImport::Core)


=head1 SYNOPSIS

	use GwasCentral::DataImport::RetrieveMarker;
	use GwasCentral::Database::Marker;

	#set up retrieve marker object
    my $retrieve_marker =
		  GwasCentral::DataImport::RetrieveMarker->new(
			{ conf_file => $self->config } );
		$retrieve_marker->accession_db( 'dbSNP' );

	#set up marker db and populate db in retrieve_marker
	my $marker_db = GwasCentral::Database::Marker->new(
		{
			conf_file  => $self->config
		}
	);
	$retrieve_marker->db($marker_db);


=head1 DESCRIPTION

This class deals with marker DataImport logic. It returns the marker in BioSeqFeature format along with a status hash.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::RetrieveMarker;

	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::Base);
	with qw(GwasCentral::DSContainer);
	use Bio::SeqFeature::Generic;

	use English qw( -no_match_vars );
	use Data::Dumper;

	# Private variables & constants
	has 'accession_source'=>('is'=>'rw'); #accession source name (e.g. dbSNP)

	sub has_config { 0; }

=head2 get_validated_marker

	  Usage      : my $marker = $rmarker->get_validated_marker('rs699');
	  Purpose    : Retrieve validated marker using accession
	  Returns    : Nothing
	  Arguments  : DB accession
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

sub get_validated_marker {
		my ( $self, $accession, $status ) = @_;
		$status ||={};

		#retrieve marker from BrowserDB
		my $marker =
		  $self->DS('Browser')->get_marker_by_id( $accession );
		#if no marker present then set n flag and return undef
		if ( !$marker ) {
			$self->log->debug( "Marker with accession $accession ("
				  . $self->accession_source
				  . ") not found" );
				 $status->{n}=1;
			return (undef,$status);
		}

		#if marker is dead set d flag and check revision history to see if it is merged
		if ($marker->{Status} eq "dead") {
			$self->log->info("$accession status is dead");

			if ($marker->{CurrentIdentifier}) {
				my $replacedby_marker =  $self->DS('Browser')->get_marker_by_id( $marker->{CurrentIdentifier} );
				$self->log->info("$accession replaced by " . $marker->{CurrentIdentifier});

				$status->{m}=1;

#				if (defined($latest_rev->strandflipped)) {
#					$status->{f}=1;
#				}

				#if merged marker is dead with no pointer then set r flag
				if ($replacedby_marker->{Status} eq "dead") {
					if ($replacedby_marker->{CurrentIdentifier}) {
						$self->log->info("Get validated marker for ".$replacedby_marker->{CurrentIdentifier});
						return $self->get_validated_marker($replacedby_marker->{CurrentIdentifier},$status);
					}
					$status->{r}=1;
					$self->log->warn( "Replacement marker ". $replacedby_marker->{Accession}
						  . " marked as DEAD" );
						 return (undef,$status);
				}

				return ($self->_convert_marker_to_seqfeature($replacedby_marker),$status);
			}
			else {
				$status->{d}=1;
			$self->log->error(
"Got reference to dead marker $accession with no pointer to current marker which replaced it"
				);
				return (undef,$status);
			}
		}

		#convert marker to seqfeature and return
		return ($self->_convert_marker_to_seqfeature($marker), $status)
	}

=head2 _convert_marker_to_seqfeature

	  Usage      : my $marker = $rmarker->_convert_marker_to_seqfeature($marker)
	  Purpose    : Convert GwasCentral::Schema::Marker to Bio::SeqFeature
	  Returns    : Bio::SeqFeature
	  Arguments  : GwasCentral::Schema::Marker object
	  Throws     :
	  Status     : Public
	  Comments   :

=cut

	sub _convert_marker_to_seqfeature {
		my ($self, $marker)=@_;

#		my @alleles = $self->db->get_limited_alleles_by_markerid($marker->markerid);
#
#		my @allele_seq_list;
#
#		foreach my $allele (@alleles) {
#			if ( $allele->status eq "active" ) {
#				push @allele_seq_list, $allele->alleleseq;
#			}
#		}
#
#		my ($start, $stop, $strand, $chr);
#
#		if ( !$coords ) {
#			$self->log->error("MarkerCoord not found for ref_assembly all coord data will be NULL");
#		}
#		else {
#			$start = $coords->start;
#			$stop = $coords->stop;
#			$strand = $coords->strand;
#			$chr = $coords->chr;
#		}
		
		#create generic SeqFeature and add chr, alleles and flanks as tags
		#set source to accession and display_name to identifier
		my @allele_list = $marker->{Alleles} ? split(':',$marker->{Alleles}) : ();
		my @alleles = map { /^\((.+)\)$/ } @allele_list;
		my $feature = Bio::SeqFeature::Generic->new(
			-seq_id => $marker->{Chr},
			-start => $marker->{Start},
			-stop => $marker->{Stop},
			-strand => $marker->{Strand},
			-display_name => $marker->{Identifier},
			-source => $marker->{Accession},
			-tag          => { alleles=>\@alleles, upstream30bp => $marker->{Upstream30bp}, downstream30bp => $marker->{Downstream30bp} }
			);
		return $feature;
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: RetrieveMarker.pm 1646 2011-04-05 16:14:08Z rcf8 $

=cut