# $Id: AffymetrixLookup.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::Validation::Plugin::AffymetrixLookup - Validation Plugin dealing with affymetrix IDs


=head1 SYNOPSIS

	#in Validation template file
	plugins = AffymetrixLookup

	#the plugin is called by the Validation pipeline

=head1 DESCRIPTION

Plugin to deal with Affymetrix ID lookups.

Does the following:
Gets affymetrix->rsid data from CSV files provided by Affymetrix. Then for each line it:
Gets the RSid using the Affymetrix ID
Compares the strand in the DB with the strand in the Affymetrix file -> if they are different does a strand flip

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Plugin::AffymetrixLookup;

	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::DataImport::Plugin);
	use Data::Dumper qw(Dumper);

	has 'affyid' => ('is'=>'rw', 'default' => sub { {} });

	sub after_setup {
		my ($self, $core)=@_;

        my $files = $core->template->{affymetrixfile};

		#my @annot_files = ref($files eq 'ARRAY') ? @{$core->template->{affymetrixfile}} : ($files);

my @annot_files =  @{$core->template->{affymetrixfile}};


		$self->log->logdie("'affymetrixid' field not set in template!") if !$core->logic_maker->field_exists('affymetrixid');

      	my $fileparser = GwasCentral::FileParser->new();

		$self->log->info("before AffymetrixLookup method");
		foreach my $file ( @annot_files ) {
			$self->log->info("Plugin reading affy -> rsid lookup $file");
			$fileparser->parse_file(
				filename => $file,
				filetype => 'delimited',
				field_sep=>',',
				handler  => sub { $self->_affyid_reader(@_) },
			);
		}
#		$self->log->info("affys:".Dumper($self->affyid));
	}

	sub process_before_marker {
		my ($self, $fields,$core) = @_;
		my $accession = $fields->{'accession'};

		my $blank_rsid =$core->template->{'no_rsid'};

#TODO   add provision for specifiying blank N/A zero rsid

		if ($accession eq $blank_rsid) {
			my $affyid = $fields->{'affymetrixid'};
			my $lookup = $self->affyid->{$affyid}->{accession};
			if ($lookup eq "---"){

            $self->log->error("$fields->{'affymetrixid'} does not have a rsid");
            $fields->{'accession'}=$affyid;

			}else{
			$fields->{'accession'}=$lookup;

			$self->log->info("Looked up rsid in affy hash:$lookup");
			}
		}


#		my $rsid_lookup =$core->template->{'lookup_affyid'};
#
#		if ($rsid ==1){
#
#			my $rsid=$fields->{'accession'}
#
#
#		}



	}

	sub process_after_marker {
		my ($self, $element, $core)=@_;
#$self->log->info("gets to process after marker ");
		return if !$element->marker;

		my $affyid = $element->line->{'affymetrixid'};
		my $skip = $core->template->{'skip'};

		if ($self->affyid->{$affyid}) {
			if (!$element->marker->strand) {
				$self->log->error("Strand not found in database marker");
				$element->status->{s}=1;
				return;
			}

			if (!$self->affyid->{$affyid}->{strand}) {
				$self->log->error("Strand not found in Affymetrix file");
				$element->status->{s}=1;
				return;
			}

			$self->log->debug("strands: marker = ".$element->marker->strand.", affy = ".$self->affyid->{$affyid}->{strand});

			if ($self->affyid->{$affyid}->{strand} ne $element->marker->strand) {
				$self->log->info("Setting strand flip for ".$element->marker->source_tag);
				$element->flip(1);
			}
		}
		else {
			$self->log->error("AffyID $affyid not found for strand flip! Strand was:".$element->marker->strand);
		}




    my $pvalue = $element->line->{'pvalue'};



    if ($pvalue eq $skip) {


    	$self->log->info ("Plugin AffymetrixLookup skips ". $element->line->{'accession'}. "(".$element->line->{'affymetrixid'}.")". " pvalue field is $skip");
    $element->status->{p}=1;
    }






	}

	sub _affyid_reader {
		my ( $self, $line ) = @_;
		return if $line->[0] =~ /^Probe/;
		return if $line->[0] =~ /^#/;
#        $self->log->info(Dumper($line));
#		print "affy:".$line->[0]."=> accession = ".$line->[1].", strand = ".$line->[4]."1\n";


		$self->affyid->{ $line->[0] } =  {accession=>$line->[1], strand=>($line->[4] eq "---" ? undef : int($line->[4]."1") )};
	}


1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: AffymetrixLookup.pm 1515 2010-07-13 13:52:11Z rcf8 $

=cut