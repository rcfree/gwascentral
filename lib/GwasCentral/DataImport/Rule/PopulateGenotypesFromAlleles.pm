# $Id: PopulateGenotypesFromAlleles.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::PopulateGenotypesFromAlleles - Rule to determine genotypes using alleles


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('PopulateGenotypesFromAlleles');
	

=head1 DESCRIPTION

Uses provided alleles fields to build genotypes (e.g. A and T become (A), (A)+(T) and (T).

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::PopulateGenotypesFromAlleles;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Rule);
no Moose;
use Data::Dumper qw(Dumper);

sub run {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $allele1 = $line->{"allele1"};
	my $allele2 = $line->{"allele2"};

	$line->{genotype11} = "($allele1)";
	$line->{genotype12} = "($allele1)+($allele2)";
	$line->{genotype22} = "($allele2)";
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: PopulateGenotypesFromAlleles.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

