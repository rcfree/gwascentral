# $Id: PositionsMatch.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::PositionsMatch - Rule to determine whether chromosomes and positions match between retrieved marker and DataElement


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('PositionsMatch');
	$rule->settings->{'chromosome'}=1;
	$rule->settings->{'position'}=1;

=head1 DESCRIPTION

Compares the chromosome and position in the DataElement 'marker' with the 'line'
Can specify whether chromosome or position part of the rule is switched on (1) or off (undef) using the settings.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::PositionsMatch;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Rule);
no Moose;
use Data::Dumper qw(Dumper);

sub run {
	my ( $self, $element ) = @_;

	my $status = $element->status;
	my $marker = $element->marker;
	my $line   = $element->line;

	my $start  = $marker->start;
	my @chr    = $marker->seq_id;
	my $strand = $marker->strand;

	if ( $self->settings->{'chromosome'} ) {
		if ( $line->{chromosome} ) {
			if ( $chr[0] ne $line->{chromosome} ) {
				$status->{c} = 1;
				$self->log->error( "Chromosomes do not match (db:"
					  . $chr[0]
					  . " and file:"
					  . $line->{chromosome}
					  . ")" );
			}
		}
		else {
			$status->{c} = 1;
			$self->log->error(
				"Chromosome was NULL in file (db:" . $chr[0] . ")" );
		}
	}

	if ( $self->settings->{'positions'} ) {
		if ( $line->{position} ) {
			if ( $start != $line->{position} ) {
				$status->{p} = 1;
				$self->log->error( "Positions do not match (db:$start and file:"
					  . $line->{position}
					  . ")" );
			}
		}
		else {
			$status->{p} = 1;
			$self->log->error("Position was NULL in file (db:$start)");
		}
	}

	return ($status);
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: PositionsMatch.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

