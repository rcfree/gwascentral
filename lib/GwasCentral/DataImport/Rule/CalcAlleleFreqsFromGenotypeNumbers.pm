# $Id: CalcAlleleFreqsFromGenotypeNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::CalcAlleleFreqsFromGenotypeNumbers - PanelRule to calculate allele frequencies from genotype numbers


=head1 SYNOPSIS

	my $rule = $lmaker->import_rule('CalcAlleleFreqsFromGenotypeNumbers');


=head1 DESCRIPTION

Uses allele frequencies to calculate genotype numbers and deals with multiple panels.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Rule::CalcAlleleFreqsFromGenotypeNumbers;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::PanelRule);
no Moose;
use Data::Dumper qw(Dumper);

sub run_multi_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	foreach my $panel ( @{ $self->panelnames } ) {

		my $gt_freq11 = $line->{"genotype11_number:$panel"};
		my $gt_freq12 = $line->{"genotype12_number:$panel"};
		my $gt_freq22 = $line->{"genotype22_number:$panel"};

		my $allele_freq1 = ( $gt_freq11 * 2 ) + $gt_freq12;
		my $allele_freq2 = ( $gt_freq22 * 2 ) + $gt_freq12;
		my $allele_total = $allele_freq1 + $allele_freq2;

		$line->{"allele1_freq:$panel"} =
		  sprintf( "%.3f", $allele_freq1 / $allele_total );
		$line->{"allele2_freq:$panel"} =
		  sprintf( "%.3f", $allele_freq2 / $allele_total );
		$line->{"alleles_total:$panel"} = $allele_total;

	}
}

sub run_single_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $gt_freq11 = $line->{"genotype11_number"};

	my $gt_freq12 = $line->{"genotype12_number"};
	my $gt_freq22 = $line->{"genotype22_number"};

	my $allele_freq1 = ( $gt_freq11 * 2 ) + $gt_freq12;
	my $allele_freq2 = ( $gt_freq22 * 2 ) + $gt_freq12;
	my $allele_total = $allele_freq1 + $allele_freq2;

	$line->{"allele1_freq"} = sprintf( "%.3f", $allele_freq1 / $allele_total );
	$line->{"allele2_freq"} = sprintf( "%.3f", $allele_freq2 / $allele_total );
	$line->{"alleles_total"} = $allele_total;
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: CalcAlleleFreqsFromGenotypeNumbers.pm 1515 2010-07-13 13:52:11Z rcf8 $

=cut

