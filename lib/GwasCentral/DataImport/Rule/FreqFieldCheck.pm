# $Id: FreqFieldCheck.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::FreqFieldCheck - PanelRule to determine whether required fields are present


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule('FreqFieldCheck');
	

=head1 DESCRIPTION

Informs the user of a template if required fields are missing (e.g. allele_freqs). Only once for the first row and then is switched off.
Runs once all other rules have run.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::FreqFieldCheck;

use strict;
use warnings;
use Data::Dumper qw(Dumper);

use Moose;
extends qw(GwasCentral::DataImport::PanelRule);
has 'is_done' => ('is'=>'rw');
no Moose;

sub run_multi_panel {
}

sub run_single_panel {
}

sub after_Rules {
	my ( $self, $element ) = @_;

	if ( $self->panelnames ) {
		$self->after_Rules_multi_panel($element);
	}
	else {
		$self->after_Rules_single_panel($element);
	}

}

sub after_Rules_multi_panel {
	my ( $self, $element ) = @_;
	return if $self->is_done;

	my $status = $element->status;
	my $marker = $element->marker;
	my $line   = $element->line;

	my $errors = 0;

	my @gt_order = @{ $self->settings->{gt_order} };
	my @al_order = @{ $self->settings->{al_order} };
	my @fields   = ();

	push @fields, "accession";

	foreach my $panel ( @{ $self->panelnames } ) {
		foreach my $pos (@gt_order) {
			push @fields, "genotype$pos" . "_number:$panel";
			push @fields, "genotype$pos" . "_freq:$panel";
			push @fields, "genotype$pos";
		}

		foreach my $pos (@al_order) {
			push @fields, "allele$pos" . "_freq:$panel";
			push @fields, "allele$pos";
		}
		push @fields, "genotype_total:$panel";
	}
	foreach my $field (@fields) {
		if ( !$line->{$field} && $line->{$field} ne '0' ) {
			$self->log->warn("$field was not present in the first line!");
			$errors++;
		}
	}
	$self->log_errors( $errors, $line );

}

sub after_Rules_single_panel {
	my ( $self, $element ) = @_;
	return if $self->is_done;

	my $status = $element->status;
	my $marker = $element->marker;
	my $line   = $element->line;

	my $errors = 0;

	my @gt_order = @{ $self->settings->{gt_order} };
	my @al_order = @{ $self->settings->{al_order} };
	my @fields   = ();

	foreach my $pos (@gt_order) {
		push @fields, "genotype$pos" . "_number";
		push @fields, "genotype$pos" . "_freq";
		push @fields, "genotype$pos";
	}

	foreach my $pos (@al_order) {
		push @fields, "allele$pos" . "_freq";
		push @fields, "allele$pos";
	}

	foreach my $field (@fields) {
		if ( !$line->{$field} ) {
			$self->log->warn("$field was not present in the first line!");
			$errors++;
		}
	}
	$self->log_errors( $errors, $line );
}

sub log_errors {
	my ( $self, $errors, $line ) = @_;
	if ( $errors > 0 ) {
		$self->log->logdie(
			"FreqFieldCheck found $errors errors with this parser template\n"
			  . Dumper($line) );
	}
	$self->log->info(
		"FreqFieldCheck found no errors with this parser template");
	$self->is_done(1);
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: FreqFieldCheck.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

