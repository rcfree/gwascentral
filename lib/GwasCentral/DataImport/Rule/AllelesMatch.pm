# $Id: AllelesMatch.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::AllelesMatch - Rule to determine whether alleles match between retrieved marker and DataElement


=head1 SYNOPSIS

	my $rule = $lmaker->import_rule('AllelesMatch');


=head1 DESCRIPTION

Compares the alleles in the DataElement 'marker' with the 'line' (so should only be run once alleles are in separate fields)
The rule assumes that any strand flips have been carried out beforehand.
If invalid sets the 'a' status to 1.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::DataImport::Rule::AllelesMatch;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::Rule);
no Moose;

use List::Compare;

use Data::Dumper qw(Dumper);

sub run {
	my ( $self, $element ) = @_;

	my $status = $element->status;
	my $marker = $element->marker;
	my $line   = $element->line;

	my @allele_seq_list = $marker->get_tag_values('alleles');
	my @input_alleles   = ( $line->{allele1}, $line->{allele2} );

	my $valid = $self->compare_allele_seq( \@input_alleles, \@allele_seq_list );

	if ( !$valid ) {
		$self->log->debug( "Alleles invalid. Input:"
			  . join( ",", @input_alleles ) . " DB:"
			  . join( ",", @allele_seq_list ) );
		$status->{a} = 1;
	}
	return $status;
}

=head2 compare_allele_seq

  Usage      : my $valid = $self->compare_allele_seq( \@input_alleles, \@allele_seq_list );
  Purpose    : Compare two arrayrefs containing alleles
  Returns    : 1 if sequences are valid, undef if not
  Arguments  : Allele arrayref 1;Allele arrayref 2
  Throws     :
  Status     :
  Comments   :

=cut

sub compare_allele_seq {
	my ( $self, $freq_allele_seq, $db_allele_seq ) = @_;
	my $allele_valid;

# Compare the Allele Sequence ie mainly Check if all the alleles are present and addition or deletion of Alleles
	my $lc           = List::Compare->new( $freq_allele_seq, $db_allele_seq );
	my $intersection = $lc->get_intersection;

	#print Dumper($freq_allele_seq);
	#print Dumper($db_allele_seq);
	#		print $self->log->info($intersection);
	#
	#		print $self->log->info($freq_allele_seq,$db_allele_seq);
	#
	#		exit;

	if ( $intersection == '2' ) {
		return 1;
	}
	else {
		return undef;
	}
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: AllelesMatch.pm 1515 2010-07-13 13:52:11Z rcf8 $

=cut

