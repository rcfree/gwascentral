# $Id: File.pm 1272 2009-08-21 15:08:35Z rcf8 $

=head1 NAME

HGVbaseG2P::DataImport::Rule::StrandFlip::File - Strand flipping depending on comparison between strand in marker and strand in template


=head1 SYNOPSIS

	my $rule = $lmaker->import_rule('StrandFlip::File');


=head1 DESCRIPTION

Flips the alleles if the 'marker' strand and template strand are not equal.
This may not work always, for example in some cases, extra template code would be required
to add a + or remove a + from the line strand (so that it is +1 or -1). It depends on the data source.

=head1 SUBROUTINES/METHODS

=cut

package HGVbaseG2P::DataImport::Rule::StrandFlip::File;


	use strict;
	use warnings;

	use Moose;
	extends qw(GwasCentral::DataImport::Rule::StrandFlip);
	use List::Compare;

	use Data::Dumper qw(Dumper);

	sub run {
		my ($self, $element) = @_;

		my $marker = $element->marker;
		$self->log->info("marker strand:".$element->marker->strand." and file strand:".$self->settings->{'strand'});
		
		if ($element->marker->strand ne $self->settings->{'strand'}) {
			$self->strand_flip($element->line);
		}
	}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 SVNINFO

$Id: File.pm 1272 2009-08-21 15:08:35Z rcf8 $

=cut