# $Id: SplitField.pm 1515 2010-07-13 13:52:11Z rcf8 $

=head1 NAME

GwasCentral::DataImport::SplitField - PanelRule to split field based on separator


=head1 SYNOPSIS
	
	my $rule = $lmaker->import_rule(
				'SplitField',
				{
					order     => [@gt_order],
					separator => $template->{'gt_freq_separator'},
					type      => 'genotype',
					unit      => 'freq'
				}
			  );
	

=head1 DESCRIPTION

Splits fields based on a separator and deals with multiple panels.
The order specifies the order in which the split values are stored.
The separator specifies the character to separate by
The type should be allele or genotype
The unit should be freq or number

N.B. The latter two are concatenated together to form the correct field names.

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::DataImport::Rule::SplitField;

use strict;
use warnings;

use Moose;
extends qw(GwasCentral::DataImport::PanelRule);
no Moose;
use Data::Dumper qw(Dumper);

sub run_multi_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $separator = $self->settings->{'separator'};
	my $type      = $self->settings->{'type'};
	my $unit      = $self->settings->{'unit'};
	my @order     = @{ $self->settings->{'order'} };

	foreach my $panel ( @{ $self->panelnames } ) {
		my $freqs_field = $type . "_$unit" . "s:$panel";
		my @freqs       = split( $separator, $line->{$freqs_field} );
		my $counter     = 0;
		foreach my $order ( @{ $self->settings->{order} } ) {
			my $freq_field = "$type$order" . "_$unit:$panel";
			$line->{$freq_field} = $freqs[$counter];
			$counter++;
		}
	}
}

sub run_single_panel {
	my ( $self, $element ) = @_;
	my $line = $element->line;

	my $separator = $self->settings->{'separator'};
	my $type      = $self->settings->{'type'};
	my $unit      = $self->settings->{'unit'};
	my @order     = @{ $self->settings->{'order'} };

	my $field = $type . ( $unit ? "_$unit" : "" ) . "s";
	my @freqs = split( $separator, $line->{$field} );
	my $counter = 0;
	foreach my $order (@order) {
		my $freq_field = "$type$order" . ( $unit ? "_$unit" : "" );
		$line->{$freq_field} = $freqs[$counter];
		$counter++;
	}
}
1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 SVNINFO

$Id: SplitField.pm 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

