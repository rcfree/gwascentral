package GwasCentral::Schema::Session::Instances;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Session::Instances

=cut

__PACKAGE__->table("instances");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 label

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 description

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 url

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 consumer_key

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 consumer_secret

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 is_active

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "description",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "url",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "consumer_key",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "consumer_secret",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "is_active",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 1,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("label", ["label"]);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:15:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:l0lyHOUmPUoZF5xr+ZAl6A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
