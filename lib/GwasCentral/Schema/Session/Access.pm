package GwasCentral::Schema::Session::Access;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Session::Access

=cut

__PACKAGE__->table("access");

=head1 ACCESSORS

=head2 access_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 identity

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 resource

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 accesslevel

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 date

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 response

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=cut

__PACKAGE__->add_columns(
  "access_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "identity",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "resource",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "accesslevel",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "date",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "response",
  { data_type => "varchar", is_nullable => 0, size => 10 },
);
__PACKAGE__->set_primary_key("access_id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:15:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:miw2xt7woF3uQBV/uNCZaA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
