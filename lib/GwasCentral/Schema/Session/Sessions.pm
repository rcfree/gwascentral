package GwasCentral::Schema::Session::Sessions;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Session::Sessions

=cut

__PACKAGE__->table("sessions");

=head1 ACCESSORS

=head2 id

  data_type: 'char'
  is_nullable: 0
  size: 72

=head2 session_data

  data_type: 'longtext'
  is_nullable: 1

=head2 expires

  data_type: 'integer'
  is_nullable: 1

=head2 time_stamp

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "char", is_nullable => 0, size => 72 },
  "session_data",
  { data_type => "longtext", is_nullable => 1 },
  "expires",
  { data_type => "integer", is_nullable => 1 },
  "time_stamp",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 1,
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:15:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NNlqX7YXyeNBhWTmH19fzQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
