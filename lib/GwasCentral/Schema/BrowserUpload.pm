package GwasCentral::Schema::BrowserUpload;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:eE2eAWUacrxOhjWo3GwEjw

#provides DBI compatibility from DataSource::Browser
sub prepare {
	my ($self,@attrs) = @_;
	my $dbh = $self->storage->dbh;
	return $dbh->prepare(@attrs);
}

sub do {
	my ($self, @attrs) = @_;
	my $dbh = $self->storage->dbh;
	return $dbh->do(@attrs);
}


# You can replace this text with custom content, and it will be preserved on regeneration
1;
