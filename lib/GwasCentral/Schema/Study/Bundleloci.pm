package GwasCentral::Schema::Study::Bundleloci;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Bundleloci

=cut

__PACKAGE__->table("BundleLoci");

=head1 ACCESSORS

=head2 bundlelociid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 bundlename

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 loci

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "bundlelociid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "bundlename",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "loci",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("bundlelociid");
__PACKAGE__->add_unique_constraint("BundleName", ["bundlename"]);

=head1 RELATIONS

=head2 genotypedbundles

Type: has_many

Related object: L<GwasCentral::Schema::Study::Genotypedbundle>

=cut

__PACKAGE__->has_many(
  "genotypedbundles",
  "GwasCentral::Schema::Study::Genotypedbundle",
  { "foreign.bundlelociid" => "self.bundlelociid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:U0c5vig2/vJZZdTnWQvPEw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
