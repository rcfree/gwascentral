package GwasCentral::Schema::Study::ResultsetcrossrefOld;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::ResultsetcrossrefOld

=cut

__PACKAGE__->table("ResultsetCrossref_old");

=head1 ACCESSORS

=head2 resultsetcrossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 resultsetid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "resultsetcrossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "resultsetid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("resultsetcrossrefid");
__PACKAGE__->add_unique_constraint("ResultsetID", ["resultsetid", "crossrefid"]);

=head1 RELATIONS

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::CrossrefOld>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Study::CrossrefOld",
  { crossrefid => "crossrefid" },
);

=head2 resultsetid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Resultset>

=cut

__PACKAGE__->belongs_to(
  "resultsetid",
  "GwasCentral::Schema::Study::Resultset",
  { resultsetid => "resultsetid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5QYvPkgOw2xjb4SOC1zySw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
