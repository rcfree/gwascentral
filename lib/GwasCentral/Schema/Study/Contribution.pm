package GwasCentral::Schema::Study::Contribution;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Contribution

=cut

__PACKAGE__->table("Contribution");

=head1 ACCESSORS

=head2 contributionid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 researcherid

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 submissionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 issubmitter

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 0

=head2 isauthor

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 0

=head2 issource

  data_type: 'enum'
  extra: {list => ["yes","no"]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "contributionid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "researcherid",
  {
    data_type => "smallint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "submissionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "issubmitter",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 0,
  },
  "isauthor",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 0,
  },
  "issource",
  {
    data_type => "enum",
    extra => { list => ["yes", "no"] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("contributionid");

=head1 RELATIONS

=head2 researcherid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Researcher>

=cut

__PACKAGE__->belongs_to(
  "researcherid",
  "GwasCentral::Schema::Study::Researcher",
  { researcherid => "researcherid" },
);

=head2 submissionid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Submission>

=cut

__PACKAGE__->belongs_to(
  "submissionid",
  "GwasCentral::Schema::Study::Submission",
  { submissionid => "submissionid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:I9G4HYtueeoIn3ifzqZRaA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
