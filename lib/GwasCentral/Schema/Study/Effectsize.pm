package GwasCentral::Schema::Study::Effectsize;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Effectsize

=cut

__PACKAGE__->table("EffectSize");

=head1 ACCESSORS

=head2 effectsizeid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 usedmarkersetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 riskallele

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 riskallelefreq

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 riskallelecomment

  data_type: 'text'
  is_nullable: 1

=head2 type

  data_type: 'enum'
  extra: {list => ["OR","BC","RR","DP"]}
  is_nullable: 0

=head2 value

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 lower95bound

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 upper95bound

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 stderror

  data_type: 'double precision'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "effectsizeid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "usedmarkersetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "riskallele",
  { data_type => "char", is_nullable => 1, size => 1 },
  "riskallelefreq",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "riskallelecomment",
  { data_type => "text", is_nullable => 1 },
  "type",
  {
    data_type => "enum",
    extra => { list => ["OR", "BC", "RR", "DP"] },
    is_nullable => 0,
  },
  "value",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "lower95bound",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "upper95bound",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "stderror",
  {
    data_type => "double precision",
    extra => { unsigned => 1 },
    is_nullable => 1,
  },
  "description",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("effectsizeid");
__PACKAGE__->add_unique_constraint("UsedmarkersetID", ["usedmarkersetid"]);

=head1 RELATIONS

=head2 usedmarkersetid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Usedmarkerset>

=cut

__PACKAGE__->belongs_to(
  "usedmarkersetid",
  "GwasCentral::Schema::Study::Usedmarkerset",
  { usedmarkersetid => "usedmarkersetid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-08-01 11:51:02
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NIDoLoxKNmOy87riaEG7/g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
