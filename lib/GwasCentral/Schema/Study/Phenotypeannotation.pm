package GwasCentral::Schema::Study::Phenotypeannotation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypeannotation

=cut

__PACKAGE__->table("PhenotypeAnnotation");

=head1 ACCESSORS

=head2 phenotypeannotationid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 annotationorigin

  data_type: 'enum'
  extra: {list => ["mesh","hpo","unknown"]}
  is_nullable: 1

=head2 phenotypeidentifier

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 exactmatch

  data_type: 'enum'
  default_value: 1
  extra: {list => [1,0]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "phenotypeannotationid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "annotationorigin",
  {
    data_type => "enum",
    extra => { list => ["mesh", "hpo", "unknown"] },
    is_nullable => 1,
  },
  "phenotypeidentifier",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "exactmatch",
  {
    data_type => "enum",
    default_value => 1,
    extra => { list => [1, 0] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("phenotypeannotationid");

=head1 RELATIONS

=head2 pppas

Type: has_many

Related object: L<GwasCentral::Schema::Study::Pppa>

=cut

__PACKAGE__->has_many(
  "pppas",
  "GwasCentral::Schema::Study::Pppa",
  { "foreign.phenotypeannotationid" => "self.phenotypeannotationid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pKO03ihKLN+fHFu7hTgqAQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
