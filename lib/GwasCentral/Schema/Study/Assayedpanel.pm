package GwasCentral::Schema::Study::Assayedpanel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Assayedpanel

=cut

__PACKAGE__->table("Assayedpanel");

=head1 ACCESSORS

=head2 assayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 totalnumberofindividuals

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexmale

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexfemale

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexunknown

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofprobands

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofparents

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "assayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "totalnumberofindividuals",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexmale",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexfemale",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexunknown",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofprobands",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofparents",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("assayedpanelid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);

=head2 experimentassayedpanels

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimentassayedpanel>

=cut

__PACKAGE__->has_many(
  "experimentassayedpanels",
  "GwasCentral::Schema::Study::Experimentassayedpanel",
  { "foreign.assayedpanelid" => "self.assayedpanelid" },
  {},
);

=head2 frequencyclusters

Type: has_many

Related object: L<GwasCentral::Schema::Study::Frequencycluster>

=cut

__PACKAGE__->has_many(
  "frequencyclusters",
  "GwasCentral::Schema::Study::Frequencycluster",
  { "foreign.assayedpanelid" => "self.assayedpanelid" },
  {},
);

=head2 selectioncriteria_assayedpanelids

Type: has_many

Related object: L<GwasCentral::Schema::Study::Selectioncriteria>

=cut

__PACKAGE__->has_many(
  "selectioncriteria_assayedpanelids",
  "GwasCentral::Schema::Study::Selectioncriteria",
  { "foreign.assayedpanelid" => "self.assayedpanelid" },
  {},
);

=head2 selectioncriteria_sourceassayedpanelids

Type: has_many

Related object: L<GwasCentral::Schema::Study::Selectioncriteria>

=cut

__PACKAGE__->has_many(
  "selectioncriteria_sourceassayedpanelids",
  "GwasCentral::Schema::Study::Selectioncriteria",
  { "foreign.sourceassayedpanelid" => "self.assayedpanelid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hYYygwjSgDnMfoMLLcMDdA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
