package GwasCentral::Schema::Study::Hotlink;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Hotlink

=cut

__PACKAGE__->table("Hotlink");

=head1 ACCESSORS

=head2 hotlinkid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 hotlinklabel

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 urlprefix

  data_type: 'varchar'
  is_nullable: 0
  size: 300

=head2 urlsuffix

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=cut

__PACKAGE__->add_columns(
  "hotlinkid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "hotlinklabel",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "urlprefix",
  { data_type => "varchar", is_nullable => 0, size => 300 },
  "urlsuffix",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
);
__PACKAGE__->set_primary_key("hotlinkid");

=head1 RELATIONS

=head2 experimenthotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimenthotlink>

=cut

__PACKAGE__->has_many(
  "experimenthotlinks",
  "GwasCentral::Schema::Study::Experimenthotlink",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);

=head2 phenotypemethodhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypemethodhotlink>

=cut

__PACKAGE__->has_many(
  "phenotypemethodhotlinks",
  "GwasCentral::Schema::Study::Phenotypemethodhotlink",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);

=head2 resultsethotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Resultsethotlink>

=cut

__PACKAGE__->has_many(
  "resultsethotlinks",
  "GwasCentral::Schema::Study::Resultsethotlink",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);

=head2 samplepanelhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Samplepanelhotlink>

=cut

__PACKAGE__->has_many(
  "samplepanelhotlinks",
  "GwasCentral::Schema::Study::Samplepanelhotlink",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);

=head2 studyhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studyhotlink>

=cut

__PACKAGE__->has_many(
  "studyhotlinks",
  "GwasCentral::Schema::Study::Studyhotlink",
  { "foreign.hotlinkid" => "self.hotlinkid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UzsTCZXwHl8r+GUi4oLjng


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
