package GwasCentral::Schema::Study::Phenotypemethodcitation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypemethodcitation

=cut

__PACKAGE__->table("PhenotypeMethodCitation");

=head1 ACCESSORS

=head2 phenotypemethodcitationid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypemethodid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 citationid

  data_type: 'smallint'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "phenotypemethodcitationid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypemethodid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "citationid",
  {
    data_type => "smallint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("phenotypemethodcitationid");
__PACKAGE__->add_unique_constraint("PhenotypeMethodID", ["phenotypemethodid", "citationid"]);

=head1 RELATIONS

=head2 phenotypemethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->belongs_to(
  "phenotypemethodid",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { phenotypemethodid => "phenotypemethodid" },
);

=head2 citationid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Citation>

=cut

__PACKAGE__->belongs_to(
  "citationid",
  "GwasCentral::Schema::Study::Citation",
  { citationid => "citationid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AAVGgvwWe6YJDZJkbKdIDQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
