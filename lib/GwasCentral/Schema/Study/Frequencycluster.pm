package GwasCentral::Schema::Study::Frequencycluster;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Frequencycluster

=cut

__PACKAGE__->table("FrequencyCluster");

=head1 ACCESSORS

=head2 frequencyclusterid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 usedmarkersetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 assayedpanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 numberofgenotypedsamples

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 pvaluehwe

  data_type: 'decimal'
  extra: {unsigned => 1}
  is_nullable: 1
  size: [6,5]

=cut

__PACKAGE__->add_columns(
  "frequencyclusterid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "usedmarkersetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "markerid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
  "experimentid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "assayedpanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "numberofgenotypedsamples",
  { data_type => "smallint", extra => { unsigned => 1 }, is_nullable => 0 },
  "pvaluehwe",
  {
    data_type => "decimal",
    extra => { unsigned => 1 },
    is_nullable => 1,
    size => [6, 5],
  },
);
__PACKAGE__->set_primary_key("frequencyclusterid");

=head1 RELATIONS

=head2 allelefrequencies

Type: has_many

Related object: L<GwasCentral::Schema::Study::Allelefrequency>

=cut

__PACKAGE__->has_many(
  "allelefrequencies",
  "GwasCentral::Schema::Study::Allelefrequency",
  { "foreign.frequencyclusterid" => "self.frequencyclusterid" },
  {},
);

=head2 fcs

Type: has_many

Related object: L<GwasCentral::Schema::Study::Fcs>

=cut

__PACKAGE__->has_many(
  "fcs",
  "GwasCentral::Schema::Study::Fcs",
  { "foreign.frequencyclusterid" => "self.frequencyclusterid" },
  {},
);

=head2 assayedpanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Assayedpanel>

=cut

__PACKAGE__->belongs_to(
  "assayedpanelid",
  "GwasCentral::Schema::Study::Assayedpanel",
  { assayedpanelid => "assayedpanelid" },
);

=head2 usedmarkersetid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Usedmarkerset>

=cut

__PACKAGE__->belongs_to(
  "usedmarkersetid",
  "GwasCentral::Schema::Study::Usedmarkerset",
  { usedmarkersetid => "usedmarkersetid" },
);

=head2 genotypefrequencies

Type: has_many

Related object: L<GwasCentral::Schema::Study::Genotypefrequency>

=cut

__PACKAGE__->has_many(
  "genotypefrequencies",
  "GwasCentral::Schema::Study::Genotypefrequency",
  { "foreign.frequencyclusterid" => "self.frequencyclusterid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6NptPZGobK96PQZRNw73BQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
