package GwasCentral::Schema::Study::Phenotypevalue;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypevalue

=cut

__PACKAGE__->table("PhenotypeValue");

=head1 ACCESSORS

=head2 phenotypevalueid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypemethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 samplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 500

=head2 valuerank

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 valueismean

  data_type: 'varchar'
  is_nullable: 1
  size: 3

=head2 stddev

  data_type: 'float'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 median

  data_type: 'float'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 min

  data_type: 'float'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 max

  data_type: 'float'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofindividuals

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 qualifier

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "phenotypevalueid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypemethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "samplepanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 500 },
  "valuerank",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "valueismean",
  { data_type => "varchar", is_nullable => 1, size => 3 },
  "stddev",
  { data_type => "float", extra => { unsigned => 1 }, is_nullable => 1 },
  "median",
  { data_type => "float", extra => { unsigned => 1 }, is_nullable => 1 },
  "min",
  { data_type => "float", extra => { unsigned => 1 }, is_nullable => 1 },
  "max",
  { data_type => "float", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofindividuals",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "qualifier",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("phenotypevalueid");
__PACKAGE__->add_unique_constraint(
  "CombinedMethodID",
  ["phenotypemethodid", "samplepanelid", "value"],
);

=head1 RELATIONS

=head2 phenotypemethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->belongs_to(
  "phenotypemethodid",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { phenotypemethodid => "phenotypemethodid" },
);

=head2 samplepanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:eWZY+QYPspNLVIlDa0aJ5A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
