package GwasCentral::Schema::Study::Hotlinker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Hotlinker

=cut

__PACKAGE__->table("Hotlinker");

=head1 ACCESSORS

=head2 hotlinkerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 hotlinkcollectionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 hotlinkid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "hotlinkerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "hotlinkcollectionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "hotlinkid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("hotlinkerid");

=head1 RELATIONS

=head2 hotlinkcollectionid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Hotlinkcollection>

=cut

__PACKAGE__->belongs_to(
  "hotlinkcollectionid",
  "GwasCentral::Schema::Study::Hotlinkcollection",
  { hotlinkcollectionid => "hotlinkcollectionid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UVrnMTMXfFkXvswj94FoCw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
