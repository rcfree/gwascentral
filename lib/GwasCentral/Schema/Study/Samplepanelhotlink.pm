package GwasCentral::Schema::Study::Samplepanelhotlink;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Samplepanelhotlink

=cut

__PACKAGE__->table("SamplepanelHotlink");

=head1 ACCESSORS

=head2 samplepanelhotlinkid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 samplepanelid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 hotlinkid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "samplepanelhotlinkid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "samplepanelid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "hotlinkid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("samplepanelhotlinkid");
__PACKAGE__->add_unique_constraint("SamplepanelID", ["samplepanelid", "hotlinkid"]);

=head1 RELATIONS

=head2 samplepanelid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid" },
);

=head2 hotlinkid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Hotlink>

=cut

__PACKAGE__->belongs_to(
  "hotlinkid",
  "GwasCentral::Schema::Study::Hotlink",
  { hotlinkid => "hotlinkid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UpNJ9Z988ni9ZV3p42re+g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
