package GwasCentral::Schema::Study::Experiment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Experiment

=cut

__PACKAGE__->table("Experiment");

=head1 ACCESSORS

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 phenotypemethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 design

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 experimenttype

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 totalmarkerstested

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 totalmarkersimported

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 objective

  data_type: 'text'
  is_nullable: 1

=head2 outcome

  data_type: 'text'
  is_nullable: 1

=head2 comments

  data_type: 'text'
  is_nullable: 1

=head2 individualdatastatement

  data_type: 'text'
  is_nullable: 1

=head2 frequencysummary

  data_type: 'text'
  is_nullable: 1

=head2 significancesummary

  data_type: 'text'
  is_nullable: 1

=head2 timecreated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0

=head2 genotypedbundle

  data_type: 'text'
  is_nullable: 1

=head2 accesslevel

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 importconfig

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "phenotypemethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "design",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "experimenttype",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "totalmarkerstested",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "totalmarkersimported",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "objective",
  { data_type => "text", is_nullable => 1 },
  "outcome",
  { data_type => "text", is_nullable => 1 },
  "comments",
  { data_type => "text", is_nullable => 1 },
  "individualdatastatement",
  { data_type => "text", is_nullable => 1 },
  "frequencysummary",
  { data_type => "text", is_nullable => 1 },
  "significancesummary",
  { data_type => "text", is_nullable => 1 },
  "timecreated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
  },
  "genotypedbundle",
  { data_type => "text", is_nullable => 1 },
  "accesslevel",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "importconfig",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("experimentid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 phenotypemethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypemethod>

=cut

__PACKAGE__->belongs_to(
  "phenotypemethodid",
  "GwasCentral::Schema::Study::Phenotypemethod",
  { phenotypemethodid => "phenotypemethodid" },
);

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);

=head2 experimentassayedpanels

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimentassayedpanel>

=cut

__PACKAGE__->has_many(
  "experimentassayedpanels",
  "GwasCentral::Schema::Study::Experimentassayedpanel",
  { "foreign.experimentid" => "self.experimentid" },
  {},
);

=head2 experimenthotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimenthotlink>

=cut

__PACKAGE__->has_many(
  "experimenthotlinks",
  "GwasCentral::Schema::Study::Experimenthotlink",
  { "foreign.experimentid" => "self.experimentid" },
  {},
);

=head2 experimentplatforms

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimentplatform>

=cut

__PACKAGE__->has_many(
  "experimentplatforms",
  "GwasCentral::Schema::Study::Experimentplatform",
  { "foreign.experimentid" => "self.experimentid" },
  {},
);

=head2 resultsets

Type: has_many

Related object: L<GwasCentral::Schema::Study::Resultset>

=cut

__PACKAGE__->has_many(
  "resultsets",
  "GwasCentral::Schema::Study::Resultset",
  { "foreign.experimentid" => "self.experimentid" },
  {},
);

=head2 usedmarkersets

Type: has_many

Related object: L<GwasCentral::Schema::Study::Usedmarkerset>

=cut

__PACKAGE__->has_many(
  "usedmarkersets",
  "GwasCentral::Schema::Study::Usedmarkerset",
  { "foreign.experimentid" => "self.experimentid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EfKkqruVjNM88KNa7KMqFg

__PACKAGE__->many_to_many(usedassayedpanels => 'experimentassayedpanels', 'assayedpanelid' );
__PACKAGE__->many_to_many(hotlinks => 'experimenthotlinks', 'hotlinkid' );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
