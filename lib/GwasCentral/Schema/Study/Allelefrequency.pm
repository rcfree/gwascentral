package GwasCentral::Schema::Study::Allelefrequency;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Allelefrequency

=cut

__PACKAGE__->table("AlleleFrequency");

=head1 ACCESSORS

=head2 allelefrequencyid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 frequencyclusterid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 allelecombo

  data_type: 'varchar'
  is_nullable: 1
  size: 767

=head2 frequencyasproportion

  data_type: 'decimal'
  extra: {unsigned => 1}
  is_nullable: 0
  size: [4,3]

=cut

__PACKAGE__->add_columns(
  "allelefrequencyid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "frequencyclusterid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "allelecombo",
  { data_type => "varchar", is_nullable => 1, size => 767 },
  "frequencyasproportion",
  {
    data_type => "decimal",
    extra => { unsigned => 1 },
    is_nullable => 0,
    size => [4, 3],
  },
);
__PACKAGE__->set_primary_key("allelefrequencyid");

=head1 RELATIONS

=head2 frequencyclusterid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Frequencycluster>

=cut

__PACKAGE__->belongs_to(
  "frequencyclusterid",
  "GwasCentral::Schema::Study::Frequencycluster",
  { frequencyclusterid => "frequencyclusterid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:9XEA565D0TedtFzMotlQbg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
