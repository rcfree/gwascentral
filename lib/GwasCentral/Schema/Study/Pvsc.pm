package GwasCentral::Schema::Study::Pvsc;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Pvsc

=cut

__PACKAGE__->table("PVSC");

=head1 ACCESSORS

=head2 pvscid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypevalueid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 selectioncriteriaid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "pvscid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypevalueid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "selectioncriteriaid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("pvscid");
__PACKAGE__->add_unique_constraint(
  "PhenotypeValueID",
  ["phenotypevalueid", "selectioncriteriaid"],
);

=head1 RELATIONS

=head2 phenotypevalueid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypevalue>

=cut

__PACKAGE__->belongs_to(
  "phenotypevalueid",
  "GwasCentral::Schema::Study::Phenotypevalue",
  { phenotypevalueid => "phenotypevalueid" },
);

=head2 selectioncriteriaid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Selectioncriteria>

=cut

__PACKAGE__->belongs_to(
  "selectioncriteriaid",
  "GwasCentral::Schema::Study::Selectioncriteria",
  { selectioncriteriaid => "selectioncriteriaid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hX9NjRBCU5uDaqSvNkJIPw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
