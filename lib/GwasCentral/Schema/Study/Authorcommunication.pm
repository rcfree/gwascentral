package GwasCentral::Schema::Study::Authorcommunication;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Authorcommunication

=cut

__PACKAGE__->table("AuthorCommunication");

=head1 ACCESSORS

=head2 authorcommunicationid

  data_type: 'mediumint'
  is_auto_increment: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 contactid

  data_type: 'mediumint'
  is_nullable: 0

=head2 contactname

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 datecontacted

  data_type: 'date'
  is_nullable: 1

=head2 dateresponse

  data_type: 'date'
  is_nullable: 1

=head2 typeofresponse

  data_type: 'enum'
  extra: {list => ["Public response and released additional data","Private response and released additional data","Public response","Private response","E-mail bounce"]}
  is_nullable: 1

=head2 responsetext

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "authorcommunicationid",
  { data_type => "mediumint", is_auto_increment => 1, is_nullable => 0 },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "contactid",
  { data_type => "mediumint", is_nullable => 0 },
  "contactname",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "datecontacted",
  { data_type => "date", is_nullable => 1 },
  "dateresponse",
  { data_type => "date", is_nullable => 1 },
  "typeofresponse",
  {
    data_type => "enum",
    extra => {
          list => [
                "Public response and released additional data",
                "Private response and released additional data",
                "Public response",
                "Private response",
                "E-mail bounce",
              ],
        },
    is_nullable => 1,
  },
  "responsetext",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("authorcommunicationid");

=head1 RELATIONS

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-10-11 10:32:10
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5PeTSbnLh8UznWEpgPFrxQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
