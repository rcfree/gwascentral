package GwasCentral::Schema::Study::Samplepaneloverlap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Samplepaneloverlap

=cut

__PACKAGE__->table("SamplepanelOverlap");

=head1 ACCESSORS

=head2 samplepaneloverlapid

  data_type: 'smallint'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 samplepanelid_new

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 samplepanelid_old

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 overlapdetails

  data_type: 'varchar'
  default_value: 'This samplepanel overlaps with another'
  is_nullable: 1
  size: 250

=cut

__PACKAGE__->add_columns(
  "samplepaneloverlapid",
  {
    data_type => "smallint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "samplepanelid_new",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "samplepanelid_old",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "overlapdetails",
  {
    data_type => "varchar",
    default_value => "This samplepanel overlaps with another",
    is_nullable => 1,
    size => 250,
  },
);
__PACKAGE__->set_primary_key("samplepaneloverlapid");

=head1 RELATIONS

=head2 samplepanelid_new

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid_new",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid_new" },
);

=head2 samplepanelid_old

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Samplepanel>

=cut

__PACKAGE__->belongs_to(
  "samplepanelid_old",
  "GwasCentral::Schema::Study::Samplepanel",
  { samplepanelid => "samplepanelid_old" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:n1WlxSFV2qGeBH/f1c8YVg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
