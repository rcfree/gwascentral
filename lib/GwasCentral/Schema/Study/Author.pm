package GwasCentral::Schema::Study::Author;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Author

=cut

__PACKAGE__->table("Author");

=head1 ACCESSORS

=head2 authorid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 researcherid

  data_type: 'smallint'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 studyid

  data_type: 'mediumint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "authorid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "researcherid",
  {
    data_type => "smallint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "studyid",
  {
    data_type => "mediumint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("authorid");
__PACKAGE__->add_unique_constraint("ResearcherID", ["researcherid", "studyid"]);

=head1 RELATIONS

=head2 researcherid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Researcher>

=cut

__PACKAGE__->belongs_to(
  "researcherid",
  "GwasCentral::Schema::Study::Researcher",
  { researcherid => "researcherid" },
);

=head2 studyid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Study>

=cut

__PACKAGE__->belongs_to(
  "studyid",
  "GwasCentral::Schema::Study::Study",
  { studyid => "studyid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OdD31IFEt7u/R5iZHbUk3Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
