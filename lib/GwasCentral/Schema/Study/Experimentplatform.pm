package GwasCentral::Schema::Study::Experimentplatform;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Experimentplatform

=cut

__PACKAGE__->table("ExperimentPlatform");

=head1 ACCESSORS

=head2 experimentplatformid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 platformid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "experimentplatformid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "platformid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("experimentplatformid");
__PACKAGE__->add_unique_constraint("BundleLociID_2", ["platformid", "experimentid"]);

=head1 RELATIONS

=head2 platformid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Platform>

=cut

__PACKAGE__->belongs_to(
  "platformid",
  "GwasCentral::Schema::Study::Platform",
  { platformid => "platformid" },
);

=head2 experimentid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->belongs_to(
  "experimentid",
  "GwasCentral::Schema::Study::Experiment",
  { experimentid => "experimentid" },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iGgl92icWRPNROk0d9kR1Q


# You can replace this text with custom content, and it will be preserved on regeneration
1;
