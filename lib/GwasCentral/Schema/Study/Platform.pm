package GwasCentral::Schema::Study::Platform;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Platform

=cut

__PACKAGE__->table("Platform");

=head1 ACCESSORS

=head2 platformid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 platformname

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 loci

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "platformid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "platformname",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "loci",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("platformid");
__PACKAGE__->add_unique_constraint("BundleName", ["platformname"]);

=head1 RELATIONS

=head2 experimentplatforms

Type: has_many

Related object: L<GwasCentral::Schema::Study::Experimentplatform>

=cut

__PACKAGE__->has_many(
  "experimentplatforms",
  "GwasCentral::Schema::Study::Experimentplatform",
  { "foreign.platformid" => "self.platformid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:HaVt/SqF9b+cTOUygHWfCg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
