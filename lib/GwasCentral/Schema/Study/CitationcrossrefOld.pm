package GwasCentral::Schema::Study::CitationcrossrefOld;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::CitationcrossrefOld

=cut

__PACKAGE__->table("CitationCrossref_old");

=head1 ACCESSORS

=head2 citationcrossrefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 citationid

  data_type: 'smallint'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "citationcrossrefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "citationid",
  {
    data_type => "smallint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("citationcrossrefid");
__PACKAGE__->add_unique_constraint("CitationID", ["citationid", "crossrefid"]);

=head1 RELATIONS

=head2 citationid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Citation>

=cut

__PACKAGE__->belongs_to(
  "citationid",
  "GwasCentral::Schema::Study::Citation",
  { citationid => "citationid" },
);

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::CrossrefOld>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Study::CrossrefOld",
  { crossrefid => "crossrefid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vV6e424AscwzrmgA/TpPsw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
