package GwasCentral::Schema::Study::Hotlinkcollection;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Hotlinkcollection

=cut

__PACKAGE__->table("HotlinkCollection");

=head1 ACCESSORS

=head2 hotlinkcollectionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 studyid

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "hotlinkcollectionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "studyid",
  { data_type => "integer", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("hotlinkcollectionid");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-08-01 11:51:02
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:w4YMvr0jilRxBF3bg/e5Tw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
