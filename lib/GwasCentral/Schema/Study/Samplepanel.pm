package GwasCentral::Schema::Study::Samplepanel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Samplepanel

=cut

__PACKAGE__->table("Samplepanel");

=head1 ACCESSORS

=head2 samplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 submittedinstudyidentifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 parentsamplepanelid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 composition

  data_type: 'text'
  is_nullable: 1

=head2 totalnumberofindividuals

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexmale

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexfemale

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofsexunknown

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofprobands

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 numberofparents

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 modeofrecruitment

  data_type: 'text'
  is_nullable: 1

=head2 diagnosisagerange

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 diagnosisperiod

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 samplingagerange

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 samplingperiod

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 populationinfo

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 geographicregioninfo

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 ethnicityinfo

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 birthplaceinfo

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 admixtureinfo

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 environmentinfo

  data_type: 'text'
  is_nullable: 1

=head2 sourceofdna

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 dnasarepooled

  data_type: 'enum'
  default_value: 'Undefined'
  extra: {list => ["Undefined","Pre-prep","Post-prep","No"]}
  is_nullable: 0

=head2 dnasarewga

  data_type: 'enum'
  default_value: 'Undefined'
  extra: {list => ["Undefined","None","All","Some"]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "samplepanelid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "submittedinstudyidentifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "parentsamplepanelid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "composition",
  { data_type => "text", is_nullable => 1 },
  "totalnumberofindividuals",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexmale",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexfemale",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofsexunknown",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofprobands",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "numberofparents",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "modeofrecruitment",
  { data_type => "text", is_nullable => 1 },
  "diagnosisagerange",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "diagnosisperiod",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "samplingagerange",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "samplingperiod",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "populationinfo",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "geographicregioninfo",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "ethnicityinfo",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "birthplaceinfo",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "admixtureinfo",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "environmentinfo",
  { data_type => "text", is_nullable => 1 },
  "sourceofdna",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "dnasarepooled",
  {
    data_type => "enum",
    default_value => "Undefined",
    extra => { list => ["Undefined", "Pre-prep", "Post-prep", "No"] },
    is_nullable => 0,
  },
  "dnasarewga",
  {
    data_type => "enum",
    default_value => "Undefined",
    extra => { list => ["Undefined", "None", "All", "Some"] },
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("samplepanelid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);
__PACKAGE__->add_unique_constraint("Name", ["name", "submittedinstudyidentifier"]);

=head1 RELATIONS

=head2 phenotypevalues

Type: has_many

Related object: L<GwasCentral::Schema::Study::Phenotypevalue>

=cut

__PACKAGE__->has_many(
  "phenotypevalues",
  "GwasCentral::Schema::Study::Phenotypevalue",
  { "foreign.samplepanelid" => "self.samplepanelid" },
  {},
);

=head2 samplepanelhotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Samplepanelhotlink>

=cut

__PACKAGE__->has_many(
  "samplepanelhotlinks",
  "GwasCentral::Schema::Study::Samplepanelhotlink",
  { "foreign.samplepanelid" => "self.samplepanelid" },
  {},
);

=head2 selectioncriterias

Type: has_many

Related object: L<GwasCentral::Schema::Study::Selectioncriteria>

=cut

__PACKAGE__->has_many(
  "selectioncriterias",
  "GwasCentral::Schema::Study::Selectioncriteria",
  { "foreign.samplepanelid" => "self.samplepanelid" },
  {},
);

=head2 studysamplepanels

Type: has_many

Related object: L<GwasCentral::Schema::Study::Studysamplepanel>

=cut

__PACKAGE__->has_many(
  "studysamplepanels",
  "GwasCentral::Schema::Study::Studysamplepanel",
  { "foreign.samplepanelid" => "self.samplepanelid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SsiqcfjXQmpfgXiOWHxcYw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
