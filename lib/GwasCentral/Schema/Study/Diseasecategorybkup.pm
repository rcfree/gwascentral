package GwasCentral::Schema::Study::Diseasecategorybkup;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Diseasecategorybkup

=cut

__PACKAGE__->table("DiseaseCategoryBkup");

=head1 ACCESSORS

=head2 diseasecategoryid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 parentcategoryid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 meshtreeid

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 description

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "diseasecategoryid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "parentcategoryid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "meshtreeid",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "description",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-26 02:12:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kmG5EyK6mHCNcIqJfJT8Vg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
