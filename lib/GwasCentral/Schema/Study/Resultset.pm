package GwasCentral::Schema::Study::Resultset;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Resultset

=cut

__PACKAGE__->table("Resultset");

=head1 ACCESSORS

=head2 resultsetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 experimentid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 analysismethodid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 identifier

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 accession

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 accessionversion

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 protocolparameters

  data_type: 'text'
  is_nullable: 1

=head2 timecreated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "resultsetid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "experimentid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "analysismethodid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "identifier",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "accession",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "accessionversion",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "protocolparameters",
  { data_type => "text", is_nullable => 1 },
  "timecreated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
  },
);
__PACKAGE__->set_primary_key("resultsetid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);
__PACKAGE__->add_unique_constraint("Name", ["name", "experimentid"]);

=head1 RELATIONS

=head2 analysismethodid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Analysismethod>

=cut

__PACKAGE__->belongs_to(
  "analysismethodid",
  "GwasCentral::Schema::Study::Analysismethod",
  { analysismethodid => "analysismethodid" },
);

=head2 experimentid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Experiment>

=cut

__PACKAGE__->belongs_to(
  "experimentid",
  "GwasCentral::Schema::Study::Experiment",
  { experimentid => "experimentid" },
);

=head2 resultsethotlinks

Type: has_many

Related object: L<GwasCentral::Schema::Study::Resultsethotlink>

=cut

__PACKAGE__->has_many(
  "resultsethotlinks",
  "GwasCentral::Schema::Study::Resultsethotlink",
  { "foreign.resultsetid" => "self.resultsetid" },
  {},
);

=head2 significances

Type: has_many

Related object: L<GwasCentral::Schema::Study::Significance>

=cut

__PACKAGE__->has_many(
  "significances",
  "GwasCentral::Schema::Study::Significance",
  { "foreign.resultsetid" => "self.resultsetid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-07-28 17:11:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7zAx81L2Z1DaWBhEoJaFCA

*experiment = \&experimentid;
*analysismethod = \&analysismethodid;
__PACKAGE__->many_to_many(hotlinks => 'resultsethotlinks', 'hotlinkid' );

sub unique_name {
	my ($self) = @_;
	my $studyname = $self->experimentid->studyid->name;
	my $resultsetname = $self->name;
	#$studyname     =~ s/\:|\,|\(|\)//g;
	#$resultsetname =~ s/\:|\,|\(|\)//g;
	return $studyname . " - " . $resultsetname . " (" . $self->identifier . ")";
}

sub to_hash {
	my ($self) = @_;
	my %resultset = $self->get_columns;
	my %exp = $self->experimentid->get_columns;
	my $study = {$self->experimentid->studyid->get_columns};
	$exp{studyid}=$study;	
	$resultset{experimentid}=\%exp;
	
	my $pmethod = $self->experimentid->phenotypemethodid;
	$resultset{experimentid}->{phenotypemethodid}={$pmethod->get_columns};
	$resultset{experimentid}->{phenotypemethodid}->{phenotypepropertyid}={$pmethod->phenotypepropertyid->get_columns};
	$resultset{server}=$self->server;
	$resultset{accesslevel}=$self->accesslevel;
	return \%resultset;
}

sub accesslevel {
	my ($self,$value)=@_;
	
	if (defined($value)) {
		$self->{_accesslevel}=$value;
	}
	return $self->{_accesslevel};
}

sub study_name {
	my ($self) = @_;
	$self->experimentid->studyid->name;
}

sub study_ident {
	my ($self) = @_;
	$self->experimentid->studyid->identifier;
}

sub phenotype_ident {
	my ($self) = @_;
	$self->experimentid->phenotypemethodid->identifier;
}

sub phenotype_name {
	my ($self) = @_;
	$self->experimentid->phenotypemethodid->phenotypepropertyid->name;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
