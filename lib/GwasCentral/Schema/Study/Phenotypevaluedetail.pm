package GwasCentral::Schema::Study::Phenotypevaluedetail;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Study::Phenotypevaluedetail

=cut

__PACKAGE__->table("PhenotypeValueDetail");

=head1 ACCESSORS

=head2 phenotypevaluedetailid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 phenotypevalueid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 attribute

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 phenotypevaluedetail

  data_type: 'varchar'
  is_nullable: 0
  size: 150

=head2 unitofmeasurement

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 qualifier

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "phenotypevaluedetailid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "phenotypevalueid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "attribute",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "phenotypevaluedetail",
  { data_type => "varchar", is_nullable => 0, size => 150 },
  "unitofmeasurement",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "qualifier",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("phenotypevaluedetailid");
__PACKAGE__->add_unique_constraint("Attribute", ["attribute", "phenotypevalueid"]);

=head1 RELATIONS

=head2 phenotypevalueid

Type: belongs_to

Related object: L<GwasCentral::Schema::Study::Phenotypevalue>

=cut

__PACKAGE__->belongs_to(
  "phenotypevalueid",
  "GwasCentral::Schema::Study::Phenotypevalue",
  { phenotypevalueid => "phenotypevalueid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 13:17:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TwxRQT5VmiAe7SbST+8kxg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
