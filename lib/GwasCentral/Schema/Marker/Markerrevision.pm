package GwasCentral::Schema::Marker::Markerrevision;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Markerrevision

=cut

__PACKAGE__->table("MarkerRevision");

=head1 ACCESSORS

=head2 markerrevisionid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 replacedbymarkerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 strandflipped

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 changetype

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 changetrigger

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 timecreated

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 0

=head2 comment

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=cut

__PACKAGE__->add_columns(
  "markerrevisionid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "replacedbymarkerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "strandflipped",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "changetype",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "changetrigger",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "timecreated",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 0,
  },
  "comment",
  { data_type => "varchar", is_nullable => 1, size => 250 },
);
__PACKAGE__->set_primary_key("markerrevisionid");
__PACKAGE__->add_unique_constraint("MarkerID", ["markerid", "replacedbymarkerid"]);

=head1 RELATIONS

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);

=head2 replacedbymarkerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "replacedbymarkerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "replacedbymarkerid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:HhJ11oTZ91qTi8RdY0hJZA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
