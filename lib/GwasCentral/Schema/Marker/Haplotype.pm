package GwasCentral::Schema::Marker::Haplotype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Haplotype

=cut

__PACKAGE__->table("Haplotype");

=head1 ACCESSORS

=head2 haplotypeid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=head2 identifier

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 14

=head2 markersetid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 localid

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "haplotypeid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
  "identifier",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 14 },
  "markersetid",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "localid",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);
__PACKAGE__->set_primary_key("haplotypeid");
__PACKAGE__->add_unique_constraint("Identifier", ["identifier"]);

=head1 RELATIONS

=head2 haplotypealleles

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Haplotypeallele>

=cut

__PACKAGE__->has_many(
  "haplotypealleles",
  "GwasCentral::Schema::Marker::Haplotypeallele",
  { "foreign.haplotypeid" => "self.haplotypeid" },
  {},
);

=head2 haplotypecrossrefs

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Haplotypecrossref>

=cut

__PACKAGE__->has_many(
  "haplotypecrossrefs",
  "GwasCentral::Schema::Marker::Haplotypecrossref",
  { "foreign.haplotypeid" => "self.haplotypeid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gJqty6oDSbDzKvH56Mry9g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
