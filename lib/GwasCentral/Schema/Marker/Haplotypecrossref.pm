package GwasCentral::Schema::Marker::Haplotypecrossref;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Haplotypecrossref

=cut

__PACKAGE__->table("HaplotypeCrossref");

=head1 ACCESSORS

=head2 haplotypecrossrefid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 haplotypeid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 crossrefid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "haplotypecrossrefid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "haplotypeid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "crossrefid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("haplotypecrossrefid");
__PACKAGE__->add_unique_constraint("HaplotypeID", ["haplotypeid", "crossrefid"]);

=head1 RELATIONS

=head2 haplotypeid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Haplotype>

=cut

__PACKAGE__->belongs_to(
  "haplotypeid",
  "GwasCentral::Schema::Marker::Haplotype",
  { haplotypeid => "haplotypeid" },
);

=head2 crossrefid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Crossref>

=cut

__PACKAGE__->belongs_to(
  "crossrefid",
  "GwasCentral::Schema::Marker::Crossref",
  { crossrefid => "crossrefid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:3bT/suSz5Nd6ZNi0o6x9NQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
