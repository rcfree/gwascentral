package GwasCentral::Schema::Marker::Markeralias;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Markeralias

=cut

__PACKAGE__->table("MarkerAlias");

=head1 ACCESSORS

=head2 markeraliasid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 alias

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 isprimary

  data_type: 'tinyint'
  extra: {unsigned => 1}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "markeraliasid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "alias",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "isprimary",
  { data_type => "tinyint", extra => { unsigned => 1 }, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("markeraliasid");
__PACKAGE__->add_unique_constraint("MarkerID", ["markerid", "isprimary"]);

=head1 RELATIONS

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Oa+lgvM6Dfgv4Qn+YI0YQA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
