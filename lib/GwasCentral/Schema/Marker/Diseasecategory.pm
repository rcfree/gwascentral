package GwasCentral::Schema::Marker::Diseasecategory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Diseasecategory

=cut

__PACKAGE__->table("DiseaseCategory");

=head1 ACCESSORS

=head2 diseasecategoryid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 parentcategoryid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 meshtreeid

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 description

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "diseasecategoryid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "parentcategoryid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "meshtreeid",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "description",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("diseasecategoryid");
__PACKAGE__->add_unique_constraint("Name", ["name"]);

=head1 RELATIONS

=head2 parentcategoryid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Diseasecategory>

=cut

__PACKAGE__->belongs_to(
  "parentcategoryid",
  "GwasCentral::Schema::Marker::Diseasecategory",
  { diseasecategoryid => "parentcategoryid" },
);

=head2 diseasecategories

Type: has_many

Related object: L<GwasCentral::Schema::Marker::Diseasecategory>

=cut

__PACKAGE__->has_many(
  "diseasecategories",
  "GwasCentral::Schema::Marker::Diseasecategory",
  { "foreign.parentcategoryid" => "self.diseasecategoryid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1E77O6kMU1wxwmzaCv78UA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
