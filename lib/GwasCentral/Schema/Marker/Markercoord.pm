package GwasCentral::Schema::Marker::Markercoord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Markercoord

=cut

__PACKAGE__->table("MarkerCoord");

=head1 ACCESSORS

=head2 markercoordid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 markerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 chr

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 start

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 stop

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 span

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 strand

  data_type: 'smallint'
  default_value: 0
  is_nullable: 0

=head2 mapweight

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 genomebuild

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 5

=head2 assemblytype

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 assemblyname

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 mappedgene

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=cut

__PACKAGE__->add_columns(
  "markercoordid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "markerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "chr",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "start",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "stop",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "span",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "strand",
  { data_type => "smallint", default_value => 0, is_nullable => 0 },
  "mapweight",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "genomebuild",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 5 },
  "assemblytype",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "assemblyname",
  { data_type => "varchar", is_nullable => 0, size => 30 },
#  "mappedgene",
#  { data_type => "varchar", is_nullable => 1, size => 15 },
);
__PACKAGE__->set_primary_key("markercoordid");

=head1 RELATIONS

=head2 markerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Marker>

=cut

__PACKAGE__->belongs_to(
  "markerid",
  "GwasCentral::Schema::Marker::Marker",
  { markerid => "markerid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:c0Yf19Qq1y1JLO6Gde0vjw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
