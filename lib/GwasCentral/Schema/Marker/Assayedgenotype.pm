package GwasCentral::Schema::Marker::Assayedgenotype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Assayedgenotype

=cut

__PACKAGE__->table("AssayedGenotype");

=head1 ACCESSORS

=head2 assaymarkerid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 genotypeid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "assaymarkerid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "genotypeid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("assaymarkerid", "genotypeid");

=head1 RELATIONS

=head2 assaymarkerid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Assaymarker>

=cut

__PACKAGE__->belongs_to(
  "assaymarkerid",
  "GwasCentral::Schema::Marker::Assaymarker",
  { assaymarkerid => "assaymarkerid" },
);

=head2 genotypeid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Genotype>

=cut

__PACKAGE__->belongs_to(
  "genotypeid",
  "GwasCentral::Schema::Marker::Genotype",
  { genotypeid => "genotypeid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-07-04 15:06:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:l5P8w2QlmBNLyhi7CnuSEw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
