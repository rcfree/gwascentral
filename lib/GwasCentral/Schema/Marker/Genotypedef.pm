package GwasCentral::Schema::Marker::Genotypedef;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Marker::Genotypedef

=cut

__PACKAGE__->table("Genotypedef");

=head1 ACCESSORS

=head2 genotypedefid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 genotypeid

  data_type: 'integer'
  default_value: 0
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 alleleid

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

=head2 type

  data_type: 'enum'
  extra: {list => ["Observed","Ratio","Count","Signal"]}
  is_nullable: 0

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=cut

__PACKAGE__->add_columns(
  "genotypedefid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "genotypeid",
  {
    data_type => "integer",
    default_value => 0,
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "alleleid",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "type",
  {
    data_type => "enum",
    extra => { list => ["Observed", "Ratio", "Count", "Signal"] },
    is_nullable => 0,
  },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 50 },
);
__PACKAGE__->set_primary_key("genotypedefid");

=head1 RELATIONS

=head2 alleleid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Allele>

=cut

__PACKAGE__->belongs_to(
  "alleleid",
  "GwasCentral::Schema::Marker::Allele",
  { alleleid => "alleleid" },
);

=head2 genotypeid

Type: belongs_to

Related object: L<GwasCentral::Schema::Marker::Genotype>

=cut

__PACKAGE__->belongs_to(
  "genotypeid",
  "GwasCentral::Schema::Marker::Genotype",
  { genotypeid => "genotypeid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-06 14:15:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ENQMr3+r7BBKpnfMhatz2A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
