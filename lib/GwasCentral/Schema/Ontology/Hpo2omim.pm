package GwasCentral::Schema::Ontology::Hpo2omim;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::Hpo2omim

=cut

__PACKAGE__->table("hpo2omim");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 omimid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 omimname

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 evidencecode

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "omimid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "omimname",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "evidencecode",
  { data_type => "varchar", is_nullable => 0, size => 10 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-10-11 11:23:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ew3SssHP3Bt0TXwRciLbRA

 __PACKAGE__->belongs_to(hpo_term => 'GwasCentral::Schema::Ontology::Hpo_term', {'hpoid' => 'hpoid'} );
 __PACKAGE__->belongs_to(mesh2omim => 'GwasCentral::Schema::Ontology::Mesh2omim', {'omimid' => 'omimid'} );

# You can replace this text with custom content, and it will be preserved on regeneration
1;
