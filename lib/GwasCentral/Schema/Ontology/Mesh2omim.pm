package GwasCentral::Schema::Ontology::Mesh2omim;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::Mesh2omim

=cut

__PACKAGE__->table("mesh2omim");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 meshid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 omimid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "meshid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "omimid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-10-11 11:23:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fW1kuS9+0opccY0Mn0E0KA

 __PACKAGE__->belongs_to(mesh_heading => 'GwasCentral::Schema::Ontology::Mesh_heading', {'meshid' => 'meshid'} );
 __PACKAGE__->belongs_to(hpo2omim => 'GwasCentral::Schema::Ontology::Hpo2omim', {'omimid' => 'omimid'} );

# You can replace this text with custom content, and it will be preserved on regeneration
1;
