package GwasCentral::Schema::Ontology::Hpo2mesh;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::Hpo2mesh

=cut

__PACKAGE__->table("hpo2mesh");

=head1 ACCESSORS

=head2 hpo2meshid

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 meshid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 mappingtype

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "hpo2meshid",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "meshid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "mappingtype",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2011-06-07 09:40:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:d9ppQHOfh/0G5WWJ0OEnNA

__PACKAGE__->belongs_to(hpo_term => 'GwasCentral::Schema::Ontology::HpoTerm', {'hpoid' => 'hpoid'} );
 __PACKAGE__->belongs_to(mesh_heading => 'GwasCentral::Schema::Ontology::MeshHeading', {'meshid' => 'meshid'} );
 
# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
