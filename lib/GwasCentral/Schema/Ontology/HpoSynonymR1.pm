package GwasCentral::Schema::Ontology::HpoSynonymR1;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

GwasCentral::Schema::Ontology::HpoSynonymR1

=cut

__PACKAGE__->table("hpo_synonym_R1");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 hpoid

  data_type: 'varchar'
  is_nullable: 0
  size: 15

=head2 synonymtext

  data_type: 'varchar'
  is_nullable: 0
  size: 300

=head2 synonymtype

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 synonymref

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 hgvbaseuse

  data_type: 'enum'
  default_value: 0
  extra: {list => [0,1]}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "hpoid",
  { data_type => "varchar", is_nullable => 0, size => 15 },
  "synonymtext",
  { data_type => "varchar", is_nullable => 0, size => 300 },
  "synonymtype",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "synonymref",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "hgvbaseuse",
  {
    data_type => "enum",
    default_value => 0,
    extra => { list => [0, 1] },
    is_nullable => 0,
  },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-10-11 11:23:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:aeumC8FxS3+AZEq8pU01Dg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
