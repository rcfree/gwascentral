package GwasCentral::DataSource::Study;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(
GwasCentral::DataSource::Base 
GwasCentral::DataSource::Study::StudyList
GwasCentral::DataSource::Study::PhenotypeList);
with qw(
GwasCentral::DataSource::Nature::DBIC
GwasCentral::DataSource::Nature::Controlled
);

has '+source_name' => ( 'default' => 'Study' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

sub get_study_by_identifier {
	my ($self, $ident)=@_;
	#$self->log->debug("get study by identifier $ident");
	
	$self->throw("Study identifier not supplied","DataSource") if !$ident;

	my $study = $self->dbh->resultset("Study")->search({ Identifier => $ident })->first;
	$study and $self->populate_study_access_levels($study);
	return $study;
}

sub get_spanel_by_identifier {
	my ($self, $ident)=@_;
	#$self->log->debug("get study by identifier $ident");
	
	$self->throw("Sample identifier not supplied","DataSource") if !$ident;

	my $spanel= $self->dbh->resultset("Samplepanel")->search({ Identifier => $ident })->first;
	return $spanel;
}

sub get_studies_by_identifiers {
	my ($self, $idents)=@_;
	#$self->log->debug("get studies by identifiers ".join(";",@{$idents || []}));
	
	$self->throw("Study identifiers not supplied","DataSource") if !$idents;

	my @studies = $self->dbh->resultset("Study")->search({ Identifier => {-in => $idents} })->all;
	@studies and $self->populate_study_access_levels(\@studies);
	return @studies;
}

sub get_pmethod_by_identifier {
	my ($self, $ident)=@_;
	$self->log->debug("get study by identifier $ident");
	
	$self->throw("Pmethod identifier not supplied","DataSource") if !$ident;

	my $pmethod = $self->dbh->resultset("Phenotypemethod")->search({ Identifier => $ident })->first;
	$self->throw("Phenotype $ident was not found","DataSource") if !$pmethod;
	$self->populate_pmethod_access_levels($pmethod);
	return $pmethod;
}

sub count_phenotypeannotations_by_phenotypeidentifier {
	my ($self, $identifier) = @_;
	return $self->dbh->resultset('Phenotypeannotation')->search({ phenotypeidentifier => $identifier })->count;
}

sub get_resultset_by_identifier {
	my ($self, $ident)=@_;
	$self->log->info("get resultset by identifier $ident");
	
	$self->throw("Resultset identifier not supplied","DataSource") if !$ident;
	
	#my ($url, $ident) = $self->get_url_and_ident_from_connect_id($connectid,"resultset");
	
	my $study;

	$study = $self->dbh->resultset("Resultset")->search({ Identifier => $ident })->first;
	$self->throw("Resultset $ident was not found","DataSource") if !$study;
	$self->populate_rset_access_levels($study);
	return $study;
}
	
sub get_all_unhidden_studies {
	my ($self) = @_;
#	my @studies = inner(@_);
#	
#	#populate access levels from AccessControl module for each study
#	$self->_populate_study_access_levels(\@studies);	
#	
	my $params = {ishidden => 'no' };
	#$self->portal and $params->{'portal'}=$self->portal;
	return $self->dbh->resultset('Study')->search($params)->all;
}

sub get_all_phenotypemethods {
	my ($self)=@_;
	my @pmethods = inner(@_);
	my $params = {'studyid.ishidden' => 'no' };
	#$self->portal and $params->{'studyid.portal'}=$self->portal;
	return $self->dbh->resultset("Phenotypemethod")->search($params,{join=>'studyid'})->all();
}

sub populate_study_access_levels {
	my ($self, $studies_ref) = @_;
	
	#only bother getting access levels if is_controlled
	if (ref($studies_ref) ne 'ARRAY') { 
		$studies_ref=[$studies_ref];
	}
	my %levels = $self->AccessControl->get_study_and_experiment_access_levels($studies_ref);

	foreach my $study(@{$studies_ref}) {
			my $study_ident = $study->identifier;
			$study->accesslevel($levels{$study_ident}->{study});
			$study->response_status($levels{$study_ident}->{response});
			$study->child_accesslevel($levels{$study_ident}->{all_experiments});

	}
}

sub populate_rset_access_levels {
	my ($self, $rsets_ref) = @_;
	
	#only bother getting access levels if is_controlled
	if (ref($rsets_ref) ne 'ARRAY') { 
		$rsets_ref=[$rsets_ref];
	}
	

	foreach my $rset(@{$rsets_ref}) {
		my $level = $self->AccessControl->get_resultset_access_levels($rset);
		my $rset_ident = $rset->identifier;
		$rset->accesslevel($level);
	}
}

sub populate_pmethod_access_levels {
	my ($self, $pmethods_ref) = @_;
	
	#only bother getting access levels if is_controlled
	if (ref($pmethods_ref) ne 'ARRAY') { $pmethods_ref=[$pmethods_ref]; }
	my %levels = $self->AccessControl->get_phenotype_and_experiment_access_levels($pmethods_ref);
	
	foreach my $pmethod(@{$pmethods_ref}) {
		my $study_ident = $pmethod->studyid->identifier;
		$pmethod->accesslevel($levels{$study_ident}->{study});
		$pmethod->response_status($levels{$study_ident}->{response});
		$pmethod->child_accesslevel($levels{$study_ident}->{all_experiments});
	}
}

sub get_marker_sigs_gt_threshold {
		my ($self, $identifier, $threshold) = @_;
		my $pvalue_threshold = 10**( -int($threshold) );
		return $self->dbh->resultset('Significance')->search({
			'unadjustedpvalue'=>{'<=',$pvalue_threshold},
			'usedmarkersetid.markeridentifier'=>$identifier,
			'studyid.ishidden' => 'no',
		},{ join => {'usedmarkersetid'=>{'experimentid'=>'studyid'} }, prefetch => {'resultsetid'=>{'experimentid'=>['studyid',{'phenotypemethodid'=>'phenotypepropertyid'}]}}})->all;
	}

sub get_apanel_by_name_and_study {
		my ($self, $name, $study_ident)=@_;
		my $apanels = $self->dbh->resultset("Assayedpanel")->search({ 'me.Name' => $name, 'studyid.identifier' => $study_ident },{join=>'studyid'})->first;

		return $apanels;
	}

sub get_mesh_phenotypeannotations_by_phenotypemethod {
		my ($self, $identifier) = @_;
		return $self->dbh->resultset('Phenotypemethod')->search({ identifier => $identifier, annotationorigin=> 'mesh' },
		{join=>{'phenotypepropertyid'=>{'pppas'=>'phenotypeannotationid'}},
		'+select' => ['phenotypeannotationid.phenotypeidentifier'],
		'+as' => ['pid'],
		group_by => 'phenotypeidentifier',
		});
	}
	
	sub get_hpo_phenotypeannotations_by_phenotypemethod {
		my ($self, $identifier) = @_;
		return $self->dbh->resultset('Phenotypemethod')->search({ identifier => $identifier, annotationorigin=> 'hpo' },
		{join=>{'phenotypepropertyid'=>{'pppas'=>'phenotypeannotationid'}},
		'+select' => ['phenotypeannotationid.phenotypeidentifier'],
		'+as' => ['pid'],
		group_by => 'phenotypeidentifier',
		});
	}
1;
