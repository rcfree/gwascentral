package GwasCentral::DataSource::Xapian;
use Moose;
extends 'GwasCentral::DataSource::Base';
	use English qw( -no_match_vars );
	use Data::Dumper qw(Dumper);
	use FindBin;
	use Carp qw(croak cluck);

	use Encode qw/from_to/;

	has 'db_of_dbdir' => (is=>'rw', default=>sub { {} });
	has 'qp' => (is=>'rw');
	
	use Search::Xapian qw/:standard :enq_order/;
	use Search::Xapian::MSet::Tied;

	sub db {
		my ( $self, $dbdir ) = @_;
		my %db_of_dbdir = %{ $self->db_of_dbdir };

		# Open db if we don't have it already opened
		if ( !exists $db_of_dbdir{$dbdir} ) {
			$self->log->info("Opening database in $dbdir");
			eval {
				$db_of_dbdir{$dbdir} = Search::Xapian::Database->new($dbdir);
			};
			if ($@) {
				$self->throw("Error opening database in $dbdir: $@");
			}
		}
		return $db_of_dbdir{$dbdir};
	}
	
	sub _prepare_Enquiry {
		my ($self, $dbname, $q) = @_;
		
		#prepare query and enquiry for specified database and terms
		
		my $config = $self->config->{'Search'};
		my $dbdir =
		    $self->config->{'basedir'} . '/'
		  . $config->{index_basedir} . '/'
		  . $dbname;
		my $db = $self->db($dbdir);
		$db->reopen();
		
		my $qp = Search::Xapian::QueryParser->new($db);
		
		my $stemmer = Search::Xapian::Stem->new( $config->{language} );
		$qp->set_stemmer($stemmer);
		$qp->set_default_op(OP_AND);

		$qp->add_boolean_prefix( "site",  "H" );
		$qp->add_boolean_prefix( "year",  "Y" );
		$qp->add_boolean_prefix( "month", "M" );
		$qp->add_boolean_prefix( "date",  "D" );
		$qp->add_boolean_prefix( "id",    "Q" );
		$qp->add_prefix( "title", "T" );

		from_to( $q, 'utf-8', 'iso-8859-1' ) if $config->{utf8_query};
		
		$qp->set_stemming_strategy(STEM_ALL);
		$self->qp( $qp );
		my $enq;
		eval {
			my $query = $qp->parse_query( $q,
				FLAG_WILDCARD | FLAG_BOOLEAN | FLAG_LOVEHATE |
				  FLAG_BOOLEAN_ANY_CASE );
			$enq = $self->db($dbdir)->enquire($query);
		};
		if ($@) {
			return undef;
		}
		return $enq;
	}
	
	sub get_terms {
		my ($self, $dbname) = @_;
		my $config = $self->config->{'Search'};
		my $dbdir =
		    $self->config->{'basedir'} . '/'
		  . $config->{index_basedir} . '/'
		  . $dbname;
		my $db = $self->db($dbdir);
		
	}
	
	sub search_all {
		my ( $self, $qterms, $dbname) = @_;
		
		my $q = $qterms;
		my $config = $self->config->{'Search'};

		my $enq = $self->_prepare_Enquiry($dbname, $q);
		if (!$enq) {
			return ( 0, [] );
		}
		
		$self->log->info(
			sprintf "Parsed Xapian query is '%s'\n",
			$enq->get_query()->get_description()
		);

#		if ( $config->{order_by_date} ) {
#			$enq->set_docid_order(ENQ_DESCENDING);
#			$enq->set_weighting_scheme( Search::Xapian::BoolWeight->new() );
#		}
		my @matches_tied;
		
		my $mset = $enq->get_mset( 0, 1000 );
		tie( @matches_tied, 'Search::Xapian::MSet::Tied', $mset );
		return undef if @matches_tied == 0;

		my $total = scalar(@matches_tied);

		my @match_data = map { $self->extract_data( $_->get_document ) } @matches_tied;

		return ( $total, \@match_data );
	}
	
	sub search_page {
		my ( $self, $qterms, $dbname, $page, $page_size, $ids, $listfilt, $order_by ) = @_;
		my %id_hash = $ids ? map { $_=>1} @{$ids} : ();
		
		my $q = $qterms;

		my $config = $self->config->{'Search'};
		$page      ||= 1;
		$page_size ||= $config->{page_size};
		
		my $enq = $self->_prepare_Enquiry($dbname, $q);
		if (!$enq) {
			return ( 0, [] );
		}
		$self->log->debug(
			sprintf "Parsed Xapian query is '%s'\n",
			$enq->get_query()->get_description()
		);

		if ( $config->{order_by_date} ) {
			$enq->set_docid_order(ENQ_DESCENDING);
			$enq->set_weighting_scheme( Search::Xapian::BoolWeight->new() );
		}
		my @matches_tied;
		
		my $mset = $enq->get_mset( ( $page - 1 ) * $page_size, 1000 );
		tie( @matches_tied, 'Search::Xapian::MSet::Tied', $mset );
		return undef if @matches_tied == 0;

		my $start = ( $page - 1 ) * $page_size;
		my $end = $start + $page_size;
		if ($listfilt->{added}) {
			$end = $start + $page_size + scalar(@{$ids});
		}
		
		my $total = scalar(@matches_tied);
		$end > $total and $end = $total;
		
		my @matches=();

		my $not_added=0;
		foreach my $match (@matches_tied) {
			my $match =  $self->extract_data( $match->get_document );
			my $add=1;
			if (!$listfilt->{added} and $listfilt->{canad}) {
				if ($id_hash{$match}) {
					$add=0;
					$not_added++;
				}
			}
			elsif ($listfilt->{added} and !$listfilt->{canad}) {
				if (!$id_hash{$match}) {
					$add=0;
					$not_added++;
				}				
			}
			if ($add) {
				push @matches,$match;
				last if $page_size ne 'all' and scalar(@matches)>$page_size
			}
		}
		
		my $pager = Data::Page->new;
		$pager->total_entries($mset->get_matches_estimated - $not_added);
		
		$pager->entries_per_page($page_size);
		$pager->current_page($page);
		return ( $pager, \@matches );
	}

=item extract_data <item> <query>

    Extract data from a L<Search::Xapian::Document>. Defaults to
    using Storable::thaw.

=cut

	sub extract_data {
		my ( $self, $item ) = @_;
		my $data = $item->get_data;
		return $data;
	}

1;
