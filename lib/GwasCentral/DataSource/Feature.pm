package GwasCentral::DataSource::Feature;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(GwasCentral::DataSource::Base GwasCentral::DataSource::Feature::GeneList);
with qw(
GwasCentral::DataSource::Nature::SeqFeature

);

has '+source_name' => ( 'default' => 'Feature' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

 sub search_attributes {
	    my ($self, $qstring, $tags, $page, $page_size) = @_;
	
	    my @results = $self->dbh->search_attributes($qstring,$tags);

	    @results == 0 and return undef;
	    warn "total_entries=",scalar(@results), ", entries_per_page=$page_size, current_page=$page";
#	    my $pager = Data::Page->new;
#		$pager->total_entries(scalar(@results));
#		$pager->entries_per_page($page_size);
#		$pager->current_page($page);
#	   
	    # Get a slice of the results-list corresponding to pager parameters
	    my $i = $page_size * ($page-1);
	    $self->log->info("getting slice [",$i," .. ",$i+$page_size,"] from results-array");
	    #@results = @results[$i .. $i+$page_size-1];
	    my @pri_ids = map { $_->[4] } @results;
		my @features = $self->dbh->fetch_many(@pri_ids);
		$self->log->info("features:".Dumper(\@features));
	    return ( \@features );
	}
	
	sub search_id {
	    my ($self, $qstring, $page,$page_size) = @_;
	
	    my @features = $self->dbh->get_features_by_alias($qstring);
	   
	    #remove non standard chr (e.g. chr_6_hap)
	    my @results = grep { length($_->ref)<=5 } @features;
	   
	    return @features;
	}
	
	sub search_gene {
	    my ($self, $qstring, $page,$page_size) = @_;
	$self->log->info("search_gene:$qstring");
	    my @features = $self->dbh->features(
	    	-name => $qstring,
	    	-type => 'mRNA:UCSC'
	    );
	   
	    #remove non standard chr (e.g. chr_6_hap)
	    my @results = grep { length($_->ref)<=5 } @features;
	    $self->log->info(scalar(@results)." genes found");
	    return @features;
	}
	
	sub search_region {
	    my ($self, $qstring) = @_;
		$self->log->info("search_region:$qstring");
	    my @features = $self->dbh->get_features_by_alias($qstring);
	   
	    #remove non standard chr (e.g. chr_6_hap)
	    my @results = grep { length($_->ref)<=5 } @features;
	    $self->log->info(scalar(@results)." feature regions found");
	    return @features;
	}
	
	sub get_biggest_feature {
		my ($self, $feature_args) = @_;
	    my @features = $self->dbh->features(
	    	%{$feature_args}
	    );
		return undef if @features == 0;	

		my @feature_sizes;
		foreach my $feature (@features) {
			my $feature_size = $feature->end - $feature->start;
			push @feature_sizes,
			  { size => $feature_size, feat => $feature };
		}
		my @features_sorted_by_size =
			reverse sort { $a->{size} <=> $b->{size} } @feature_sizes;
		my $biggest_feature = $features_sorted_by_size[0]->{feat};
		return $biggest_feature;
	}
	
	sub get_mapped_gene {
		my ($self, $ref, $start, $stop) = @_;
		
		my $feat = $self->get_biggest_feature({
			-ref => $ref,
			-start => $start,
			-stop => $stop,
			-range_type => 'overlaps',
			-type => 'mRNA'
		});
		#$self->log->info("feat:".Dumper($feat));
		
		return $feat ? $feat->display_name : undef;
	}
	
	sub import_gff_file {
		my ($self, $gff_filename) = @_;
		my $loader = Bio::DB::SeqFeature::Store::GFF3Loader->new(-store    => $self->dbh,
		                                                         -sf_class => 'Bio::DB::SeqFeature',
		                                                         -tmpdir   => File::Spec->tmpdir(),
		                                                         -fast     => 0)
		  or die "Couldn't create GFF3 loader";
		
		# on signals, give objects a chance to call their DESTROY methods
		$SIG{TERM} = $SIG{INT} = sub {  undef $loader;  die "Aborted..."; };
		$loader->load($gff_filename);	
	}
1;
