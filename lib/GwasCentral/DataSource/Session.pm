package GwasCentral::DataSource::Session;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(GwasCentral::DataSource::Base);
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'Session' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

1;
