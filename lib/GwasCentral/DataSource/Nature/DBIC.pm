package GwasCentral::DataSource::Nature::DBIC;
use FindBin;
use lib "$FindBin::Bin/../../";
use Moose::Role;
use DBIx::Class;

has 'dbh' => ('is'=>'rw');
has 'dsn' => ('is'=>'rw');
has 'user' => ('is'=>'rw');
has 'pass' => ('is'=>'rw');
has 'options' => ('is'=>'rw');
has 'source_name' => ('is'=>'ro');


sub init {
	my($self, $args) = @_;
	
	#look for source name (e.g. study, marker)
	if (!$self->source_name) {
		$self->throw("No 'source_name' specified in class ".ref($self));
	}
	my $options =  $self->options_from_config(ucfirst($self->source_name));
	
	$self->log->info("Connecting to ".ucfirst($self->source_name)." database ".$options->{dsn}." as user ".$options->{user});

	
	$self->dsn($options->{dsn});
	$self->log->info("version of DBIx::Class = ".$DBIx::Class::VERSION);
	my $module = "GwasCentral::Schema::".ucfirst($self->source_name);
	eval "require $module;";
	
	my $dbh = $module->connect($options->{dsn},
                               $options->{user},
                              	$options->{pass},
                               $options->{options}
                               ) or $self->throw("Could not initialize db-connection. DBI::errstr=".$DBI::errstr);
    $self->dbh($dbh);
}

sub prepare {
	my ($self,@attrs) = @_;
	my $dbh = $self->dbh->storage->dbh;
	return $dbh->prepare(@attrs);
}

sub do {
	my ($self, @attrs) = @_;
	my $dbh = $self->dbh->storage->dbh;
	return $dbh->do(@attrs);
}

1;