package GwasCentral::DataSource::Nature::SeqFeature;
use FindBin;
use lib "$FindBin::Bin/../../";
use Moose::Role;
use Bio::DB::SeqFeature::Store;

has 'dbh' => ('is'=>'rw');
has 'dbi_dbh' => ('is'=>'rw');
has 'dsn' => ('is'=>'rw');
has 'user' => ('is'=>'rw');
has 'pass' => ('is'=>'rw');
has 'options' => ('is'=>'rw');
has 'source_name' => ('is'=>'ro');

sub init {
	my($self, $args) = @_;
	
	#look for source name (e.g. study, marker)
	if (!$self->source_name) {
		$self->throw("No 'source_name' specified in class ".ref($self));
	}
	my ($options) =  $self->options_from_config();
	
	$self->log->info("Connecting to ".ucfirst($self->source_name)." database ".$options->{dsn}." as user ".$options->{user});

	
	 my $dbh = Bio::DB::SeqFeature::Store->new(-adaptor => 'DBI::mysql',
						      -dsn     => $options->{dsn},
						      -user    => $options->{user},
						      -pass    => $options->{pass}) or die $@;
    $self->dbi_dbh($dbh->dbh);
    
    $self->dbh($dbh);
}

1;