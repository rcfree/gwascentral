package GwasCentral::DataSource::Nature::DBI;
use FindBin;
use lib "$FindBin::Bin/../../";
use Moose::Role;

has 'dbh' => ('is'=>'rw');

has 'source_name' => ('is'=>'ro');


sub init {
	my($self, $args) = @_;
	
	#look for source name (e.g. study, marker)
	if (!$self->source_name) {
		$self->throw("No 'source_name' specified in class ".ref($self));
	}
	my $options =  $self->options_from_config(ucfirst($self->source_name));
	
	$self->log->info("Connecting to ".ucfirst($self->source_name)." database ".$options->{dsn}." as user ".$options->{user});

	my $dbh = DBI->connect($options->{dsn}, $options->{user},$options->{pass},$options->{options}) or $self->throw("Can't connect to:".$self->source_name." database");
    $self->dbh($dbh);
}

1;