package GwasCentral::DataSource::Marker;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../../../../";
use LWP::UserAgent;
extends qw(
GwasCentral::DataSource::Base
GwasCentral::DataSource::Marker::MarkerList
);
with qw(
GwasCentral::DataSource::Nature::DBIC
);

has '+source_name' => ( 'default' => 'Marker' );
use Data::Dumper qw(Dumper);
use Carp qw(confess);

sub get_marker_and_refcoords_by_identifier {
	my ($self, $id) = @_;
	my $marker = $self->dbh->resultset('Marker')->search({ -or=>[{'me.Identifier' => $id},{'accession'=>$id}] })->first;
	
	my $coords;
	$marker and $coords = $marker->markercoords->search({'assemblyname' => $self->config->{assembly_name} } )->first;
	return ($marker,$coords);
}

sub get_marker_by_identifier {
	my ($self, $id) = @_;
	my $marker = $self->dbh->resultset('Marker')->search({'me.Identifier' => $id})->first;
	
	return $marker;
}

1;


1;
