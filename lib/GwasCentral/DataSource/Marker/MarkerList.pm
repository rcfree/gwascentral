package GwasCentral::DataSource::Marker::MarkerList;
use Data::Dumper qw(Dumper);
use strict;
use warnings;
use Tie::Hash::Indexed;
use GwasCentral::Base qw(shorten_author_list);

sub get_marker_list {
	my ( $self, $args ) = @_;
	$self->log->info("get local studies");
	my ($opts, $params) = $self->_prepare_marker_list_options($args);
	
	$self->log->info("final params:".Dumper($params)." and opts:".Dumper($opts));
	return if !$args->{Query}->fval('IdentifierList');
	my $marker_rs         = $self->dbh->resultset("Marker")->search($params, $opts);
	return ( [], 0, undef) if !$marker_rs;
	my $pager = $args->{Query}->pager;
	my @markers          =  $marker_rs->all;

	
	my @results = map { $self->marker_to_result($_) } @markers;
		
	return (\@results,$pager);
}

sub _prepare_marker_list_options {
	my ($self, $args) = @_;
	my $q = $args->{Query};
	my $page_size = $q->page_size;
	my $opts = {};
	
	my $params = {};
	
	my $il = $q->f('IdentifierList');
	
	if (!$il->ignore) {
		my $ident_list = $il->value;
		$self->log->info("il value:".Dumper($il->value));
		 $params->{'identifier'} = {-in => $ident_list };
	}
	#$self->log->info("opts:".Dumper($opts)."\nparams:".Dumper($params));
	return ($opts, $params);
}

sub marker_to_result {
	my ($self, $marker) = @_;
	my @results = ();
	my $counter = 0;
	#my $local_label  = $self->Connect->local_instance->{label};
	my $local_label = "gwascentral.org";
		tie my %item, "Tie::Hash::Indexed";
		my $coords = $marker->markercoords->search({'assemblyname'=>$self->config->{assembly_name}})->first;
		%item = (
			'identifier' => $marker->identifier,
			'accession' => $marker->accession,
			'chr' => ($coords and $coords->chr),
			'start' => ($coords and $coords->start),
			'stop' => ($coords and $coords->stop),
			'ref_coords' => ($coords and "chr".$coords->chr.":".$coords->start."..".$coords->stop),
			'variationtype' => $marker->variationtype,
			'status' => $marker->status,
			'sequence' => $marker->upstream10bp.$marker->alleleseqs.$marker->downstream10bp,
		);
		return \%item;
}

1;