package GwasCentral::DataSource::Study::StudyList;
use Data::Dumper qw(Dumper);
use Tie::Hash::Indexed;

sub get_study_list {
	my ( $self, $args ) = @_;
	my ($opts, $params) = $self->_prepare_study_list_options($args);
	
	#$self->log->info("final params:".Dumper($params)." and opts:".Dumper($opts));
	
	my $study_rs         = $self->dbh->resultset("Study")->search($params, $opts);

	return ( [], 0, undef) if !$study_rs;
	my $pager;
	if ($args->{Query}->page_size eq 'all') {
		$pager = $args->{Query}->new_pager($study_rs->count);
	}
	else {
		$pager = $study_rs->pager;
	}
	if ($pager && $args->{Query}->page>$pager->last_page ) {
		$self->throw("Unable to display page ".$args->{Query}->page.". There are only ".$pager->last_page." pages available.");
	}
	my @studies          =  $study_rs->all;

	$self->populate_study_access_levels( \@studies );

	my @results = map { $self->study_to_result($_) } @studies;
		
	return (\@results,$pager);
}

sub _prepare_study_list_options {
	my ($self, $args) = @_;
	my $q = $args->{Query};
	my $page_size = $q->page_size;
	my $opts = {};
	if ($page_size ne 'all') {
		$opts = {
			rows => int($page_size),
			page => int($q->page),
		};
	}
	my $params = {'me.ishidden'=>'no'};
	$q->fval('Threshold') and $params->{'me.significancelevel'}={'>='=>$q->fval('Threshold') };
	my $mcount = $q->fval('MarkerCount');
	if ($mcount && $mcount ne 'all') {
		my ($scount, $ecount) = split(",",$q->fval('MarkerCount'));
		$params->{'(select experimentid2.totalmarkersimported from Experiment experimentid2 where experimentid2.studyid = me.studyid order by experimentid2.totalmarkersimported desc limit 0,1)'} = {'>'=>$scount,'<'=>$ecount};
	}
	my $ident = $q->f('IdentifierList');
	
	if (!$ident->ignore) {
		my $ident_list = $ident->as_array;
		if (ref($ident_list) eq 'ARRAY') {
		 	$params->{'me.identifier'} = {-in => $ident_list };
		}
		else {
			$params->{'me.identifier'} = $ident_list;
		}
	}
	my $sort_by = $q->f('SortBy')->by_index(2);
	$opts->{order_by}=$sort_by;
	$opts->{columns}=['studyid','name','identifier','studyabstract','timecreated','ishidden','isdisplayed','significancelevel'];
	#$self->log->info("opts:".Dumper($opts)."\nparams:".Dumper($params));

	return ($opts, $params);
}

sub study_to_result {
	my ($self, $study) = @_;
	my @results = ();
	my $counter = 0;

		my $study_identifier   = $study->identifier;
		my @resultsets         = ();
		my %gtbundles_flag_for = ();
		my @marker_counts      = ();
		
		my @experiments = $study->experiments->search({},{'columns'=>['experimentid','totalmarkersimported','genotypedbundle'], 'prefetch' => {'phenotypemethodid' => 'phenotypepropertyid'}});
		
		foreach my $experiment ( @experiments ) {
			if ( $experiment->genotypedbundle ) {
				%gtbundles_flag_for =
				  map { $_ => 1 } split( /\,/, $experiment->genotypedbundle );
			}

			foreach my $resultset ( $experiment->resultsets->search({},{'columns'=>['resultsetid','identifier']} )) {
				push @resultsets, $resultset->identifier;
			}

			push @marker_counts, int( $experiment->totalmarkersimported );

		}
		my @sorted_counts = reverse sort { $a <=> $b } @marker_counts;
		my @citations = map {
			{
				authors   => $_->authors,
				title     => $_->title,
				detail    => $_->detail,
				pubmedid  => $_->pubmedid,
				doi       => $_->doi,
			}
		} $study->citations->search( { 'isprimary' => 'yes' } );

		my @other_citations = map {
			{
				authors   => $_->authors,
				title     => $_->title,
				detail    => $_->detail,
				pubmedid  => $_->pubmedid,
				doi       => $_->doi,
			}
		} $study->citations->search( { 'isprimary' => 'no' } );

		my @pproperties = ();
		foreach my $exp ( @experiments ) {
			$exp->phenotypemethodid
			  and push @pproperties, $exp->phenotypemethodid->phenotypepropertyid;
		}

		my %phenotypes = map { $_->name => 1 } @pproperties;

#		my @ontology_identifiers = ();
#		foreach my $pproperty (@pproperties) {
#			my @pannotations = $pproperty->phenotypeannotations;
#			foreach my $pannotation (@pannotations) {
#				my $pidentifier = $pannotation->phenotypeidentifier;
#				push @ontology_identifiers,
#				  {
#					mesh_identifier => $pidentifier,
#				  };
#			}
#		}
		@citations = (@citations, @other_citations);
		tie my %item, "Tie::Hash::Indexed";
		%item = (
			'identifier'              => $study_identifier,
			'number'                  => $study->number,
			'name'                    => $study->name,
			'phenotypes'              => [ keys %phenotypes ],
			'date_created'            => $study->timecreated,
			'date_updated'            => $study->timeupdated,
			'accesslevel'      => $study->accesslevel,
			'request_response'        => $study->response_status,
			'child_accesslevel' => $study->child_accesslevel,
			'citations'       => \@citations,
			'resultsets'              => \@resultsets,
			'genotyping_platform'     => [ keys %gtbundles_flag_for ],
			'number_of_markers'       => \@sorted_counts,
			'abstract'                => $study->studyabstract,
			'highest_pvalue' => $study->significancelevel,
		);
		return \%item;
}

1;
