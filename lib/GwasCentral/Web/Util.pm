# $Id: Util.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Web::Util - Collection of utility modules for the HGVbase-G2P web application


=head1 SYNOPSIS

    use GwasCentral::Web::Util qw(open_infile);

    # Get filehandle on regular or compressed files
    my $uri = add_uri_parameters($uri,{'content-type'=>'text/xml'});
    
    

=head1 DESCRIPTION

This module contains several utility modules that are used by the web application part
of the HGVbase-G2P system, such as dealing with URI parameters. 

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Web::Util;

	use strict;
	use warnings;
	use English qw( -no_match_vars );

	# Use OIO inside-out object framework for this non-OO package as well
	use Moose::Exporter;
	use Log::Log4perl qw( :easy );
	use Carp qw(croak cluck confess carp);

	BEGIN {

		# Export no subroutines by default, only this list on demand
		our @EXPORT_OK = qw( load_config
		  add_uri_parameters
		);
	}

=head2 load_config

  Usage      : my %config = load_config('./conf/hgvbase.conf');
  Purpose    : Load configuration info from file into hash via Config::General
  Returns    : Multi-level hash 
  Arguments  : Path to the configuration file
  Throws     : N/A
  Status     : Public
  Comments   : See Config::General for details on the structure of config-files, 
               as well as conf/hgvbase.conf for the master HGVbase-G2P config.

=cut

	 sub add_uri_parameters {
    	my ($uri, $par_hash)=@_;
    	
    	my @temp = split(/\?/,$uri);
    	
    	my %pars;
    	#already parameters
    	if ($temp[1]) {
    		my @temp2 = split('&',$temp[1]);
    		foreach my $pair(@temp2) {
    			my @temp3 = split('=',$pair);
    			$pars{$temp3[0]}=$temp3[1];
    		}	
    	}
    	
    	foreach my $new_par(keys %{$par_hash}) {
    		$pars{$new_par}=$par_hash->{$new_par};
    	}
    	
    	my @pars_list = map { $_."=".$pars{$_}; } keys %pars;
    	
    	my $final_uri = $temp[0]."?".join("&",@pars_list);
    	
    	return $final_uri;
    }

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Mummi <gthorisson@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Util.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut

