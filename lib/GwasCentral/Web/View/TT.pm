# $Id: TT.pm 1475 2010-06-19 19:15:58Z rcf8 $

=head1 NAME

  GwasCentral::Web::View::TT - Catalyst View component for Template Toolkit

=head1 SYNOPSIS


=head1 DESCRIPTION

  This class is a View component for presenting HGVbase-G2P website content via
  Template Toolkit templates. 

=cut

package GwasCentral::Web::View::TT;

use strict;
use warnings;

use base 'Catalyst::View::TT';
use Env qw(GWASCENTRAL_HOME);
my $home =  $GWASCENTRAL_HOME;
use GwasCentral::Base qw(shorten_author_list);
use Data::Dumper qw(Dumper);
__PACKAGE__->config({
    CATALYST_VAR => 'c',
    INCLUDE_PATH => [
       $home.'/htmltemplates/src',
       $home.'/htmltemplates/lib'
    ],
    ENCODING => 'utf8',
    ABSOLUTE => 1,
    PRE_PROCESS  => 'config.tt2',
    WRAPPER      => 'wrapper.tt2',
    ERROR        => 'error.tt2',
    TIMER        => 0,
    DEBUG => 1,
	EVAL_PERL => 1,
    # Extra from Mummi
    TEMPLATE_EXTENSION => '.tt2',
    INTERPOLATE  => 1,
    FILTERS => {
	highlight => [ sub {
	    my ($context, $terms) = @_;
	    return sub {
		my $input = shift;
		foreach my $term(@$terms) {
		    $term =~ s/\"//g;
		    $input =~ s|([^\/,=])($term)|$1<span style=\"background-color: yellow; font-weight: bold\">$2</span>|gi;
		}
		return $input;
	    }
	}, 1],
	strip_hrefs => sub {
	    my $input = shift;
	    warn "stripping hrefs from '$input'";
	    $input =~ s/\<a\s+href=[^\>^\<]+\>([^\>^\<]+)\<\/a\>/$1/gxmsi;
	    warn "after stripping: '$input'";
	    return $input;
	},
	# convert bp into nice Mb/Kb units (borrowed from GBrowse CGI-script!!)
	unit_label => sub {
	    my $value = shift;
	    #my $unit     = $CONFIG->setting('units')        || 'bp';
	    #my $divider  = $CONFIG->setting('unit_divider') || 1;
	    my $unit     =  'bp';
	    my $divider  =  1;
	    $value /= $divider;
	    my $abs = abs($value);
	    
	    my $label;
	    $label = $abs >= 1e9  ? sprintf("%.4g G%s",$value/1e9,$unit)
		: $abs >= 1e6  ? sprintf("%.4g M%s",$value/1e6,$unit)
		: $abs >= 1e3  ? sprintf("%.4g k%s",$value/1e3,$unit)
		: $abs >= 1    ? sprintf("%.4g %s", $value,    $unit)
		: $abs >= 1e-2 ? sprintf("%.4g c%s",$value*100,$unit)
		: $abs >= 1e-3 ? sprintf("%.4g m%s",$value*1e3,$unit)
		: $abs >= 1e-6 ? sprintf("%.4g u%s",$value*1e6,$unit)
		: $abs >= 1e-9 ? sprintf("%.4g n%s",$value*1e9,$unit)
		: sprintf("%.4g p%s",$value*1e12,$unit);
	    if (wantarray) {
		return split ' ',$label;
	    } else {
		return $label;
	    }
	},
	shorten_author_list =>sub {
		return shorten_author_list(@_);
	}
    }
});

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>
  Rob Free <rcfree@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

