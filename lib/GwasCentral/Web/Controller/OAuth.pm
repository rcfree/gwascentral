# $Id: Study.pm 1537 2010-09-21 10:56:08Z rcf8 $

=head1 NAME

HGVbaseG2P::Web::Controller::Study - Catalyst Controller for a study

=head1 DESCRIPTION

  This class retrieves study-based data for a particular Study or 
  list of studies and outputs the data via a TT template, or puts it directly into the 'body' as XML, JSON or text.
  Most of the search logic is contained in the Search API.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::OAuth;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Env;

use Data::Dumper qw(Dumper);

my $home = $ENV{HGVBASEG2PWEB_HOME};

sub auth :Local {
	my ($self, $c) = @_;
	#my $auth = GwasCentral::Connect->new({conf_file=>$c->config});
	#$auth->receive($c->req->params); 
}

sub connected :Local {
	my ($self, $c) = @_;
	my $new_gcuser = $c->req->param('identity_hash');
	
	if (!$c->session->{identity_hash}) {
		$c->res->output('nouser');
	}
	else {
		if ($c->session->{identity_hash} eq $new_gcuser) {
			$c->res->output('sameuser')
		}
		else {
			$c->res->output('diffuser');
		}
	}
}

sub request :Local {
	my ($self, $c) = @_;
	$c->log->info("gets to oauth/request");
	
	my $connect = $c->stash->{Connect};
	#$connect->db($c->model('Connect'));
	$c->log->info("gets to response");
	$c->log->info("request params:".Dumper($c->req->params));
	#eval {
	my 	($response, $gcuser) = $connect->response($c->req->params, $c->req->method);
#	};
#	if ($@) {
#		warn "error:$@";
#		$c->res->output($@);
#		return;
#	}
	$c->log->info("set session identity_hash to ".$c->req->param('identity_hash'));
	$c->session->{identity_hash}=$c->req->param('identity_hash');
	$c->res->output($response);
}

sub access :Local {

}

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>
  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
