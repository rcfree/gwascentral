# $Id: Browser.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

  GwasCentral::Web::Controller::Browser - Catalyst Controller for Browser

=head1 DESCRIPTION

  This class handles retrieving & transforming data for the Browser part of GwasCentral
  It ties together the various views, config settings and AJAX calls.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Browser;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=3;
use List::MoreUtils qw(any uniq);
use List::Compare;
use Bio::Graphics::Browser;
use GwasCentral::Browser::Upload;
use JSON::XS;
use GwasCentral::Browser::Genome;
use GwasCentral::Search::Query::Browser;

my $home = $ENV{GWASCENTRAL_HOME};

=head2 default
Action to deal with browser default. Sets up the stash with session variables and collated hashrefs for studies and resultsets

=cut

sub root : Regex('^browser$') {
	my ( $self, $c ) = @_;
	$c->forward('genome');
}

sub genome : Regex('^browser/genome$') {
	my ( $self, $c ) = @_;

	#$c->forward('common');
	$c->stash->{template}   = 'browser.tt2';

	$c->stash->{'browser_template'} = 'genome';
	my $q = $self->do_search($c, "Browser::Genome");
	if ($q) {
		$q->f('AddedResultsets')->upload_data($c->session->{upload_data});
		$q->f('AddedResultsets')->update;
		$c->log->info("ar:".Dumper($q->f('AddedResultsets')));
	}
}	

sub common : Private {
	my ( $self, $c ) = @_;

	my $studies;
	my $resultsets;
	if ( $c->req->param('remove_uploads') ) {
		my @remove_uploads = split( ",", $c->req->param('remove_uploads') );

		my $bu =
		  GwasCentral::Browser::Upload->new( { conf_file => $c->config } );
		$bu->upload_data( $c->session->{upload_data} );
		foreach my $upload (@remove_uploads) {

			$bu->delete_upload($upload);
			if ($@) {
				$c->stash->{'upload_errors'} = $@;
				$c->session->{upload_data} = $bu->upload_data;
				return;
			}
		}
	}

	#eval {
	my $browser_template = $c->req->captures->[0];

	$c->stash->{template}   = 'browser.tt2';

	$c->stash->{'browser_template'} = 'genome';
}

sub selected : Regex('^browser/selected$') {
	my ( $self, $c ) = @_;
	$c->stash->{template}   = 'browser.tt2';
	$c->stash->{'browser_template'} = 'selected';
}

sub region : Regex('^browser/region$') {
	my ( $self, $c ) = @_;

	$c->stash->{template}   = 'browser.tt2';

	$c->stash->{'browser_template'} = 'region';
	$c->req->params->{sessionid}=$c->sessionid;
	$c->log->info("sessionid in region:".$c->sessionid);
	my $q = $self->do_search($c, "Browser::Region");
	if ($q) {
		$q->f('AddedResultsets')->upload_data($c->session->{upload_data});
		$q->f('AddedResultsets')->update;
	}
}

sub file_upload : Regex('^browser/file_upload$') {
	my ( $self, $c ) = @_;

	$c->stash->{upload}->{data} = [];
	my $data = $c->stash->{upload}->{data};
	my $id   = $c->req->params->{progress_id};
	$c->log->info( "progressid:" . $id. ", sessionid:".$c->sessionid );
	my $tempname = GwasCentral::Browser::Upload::convert_tempfile(
		{
			filename => $c->req->upload('file')->tempname,
			fh       => $c->req->upload('file')->fh,
			tempdir  => $c->config->{tempdir},
			progressid => $id,
		}
	);

	#$c->session->{files}->{$id} = $tempname;
	$c->res->output('success');
	$c->log->info( "resultset_name:" . $c->req->params->{'resultset_name'} );
}

sub clear_upload : Regex('^browser/clear_upload$') {
	my ( $self, $c ) = @_;
	$c->session->{upload_data}      = {};
	$c->stash->{'upload_messages'}  = "Successfully removed all uploads";
	$c->stash->{template}           = 'browser.tt2';
	$c->stash->{upload_data}        = $c->session->{upload_data};
	$c->stash->{'browser_template'} = 'upload';
}

sub custom : Regex('^browser/custom$') {
	my ( $self, $c ) = @_;
	$c->forward('common');
	my $progressid = $c->req->params->{'progressid'};
	$c->log->info("progressid:$progressid");
	$c->log->info("sessionid:".$c->sessionid);
	$c->log->info(
		"upload; upload_data:" . Dumper( $c->session->{upload_data} ) );
	$c->stash->{template}           = 'browser.tt2';
	$c->stash->{upload_data}        = $c->session->{upload_data};
	$c->stash->{'browser_template'} = 'upload';
	
	return if !$progressid;
	my $sess = $c->session;
	my $repeat = 0;
	
#	GETPROGRESS:
#	my $file = $c->session->{files}->{$progressid};
#	$c->log->error("try to get file for progressid:".$progressid);
#	if (!$file && $repeat<3) {
#		$c->log->error("unable to get file $repeat times for progressid:".$progressid);
#		sleep 5;
#		$repeat++;
#		goto GETPROGRESS;
#	}
#	
#	$c->log->info(
#		"custom - files:" . Dumper( $c->session->{files} ) );
#	$c->log->info( "filename:" . $file . " for progressid:$progressid" );
	my $browser_upload =
	  GwasCentral::Browser::Upload->new( { conf_file => $c->config } );
	$browser_upload->upload_data( $c->session->{upload_data} );
	$browser_upload->DS( "Browser", $c->model('Browser') );
	eval {
		my @rs_idents = $browser_upload->do_upload(
			{
				file           => $progressid,
				study_name     => $c->req->params->{'study_name'},
				resultset_name => $c->req->params->{'resultset_name'},
				resultset_identifier =>
				  $c->req->params->{'resultset_identifier'},
				field_separator => $c->req->params->{'separator'},
				sessionid       => $c->sessionid,
				tempdir  => $c->config->{tempdir},
			}
		);
	};

	if ($@) {
		$c->stash->{'upload_errors'} = $@;
		$c->session->{upload_data} = $browser_upload->upload_data;
		return;
	}
	$c->log->info( "upload_data:" . Dumper( $browser_upload->upload_data ) );
	$c->session->{upload_data}     = $browser_upload->upload_data;
	$c->stash->{upload_data}       = $c->session->{upload_data};
	$c->stash->{'upload_messages'} = $browser_upload->messages;
	$c->stash->{'upload_issues'}   = $browser_upload->issues;
}

sub remove_upload : Regex('^browser/remove_upload$') {
	my ( $self, $c ) = @_;
	$c->forward('common');
	$c->stash->{template}         = 'browser.tt2';
	$c->stash->{browser_template} = 'upload';
	my $bu = GwasCentral::Browser::Upload->new( { conf_file => $c->config } );
	$bu->upload_data( $c->session->{upload_data} );
	eval { $bu->delete_upload( $c->req->param('id') ); };
	if ($@) {
		$c->stash->{'upload_errors'} = $@;
		$c->session->{upload_data} = $bu->upload_data;
		return;
	}

	$c->session->{upload_data} = $bu->upload_data;
	$c->stash->{upload_data}   = $c->session->{upload_data};

	$c->stash->{upload_messages} = $bu->messages;
	$c->stash->{'upload_issues'} = $bu->issues;
}

=head2 store

Action to deal with AJAX call for session 'store'. Uses supplied parameters to populate session

=cut

sub store : Local {
	my ( $self, $c ) = @_;

	#return 'success' if no errors
	my $basic = $c->stash->{basic};
	my $ar = $basic->f('AddedResultsets');
	$c->stash->{datasets} = $ar->resultsets;
	$c->stash->{studies} = $ar->added_studies;
	$c->stash->{phenotypes} = $ar->added_phenotypes;
	$c->stash->{datasets_by_study}=$ar->resultsets_by_study;
	$c->stash->{datasets_by_phenotype}=$ar->resultsets_by_phenotype;
}

=head2 clear

Action to deal with AJAX call for session 'clear'. Deletes the session completely and resets it.

=cut

sub clear : Path('browser/clear') {
	my ( $self, $c ) = @_;
	$c->delete_session('session expired');
	$c->session( {} );
	$c->forward( 'GwasCentral::Web::Controller::Study', 'studies' );
	$c->stash->{template} = 'studies.tt2';
}

sub rmarkers : Regex('^browser/rmarkers$') {
	my ( $self, $c ) = @_;
	$c->stash->{template}   = 'browser.tt2';

	$c->stash->{'browser_template'} = 'rmarkers';
	$self->do_search($c, "Browser::RegionMarkers");
	
	
}

=head2 study_markers

  Usage      : Controller action which maps to URL /study/HGVSTxxx/markers (Markers tab)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub tmarkers : Regex('^browser/tmarkers$') {
	my ( $self, $c ) = @_;

	#retrieve study via 'study' action - if an error then return
	$c->stash->{ignore_non_html}=1;
	
		$c->stash->{browser_template} =
	  "tmarkers";    #current tab template (becomes 'study/markers.tt2')
	
	$c->stash->{template}   = 'browser.tt2';
	$self->do_search($c, 'Browser::TopMarkers');
}


sub browser_resultset : Regex('^browser/tmarkers/(HGV\w+)$') {
	my ( $self, $c ) = @_;
	$c->req->params->{rid}=$c->req->captures->[0];
	$c->log->info("rid:".$c->req->params->{rid});
	$c->stash->{template}   = 'browser.tt2';
	$self->do_search($c,'ResultsetMarkers');
}

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
