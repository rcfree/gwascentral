# $Id: User.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

  HGVbaseG2P::Web::Controller::User - Catalyst Controller for User pages

=head1 DESCRIPTION

  This module is a sub class of Catalyst::Controller::UserRegistration which deals with most (non-openID) logic behind the user access system.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::User;

use strict;
use warnings;


use Data::Dumper qw(Dumper);
use base qw(Catalyst::Controller::UserRegistration Catalyst::Controller::GwasCentral);

=head2 default
Action to deal with browser default. Sets up the stash with session variables and collated hashrefs for studies and resultsets

=cut

sub after_register {
	my ($self, $c) = @_;
	
	my $ac = $c->get_access_control;
	eval {
		$ac->create_user($c->req->params->{'email'},$c->req->params->{'nickname'});
	};
	if ($@ && $@ !~/Duplicate entry/) {
		$c->stash->{user_errors}="GAS error:$@";
		return;
	}
}

sub before_register {
	my ($self, $c) = @_;
	if (!$c->req->param('accept_terms')) {
		$c->stash->{user_errors}="You must accept the terms and conditions";
		return;
	}
}

sub before_login {
	my ($self, $c) = @_;
	$c->req->param('returnurl') and $c->session->{returnurl}=$c->req->param('returnurl');
	$c->stash->{returnurl} = $c->req->param('returnurl');
}

sub after_login {
	my ($self, $c) = @_;
	$c->user->{url} = $c->user->email;
	my $acontrol = $self->get_AccessControl($c);
	$c->log->info("session id after login:".$c->sessionid);
	#$c->session->{is_admin} = $acontrol->is_current_user_admin_of_any_studies;
	
	$c->log->info("logged in as:".$c->req->param('email')." now redirecting to ".$c->session->{returnurl});
}

sub request :Path('user/request') {
	my ($self, $c) = @_;
	$c->stash->{template}='user/request.tt2';
	my $acontrol = $c->get_access_control();
	
	my $entity = $c->req->param('entity');
	
	eval {
		if ($entity eq 'study') {
			my ($req, $study) = $acontrol->request_study_access($c->req->param('id'));
			$c->stash->{entity}=ucfirst($entity);
			$c->stash->{identifier}=$study->identifier;
			$c->stash->{title}=$study->name;
		}
	};
	
	$@ and $c->stash->{error}=$@->message;
}

1;