# $Id: Phenotype.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Phenotype - Catalyst Controller for phenotypes

=head1 DESCRIPTION

TODO

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Phenotype;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

#use GwasCentral::Schema;
use JSON::XS;
use Data::Dumper qw(Dumper);

# Globals
my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
$json_coder->space_after(1);

=head2 view

Action which redirects to the default action

=cut

#sub summary : Local {
#    my ( $self, $c ) = @_;
#    my $url = $c->req->uri->as_string;
#    $url =~ s|(\w+)/view|$1|;
#    $c->response->redirect($url);
#}

=head2 default

Action which retrieves a phenotype method and passes it to the template

=cut

sub default :Regex('^phenotype/(HGV\w+)$') {
    my ( $self, $c) = @_;
	
	$c->req->params->{id}=$c->req->captures->[0];
	$c->stash->{template}    = 'phenotype.tt2';
	eval {
		$self->do_search($c,'Phenotype');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

sub pmethod :Regex('^phenotypemethod/(HGV\w+)$') {
    my ( $self, $c) = @_;
	$c->res->redirect("/phenotype/".$c->req->captures->[0]);
}

sub phenotypes :Regex('^phenotypes$')  {
	my ($self, $c) = @_;
	$c->forward('phenotypes_term');
}

sub phenotypes_tree :Regex('^phenotypes/tree$') {
	my ($self, $c) = @_;
	$c->stash->{template}='phenotypes.tt2';
	$c->stash->{phenotypes_template}='tree';
	eval {
		$self->do_search($c,'Phenotypes::Tree');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

sub phenotypes_term :Regex('^phenotypes/term$') {
	my ($self, $c) = @_;
	$c->stash->{template}='phenotypes.tt2';
	$c->stash->{phenotypes_template}='term';
	eval {
		$self->do_search($c,'Phenotypes::Term');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

=head2 experiments

Action which gets experiments associated with a phenotypemethod and passes them to a template.

=cut

sub experiments :Regex('^phenotypemethod/experiments/(HGV\w+)$') {
    my ( $self, $c ) = @_;
	my $id = $c->req->captures->[0];
	
    # Retrieve experiments with phenotype method from the database
    my $experiments = $c->model("StudyDB")->get_experiments_with_phenotypemethod_by_identifier($id);
    
  	if ($experiments->count==1 && $experiments->single->resultsets->count==1) {
  		$c->stash->{single}=1;
  	}
  	
    $c->stash->{experiments}=$experiments;
    
    $c->stash->{template}="phenotypemethod/experiments.tt2";
    return;
}


sub meshtree :Path('phenotypes/meshtree') {
    my ( $self, $c ) = @_;
    $c->stash->{template}="phenotypes/meshtree.tt2";
    return;
}

sub hpotree :Path('phenotypes/hpotree') {
    my ( $self, $c ) = @_;
    $c->stash->{template}="phenotypes/hpotree.tt2";
    return;
}

# deals with MeSH concept autocomplete and concept<->heading mapping
sub meshconcept :Regex('^phenotypes/meshconcept$') {
    my ( $self, $c ) = @_;
	my $value = $c->req->param('q');
    my $category = $c->req->param('category');
    my $htmlstring ='';
    my $pidstring='';
  	my %uniquehash=();
 
    # accepts either a q (autocomplete) or a catgeory (concept<->heading mapping) but not both.
    if (($value) && ($category)){ 
		my $urlc = '/phenotypes';
   		$c->response->redirect($urlc);
	}
    
    # queryString present = autocomplete

	my $lookup = GwasCentral::Search::Lookup->new({conf_file=>$c->config});
	$lookup->populate_DS_from_context($c);
	
    if ($value ne ''){
   
	  	my @lookup_rows = $lookup->autocomplete({
	  		rows => 15,
	  		sorted => 1,
	  		lookup_sources => ['ontology'],
	  		value => $value,
	  	});
	  
 		$c->res->content_type('text/html');
		
		$htmlstring = join("\n",@lookup_rows);
    }
	
	# category present = convert category to heading, find all child headings and return pid's if used in Study database
	elsif ($category ne ''){
		$htmlstring = $htmlstring."?";
		
		my @pids = $lookup->category_to_headings($category);
		
 	   foreach my $key(@pids) {
        	$pidstring=$pidstring."pid=".$key."&";
    	}
    
		$htmlstring=$htmlstring.$pidstring."concept=".$category;
		my $urlc = '/phenotypes'.$htmlstring;
		$c->response->redirect($urlc);
	}

	# no queryString or category means the path has been accessed directly
	else {  
   		#$htmlstring = 'This script should not be accessed directly!';
   		my $urlc = '/phenotypes';
   		$c->response->redirect($urlc);
	}
	$c->res->content_type('text/html');
	$c->res->output($htmlstring);
    
    return;
}



=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>
  Rob Free <rcfree@gmail.com>
  Tim Beck <tb143@le.ac.uk>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
