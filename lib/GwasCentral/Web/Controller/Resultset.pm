# $Id: Study.pm 1537 2010-09-21 10:56:08Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Study - Catalyst Controller for a study

=head1 DESCRIPTION

  This class retrieves study-based data for a particular Study or 
  list of studies and outputs the data via a TT template, or puts it directly into the 'body' as XML, JSON or text.
  Most of the search logic is contained in the Search API.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Resultset;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Env;

use Data::Dumper qw(Dumper);

my $home = $ENV{HGVBASEG2PWEB_HOME};

=head2 resultset

  Usage      : Controller action which maps to URL /study/HGVSTxxx (used by all study report pages - see below)
  Purpose    : Action related to Study report which retrieves information using DB and via Access Control
  Returns    : N/A
  Arguments  : N/A 
  Throws     : N/A
  Status     : Public
  Comments   : 

=cut

sub resultset : Regex('^resultset/(HGV\w+)$') {
	my ( $self, $c ) = @_;
	my $rs_ident = $c->req->captures->[0];
	
	#output results as TT template or to XML/JSON/text using 'output_report'
	my $resultset = $c->model('StudyDB')->get_resultset_by_identifier($rs_ident);
	$c->output_report( 'resultset', $resultset);

}


=head1 AUTHOR

  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
