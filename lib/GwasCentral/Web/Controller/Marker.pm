# $Id: Marker.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

  GwasCentral::Web::Controller::Marker - Catalyst Controller for Markers

=head1 DESCRIPTION

  This class handles retrieving & transforming data for a single marker, this includes frequency and association data.
  Data is then passed to the respective template.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Marker;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

use Data::Dumper qw(Dumper);

sub markers :Regex('^markers$') {
	my ($self, $c) = @_;
	
	eval {
		$self->do_search($c,'Markers');
	};
	if ($@) {
		$c->stash->{error}          = $@;
	}
	$c->stash->{template}='markers.tt2';
	
}


=head2 default

Retrieves marker by identifier and places this in the stash.

=cut

sub marker : Regex('^marker/([\w|:]+)$') {
    my ( $self, $c) = @_;
    $c->req->params->{mid} = $c->req->captures->[0];

    $c->stash->{marker_template}="summary";
	
	$c->stash->{template} = 'marker.tt2';
	
	eval {
   		$self->do_search($c,'Marker');
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

=head2 results

Retrieves association results for a specific marker and threshold. Processes the results to determine the associated
 studies, experiments and resultsets and then makes these available to the template.

=cut

sub results_table : Regex('^marker/([\w|:]+)/results$') {
	my ( $self, $c) = @_;
	$c->log->info("mid:".$c->req->captures->[0]);
	
	$c->req->params->{mid} = $c->req->captures->[0];    
	my $format = $c->req->params->{format};
	$c->req->params->{format}="html";
	$self->do_search($c,'Marker');
	$c->req->params->{format}=$format;
	$self->do_search($c, 'Marker::Results');
	$c->stash->{template} = 'marker.tt2';
	$c->stash->{marker_template}="results";
	$c->req->params->{format}="html";
}

sub results_single : Regex('^marker/([\w|:]+)/results/([\w]+)$') {
	my ( $self, $c) = @_;
	$c->log->info("mid:".$c->req->captures->[0]);
	$c->log->info("rid:".$c->req->captures->[1]);
	$c->req->params->{mid} = $c->req->captures->[0];
	$c->req->params->{rid} = $c->req->captures->[1];
    $c->stash->{template}="marker/results/single.tt2";
    $self->do_search($c,'Marker::Result');
}

=head2 frequencies

Retrieves frequency data for a specific marker. This is converted into a format which can be used by the template to present stacked
frequency data.

=cut

sub frequencies : Regex('^marker/(HGVM\w+)/frequencies$') {
	my ( $self, $c) = @_;
    my $id = $c->req->captures->[0];
    my $marker = $c->model("MarkerDB")->get_marker_by_identifier($id);
    
    $c->stash->{marker_template}="frequencies";
    $c->stash->{template} = 'marker.tt2';
    $c->stash->{marker}   = $marker || return;
    $c->do_search('Marker::Frequencies', { id => $id, v => $c->req->param('v') || 'report'});
}


=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>
  Rob Free <rcfree@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
