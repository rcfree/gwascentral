# $Id: Assayedpanel.pm 1487 2010-06-29 09:04:05Z rcf8 $

=head1 NAME

  GwasCentral::Web::Controller::Assayedpanel - Catalyst Controller for Assayedpanels

=head1 DESCRIPTION

  This class handles data & business logic relating to Assayedpanel entries.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Assayedpanel;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);

=head2 default

Retrieves an assayedpanel from the database (by identifier) and puts it in the stash for the template

=cut

sub default :Regex('^assayedpanel/(HGV\w+)$') {
    my ( $self, $c ) = @_;
    my $id = $c->req->captures->[0];

    my $apanel = $c->model("StudyDB")->get_assayedpanel_by_identifier($id);
    
    $c->stash->{apanelid} = $id;
    $c->stash->{template} = 'assayedpanel/view.tt2';
    $c->stash->{apanel} = $apanel;
    return; 
}


=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>
  Rob Free <rcfree@gmail.com>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
