# $Id: Study.pm 1537 2010-09-21 10:56:08Z rcf8 $

=head1 NAME

GwasCentral::Web::Controller::Study - Catalyst Controller for a study

=head1 DESCRIPTION

  This class retrieves study-based data for a particular Study or 
  list of studies and outputs the data via a TT template, or puts it directly into the 'body' as XML, JSON or text.
  Most of the search logic is contained in the Search API.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Genes;

use strict;
use warnings;
use base qw(Catalyst::Controller::GwasCentral);
use Env;

use Data::Dumper qw(Dumper);

# deals with MeSH concept autocomplete and concept<->heading mapping
sub lookup :Regex('^genes/lookup/(.+)$') {
    my ( $self, $c ) = @_;
	my $value = $c->req->param('q');
	my $type = $c->req->captures->[0];
  	my $lookup = GwasCentral::Search::Lookup->new({conf_file=>$c->config});
  	$lookup->populate_DS_from_context($c);
  	my @lookup_rows = $lookup->autocomplete({
  		rows => 10,
  		sorted => 1,
  		lookup_sources => [$type],
  		value => $value,
  	});
  	
  	my $htmlstring = join("\n",@lookup_rows);
  	$c->res->content_type('text/html');
	$c->res->output($htmlstring);
}


sub loci :Regex('^genes$') { 
	my ( $self, $c ) = @_;
	$c->req->captures->[0]="phenotypes";
	$c->forward("genes_extra");
#	$c->stash->{template}    = 'genes.tt2';
#	$c->stash->{genes_template}="phenotypes";
#	$c->stash->{genes_qname}="Genes::Phenotypes";
#	eval {
#		$self->do_search($c,'Genes');
#	};
#	if ($@) {
#		$c->stash->{error}=$@;
#	}
}

sub genes_extra :Regex('^genes/(.+)$') { 
	my ( $self, $c ) = @_;
	my $extra  = $c->req->captures->[0];
	$c->req->params->{subquery}=$extra;
	$self->do_search($c,'Genes');
	$c->stash->{template}    = "genes.tt2";
	my $qname = 'Genes_'.ucfirst($extra);
	$c->stash->{genes_template}=$extra;
	$c->stash->{genes_qname}=$qname;
	#$qname =~ s/_/::/g;
	#eval {
		#$self->do_search($c,$qname);
	#};
#	if ($@) {
#		$c->stash->{error}=$@->message;
#	}
}

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
