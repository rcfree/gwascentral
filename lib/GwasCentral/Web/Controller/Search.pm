# $Id: Search.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

  HGVbaseG2P::Web::Controller::Search - Catalyst Controller for the free-text website search

=head1 DESCRIPTION

  This class is a wrapper around the 'All' query in the HGVbaseG2P search API. All logic is dealt with by 
  these HGVbaseG2P::Search modules.

=head1 METHODS

TODO

=cut

package GwasCentral::Web::Controller::Search;

use strict;
use warnings;
use English;
use Data::Dumper;

use base 'Catalyst::Controller::GwasCentral';
use GwasCentral::Search::Query::All;
use GwasCentral::Search::Lookup;
use GwasCentral::DataSource::Util qw(DataSources_from_context);
use Carp qw(confess);


sub default : Regex('^search$') {
	my ( $self, $c ) = @_;
	$c->stash->{template} = 'search.tt2';
#	#my $current_query = $c->req->captures->[1] || 'studies';
#	#my $extra_params = {};
#	
#	#my $search=HGVbaseG2P::Search->new({conf_file=>$c->config});
#	#$search->context($c);
#	
#	#eval {
		$self->do_search($c,'All');
	#};
	#if ($@) {
	#	confess;
	#}
#		$c->stash->{search}=$results;
#		$c->stash->{requests} = $c->get_access_control->get_pending_request_counts;
#		$c->stash->{current_query}=$c->stash->{search}->{all}->current;
#		$c->session->{search_recog_as}=$c->stash->{search}->{all}->recognised_as;
#		$c->session->{search_query}=$c->req->param('q');
#		$c->session->{search_threshold}=$c->req->param('t') || '0';
#		$c->res->content_type('text/html');
#		if ($results->{all}->goto_report) {
#			$c->res->redirect($results->{all}->goto_report);
#		}
		
#	};
#	if ($@) {
#		$c->log->info("error:$@");
#		if ($@=~/No query/) {
#			$c->stash->{no_query}=1;
#			$c->stash->{search}->{all}=$search->new_query('All');
#		}
#		else {
#			$c->session->{error}=$@;
#		}
#	}
}

# deals with MeSH concept autocomplete and concept<->heading mapping
sub lookup :Regex('^search/lookup$') {
    my ( $self, $c ) = @_;
	my $value = $c->req->param('q');

  	my $lookup = GwasCentral::Search::Lookup->new({conf_file=>$c->config});
  	$lookup->populate_DS_from_context($c);
  	my @lookup_rows = $lookup->autocomplete({
  		rows => 10,
  		sorted => 1,
  		lookup_sources => ['markers','symbols', 'region', 'studies','ontology'],
  		value => $value,
  	});
  	
  	my $htmlstring = join("\n",@lookup_rows);
  	$c->res->content_type('text/html');
	$c->res->output($htmlstring);
}

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>
  
=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
