# $Id: SessionDB.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

GwasCentral::Web::Model::SessionDB - Catalyst session database module wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper for session database based on L<DBIx::Class> Model using schema L<GwasCentral::Schema::Session>

=cut

package GwasCentral::Web::Model::Session;

use strict;
use base 'Catalyst::Model::DBIC::Schema';
use GwasCentral::DataSource::Session;

my $config = GwasCentral::Web->config;
my $sds = GwasCentral::DataSource::Session->new({conf_file => $config});
__PACKAGE__->config({
	schema_class => "GwasCentral::Schema::Session",
	connect_info => [
		$sds->dsn, $sds->user, $sds->pass
	]
});

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
