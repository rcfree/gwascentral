# $Id: StudyDB.pm 1447 2010-05-26 14:25:42Z rcf8 $


=head1 NAME

GwasCentral::Web::Model::StudyDB - Catalyst Study database module wrapper class

=head1 SYNOPSIS

See L<GwasCentral::Web>

=head1 DESCRIPTION

Catalyst wrapper based on L<Catalyst::Model::Adaptor> Model for L<GwasCentral::Database::Study> database utility module using schema L<GwasCentral::Schema::Study>

=cut

package GwasCentral::Web::Model::Study;

use strict;
use base qw(Catalyst::Model::Adaptor);

use Data::Dumper qw(Dumper);

my $config = GwasCentral::Web->config;

__PACKAGE__->config( class => 'GwasCentral::DataSource::Study', args => { conf_file => $config } );

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
