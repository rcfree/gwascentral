# $Id: Update.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

  GwasCentral::Web::Update - Update limited markers and derived bins in Study and Browser databases.

=head1 SYNOPSIS


=head1 DESCRIPTION

  This module is used to by the Access Control system to update the available 'limited' markers in both the Study and Browser database.

=cut

package GwasCentral::Web::Update;

use Moose;
use Data::Dumper qw(Dumper);
use GwasCentral::Browser::Creator;
use YAML qw(Dump Bless);

extends 'GwasCentral::Base';

has 'study_db' => ('is'=>'rw');
has 'marker_db' => ('is'=>'rw');
has 'browser_db' => ('is'=>'rw');

sub recreate_limited_data_for_experiment {
	my ($self, $exp_identifier, $top_no) = @_;
	
	my $experiment = $self->study_db->get_experiment_by_identifier($exp_identifier);
	
	#update top X significances to state 'islimited'
	my @updated = ();
	
	foreach my $rs($experiment->resultsets) {
		$rs->significances->search({'islimited'=>'yes'})->update({'islimited'=>undef});
		$self->log->info("rs_islimited_count:".$rs->significances->search({'islimited'=>'yes'})->count);
		my $sigs_rs = $rs->significances->search({},{'order_by'=>'unadjustedpvalue ASC','page'=>1, rows=>$top_no});
		my @sigids = map { $_->significanceid } $sigs_rs->all;
		$rs->significances->search({significanceid=>{'in'=>\@sigids}})->update({islimited=>'yes'});
		
		push @updated, $rs->significances->search({'islimited'=>'yes'})->count;
	}
	
	#now update using creator module
	my $creator = GwasCentral::Browser::Creator->new({ 
		conf_file=>$self->config, 
		no_init=>1,
		study_db => $self->study_db,
		marker_db => $self->marker_db,
		browser_db => $self->browser_db
	});
	
	my @rsets = $experiment->resultsets;
	
	$creator->prepare_calc_genome;
	$creator->prepare_calc_region;
	
	foreach my $rset(@rsets) {
		my $exp = $rset->experiment;
		my $study = $exp->study;
		$creator->copy_marker_data( $study, $exp, $rset, 1 );
		$creator->browser->get_db->delete_marker_binned_ltd_chr_by_resultset(
						{ resultset => $rset->identifier, binsize => 3 } );
		$creator->genome_bins($rset,1);
		$creator->browser->get_db->delete_marker_binned_ltd_by_resultset(
						{ resultset => $rset->identifier, binsize => 1 } );
		$creator->region_bins($rset,1);	
	}
	$creator->delete_marker_lists(1,$creator->ltd_changed_markers);
	$creator->generate_marker_lists(1,$creator->ltd_changed_markers);
}



1;