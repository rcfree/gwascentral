# $Id: Output.pm 1447 2010-05-26 14:25:42Z rcf8 $

=head1 NAME

  GwasCentral::Web::Output - Output data structures to various formats

=head1 SYNOPSIS


=head1 DESCRIPTION

  This class is used to output data structures (e.g. hashified DBIx::Class objects) to different text formats for output

=cut

package GwasCentral::Web::Output;

use Moose;
use Data::Dumper qw(Dumper);
use YAML qw(Dump Bless);

extends 'GwasCentral::Base';

sub output_formats {
	return {
		'xml' => 'text/xml',
		'json' => 'text/json',
		'html' => 'text/html',
		'yaml' => 'text/yaml',
		'text' => 'text/plain',
	};
}

sub output_item {
	my ($self, $item, $item_name, $format) = @_;
	$self->log->info("item_name:$item_name");
	$self->log->info("format:$format");
	!$item and confess "Dying to meet you..";
	my $struct       = $item->to_hash;
	my $content_type = $self->output_formats->{$format};
	
	die GwasCentral::Exception->new({message=>ucfirst($item_name)." cannot be output as '$format'"}) if !$content_type;
	
	my $method = "_output_to_".$format;
	$self->log->info("output method:$method");
	
	my $output = $self->$method($struct,$item_name);
	return ($output,$content_type);
}

sub _output_to_xml {
	my ($self, $struct,$item_name) = @_;
	my $xml = new XML::Simple( NoAttr => 1, RootName => $item_name )
	  or die "unable to create XML::Simple";
	return $xml->XMLout($struct, XMLDecl=>1);
}

sub _output_to_json {
		my ($self, $struct) = @_;
	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
		$json_coder->space_after(1);
		my $json = $json_coder->encode($struct);
		$self->log->info("json:$json");
		return $json;
}

sub _output_to_yaml {
	my ($self, $struct) = @_;
	return YAML::Dump($struct);
}

sub _output_to_text {
		my ($self, $struct) = @_;
	my %fieldhash=();
		my %hash = ();
		my @data = ($struct);
		foreach my $field ( keys %{$struct} ) {
			my $value = $struct->{$field};
			if ( ref($value) eq 'ARRAY' ) {
				my $subcount = 1;
				foreach my $subfield ( @{$value} ) {
					if ( !$fieldhash{ $field . $subcount } ) {
						$fieldhash{ $field . $subcount } = 1;
					}

					my $subvalue;
					if ( ref($subfield) eq 'HASH' ) {
						my @subvalues =
						  map { $_ . ": " . $subfield->{$_} } keys %{$subfield};
						$subvalue = join( ";", @subvalues );
					}
					else {
						$subvalue = $subfield;
					}
					$hash{ $field . $subcount } = $subvalue;

					$subcount++;
				}
			}
			else {
				$fieldhash{$field} = 1;
				$hash{$field}      = $value;
			}
		}
		my $content = join( ",", sort keys %fieldhash ) . "\n";
		foreach my $datum (@data) {
			my @line = ();
			foreach my $field ( sort keys %fieldhash ) {
				push @line, $datum->{$field};
			}
			$content .= "\"" . join( "\",\"", @line ) . "\"\n";
		}
		return $content;
}


=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
1;