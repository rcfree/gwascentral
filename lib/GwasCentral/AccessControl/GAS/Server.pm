# $Id: GAS.pm 1526 2010-09-16 13:48:27Z rcf8 $

=head1 NAME

HGVbaseG2P::DataAccess::GAS - Web service access to Generic Access control Systemd


=head1 SYNOPSIS

TODO


=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::AccessControl::GAS::Server;
use Moose;
extends qw(GwasCentral::Base);
has 'ua'           => ( 'is' => 'rw', 'builder' => '_setup_ua' );
has 'content_type' => ( 'is' => 'rw', 'default' => 'text/xml' );
has 'identity'     => ( 'is' => 'rw', 'default' => 'anon' );
has 'user_level'   => ( 'is' => 'rw', 'default' => 'none' );
has 'anon_level'   => ( 'is' => 'rw', 'default' => 'none' );
has 'host' => ('is'=>'rw');
no Moose;

use Carp qw(confess);
use LWP::UserAgent;
use XML::Simple;
use Data::Dumper qw(Dumper);

sub _setup_ua {
	my $lwp = LWP::UserAgent->new;
	$lwp->cookie_jar( {} );
	return $lwp;
}


sub get_access {
	my ( $self, $uri ) = @_;
	my $access =
	  $self->get( "access", { user => $self->identity, uri => $uri } );
	my $full_access = $self->config->{GAS}->{full_access};
	$full_access and $access->{access_level} = $full_access;
	return $access;
}

sub get_resource {
	my ( $self, $uri ) = @_;
	return $self->get( "resource", { user => $self->identity, uri => $uri } );
}

sub get_resource_levels {
	my ( $self, $uri ) = @_;
	my $rsrc = $self->get_resource($uri);
	$self->log->info( "rsrc:" . Dumper($rsrc) );
}

sub get_access_level {
	my ( $self, $uri ) = @_;
	my $acc         = $self->get_access($uri);
	my $full_access = $self->config->{GAS}->{full_access};
	return $full_access ? 'full' : $acc->{access_level};
}

sub set_access_level {
	my ( $self, $uri, $level, $user_id, $response, $identity ) = @_;
	
	return $self->put(
		"access",
		{
			user     => $self->identity,
			user_id  => $user_id,
			uri      => $uri,
			level    => $level,
			response => $response,
			identity => $identity
		}
	);
}

sub delete_access {
	my ( $self, $uri, $user_id, $identity ) = @_;
	$self->log->info("delete access");
	return $self->delete(
		"access",
		{
			user     => $self->identity,
			user_id  => $user_id,
			uri      => $uri,
			identity => $identity
		}
	);
}

sub set_anon_access_level {
	my ( $self, $uri, $level ) = @_;
	return $self->set_access_level( $uri, $level, 'anon' );
}

sub get_admin_count {
	my ( $self, $query ) = @_;
	return $self->get( "admin",
		{ user => $self->identity, count => 1, query => $query } )->{count};
}

sub get_admin {
	my ( $self, $page, $page_size, $query, $uri_list ) = @_;
	$self->log->info( "uri_list:" . Dumper($uri_list) );
	my $data = $self->get(
		"admin",
		{
			user      => $self->identity,
			page      => $page || 1,
			page_size => $page_size || 20,
			query     => $query,
			uri       => $uri_list
		}
	);

	#$self->log->info("get_admin:".Dumper($data));
	return ref( $data->{resource} ) eq 'ARRAY'
	  ? $data->{resource}
	  : [ $data->{resource} ];
}

sub set_user_access_level {
	my ( $self, $uri, $level ) = @_;
	return $self->set_access_level( $uri, $level, 'user' );
}

sub login_anon {
	my ($self) = @_;
	return $self->login('anon');
}

sub login {
	my ( $self, $identity ) = @_;
	if ( $self->config->{no_access_control} ) {
		$self->identity('anon');
		return undef;
	}
	my $user = $self->get_user( $identity || $self->identity );

	#catch exception - if none then set identity and return result
	#else pass exception up chain
	eval {
		$self->post( "login", { identity => $identity || $self->identity } );
	};
	if ($@) {
		$self->throw( "Unable to login '"
			  . $identity
			  . "' as user to <br/>GAS server at '"
			  . $self->host
			  . "' <br/>Is it running? Error:".$@ );
	}
	$self->identity($identity);
	return $user;
}

sub create_user {
	my ( $self, $identity, $name ) = @_;
	return $self->put( "user",
		{ identity => $identity, nickname => $name, user => $self->identity } );
}

sub create_resource {
	my ( $self, $uri, $anon_level, $user_level ) = @_;
	$self->put(
		"resource",
		{
			uri        => $uri,
			anon_level => $anon_level || $self->anon_level,
			user_level => $user_level || $self->user_level,
			user       => $self->identity
		}
	);
}

sub delete_resource {
	my ( $self, $uri ) = @_;
	$self->delete( "resource", { uri => $uri, user => $self->identity } );
}

sub get_user {
	my ( $self, $identity ) = @_;
	$self->get( "user", { identity => $identity, user => $self->identity } );
}

sub get_user_by_id {
	my ( $self, $id ) = @_;
	$self->get( "user", { user_id => $id, user => $self->identity } );
}

sub get_all_users {
	my ( $self, $identity, $name ) = @_;
	$self->get( "user", { all => 1, user => $self->identity } )->{user};
}

sub delete_user {
	my ( $self, $identity ) = @_;
	$self->delete( "user", { identity => $identity, user => $self->identity } );
}

sub update_user {
	my ( $self, $identity, $name ) = @_;
	$self->post( "user",
		{ identity => $identity, name => $name, user => $self->identity } );
}

sub get {
	my ( $self, $uri, $params ) = @_;
	$self->request( 'GET', $uri, $params );
}

sub make_access_request {
	my ( $self, $uri ) = @_;
	my $reqs =
	  $self->put( "request", { uri => $uri, user => $self->identity } );
	return $reqs->{requests};
}

sub get_pending_requests {
	my ( $self, $uri ) = @_;
	my $reqs =
	  $self->get( "request", { user => $self->identity, uri => $uri } );
	return $reqs->{requests};
}

sub put {
	my ( $self, $uri, $params ) = @_;
	$self->request( 'PUT', $uri, $params );
}

sub post {
	my ( $self, $uri, $params ) = @_;
	$self->request( 'POST', $uri, $params );
}

sub delete {
	my ( $self, $uri, $params ) = @_;
	$self->request( 'DELETE', $uri, $params );
}

sub request {
	my ( $self, $action, $uri, $params ) = @_;
	shift;

	my @opts = ();
	if ($params) {
		foreach my $id ( keys %{$params} ) {
			my $value = $params->{$id};
			if ( ref($value) eq 'ARRAY' ) {
				foreach ( @{$value} ) {
					$_ and push @opts, $id . "=" . $_;
				}
			}
			else {
				$value and push @opts, $id . "=" . $value;
			}
		}
	}

	#my @opts = $params ? map { $_."=".$params->{$_} } keys %{$params} : ();
	my $param_list = join( "&", @opts );

	my $fulluri = $self->host . "/$uri";

	#$self->log->info("fulluri:$fulluri");

	$fulluri .= "?$param_list";

	my $req = HTTP::Request->new( $action => $fulluri );

	if ( $action eq 'PUT' ) {
		my %opts2;
		foreach ( keys %{$params} ) {
			$opts2{$_} = $params->{$_};
		}
		my $xml = XMLout( \%opts2, NoAttr => 1, RootName => 'data' );
		$self->log->debug("xml:$xml");
		$req->content($xml);
		$self->log->debug( "req content:" . $req->content );
	}

	$req->content_type( $self->content_type );

	$self->log->debug("$action $fulluri");

	#eval {
	my ($content,$res);
	eval {
			$res = $self->ua->request($req);
			#$self->log->info(" res:".Dumper($res));
			#$self->log->info( "res content:" . $res->content );
			$content = XMLin( $res->content );
	};
	if ($@) {
		$self->throw("Unable to get /$uri <br/> from GAS server ".$self->host."Error:$@");
	}
		
	if ( $res->is_success ) {
		return $content->{data};
	}
	else {
		$self->throw( $content->{data}->{error} || 'unknown' );
	}
}
1;
