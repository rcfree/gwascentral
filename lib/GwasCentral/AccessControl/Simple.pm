package GwasCentral::AccessControl::Simple;
use Moose;
extends qw(GwasCentral::AccessControl::Base);
has 'identity' => ('is'=>'rw', 'default' => 'public', 'trigger' => \&_set_user_and_access_level);

use Data::Dumper qw(Dumper);
use MIME::Base64;
use Storable qw/nfreeze thaw/;
use Digest::MD5 qw(md5_hex);
use Carp qw(cluck);

no Moose;

sub _set_user_and_access_level {
	my ( $self, $identity ) = @_;
	$self->identity_hash(md5_hex($identity));
	return;
}

sub get_fallback {
	my ($self, $study, $study_level,) = @_;
	return $study_level if $study_level;
	return { accesslevel => $study->isdisplayed eq 'yes' ? 'full' : 'none' };
}

sub get_study_access_levels {
	my ( $self, $studies ) = @_;
	my $local_connect = $self->Connect->local_instance;
	my %levels;
	foreach my $study(@{$studies}) {
		if (!$study) { $self->throw('A study was not found. You may need to rebuild Xapian indices','AccessControl') };
		my $resource = $local_connect->{label}.".study.".$study->identifier;
		my $fallback = $self->get_fallback($study,undef);
		$levels{$study->identifier}=$self->Connect->db->get_access($resource, $self->identity_hash,$fallback);
	}

	return %levels;
}

sub get_studies_with_admin_level {
	my ( $self, $page, $page_size, $query, $uri_list ) = @_;
	my $al_rs = $self->Connect->db->dbh->resultset('Access')->search({'accesslevel'=>'admin'},{page=>$page, rows => 50});
	
	return {
		'data' => [$al_rs->all],
		'count' => $al_rs->count
	};
}

sub get_phenotype_and_experiment_access_levels {
	my ( $self, $phenotypes_ref, $server ) = @_;
	$self->log->info("phenotypes_ref:$phenotypes_ref");

	#get studies represented by phenotypes then get access levels for these
	my @studies = map { $_->studyid } @{$phenotypes_ref};
	
	return $self->get_study_and_experiment_access_levels(\@studies);
}

sub get_study_and_experiment_access_levels {
	my ( $self, $studies ) = @_;
	my $local_connect = $self->Connect->local_instance;
	
	my %study_levels_for = $self->get_study_access_levels($studies);
	
	my %combined_levels_for = ();
	foreach my $ident(keys %study_levels_for) {
		$combined_levels_for{$ident}=$study_levels_for{$ident};
	}
	
	foreach my $study ( @{$studies} ) {
		my %exp_levels = ();
		my $ident = $study->identifier;
		my @experiments = $study->experiments;
		my $fallback = $self->get_fallback($study,$study_levels_for{$ident});
		foreach my $exp(@experiments) {
			my $resource = $local_connect->{label}.".experiment.".$exp->identifier;
			$exp_levels{$exp->identifier} = $self->Connect->db->get_access($resource, $self->identity_hash, $fallback);
		}
		
		$combined_levels_for{$ident}->{experiments}     = \%exp_levels;
		$combined_levels_for{$ident}->{all_experiments} = $study_levels_for{$ident}->{accesslevel};
	}
	$self->log->debug("study_levels_for:".Dumper(\%combined_levels_for));
	return %combined_levels_for;
}

sub get_resultset_access_level {
	my ($self, $resultset) = @_;
	!$resultset and $self->throw("Undef resultset supplied to $self");
	my $study = $resultset->experimentid->studyid;
	my %study_levels_for = $self->get_study_access_levels([$study]);
	return $study_levels_for{$study->identifier}->{access_level};
}

sub extract_admin_access_studies {
	my ($self, $identifiers) = @_;
	#return ['HGVST156','HGVST200','HGVST300'];
	return $identifiers;
}


sub is_current_user_admin_of_any_studies {
	return 1;
}

sub store_levels {
	my ($self, $args) = @_;
	my $study = $args->{'study'};
	my $user = $args->{'user'};
	my $response = $args->{'response'};
	
	my $uribase = $self->config->{WebSite}->{gas_baseuri};
	
	#my $study = $self->study_db->get_study_by_identifier($study_ident);
	#$self->log->debug("study:$study_ident");
	
	my $study_uri = "$uribase/study/".$study->identifier;
	
	$self->create_resource_if_does_not_exist($study_uri);
	
	my @exps = $study->experiments;
	foreach my $exp(@exps) {
		
		my $experiment_uri = "$uribase/experiment/".$exp->identifier;
		
		$self->create_resource_if_does_not_exist($experiment_uri);
	}
	
	if ($user) {
		$self->log->debug("user:$user");
		my $slevel = $args->{'study_level'};
		$self->log->debug("set access level:$uribase/study/".$study->identifier ." to $slevel");
		$self->gas->set_access_level("$uribase/study/".$study->identifier, $slevel, $user, $response);
		my @exps = $study->experiments;
		
		foreach my $exp(@exps) {
			my $elevel = $args->{"user_".$exp->identifier};
			$self->log->debug("set access level:$uribase/experiment/".$exp->identifier ." to $elevel");
			$self->gas->set_access_level("$uribase/experiment/".$exp->identifier,$elevel,$user);
		}
	}
	else {
		my $sanon_level = $args->{'anon_study'};
		my $suser_level = $args->{'user_study'};

		$self->gas->set_access_level($study_uri, $sanon_level, undef, undef, "anon");
		$self->log->debug("set anon access level:$uribase/study/".$study->identifier ." to $sanon_level");
		$self->gas->set_access_level($study_uri, $suser_level, undef, undef, "user");
		$self->log->debug("set user access level:$uribase/study/".$study->identifier ." to $suser_level");
		
		foreach my $exp(@exps) {
			my $experiment_uri = "$uribase/experiment/".$exp->identifier;
			my $eanon_level = $args->{"anon_".$exp->identifier};
			my $euser_level = $args->{"user_".$exp->identifier};
			$self->gas->set_access_level($experiment_uri,$eanon_level,undef,undef,"anon");
			$self->log->debug("set anon access level:$experiment_uri to $eanon_level");
			$self->gas->set_access_level("$uribase/experiment/".$exp->identifier,$euser_level,undef,undef,"user");
			$self->log->debug("set user access level:$experiment_uri to $euser_level");
		}
	}
}

1;
