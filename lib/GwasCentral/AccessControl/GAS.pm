package GwasCentral::AccessControl::GAS;
use Moose;
use GwasCentral::AccessControl::GAS::Server;
extends qw(GwasCentral::AccessControl::Base);
has '_gas'=>('is'=>'rw');
has 'connect' => ('is'=>'rw');
has 'md5_identity' => ('is'=>'rw', 'trigger' => \&_login_user);
use Data::Dumper qw(Dumper);
use Digest::MD5 qw(md5_hex);

sub BUILD {
	my ($self) = @_;
	$self->_gas(GwasCentral::AccessControl::GAS::Server->new({conf_file => $self->config, host => $self->config->{GAS}->{host} }));
}
around 'identity' => sub {
	my ($orig, $self, $identity) = @_;

    #return $self->identity if !defined($identity);
    $identity and $identity =~ s/\/$//;
	$identity and $self->md5_identity(md5_hex($identity));
	$self->$orig($identity);
};

sub _login_user {
	my ($self, $new_val, $old_val) = @_;
	return if !$new_val;
	$self->log->info("log in as ".$new_val);
	my $user = $self->_gas->login($new_val);
	$self->user($user);
}

sub update_identity {
	my ($self, $new_val) = @_;
	my $identity = undef;
	if (defined($new_val)) {
		$identity = md5_hex($new_val);
		$self->md5_identity($identity);
	}

	if ($identity) {
		$self->access_state('private');
		$self->log->info("access state set to private for '$identity'");
	}
	else {
		$identity = $self->config->{GAS}->{public_user};
		$self->access_state('public');
		$self->log->info("access state set to public");
	}
	
#	eval {
#		my $user = $self->_gas->login($identity);
#		$self->user($user);
#	};
#	if ($@) {
#		$self->throw($@);
#	}
}

no Moose;

sub get_study_access_levels {
	my ( $self, $studies ) = @_;
	my $local_connect = $self->connect->local_instance;
	my %levels;
	$self->log->info("user:".$self->md5_identity);
	foreach my $study(@{$studies}) {
		if (!$study) { $self->throw('A study was not found. You may need to rebuild Xapian indices') }
		my $id = ref($study) eq  'HASH' ? $study->{identifier} : $study->identifier;
		eval {
			$self->log->info("getting access from gas for:".$local_connect->{label}.".study.$id");
			my $sa_levels = $self->_gas->get_access($local_connect->{label}.".study.$id");
			#$self->log->info("sa_levels for $id:".Dumper($sa_levels));
			$levels{$id} = $sa_levels;
		};
		if ($@) {
			$self->log->info("unable to get from gas ($@), so using isdisplayed value instead");
			$levels{$id} = {access_level=>($study->{isdisplayed} eq 'yes' ? 'full' : 'none')};
		}
	}

	return %levels;
}


sub get_phenotype_and_experiment_access_levels {
	my ( $self, $phenotypes ) = @_;
	
	my $gas = $self->_gas;
	
	#get studies represented by phenotypes then get access levels for these
	my @studies = map { $_->studyid } @{$phenotypes};
	
	return $self->get_study_and_experiment_access_levels(\@studies);
}

sub get_study_and_experiment_access_levels {
	my ( $self, $studies ) = @_;
	
	my $gas = $self->_gas;
	
	my %study_levels = $self->get_study_access_levels($studies);
	
	my %levels = ();
	foreach my $slevel(keys %study_levels) {
		$levels{$slevel}->{study}=$study_levels{$slevel}->{access_level};
		$levels{$slevel}->{response}=$study_levels{$slevel}->{response};
	}
	
	foreach my $study ( @{$studies} ) {
		my $id = $study->identifier;
		my ($experiments, $exps_level) = $self->get_experiment_access_levels_from_parent( $study, $study_levels{$id} );
		$self->log->info("exps:".Dumper($experiments));
		$levels{$id}->{experiments}     = $experiments;
		$levels{$id}->{all_experiments} = $exps_level;
	}
	#$self->log->info("levels:".Dumper(\%levels));
	return %levels;
}

sub get_experiment_access_levels_from_parent {
	my ( $self, $parent, $parent_level ) = @_;
	
	#get all experiments for a study, and a count
	my @exps = $parent->experiments;
	my %exp_levels = $self->get_experiment_access_levels(\@exps,$parent_level);
	
	#if study_level is none set experiment levels to 'none'
	if ( $parent_level and $parent_level eq 'none' ) {
		%exp_levels = map { $_ => $parent_level } keys %exp_levels;
	}

	my $exp_count = scalar( keys %exp_levels );

	#loop through each and count the number for each type of access level
	my %lcounts = ( none => 0, request => 0, full => 0, hidden => 0, limited => 0, admin => 0 );
	foreach my $e_level ( values %exp_levels ) {
		$lcounts{$e_level}++;
	}

	my $overall_level = 'full';

#depending on the number of experiments at each access level set the overall level
	if ( $lcounts{'none'} == $exp_count ) {
		$overall_level = 'none';
	}
	elsif ( $lcounts{'request'} > 0 && $lcounts{'full'} == 0 && $lcounts{'admin'} == 0) {
		$overall_level = 'request';
	}

	return ( \%exp_levels, $overall_level );
}

sub get_experiment_access_levels {
	my ( $self, $experiments, $parent_level ) = @_;

	my %exp_levels = ();
	
	foreach my $exp(@{$experiments}) {
		if ($exp eq 'UPLOAD') {
			$exp_levels{'UPLOAD'}='full';
		}
		else {
			my $id = $exp->identifier;
			eval {
				$exp_levels{$id} = $self->_gas->get_access_level($self->connect->local_instance->{label}.".experiment.$id");
			};
			if ($@) {
				$self->log->info("unable to get from gas ($@), so using parent_level instead");
				$exp_levels{$id} = $parent_level;
			}
		}
		
	
	}
	
	return %exp_levels;
}

sub get_studies_with_admin_level {
	my ( $self, $page, $page_size, $query, $uri_list ) = @_;
	$self->log->info("admins:".Dumper());
	return {
		'data' => $self->_gas->get_admin( $page, $page_size, $query, $uri_list ),
		'count' => $self->_gas->get_admin_count( $query ),
	};
}

sub create_resource_as_admin {
	my ($self, $uri) = @_;
	eval {
		$self->create_resource_if_does_not_exist($uri, "none", "none");
	};
	if ($@) {
		warn $@->message;
	}
	$self->log->info("setting resource as admin for ". $self->md5_identity);
	$self->_gas->set_access_level($uri, 'admin', undef, undef,$self->md5_identity);
}

#$uri, $level, $user_id, $response, $identity
sub create_resource_if_does_not_exist {
	my ($self, $uri)=@_;
	eval {
		$self->log->info("get resource $uri");
		$self->_gas->get_resource($uri);
	};
	if ($@ =~ /does not exist/) {
		$self->log->info("create resource $uri");
		$self->_gas->create_resource($uri, "none", "none",undef);
	}
	elsif ($@) {
		$self->throw($@);
	}
}

1;