package GwasCentral::AccessControl::None;
use Moose;
extends qw(GwasCentral::AccessControl::Base);

use Data::Dumper qw(Dumper);
use MIME::Base64;
use Storable qw/nfreeze thaw/;

no Moose;

sub _set_user_and_access_level {
}

sub get_study_access_levels {
	my ( $self, $studies ) = @_;
	
	my %levels;
	foreach my $study(@{$studies}) {
		if (!$study) { $self->throw('A study was not found. You may need to rebuild Xapian indices') };
		$levels{$study->identifier}={accesslevel=>($study->isdisplayed eq 'yes' ? 'full' : 'none')};
	}

	return %levels;
}

sub get_phenotype_and_experiment_access_levels {
	my ( $self, $phenotypes_ref, $server ) = @_;

	#get studies represented by phenotypes then get access levels for these
	my @studies = map { $_->studyid } @{$phenotypes_ref};
	
	return $self->get_study_and_experiment_access_levels(\@studies);
}

sub get_study_and_experiment_access_levels {
	my ( $self, $studies ) = @_;
		
	my %study_levels_for = $self->get_study_access_levels($studies);
	
	my %combined_levels_for = ();
	foreach my $ident(keys %study_levels_for) {
		$combined_levels_for{$ident}->{study}=$study_levels_for{$ident}->{accesslevel};
		$combined_levels_for{$ident}->{response}=$study_levels_for{$ident}->{response};
	}
	
	foreach my $study ( @{$studies} ) {
		my $exp_levels = {};
		my $all_exp_level = {};
		my $ident = $study->identifier;
#		my @experiments = $study->experiments->search({},{'columns'=>'identifier'});
#		$exp_levels = {map { $_->identifier => $study_levels_for{$ident}->{accesslevel} } @experiments };
#		
#		$combined_levels_for{$ident}->{experiments}     = $exp_levels;
		$combined_levels_for{$ident}->{all_experiments} = $study_levels_for{$ident}->{accesslevel};
	}
	return %combined_levels_for;
}

sub get_resultset_access_levels {
	my ($self, $resultset) = @_;
	!$resultset and $self->throw("Undef resultset supplied to $self");
	my $study = $resultset->experimentid->studyid;
	my %study_levels_for = $self->get_study_access_levels([$study]);
	return $study_levels_for{$study->identifier}->{accesslevel};
}

sub extract_admin_access_studies {
	my ($self, $identifiers) = @_;
	#return ['HGVST156','HGVST200','HGVST300'];
	return $identifiers;
}


sub is_current_user_admin_of_any_studies {
	return 1;
}

1;