# $Id: FileParser.pm 1516 2010-07-27 12:50:28Z rcf8 $

=head1 NAME

GwasCentral::FileParser - Generic input-file parser library, for XML and CSV files alike


=head1 SYNOPSIS

    use GwasCentral::FileParser;

    my $parser = GwasCentral::FileParser->new(log4perl_conf => 'log4perl.conf')

    # Parse character-delimited text table, handle each row with callback
    my $csv_handler = sub {
         my $lineref = shift;
         [do something with arrayref with fields in row]};
    $parser->parse_file(filename => 'test.csv', 
		       filetype => 'csv', 
		       handler  => $csv_handler)}

    # Parse XML-file and pass in handler object
    my $xml_handler = [instance of Data::Stag::BaseHandler or subclass];
    $parser->parse_file(filename => 'test.xml', 
		       filetype => 'xml', 
		       handler  => $xml_handler)}
    

=head1 DESCRIPTION

This class encapsulates the logic involved in setting up and running
parsers for various datafiles, most commonly tab or comma delimited text
tables or XML-files. The calling code only needs to pass in an appropriate
handler (callback or object) that knows how to deal with the data.
  Various classes in the GwasCentral:: hierarchy may want to inherit from
this class (see GwasCentral::Database for an example).

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::FileParser;

use Moose;
extends qw(GwasCentral::Base);

no Moose;

use English qw( -no_match_vars );


use Data::Dumper;
use Text::CSV_XS;
use Text::CSV::Encoded;
use IO::File;
use Data::Stag;
use FindBin;
use lib "$FindBin::Bin/../";
use GwasCentral::Base qw(open_infile);

sub has_config { undef };

=head2 parse_file

  Usage      : $parser->parse_file({filename => 'testfile.csv',
                                  format   => 'csv',
                                  handler  => sub {do something with row data},
                                  field_sep=> ",",
  });
  Purpose    : Parse file of the specific format and process with handler
  Returns    : nothing
  Arguments  : hashref with named arguments:
                 filename  => path to file to parse
                 format    => format of input file (xml or csv)
                 handler   => callback or object, depending on format
                 field_sep => field separator in CSV-file (optional, defaults to tab (\t)
  Throws     : 
  Status     : Public

=cut

sub parse_file {
    my ($self, $args) = @_;
	#$self->log->info("args:".Dumper(\@_));
    # Validate method arguments
    exists $args->{'filename'} || die "filename argument must be provided";
    exists $args->{'filetype'} || die "filetype argument must be provided";
    exists $args->{'handler'}  || die "handler argument must be provided";
    $args->{'field_sep'} ||= "\t"; # field-separator for CSV defaults to tab character
    
    # Open a filehandle to the named file
    $self->log->debug("Opening file $args->{filename}");
    my $fh = open_infile($args->{'filename'});

    if($args->{'filetype'} eq 'delimited') {
	$self->parse_delimited($args->{limit},$fh, $args->{field_sep}, $args->{handler});
    }
    elsif($args->{filetype} eq 'xml') {
	$self->parse_xml($fh, $args->{handler});
    }
    else {
	die "cannot handle file type $args->{filetype}";
    }
    return;
}

=head2 parse_delimited

  Usage      : $parser->parse_delimited(10,$file_handle,',',
                                  sub {do something with row data},
                                  );
  Purpose    : Parse delimited text file using the separator and process with handler
  Returns    : nothing
  Arguments  : Limit to number of lines;IO file handle;separator charactor;handler callbak
  Throws     : 
  Status     : Public

=cut

sub parse_delimited {
    my ($self, $limit, $fh, $sep, $handler_callback) = @_;
        
    # Set up the CVS parser
    my $csv = Text::CSV::Encoded->new ({ encoding  => "utf8",
    	sep_char => $sep,
     });

    # Pass each line in the input file to the handler    
    $self->log->info("Parsing delimited file with field-separator '$sep'");
    my $counter=0;
    while(my $line = $csv->getline($fh)) {
    	$self->log->info("line:".Dumper($line));
    	if ($limit && $counter > $limit) {
    		return 1;
    	}
	$handler_callback->($line);
	$counter++;
    }
    return undef;
}

sub parse_xml {
    my ($self, $fh, $handler_obj) = @_;

    # Set up XML-parsing framework and run
    $self->log->info("Parsing XML-file");
    Data::Stag->parse(-handler => $handler_obj, -fh => $fh, -format => 'xml');
}
1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Mummi <gthorisson@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Mummi <gthorisson@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: FileParser.pm 1516 2010-07-27 12:50:28Z rcf8 $ 

=cut

