# $Id: Util.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

HGVbaseG2P::Core - Collection of utility modules for the HGVbase-G2P system
Also provides log and config options to modules.

=head1 SYNOPSIS

    use HGVbaseG2P::Core qw(open_infile);

    # Get filehandle on regular or compressed files
    my $fh = open_infile('/tmp/foo/bar.txt');
    my $fh = open_infile('/tmp/foo/bar.txt.gz');
    
    # Load configuration file into hash
    my %config = load_config('./conf/hgvbase.conf');
    

=head1 DESCRIPTION

This module contains several utility modules that are used in several parts
of the HGVbase-G2P system, such as initializing the logging subsystem and
reading configuration file. No subroutines are exported by default. 

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Base;
use Moose;
use Data::Dumper qw(Dumper);
use GwasCentral::Exception;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use base qw(Exporter);
	
use Data::Dumper;
use Config::General;
use MIME::Lite;
use Log::Log4perl qw( :easy );
use IO::Uncompress::AnyUncompress qw(anyuncompress $AnyUncompressError);
use Carp qw(croak cluck confess carp);
use Data::Page;
use Data::GUID;
  
my $FROM = 'help@hgvbaseg2p.org';
my $SEND_TYPE = "smtp";
my $SEND_SERVER = "mailsend.le.ac.uk";
use Carp qw(confess cluck);

has 'log' => ( is => 'ro',lazy=>1,isa=>'Log::Log4perl::Logger', default => \&_default_log);
has 'config' => (is =>'rw',isa=>'Any', default=>\&_default_config);
has 'conf_file' => (is =>'rw', isa=>'Any');
has 'log4perl_conf' => (is =>'rw', isa=>'Any');
has 'stacktrace' => ('is'=>'rw', 'default'=>undef);

our @EXPORT_OK = qw( load_config
		  open_infile
		  open_outfile
		  open_appendfile
		  load_module
		  send_email
		  pluralize
		  get_field
		  set_field
		  split_number_list
		  new_pager
		  shorten_author_list
		);


no Moose;

sub _default_log {
	my ($self) = @_;
	
	my $log4perl_conf = $self->log4perl_conf || $self->config->{log4perl_conf};
	Log::Log4perl->init_once($log4perl_conf);
	#warn "log:".ref($self).", with conf:".($log4perl_conf || 'undef');
    my $log = get_logger( ref($self) );
	return $log;
}

sub _default_config {
	my ($self) = @_;
	!$self->conf_file and return {};
	my $conf_file = $self->conf_file;
	my %config = load_config( $conf_file );
	return \%config;
}

	
	
=head2 load_config

  Usage      : my %config = load_config('./conf/hgvbase.conf');
  Purpose    : Load configuration info from file into hash via Config::General
  Returns    : Multi-level hash 
  Arguments  : Path to the configuration file
  Throws     : N/A
  Status     : Public
  Comments   : See Config::General for details on the structure of config-files, 
               as well as conf/hgvbase.conf for the master HGVbase-G2P config.

=cut

	sub load_config {
		my ($config_file, $nointerp) = @_;
		#return hash if provided instead of config file
		if ( ref($config_file) eq 'HASH' ) {
			return %{$config_file};
		}
		
		-d $config_file and $config_file.="/main.conf";
		-f $config_file
		  or LOGCONFESS "Provided configfile-path $config_file is invalid";
		my $config = new Config::General(
			-ConfigFile      => $config_file,
			-InterPolateVars => ($nointerp ? 0 : 1),
			-IncludeDirectories => 1, 
			-IncludeRelative=>1, 
			-UseApacheInclude=>1 
		);
		return $config->getall();
	}


	
=head2 init_logger

  Usage      : $logger = init_logger('./conf/log4perl.conf', 'MyCategory');
               $logger = init_logger();
  Purpose    : Initialize a Log::Log4Perl logger, as part of a logging framework
               (via a configuration file), or a generic logger (no config file).
  Returns    : Log::Log4perl::Logger instance.
  Arguments  : Path to the configuration file (optional)
               Category for the logger (optional)
  Throws     : N/A
  Status     : Public

=cut
	
	sub init_logger {
		my ( $log4perl_config, $category ) = @_;
		warn "log4perl_config:".($log4perl_config || 'undef')." and category:".($category || 'undef');
  # Initalize logging framework if not already done, and get reference to logger
  
		if (Log::Log4perl->initialized) {
			warn "Logger already initialised";
		}
		else {
			warn "Init logger using using configuration in ".($log4perl_config || "undef");
		}
		my $logger = Log::Log4perl->get_logger($category);
		$logger->info(
	"Getting logger for ".($category || 'undef')
				);
		return $logger;
	}
	
#	sub init_logger {
#		my ( $log4perl_config, $category ) = @_;
#
#  # Initalize logging framework if not already done, and get reference to logger
#		my $logger;
#		if ( $log4perl_config && -f $log4perl_config && $category ) {
#			Log::Log4perl->init_once($log4perl_config);
#			$logger = Log::Log4perl->get_logger($category);
#			$logger->trace(
#"Logger initialized for $category, using configuration in $log4perl_config"
#			);
#		}
#		else {
#			Log::Log4perl->easy_init(
#				{
#					level  => $INFO,
#					layout => "%F{1} %L:%p> %m%n",
#					file   => 'STDERR'
#				}
#			);
#			$logger = Log::Log4perl->get_logger();
#			$logger->debug("Generic logger initialized");
#		}
#		return $logger;
#	}

=head2 open_infile

  Usage      : $fh = open_infile('test1.csv');
               $fh_gzipped = open_infile('test2.csv.gz') 
               print $fh "foobar\n";
  Purpose    : Open file for reading, decompressing on the fly if needed
  Returns    : IO::Handle compatible object
  Arguments  : Path to the file to open. Can be .gz, .zip or .bz2 which
               will prompt the usage of an appropriate decompression-module
  Throws     : N/A
  Status     : Public
  Comments   : Relies on IO::Uncompress::AnyUncompress for decompressing a 
               variety of compression formats 

=cut

	sub open_infile {
		my ($filename) = @_;
		#confess "Unable to open empty file" if length($filename)==0;
		# Decompress on the fly if appropriate and open IO handle to return
		my $fh;
		if ( $filename =~ /\.(gz|Z|zip)\Z/xms ) {
			$fh = IO::Uncompress::AnyUncompress->new($filename)
			  || die "Uncompress failed: $AnyUncompressError";
		}
		else {
			$fh = IO::File->new();
			$fh->open( $filename, '<' )
			  || die "can't open file '$filename': " . $!;
		}
		return $fh;
	}

=head2 open_outfile

  Usage      : $fh = open_outfile('testout.csv');
               $fh_gzipped = open_outfile('testout.csv.gz') 
               print $fh "foobar\n";
  Purpose    : Open file for writing, compressing on the fly if needed
  Returns    : IO::Handle compatible object
  Arguments  : Path to the file to open. Can be .gz, .zip or .bz2 which
               will prompt the usage of an appropriate compression-module
  Throws     : N/A
  Status     : Public
  Comments   : Relies on the IO::Compress::*  family of modules for compressing,
               so if you need to e.g. make a .bz2 file you need to have the
               IO::Compress::Bzip CPAN-module installed 

=cut

	sub open_outfile {
		my ($filename) = @_;

		# Set up handle to either plain file or compressed stream into file
		my $fh;
		my %compress_class_of = (
			gz  => 'Gzip',
			zip => 'Zip',
			Z   => 'Zip',
			bz2 => 'Bzip2'
		);
		if ( my ($suffix) = $filename =~ /\.((gz|Z|zip|bz)\d*)\Z/xms ) {
			my $compress_class = 'IO::Compress::' . $compress_class_of{$suffix};
			eval "require $compress_class";
			$fh = $compress_class->new($filename)
			  || die "Compress failed: $! $@ ($filename)";
		}
		else {
			$fh = IO::File->new();
			$fh->open( $filename, '>' )
			  || die "can't open file $filename: " . $!;
		}
		return $fh;
	}

	sub open_appendfile {
		my ($filename) = @_;

		# Set up handle to either plain file or compressed stream into file
		my $fh;
		my %compress_class_of = (
			gz  => 'Gzip',
			zip => 'Zip',
			Z   => 'Zip',
			bz2 => 'Bzip2'
		);
		if ( my ($suffix) = $filename =~ /\.((gz|Z|zip|bz)\d*)\Z/xms ) {
			my $compress_class = 'IO::Compress::' . $compress_class_of{$suffix};
			eval "require $compress_class";
			$fh = $compress_class->new($filename)
			  || die "Compress failed: $! $@";
		}
		else {
			$fh = IO::File->new();
			$fh->open( $filename, '>>' )
			  || die "can't open file $filename: " . $!;
		}
		return $fh;
	}

	sub determine_opts {
		my $self_or_option = shift;
		my $option;
		if (ref($self_or_option)) {
			$option = shift;
		}
		else {
			$option = $self_or_option;
			$self_or_option = undef;
		}
		return ($self_or_option, $option, @_);
	}
	sub load_module {
		my ($self, $module, $args) = determine_opts(@_);
		
		eval "require $module";
		if ($@) {
			throw("Unable to load '$module' in 'load_module'. Error:".$@);
		}
		my $mod;
		
		eval {
			if ($args) {
				$mod = $module->new($args);
			}
			else {
				$mod = $module->new;
			}
		};
		if ($@) {
			throw("Unable to access 'new' method for '$module' in 'load_module' (Error:".$@.")");
		}
		return $mod;
	}
	
	sub send_email {
		my ($to, $subject, $data)=@_;
		my $to_list = $to;
		my $from = 'help@hgvbaseg2p.org';
		
		if (ref($to) eq "ARRAY") {
			$to_list = join(";",@{$to});
		}

		my $msg = new MIME::Lite;
	
	   $msg->attr("content-type"         => "text/html");
	   $msg->attr("content-type.charset" => "latin1");
	   $msg->attr("content-type.name"    => "homepage.html");
	   $msg->build(
	       From     =>$FROM,
	       To       =>$to_list,
		   Encoding =>'quoted-printable',
	       Subject  =>$subject,
	       Data     =>$data
	   );
	
	   MIME::Lite->send($SEND_TYPE,$SEND_SERVER,Debug=>0);
	   #$msg->send;
	   INFO "email sent to: $to_list";
	   INFO "subject: $subject\n body:$data";
	}
	
	sub pluralize {
		my ($value,$singular,$plural) = @_;
		if (ref($value) eq 'ARRAY') {
			$value = scalar(@{$value});
		}
		elsif (ref($value) eq 'HASH') {
			$value=scalar(keys %{$value});
		}
		!$plural and $plural=$singular."s";
		return $value." ".$singular if $value == 1;
		return $value." ".$plural;
	}
	
	sub throw {
		my ($self, $message, $exception_type) = determine_opts(@_);
		warn "Error message:".$message;
		cluck "Error thrown in '".($self || 'undefined') ."' message:$message";
		if ($exception_type) {
			my $exception_mod = "GwasCentral::Exception::$exception_type";
			$exception_mod->throw(message=>$message);
		}
		else {
			GwasCentral::Exception->throw(message => $message);
		}
	}
	
	sub trim {
		my $string = shift;
		$string =~ s/^\s+//;
		$string =~ s/\s+$//;
		return $string;
		
	}
	
	sub split_number_list {
		my ($numbers,$prefix) = @_;
		my @blocks = split( ",", $numbers );
		my @ids = ();
	
		foreach my $block(@blocks) {
			my @range = split("-", $block);
			if (@range==0 || @range>2) {
				warn "Too many identifiers in range: '$block' - ignored";
				next;
			}
			if ($range[1] && $range[0]>$range[1]) {
				warn "To value greater than from value in range: '$block' - ignored";
				next;
			}
			@ids = @range==2 ? (@ids, $range[0]..$range[1]) : (@ids,$block);
		}
		return map { $prefix.$_ } @ids;
	}
	
	sub new_pager {
		my ($total, $page_size, $page) = @_;
		my $pager = Data::Page->new;
		$pager->total_entries($total);
		$pager->entries_per_page($page_size);
		$pager->current_page($page);
		return $pager;
	}
	
	
sub shorten_author_list {
	my ( $author_list, $max_length ) = @_;
	$max_length ||= 3;
	$author_list =~ s/\n//;

	#warn "start:".time;
	my @authors = split( ", ", $author_list );

	#warn "end:".time;
	if ( scalar(@authors) > $max_length ) {
		return
		  join( ", ", @authors[ 0 .. $max_length - 1 ] )
		  . '&nbsp;<i>et al.</i>';
	}
	else {
		return join( ", ", @authors );
	}
}

sub create_guid {
	my ($self,$name) = @_;
 	my $dg    = Data::GUID->from_string($name);
	return $dg->as_base64();
}

1;

=head1 SEE ALSO

L<HGVbaseG2P::FileParser>, L<HGVbaseG2P::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Mummi <gthorisson@gmail.com>
Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Util.pm 1560 2010-10-28 15:55:59Z rcf8 $ 

=cut

