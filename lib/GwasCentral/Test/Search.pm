package GwasCentral::Test::Search;
use Exporter;
use Test::More;
use Test::Exception;
#use strict;
#use warnings;
use GwasCentral::DataSource::Util qw( new_AccessControl);
use GwasCentral::Search::Util qw(new_Query);
use Log::Log4perl qw(:easy);
use Spreadsheet::ParseExcel;
use XML::RSS;
use JSON qw(decode_json);
use YAML;
use Data::Dumper qw(Dumper);
Log::Log4perl->easy_init($DEBUG);

use base qw(Exporter);	
our @EXPORT_OK = qw(test_query_results get_query test_query_content test_page_sizes);

sub test_query_results {
	my ($type_or_query, $params,$exp_vals) = @_;
	my $query = get_query($type_or_query, $params);
	$query->prepare_Filters;
	my $data;
	lives_ok { $data = $query->results } "$type_or_query Query does not die";
	INFO "pager:".Dumper($query->pager);
	is $query->pager->total_entries,$exp_vals->{total},"Expected number (".$exp_vals->{total}.") of results in $type_or_query query in total";
	my $page_count = $exp_vals->{total}<$query->page_size ? $exp_vals->{total} : $query->page_size;
	is scalar(@{$query->data}), $page_count, "Expected number of results on page 1";
	is $query->no_results,$exp_vals->{no_results},"No results flag is correct (".($exp_vals->{no_results} || "undef").")";
	is $query->no_display,$exp_vals->{no_display},"No display flag is correct (".($exp_vals->{no_display} || "undef").")";
	is $query->recognised_as, $exp_vals->{recognised_as},"Results recognised as ".$exp_vals->{recognised_as};
	return $query;
}

sub test_large_page_fails {
	my ($type_or_query) = @_;
	my $query = get_query($type_or_query, $params);
	$params->{page}=9999;
	$query->prepare_Filters;
	throws_ok { $data = $query->results } qr/only .+ pages available/, "$type Query throws correct exception";
}

sub test_page_sizes {
	my ($type_or_query,$params, $page_sizes) = @_;
	my $query = get_query($type_or_query, $params);
	foreach my $ps(@{$page_sizes}) {
		$query->params->{page_size}=$ps;
		lives_ok {
			$query->prepare_Filters;
			$data = $query->results;
		} "$query with page_size $ps";
	}
}

sub get_query {
	my ($type_or_query, $params) = @_;
	return $type_or_query if ref($type_or_query);

	my $ac = new_AccessControl('conf/main.conf');
	my $query = new_Query( $type_or_query, { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );
	$query->host("http://gwascentral.org");
	$query->params($params);
	return $query;
}

sub test_query_content {
	my ($type_or_query, $params,$formats) = @_;
	#my @paths = keys %{$template{Path}};
	
	my $query = get_query($type_or_query,$params);
	
#	test_unrecognised_content_type_fails($query);
#	
#	#my $query = get_query($type_or_query,$params);
#	test_unrecognised_format_fails($query);
#	
#	$query->content_type(undef);
#	
#	foreach my $format(keys %{$formats}) {
#		test_recognised_format_works($query,$format);
#	}
	
	foreach my $format(keys %{$formats}) {
		test_recognised_content_type_works($query,$formats->{$format},$format );
	}
}

sub test_unrecognised_content_type_fails {
	my ($q) = @_;
	
	$q->prepare_Filters;
	throws_ok {
		$q->content_type('text/nothing');
	} qr/text\/nothing not available/,"Unrecognised content type fails";
}

sub test_unrecognised_format_fails {
	my ($q) = @_;
	$q->params->{format}="nothing";
	throws_ok {
		$q->prepare_Filters;
	} qr/value 'nothing' is not in the available list/, "Unrecognised format failes";
}

sub test_recognised_content_type_works {
	my($q,$ct, $format) = @_;
	$q->content_type($ct);
	$q->prepare_Filters;
	isa_ok $q->Format,"GwasCentral::Search::Format::".$format;
	my $method = "test_".$format."_content";
	$method->($q->output);
}

sub test_recognised_format_works {
	my($q,$format) = @_;
	$q->params->{format}=$format;
	$q->prepare_Filters;
	isa_ok $q->Format,"GwasCentral::Search::Format::".$format;
	my $method = "test_".$format."_content";
	$method->($q->output);
}

sub test_xml_content {
	my ($content) = @_;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "XML content returned"
}

sub test_excel_content {
	my ($content) = @_;
	lives_ok { 
		my $parser   = Spreadsheet::ParseExcel->new();
    	$parser->parse(\$content);
	} "Excel content returned"
}

sub test_yaml_content {
	my ($content) = @_;
	lives_ok { 
		YAML::Load($content);
	} "YAML content returned";
}

sub test_json_content {
	my ($content) = @_;
	lives_ok { 
		decode_json($content);
	} "JSON content returned"
}

sub test_atom_content {
	my ($content) = @_;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "Atom feed content returned"
}

sub test_rss_content {
	my ($content) = @_;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "RSS feed content returned"
}

sub test_csv_content {
	my ($content) = @_;
	my @lines = split(/\n/,$content);
	my @line1 = split(",",$lines[0]);
	ok scalar(@lines)>1 && scalar(@line1)>1,"CSV content returned";
}

sub test_tsv_content {
	my ($content) = @_;
	my @lines = split(/\n/,$content);
	my @line1 = split(/\t/,$lines[0]);
	ok scalar(@lines)>1 && scalar(@line1)>1,"TSV content returned";
}

sub test_ssv_content {
	my ($content) = @_;
	my @lines = split(/\n/,$content);
	my @line1 = split(' ',$lines[0]);
	ok scalar(@lines)>1 && scalar(@line1)>1,"SSV content returned";
}
