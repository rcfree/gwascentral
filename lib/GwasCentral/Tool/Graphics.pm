# $Id$

=head1 NAME

HGVbaseG2P::Browser::Graphics - Graphics methods outside the GBrowse glyph system used by the Browser


=head1 SYNOPSIS

	  
=head1 DESCRIPTION

The Graphics module contains methods which generate GD objects which can be used to output PNG or other formats. At the moment provision is provided for odds ratio stem plot graphs

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Tool::Graphics;

	use strict;
	use warnings;

	# Use OIO inside-out object framework for our OO hierarachy
	use Moose;

	use English qw( -no_match_vars );
	use Data::Dumper;
	use Log::Log4perl qw(:easy);
	use GwasCentral::Browser::Util qw(poslog);
	use GD;
	
	Log::Log4perl->easy_init($DEBUG);
	
	BEGIN {
	    # Export no subroutines by default, only this list on demand
	    our @EXPORT_OK = qw(
				 odds_ratio
				 );
	}

	my $VERSION="1.00";

	my $end_val = 2;
	my $xsize = 150;
	my $ysize = 60;
	my $end_size = 10;
	my $mid_size = 20;

=head2 odds_ratio

	  Usage      : my $gd = odds_ratio({ lower_ci=>1.1, odds_ratio=>3.2, upper_ci=>4.5});
	  			   print OUTFILE, $gd->png; #output PNG to file
	  Purpose    : Generate graphical representation of odds ratio
	  Returns    : GD object containing stem plot for specified CIs and OR
	  Arguments  : A hashref containing:
	  				-lower_ci - lower 95% confidence interval
	  				-upper_ci - upper 95% confidence interval
	  				-odds_ratio - odd ratio
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

	sub odds_ratio {
	    my ($self, $attrs)=@_;
	    my $start_val = 0;
	
	    my $lower_ci = $attrs->{lowerci};
	    my $upper_ci = $attrs->{upperci};
	    my $odds_ratio = $attrs->{value};
	    my $xsize = $attrs->{xsize} || $xsize;
	    my $ysize = $attrs->{ysize} || $ysize;
		my $ycenter = $ysize/2;
	 	my $gd = GD::Image->new($xsize,$ysize);
	 	
	 	my $white = $gd->colorAllocate(255,255,255);
	 	my $black = $gd->colorAllocate(0,0,0);
	 	$gd->transparent($white);
	 	$gd->setStyle($black,$black,$black,$black,$white,$white); 
	 	$gd->line(get_scaled_x(1),0,get_scaled_x(1),$ysize,gdStyled);
	  	
	 	my $x_lower_ci=get_scaled_x($lower_ci);
	 	my $x_upper_ci=get_scaled_x($upper_ci);
	 	my $x_odds_ratio = get_scaled_x($odds_ratio);
		$gd->line($x_lower_ci,$ycenter-($end_size/2),$x_lower_ci,$ycenter+($end_size/2),$black);
		$gd->line($x_upper_ci,$ycenter-($end_size/2),$x_upper_ci,$ycenter+($end_size/2),$black);
		$gd->filledEllipse($x_odds_ratio,$ycenter,6,6,$black);
		#$gd->line($x_odds_ratio,$ycenter-($mid_size/2),$x_odds_ratio,$ycenter+($mid_size/2),$black);
		$gd->line($x_lower_ci,$ycenter,$x_upper_ci,$ycenter,$black);
	 	return $gd;
	}
	
=head2 get_scaled_x

	  Usage      : my $x = get_scaled_x(10);
	  Purpose    : Calculates x pixel position based on logged value and size of GD image and end value
	  Returns    : GD object containing stem plot for specified CIs and OR
	  Arguments  : A hashref containing:
	  				-lower_ci - lower 95% confidence interval
	  				-upper_ci - upper 95% confidence interval
	  				-odds_ratio - odds ratio
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

	sub get_scaled_x {
		my ($value) = @_;
		return ($xsize/$end_val)*(poslog($value)+1);
	}

		
1;



=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2008> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id$ 

=cut
