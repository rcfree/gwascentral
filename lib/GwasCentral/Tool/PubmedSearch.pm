# $Id: AllelesMatch.pm 1332 2009-10-19 11:14:21Z rob $

=head1 NAME

HGVbaseG2P::DataImport::Tools::PubmedSearch


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::PubmedSearch;

use strict;
use warnings;
use WWW::Search;
use Moose;
extends qw(GwasCentral::Base);
has 'pmid' => ( 'is' => 'rw' );
no Moose;
use List::Compare;
use HTML::Entities;
use Data::Dumper qw(Dumper);

use Exporter;
our @EXPORT = qw(pubmed_search);

sub BUILD {
	my ($self) = @_;

	my $pubmedlite = new WWW::Search('PubMedLite');
	$self->pmid($pubmedlite);

}

#perform the pubmed search
sub pubmed_search {
	my ( $self, $pmd ) = @_;

	#my $pmid   = '18445777';
	my $hoh;
	if ( defined($pmd) && ( $pmd ne "" ) ) {
		$self->log->info("performing PUBMED lookup for PMID:$pmd");
		$self->pmid->native_query($pmd);
		my $article = $self->pmid->next_result;

		#get the pubmed details of interest and squish into a hash
		my $doi            = $article->{'doi'};
		my $pubmed_id      = $article->{'pmid'};
		my $journal        = $article->{'journal'};
		my $journal_abbrev = $article->{'journal_abbreviation'};
		my $title          = $article->{'title'};
		my $year           = $article->{'year'};
		my $affiliation    = $article->{'affiliation'};
		my $month          = $article->{'month'};
		my $issue          = $article->{'issue'};
		my $page           = $article->{'page'};
		my $ab       = $article->{'abstract'};
		my $abstract = encode_entities($ab);
		my $text_url       = $article->{'text_url'};
		my $volume         = $article->{'volume'};
		my $author         = $article->{'author'};
		my $urls           = $article->{'urls'};
		my $detail;

		#filter the journal article stuff
		#Nature 2007 Jun 7;447(7145):661-78
		if (   ( defined($journal_abbrev) )
			&& ( defined($year) )
			&& ( defined($month) )
			&& ( defined($volume) )
			&& ( defined($issue) )
			&& ( defined($page) ) )
		{
			$detail = "$journal $year;$volume($issue):$page";
		}
		else {
			$detail = "$journal $year";
		}
		$hoh = {
			doi                  => $doi,
			pmid                 => $pubmed_id,
			journal              => $journal,
			journal_abbreviation => $journal_abbrev,
			title                => $title,
			year                 => $year,
			affliation           => $affiliation,
			month                => $month,
			issue                => $issue,
			page                 => $page,
			abstract             => $abstract,
			doi_url              => $text_url,
			volume               => $volume,
			author               => $author,
			detail               => $detail
		};

	}
	else {

#            $self->log->info("NO Pubmed recored for PMID:$pmid, unable to get pubmed details");

		$hoh = {
			doi                  => '',
			pmid                 => $pmd,
			journal              => '',
			journal_abbreviation => '',
			title                => '',
			year                 => '',
			affliation           => '',
			month                => '',
			issue                => '',
			page                 => '',
			abstract             => '',
			text_url             => '',
			volume               => '',
			author               => '',
			detail               => '',

		};
	}
	sleep 2;
	return ($hoh);

}
1;
