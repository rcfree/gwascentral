# $Id: Util.pm 1560 2010-10-28 15:55:59Z rcf8 $

=head1 NAME

HGVbaseG2P::Core - Collection of utility modules for the HGVbase-G2P system
Also provides log and config options to modules.

=head1 SYNOPSIS

    use HGVbaseG2P::Core qw(open_infile);

    # Get filehandle on regular or compressed files
    my $fh = open_infile('/tmp/foo/bar.txt');
    my $fh = open_infile('/tmp/foo/bar.txt.gz');
    
    # Load configuration file into hash
    my %config = load_config('./conf/hgvbase.conf');
    

=head1 DESCRIPTION

This module contains several utility modules that are used in several parts
of the HGVbase-G2P system, such as initializing the logging subsystem and
reading configuration file. No subroutines are exported by default. 

=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Tool::Gene;
use Moose;
use Data::Dumper qw(Dumper);
use GwasCentral::Exception;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use base qw(Exporter);
	

our @EXPORT_OK = qw( load_config
		 separate_features
		);

no Moose;

sub separate_features {
	my ($feat_ds, $features_ref, $is_indexed) = @_;
	my @features = @{$features_ref};
	
	my @feature_list = ();
	my @gene_list = ();
	my @gene_aliases = ();
	
	foreach my $feat(@features) {
		my $type = $feat->type;
		$type=~s/:.+//g;
		my $gene_name;
		my $id;
		if ($feat->has_tag('parent_id')) {
			my @parent_ids = $feat->get_tag_values('parent_id');
			if ($parent_ids[0]) {
				my @par_f = $feat_ds->search_id($parent_ids[0]);
				next if !$par_f[0];
				$gene_name = $par_f[0]->display_name;
				$id = $par_f[0]->primary_id;
				if ($par_f[0]->has_tag('Alias')) {
					my @aliases = $par_f[0]->get_tag_values('Alias');
					foreach my $alias(@aliases) {
						push @gene_aliases, [$id, $alias];
					}
				}
				
			}
	
		}
		else {
			$gene_name = $feat->display_name;
			$id = $feat->primary_id;
			if ($feat->has_tag('Alias')) {
				my @aliases = $feat->get_tag_values('Alias');
				foreach my $alias(@aliases) {
					push @gene_aliases, [$id, $alias];
				}
			}
		}
		if ($type eq 'mRNA') {
			push @gene_list, [$id, $feat->ref, $gene_name, $feat->start, $feat->stop ];
		}
		else {
			push @feature_list, [$id, $feat->ref, $feat->start, $feat->stop, $type ];
		}
	}
	if ($is_indexed) {
		my @transcript_info;
		my %keyed_features;
		foreach my $f(@feature_list) {
			!$keyed_features{$f->[0]} and $keyed_features{$f->[0]}=[];
			push @{$keyed_features{$f->[0]}}, {
				ref => $f->[1],
				start => $f->[2],
				stop => $f->[3],
				type => $f->[4]
			};
		}
				 
		foreach my $gene(@gene_list) {
			push @transcript_info, {
				hgnc_label => $gene->[2],
				ref => $gene->[1],
				start => $gene->[3],
				stop => $gene->[4],
				features => $keyed_features{$gene->[0]}
			};
		}
		return \@transcript_info;
	}
	else {
		return (\@feature_list, \@gene_list, \@gene_aliases);
	}
}
