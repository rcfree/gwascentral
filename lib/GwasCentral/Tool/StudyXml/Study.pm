# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::Study


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::Study;


    use strict;
    use warnings;

    use FindBin;

    use strict;
    use warnings;
    use Encode;

    use List::Compare;
use Moose;
   # extends qw(HGVbaseG2P::DataImport::Tool::GenerateStudyXml);
    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);

    #use Exporter;
    #our @EXPORT =qw(pubmed_search);



has 'pmid' => ('is'=>'rw', default=>sub{{}});
has 'core' => ('is'=>'rw', default=>sub{{}});



    sub study
    {
        my ($self) = @_;



        return stag_nodify( [ Study => [] ] );

    }

    sub studycitation
    {

        my ($self) = @_;

        return stag_nodify( [ StudyCitation => [] ] );

    }

    sub citation
    {

        my ( $self, $citation ) = @_;






        return
          stag_nodify(
                       [
                         Citation => [
                                       [ DOI       => $citation->{doi} ],
                                       [ PubmedID  => $citation->{pubmedid} ],
                                       [ Authors   => $citation->{authors} ],
                                       [ Title     => $citation->{title} ],
                                       [ Detail    => $citation->{detail} ],
                                       [ MeshTerms => $citation->{meshterms} ],
                         ]
                       ]
          );

    }

    sub studycrossref
    {
        my ( $self, $study_crossref ) = @_;

        return stag_nodify(
            [
               StudyCrossref => [
                   [
                      Crossref => [
                          [
                             Hotlink => [
                                 [
                                    HotlinkLabel =>
                                      $study_crossref->{hotlinklabel}
                                 ],
                                 [ UrlPrefix => $study_crossref->{urlprefix} ],
                                 [ UrlSuffix => $study_crossref->{urlsuffix} ],
                             ]
                          ]
                      ]
                   ]
                 ]

            ]
        );

    }

    sub crossref
    {

        my ($self) = @_;
        return stag_nodify( [ Crossref => [] ] );

    }

    sub urlid
    {

        my ($self) = @_;
        return stag_nodify( [ UrlID => [] ] );

    }


    sub studyhotlink
    {
        my ( $self, $data ) = @_;

        return stag_nodify(

            [
               StudyHotlink => [
                [Hotlink => [
                   [ HotlinkLabel  => $data->{hotlinklabel} ],
                   [ UrlPrefix     => $data->{urlprefix} ],
                   [ UrlSuffix => $data->{url_suffix} ],

               ]
]

               ]

               ]

        );

    }

    sub studyinfo
    {
        my ( $self, $study_data ) = @_;
        return stag_nodify(
            [
               StudyInfo => [
                   [ Identifier       => $study_data->{study_identifier} ],
                   [ StudyDesign      => $study_data->{study_design} ],
                   [ AccessionVersion => $study_data->{study_accession} ],
                   [ Background       => $study_data->{background} ],
                   [ StudySizeReason  => $study_data->{studysize_reason} ],
                   [ StudyPower       => $study_data->{studysize_power} ],
                   [ SourcesOfBias    => $study_data->{scource_of_bias} ],
                   [ Limitations      => $study_data->{limitations} ],
                   [ Title            => $study_data->{title} ],
                   [ StudyAbstract         => $study_data->{abstract} ],
                   [ Description      => $study_data->{description} ],
                   [ Name             => $study_data->{study_name} ],
                   [ IsDisplayed      => $study_data->{isdisplayed} ],
                   [ IsHidden      => $study_data->{ishidden} ],
                   [ Conclusions      => $study_data->{conclusions} ],
                   [ Acknowledgements => $study_data->{acknowledgements} ],
                   [ KeyResults       => $study_data->{keyresults} ],
                   [ Objectives       => $study_data->{objectives} ],

               ]
            ]
        );
    }

    sub submitted_instudy
    {

        my ( $self, $submitted ) = @_;

        return stag_nodify(

            [
               SubmittedInStudyID => [
                    [ Study => [ [ Identifier => $submitted ] ] ]
               ]
               ]
            );

        }


        sub submission
        {
            my ( $self, $submission ) = @_;



            return stag_nodify(
                [
                   Submission => [
                       [ Name        => $submission->{name} ],
                       [ Description => $submission->{description} ],

                   ]
                ]
            );
        }

        sub contribution
        {

            my ( $self, $contribution ) = @_;

            return stag_nodify(
                [
                   Contribution => [
                       [ IsSubmitter => $contribution->{submitter} ],
                       [ IsSource    => $contribution->{source} ],
                       [ IsAuthor    => $contribution->{author} ],

                   ]
                ]
            );
        }

        sub researcher
        {

            my ( $self, $researcher ) = @_;

            return stag_nodify(
                [
                   Researcher => [
                                  [ ShortName   => $researcher->{shortname} ],
                                  [ FullName    => $researcher->{fullname} ],
                                  [ Institution => $researcher->{institution} ],
                                  [ WWW         => $researcher->{www} ],
                                  #[ SubmitterHandle => $researcher->{handle} ],
                                  [ Department => $researcher->{department} ],
                                  [ Address    => $researcher->{address} ],
                                  [ Fax        => $researcher->{fax} ],
                                  [ Email      => $researcher->{email} ],
                   ]
                ]

            );

        }



    1;
