# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::Experiment


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::Experiment;


    use strict;
    use warnings;

    use FindBin;
    use lib "$FindBin::Bin/../../../";

    use strict;
    use warnings;
use Moose;
    use List::Compare;

    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);


#use Exporter;
#our @EXPORT =qw(pubmed_search);

has 'pmid' => ('is'=>'rw', default=>sub{{}});





sub experiment
    {
        my ($self) = @_;
        return stag_nodify( [ Experiment => [] ] );
    }


    sub experimentinfo
    {
        my ( $self, $data )= @_;


        return
          stag_nodify(
                       [
                          ExperimentInfo => [
                              [ Identifier        => $data->{'identifier'} ],
                              [ Name              => $data->{'name'} ],
                              [ ExperimentType              => $data->{'type'}],
                              [ Label             => $data->{'label'} ],
                              [ Description       => $data->{'description'}],
                              [ PhenotypeMethodID => $data->{'pm_macro'} ],
                              [IndividualDataStatement =>$data->{'statement'}],
                          ]
                       ]
          );
    }

sub experiment_assayedpanel{
    my ($self,$data)=@_;


return stag_nodify(
            [
               ExperimentAssayedpanel => [
                   [ AssayedpanelID => $data->{'ap_macroid'} ],

                   #   [ ExperimentID => $exp_id ],
               ]
            ]
        );


}

sub resultset
    {
        my ( $self, $data ) = @_;
        return
          stag_nodify(
                       [
                         Resultset => [
                                        [ Identifier  => $data->{'identifier'}],
                                        [ Name        => $data->{'name'}],
                                        [ Description => $data->{'description'} ],
                                        [ AnalysisMethodID   => $data->{'am_macro'}],
                                        [ ProtocolParameters => $data->{'params'}],


                         ]
                       ]
          );


    }


sub genotyping_platform
    {

        my ( $self, $data ) = @_;
        return
          stag_nodify(
                       [
                          GenotypedBundle => [
                                  [
                                    BundleLoci =>
                                      [ [ BundleName => $data->{bundle} ], ]
                                  ]
                          ]
                       ],
          );
    }

    sub experimenthotlink
    {
        my ( $self, $data ) = @_;

        return stag_nodify(

            [
               ExperimentHotlink => [

               [ Hotlink => [
                   [ HotlinkLabel  => $data->{hotlinklabel} ],
                   [ UrlPrefix     => $data->{urlprefix} ],
                   [ UrlSuffix => $data->{urlsuffix} ],

               ]
]

               ]

               ]

        );

    }




1;