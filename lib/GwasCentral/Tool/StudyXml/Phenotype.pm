# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::Phenotypes


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::Phenotype;


    use strict;
    use warnings;

    use FindBin;
    use lib "$FindBin::Bin/../../../";

    use strict;
    use warnings;

 use Moose;
    use List::Compare;

    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);




sub phenotypemethod
    {

        my ( $self, $phenotype_method )
          = @_;

        return stag_nodify(
            [
               PhenotypeMethod => [
                   [ '@' => [ [ id => $phenotype_method->{phenomethod_id} ] ] ],
                   [ Identifier   => $phenotype_method->{identifier} ],
                   [ Name         => $phenotype_method->{name}],
                   [ VariableType => $phenotype_method->{variabletype} ],
                   [ Description  => $phenotype_method->{description} ],
                   [ Sample  => $phenotype_method->{sample} ],
                   [ MeasuredAttribute  => $phenotype_method->{measured_attribute} ],
                   [ VariableType  => $phenotype_method->{variable_type} ],
                   [ Unit  => $phenotype_method->{unit} ],
                   [ Circumstance  => $phenotype_method->{circumstance} ],


               ]
            ]
        );
    }



    sub phenotypeproperty{

        my ( $self, $data ) = @_;
        return stag_nodify [
                    PhenotypeProperty => [ [ Name => $data->{name} ], ] ];

    }





    sub phenotypeannotation{


        my ( $self, $data ) = @_;
        return stag_nodify [PhenotypeAnnotation  =>
                     [ [ PhenotypeIdentifier => $data->{identifier} ],
                     [ ExactMatch=> $data->{exactmatch} ],
                     [ AnnotationOrigin=> $data->{annotation_origin} ]
                      ] ];

    }


sub pppa
    {

        my ($self) = @_;

        return stag_nodify( [ PPPA => [] ] );

    }


sub phenotypevalue {

    my ($self,$data)=@_;

 [
                                         PhenotypeValue => [
                                              [ PhenotypeMethodID   => $data->{'pm_macro'} ],
                                              [ Value               => $data->{'value'}],
                                              [ ValueIsMean         => $data->{'value_mean'}],
                                              [ NumberOfIndividuals => $data->{'total'} ],
                                              [ StdDev              => $data->{'std_dev'}],
                                              [ Median              =>$data->{'median'} ],
                                              [ Min                 => $data->{'min'}],
                                              [ Max                 => $data->{'max'}],
                                         ]
                                       ]





}



1;