# $Id$

=head1 NAME

HGVbaseG2P::DataImport::Tools::Samplepanel


=head1 SYNOPSIS

    my $pmid_search =


=head1 DESCRIPTION



=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::Tool::StudyXml::Samplepanel;


    use strict;
    use warnings;

    use FindBin;
    use lib "$FindBin::Bin/../../../";

    use strict;
    use warnings;

    # Use OIO inside-out object framework for our OO hierarachy
use Moose;
    use List::Compare;

    use Carp qw(cluck croak confess);
    use Data::Stag::XMLWriter;
    use Data::Stag qw( :all );
    use Data::Dumper qw(Dumper);

    #use Exporter;
    #our @EXPORT =qw(pubmed_search);

has 'pmid' => ('is'=>'rw', default=>sub{{}});




    sub studysamplepanel
    {

        my ($self) = @_;
        return stag_nodify( [ StudySamplepanel => [] ] );

    }


    sub samplepanel
    {
        my ( $self, $sp ) = @_;
        return stag_nodify(
            [
               Samplepanel => [
                   [ '@' => [ [ id => $sp->{id} ] ] ],
                   [ Identifier               => $sp->{identifier} ],
                   [SubmittedInStudyIdentifier => $sp->{study_identifier}],
                   [ Name                     => $sp->{name} ],
                   [ TotalNumberOfIndividuals => $sp->{total} ],
                   [ Label                    => $sp->{label} ],
                   [ Accession                => $sp->{accession} ],
                   [ AccessionVersion         => $sp->{accessionversion} ],
                   [ Description              => $sp->{description} ],
                   [ Composition              => $sp->{compostion} ],
                   [NumberOfSexMale => $sp->{ number_male} ],
                   [NumberOfSexFemale =>  $sp->{number_female} ],
                   [NumberOfSexUnknown =>$sp->{ number_unkown} ],
                   [ NumberOfProbands =>  $sp->{number_probands } ],
                   [ NumberOfParents =>  $sp->{number_parents} ],
                   [ModeOfRecruitment => $sp->{recruitment} ],
                   [DiagnosisAgeRange => $sp->{diagnosis_age} ],
                   [DiagnosisPeriod => $sp->{diagnosis_period}],
                  [ SamplingAgeRange => $sp->{sampling_age} ],
                  [ SamplingPeriod => $sp->{sampling_period} ],
                  [ PopulationInfo => $sp->{population_info} ],
                  [ GeographicRegionInfo =>$sp->{geographic_info}],
                  [ EthnicityInfo  => $sp->{ethinic_info} ],
                  [ BirthPlaceInfo => $sp->{birth_info} ],
                  [ AdmixtureInfo  => $sp->{admixture_info} ],
                  [EnvironmentInfo => $sp->{environment_info}],
                  [ DNAsArePooled => $sp->{dnas_pooled} ],
                  [ DNAsAreWGA    => $sp->{dnas_wga} ],


                               ]
                            ]

                         );

                   }#sub



 1;
