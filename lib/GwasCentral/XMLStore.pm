# $Id: XMLStore.pm 1584 2010-11-04 15:44:16Z rkh7 $

=head1 NAME

GwasCentral::XMLStore - Storing XML-data to a DBIx::DBStag database


=head1 SYNOPSIS

    use English qw( -no_match_vars );
    use GwasCentral::XMLStore;
    my $xmldb = GwasCentral::XMLStore->new(conf_file => 'path to master config');
    eval {
       $xmldb->store_file($xmlfile);
    };
    # catch exceptions
    if($EVAL_ERROR) {
        ...
    }


=head1 DESCRIPTION

This class implements a handler for a stream of XML-events which will attempt to
store them in a relational database. It will also generate those events via its
inherited parsing-capabilities.
  In addition to inherited generic DBStag functionality from Database.pm, this class has several
methods for handling XML-content specific to HGVbase-G2P. This mostly involves
<Study> and <Experiment> container-elements and ways of storing these bit-by-bit
and not all in one big chunk. See code for details.

NB Objects of this class can also do send regular non-DBStag queries to the
database, and so taking advantage of both the master configuration and the
shared SQL-templates in perl/sqltemplates/.

=head1 SUBROUTINES/METHODS

=cut

package GwasCentral::XMLStore;

use Moose;
extends qw(GwasCentral::Base);

has 'stats' => (is => 'rw'); # Place to put various stats in
has 'current_studyid' => (is => 'rw'); # Global ID of the Study currently being processed
has 'current_experimentid' => (is => 'rw'); # Global ID of the Experiment currently being processed
has 'store_error' => (is => 'rw'); #
has 'depth2store' => (is => 'rw'); #
has 'handler' => (is => 'rw'); #
has 'dsn' => (is =>'rw');
has 'user' => (is =>'rw');
has 'pass' => (is =>'rw');
has 'parser' => (is => 'rw');
has 'dbstag' => (is=>'rw');


no Moose;

use English qw( -no_match_vars );
use Time::HiRes qw( time );
use Number::Format qw(format_number);
use Digest::MD5 qw( md5_hex );
use Data::Dumper;
use FindBin;
use Cwd;
use Data::Stag qw( :all );
#use GwasCentral::DataSource::Study;
use GwasCentral::FileParser;
use Data::Stag::BaseHandler;
use DBIx::DBStag;

sub BUILD {
	my ($self, $args) = @_;
	my $config = $self->config;
	my $dbconfig = $config->{Database};
	my $sconfig = $dbconfig->{Study};
	my $dsn = $self->dsn || $sconfig->{dsn} || $dbconfig->{dsn};
	my $host = $sconfig->{host} || $dbconfig->{host};
	my $type = $sconfig->{type} || $dbconfig->{type} || 'mysql';
	if (!$dsn) {
		$dsn = "dbi:".$type.":".($sconfig->{name} || die "No database DSN or name provided as a dsn parameter or in configfile");
		$dsn.=($host and ":".$host);
	}
	
	my $dbuser = $self->user || $sconfig->{user} || $dbconfig->{user} || getlogin();
	my $dbpassw = $self->pass || $sconfig->{pass} || $dbconfig->{pass};
	$self->log->info("connecting to $dsn as $dbuser\n");
	
	my $stag = DBIx::DBStag->connect($dsn,$dbuser,$dbpassw,  {
                                   PrintError => 1, # warn() on errors
                                   RaiseError => 1, # do die on error
                                   AutoCommit => 0, # does not commit immediately

                               });
	$self->dbstag($stag);

	# Inherit from foreign (non-OIO) objects, special tricks needed here to
    # install the proper SAX-even handler callbacks :!
    $self->log->info("creates basehandler");
    my $handler = Data::Stag::BaseHandler->new();
    $handler->trap_h({Allele         => sub { $self->handle_allele(@_)         },
		      StudyInfo      => sub { $self->handle_studyinfo(@_)      },
		      Experiment     => sub { $self->handle_experiment(@_)     },
		      ExperimentInfo => sub { $self->handle_experimentinfo(@_) },
		  });
    *Data::Stag::BaseHandler::catch_end = sub {$self->catch_end(@_)};
    #$handler->catch_end_sub( sub {$self->catch_end(@_)} );
    $self->handler($handler);
    $self->log->info("inherits basehandler");
#
    # Set mapconf, so DBIx::DBStag knows about some cryptic FK-relations. See
    # http://search.cpan.org/~cmungall/DBIx-DBStag-0.09/DBIx/DBStag.pm#mapping
    my $stagmap_file;
    if($self->config->{dbstag_meta}) {
        $stagmap_file  = "$FindBin::Bin/../".$self->config->{dbstag_meta};
    }
    elsif($args->{stagmap}) {
        $stagmap_file = $args->{stagmap};
    }
    if($stagmap_file) {
        -f $stagmap_file or  $self->log->logcroak("Provided dbstag_meta file $stagmap_file is not valid");
        $self->log->info("Setting stagmap-file to $stagmap_file");
        $self->data_source->mapconf($stagmap_file);
    }

    # Set the depth at which want to start storing stuff from the XML. Nomally
    # this is 1, but when things are nested under <Study> or <Experiment>, this
    # needs special care (see below)
    $self->depth2store(1);
    $self->log->info("set depth to store");

    my $parser = GwasCentral::FileParser->new({conf_file=>$self->config});
    $self->parser($parser);
}

=head2 store_file

  Usage      : $xmldb->store_file('markers.xml');
  Purpose    : Store data in XML-file to compatible relational database
  Returns    : N/A
  Arguments  : Path to the XML-file to parse
  Throws     : Dies on errors
  Status     : Public
  Comments   : Storing will only be attempted for XML-elements which
               match the database tables and FK-relations between them.

=cut

sub store_file {
    my ($self, $filename) = @_;

    # Parse file and handle the XML-stream with self
    #$self->log->debug("Storing XML-file $filename");
    my $stime = time();

    # ensure current study ID and experiment ID are reset for each initialisation (important for tests which use one instance of XMLStore
	$self->current_studyid(undef);
	$self->current_experimentid(undef);

    $self->log->info("store file");
    $self->stats({}); # reset stats
    $self->parser->parse_file({filename => $filename,
		     filetype => 'xml',
		     handler  => $self->handler});

    # Print out some stats
    my $totaltime = time() - $stime;
    $self->stats->{totaltime} = $totaltime;
    $self->stats->{'Performance'} = format_number(($self->stats->{totalstored}||0)/$totaltime, 1).' nodes/s';
    $self->log->warn("STATS:\n",Dumper($self->stats),"\n");

    # Die if any store-errors were encountered
    if($self->store_error()) {
	$self->log->logdie("Got error(s) during load, so throwing exception right here");
    }

    return 1; # Or else return true if everything went smoothly
}


# Internal methods for handling project-specific XML-elements

sub handle_studyinfo {
    my ($self, $handler, $node) = @_;
    $self->log->info("storing studyinfo");

    # Create a new Study node with the metadata
    my $study_node = stag_nodify([Study => []]);

    foreach my $n($node->children()) {
	$study_node->addchild($n);
    }

    $self->current_studyid($node->get('Identifier')) || die "Need Identifier column for the Study container";
    $study_node->set(q{@} => [[id=>$self->current_studyid]]);
    $self->log->info("Storing new Study node w/ id=".$self->current_studyid.", currently at depth ".$handler->depth);

    # !!TODO: need exception handling here, like in catch_end
    $self->dbstag->storenode($study_node);
    $self->stats->{stored}->{'Study'}++;
    $self->depth2store($handler->depth);
    return;
}

sub handle_experiment  {
    my ($self, $handler, $node) = @_;
    $self->log->info("in handle_experiment(), depth=".$handler->depth.", decreasing depth coz we're closing <Experiment>");
    $self->depth2store($handler->depth);
    $self->current_experimentid(undef);
    if(my $atts = $node->get(q{@})) {
	$node->set('StudyID', $self->current_studyid);
	$self->log->debug("Got experiment for lookup, ",$node->itext);
	# !!TODO: need exception handling here, like in catch_end
	$self->dbstag->storenode($node) || $self->log->logconfess($EVAL_ERROR);
    }
    return;
}

sub handle_experimentinfo {
    my ($self, $handler, $node) = @_;

    # Create new Experiment node
    my $exp_node = stag_nodify([Experiment => [[StudyID => $self->current_studyid]]]);
    foreach my $n($node->children()) {
	$exp_node->addchild($n);
    }
    $self->current_experimentid('exp_global');
    $exp_node->set(q{@} => [[id=>$self->current_experimentid]]);
    $self->log->debug("Storing new Experiment node w/ id=$self->current_experimentid, currently at depth ".$handler->depth);
    # !!TODO: need exception handling here, like in catch_end
    $self->dbstag->storenode($exp_node, {assigned_node_h=>{'Type'=>1}});
    $self->stats->{stored}->{'Experiment'}++;
    $self->depth2store($handler->depth);
    return;
}

## Other db-specific handling of elements being imported

# Create digest from allele-seq before storing
sub handle_allele  {
    my ($self, $handler, $node) = @_;
    my $aseq_digest = md5_hex($node->get('AlleleSeq'));
    $self->log->is_debug and
	#$self->log->debug("Creating digest $aseq_digest from allele '",$node->get('AlleleSeq'),"'");
    $node->add('AlleleSeqDigest', $aseq_digest);
    return $node;
}

# Handler for any elements not specifically handled above. Basically just store
# the node in the database if it's at a certain level in the tree and DBIx::DBStag
# will take care of the rest
sub catch_end  {
    my ($self, $handler, $event, $node) = @_;

    $self->log->debug("checking if I should store element '$event' at depth ".$handler->depth);
    if ($event !~ /\A(Experiment|ExperimentInfo|StudyInfo|Study)\Z/xms && $handler->depth == $self->depth2store) {
	$self->stats->{total2store}++;
	#$self->log->debug("Current depth is ",$self->depth, ", depth2store=".$self->depth2store.", storing node #$stats{total2store} <".$node->name.'>');
	    $self->stats->{total2store} % 100 == 0 and $self->log->info("Total $self->stats->{total2store} nodes stored");

	# Switch statement, sort of, to figure out which global ID to set before storing
	defined($self->current_experimentid)  ? $node->set('ExperimentID', $self->current_experimentid)
      : defined($self->current_studyid)       ? $node->set('StudyID', $self->current_studyid)
      : undef
      ;

	eval {
	    $self->dbstag->storenode($node);
	};
	if($EVAL_ERROR) {
	    $self->log->error("could not store node #hthj$self->stats{total2store}: $@\n", $node->xml());
	    $self->stats->{errorstore}++;
	    $self->store_error(1);
 	}
	else {
	    $self->stats->{totalstored}++;
	    $self->stats->{stored}->{$event}++;
	}
	return;
    }
    else {return $node;}
}

1;

=head1 SEE ALSO

L<GwasCentral::FileParser>, L<GwasCentral::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Mummi <gthorisson@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Mummi <gthorisson@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: XMLStore.pm 1584 2010-11-04 15:44:16Z rkh7 $

=cut

