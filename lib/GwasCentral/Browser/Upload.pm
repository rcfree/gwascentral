# $Id: Creator.pm 1463 2010-06-07 10:04:16Z rcf8 $

=head1 NAME

GwasCentral::Browser::Upload - Upload user data to Browser database and generate binned data/result set marker lists from Study/Marker database


=head1 SYNOPSIS

    my $browser_upload = GwasCentral::Browser::Upload->new({ conf_file => $c->config });
	$browser_upload->upload_data($c->session->{upload_data});
	$browser_upload->browser_db($c->model('BrowserDB'));
	eval {
		my @rs_idents = $browser_upload->do_upload({
			file =>'/tmp/wfdfgdfg', 
			study_name=>'My study', 
			resultset_name =>'My resultset', 
			sessionid => $c->sessionid
		});
	};
	if ($@) {
		$c->stash->{'upload_errors'}=$@;
		$c->session->{upload_data}=$browser_upload->upload_data;
		return;
	}
	  
=head1 DESCRIPTION

Module dealing with temporary small uploads to the Browserupload database.

The work is done by 'do_upload' which reads the file in and deals with fatal problems by throwing intelligible Exceptions.
These exceptions are caught by Web::Controller and displayed on the HTML page as red messages.
There is also a method to delete uploads given an upload identifier.

NB. In order to work this requires an instance of Database::Browser setup before calling do_upload.


=head1 SUBROUTINES/METHODS 

TODO

=cut

package GwasCentral::Browser::Upload;


use Moose;
extends qw(GwasCentral::Base);
with qw(GwasCentral::DSContainer);
has 'Creator' => ('is' => 'rw'); #Browser::Creator object
has 'FileParser' => ('is'=>'rw'); #FileParser object
has 'upload_data' => ('is'=>'rw', 'default' => sub { {} }); #upload data keyed by resultset identifier
has 'messages' => ('is'=>'rw', 'default' => sub { [] }); #list of upload messages
has 'issues' => ('is'=>'rw', 'default' => sub { [] }); #list of upload issues
no Moose;

my @failed_lines = ();
my $successful_lines = 0;
my @failed_pvalues = ();
my @dead_ids = ();
my @changed_ids = ();
my @no_coords_ids = ();

use GwasCentral::FileParser;
use GwasCentral::Base qw(open_infile open_outfile pluralize);
use GwasCentral::Browser::Util qw(neglog chr_list);
use GwasCentral::Browser::Creator;
use GwasCentral::DataSource::BrowserUpload;
use GwasCentral::Browser::CustomResultset;
use Data::Dumper qw(Dumper);
use File::Temp qw(tempfile);
use Scalar::Util qw(looks_like_number);
use LWP::UserAgent;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use Benchmark;

use GwasCentral::DataImport::RetrieveMarker;
use GwasCentral::DataSource::Marker;

sub do_upload {
	my ($self, $attrs) = @_;
	$self->log->info("attrs:".Dumper($attrs));
	my $fp = GwasCentral::FileParser->new({ conf_file => $self->config });
	
	$self->FileParser($fp);
	
	my $max_lines = $self->config->{max_upload_lines} || $self->throw("No 'max_upload_lines' in config");
	my $max_uploads = $self->config->{max_uploads} || $self->throw("No 'max_uploads' in config");
	my $tempdir = $attrs->{tempdir};
	my $file = $tempdir."/uploads/".$attrs->{file};
	my $study_name = $attrs->{study_name};
	my $resultset_name = $attrs->{resultset_name};
	my $separator = $attrs->{field_separator};
	$self->log->info("file:$file");
	
	if (!$file) {
		$self->throw("Sorry. The file was not uploaded. The upload was either aborted or it was too big (>".$self->config->{max_upload_kb}."Kb).");
	}
	
	if ($file eq 'toobig') {
		$self->throw("Sorry. The file you tried to upload was too big (>" . $self->config->{max_upload_kb}."Kb).");
	}
	
	my $browser_db = $self->DS('Browser');
	my $upload_db = GwasCentral::DataSource::BrowserUpload->new({ conf_file => $self->config} );

	my $creator = GwasCentral::Browser::Creator->new({ conf_file => $self->config, no_init=>1} );
	$creator->DS('Browser',$upload_db );
	$creator->Browser->DS('Browser',$upload_db);
	$self->Creator($creator);
	!$self->upload_data and $self->upload_data({});
	
	my @rs_idents = ();
	@failed_lines = ();
	@failed_pvalues = ();
	if (scalar(keys %{$self->upload_data})>=$max_uploads) {
		$self->throw("Sorry you have reached the maximum number of uploads (". $max_uploads."). You will have to remove one or more uploads before trying again.");
	}
	
	my $rs_ident = $upload_db->insert_upload_data({
		study_name=>$study_name,
		resultset_name=>$resultset_name,
		sessionid => $attrs->{sessionid}
	});
		
	my $sep_char = qr(\s+|,|\t);
	my $is_over_max_lines=0;
	my $issue = 0;
	eval {
		$is_over_max_lines = $self->_upload_single({
			file => $file,
			field_separator=>$sep_char,
			rs_ident=>$rs_ident,
		});
		push @rs_idents, $rs_ident;
		
	};
	if ($@) {
		if (ref($@) eq 'GwasCentral::Exception') {
			die $@;
		}
		else {
			$self->throw("An unknown error occurred during upload:".$@);
		}
	}

	if ($successful_lines == 0) {
		$self->throw("No rsIDs were recognised and no p-values could be obtained from the file. Please check it is in the specified format.");
	}
	
	if ($is_over_max_lines) {
		push @{$self->issues},"Only the first ".pluralize($max_lines,"line")." were uploaded from the file." ;
	}
	
	if (scalar(@failed_lines)>0) {
			push @{$self->issues},pluralize(\@failed_lines, "line")." in the file did not have recognised dbSNP ids";
			$issue = 1;
	}
	
	if (scalar(@failed_pvalues)>0) {
			push @{$self->issues},pluralize(\@failed_pvalues, "line")." in the file did not have numeric p-values";
			$issue = 1;
	}
	
	if (scalar(@changed_ids)>0) {
			push @{$self->issues},pluralize(\@changed_ids, "line")." had dbSNP IDs which have changed";
			$issue = 1;
	}
	
	if (scalar(@dead_ids)>0) {
			push @{$self->issues},pluralize(\@dead_ids, "line")." had dbSNP IDs which are dead";
			$issue = 1;
	}
	
	if (scalar(@no_coords_ids)>0) {
			push @{$self->issues},pluralize(\@no_coords_ids, "line")." had dbSNP Markers with no co-ordinates";
			$issue = 1;
	}
		
	if ($issue) {
		 unshift @{$self->issues},"Uploaded file with minor ".pluralize($self->issues,"issue").".";
	}
	else {
		push @{$self->messages},"Uploaded file successfully.";
	}
	$self->upload_data->{$rs_ident}={ study_ident => 'UPLOAD', study_name => $study_name, resultset_name => $resultset_name, resultset_identifier => $rs_ident, success_count => $successful_lines, failed_ids => \@failed_lines, failed_pvalues => \@failed_pvalues, dead_ids => \@dead_ids, changed_ids => \@changed_ids, no_coords_ids => \@no_coords_ids  };
	
	return $self->upload_data;
}

sub convert_tempfile {
	my ($attrs) = @_;
	my $infile_name = $attrs->{filename};
	my $infile_fh = $attrs->{fh};
	my $tempdir = $attrs->{tempdir};
	my $progressid = $attrs->{progressid};
	
	#my ($tempfh, $tempinfile) = tempfile($tempdir ."/uploads/$progressid");
	my $tempinfile = $tempdir ."/uploads/$progressid";
	
	my $tempfh = open_outfile($tempinfile);
	while(my $line = $infile_fh->getline){ 
	    $line =~ s/\r/\n/g; 
	    $tempfh->print($line);
	}
	$infile_fh->close;
	$tempfh->close;
	unlink $infile_name;
	chmod 0777, $tempinfile;
	return $tempinfile;
}

sub _upload_single {
	my ($self, $attrs) = @_;
	my $separator = $attrs->{field_separator};
	my $creator = $self->Creator;
	my $rs_ident = $attrs->{rs_ident};
	my $file = $attrs->{file};
	
	my $fh = open_infile($file);
	foreach my $chr ( keys %{ chr_list() } ) {
		$creator->Browser->get_db->delete_marker_data_by_resultset_and_chr(
				{ resultset =>  $rs_ident, chr => $chr } );
	}	
	
	$successful_lines = 0;
	my $all_lines = 0;
	@failed_lines = ();
	@failed_pvalues = ();
	@dead_ids = ();
	@changed_ids = ();
	@no_coords_ids = ();
	
	my $marker_sth = $self->DS('Browser')->get_marker_sth_by_accession;
	my $is_over_max_lines = 0;
	
	while (my $line = <$fh>) {
		my @fields = split ($separator,$line);
		$self->throw("Sorry the file you uploaded has more than 2 columns") if scalar(@fields)>2;
		
		next if length($fields[0])==0;
		$all_lines++;
		
		
		if ($all_lines > $self->config->{max_upload_lines}) {
			$is_over_max_lines = 1; last; 
		}
			
		if (!looks_like_number($fields[1])) {
			push @failed_pvalues,$fields[0]."=".$fields[1];
			next;
		}
		$marker_sth->execute($fields[0]);
		my $marker = $marker_sth->fetchrow_hashref;
		
		if (!$marker) {
			push @failed_lines,$fields[0];
			next;
		}
		if ($marker->{Status} ne 'active') {
			if ($marker->{CurrentAccession}) {
				$marker_sth->execute($marker->{CurrentAccession});
				$marker = $marker_sth->fetchrow_hashref;
				if (!$marker) {
					push @dead_ids,$fields[0];
					next;
				}
				else {
					push @changed_ids,$fields[0]."->".$marker->{Accession};
				}
			}
			else {
				push @dead_ids,$fields[0];
				next;
			}
		}
		
		my $chr = $marker->{'Chr'};
		if (!$chr) {
			push @no_coords_ids,$fields[0];
			next;
		}
		
		my $sth = $creator->get_sth_markers( $chr, undef );
		tie my %record, "Tie::Hash::Indexed";
		%record = (
			Study_Identifier     => "Upload",
			Resultset_Identifier => $rs_ident,
			UnadjustedPValue     => $fields[1],
			NegLogPValue => sprintf( "%.3f", neglog( $fields[1] ) ),
			Marker_Identifier => $marker->{Identifier},
			Marker_Accession  => $fields[0],
			VariationType     => "SNP",
			Chr               => $chr,
			Start             => $marker->{Start},
			Stop              => $marker->{Stop},
			Strand            => "",
			Upstream30bp      => $marker->{Upstream30bp},
			Downstream30bp      => $marker->{Downstream30bp},
			Alleles           => $marker->{Alleles},
			SignificanceID       => undef,
			UsedmarkersetID   => undef,
			RiskAllele        => undef,
			RiskAlleleFreq    => undef,
			ES_Type           => undef,
			ES_Value          => undef,
			ES_Lower95Bound   => undef,
			ES_Upper95Bound   => undef,
			ES_StdError       => undef,
		);
		$sth->execute( values %record );
		$successful_lines++;
	}
	my $rs = GwasCentral::Browser::CustomResultset->new({
		identifier => $rs_ident
	});
	$creator->Browser->DS('Browser')
					  ->delete_marker_binned_chr_by_resultset(
						{ resultset => $rs->identifier, binsize => 3 } );
	$creator->Browser->DS('Browser')->delete_marker_binned_by_resultset(
						{ resultset => $rs->identifier, binsize => 1 } );
	$creator->prepare_calc_genome;
	$creator->prepare_calc_region;
	$creator->calc_genome_binned($rs,undef);
	$creator->calc_region_binned($rs,undef);
	
	return $is_over_max_lines;
}

sub delete_upload {
	my ($self, $id) = @_;
	delete $self->upload_data->{$id};
	$self->messages(["Upload '$id' was removed successfully."]);
}

sub remote_upload {
	my ( $self, $attrs ) = @_;
	my $remote_file = $attrs->{file};
	my $title       = $attrs->{title};
	$self->throw( "File not provided"  )
	  if !$remote_file;
	$self->throw( "Title not provided"  )
	  if !$title;

	my $ua =
	  new LWP::UserAgent( agent =>
'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.5) Gecko/20060719 Firefox/1.5.0.5'
	  );
	my $res     = $ua->get($remote_file);
	my $tempdir = $self->config->{tempdir};
	$self->config->{max_upload_lines} = 500000;
	$self->throw( "Unable to get data from '$remote_file'" )
	  if !$res->is_success;

	my ( $tempfh, $tempinfile ) = tempfile( $tempdir . "/uploads/ruXXXXXXX" );
	my $content = $res->content;
	gunzip \$content => $tempfh;
	$tempfh->close;

	my $fh = open_infile($tempinfile);

	my $tempname = convert_tempfile(
		{
			filename => $tempinfile,
			fh       => $fh,
			tempdir  => $tempdir
		}
	);
	$tempfh->close;

	return $self->do_upload(
		{
			file           => $tempname,
			study_name     => "Remote Upload",
			resultset_name => $attrs->{title},
			sessionid      => $attrs->{sessionid}
		}
	);
}

1;