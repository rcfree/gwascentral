# $Id: Binning.pm 1447 2010-05-26 14:25:42Z rcf8 $

package GwasCentral::Browser::Binning;

=head1 NAME

HGVbaseG2P::Browser::Binning - Specialised class for creation of binned values including generation of bin-based stats


=head1 SYNOPSIS
    
TODO

=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

TODO

=cut

use strict;
use warnings;

use Moose;
use Math::Round qw(nlowmult);

extends qw(GwasCentral::Base);

# Private variables & constants
has 'data'     => ( 'is' => 'rw' );
has 'bin_data' => ( 'is' => 'rw' );
has 'output_to' => ( 'is' => 'rw' );

use Carp qw(cluck confess);
use Data::Dumper;

use GwasCentral::Base qw(load_module);
use GwasCentral::Browser::Util qw(poslog);

use POSIX qw(strftime);
use File::Basename;

=head2 BUILD

	  Usage      : Internally called by new method
	  Purpose    : Read data from file or use data structure
	  Returns    : Nothing
	  Arguments  : Hashref of:
	                data - raw data as array
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub BUILD {
	my ( $self, $args ) = @_;

	#if raw data supplied, store this in the object
	if ( $args->{data} ) {
		#$self->log->debug("Using raw data");
		$self->data( $args->{data} );
	}
}

sub has_config { 0 };

=head2 process_values

	  Usage      : $analyse->process_values(
					{
						process_field   => '4',
						process_handler => sub {
							my $val = shift;
							$val ? sprintf( "%.2f", -log($val) / log(10) ) : '0.00';
				  		}
					}
					);
	  Purpose    : Processes values in an Analyse dataset using the field provided and the process handler.
	  Returns    : Nothing
	  Arguments  : Hashref containing:
	  				process_field -  identifies field to use for process 
	  								number if data is array of arrays, field (ie. key) if data is an array of hashes
	  			    process handler - reference to sub for processing the value to process is provided as the first argument in the routine
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub process_values {
	my ( $self, $attrs ) = @_;
	my $process_field   = $attrs->{process_field};
	my $process_handler = $attrs->{process_handler};

	my $data = $self->data;

	#do different things for hash and array based data structure
	if ( ref( $data->[0] ) eq 'HASH' ) {
		foreach my $item ( @{$data} ) {
			my $unprocessed = $item->{$process_field};
			my $processed   = $process_handler->($unprocessed);
			$item->{$process_field} = $processed;
		}
	}
	elsif ( ref( $data->[0] ) eq 'ARRAY' ) {
		foreach my $item ( @{$data} ) {
			my $unprocessed = $item->[$process_field];
			my $processed   = $process_handler->($unprocessed);
			$item->[$process_field] = $processed;
		}
	}
}

=head2 combine_data

	  Usage      :  my @data = ($data1, $data2);
	  				my $combi_data = HGVbaseG2P::Analyse::combine_data(\@data);
					
	  Purpose    : Combines data from multiple data sets (ie. arrays of hashes) and renumbers keys if they are the same
	               eg. combining results of three binning experiments will result in bin_content, bin_content1 and bin_content2.
	               NOTE: The datasets must represent the same points (on a graph for example)
	  Returns    : Array of hashes with the combined data
	  Arguments  : Arrayref containing datasets (ie. arrayref of hashes)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub combine_data {
	my ($data) = @_;

	my @newdata = ();
	my $set_no  = 0;
	foreach my $set ( @{$data} ) {

		my $item_no = 0;
		foreach my $item ( @{$set} ) {
			foreach my $key ( keys %{$item} ) {
				if ( $newdata[$item_no]->{$key} ) {
					$newdata[$item_no]->{ $key . $set_no } = $item->{$key};
				}
				else {
					$newdata[$item_no]->{$key} = $item->{$key};
				}
			}
			$item_no++;
		}
		$set_no++;
	}
	return \@newdata;
}

=head2 use_threshold

	  Usage      :  $analyse->use_threshold(
					{
						value => 4,
						field => '4',
					}
					);
					
	  Purpose    : Processes data in an Analyse object to remove data below a threshold.
	  Returns    : Nothing
	  Arguments  : Hashref containing:
	  				value - value of threshold
	  				field - field (key) name or number of array position
	  				type - for future expansion - will default to above but may add below etc.
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut    

sub use_threshold {
	my ( $self, $attrs ) = @_;
	my $type     = $attrs->{type};
	my $field    = $attrs->{field};
	my $value    = $attrs->{value};
	my $old_data = $self->data;
	my @new_data = ();
	my $counter  = 0;

	my $is_array = 0;
	ref( $old_data->[0] ) eq 'ARRAY' and $is_array = 1;

	#$self->log->debug("Use threshold options:" . Dumper($attrs));
	if ($is_array) {
		foreach my $item ( @{$old_data} ) {
			if ( $item->[$field] < $value ) {
			}
			else {

	  #$self->log->debug("ADDED item ".$item->{$field}. "< $value -> $counter");
				push @new_data, $item;
			}
		}
	}
	else {
		foreach my $item ( @{$old_data} ) {
			if ( $item->{$field} < $value ) {
			}
			else {

	  #$self->log->debug("ADDED item ".$item->{$field}. "< $value -> $counter");
				push @new_data, $item;
			}
		}
	}

	$self->data( \@new_data );
}

=head2 statistic

	  Usage      :  $analyse->statistic(
					{
						type => 'Maximum',
						field => 'PValue',
					}
					);
					
	  Purpose    : Returns a statistic for the data in the Analyse object. Wraps the Statistic::Basic::* classes to do most of these
	               except Minimum and Maxumum
	  Returns    : The statistic based on the type and field supplied
	  Arguments  : Hashref containing:
	  				type - Type of Statistic. The options are Minimum, Maximum plus those available in CPAN including 
	  				       Mean, Mode, Median, StdDev and Variance
	  				field - field (key) name to use
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut    

sub statistic {
	my ( $self, $attrs ) = @_;
	my $vector =
	  $self->get_values_as_array( $attrs->{field}, $attrs->{sum},
		$attrs->{logged} );
	if ( $attrs->{type} eq 'Maximum' ) {
		my @sorted = reverse sort { $a <=> $b } @{$vector};
		return $sorted[0];
	}
	if ( $attrs->{type} eq 'Minimum' ) {
		my @sorted = sort { $a <=> $b } @{$vector};
		return $sorted[0];
	}
	my $stats_mod = "Statistics::Basic::" . $attrs->{type};
	$self->log->debug("Using $stats_mod to retrieve statistic");
	$self->log->debug("Passing $vector");
	my $stat = load_module( $stats_mod, $vector );
	return $stat->query;
}

=head2 get_values_as_array

	  Usage      :  $analyse->get_values_as_array('PValue');

	  Purpose    : Get an arrayref of values from the data part of an Analyse object using the field provided.
	  Returns    : An arrayref of values from an Analyse object.
	  Arguments  : Field name - key in hash of Analyse dataset
	               
	               optional:
	               Raw data - use data from outside the Analyse object
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut    

sub get_values_as_array {
	my ( $self, $field, $sum ) = @_;
	my @vector = ();

	use List::Util qw(sum);

	my $data = $self->data;

	if ($sum) {
		foreach my $item ( @{$data} ) {
			my $value = 0;
			foreach ( split( /\|/, $item->{$field} ) ) {
				if ( $_ > 0 ) { $value += poslog($_) }
			}
			push @vector, $value;
		}
	}
	else {
		foreach my $item ( @{$data} ) {
			push @vector, $item->{$field};
		}
	}

	return \@vector;
}

=head2 latest_max

	  Usage      : my $max = $bin_analyse->latest_max;
	  Purpose    : Get maximum value of binned data once it has been processed (object stores the latest_data in $self->bin_data
	  Returns    : Value of maximum
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub latest_max {
	my ($self) = @_;
	my $bin_data = $self->bin_data;

	if ( $self->output_to eq 'array' ) {
		my @sorted_items = reverse sort { $a->[1] <=> $b->[1] } @{$bin_data};
		warn "latest max:" . $sorted_items[0]->[1];
		return $sorted_items[0]->[1];
	}
	else {
		my @sorted_items =
		  reverse sort { $a->{bin_content} <=> $b->{bin_content} } @{$bin_data};
		warn "latest max:" . $sorted_items[0]->{bin_content};
		return $sorted_items[0]->{bin_content};
	}
}

=head2 clear_latest

	  Usage      : Usually internally used
	  Purpose    : Clear the binned data store
	  Returns    : Nothing
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub clear_latest {
	my ($self) = @_;
	$self->bin_data( [] );
}

=head2 bin

	  Usage      : my $binned = $analyse->bin(
						{
							position_field => '2',
							bin_size       => $attrs->{bin_size},
							total_length   => $self->chr_length,
							value_field    => '4',
							bin_content    => 'Count',
							calc_ranges    => 1,
							start_position      => $self->round_start,
							stop_position      => $self->round_stop,
							output_to      => 'array'
						}
					);
	  Purpose    : Perform binning algorithm.
	  Returns    : Binned data (in an arrayref of hashes)
	               Hashes contain the following info:
	                 bin_no - number of bin
	                 bin_content - depends on bin_content specified can be a value or an arrayref
	                 bin_start - depends on whether this is asked for (calculated from the start and bin_size)
	                 bin_stop - depends on whether this is asked for (calculated from the start and bin_size)
	  Arguments  : Hashref containing:
	  						position_field - hash key or array position specifying the field containing the position
							bin_size       - size of bins to create
							total_length   - total length of zone to create bins for
							value_field    - hash key or array position specifying the field containing the value
							bin_content    - output type of binning - available options are array, values, count         
							calc_ranges    - generate bin_start and bin_stop in hash as well as bin_content
							start_position      - start point for calc_ranges
							stop_position      - stop point for calc_ranges
							output_to      - output to array of arrays (array) or hashes (hash)
	  Throws     : 
	  Status     : Public
	  Comments   : Calls the bin_to_array or bin_to_hash method depending on the value of output_to
=cut

sub bin {
	my ( $self, $attrs ) = @_;
	my $output_to = $attrs->{output_to} || 'hash';
	$self->output_to($output_to);

	my $method = "bin_to_" . $output_to;
	return $self->$method($attrs);
}

#internal method to bin data to array of arrays (faster)
sub bin_to_array {
	my ( $self, $attrs ) = @_;
	$self->log->debug("bin to array");
	my $position_field  = $attrs->{position_field};
	my $bin_size        = $attrs->{bin_size};
	my $total_length    = $attrs->{total_length};
	my $bin_content     = $attrs->{bin_content};
	my $value_field     = $attrs->{value_field};
	my $process_handler = $attrs->{process_handler};
	my $threshold       = $attrs->{threshold};

	my $bin_start = $attrs->{start_position} || 1;
	my $bin_stop  = $attrs->{stop_position}  || $total_length;
	#$self->log->debug("bin_start:$bin_start and bin_stop:$bin_stop");
	my $output_to = 'array';

	my $data = $self->data;
	#$self->log->debug("Total length:$total_length and bin size:$bin_size");

	my $number_of_bins = int( ( $bin_stop - $bin_start ) / $bin_size ) + 1;
	#$self->log->debug("No of bins $number_of_bins");
	my @bins = ();

	#set empty bin values (arrayrefs)
	for ( 0 .. $number_of_bins ) {
		$bins[$_]->[0] = "$_";
		$bins[$_]->[1] = [];
	}

	#if input is an array of arrays then set flag
	my $is_array = 0;
	ref( $data->[0] ) eq 'ARRAY' and $is_array = 1;

	#loop through each item in the data and generate bins with arrays of values
	foreach my $item ( @{$data} ) {

		#get position and value from item
		my ( $pos, $value );
		if ($is_array) {
			$pos   = $item->[$position_field] - $bin_start;
			$value = $item->[$value_field];
		}
		else {
			$pos   = $item->{$position_field} - $bin_start;
			$value = $item->{$value_field};
		}

		#set the bin position value based on item pos / bin size
		#go the next item if the pos is greater than the expected number of bins
		my $bin_pos = nlowmult( 1, $pos / $bin_size );
		next if $bin_pos > $number_of_bins;

		#add the value to the array in the bin
		if ( $bin_content eq 'array' ) {
			push @{ $bins[$bin_pos]->[1] }, $item;
		}
		else {
			push @{ $bins[$bin_pos]->[1] }, $value;
		}
	}

	#calculate stats for the contents of all bins
	if ( $bin_content ne 'array' and $bin_content ne 'values' ) {
		@bins =
		  @{ $self->calculate_bin_stats( \@bins, $bin_content, $output_to ) };
	}

	#process binned stats or values if a process handler is provided
	if ($process_handler) {
		foreach my $bin (@bins) {
			$bin->[1] = $process_handler->( $bin->[1] );
		}
	}

	#calculate bin ranges (start and stop) based on provided start and stop
	if ( $attrs->{calc_ranges} ) {
		@bins = @{
			$self->calculate_ranges( \@bins, $bin_start, $attrs->{bin_size},
				$output_to )
		  };
	}

	#return final binned data
	$self->bin_data( \@bins );
	return \@bins;
}

#internal method to bin data to array of hashes (slower but useful for development)
sub bin_to_hash {
	my ( $self, $attrs ) = @_;

	my $position_field  = $attrs->{position_field};
	my $bin_size        = $attrs->{bin_size};
	my $total_length    = $attrs->{total_length};
	my $bin_content     = $attrs->{bin_content};
	my $value_field     = $attrs->{value_field};
	my $process_handler = $attrs->{process_handler};
	my $threshold       = $attrs->{threshold};

	my $bin_start = $attrs->{start_position} || 1;
	my $bin_stop  = $attrs->{stop_position}  || $total_length;

	my $output_to = 'hash';

	my $data = $self->data;
	#$self->log->debug("Total length:$total_length and bin size:$bin_size");

	my $number_of_bins = int( ( $bin_stop - $bin_start ) / $bin_size );
	#$self->log->debug("No of bins $number_of_bins");

	#set empty bin values (arrayrefs)
	my @bins = ();

	for ( 0 .. $number_of_bins ) {
		$bins[$_]->{bin_no}      = "$_";
		$bins[$_]->{bin_content} = [];
	}

	#if input is an array of arrays then set flag
	my $is_array = 0;

	ref( $data->[0] ) eq 'ARRAY' and $is_array = 1;

	#loop through each item in the data and generate bins with arrays of values
	foreach my $item ( @{$data} ) {
		my ( $pos, $value );
		#$self->log->debug("bin_item:".Dumper($item));
		#get position and value from item
		if ($is_array) {
			$pos   = $item->[$position_field] - $bin_start;
			$value = $item->[$value_field];
		}
		else {
			$pos   = $item->{$position_field} - $bin_start;
			$value = $item->{$value_field};
		}

		#set the bin position value based on item pos / bin size
		#go the next item if the pos is greater than the expected number of bins

		my $bin_pos = nlowmult( 1, $pos / $bin_size );

		#goto next item if outside binning area
		next if $bin_pos > $number_of_bins || $bin_pos < 0;

#$self->log->debug("bin_pos: $bin_pos, number_of_bins:$number_of_bins, value:".$item->{$value_field});

		#add the value to the array in the bin
		if ( $bin_content eq 'array' ) {
			push @{ $bins[$bin_pos]->{bin_content} }, $item;
		}
		else {
			push @{ $bins[$bin_pos]->{bin_content} }, $value;
		}
	}

	#calculate stats for the contents of all bins
	if ( $bin_content ne 'array' and $bin_content ne 'values' ) {
		@bins =
		  @{ $self->calculate_bin_stats( \@bins, $bin_content, $output_to ) };
	}

	#process binned stats or values if a process handler is provided
	if ($process_handler) {
		foreach my $bin (@bins) {
			$bin->{bin_content} = $process_handler->( $bin->{bin_content} );
		}
	}

	#calculate bin ranges (start and stop) based on provided start and stop
	if ( $attrs->{calc_ranges} ) {
		@bins = @{
			$self->calculate_ranges( \@bins, $bin_start, $attrs->{bin_size},
				$output_to )
		  };
	}

	#return final binned data
	$self->bin_data( \@bins );
	return \@bins;
}

=head2 calculate_bin_stats

	  Usage      : @bins = @{$self->calculate_bin_stats(\@bins,"Max",'array')};
	  Purpose    : Calculate statistics for data binned into arrays
	  Returns    : New binned data object containing either array or hash of binned data
	  Arguments  : Binned data - data binned into arrays
	               Statistic - value to generate for each bin. Options include Max, Min, Count and Statistics::Basic::* modules
	                           eg. StDev, Variance etc.
	               Output to - array of arrays or hashes
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub calculate_bin_stats {
	my ( $self, $bins, $bin_stat, $output_to ) = @_;

	foreach my $item ( @{$bins} ) {
		my @items = ();

		if ( $output_to eq 'array' ) {
			@items = @{ $item->[1] };
		}
		else {
			@items = @{ $item->{bin_content} };
		}

		if ( $bin_stat eq 'Max' ) {

			#warn "item:".$item->{bin_no}."-".Dumper($item);
			if (@items) {
				my @sorted_items = reverse sort { $a <=> $b } @items;

				#$self->log->debug("reverse items:".Dumper(\@sorted_items));
				$output_to eq 'array'
				  ? $item->[1] = $sorted_items[0]
				  : $item->{bin_content} = $sorted_items[0];
			}
			else {
				$output_to eq 'array'
				  ? $item->[1] = '0.00'
				  : $item->{bin_content} = '0.00';
			}

			#warn "content:".$item->{bin_content};
		}
		elsif ( $bin_stat eq 'Min' ) {
			if (@items) {
				my @sorted_items = sort { $a <=> $b } @items;

				#$self->log->debug("reverse items:".Dumper(\@sorted_items));
				$output_to eq 'array'
				  ? $item->[1] = $sorted_items[0]
				  : $item->{bin_content} = $sorted_items[0];
			}
			else {
				$output_to eq 'array'
				  ? $item->[1] = '0.00'
				  : $item->{bin_content} = '0.00';
			}
		}
		elsif ( $bin_stat eq 'Count' ) {
			$output_to eq 'array'
			  ? $item->[1] = scalar(@items)
			  : $item->{bin_content} = scalar(@items);
		}
		else {
			my $stats_mod = "Statistics::Basic::" . $bin_stat;
			my $stat = load_module( $stats_mod, \@items );
			$output_to eq 'array'
			  ? $item->[1] = $stat->query
			  : $item->{bin_content} = $stat->query;
		}
	}
	return $bins;
}

=head2 bin_with_threshold

	  Usage      : my $binned = $analyse->bin_with_threshold(
						{
							position_field => '2',
							bin_size       => $attrs->{bin_size},
							total_length   => $self->chr_length,
							value_field    => '4',
							bin_content    => 'Count',
							calc_ranges    => 1,
							bin_start      => $self->round_start,
							bin_stop       => $self->round_stop,
							output_to      => 'array',
							threshold_value => 4,
						    threshold_type => 'number'
						}
					);
	  Purpose    : Perform binning algorithm with a threshold on resulting bins
	  Returns    : Binned data passing a threshold - see 'bin'
	  Arguments  : Hashref containing the same as 'bin':
	  						position_field - hash key or array position specifying the field containing the position
							bin_size       - size of bins to create
							total_length   - total length of zone to create bins for
							value_field    - hash key or array position specifying the field containing the value
							bin_content    - output type of binning - available options are array, values, count         
							calc_ranges    - generate bin_start and bin_stop in hash as well as bin_content
							bin_start      - start point for calc_ranges
							bin_stop       - stop point for calc_ranges
							output_to      - output to array of arrays (array) or hashes (hash)
				   Also:
				            threshold_value - value to use as threshold
				            threshold_type - type of threshold which can be number (only data returned are top X values)
				                             or percent (only the top percentage of values are returned eg. 20% of the top values)
	  Throws     : 
	  Status     : Public
	  Comments   : Using the standard binning algorithm to bin data into arrays initially then uses this data to remove values not passing
	               the threshold. Finally calculates statistics for these eg. mean of top 20% of values
=cut

sub bin_with_threshold {
	my ( $self, $attrs ) = @_;
	my $content_type = $attrs->{bin_content};
	my $output_to    = $attrs->{output_to};

#get bins with values in and then use these to build means etc. - use standard bin with attributes passed in
	$attrs->{bin_content} = 'values';
	my @bins = @{ $self->bin($attrs) };

	my $t_value = $attrs->{threshold_value};
	my $t_type  = $attrs->{threshold_type};

	my $item_no = 0;

	#loop through each data item
	foreach my $item (@bins) {

		my $no_in_bin = scalar( @{ $item->{bin_content} } );
		my $bin_content =
		  $output_to eq 'array' ? $item->[1] : $item->{bin_content};

#for threshold type 'number' reverse sort the data (ie. descending) and use the top X values
		if ( $t_type eq 'number' ) {
			my $threshold = $t_value;
			my @sorted = reverse sort { $a <=> $b } @{$bin_content};

#$self->log->debug("Sorting count: no in bin:$no_in_bin, threshold value:$t_value");

			$threshold > $no_in_bin and $threshold = $no_in_bin;

			my @slice = @sorted[ 0 .. $threshold - 1 ];
			$output_to eq 'array'
			  ? $item->[1] = \@slice
			  : $item->{bin_content} = \@slice;

		}

#for threshold type 'percent' reverse sort the data (ie. descending) and use the top X% of values
		elsif ( $t_type eq 'percent' ) {
			my $threshold = $no_in_bin * ( $t_value / 100 );
			my @sorted = reverse sort { $a <=> $b } @{$bin_content};

#$self->log->debug("Sorting percent: no in bin:$no_in_bin, threshold value:$t_value");

			my @slice = @sorted[ 0 .. $threshold - 1 ];
			$output_to eq 'array'
			  ? $item->[1] = \@slice
			  : $item->{bin_content} = \@slice;

#$self->log->debug("Sorted: new threshold: $threshold, content:".Dumper($bin_content));

		}
		$item_no++;
	}

	#calculate the stats on the thresholded data
	@bins = @{ $self->calculate_bin_stats( \@bins, $content_type ) };

	#return the thresholded data
	$self->bin_data( \@bins );
	return \@bins;
}

=head2 calculate_ranges

	  Usage      : @bins = @{$self->calculate_ranges(\@bins,$bin_start,$bin_size,$output_to)};
	  Purpose    : Calculate ranges for each for item binned into arrays
	  Returns    : New binned data object containing bin_start and bin_stop keys in each hash
	  Arguments  : Binned data - data binned into arrays
	               Start - start point of zone
	               Bin size - size of bins to generate ranges for
	               Output to - Specify if array of arrays (array) or hashes (hash)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub calculate_ranges {
	my ( $self, $bins, $bin_start, $bin_size, $output_to ) = @_;

	if ( $output_to eq 'array' ) {
		foreach my $item ( @{$bins} ) {
			$item->[2] = $bin_start;
			$bin_start += $bin_size - 1;
			$item->[3] = $bin_start;
			$bin_start++;
		}

	}
	else {
		foreach my $item ( @{$bins} ) {
			$item->{bin_start} = $bin_start;
			$bin_start += $bin_size - 1;
			$item->{bin_stop} = $bin_start;
			$bin_start++;
		}
	}

	return $bins;

}
1;

=head1 SEE ALSO



=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2008> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: Binning.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut

