# $Id: Region.pm 1540 2010-09-22 08:35:36Z rcf8 $

=head1 NAME

HGVbaseG2P::Browser::Region - Generate features for Browser region view


=head1 SYNOPSIS
		
	  my $browser = HGVbaseG2P::Browser::Region->new({
				  conf_file =>
					$home."/conf/hgvbase.conf",
					feature_list=>$self->new_feature_list,
					autodb=>1
	  });
	  
	  my %attrs = %{$self->configuration};

	  $attrs{ref}=$segment->seq_id;
	  $attrs{seg_start}=$segment->start;
	  $attrs{seg_stop}= $segment->stop;
	  
      my $feature_list = $browser->generate_features(\%attrs);
	  
	  
=head1 DESCRIPTION

TODO

=head1 SUBROUTINES/METHODS 

TODO

=cut

package GwasCentral::Browser::Region;

use Moose;
extends qw(GwasCentral::Browser::Core);
use GwasCentral::Browser::Util
  qw(chr_list chr_length split_data_by_field format_bases poslog);
use List::Util qw(sum max);
use List::MoreUtils qw(true firstval);
use Data::Dumper qw(Dumper);
no Moose;

=head2 generate_region_header

	  Usage      : my ($html, $sess_data) = $hgvbrowser->generate_region_header({
						resultsetid => $resultsetid,
						threshold => $threshold,
						sess_data => $session->config_hash->{plugins}->{'P value Data'},
						chr=>$segment->seq_id,
						start=>$segment->start,
						length=>$segment->length
	               });
	  Purpose    : Generate specific HTML header and session data for region browser containing study info and das link etc.
	  Returns    : HTML header text and hash containing Gbrowse session data
	  Arguments  : In attributes hash:
	  			   sess_data => session data hash from GBrowse plugin
	               resultsetid => analysis method id (generally from CGI param)
	               threshold => significance threshold (generally from CGI param)
	               chr => chromosome name (ChrN)
	               start => start of GBrowse segment
	               length => length of GBrowse segment
	               
	  Throws     : 
	  Status     : Public
	  Comments   : Separates web logic from feature generation - all values required are passed in/out.
	               This means the session data hash and parameter data are passed in and returned.
	               N.B. This may change as it is not as important as in V1 of the Browser
	
=cut

sub generate_region_header {
	my ( $self, $attrs ) = @_;

	my $sess_data      = $attrs->{sess_data};
	my $par_resultsets = $attrs->{resultsets};
	my $par_threshold  = $attrs->{threshold};
	my $chr            = $attrs->{chr};
	my $start          = $attrs->{start};
	my $length         = $attrs->{length};
	my $par_settings   = $attrs->{settings};
	my $par_tracks     = $attrs->{tracks};
	my $threshold;
	my $fulllabel;
	my $html = '';

	my $sess_resultsets    = $sess_data->{resultsets};
	my $sess_resultsetinfo = $sess_data->{resultsetinfo};
	my $sess_fulllabel     = $sess_data->{fulllabel};
	my $is_displayed       = $sess_data->{is_displayed};

	#compare session and CGI params
	my ( $resultsets, $get_info, $resultsetinfo ) =
	  $self->compare_resultsets( $par_resultsets, $sess_resultsets );

	#set values in session hash
	$par_threshold and $sess_data->{threshold} = $par_threshold;
	$sess_data->{resultsets}   = $resultsets;
	$sess_data->{is_displayed} = $is_displayed;
	$par_tracks   and $sess_data->{tracks}   = $par_tracks;
	$par_settings and $sess_data->{settings} = $par_settings;

	#generate HTML for header
	$chr =~ /Chr(.+)/;
	return ( $html, $sess_data );
}

=head2 generate_features

	  Usage      : my ($attrs) = $browser->generate_features({
    				bin_size=>3000000,
    				threshold=>$threshold,
    				resultsetid=>$resultsetid,
    				logscale=>$logscale,

    			   });
    			   
	  Purpose    : Main method to generate feature data for use by RegionView.pm gbrowse plugin.
	  Returns    : $attrs - attributes passed in from RegionView plugin
	  				
	  Arguments  : In hash ref
	  				 threshold - -log of P value threshold
	  				 resultsets - comma-separated list of resultsetids to generate genome data for
	  				 logscale - flag indicating whether log (1) or linear (0) scale should be used
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub generate_features {
	my ( $self, $attrs ) = @_;

	my %messages = $self->prepare_browser($attrs);
	$self->format_chr( $attrs->{ref} );
	$self->setting( 'lowres_binsize',  '1000000' );
	$self->setting( 'highres_binsize', '50000' );

	my $start = $attrs->{seg_start} || die "No seg_start supplied";
	my $stop  = $attrs->{seg_stop} || die "No seg_stop supplied";
	my $range = $stop - $start;
	
#	if ( $range < 50000 ) {
#		my $centre = $start + ( $range / 2 );
#		$start = $centre - 25000;
#		$stop  = $centre + 25000;
#	}
	$self->seg_start($start);
	$self->seg_stop($stop);
	
	if ( scalar( keys %messages ) > 0 ) {
		if ( $messages{notfound} ) {
			$self->add_textbox( "The following resultsets do not exist",
				$messages{notfound}, 'red', 'pink' );
		}

		if ( $messages{none} ) {
			$self->add_textbox(
				"No data access is provided for the following resultsets",
				$messages{none}, 'red', 'pink' );
		}

		if ( $messages{limited} ) {
			$self->add_textbox(
				"Limited data access is provided for the following resultsets",
				$messages{limited}, 'royalblue', 'lightblue'
			);
		}
	}

	#passed in from CGI param or session data
	$self->is_displayed( $attrs->{'is_displayed'} );

	my ( $bin_start, $bin_stop ) = $self->round_positions;

	#generate features based on resolution level
	if ( $self->is_high_resolution ) {
		$self->add_high_res_features;
	}
	else {
		$self->add_low_res_features;
	}

	return $self->features;
}

=head2 add_high_res_features

	  Usage      : $self->add_high_res_features;
	  Purpose    : Main method called to add high resolution-based features to feature list
	  Returns    : Nothing
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : Uses 'tracks' and 'settings' accessors to determine what to display and sizes etc.
	
=cut

sub add_high_res_features {

	my ($self) = @_;

	my $data = $self->get_marker_data;

	my $major_bin = $self->setting("lowres_binsize") / 1000000;
	my $minor_bin = $self->setting("highres_binsize") / 1000;

	my ($binned_data) = $self->calculate_binned_data(
		{
			bin_size => $self->setting("highres_binsize"),
			data     => $data
		}
	);
	my (
		$max_and_counts, $sig_markers, $multi_sig_markers,
		$max_pvalue,     $max_count
	) = $self->calculate_max_and_sig_counts( { data => $binned_data } );

	my $threshold = $self->threshold;
	$threshold eq 'ZERO' and $threshold = 0;
	if ( $self->track("stacked") ) {
		$self->add_line();

		$self->add_sig_counts_glyph( { 
			height => 50, 
			key => "Study data: Stacked counts of -log p-values >= $threshold ($minor_bin"
			  . "-kb regions)" 
		} );

		my ( $chr_max_count, $rs_counts ) = $self->add_sig_counts(
			{
				data     => $max_and_counts,
				bin_size => 15000,
			}
		);

		$self->features->set( 's', 'max_score' => $chr_max_count );
	}

	if ( $self->track("present") ) {
		$self->add_line();
		$self->add_heading(
			"Study data: Marker coverage for selected studies ($minor_bin"
			  . "-kb regions)" );

		$self->add_bins_present_glyph();
		$self->add_bins_present(
			{
				data     => $max_and_counts,
				bin_size => 15000,
			}
		);
	}

	if ( $self->track("line_trace") ) {
		$self->add_line();

		$self->add_pvalue_traces(
			{
				height     => 50,
				data       => $max_and_counts,
				max_pvalue => $max_pvalue,
				key => "Study data: Line plot of maximum -log p-values for selected studies ($minor_bin"
			  . "-kb regions)"
			}
		);
	}

	if ( $self->track("sig_markers_single") ) {
		$self->add_line();
		$self->add_sig_markers( { data => $sig_markers } );
	}

	if ( $self->track("sig_markers_multiple") ) {
		$self->add_line();
		$self->add_combined_sig_markers( { data => $multi_sig_markers } );
	}

}

=head2 add_low_res_features

	  Usage      : $self->add_low_res_features;
	  Purpose    : Main method called to add low resolution-based features to feature list
	  Returns    : Nothing
	  Arguments  : None
	  Throws     : 
	  Status     : Public
	  Comments   : Uses 'tracks' and 'settings' accessors to determine what to display and sizes etc.
	
=cut

sub add_low_res_features {
	my ($self) = @_;

	my $lowres_binsize = $self->setting("lowres_binsize") / 1000000;
	my $highres_binsize = $self->setting("highres_binsize") / 1000;
	my $threshold = $self->threshold;
	$self->log->info("adding lowres features");
	my $data = $self->get_marker_binned_data( { bin_size => $lowres_binsize } );
	$threshold eq 'ZERO' and $threshold = 0;
	
	$self->logscale(1);

	if ( $self->track("stacked") ) {

		$self->add_line();
		
		$self->add_sig_counts_glyph( { 
			height => 50, 
			key => "Study data: Stacked counts of -log p-values >= $threshold ($lowres_binsize"
			  . "-Mb regions) click a histogram bar to view region" 
		} );

		my ( $chr_max_count, $rs_counts ) = $self->add_sig_counts(
			{
				data           => $data->{ $self->chr },
				bin_size       => $self->setting("lowres_binsize"),
				bin_size_short => $lowres_binsize
			}
		);
		$self->features->set( 's', 'max_score' => $chr_max_count );
	}

	if ( $self->track("present") ) {
		$self->add_line();
		$self->add_heading(
			"Study data: Marker coverage for selected studies ($lowres_binsize"
			  . "-Mb regions)" );

		$self->add_bins_present_glyph();
		$self->add_bins_present(
			{
				data           => $data->{ $self->chr },
				bin_size       => $self->setting("lowres_binsize"),
				bin_size_short => $lowres_binsize
			}
		);
	}

	if ( $self->track("line_trace") ) {
		$self->add_line();
		$self->add_heading(
			"Study data: Line plot of maximum -log p-values for selected studies ($highres_binsize"
			  . "-kb regions) - zoom in to show" );
	}

	if ( $self->track("sig_markers_single") ) {
		$self->add_line();
		$self->add_heading(
"Study data: Significant markers for individual resultsets - zoom in to show"
		);
	}
	if ( $self->track("sig_markers_multiple") ) {
		$self->add_line();
		$self->add_heading(
"Study data: Significant markers across all selected resultsets - zoom in to show"
		);
	}
}

=head2 add_pvalue_traces

	  Usage      : $self->add_max_pvalues({
					 bin_size => $attrs->{bin_size}
				   });
	  Purpose    : Get max pvalues for specified bin size and add to feature list as overlapping traces
	  Returns    : Nowt
	  Arguments  : Hashref of:
	  				max_pvalue - maximum value in data set - used as max_score for glyph
	  				data - binned data to use
	                bin_size - size of bin to use
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_pvalue_traces {
	my ( $self, $attrs ) = @_;

	my $max_pval = $attrs->{max_pvalue};
	my $features = $self->features;
	my $ref      = $self->ref;
	my $data     = $attrs->{data};
	my $key = $attrs->{key};
	
	$features->add_type(
		maxp => {
			glyph       => 'stackplot',
			fgcolor     => 'red',
			bgcolors    => $self->config->{Browser}->{bgcolors},
			scale_color => 'black',
			graphtype   => 'line',
			height      => $attrs->{height},
			min_score   => 0,
			max_score   => (
				  $self->threshold > $max_pval
				? $self->threshold
				: $max_pval
			),
			scale           => 'left',
			bump            => 0,
			key             => 0,
			threshold       => $self->threshold,
			'balloon hover' => 'Maximum P values across this region',
			method          => 'trace',
			source          => 'HGVbaseG2P',
			point_symbol    => 'disc',
			key => $key,
		}
	);

#loop through items, create a subfeature array, finally add as a segment to a feature and add this to the feature list
	my @subfeatures;
	foreach my $item ( @{$data} ) {

		my $score = $item->{bin_max};

		my $subfeature = Bio::Graphics::Feature->new(
			-start => $item->{bin_start},
			-stop  => $item->{bin_stop},
			-score => $score,
			-ref   => $ref,
		);
		push @subfeatures, $subfeature;

	}

	my $f = Bio::Graphics::Feature->new(
		-segments => \@subfeatures,
		-type     => 'maxp'
	);
	$features->add_feature($f);

}

=head2 add_sig_markers

	  Usage      : $self->add_sig_markers( { data => $data } );
	  Purpose    : For each resultset, creates a marker glyph and adds supplied markers to feature list
	  Returns    : Nowt
	  Arguments  : Hashref of:
	                data - hashref of markers with resultsetid as key (see calculate_max_and_sig_counts)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_sig_markers {
	my ( $self, $attrs ) = @_;

	my $data          = $attrs->{data};
	my $features      = $self->features;
	my $ref           = $self->ref;
	my $feature_count = 0;

	my @bgcolors =
	  split( /\s/, $self->config->{Browser}->{bgcolors} );

	my $rscounter = 0;

	#loop through each resultset in data and add a dot glyph
	foreach my $rs_ident ( @{ $self->resultsets_and_uploads } ) {
		my $unique_name;
		my $type = "sigmarkers".$rs_ident;
		if ($rs_ident =~ /^U/) {
			my $upload_data = $self->upload_data->{$rs_ident};
			$unique_name = $upload_data->{study_name}." - ".$upload_data->{resultset_name};
		}
		else {
			$unique_name = $self->summaries->{$rs_ident}->unique_name;
		}		
		$features->add_type(
			$type => {
				bgcolor => $bgcolors[$rscounter],
				glyph   => 'dot',
				fgcolor => 'black',
				label   => 1,
				bump    => 1,
				title   => 1,
				height  => 8,
				clip    => 'true',
				scale   => 'none',
				'balloon hover' => $rs_ident !~/^U/ ?
'sub { my $feature = shift; return "Click to view significance" }' : 'sub { my $feature = shift; return "Significance is not in database" }',
				key => "Study data: Significant markers - ".$unique_name
			}
		);

#loop through each significant marker for the resultset and add it to the features
		my $sig_count = 0;
		
		foreach my $sig ( @{ $data->{$rs_ident} } ) {
			if ($sig) {
				my $url =
				    "goToPage('marker/"
				  . $sig->{'Marker_Identifier'}
				  . "/results?v=table&rfilter="
				  . $sig->{Resultset_Identifier} . "&t="
				  . $self->threshold . "')";

				my $feature = Bio::Graphics::Feature->new(
					-start => $sig->{'Start'},
					-stop  => $sig->{'Stop'},
					-score => $sig->{'NegLogPValue'},
					-ref   => $ref,
					-type  => 'Marker' . $rs_ident,
					-name  => $sig->{'Marker_Accession'} . " (-log p="
					  . sprintf( "%.2f", $sig->{'NegLogPValue'} ) . ")",
					-url => ($rs_ident !~/^U/ and $url),
				);

				$features->add_feature( $feature => $type );
				$sig_count++;
			}

		}

		#if no significant markers for a resultset - highlight this
		if ( $sig_count == 0 ) {
			$self->add_heading("Study data: Significant markers - ".$unique_name." - none present in this region");
		}
		$rscounter++;
		$rscounter != scalar( @{ $self->resultsets } )
		  and $self->add_dashed_line;
	}
}

=head2 add_combined_sig_markers

	  Usage      : $self->add_combined_sig_markers( { data => $data } );
	  Purpose    : For marker data provided (significant in multiple resultsets) adds markers to feature list
	  Returns    : Nowt
	  Arguments  : Hashref of:
	                data - hashref of markers with markerid as key (see calculate_max_and_sig_counts)
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub add_combined_sig_markers {
	my ( $self, $attrs ) = @_;

	my $data = $attrs->{data};

	my $features      = $self->features;
	my $ref           = $self->ref;
	my $feature_count = 0;

	my $rscounter = 0;

	#add diamond glyph to represent multi markers
	my $type = "multisig";
	$features->add_type(
		$type => {
			bgcolor => 'black',
			glyph   => 'diamond',
			fgcolor => 'black',
			label   => 1,
			bump    => 1,
			title   => 1,
			height  => \&marker_size,
			width   => \&marker_size,
			clip    => 'true',
			scale   => 'none',
			'balloon hover' =>
'sub { my $feature = shift; return "Click to view significances" }',
			key => "Study data: Significant markers across all selected resultsets",
		}
	);

	my $sig_count = 0;

	#loop through each marker
	foreach my $marker ( keys %{$data} ) {
		my @sigids = ();

		#for each marker, add significance IDs into an array
		my @multi_sigs = @{ $data->{$marker} };
		foreach (@multi_sigs) {
			push @sigids, $_->{'Resultset_Identifier'};
		}
		my $sig      = $multi_sigs[0];
		my $rs_count = scalar(@multi_sigs);

#only add the feature if the number of resultsets they are signficant in passes the threshold set by the user
#use the array of significances in the javascript call (showSigs)
		if ( $rs_count >= $self->setting('resultsets_with_sig') ) {
			my $url =
			    "goToPage('marker/"
			  . $sig->{'Marker_Identifier'}
			  . "/results?v=table&rfilter="
			  . join( ",", @sigids ) . "&t="
			  . $self->threshold . "')";
			my $feature = Bio::Graphics::Feature->new(
				-start => $sig->{'Start'},
				-stop  => $sig->{'Stop'},
				-score => scalar(@multi_sigs),
				-ref   => $ref,
				-type  => 'MarkerAll',
				-name  => $sig->{'Marker_Accession'} . " ("
				  . $rs_count
				  . " resultsets)",
				-url => $url
			);

			$features->add_feature( $feature => $type );
			$sig_count++;
		}

	}

	#if none significant in multiple resultset highlight this
	if ( $sig_count == 0 ) {
		$self->add_heading("Study data:Significant markers across all selected resultsets - none present in this region");
	}
	$rscounter++;

}

=head2 compare_resultsets

	  Usage      : my ($resultsetid, $get_info,$resultsetinfo) = $self->compare_resultsetid($par_resultsetid, $sess_resultsetid);
	  Purpose    : Compare analysis method ID retrieved from parameter and session and returns get_info flag and other useful info
	  Returns    : $resultsetid - resultsetid to use
	               $get_info - flag to specify whether to read DB for study info 
	               $resultsetinfo - method info for empty resultsetid
	  Arguments  : $par_resultsetid - resultsetid from CGI parameter
	               $sess_resultsetid - resultsetid from session parameter
	  Throws     : 
	  Status     : Public
	  Comments   : 
	
=cut

sub compare_resultsets {
	my ( $self, $par_resultsets, $sess_resultsets ) = @_;
	my ( $resultsets, $get_info, $rsinfo );
	if ( $par_resultsets && $sess_resultsets ) {

		if ( $par_resultsets ne $sess_resultsets ) {
			$sess_resultsets = $par_resultsets;
			$resultsets      = $par_resultsets;
			$get_info        = 1;
		}
		else {
			$resultsets = $par_resultsets;
			$get_info   = 1;
		}
	}
	elsif ( $par_resultsets && !$sess_resultsets ) {
		$resultsets = $par_resultsets;
		$get_info   = 1;
	}
	elsif ( !$par_resultsets && $sess_resultsets ) {
		$resultsets = $sess_resultsets;
		$get_info   = 1;
	}
	else {
		$rsinfo = "No Result Set";
	}
	return ( $resultsets, $get_info, $rsinfo );
}

=head2 marker_size

	  Usage      : height          => \&marker_size,
	  Purpose    : GBrowse callback to calculate size of glyph depending on feature score (used by add_combined_sig_markers)
	  Returns    : 4*score or 1 if no score
	  Arguments  : Dealt with by GBrowse
	  Throws     : 
	  Status     : Public
	  Comments   : 
=cut

sub marker_size {
	my ($feature) = @_;
	my $val = $feature->score;
	if ( !$val ) {
		return 1;
	}
	else {
		return $val * 4;
	}
}
