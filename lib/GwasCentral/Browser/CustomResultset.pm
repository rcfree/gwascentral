# $Id$

=head1 NAME

HGVbaseG2P::Search::Filter::PhenotypeID


=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 SUBROUTINES/METHODS 

=cut

package GwasCentral::Browser::CustomResultset;
use Moose;
use MooseX::Aliases;

extends qw(GwasCentral::Base);
use Data::Dumper qw(Dumper);
has 'resultset_identifier' => ('is'=>'rw', 'alias' => 'identifier');
has 'resultset_name' => ('is'=>'rw', 'alias' => 'name');
has 'phenotype_name' => ('is'=>'rw');
has 'phenotype_ident' => ('is'=>'rw');
has 'study_name' => ('is'=>'rw');
has 'no_coords_ids' => ('is'=>'rw');
has 'dead_ids' => ('is'=>'rw');
has 'study_ident' => ('is'=>'rw');
has 'success_count' => ('is'=>'rw');
has 'failed_pvalues' => ('is'=>'rw');
has 'failed_ids' => ('is'=>'rw');
has 'changed_ids' => ('is'=>'rw');
has 'accesslevel' => ('is'=>'rw','default'=>'upload');
1;