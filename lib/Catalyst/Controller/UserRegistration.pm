# $Id: UserRegistration.pm 1447 2010-05-26 14:25:42Z rcf8 $

package Catalyst::Controller::UserRegistration;

=head1 NAME

  Catalyst::Controller::UserRegistration - Custom Catalyst Controller for registration of users

=head1 DESCRIPTION

  This class is a controller that deals with login, registration, user profiles and other user related things

=head1 METHODS

TODO

=cut

use strict;
use warnings;
use base qw(Catalyst::Controller::reCAPTCHA);
use Digest;
use Data::Dumper qw(Dumper);
use MIME::Lite;
use Crypt::GeneratePassword qw(chars);
use HTML::Strip;

sub begin : Private {
	my ($self, $c) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
}

sub before_register {
	my ( $self, $c ) = @_;
	$c->log->info("before_register empty");
}

sub after_register {
	my ( $self, $c ) = @_;
	$c->log->info("after_register empty");
}

sub before_login {
	my ( $self, $c ) = @_;
	$c->log->info("before_login empty");
}

sub after_login {
	my ( $self, $c ) = @_;
	$c->log->info("after_login empty");
}

sub before_logout {
	my ( $self, $c ) = @_;
	$c->log->info("before_logout empty");
}

sub after_logout {
	my ( $self, $c ) = @_;
	$c->log->info("after_logout empty");
}

sub get_user_by_id {
	my ($self, $c, $userid) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};
	
	my $userid_field = $config->{userid_field};
	my $user = $c->model($realm->{store}->{user_model})->search({$userid_field=>$userid})->single;
	return $user;
}

sub get_user_by_activation_code {
	my ($self, $c, $activation_code) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};
	
	my $userid_field = $config->{userid_field};
	my $user = $c->model($realm->{store}->{user_model})->search({'activation_code'=>$activation_code})->single;
	return $user;
}

sub encrypt_password {
	my ($self, $c, $password) = @_;
	
	my $config = $c->config->{'Controller::UserRegistration'};
	
	#use password type specified in realm and hash it as required
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};

	my $password_type = $realm->{credential}->{password_type};
	
	if ($password_type eq 'hashed') {
		$c->log->info("password hashed");
		my $hash = Digest->new($realm->{credential}->{password_hash_type});
		$hash->add($password);
		$password = $hash->digest;
	}
	return $password;
}

sub create_user {
	my ( $self, $c, $activation_code ) = @_;
	
	my $config = $c->config->{'Controller::UserRegistration'};
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};
	
	
	my $userid_field = $config->{userid_field};
	my @fields = (split( /\ /, $config->{required_fields} ), split( /\ /, $config->{optional_fields}));
	unshift @fields, $config->{userid_field};
	
	my $password = $self->encrypt_password($c,$c->req->param($config->{password_field}));
	
	#strip HTML from field values
	my $hs = HTML::Strip->new();
	my %user_hash = map { $_ => $hs->parse($c->req->param($_)) } @fields;
	
	#set password later because it may be hashed (so not the original parameter)
	$user_hash{$config->{password_field}}=$password;
	$user_hash{activation_code}=$activation_code;
	
	#create user and deal with errors	
	eval {
		#store User via model
		$c->model($realm->{store}->{user_model})->create(\%user_hash);
	};
	if ($@) {
		push @{$c->stash->{'user_errors'}}, "A problem occurred: $@";
		return;
	}
}

sub update_user {
	my ( $self, $c ) = @_;
	
	my $config = $c->config->{'Controller::UserRegistration'};
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};
	my $userid_field = $config->{userid_field};
	
	my $curr_user = $self->get_user_by_id($c, $c->req->param("curr_$userid_field"));
	
	
	my @fields = (split( /\ /, $config->{required_fields} ), split( /\ /, $config->{optional_fields}));
	
	unshift @fields, $userid_field;
	$c->log->info("fields:".Dumper(\@fields));
	
	#strip HTML from field values
	my $hs = HTML::Strip->new();
	my %user_hash = map { $_ => $hs->parse($c->req->param($_)) || undef } @fields;
	$c->log->info("updated:".Dumper(\%user_hash));
	
	eval {
		#store User via model
		$curr_user->update(\%user_hash);
		
	};
	if ($@) {
		push @{$c->stash->{'user_errors'}}, "A problem occurred: $@";
		return;
	}
	$c->log->info("curr_user updated:".Dumper({$curr_user->get_columns()}));
	$c->user($curr_user);
}

sub login : Regex('^user/login$') {
	my ( $self, $c ) = @_;
	$c->log->info("gets to login");
	my $config = $c->config->{'Controller::UserRegistration'};
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	
	$c->stash->{template}='user/login.tt2';
	if ($c->user_exists) {
		$c->stash->{user_messages}="User '".$c->user->email."' is already logged in. <a href='/user/logout'>Log out</a>";
		$c->stash->{no_page}=1;
		return;
	}
	
	if (!$c->req->param('postback')) {
		$c->log->info("no postback");
		if ($config->{redirect_to_url}) {
			$c->req->param('returnurl') and $c->session->{returnurl}=$c->req->param('returnurl');
		}
		return;
	}
	
	$self->before_login($c);
	
	#$c->log->info("user_params:".Dumper($c->req->params));
	
	my $userid   = $c->req->param( $config->{userid_field} );
	my $password = $c->req->param( $config->{password_field} );

	my $returnurl = $c->session->{returnurl};

	if (
		$c->authenticate(
			{
				$config->{userid_field}   => $userid,
				$config->{password_field} => $password
			}, $config->{user_realm}
		)
	  )
	{
		if ($c->user->activation_code) {
			$c->log->info("activation code still present");
			$config->{redirect_to_url}=undef;
			$c->forward('logout');
			$c->stash->{template} = 'user/login.tt2';
			$c->stash->{user_errors} = "Account not activated. You should have been sent an email containing a link to activate your account.";
			return;
		}
			
		$c->log->info("authenticates: $userid => $password");
		$self->after_login($c);
		
		if ($config->{redirect_to_url}) {
			$c->response->redirect($c->session->{returnurl} || $config->{default_redirect_url});
		}
	}
	else {
		$c->stash->{user_errors} = "Email address or password not recognised. Please try again.";
	}
}

sub register : Path('user/register') {
	my ( $self, $c ) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	$c->user and $c->res->redirect($config->{default_redirect_url});
	
	$c->stash->{template} = 'user/register.tt2';
	
	$c->forward('captcha_get');
	return if !$c->req->param('postback');
	
	$c->stash->{user_fields}=$c->req->params;
	$c->log->info("before_register");
	$self->before_register($c);

	my @errors = ();

	
	my @fields = split( /\ /, $config->{required_fields} );

	unshift @fields, $config->{password_field};
	unshift @fields, $config->{userid_field};
	
	if ( !$c->forward('captcha_check') ) {
		$c->stash->{user_errors} = "There was a problem with your recaptcha entry.";
		return;
	}
	
	if ($self->get_user_by_id($c, $c->req->param($config->{userid_field}))) {
		$c->stash->{user_errors} = "This ". $config->{userid_field}." is already present. Please choose a different one.";
		return;
	}
	
	$c->log->info("check fields");
	foreach my $field (@fields) {
		if ( !$c->req->param($field) ) {
			push @errors, "You did not enter a $field";
		}
	}
	
	$c->log->info("check passwords");
	
	#check if passwords do not match
	if ( $config->{confirm_password_field} ) {
		my $conf_pwd_field = $config->{confirm_password_field};
		my $pwd_field      = $config->{password_field};
		if ( $c->req->param($conf_pwd_field) ne $c->req->param($pwd_field) ) {
			push @errors,"Passwords do not match";
		}
	}
	
	#if errors then put them in the user_errors accessor
	if ( scalar( @errors > 0 ) ) {
		$c->stash->{user_errors}=\@errors;
		return;
	}
	
	my $activation_code = generate_word(20);
	 
	#create user - errors are caught
	eval { $self->create_user($c, $activation_code); };
	if ( scalar( @errors > 0 ) ) {
		return;
	}
	
	$c->log->info("after_register");
	$self->after_register($c);
	
	my $email_field = $config->{email_field};
	my $rconfig = $config->{'email_user'};
	
	eval {
		$self->send_email($c,$c->req->param($email_field), $rconfig->{activate_subject}, $rconfig->{activate_text}." \n\nTo activate your account copy '".$rconfig->{activate_url}."/$activation_code' and paste it into your browser address bar or click this <a href='".$rconfig->{activate_url}."/$activation_code'>activation link</a><br/>The link will expire in 48 hours.");
	};
	if ($@) {
		$c->stash->{user_errors}="An error occurred while sending the activation email:$@.";
		return;
	}
#	if ($config->{login_after_register}) {
#		$c->detach('dologin');
#	}
}

sub changepassword : Path('user/changepassword') {
	my ( $self, $c ) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	
	$c->stash->{template} = 'user/changepassword.tt2';

	return if !$c->req->param('postback');

	my @errors = ();
	
	$c->log->info("check fields");
	
	if ( !$c->req->param($config->{password_field}) ) {
		push @errors, "You did not enter a password";
	}
	
	#check old password
	my $encrypted_old_password = $self->encrypt_password($c, $c->req->param('old_password'));
	#my $conf_pwd_field = $config->{confirm_password_field};
	#my $pwd_field      = $config->{password_field};
	if ( $encrypted_old_password ne $c->user->password ) {
		push @errors,"You entered the wrong old password";
	}
	
	
	#check if passwords do not match
	my $conf_pwd_field = $config->{confirm_password_field};
	my $pwd_field      = $config->{password_field};
	if ( $c->req->param($conf_pwd_field) ne $c->req->param($pwd_field) ) {
		push @errors,"New passwords do not match";
	}
	
	#if errors then put them in the user_errors accessor
	if ( scalar( @errors > 0 ) ) {
		$c->stash->{user_errors}=\@errors;
		return;
	}
	 
	#create user - errors are caught
	my $userid = $c->req->param($config->{userid_field});
	eval { 
		$self->update_password($c, $userid, $c->req->param($pwd_field)); 
	};
	if ( scalar( @errors > 0 ) ) {
		return;
	}
}

sub logout : Regex('^user/logout$') {
	my ( $self, $c ) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	$c->log->info("config:".Dumper($config));
	$c->req->param('returnurl') and $c->session->{returnurl}=$c->req->param('returnurl');
	
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	if (!$c->user_exists) {
		$c->stash->{errors}="Not logged into system";
	}
	$self->before_logout($c);
	$c->logout();
	$self->after_logout($c);
	
	if ($config->{redirect_to_url}) {
		$c->response->redirect($c->session->{returnurl} || $config->{default_redirect_url});
	}
}

sub update_password {
	my ($self, $c, $userid, $password) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	
	my $realm = $c->config->{'Plugin::Authentication'}->{realms}->{$config->{user_realm}};
	
	$c->log->info("update_password: password updated with $password");
	
	#update password via model
	my $encrypted_password = $self->encrypt_password($c,$password);
	$c->model($realm->{store}->{user_model})->search({ $config->{userid_field} => $userid})->update({$config->{password_field} => $encrypted_password});
	$c->user and $c->persist_user;
}

sub resetpassword : Path('user/resetpassword') {
	my ($self, $c) = @_;
	
	my $config = $c->config->{'Controller::UserRegistration'};
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	
	$c->stash->{template}='user/resetpassword.tt2';
	return if !$c->req->param('postback');
	
	
	my $userid = $c->req->param($config->{userid_field});
	my $user = $self->get_user_by_id($c, $userid);
	my $email_field = $config->{email_field};
	my $password_field = $config->{password_field};
	
	if (!$user) {
		$c->stash->{user_errors} = "The ".$email_field." you entered has not been registered.";
		return;
	}

   my $new_password = generate_word(10);
   $self->update_password($c,$userid, $new_password);
   my $rconfig = $config->{'email_user'};
   $self->send_email($c,$user->$email_field, $rconfig->{reset_subject}, $rconfig->{reset_text}." \n\nThe new password is: ".$new_password);
}

sub generate_word {
	my ($length) = @_;
	#generate word using 'Crypt::GeneratePassword'
	my $char_set = [qw(a b c d e f g h i j k l m n o p q r s t u v w x y z 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)];
	chars($length,$length,$char_set);
}

sub send_email {
	my ($self, $c, $to, $subject, $body) = @_;
	my $config = $c->config->{'Controller::UserRegistration'};
	
	my $rconfig = $config->{'email_user'};
	my $msg = MIME::Lite->new;
   $msg->attr("content-type"         => "text/html");
   $msg->attr("content-type.charset" => "latin1");
   $msg->build(
       From     =>$rconfig->{from},
       To       => $to,
	   Encoding =>'quoted-printable',
       Subject  => $subject,
       Data=>$body
   );
#   my $part = MIME::Lite->new(
#   	Type=>'text/html',
#   	Data => $body
#   	);
   	
   
   MIME::Lite->send($rconfig->{type},$rconfig->{server},Debug=>0,Port=>$rconfig->{port} || 25);
   #$msg->attach($part);
   $c->log->info("email:".Dumper($msg));
   $msg->send;
}

sub activate :Regex('^user/activate/(\w+)$')  {
	my ($self, $c) = @_;
	
	$c->stash->{template}="user/activate.tt2";
	
	my $activation_code = $c->req->captures->[0];
	my $user = $self->get_user_by_activation_code($c,$activation_code);
	
	if (!$user) {
		$c->stash->{user_errors} = "The activation code was not recognised, or the account has already been activated.";
		return;
	}
	$user->update({'activation_code'=>undef});
}	

sub profile :Path('user/profile') {
	my ($self, $c) = @_;
	
	my $config = $c->config->{'Controller::UserRegistration'};
	my $userid_field = $config->{userid_field};
	my $curr_userid = $c->req->param("curr_$userid_field");
	my $userid = $c->req->param($userid_field);
	
	$config->{disable} and $c->res->redirect($config->{default_redirect_url});
	!$c->is_user_logged_in and $c->res->redirect($config->{default_redirect_url});
	
	$c->stash->{template}="user/profile.tt2";
	return if !$c->req->param('postback');

	my @errors = ();
	my @fields = split( /\ /, $config->{required_fields} );

	unshift @fields, $config->{userid_field};
	
	if ($userid ne $curr_userid and $self->get_user_by_id($c, $userid)) {
		$c->stash->{user_errors} = "This ". $config->{userid_field}." has already been used. Please choose a different one.";
		return;
	}
	
	$c->log->info("check fields");
	foreach my $field (@fields) {
		if ( !$c->req->param($field) ) {
			push @errors, "You did not enter a $field";
		}
	}
	
	#if errors then put them in the user_errors accessor
	if ( scalar( @errors > 0 ) ) {
		$c->stash->{user_errors}=\@errors;
		return;
	}
	
	my $user;
	#create user - errors are caught
	eval { $self->update_user($c); };
	if ( $c->stash->{user_errors} ) {
		return;
	}
	
	$c->stash->{user_messages}="User profile updated successfully";
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: UserRegistration.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut