# $Id: OpenID.pm 1447 2010-05-26 14:25:42Z rcf8 $

package Catalyst::Controller::OpenID;

=head1 NAME

  Catalyst::Controller::OpenID - Custom Catalyst Controller for openID pages (incomplete)

=head1 DESCRIPTION

  This controller that deals with creating templates for login of new and old users via openID.
  It is used in conjunction with Catalyst::Authentication::OpenID which provides the logic for the login.

=head1 METHODS

TODO

=cut

use strict;
use warnings;
use base qw(Catalyst::Controller);

sub openid :Path('user/openid/login') {
	my ($self, $c) = @_;
	$c->stash->{returnurl}=$c->req->param('returnurl');
	$c->stash->{template}='user/openid/login.tt2';
}

sub openid_dologin :Path('user/openid/dologin') {
	my ($self, $c) = @_;
	$c->log->info("dologin_params:".Dumper($c->req->params));
	$c->req->param('returnurl') and $c->session->{returnurl}=$c->req->param('returnurl');
		
	my $returnurl = $c->session->{returnurl};
	
	eval {
		if ($c->authenticate()) {
			$c->log->info("catalyst user:".Dumper($c->user));
			my $acontrol = $c->get_access_control;
			$c->session->{is_admin} = $acontrol->is_user_admin;
			
			if ($c->stash->{user_error} =~ /does not exist/) {
				$c->user->{unregistered}=1;
				$c->stash->{template}='user/register.tt2';
				return;
			}
			
			$c->log->info("logged in as:".Dumper($c->user->{url})." now redirecting to $returnurl");
			$c->log->info("user:".Dumper($acontrol->user));
			$c->user->{nickname}=$acontrol->user->{nickname};
			$c->response->redirect($returnurl);
		}
	};
	if ($@) {
		$c->stash->{template}='user/login.tt2';
		$c->stash->{user_error}="An error occurred while attempting login. Please try again.<br/>The error was: ".$@;
		return;
	}
}

sub openid_new :Path('user/openid/new') {
	my ($self,$c) = @_;
	$c->stash->{template}='user/registeropenid.tt2';
	
	#terms not accepted - return to register page with message
	if (!$c->req->param('accept_terms') && $c->req->param('postback')) {
		$c->stash->{error}="You must accept the terms and conditions";
	}
	
	#create user and deal with errors
	my $ac = $c->get_access_control;
	eval {
		$ac->create_user($c->req->param('openid_identity'),$c->req->param('nickname'));
	};
	if ($@) {
		$c->stash->{error}="Error registering OpenID:$@";
	}
	
	#set user to fully registered and redirect to previous url
	my $returnurl = $c->session->{returnurl};
	$c->user->{unregistered}=undef;
	$c->user->{nickname}=$c->req->param('nickname');
	$c->res->redirect($returnurl);
}
1;
=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>


=head1 LICENCE AND COPYRIGHT

Copyright (c) <2010> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=head1 CVSINFO

$Id: OpenID.pm 1447 2010-05-26 14:25:42Z rcf8 $ 

=cut