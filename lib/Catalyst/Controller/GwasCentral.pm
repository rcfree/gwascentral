package Catalyst::Controller::GwasCentral;
use base qw(Catalyst::Controller);
use FindBin;
use lib "$FindBin::Bin/../../../lib";

use GwasCentral::Search::Util qw(new_Query);
use File::Basename;
use GwasCentral::Search::Query::Basic;
use GwasCentral::AccessControl::None;

#use HGVbaseG2P::Web::Util qw(add_uri_parameters);
#use HGVbaseG2P::AccessControl;
use Carp qw(cluck);
use Moose;
use GwasCentral::DataSource::Util
  qw(DataSources_from_context new_AccessControl);
use GwasCentral::Base qw(load_module);
use JSON::XS;
use URI;
use GwasCentral::Web::Output;
use Data::Dumper qw(Dumper);

=head2 begin

Begin is run prior to default. Here core variables are set up for use by templates.
The most important is initialisation of the session if it has not been populated already.
P3P is also added to the header so that it plays nicely in IE.
The 'w3c/p3p.xml' file contains this policy information for the usage of data etc.

Also set up are base_ values for resultsets, studies and phenotypes
and a GUID which can be used to prevent file caching in browsers.

=cut

sub get_base_url {
	my ( $self, $c ) = @_;
	my @url = split( /,/, $c->req->base );
	$url[0] =~ s/\/$//;
	return $url[0];
}

sub begin : Private {
	my ( $self, $c ) = @_;
	$c->req->params->{sessionid}=$c->sessionid;
		
	eval {
		
		$c->stash->{base_url} = $self->get_base_url($c);
		
		#P3P added to header for IE6 cookies
		$c->res->header(
			P3P => 'policyref="' . $self->get_base_url($c) . '/w3c/p3p.xml"' );

		tie my %tabs, "Tie::Hash::Indexed";
		%tabs = (
		'Phenotypes' => '/phenotypes',
		'Genes' => '/genes',
		'Markers' => '/markers',
		'Studies' => '/studies',
		'Browser' => '/browser',
		'GWAS Mart' => '/biomart/martview?VIRTUALSCHEMANAME=default&amp;ATTRIBUTES=study.default.study.identifier|study.default.study.name&amp;FILTERS=&VISIBLEPANEL=filterpanel'
		);
		$c->stash->{tabs}=\%tabs;
		
		my $ac = $self->get_AccessControl($c);
	
		$c->model("Study")->AccessControl($ac);
	
		$self->store_settings($c);
	};
	if ($@) {
		$c->stash->{error}=$@;
	}
}

sub store_settings {
	my ( $self, $c ) = @_;
	my $basic =
	  GwasCentral::Search::Query::Basic->new( { conf_file => $c->config } );

	#populate browser params with browser settings session data first
	$basic->params( $c->session->{browser_settings} || {} );
	#update changes using URL params
	$basic->update_params( $c->req->params );
	
	$c->session->{browser_settings} = $basic->include_params( ['r[]','t'] );
	$basic->DataSources( DataSources_from_context($c) );
	$basic->f('AddedResultsets')->upload_data( $c->session->{upload_data} );
	
	$basic->prepare_Filters();
	$c->stash->{basic} = $basic;
}

sub get_host {
	my ( $self, $c ) = @_;
	return $c->req->uri->scheme . "://" . $c->req->uri->authority . "/";
}

sub get_AccessControl {
	my ( $self, $c ) = @_;
	my $ac_module = $c->config->{control_module} || 'None';

	my $acontrol = new_AccessControl(
		$c->config,
		{
			local_url     => $self->get_host($c),
			identity_hash => $c->session->{identity_hash}
		}
	);

	return $acontrol;
}

sub is_user_logged_in {
	my ( $self, $c ) = @_;
	return 1 if $c->user_exists and !$c->user->{unregistered};
}

sub do_search {
	my ( $self, $c, $type, $ignore_non_html, $pre_search_callback ) = @_;
	my $attrs = { context => $c };
	
	my $q;
	#eval {
		$q = new_Query( $type, $attrs );
		$q->params( $c->session->{browser_settings} );
		$q->host($self->get_host($c));
		$q->update_params( $c->req->params );
		$c->session->{browser_settings} = $q->include_params( ['r[]','t'] );
		$pre_search_callback and $pre_search_callback->($q);
		
		$c->log->info("params:".Dumper($q->params));
		$q->prepare_Filters;
		$c->log->info("Filters:".Dumper($q->Filters));
	#};
#	if ($@) {
#		$c->stash->{error}="Unknown error preparing query:".$@;
#		return;
#	}
	
	#place output into the 'body' of the response unless HTML, then put it into the stash as {search}->{[query_type]}
	#eval {
		if ( $q->Format->is_html ) {
			$c->res->content_type('text/html');
			if ($q->no_search_if_html) {
				$c->stash->{search}->{ $q->name } = {
					Query   => $q,
				};
			}
			else {
				$c->stash->{search}->{ $q->name } = {
					content => $q->results,
					Query   => $q,
				};
			}
		}
		else {
			return $q if $ignore_non_html;
			if ( $q->Format->is_redirect ) {
				$c->log->info("redirect_url:". $q->Format->redirect_url );
				$c->res->redirect( $q->Format->redirect_url );
			}
			else {
				$c->res->content_type( $q->Format->content_type );
				$q->Format->base_url( $self->get_base_url($c) );
				if ( $q->Format->is_download ) {
					my $filename = $q->Format->filename;
					if ($filename) {
						$c->res->headers->header(
							"Content-Disposition" => "attachment; filename="
							  . $filename );
					}
				}
				$c->res->headers->header( "Content-Transfer-Encoding" => "binary" );
				$c->res->body( $q->output );
			}
	
		}
	#};
#	if ($@) {
#		$c->stash->{error}="Unknown error getting results:".$@;
#		return;
#	}
	
	return $q;
}

sub output_report {
	my ( $self, $c, $item_name, $item ) = @_;

	my $format = $c->req->param('format');
	return if !$format || $format eq 'html';

	eval {
		my $wlogic =
		  GwasCentral::Web::Output->new( { conf_file => $c->config } );
		my ( $output, $content_type ) =
		  $wlogic->output_item( $item, $item_name, $format );

		$c->res->body($output);
		$c->res->content_type($content_type);
	};
	$@ and $c->stash->{error} = $@;
}

sub output_json {
	my ( $self, $c, $jsonp_data, $prefix, $suffix ) = @_;

	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(0);
	my $json = $json_coder->encode($jsonp_data);
	$c->res->output( ( $prefix || '' ) . $json . ( $suffix || '' ) );
}

sub json {
	my ( $self, $c, $json_data ) = @_;

	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(0);
	my $json = $json_coder->encode($json_data);
	return $json;
}

sub output_div {
	my ( $self, $c, $div_data, $prefix, $suffix ) = @_;
	my @divs = ();
	foreach my $div_key ( keys %{ $div_data || {} } ) {
		push @divs, $div_key . "=" . $div_data->{$div_key};

#$divs.="<div name='".$div_key."' id='".$div_key."'>".$div_data->{$div_key}."</div>";
	}
	$c->res->output(
		"<html><head><title>" . join( ";", @divs ) . "</title></head></html>" );
}

1;
