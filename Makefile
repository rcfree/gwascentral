# This Makefile is for the HGVbaseG2P extension to perl.
#
# It was generated automatically by MakeMaker version
# 6.54 (Revision: 65400) from the contents of
# Makefile.PL. Don't edit this file, edit Makefile.PL instead.
#
#       ANY CHANGES MADE HERE WILL BE LOST!
#
#   MakeMaker ARGV: ()
#
#   MakeMaker Parameters:

#     AUTHOR => q[Rob Free <rcfree@gmail.com>]
#     DIR => []
#     DISTNAME => q[HGVbaseG2P]
#     EXE_FILES => [q[script/assignMarkerAllelesets.pl], q[script/assignMarkerIDs.pl], q[script/backupDatabases.pl], q[script/broadT2Dliftover.pl], q[script/calculateMarkersImported.pl], q[script/clearOldData.pl], q[script/createHGVmart.pl], q[script/deletedMarkerFromdbSNP.pl], q[script/deploySystem.pl], q[script/generateBrowserDB.pl], q[script/generateBulkdata.pl], q[script/generateImages.pl], q[script/generateMarkerSummaries.pl], q[script/hgvbaseg2pweb_cgi.pl], q[script/hgvbaseg2pweb_create.pl], q[script/hgvbaseg2pweb_fastcgi.pl], q[script/hgvbaseg2pweb_server.pl], q[script/hgvbaseg2pweb_test.pl], q[script/importData.pl], q[script/importHapmap.pl], q[script/indexForTextsearch.pl], q[script/legacyMarkerFromdbSNP.pl], q[script/liftoverFromAny.pl], q[script/loadMarkerGFF.pl], q[script/markerGFFFromAny.pl], q[script/markerGFFFromDatabase.pl], q[script/markerMergeXMLFromdbSNP.pl], q[script/markerResultsets.pl], q[script/markerXMLFromDBGV.pl], q[script/markerXMLFromdbSNP.pl], q[script/markerXMLFromUniSTS.pl], q[script/optimiseBrowserDB.pl], q[script/optimiseMarkerDB.pl], q[script/optimiseStudyDB.pl], q[script/packData.pl], q[script/phenoXMLFromdbGaP.pl], q[script/precalculateBins.pl], q[script/refreshSchema.pl], q[script/scheduleJobs.pl], q[script/serverCheck.pl], q[script/storeXML.pl], q[script/updateLegacyMarkers.pl], q[script/updateMarkerMergehistory.pl], q[script/validateXML.pl], q[script/viewWebPerformance.pl]]
#     NAME => q[HGVbaseG2P]
#     NO_META => q[1]
#     PL_FILES => {  }
#     PREREQ_PM => { parent=>q[0], Catalyst::Plugin::Static::Simple=>q[0], Config::General=>q[0], Catalyst::Plugin::ConfigLoader=>q[0], ExtUtils::MakeMaker=>q[6.42], Catalyst::Runtime=>q[5.80029], Catalyst::Action::RenderView=>q[0] }
#     VERSION => q[4.0]
#     dist => { PREOP=>q[$(PERL) -I. "-MModule::Install::Admin" -e "dist_preop(q($(DISTVNAME)))"] }
#     realclean => { FILES=>q[MYMETA.yml] }
#     test => { TESTS=>q[t/00_compile.t t/00_db.t t/00_pubmed.t t/00_util.t t/01_fileparser.t t/01_podcoverage.t t/02_preprocess.t t/02_schema.t t/03_dataimport.t t/03_markerimport.t t/04_freqimport.t t/05_associmport.t t/06_dataimport_export_db.t t/06_dataimport_logicmaker.t t/06_dataimport_retrievemarker.t t/06_dataimport_rule_allelematch.t t/06_dataimport_rule_pos.t t/06_dataimport_rule_strandflip.t t/10_webapp_basic.t t/10_webapp_pages.t t/11_webapp_json.t t/20_hgvmart_compactor.t t/20_hgvmart_general.t t/30_browser_bin.t t/30_browser_core.t t/30_browser_db.t t/40_search_query.t t/70_search_query.t t/99_phenotype_bdown.t] }

# --- MakeMaker post_initialize section:


# --- MakeMaker const_config section:

# These definitions are from config.sh (via /System/Library/Perl/5.10.0/darwin-thread-multi-2level/Config.pm).
# They may have been overridden via Makefile.PL or on the command line.
AR = ar
CC = gcc-4.2
CCCDLFLAGS =  
CCDLFLAGS =  
DLEXT = bundle
DLSRC = dl_dlopen.xs
EXE_EXT = 
FULL_AR = /usr/bin/ar
LD = gcc-4.2 -mmacosx-version-min=10.6
LDDLFLAGS = -arch x86_64 -arch i386 -arch ppc -bundle -undefined dynamic_lookup -L/usr/local/lib
LDFLAGS = -arch x86_64 -arch i386 -arch ppc -L/usr/local/lib
LIBC = /usr/lib/libc.dylib
LIB_EXT = .a
OBJ_EXT = .o
OSNAME = darwin
OSVERS = 10.0
RANLIB = /usr/bin/ar s
SITELIBEXP = /Library/Perl/5.10.0
SITEARCHEXP = /Library/Perl/5.10.0/darwin-thread-multi-2level
SO = dylib
VENDORARCHEXP = /Network/Library/Perl/5.10.0/darwin-thread-multi-2level
VENDORLIBEXP = /Network/Library/Perl/5.10.0


# --- MakeMaker constants section:
AR_STATIC_ARGS = cr
DIRFILESEP = /
DFSEP = $(DIRFILESEP)
NAME = HGVbaseG2P
NAME_SYM = HGVbaseG2P
VERSION = 4.0
VERSION_MACRO = VERSION
VERSION_SYM = 4_0
DEFINE_VERSION = -D$(VERSION_MACRO)=\"$(VERSION)\"
XS_VERSION = 4.0
XS_VERSION_MACRO = XS_VERSION
XS_DEFINE_VERSION = -D$(XS_VERSION_MACRO)=\"$(XS_VERSION)\"
INST_ARCHLIB = blib/arch
INST_SCRIPT = blib/script
INST_BIN = blib/bin
INST_LIB = blib/lib
INST_MAN1DIR = blib/man1
INST_MAN3DIR = blib/man3
MAN1EXT = 1
MAN3EXT = 3pm
INSTALLDIRS = site
DESTDIR = 
PREFIX = $(SITEPREFIX)
PERLPREFIX = /
SITEPREFIX = /usr/local
VENDORPREFIX = /usr/local
INSTALLPRIVLIB = /Library/Perl/Updates/5.10.0
DESTINSTALLPRIVLIB = $(DESTDIR)$(INSTALLPRIVLIB)
INSTALLSITELIB = /Library/Perl/5.10.0
DESTINSTALLSITELIB = $(DESTDIR)$(INSTALLSITELIB)
INSTALLVENDORLIB = /Network/Library/Perl/5.10.0
DESTINSTALLVENDORLIB = $(DESTDIR)$(INSTALLVENDORLIB)
INSTALLARCHLIB = /Library/Perl/Updates/5.10.0/darwin-thread-multi-2level
DESTINSTALLARCHLIB = $(DESTDIR)$(INSTALLARCHLIB)
INSTALLSITEARCH = /Library/Perl/5.10.0/darwin-thread-multi-2level
DESTINSTALLSITEARCH = $(DESTDIR)$(INSTALLSITEARCH)
INSTALLVENDORARCH = /Network/Library/Perl/5.10.0/darwin-thread-multi-2level
DESTINSTALLVENDORARCH = $(DESTDIR)$(INSTALLVENDORARCH)
INSTALLBIN = /usr/bin
DESTINSTALLBIN = $(DESTDIR)$(INSTALLBIN)
INSTALLSITEBIN = /usr/local/bin
DESTINSTALLSITEBIN = $(DESTDIR)$(INSTALLSITEBIN)
INSTALLVENDORBIN = /usr/local/bin
DESTINSTALLVENDORBIN = $(DESTDIR)$(INSTALLVENDORBIN)
INSTALLSCRIPT = /usr/bin
DESTINSTALLSCRIPT = $(DESTDIR)$(INSTALLSCRIPT)
INSTALLSITESCRIPT = /usr/local/bin
DESTINSTALLSITESCRIPT = $(DESTDIR)$(INSTALLSITESCRIPT)
INSTALLVENDORSCRIPT = /usr/local/bin
DESTINSTALLVENDORSCRIPT = $(DESTDIR)$(INSTALLVENDORSCRIPT)
INSTALLMAN1DIR = /usr/share/man/man1
DESTINSTALLMAN1DIR = $(DESTDIR)$(INSTALLMAN1DIR)
INSTALLSITEMAN1DIR = /usr/local/share/man/man1
DESTINSTALLSITEMAN1DIR = $(DESTDIR)$(INSTALLSITEMAN1DIR)
INSTALLVENDORMAN1DIR = /usr/local/share/man/man1
DESTINSTALLVENDORMAN1DIR = $(DESTDIR)$(INSTALLVENDORMAN1DIR)
INSTALLMAN3DIR = /usr/share/man/man3
DESTINSTALLMAN3DIR = $(DESTDIR)$(INSTALLMAN3DIR)
INSTALLSITEMAN3DIR = /usr/local/share/man/man3
DESTINSTALLSITEMAN3DIR = $(DESTDIR)$(INSTALLSITEMAN3DIR)
INSTALLVENDORMAN3DIR = /usr/local/share/man/man3
DESTINSTALLVENDORMAN3DIR = $(DESTDIR)$(INSTALLVENDORMAN3DIR)
PERL_LIB =
PERL_ARCHLIB = /System/Library/Perl/5.10.0/darwin-thread-multi-2level
LIBPERL_A = libperl.a
FIRST_MAKEFILE = Makefile
MAKEFILE_OLD = Makefile.old
MAKE_APERL_FILE = Makefile.aperl
PERLMAINCC = $(CC)
PERL_INC = /System/Library/Perl/5.10.0/darwin-thread-multi-2level/CORE
PERL = /usr/bin/perl "-Iinc"
FULLPERL = /usr/bin/perl "-Iinc"
ABSPERL = $(PERL)
PERLRUN = $(PERL)
FULLPERLRUN = $(FULLPERL)
ABSPERLRUN = $(ABSPERL)
PERLRUNINST = $(PERLRUN) "-I$(INST_ARCHLIB)" "-Iinc" "-I$(INST_LIB)"
FULLPERLRUNINST = $(FULLPERLRUN) "-I$(INST_ARCHLIB)" "-Iinc" "-I$(INST_LIB)"
ABSPERLRUNINST = $(ABSPERLRUN) "-I$(INST_ARCHLIB)" "-Iinc" "-I$(INST_LIB)"
PERL_CORE = 0
PERM_DIR = 755
PERM_RW = 644
PERM_RWX = 755

MAKEMAKER   = /Library/Perl/Updates/5.10.0/ExtUtils/MakeMaker.pm
MM_VERSION  = 6.54
MM_REVISION = 65400

# FULLEXT = Pathname for extension directory (eg Foo/Bar/Oracle).
# BASEEXT = Basename part of FULLEXT. May be just equal FULLEXT. (eg Oracle)
# PARENT_NAME = NAME without BASEEXT and no trailing :: (eg Foo::Bar)
# DLBASE  = Basename part of dynamic library. May be just equal BASEEXT.
MAKE = make
FULLEXT = HGVbaseG2P
BASEEXT = HGVbaseG2P
PARENT_NAME = 
DLBASE = $(BASEEXT)
VERSION_FROM = 
OBJECT = 
LDFROM = $(OBJECT)
LINKTYPE = dynamic
BOOTDEP = 

# Handy lists of source code files:
XS_FILES = 
C_FILES  = 
O_FILES  = 
H_FILES  = 
MAN1PODS = script/assignMarkerAllelesets.pl \
	script/assignMarkerIDs.pl \
	script/backupDatabases.pl \
	script/broadT2Dliftover.pl \
	script/calculateMarkersImported.pl \
	script/clearOldData.pl \
	script/createHGVmart.pl \
	script/deletedMarkerFromdbSNP.pl \
	script/deploySystem.pl \
	script/generateBrowserDB.pl \
	script/generateBulkdata.pl \
	script/generateMarkerSummaries.pl \
	script/hgvbaseg2pweb_cgi.pl \
	script/hgvbaseg2pweb_create.pl \
	script/hgvbaseg2pweb_fastcgi.pl \
	script/hgvbaseg2pweb_server.pl \
	script/hgvbaseg2pweb_test.pl \
	script/importData.pl \
	script/importHapmap.pl \
	script/indexForTextsearch.pl \
	script/legacyMarkerFromdbSNP.pl \
	script/liftoverFromAny.pl \
	script/loadMarkerGFF.pl \
	script/markerGFFFromAny.pl \
	script/markerGFFFromDatabase.pl \
	script/markerMergeXMLFromdbSNP.pl \
	script/markerResultsets.pl \
	script/markerXMLFromDBGV.pl \
	script/markerXMLFromUniSTS.pl \
	script/markerXMLFromdbSNP.pl \
	script/optimiseBrowserDB.pl \
	script/optimiseMarkerDB.pl \
	script/optimiseStudyDB.pl \
	script/packData.pl \
	script/phenoXMLFromdbGaP.pl \
	script/precalculateBins.pl \
	script/refreshSchema.pl \
	script/scheduleJobs.pl \
	script/serverCheck.pl \
	script/storeXML.pl \
	script/updateLegacyMarkers.pl \
	script/updateMarkerMergehistory.pl \
	script/validateXML.pl
MAN3PODS = lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	lib/Bio/Graphics/Browser.pm \
	lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	lib/Bio/Graphics/Glyph/arc.pm \
	lib/Bio/Graphics/Glyph/ideogram.pm \
	lib/Bio/Graphics/Glyph/multiline_text.pm \
	lib/Bio/Graphics/Glyph/myreverseplot.pm \
	lib/Bio/Graphics/Glyph/square.pm \
	lib/Bio/Graphics/Glyph/stackplot.pm \
	lib/Bio/Graphics/Glyph/star.pm \
	lib/CGI/HGVToggle.pm \
	lib/Catalyst/Controller/OpenID.pm \
	lib/Catalyst/Controller/UserRegistration.pm \
	lib/Catalyst/Plugin/HGVbaseG2P.pm \
	lib/Catalyst/Plugin/Portal.pm \
	lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	lib/DBIx/DBSchema/DBD/mysql.pm \
	lib/DBIx/DBStag.pm \
	lib/HGVbaseG2P/Browser/Binning.pm \
	lib/HGVbaseG2P/Browser/Core.pm \
	lib/HGVbaseG2P/Browser/Creator.pm \
	lib/HGVbaseG2P/Browser/Display.pm \
	lib/HGVbaseG2P/Browser/Genome.pm \
	lib/HGVbaseG2P/Browser/Region.pm \
	lib/HGVbaseG2P/Browser/Upload.pm \
	lib/HGVbaseG2P/Browser/Util.pm \
	lib/HGVbaseG2P/ConfigLogger.pm \
	lib/HGVbaseG2P/DataAccess/GAS.pm \
	lib/HGVbaseG2P/DataImport.pm \
	lib/HGVbaseG2P/DataImport/Core.pm \
	lib/HGVbaseG2P/DataImport/DataElement.pm \
	lib/HGVbaseG2P/DataImport/Export.pm \
	lib/HGVbaseG2P/DataImport/Export/Database.pm \
	lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	lib/HGVbaseG2P/DataImport/PanelRule.pm \
	lib/HGVbaseG2P/DataImport/Plugin.pm \
	lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	lib/HGVbaseG2P/DataImport/Rule.pm \
	lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	lib/HGVbaseG2P/DataImport/Tool.pm \
	lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	lib/HGVbaseG2P/Database.pm \
	lib/HGVbaseG2P/Database/Browser.pm \
	lib/HGVbaseG2P/Database/BrowserUpload.pm \
	lib/HGVbaseG2P/Database/Feature.pm \
	lib/HGVbaseG2P/Database/Hapmap.pm \
	lib/HGVbaseG2P/Database/Hpo.pm \
	lib/HGVbaseG2P/Database/Marker.pm \
	lib/HGVbaseG2P/Database/Mart.pm \
	lib/HGVbaseG2P/Database/Ontology.pm \
	lib/HGVbaseG2P/Database/Study.pm \
	lib/HGVbaseG2P/Database/Xapian.pm \
	lib/HGVbaseG2P/Deploy.pm \
	lib/HGVbaseG2P/Exception.pm \
	lib/HGVbaseG2P/FileParser.pm \
	lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Author.pm \
	lib/HGVbaseG2P/Schema/Study/Build.pm \
	lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	lib/HGVbaseG2P/Schema/Study/Citation.pm \
	lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm \
	lib/HGVbaseG2P/Schema/Study/Significance.pm \
	lib/HGVbaseG2P/Schema/Study/Study.pm \
	lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	lib/HGVbaseG2P/Schema/Study/Submission.pm \
	lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	lib/HGVbaseG2P/Search.pm \
	lib/HGVbaseG2P/Search/Aggregator.pm \
	lib/HGVbaseG2P/Search/DataFeed.pm \
	lib/HGVbaseG2P/Search/Detector.pm \
	lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	lib/HGVbaseG2P/Search/Detector/Marker.pm \
	lib/HGVbaseG2P/Search/Detector/None.pm \
	lib/HGVbaseG2P/Search/Filter.pm \
	lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	lib/HGVbaseG2P/Search/Filter/Concept.pm \
	lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	lib/HGVbaseG2P/Search/Filter/ID.pm \
	lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	lib/HGVbaseG2P/Search/Filter/Page.pm \
	lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	lib/HGVbaseG2P/Search/Filter/Study.pm \
	lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	lib/HGVbaseG2P/Search/Filter/Variation.pm \
	lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	lib/HGVbaseG2P/Search/Performer.pm \
	lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	lib/HGVbaseG2P/Search/Query.pm \
	lib/HGVbaseG2P/Search/Query/All.pm \
	lib/HGVbaseG2P/Search/Query/Genes.pm \
	lib/HGVbaseG2P/Search/Query/Marker.pm \
	lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Query/Markers.pm \
	lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	lib/HGVbaseG2P/Search/Query/Studies.pm \
	lib/HGVbaseG2P/Search/Retriever.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	lib/HGVbaseG2P/Util.pm \
	lib/HGVbaseG2P/Web.pm \
	lib/HGVbaseG2P/Web/Controller/Admin.pm \
	lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	lib/HGVbaseG2P/Web/Controller/Browser.pm \
	lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	lib/HGVbaseG2P/Web/Controller/Gene.pm \
	lib/HGVbaseG2P/Web/Controller/Marker.pm \
	lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	lib/HGVbaseG2P/Web/Controller/Root.pm \
	lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	lib/HGVbaseG2P/Web/Controller/Search.pm \
	lib/HGVbaseG2P/Web/Controller/Study.pm \
	lib/HGVbaseG2P/Web/Controller/User.pm \
	lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	lib/HGVbaseG2P/Web/Model/UserDB.pm \
	lib/HGVbaseG2P/Web/Model/Xapian.pm \
	lib/HGVbaseG2P/Web/Output.pm \
	lib/HGVbaseG2P/Web/Update.pm \
	lib/HGVbaseG2P/Web/Util.pm \
	lib/HGVbaseG2P/Web/View/TT.pm \
	lib/HGVbaseG2P/XMLStore.pm \
	lib/Schedule/Depend.pm \
	lib/Schedule/Depend/Config.pm \
	lib/Schedule/Depend/Execute.pm \
	lib/Schedule/Depend/Utilities.pm \
	lib/WWW/Search/PubmedLiteMesh.pm \
	lib/XML/Atom/SimpleFeedOS.pm

# Where is the Config information that we are using/depend on
CONFIGDEP = $(PERL_ARCHLIB)$(DFSEP)Config.pm $(PERL_INC)$(DFSEP)config.h

# Where to build things
INST_LIBDIR      = $(INST_LIB)
INST_ARCHLIBDIR  = $(INST_ARCHLIB)

INST_AUTODIR     = $(INST_LIB)/auto/$(FULLEXT)
INST_ARCHAUTODIR = $(INST_ARCHLIB)/auto/$(FULLEXT)

INST_STATIC      = 
INST_DYNAMIC     = 
INST_BOOT        = 

# Extra linker info
EXPORT_LIST        = 
PERL_ARCHIVE       = 
PERL_ARCHIVE_AFTER = 


TO_INST_PM = lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	lib/Bio/Graphics/Browser.pm \
	lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	lib/Bio/Graphics/Glyph/arc.pm \
	lib/Bio/Graphics/Glyph/ideogram.pm \
	lib/Bio/Graphics/Glyph/multiline_text.pm \
	lib/Bio/Graphics/Glyph/myreverseplot.pm \
	lib/Bio/Graphics/Glyph/square.pm \
	lib/Bio/Graphics/Glyph/stackplot.pm \
	lib/Bio/Graphics/Glyph/star.pm \
	lib/CGI/HGVToggle.pm \
	lib/Catalyst/Controller/OpenID.pm \
	lib/Catalyst/Controller/UserRegistration.pm \
	lib/Catalyst/Plugin/HGVbaseG2P.pm \
	lib/Catalyst/Plugin/Portal.pm \
	lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	lib/DBIx/DBSchema/DBD/mysql.pm \
	lib/DBIx/DBStag.pm \
	lib/HGVbaseG2P.pm \
	lib/HGVbaseG2P/AccessControl.pm \
	lib/HGVbaseG2P/Browser/Binning.pm \
	lib/HGVbaseG2P/Browser/Core.html \
	lib/HGVbaseG2P/Browser/Core.pm \
	lib/HGVbaseG2P/Browser/Creator.pm \
	lib/HGVbaseG2P/Browser/Display.pm \
	lib/HGVbaseG2P/Browser/Genome.pm \
	lib/HGVbaseG2P/Browser/Region.pm \
	lib/HGVbaseG2P/Browser/Upload.pm \
	lib/HGVbaseG2P/Browser/Util.pm \
	lib/HGVbaseG2P/ConfigLogger.pm \
	lib/HGVbaseG2P/DataAccess/GAS.pm \
	lib/HGVbaseG2P/DataImport.pm \
	lib/HGVbaseG2P/DataImport/Core.html \
	lib/HGVbaseG2P/DataImport/Core.pm \
	lib/HGVbaseG2P/DataImport/DataElement.pm \
	lib/HGVbaseG2P/DataImport/Exception.pm \
	lib/HGVbaseG2P/DataImport/Export.pm \
	lib/HGVbaseG2P/DataImport/Export/Database.pm \
	lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	lib/HGVbaseG2P/DataImport/PanelRule.pm \
	lib/HGVbaseG2P/DataImport/Plugin.pm \
	lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	lib/HGVbaseG2P/DataImport/Rule.pm \
	lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	lib/HGVbaseG2P/DataImport/Tool.pm \
	lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	lib/HGVbaseG2P/Database.pm \
	lib/HGVbaseG2P/Database/Browser.pm \
	lib/HGVbaseG2P/Database/BrowserUpload.pm \
	lib/HGVbaseG2P/Database/Feature.pm \
	lib/HGVbaseG2P/Database/Hapmap.pm \
	lib/HGVbaseG2P/Database/Hpo.pm \
	lib/HGVbaseG2P/Database/Marker.pm \
	lib/HGVbaseG2P/Database/Mart.pm \
	lib/HGVbaseG2P/Database/Ontology.pm \
	lib/HGVbaseG2P/Database/Study.pm \
	lib/HGVbaseG2P/Database/Xapian.pm \
	lib/HGVbaseG2P/Deploy.pm \
	lib/HGVbaseG2P/Exception.pm \
	lib/HGVbaseG2P/FileParser.pm \
	lib/HGVbaseG2P/Schema/Browser.pm \
	lib/HGVbaseG2P/Schema/BrowserUpload.pm \
	lib/HGVbaseG2P/Schema/Hapmap.pm \
	lib/HGVbaseG2P/Schema/Hpo.pm \
	lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	lib/HGVbaseG2P/Schema/Marker.pm \
	lib/HGVbaseG2P/Schema/Marker/Allele.pm \
	lib/HGVbaseG2P/Schema/Marker/AssayMethodlist.pm \
	lib/HGVbaseG2P/Schema/Marker/AssayTypelist.pm \
	lib/HGVbaseG2P/Schema/Marker/Assayedgenotype.pm \
	lib/HGVbaseG2P/Schema/Marker/Assaymarker.pm \
	lib/HGVbaseG2P/Schema/Marker/Assaymarkergenotype.pm \
	lib/HGVbaseG2P/Schema/Marker/Build.pm \
	lib/HGVbaseG2P/Schema/Marker/Crossref.pm \
	lib/HGVbaseG2P/Schema/Marker/Datasource.pm \
	lib/HGVbaseG2P/Schema/Marker/Genotype.pm \
	lib/HGVbaseG2P/Schema/Marker/Genotypedef.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotype.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotypeallele.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotypecrossref.pm \
	lib/HGVbaseG2P/Schema/Marker/Hotlink.pm \
	lib/HGVbaseG2P/Schema/Marker/Marker.pm \
	lib/HGVbaseG2P/Schema/Marker/Markeralias.pm \
	lib/HGVbaseG2P/Schema/Marker/Markercoord.pm \
	lib/HGVbaseG2P/Schema/Marker/Markercrossref.pm \
	lib/HGVbaseG2P/Schema/Marker/Markerrevision.pm \
	lib/HGVbaseG2P/Schema/Ontology.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	lib/HGVbaseG2P/Schema/Session.pm \
	lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	lib/HGVbaseG2P/Schema/Study.pm \
	lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Author.pm \
	lib/HGVbaseG2P/Schema/Study/Build.pm \
	lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	lib/HGVbaseG2P/Schema/Study/Citation.pm \
	lib/HGVbaseG2P/Schema/Study/Citationcrossref.pm \
	lib/HGVbaseG2P/Schema/Study/CitationcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	lib/HGVbaseG2P/Schema/Study/Crossref.pm \
	lib/HGVbaseG2P/Schema/Study/CrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcrossref.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypemethodcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	lib/HGVbaseG2P/Schema/Study/Properties.pm \
	lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsetcrossref.pm \
	lib/HGVbaseG2P/Schema/Study/ResultsetcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm \
	lib/HGVbaseG2P/Schema/Study/Significance.pm \
	lib/HGVbaseG2P/Schema/Study/Study.pm \
	lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Studycrossref.pm \
	lib/HGVbaseG2P/Schema/Study/StudycrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	lib/HGVbaseG2P/Schema/Study/Submission.pm \
	lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	lib/HGVbaseG2P/Schema/User.pm \
	lib/HGVbaseG2P/Schema/User/User.pm \
	lib/HGVbaseG2P/Search.pm \
	lib/HGVbaseG2P/Search/Aggregator.pm \
	lib/HGVbaseG2P/Search/DataFeed.pm \
	lib/HGVbaseG2P/Search/Detector.pm \
	lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	lib/HGVbaseG2P/Search/Detector/Marker.pm \
	lib/HGVbaseG2P/Search/Detector/None.pm \
	lib/HGVbaseG2P/Search/Filter.pm \
	lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	lib/HGVbaseG2P/Search/Filter/Concept.pm \
	lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	lib/HGVbaseG2P/Search/Filter/ID.pm \
	lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	lib/HGVbaseG2P/Search/Filter/Page.pm \
	lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	lib/HGVbaseG2P/Search/Filter/Study.pm \
	lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	lib/HGVbaseG2P/Search/Filter/Variation.pm \
	lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	lib/HGVbaseG2P/Search/Performer.pm \
	lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	lib/HGVbaseG2P/Search/Query.pm \
	lib/HGVbaseG2P/Search/Query.pm.old \
	lib/HGVbaseG2P/Search/Query/All.pm \
	lib/HGVbaseG2P/Search/Query/Genes.pm \
	lib/HGVbaseG2P/Search/Query/Marker.pm \
	lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Query/Markers.pm \
	lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	lib/HGVbaseG2P/Search/Query/Studies.pm \
	lib/HGVbaseG2P/Search/Retriever.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	lib/HGVbaseG2P/Util.pm \
	lib/HGVbaseG2P/Web.pm \
	lib/HGVbaseG2P/Web/Controller/Admin.pm \
	lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	lib/HGVbaseG2P/Web/Controller/Browser.pm \
	lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	lib/HGVbaseG2P/Web/Controller/Gene.pm \
	lib/HGVbaseG2P/Web/Controller/Marker.pm \
	lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	lib/HGVbaseG2P/Web/Controller/Root.pm \
	lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	lib/HGVbaseG2P/Web/Controller/Search.pm \
	lib/HGVbaseG2P/Web/Controller/Study.pm \
	lib/HGVbaseG2P/Web/Controller/User.pm \
	lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	lib/HGVbaseG2P/Web/Model/UserDB.pm \
	lib/HGVbaseG2P/Web/Model/Xapian.pm \
	lib/HGVbaseG2P/Web/Output.pm \
	lib/HGVbaseG2P/Web/Update.pm \
	lib/HGVbaseG2P/Web/Util.pm \
	lib/HGVbaseG2P/Web/View/TT.pm \
	lib/HGVbaseG2P/XMLStore.pm \
	lib/HGVbaseG2P/hgvbase.conf \
	lib/Schedule/Depend.pm \
	lib/Schedule/Depend/Config.pm \
	lib/Schedule/Depend/Execute.pm \
	lib/Schedule/Depend/Utilities.pm \
	lib/Template/Plugin/Wiki.pm \
	lib/Template/Plugin/XML/RSS/URL.pm \
	lib/WWW/Search/PubmedLiteMesh.pm \
	lib/XML/Atom/SimpleFeedOS.pm

PM_TO_BLIB = lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	blib/lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	lib/HGVbaseG2P/Database/Xapian.pm \
	blib/lib/HGVbaseG2P/Database/Xapian.pm \
	lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	blib/lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	lib/HGVbaseG2P/Exception.pm \
	blib/lib/HGVbaseG2P/Exception.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	lib/HGVbaseG2P/DataImport/DataElement.pm \
	blib/lib/HGVbaseG2P/DataImport/DataElement.pm \
	lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	lib/Bio/Graphics/Glyph/square.pm \
	blib/lib/Bio/Graphics/Glyph/square.pm \
	lib/HGVbaseG2P/Schema/Marker/Markeralias.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Markeralias.pm \
	lib/Schedule/Depend/Utilities.pm \
	blib/lib/Schedule/Depend/Utilities.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	blib/lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	lib/HGVbaseG2P/Database/Feature.pm \
	blib/lib/HGVbaseG2P/Database/Feature.pm \
	lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	blib/lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	lib/HGVbaseG2P/Search/Detector/None.pm \
	blib/lib/HGVbaseG2P/Search/Detector/None.pm \
	lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	blib/lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	lib/Catalyst/Plugin/Portal.pm \
	blib/lib/Catalyst/Plugin/Portal.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	blib/lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	blib/lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	lib/HGVbaseG2P/Search/Filter/Page.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Page.pm \
	lib/HGVbaseG2P/Database/Marker.pm \
	blib/lib/HGVbaseG2P/Database/Marker.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	lib/HGVbaseG2P/Search/Filter.pm \
	blib/lib/HGVbaseG2P/Search/Filter.pm \
	lib/Template/Plugin/Wiki.pm \
	blib/lib/Template/Plugin/Wiki.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	blib/lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	lib/HGVbaseG2P/Schema/Marker/Marker.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Marker.pm \
	lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	blib/lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	lib/HGVbaseG2P/Web/Controller/Study.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Study.pm \
	lib/Bio/Graphics/Glyph/arc.pm \
	blib/lib/Bio/Graphics/Glyph/arc.pm \
	lib/HGVbaseG2P/Schema/Study/Citationcrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Citationcrossref.pm \
	lib/HGVbaseG2P/Schema/Ontology.pm \
	blib/lib/HGVbaseG2P/Schema/Ontology.pm \
	lib/HGVbaseG2P/AccessControl.pm \
	blib/lib/HGVbaseG2P/AccessControl.pm \
	lib/HGVbaseG2P/hgvbase.conf \
	blib/lib/HGVbaseG2P/hgvbase.conf \
	lib/XML/Atom/SimpleFeedOS.pm \
	blib/lib/XML/Atom/SimpleFeedOS.pm \
	lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	lib/HGVbaseG2P/Schema/Study/Studycrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studycrossref.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotypecrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Haplotypecrossref.pm \
	lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	lib/WWW/Search/PubmedLiteMesh.pm \
	blib/lib/WWW/Search/PubmedLiteMesh.pm \
	lib/HGVbaseG2P/Search/Performer.pm \
	blib/lib/HGVbaseG2P/Search/Performer.pm \
	lib/HGVbaseG2P/Schema/Study/Crossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Crossref.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelcrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/SamplepanelcrossrefOld.pm \
	lib/HGVbaseG2P/Database/Browser.pm \
	blib/lib/HGVbaseG2P/Database/Browser.pm \
	lib/HGVbaseG2P/Database/Mart.pm \
	blib/lib/HGVbaseG2P/Database/Mart.pm \
	lib/HGVbaseG2P/Deploy.pm \
	blib/lib/HGVbaseG2P/Deploy.pm \
	lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	blib/lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	lib/HGVbaseG2P/DataImport/PanelRule.pm \
	blib/lib/HGVbaseG2P/DataImport/PanelRule.pm \
	lib/HGVbaseG2P/Web/Controller/Root.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Root.pm \
	lib/HGVbaseG2P/Browser/Core.html \
	blib/lib/HGVbaseG2P/Browser/Core.html \
	lib/HGVbaseG2P/Web/Controller/Browser.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Browser.pm \
	lib/HGVbaseG2P/Schema/Marker/Build.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Build.pm \
	lib/HGVbaseG2P/DataImport/Plugin.pm \
	blib/lib/HGVbaseG2P/DataImport/Plugin.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	lib/HGVbaseG2P/Web/Update.pm \
	blib/lib/HGVbaseG2P/Web/Update.pm \
	lib/HGVbaseG2P/Web.pm \
	blib/lib/HGVbaseG2P/Web.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	lib/HGVbaseG2P/Schema/Marker/Assaymarker.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Assaymarker.pm \
	lib/HGVbaseG2P/Browser/Region.pm \
	blib/lib/HGVbaseG2P/Browser/Region.pm \
	lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	blib/lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	lib/HGVbaseG2P/Schema/Marker/Genotypedef.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Genotypedef.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypemethodcrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/PhenotypemethodcrossrefOld.pm \
	lib/HGVbaseG2P/DataAccess/GAS.pm \
	blib/lib/HGVbaseG2P/DataAccess/GAS.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	lib/HGVbaseG2P/Browser/Creator.pm \
	blib/lib/HGVbaseG2P/Browser/Creator.pm \
	lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	blib/lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	lib/HGVbaseG2P/FileParser.pm \
	blib/lib/HGVbaseG2P/FileParser.pm \
	lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	lib/HGVbaseG2P/Schema/Marker/Hotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Hotlink.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	blib/lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	lib/HGVbaseG2P/Browser/Genome.pm \
	blib/lib/HGVbaseG2P/Browser/Genome.pm \
	lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	lib/Template/Plugin/XML/RSS/URL.pm \
	blib/lib/Template/Plugin/XML/RSS/URL.pm \
	lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	blib/lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	lib/HGVbaseG2P/DataImport/Core.html \
	blib/lib/HGVbaseG2P/DataImport/Core.html \
	lib/HGVbaseG2P/Browser/Binning.pm \
	blib/lib/HGVbaseG2P/Browser/Binning.pm \
	lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	lib/DBIx/DBSchema/DBD/mysql.pm \
	blib/lib/DBIx/DBSchema/DBD/mysql.pm \
	lib/HGVbaseG2P/DataImport/Tool.pm \
	blib/lib/HGVbaseG2P/DataImport/Tool.pm \
	lib/HGVbaseG2P/Schema/Marker/Allele.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Allele.pm \
	lib/HGVbaseG2P/Web/Model/Xapian.pm \
	blib/lib/HGVbaseG2P/Web/Model/Xapian.pm \
	lib/HGVbaseG2P/Schema/Study/Submission.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Submission.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	lib/HGVbaseG2P/Schema/User/User.pm \
	blib/lib/HGVbaseG2P/Schema/User/User.pm \
	lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	blib/lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	lib/HGVbaseG2P/Schema/Study.pm \
	blib/lib/HGVbaseG2P/Schema/Study.pm \
	lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	blib/lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	blib/lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	blib/lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	lib/HGVbaseG2P/Database/Study.pm \
	blib/lib/HGVbaseG2P/Database/Study.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	lib/HGVbaseG2P/Web/View/TT.pm \
	blib/lib/HGVbaseG2P/Web/View/TT.pm \
	lib/Bio/Graphics/Browser.pm \
	blib/lib/Bio/Graphics/Browser.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	blib/lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	lib/HGVbaseG2P/Search/Query.pm.old \
	blib/lib/HGVbaseG2P/Search/Query.pm.old \
	lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	blib/lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	lib/HGVbaseG2P.pm \
	blib/lib/HGVbaseG2P.pm \
	lib/HGVbaseG2P/Search.pm \
	blib/lib/HGVbaseG2P/Search.pm \
	lib/HGVbaseG2P/Schema/Marker/AssayTypelist.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/AssayTypelist.pm \
	lib/HGVbaseG2P/Browser/Upload.pm \
	blib/lib/HGVbaseG2P/Browser/Upload.pm \
	lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm \
	lib/HGVbaseG2P/Search/Filter/ID.pm \
	blib/lib/HGVbaseG2P/Search/Filter/ID.pm \
	lib/HGVbaseG2P/Schema/Study/ResultsetcrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/ResultsetcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	lib/HGVbaseG2P/Database/Hapmap.pm \
	blib/lib/HGVbaseG2P/Database/Hapmap.pm \
	lib/HGVbaseG2P/Schema/BrowserUpload.pm \
	blib/lib/HGVbaseG2P/Schema/BrowserUpload.pm \
	lib/HGVbaseG2P/Schema/Marker/Crossref.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Crossref.pm \
	lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	lib/HGVbaseG2P/Database.pm \
	blib/lib/HGVbaseG2P/Database.pm \
	lib/HGVbaseG2P/Schema/Marker/Assaymarkergenotype.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Assaymarkergenotype.pm \
	lib/HGVbaseG2P/Schema/User.pm \
	blib/lib/HGVbaseG2P/Schema/User.pm \
	lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	blib/lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	lib/HGVbaseG2P/Search/Query/Genes.pm \
	blib/lib/HGVbaseG2P/Search/Query/Genes.pm \
	lib/HGVbaseG2P/Search/Query/Marker.pm \
	blib/lib/HGVbaseG2P/Search/Query/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	blib/lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	blib/lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	lib/Bio/Graphics/Glyph/ideogram.pm \
	blib/lib/Bio/Graphics/Glyph/ideogram.pm \
	lib/HGVbaseG2P/Schema/Marker/Assayedgenotype.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Assayedgenotype.pm \
	lib/HGVbaseG2P/Search/Filter/Study.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Study.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	lib/HGVbaseG2P/Web/Controller/Search.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Search.pm \
	lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	lib/HGVbaseG2P/Schema/Study/Build.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Build.pm \
	lib/HGVbaseG2P/Search/Query.pm \
	blib/lib/HGVbaseG2P/Search/Query.pm \
	lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	lib/HGVbaseG2P/Web/Model/UserDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/UserDB.pm \
	lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	blib/lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	lib/HGVbaseG2P/Search/Query/Studies.pm \
	blib/lib/HGVbaseG2P/Search/Query/Studies.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsetcrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Resultsetcrossref.pm \
	lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	blib/lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	lib/HGVbaseG2P/Web/Controller/Admin.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Admin.pm \
	lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	blib/lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	blib/lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	lib/HGVbaseG2P/Schema/Marker/Genotype.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Genotype.pm \
	lib/HGVbaseG2P/Schema/Study/StudycrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/StudycrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	lib/HGVbaseG2P/XMLStore.pm \
	blib/lib/HGVbaseG2P/XMLStore.pm \
	lib/HGVbaseG2P/DataImport/Rule.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule.pm \
	lib/CGI/HGVToggle.pm \
	blib/lib/CGI/HGVToggle.pm \
	lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	lib/HGVbaseG2P/Util.pm \
	blib/lib/HGVbaseG2P/Util.pm \
	lib/HGVbaseG2P/Browser/Util.pm \
	blib/lib/HGVbaseG2P/Browser/Util.pm \
	lib/HGVbaseG2P/Search/Aggregator.pm \
	blib/lib/HGVbaseG2P/Search/Aggregator.pm \
	lib/HGVbaseG2P/Database/Hpo.pm \
	blib/lib/HGVbaseG2P/Database/Hpo.pm \
	lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	lib/HGVbaseG2P/DataImport/Exception.pm \
	blib/lib/HGVbaseG2P/DataImport/Exception.pm \
	lib/HGVbaseG2P/Web/Util.pm \
	blib/lib/HGVbaseG2P/Web/Util.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	blib/lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	lib/Catalyst/Plugin/HGVbaseG2P.pm \
	blib/lib/Catalyst/Plugin/HGVbaseG2P.pm \
	lib/HGVbaseG2P/Schema/Marker/Markercoord.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Markercoord.pm \
	lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	blib/lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	blib/lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	lib/HGVbaseG2P/DataImport/Export/Database.pm \
	blib/lib/HGVbaseG2P/DataImport/Export/Database.pm \
	lib/DBIx/DBStag.pm \
	blib/lib/DBIx/DBStag.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	lib/HGVbaseG2P/Schema/Marker/Markercrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Markercrossref.pm \
	lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	lib/HGVbaseG2P/Schema/Study/CitationcrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/CitationcrossrefOld.pm \
	lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	blib/lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	lib/HGVbaseG2P/Database/BrowserUpload.pm \
	blib/lib/HGVbaseG2P/Database/BrowserUpload.pm \
	lib/Schedule/Depend/Execute.pm \
	blib/lib/Schedule/Depend/Execute.pm \
	lib/HGVbaseG2P/Search/Filter/Variation.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Variation.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	blib/lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	lib/Bio/Graphics/Glyph/myreverseplot.pm \
	blib/lib/Bio/Graphics/Glyph/myreverseplot.pm \
	lib/Bio/Graphics/Glyph/stackplot.pm \
	blib/lib/Bio/Graphics/Glyph/stackplot.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	blib/lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	lib/HGVbaseG2P/Web/Output.pm \
	blib/lib/HGVbaseG2P/Web/Output.pm \
	lib/Catalyst/Controller/UserRegistration.pm \
	blib/lib/Catalyst/Controller/UserRegistration.pm \
	lib/HGVbaseG2P/Schema/Hapmap.pm \
	blib/lib/HGVbaseG2P/Schema/Hapmap.pm \
	lib/HGVbaseG2P/Schema/Session.pm \
	blib/lib/HGVbaseG2P/Schema/Session.pm \
	lib/HGVbaseG2P/Web/Controller/Marker.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Marker.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	blib/lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	lib/HGVbaseG2P/Schema/Study/Study.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Study.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	lib/HGVbaseG2P/Browser/Core.pm \
	blib/lib/HGVbaseG2P/Browser/Core.pm \
	lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	lib/Bio/Graphics/Glyph/star.pm \
	blib/lib/Bio/Graphics/Glyph/star.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcrossref.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodcrossref.pm \
	lib/HGVbaseG2P/Search/Filter/Concept.pm \
	blib/lib/HGVbaseG2P/Search/Filter/Concept.pm \
	lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	lib/HGVbaseG2P/Schema/Hpo.pm \
	blib/lib/HGVbaseG2P/Schema/Hpo.pm \
	lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	blib/lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	lib/HGVbaseG2P/Schema/Study/Author.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Author.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	lib/HGVbaseG2P/Search/Query/All.pm \
	blib/lib/HGVbaseG2P/Search/Query/All.pm \
	lib/Schedule/Depend.pm \
	blib/lib/Schedule/Depend.pm \
	lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	lib/Schedule/Depend/Config.pm \
	blib/lib/Schedule/Depend/Config.pm \
	lib/HGVbaseG2P/Schema/Study/Significance.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Significance.pm \
	lib/HGVbaseG2P/Schema/Marker/AssayMethodlist.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/AssayMethodlist.pm \
	lib/HGVbaseG2P/Schema/Marker.pm \
	blib/lib/HGVbaseG2P/Schema/Marker.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	lib/HGVbaseG2P/Search/Detector.pm \
	blib/lib/HGVbaseG2P/Search/Detector.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	blib/lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotype.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Haplotype.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	blib/lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	lib/HGVbaseG2P/Web/Controller/Gene.pm \
	blib/lib/HGVbaseG2P/Web/Controller/Gene.pm \
	lib/HGVbaseG2P/Schema/Marker/Markerrevision.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Markerrevision.pm \
	lib/HGVbaseG2P/Schema/Browser.pm \
	blib/lib/HGVbaseG2P/Schema/Browser.pm \
	lib/HGVbaseG2P/DataImport/Core.pm \
	blib/lib/HGVbaseG2P/DataImport/Core.pm \
	lib/HGVbaseG2P/Search/Query/Markers.pm \
	blib/lib/HGVbaseG2P/Search/Query/Markers.pm \
	lib/HGVbaseG2P/Schema/Marker/Datasource.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Datasource.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	lib/HGVbaseG2P/DataImport.pm \
	blib/lib/HGVbaseG2P/DataImport.pm \
	lib/HGVbaseG2P/Schema/Study/CrossrefOld.pm \
	blib/lib/HGVbaseG2P/Schema/Study/CrossrefOld.pm \
	lib/HGVbaseG2P/ConfigLogger.pm \
	blib/lib/HGVbaseG2P/ConfigLogger.pm \
	lib/HGVbaseG2P/Search/Retriever.pm \
	blib/lib/HGVbaseG2P/Search/Retriever.pm \
	lib/HGVbaseG2P/Web/Controller/User.pm \
	blib/lib/HGVbaseG2P/Web/Controller/User.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	lib/HGVbaseG2P/Search/DataFeed.pm \
	blib/lib/HGVbaseG2P/Search/DataFeed.pm \
	lib/HGVbaseG2P/Schema/Study/Properties.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Properties.pm \
	lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	blib/lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	blib/lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	lib/HGVbaseG2P/DataImport/Export.pm \
	blib/lib/HGVbaseG2P/DataImport/Export.pm \
	lib/Catalyst/Controller/OpenID.pm \
	blib/lib/Catalyst/Controller/OpenID.pm \
	lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	blib/lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	lib/HGVbaseG2P/Database/Ontology.pm \
	blib/lib/HGVbaseG2P/Database/Ontology.pm \
	lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	blib/lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	lib/HGVbaseG2P/Schema/Study/Citation.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Citation.pm \
	lib/HGVbaseG2P/Browser/Display.pm \
	blib/lib/HGVbaseG2P/Browser/Display.pm \
	lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	blib/lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	blib/lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	lib/Bio/Graphics/Glyph/multiline_text.pm \
	blib/lib/Bio/Graphics/Glyph/multiline_text.pm \
	lib/HGVbaseG2P/Search/Detector/Marker.pm \
	blib/lib/HGVbaseG2P/Search/Detector/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	lib/HGVbaseG2P/Schema/Marker/Haplotypeallele.pm \
	blib/lib/HGVbaseG2P/Schema/Marker/Haplotypeallele.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm \
	blib/lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm


# --- MakeMaker platform_constants section:
MM_Unix_VERSION = 6.54
PERL_MALLOC_DEF = -DPERL_EXTMALLOC_DEF -Dmalloc=Perl_malloc -Dfree=Perl_mfree -Drealloc=Perl_realloc -Dcalloc=Perl_calloc


# --- MakeMaker tool_autosplit section:
# Usage: $(AUTOSPLITFILE) FileToSplit AutoDirToSplitInto
AUTOSPLITFILE = $(ABSPERLRUN)  -e 'use AutoSplit;  autosplit($$ARGV[0], $$ARGV[1], 0, 1, 1)' --



# --- MakeMaker tool_xsubpp section:


# --- MakeMaker tools_other section:
SHELL = /bin/sh
CHMOD = chmod
CP = cp
MV = mv
NOOP = $(TRUE)
NOECHO = @
RM_F = rm -f
RM_RF = rm -rf
TEST_F = test -f
TOUCH = touch
UMASK_NULL = umask 0
DEV_NULL = > /dev/null 2>&1
MKPATH = $(ABSPERLRUN) -MExtUtils::Command -e 'mkpath' --
EQUALIZE_TIMESTAMP = $(ABSPERLRUN) -MExtUtils::Command -e 'eqtime' --
FALSE = false
TRUE = true
ECHO = echo
ECHO_N = echo -n
UNINST = 0
VERBINST = 0
MOD_INSTALL = $(ABSPERLRUN) -MExtUtils::Install -e 'install([ from_to => {@ARGV}, verbose => '\''$(VERBINST)'\'', uninstall_shadows => '\''$(UNINST)'\'', dir_mode => '\''$(PERM_DIR)'\'' ]);' --
DOC_INSTALL = $(ABSPERLRUN) -MExtUtils::Command::MM -e 'perllocal_install' --
UNINSTALL = $(ABSPERLRUN) -MExtUtils::Command::MM -e 'uninstall' --
WARN_IF_OLD_PACKLIST = $(ABSPERLRUN) -MExtUtils::Command::MM -e 'warn_if_old_packlist' --
MACROSTART = 
MACROEND = 
USEMAKEFILE = -f
FIXIN = $(ABSPERLRUN) -MExtUtils::MY -e 'MY->fixin(shift)' --


# --- MakeMaker makemakerdflt section:
makemakerdflt : all
	$(NOECHO) $(NOOP)


# --- MakeMaker dist section:
TAR = COPY_EXTENDED_ATTRIBUTES_DISABLE=1 COPYFILE_DISABLE=1 tar
TARFLAGS = cvf
ZIP = zip
ZIPFLAGS = -r
COMPRESS = gzip --best
SUFFIX = .gz
SHAR = shar
PREOP = $(PERL) -I. "-MModule::Install::Admin" -e "dist_preop(q($(DISTVNAME)))"
POSTOP = $(NOECHO) $(NOOP)
TO_UNIX = $(NOECHO) $(NOOP)
CI = ci -u
RCS_LABEL = rcs -Nv$(VERSION_SYM): -q
DIST_CP = best
DIST_DEFAULT = tardist
DISTNAME = HGVbaseG2P
DISTVNAME = HGVbaseG2P-4.0


# --- MakeMaker macro section:


# --- MakeMaker depend section:


# --- MakeMaker cflags section:


# --- MakeMaker const_loadlibs section:


# --- MakeMaker const_cccmd section:


# --- MakeMaker post_constants section:


# --- MakeMaker pasthru section:

PASTHRU = LIBPERL_A="$(LIBPERL_A)"\
	LINKTYPE="$(LINKTYPE)"\
	PREFIX="$(PREFIX)"


# --- MakeMaker special_targets section:
.SUFFIXES : .xs .c .C .cpp .i .s .cxx .cc $(OBJ_EXT)

.PHONY: all config static dynamic test linkext manifest blibdirs clean realclean disttest distdir



# --- MakeMaker c_o section:


# --- MakeMaker xs_c section:


# --- MakeMaker xs_o section:


# --- MakeMaker top_targets section:
all :: pure_all manifypods
	$(NOECHO) $(NOOP)


pure_all :: config pm_to_blib subdirs linkext
	$(NOECHO) $(NOOP)

subdirs :: $(MYEXTLIB)
	$(NOECHO) $(NOOP)

config :: $(FIRST_MAKEFILE) blibdirs
	$(NOECHO) $(NOOP)

help :
	perldoc ExtUtils::MakeMaker


# --- MakeMaker blibdirs section:
blibdirs : $(INST_LIBDIR)$(DFSEP).exists $(INST_ARCHLIB)$(DFSEP).exists $(INST_AUTODIR)$(DFSEP).exists $(INST_ARCHAUTODIR)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists $(INST_SCRIPT)$(DFSEP).exists $(INST_MAN1DIR)$(DFSEP).exists $(INST_MAN3DIR)$(DFSEP).exists
	$(NOECHO) $(NOOP)

# Backwards compat with 6.18 through 6.25
blibdirs.ts : blibdirs
	$(NOECHO) $(NOOP)

$(INST_LIBDIR)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_LIBDIR)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_LIBDIR)
	$(NOECHO) $(TOUCH) $(INST_LIBDIR)$(DFSEP).exists

$(INST_ARCHLIB)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_ARCHLIB)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_ARCHLIB)
	$(NOECHO) $(TOUCH) $(INST_ARCHLIB)$(DFSEP).exists

$(INST_AUTODIR)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_AUTODIR)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_AUTODIR)
	$(NOECHO) $(TOUCH) $(INST_AUTODIR)$(DFSEP).exists

$(INST_ARCHAUTODIR)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_ARCHAUTODIR)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_ARCHAUTODIR)
	$(NOECHO) $(TOUCH) $(INST_ARCHAUTODIR)$(DFSEP).exists

$(INST_BIN)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_BIN)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_BIN)
	$(NOECHO) $(TOUCH) $(INST_BIN)$(DFSEP).exists

$(INST_SCRIPT)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_SCRIPT)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_SCRIPT)
	$(NOECHO) $(TOUCH) $(INST_SCRIPT)$(DFSEP).exists

$(INST_MAN1DIR)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_MAN1DIR)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_MAN1DIR)
	$(NOECHO) $(TOUCH) $(INST_MAN1DIR)$(DFSEP).exists

$(INST_MAN3DIR)$(DFSEP).exists :: Makefile.PL
	$(NOECHO) $(MKPATH) $(INST_MAN3DIR)
	$(NOECHO) $(CHMOD) $(PERM_DIR) $(INST_MAN3DIR)
	$(NOECHO) $(TOUCH) $(INST_MAN3DIR)$(DFSEP).exists



# --- MakeMaker linkext section:

linkext :: $(LINKTYPE)
	$(NOECHO) $(NOOP)


# --- MakeMaker dlsyms section:


# --- MakeMaker dynamic section:

dynamic :: $(FIRST_MAKEFILE) $(INST_DYNAMIC) $(INST_BOOT)
	$(NOECHO) $(NOOP)


# --- MakeMaker dynamic_bs section:

BOOTSTRAP =


# --- MakeMaker dynamic_lib section:


# --- MakeMaker static section:

## $(INST_PM) has been moved to the all: target.
## It remains here for awhile to allow for old usage: "make static"
static :: $(FIRST_MAKEFILE) $(INST_STATIC)
	$(NOECHO) $(NOOP)


# --- MakeMaker static_lib section:


# --- MakeMaker manifypods section:

POD2MAN_EXE = $(PERLRUN) "-MExtUtils::Command::MM" -e pod2man "--"
POD2MAN = $(POD2MAN_EXE)


manifypods : pure_all  \
	script/updateMarkerMergehistory.pl \
	script/hgvbaseg2pweb_create.pl \
	script/importHapmap.pl \
	script/markerXMLFromdbSNP.pl \
	script/generateMarkerSummaries.pl \
	script/refreshSchema.pl \
	script/assignMarkerAllelesets.pl \
	script/createHGVmart.pl \
	script/packData.pl \
	script/deploySystem.pl \
	script/markerResultsets.pl \
	script/deletedMarkerFromdbSNP.pl \
	script/hgvbaseg2pweb_fastcgi.pl \
	script/liftoverFromAny.pl \
	script/clearOldData.pl \
	script/calculateMarkersImported.pl \
	script/broadT2Dliftover.pl \
	script/assignMarkerIDs.pl \
	script/loadMarkerGFF.pl \
	script/storeXML.pl \
	script/hgvbaseg2pweb_server.pl \
	script/markerGFFFromDatabase.pl \
	script/importData.pl \
	script/optimiseMarkerDB.pl \
	script/markerXMLFromUniSTS.pl \
	script/validateXML.pl \
	script/backupDatabases.pl \
	script/updateLegacyMarkers.pl \
	script/optimiseStudyDB.pl \
	script/hgvbaseg2pweb_cgi.pl \
	script/markerGFFFromAny.pl \
	script/serverCheck.pl \
	script/generateBulkdata.pl \
	script/precalculateBins.pl \
	script/generateBrowserDB.pl \
	script/markerXMLFromDBGV.pl \
	script/markerMergeXMLFromdbSNP.pl \
	script/legacyMarkerFromdbSNP.pl \
	script/optimiseBrowserDB.pl \
	script/scheduleJobs.pl \
	script/indexForTextsearch.pl \
	script/hgvbaseg2pweb_test.pl \
	script/phenoXMLFromdbGaP.pl \
	lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	lib/HGVbaseG2P/Database/Xapian.pm \
	lib/HGVbaseG2P/Search/Filter/ID.pm \
	lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	lib/HGVbaseG2P/Exception.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	lib/HGVbaseG2P/DataImport/DataElement.pm \
	lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	lib/HGVbaseG2P/Database/Hapmap.pm \
	lib/Bio/Graphics/Glyph/square.pm \
	lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	lib/Schedule/Depend/Utilities.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	lib/HGVbaseG2P/Database/Feature.pm \
	lib/HGVbaseG2P/Database.pm \
	lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	lib/HGVbaseG2P/Search/Detector/None.pm \
	lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	lib/HGVbaseG2P/Search/Query/Genes.pm \
	lib/HGVbaseG2P/Search/Query/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	lib/Catalyst/Plugin/Portal.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	lib/Bio/Graphics/Glyph/ideogram.pm \
	lib/HGVbaseG2P/Search/Filter/Page.pm \
	lib/HGVbaseG2P/Database/Marker.pm \
	lib/HGVbaseG2P/Search/Filter/Study.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	lib/HGVbaseG2P/Search/Filter.pm \
	lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	lib/HGVbaseG2P/Web/Controller/Search.pm \
	lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	lib/HGVbaseG2P/Schema/Study/Build.pm \
	lib/HGVbaseG2P/Search/Query.pm \
	lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	lib/HGVbaseG2P/Web/Model/UserDB.pm \
	lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	lib/HGVbaseG2P/Search/Query/Studies.pm \
	lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	lib/HGVbaseG2P/Web/Controller/Admin.pm \
	lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	lib/HGVbaseG2P/XMLStore.pm \
	lib/HGVbaseG2P/DataImport/Rule.pm \
	lib/CGI/HGVToggle.pm \
	lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	lib/HGVbaseG2P/Web/Controller/Study.pm \
	lib/HGVbaseG2P/Util.pm \
	lib/Bio/Graphics/Glyph/arc.pm \
	lib/HGVbaseG2P/Browser/Util.pm \
	lib/HGVbaseG2P/Search/Aggregator.pm \
	lib/HGVbaseG2P/Database/Hpo.pm \
	lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	lib/HGVbaseG2P/Web/Util.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	lib/XML/Atom/SimpleFeedOS.pm \
	lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	lib/Catalyst/Plugin/HGVbaseG2P.pm \
	lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	lib/HGVbaseG2P/DataImport/Export/Database.pm \
	lib/DBIx/DBStag.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	lib/HGVbaseG2P/Search/Performer.pm \
	lib/WWW/Search/PubmedLiteMesh.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	lib/HGVbaseG2P/Database/Browser.pm \
	lib/HGVbaseG2P/Database/BrowserUpload.pm \
	lib/HGVbaseG2P/Database/Mart.pm \
	lib/Schedule/Depend/Execute.pm \
	lib/HGVbaseG2P/Search/Filter/Variation.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	lib/HGVbaseG2P/Deploy.pm \
	lib/Bio/Graphics/Glyph/myreverseplot.pm \
	lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	lib/Bio/Graphics/Glyph/stackplot.pm \
	lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	lib/HGVbaseG2P/Web/Output.pm \
	lib/Catalyst/Controller/UserRegistration.pm \
	lib/HGVbaseG2P/DataImport/PanelRule.pm \
	lib/HGVbaseG2P/Web/Controller/Marker.pm \
	lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	lib/HGVbaseG2P/Schema/Study/Study.pm \
	lib/HGVbaseG2P/Web/Controller/Root.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	lib/HGVbaseG2P/Web/Controller/Browser.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	lib/HGVbaseG2P/DataImport/Plugin.pm \
	lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	lib/HGVbaseG2P/Browser/Core.pm \
	lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	lib/HGVbaseG2P/Web/Update.pm \
	lib/HGVbaseG2P/Web.pm \
	lib/Bio/Graphics/Glyph/star.pm \
	lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	lib/HGVbaseG2P/Browser/Region.pm \
	lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	lib/HGVbaseG2P/Search/Filter/Concept.pm \
	lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	lib/HGVbaseG2P/DataAccess/GAS.pm \
	lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	lib/HGVbaseG2P/Browser/Creator.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	lib/HGVbaseG2P/Schema/Study/Author.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	lib/HGVbaseG2P/Search/Query/All.pm \
	lib/Schedule/Depend.pm \
	lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	lib/HGVbaseG2P/FileParser.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	lib/Schedule/Depend/Config.pm \
	lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	lib/HGVbaseG2P/Schema/Study/Significance.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	lib/HGVbaseG2P/Browser/Genome.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	lib/HGVbaseG2P/Search/Detector.pm \
	lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	lib/HGVbaseG2P/Web/Controller/Gene.pm \
	lib/HGVbaseG2P/Browser/Binning.pm \
	lib/HGVbaseG2P/DataImport/Core.pm \
	lib/HGVbaseG2P/Search/Query/Markers.pm \
	lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	lib/DBIx/DBSchema/DBD/mysql.pm \
	lib/HGVbaseG2P/DataImport/Tool.pm \
	lib/HGVbaseG2P/DataImport.pm \
	lib/HGVbaseG2P/Web/Model/Xapian.pm \
	lib/HGVbaseG2P/Schema/Study/Submission.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	lib/HGVbaseG2P/ConfigLogger.pm \
	lib/HGVbaseG2P/Search/Retriever.pm \
	lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	lib/HGVbaseG2P/Web/Controller/User.pm \
	lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	lib/HGVbaseG2P/Search/DataFeed.pm \
	lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	lib/HGVbaseG2P/Database/Study.pm \
	lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	lib/HGVbaseG2P/Web/View/TT.pm \
	lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	lib/Bio/Graphics/Browser.pm \
	lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	lib/HGVbaseG2P/Search.pm \
	lib/HGVbaseG2P/DataImport/Export.pm \
	lib/Catalyst/Controller/OpenID.pm \
	lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	lib/HGVbaseG2P/Database/Ontology.pm \
	lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	lib/HGVbaseG2P/Browser/Upload.pm \
	lib/HGVbaseG2P/Schema/Study/Citation.pm \
	lib/HGVbaseG2P/Browser/Display.pm \
	lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	lib/Bio/Graphics/Glyph/multiline_text.pm \
	lib/HGVbaseG2P/Search/Detector/Marker.pm \
	lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm \
	lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm
	$(NOECHO) $(POD2MAN) --section=1 --perm_rw=$(PERM_RW) \
	  script/updateMarkerMergehistory.pl $(INST_MAN1DIR)/updateMarkerMergehistory.pl.$(MAN1EXT) \
	  script/hgvbaseg2pweb_create.pl $(INST_MAN1DIR)/hgvbaseg2pweb_create.pl.$(MAN1EXT) \
	  script/importHapmap.pl $(INST_MAN1DIR)/importHapmap.pl.$(MAN1EXT) \
	  script/markerXMLFromdbSNP.pl $(INST_MAN1DIR)/markerXMLFromdbSNP.pl.$(MAN1EXT) \
	  script/generateMarkerSummaries.pl $(INST_MAN1DIR)/generateMarkerSummaries.pl.$(MAN1EXT) \
	  script/refreshSchema.pl $(INST_MAN1DIR)/refreshSchema.pl.$(MAN1EXT) \
	  script/assignMarkerAllelesets.pl $(INST_MAN1DIR)/assignMarkerAllelesets.pl.$(MAN1EXT) \
	  script/createHGVmart.pl $(INST_MAN1DIR)/createHGVmart.pl.$(MAN1EXT) \
	  script/packData.pl $(INST_MAN1DIR)/packData.pl.$(MAN1EXT) \
	  script/deploySystem.pl $(INST_MAN1DIR)/deploySystem.pl.$(MAN1EXT) \
	  script/markerResultsets.pl $(INST_MAN1DIR)/markerResultsets.pl.$(MAN1EXT) \
	  script/deletedMarkerFromdbSNP.pl $(INST_MAN1DIR)/deletedMarkerFromdbSNP.pl.$(MAN1EXT) \
	  script/hgvbaseg2pweb_fastcgi.pl $(INST_MAN1DIR)/hgvbaseg2pweb_fastcgi.pl.$(MAN1EXT) \
	  script/liftoverFromAny.pl $(INST_MAN1DIR)/liftoverFromAny.pl.$(MAN1EXT) \
	  script/clearOldData.pl $(INST_MAN1DIR)/clearOldData.pl.$(MAN1EXT) \
	  script/calculateMarkersImported.pl $(INST_MAN1DIR)/calculateMarkersImported.pl.$(MAN1EXT) \
	  script/broadT2Dliftover.pl $(INST_MAN1DIR)/broadT2Dliftover.pl.$(MAN1EXT) \
	  script/assignMarkerIDs.pl $(INST_MAN1DIR)/assignMarkerIDs.pl.$(MAN1EXT) \
	  script/loadMarkerGFF.pl $(INST_MAN1DIR)/loadMarkerGFF.pl.$(MAN1EXT) \
	  script/storeXML.pl $(INST_MAN1DIR)/storeXML.pl.$(MAN1EXT) \
	  script/hgvbaseg2pweb_server.pl $(INST_MAN1DIR)/hgvbaseg2pweb_server.pl.$(MAN1EXT) \
	  script/markerGFFFromDatabase.pl $(INST_MAN1DIR)/markerGFFFromDatabase.pl.$(MAN1EXT) \
	  script/importData.pl $(INST_MAN1DIR)/importData.pl.$(MAN1EXT) \
	  script/optimiseMarkerDB.pl $(INST_MAN1DIR)/optimiseMarkerDB.pl.$(MAN1EXT) \
	  script/markerXMLFromUniSTS.pl $(INST_MAN1DIR)/markerXMLFromUniSTS.pl.$(MAN1EXT) \
	  script/validateXML.pl $(INST_MAN1DIR)/validateXML.pl.$(MAN1EXT) \
	  script/backupDatabases.pl $(INST_MAN1DIR)/backupDatabases.pl.$(MAN1EXT) \
	  script/updateLegacyMarkers.pl $(INST_MAN1DIR)/updateLegacyMarkers.pl.$(MAN1EXT) \
	  script/optimiseStudyDB.pl $(INST_MAN1DIR)/optimiseStudyDB.pl.$(MAN1EXT) \
	  script/hgvbaseg2pweb_cgi.pl $(INST_MAN1DIR)/hgvbaseg2pweb_cgi.pl.$(MAN1EXT) \
	  script/markerGFFFromAny.pl $(INST_MAN1DIR)/markerGFFFromAny.pl.$(MAN1EXT) \
	  script/serverCheck.pl $(INST_MAN1DIR)/serverCheck.pl.$(MAN1EXT) \
	  script/generateBulkdata.pl $(INST_MAN1DIR)/generateBulkdata.pl.$(MAN1EXT) \
	  script/precalculateBins.pl $(INST_MAN1DIR)/precalculateBins.pl.$(MAN1EXT) \
	  script/generateBrowserDB.pl $(INST_MAN1DIR)/generateBrowserDB.pl.$(MAN1EXT) \
	  script/markerXMLFromDBGV.pl $(INST_MAN1DIR)/markerXMLFromDBGV.pl.$(MAN1EXT) \
	  script/markerMergeXMLFromdbSNP.pl $(INST_MAN1DIR)/markerMergeXMLFromdbSNP.pl.$(MAN1EXT) \
	  script/legacyMarkerFromdbSNP.pl $(INST_MAN1DIR)/legacyMarkerFromdbSNP.pl.$(MAN1EXT) \
	  script/optimiseBrowserDB.pl $(INST_MAN1DIR)/optimiseBrowserDB.pl.$(MAN1EXT) \
	  script/scheduleJobs.pl $(INST_MAN1DIR)/scheduleJobs.pl.$(MAN1EXT) \
	  script/indexForTextsearch.pl $(INST_MAN1DIR)/indexForTextsearch.pl.$(MAN1EXT) \
	  script/hgvbaseg2pweb_test.pl $(INST_MAN1DIR)/hgvbaseg2pweb_test.pl.$(MAN1EXT) \
	  script/phenoXMLFromdbGaP.pl $(INST_MAN1DIR)/phenoXMLFromdbGaP.pl.$(MAN1EXT) 
	$(NOECHO) $(POD2MAN) --section=3 --perm_rw=$(PERM_RW) \
	  lib/HGVbaseG2P/DataImport/RetrieveMarker.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::RetrieveMarker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Xapian.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Xapian.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/ID.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::ID.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Performer/AutoDetect.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Performer::AutoDetect.$(MAN3EXT) \
	  lib/HGVbaseG2P/Exception.pm $(INST_MAN3DIR)/HGVbaseG2P::Exception.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Ontology::Mesh_term.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/DataElement.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::DataElement.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::FreqFieldCheck.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Threshold.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Threshold.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Usedmarkerset.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Hapmap.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Hapmap.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/square.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::square.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Allelefrequency.$(MAN3EXT) \
	  lib/Schedule/Depend/Utilities.pm $(INST_MAN3DIR)/Schedule::Depend::Utilities.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Hpo/Term_path.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Hpo::Term_path.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Feature.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Feature.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database.pm $(INST_MAN3DIR)/HGVbaseG2P::Database.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/SortBy.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::SortBy.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/None.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::None.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Plugin::AffymetrixLookup.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Genes.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Genes.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Genes::Keyword.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/PageSize.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::PageSize.$(MAN3EXT) \
	  lib/Catalyst/Plugin/Portal.pm $(INST_MAN3DIR)/Catalyst::Plugin::Portal.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Genes/None.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Genes::None.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/allele_column_multi.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::allele_column_multi.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Samplepaneloverlap.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Phenotypes.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Phenotypes.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/MarkerResults.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::MarkerResults.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/RegionMarkers.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::RegionMarkers.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/ideogram.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::ideogram.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Page.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Page.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Study.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Study.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::CalcAlleleFreqsFromNumbers.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Hotlinkcollection.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/SessionDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::SessionDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Contribution.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Contribution.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Search.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Search.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::PopulateGenotypesFromAlleles.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Build.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Build.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Phenotype.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Phenotype.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/UserDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::UserDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::CompositionTypelist.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Genotypefrequency.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Studyoverlap.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Hotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Hotlink.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/QueryString.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::QueryString.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Studies.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Studies.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Bundleloci.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Bundleloci.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Phenotype.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Phenotype.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Admin.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Admin.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/Annotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::Annotation.$(MAN3EXT) \
	  lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm $(INST_MAN3DIR)/Bio::Das::ProServer::SourceAdaptor::hgvbaseg2p.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/GeneRegion.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::GeneRegion.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Genotypedloci.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypemethodcitation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Pvsc.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Pvsc.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/StudyOld.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::StudyOld.$(MAN3EXT) \
	  lib/HGVbaseG2P/XMLStore.pm $(INST_MAN3DIR)/HGVbaseG2P::XMLStore.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule.$(MAN3EXT) \
	  lib/CGI/HGVToggle.pm $(INST_MAN3DIR)/CGI::HGVToggle.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Experimenthotlink.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Study.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Study.$(MAN3EXT) \
	  lib/HGVbaseG2P/Util.pm $(INST_MAN3DIR)/HGVbaseG2P::Util.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/arc.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::arc.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Util.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Util.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Aggregator.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Aggregator.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Hpo.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Hpo.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Studyanalysismethod.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Marker/None.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Marker::None.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Util.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Util.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Ontology::Mesh_concept.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Pppa.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Pppa.$(MAN3EXT) \
	  lib/XML/Atom/SimpleFeedOS.pm $(INST_MAN3DIR)/XML::Atom::SimpleFeedOS.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/LogicMaker.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::LogicMaker.$(MAN3EXT) \
	  lib/Catalyst/Plugin/HGVbaseG2P.pm $(INST_MAN3DIR)/Catalyst::Plugin::HGVbaseG2P.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Tool::PubmedSearch.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Session/Sessions.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Session::Sessions.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::AllelesMatch.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::Feature.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Export/Database.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Export::Database.$(MAN3EXT) \
	  lib/DBIx/DBStag.pm $(INST_MAN3DIR)/DBIx::DBStag.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Assayedpanelcollection.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Experiment.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Experiment.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/OntologyDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::OntologyDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Performer.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Performer.$(MAN3EXT) \
	  lib/WWW/Search/PubmedLiteMesh.pm $(INST_MAN3DIR)/WWW::Search::PubmedLiteMesh.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::Annotation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Frequencycluster.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::PhenotypeID.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::Concept.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Browser.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Browser.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/BrowserUpload.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::BrowserUpload.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Mart.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Mart.$(MAN3EXT) \
	  lib/Schedule/Depend/Execute.pm $(INST_MAN3DIR)/Schedule::Depend::Execute.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Variation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Variation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Ontology::Mesh_termtree.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::MarkerFrequencies.$(MAN3EXT) \
	  lib/HGVbaseG2P/Deploy.pm $(INST_MAN3DIR)/HGVbaseG2P::Deploy.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/myreverseplot.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::myreverseplot.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Fcs.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Fcs.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/GeneRegion.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::GeneRegion.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/stackplot.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::stackplot.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Hpo/Term.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Hpo::Term.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Output.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Output.$(MAN3EXT) \
	  lib/Catalyst/Controller/UserRegistration.pm $(INST_MAN3DIR)/Catalyst::Controller::UserRegistration.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/PanelRule.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::PanelRule.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::SamplepanelSourceofdnalist.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Study.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Study.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Root.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Root.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypeproperty.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Studies::StudiesNone.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::StrandFlip::Custom.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Dialog.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Dialog.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Browser.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Browser.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Marker::Keyword.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Plugin.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Plugin.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Chromosome.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Chromosome.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Core.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Core.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/BrowserDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::BrowserDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Samplepanelcrossref.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Assayedpanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Update.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Update.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web.pm $(INST_MAN3DIR)/HGVbaseG2P::Web.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/star.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::star.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Genotypedbundle.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Region.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Region.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::RestrictToAnnotation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Ontology::Mesh_heading.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Studycitation.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Studycitation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Hotlinker.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Hotlinker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/Concept.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::Concept.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Resultsethotlink.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/StudyDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::StudyDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataAccess/GAS.pm $(INST_MAN3DIR)/HGVbaseG2P::DataAccess::GAS.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::MarkerResults.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Creator.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Creator.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/SeqFeature.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::SeqFeature.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Researcher.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Researcher.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Author.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Author.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::CalcGenotypeFreqsFromNumbers.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/SplitField.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::SplitField.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypemethodhotlink.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/MarkerCount.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::MarkerCount.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/All.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::All.$(MAN3EXT) \
	  lib/Schedule/Depend.pm $(INST_MAN3DIR)/Schedule::Depend.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::PositionsMatch.$(MAN3EXT) \
	  lib/HGVbaseG2P/FileParser.pm $(INST_MAN3DIR)/HGVbaseG2P::FileParser.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::StrandFlip::Field.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Studyhotlink.$(MAN3EXT) \
	  lib/Schedule/Depend/Config.pm $(INST_MAN3DIR)/Schedule::Depend::Config.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::RegexFields.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Significance.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Significance.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypeannotation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/Keyword.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::Keyword.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Genome.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Genome.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypevalue.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Studysamplepanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Genes.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Genes.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Hpo::Hpo2mesh.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::StrandFlip.$(MAN3EXT) \
	  lib/Catalyst/Plugin/UploadProgressSizeLimit.pm $(INST_MAN3DIR)/Catalyst::Plugin::UploadProgressSizeLimit.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::None.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypevaluedetail.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::CalcAlleleFreqsFromGenotypeNumbers.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Studies::StudiesKeyword.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Gene.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Gene.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Binning.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Binning.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Core.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Core.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/Markers.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::Markers.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Analysismethod.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Analysismethod.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Samplepanelhotlink.$(MAN3EXT) \
	  lib/DBIx/DBSchema/DBD/mysql.pm $(INST_MAN3DIR)/DBIx::DBSchema::DBD::mysql.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Tool.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Tool.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/Xapian.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::Xapian.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Submission.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Submission.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypepropertycitation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Studies.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Studies.$(MAN3EXT) \
	  lib/HGVbaseG2P/ConfigLogger.pm $(INST_MAN3DIR)/HGVbaseG2P::ConfigLogger.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/ResultSet.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::ResultSet.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/User.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::User.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Assayedpanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Phenotypemethod.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Hpo/Synonym.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Hpo::Synonym.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/ViewType.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::ViewType.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/DataFeed.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::DataFeed.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::PhenotypeMethod.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Study.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Study.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::PhenotypepropertyOld.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Diseasecategorybkup.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/View/TT.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::View::TT.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/HpoDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::HpoDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Rule::StrandFlip::File.$(MAN3EXT) \
	  lib/Bio/Graphics/Browser.pm $(INST_MAN3DIR)/Bio::Graphics::Browser.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Model/MarkerDB.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Model::MarkerDB.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::ExperimentTypelist.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::ConceptTerm.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Query::ResultsetMarkers.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Resultset.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Resultset.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Phenotype::Keyword.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Samplepanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Samplepanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search.pm $(INST_MAN3DIR)/HGVbaseG2P::Search.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Export.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Export.$(MAN3EXT) \
	  lib/Catalyst/Controller/OpenID.pm $(INST_MAN3DIR)/Catalyst::Controller::OpenID.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Filter/ExportFormat.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Filter::ExportFormat.$(MAN3EXT) \
	  lib/HGVbaseG2P/Database/Ontology.pm $(INST_MAN3DIR)/HGVbaseG2P::Database::Ontology.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Performer/NoDetect.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Performer::NoDetect.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Upload.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Upload.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Citation.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Citation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Browser/Display.pm $(INST_MAN3DIR)/HGVbaseG2P::Browser::Display.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Experimentassayedpanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/DataImport/Plugin/GSK.pm $(INST_MAN3DIR)/HGVbaseG2P::DataImport::Plugin::GSK.$(MAN3EXT) \
	  lib/Bio/Graphics/Glyph/multiline_text.pm $(INST_MAN3DIR)/Bio::Graphics::Glyph::multiline_text.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Detector/Marker.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Detector::Marker.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Marker::Annotation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Web/Controller/Samplepanel.pm $(INST_MAN3DIR)/HGVbaseG2P::Web::Controller::Samplepanel.$(MAN3EXT) \
	  lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm $(INST_MAN3DIR)/HGVbaseG2P::Search::Retriever::Genes::Annotation.$(MAN3EXT) \
	  lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm $(INST_MAN3DIR)/HGVbaseG2P::Schema::Study::Selectioncriteria.$(MAN3EXT) 




# --- MakeMaker processPL section:


# --- MakeMaker installbin section:

EXE_FILES = script/assignMarkerAllelesets.pl script/assignMarkerIDs.pl script/backupDatabases.pl script/broadT2Dliftover.pl script/calculateMarkersImported.pl script/clearOldData.pl script/createHGVmart.pl script/deletedMarkerFromdbSNP.pl script/deploySystem.pl script/generateBrowserDB.pl script/generateBulkdata.pl script/generateImages.pl script/generateMarkerSummaries.pl script/hgvbaseg2pweb_cgi.pl script/hgvbaseg2pweb_create.pl script/hgvbaseg2pweb_fastcgi.pl script/hgvbaseg2pweb_server.pl script/hgvbaseg2pweb_test.pl script/importData.pl script/importHapmap.pl script/indexForTextsearch.pl script/legacyMarkerFromdbSNP.pl script/liftoverFromAny.pl script/loadMarkerGFF.pl script/markerGFFFromAny.pl script/markerGFFFromDatabase.pl script/markerMergeXMLFromdbSNP.pl script/markerResultsets.pl script/markerXMLFromDBGV.pl script/markerXMLFromdbSNP.pl script/markerXMLFromUniSTS.pl script/optimiseBrowserDB.pl script/optimiseMarkerDB.pl script/optimiseStudyDB.pl script/packData.pl script/phenoXMLFromdbGaP.pl script/precalculateBins.pl script/refreshSchema.pl script/scheduleJobs.pl script/serverCheck.pl script/storeXML.pl script/updateLegacyMarkers.pl script/updateMarkerMergehistory.pl script/validateXML.pl script/viewWebPerformance.pl

pure_all :: $(INST_SCRIPT)/updateMarkerMergehistory.pl $(INST_SCRIPT)/hgvbaseg2pweb_create.pl $(INST_SCRIPT)/importHapmap.pl $(INST_SCRIPT)/markerXMLFromdbSNP.pl $(INST_SCRIPT)/generateMarkerSummaries.pl $(INST_SCRIPT)/refreshSchema.pl $(INST_SCRIPT)/assignMarkerAllelesets.pl $(INST_SCRIPT)/createHGVmart.pl $(INST_SCRIPT)/packData.pl $(INST_SCRIPT)/deploySystem.pl $(INST_SCRIPT)/markerResultsets.pl $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl $(INST_SCRIPT)/liftoverFromAny.pl $(INST_SCRIPT)/clearOldData.pl $(INST_SCRIPT)/calculateMarkersImported.pl $(INST_SCRIPT)/broadT2Dliftover.pl $(INST_SCRIPT)/assignMarkerIDs.pl $(INST_SCRIPT)/loadMarkerGFF.pl $(INST_SCRIPT)/storeXML.pl $(INST_SCRIPT)/generateImages.pl $(INST_SCRIPT)/hgvbaseg2pweb_server.pl $(INST_SCRIPT)/markerGFFFromDatabase.pl $(INST_SCRIPT)/importData.pl $(INST_SCRIPT)/optimiseMarkerDB.pl $(INST_SCRIPT)/markerXMLFromUniSTS.pl $(INST_SCRIPT)/validateXML.pl $(INST_SCRIPT)/backupDatabases.pl $(INST_SCRIPT)/updateLegacyMarkers.pl $(INST_SCRIPT)/optimiseStudyDB.pl $(INST_SCRIPT)/viewWebPerformance.pl $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl $(INST_SCRIPT)/markerGFFFromAny.pl $(INST_SCRIPT)/serverCheck.pl $(INST_SCRIPT)/generateBulkdata.pl $(INST_SCRIPT)/precalculateBins.pl $(INST_SCRIPT)/generateBrowserDB.pl $(INST_SCRIPT)/markerXMLFromDBGV.pl $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl $(INST_SCRIPT)/optimiseBrowserDB.pl $(INST_SCRIPT)/scheduleJobs.pl $(INST_SCRIPT)/indexForTextsearch.pl $(INST_SCRIPT)/hgvbaseg2pweb_test.pl $(INST_SCRIPT)/phenoXMLFromdbGaP.pl
	$(NOECHO) $(NOOP)

realclean ::
	$(RM_F) \
	  $(INST_SCRIPT)/updateMarkerMergehistory.pl $(INST_SCRIPT)/hgvbaseg2pweb_create.pl \
	  $(INST_SCRIPT)/importHapmap.pl $(INST_SCRIPT)/markerXMLFromdbSNP.pl \
	  $(INST_SCRIPT)/generateMarkerSummaries.pl $(INST_SCRIPT)/refreshSchema.pl \
	  $(INST_SCRIPT)/assignMarkerAllelesets.pl $(INST_SCRIPT)/createHGVmart.pl \
	  $(INST_SCRIPT)/packData.pl $(INST_SCRIPT)/deploySystem.pl \
	  $(INST_SCRIPT)/markerResultsets.pl $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl \
	  $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl $(INST_SCRIPT)/liftoverFromAny.pl \
	  $(INST_SCRIPT)/clearOldData.pl $(INST_SCRIPT)/calculateMarkersImported.pl \
	  $(INST_SCRIPT)/broadT2Dliftover.pl $(INST_SCRIPT)/assignMarkerIDs.pl \
	  $(INST_SCRIPT)/loadMarkerGFF.pl $(INST_SCRIPT)/storeXML.pl \
	  $(INST_SCRIPT)/generateImages.pl $(INST_SCRIPT)/hgvbaseg2pweb_server.pl \
	  $(INST_SCRIPT)/markerGFFFromDatabase.pl $(INST_SCRIPT)/importData.pl \
	  $(INST_SCRIPT)/optimiseMarkerDB.pl $(INST_SCRIPT)/markerXMLFromUniSTS.pl \
	  $(INST_SCRIPT)/validateXML.pl $(INST_SCRIPT)/backupDatabases.pl \
	  $(INST_SCRIPT)/updateLegacyMarkers.pl $(INST_SCRIPT)/optimiseStudyDB.pl \
	  $(INST_SCRIPT)/viewWebPerformance.pl $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl \
	  $(INST_SCRIPT)/markerGFFFromAny.pl $(INST_SCRIPT)/serverCheck.pl \
	  $(INST_SCRIPT)/generateBulkdata.pl $(INST_SCRIPT)/precalculateBins.pl \
	  $(INST_SCRIPT)/generateBrowserDB.pl $(INST_SCRIPT)/markerXMLFromDBGV.pl \
	  $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl \
	  $(INST_SCRIPT)/optimiseBrowserDB.pl $(INST_SCRIPT)/scheduleJobs.pl \
	  $(INST_SCRIPT)/indexForTextsearch.pl $(INST_SCRIPT)/hgvbaseg2pweb_test.pl \
	  $(INST_SCRIPT)/phenoXMLFromdbGaP.pl 

$(INST_SCRIPT)/updateMarkerMergehistory.pl : script/updateMarkerMergehistory.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/updateMarkerMergehistory.pl
	$(CP) script/updateMarkerMergehistory.pl $(INST_SCRIPT)/updateMarkerMergehistory.pl
	$(FIXIN) $(INST_SCRIPT)/updateMarkerMergehistory.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/updateMarkerMergehistory.pl

$(INST_SCRIPT)/hgvbaseg2pweb_create.pl : script/hgvbaseg2pweb_create.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/hgvbaseg2pweb_create.pl
	$(CP) script/hgvbaseg2pweb_create.pl $(INST_SCRIPT)/hgvbaseg2pweb_create.pl
	$(FIXIN) $(INST_SCRIPT)/hgvbaseg2pweb_create.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/hgvbaseg2pweb_create.pl

$(INST_SCRIPT)/importHapmap.pl : script/importHapmap.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/importHapmap.pl
	$(CP) script/importHapmap.pl $(INST_SCRIPT)/importHapmap.pl
	$(FIXIN) $(INST_SCRIPT)/importHapmap.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/importHapmap.pl

$(INST_SCRIPT)/markerXMLFromdbSNP.pl : script/markerXMLFromdbSNP.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerXMLFromdbSNP.pl
	$(CP) script/markerXMLFromdbSNP.pl $(INST_SCRIPT)/markerXMLFromdbSNP.pl
	$(FIXIN) $(INST_SCRIPT)/markerXMLFromdbSNP.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerXMLFromdbSNP.pl

$(INST_SCRIPT)/generateMarkerSummaries.pl : script/generateMarkerSummaries.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/generateMarkerSummaries.pl
	$(CP) script/generateMarkerSummaries.pl $(INST_SCRIPT)/generateMarkerSummaries.pl
	$(FIXIN) $(INST_SCRIPT)/generateMarkerSummaries.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/generateMarkerSummaries.pl

$(INST_SCRIPT)/refreshSchema.pl : script/refreshSchema.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/refreshSchema.pl
	$(CP) script/refreshSchema.pl $(INST_SCRIPT)/refreshSchema.pl
	$(FIXIN) $(INST_SCRIPT)/refreshSchema.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/refreshSchema.pl

$(INST_SCRIPT)/assignMarkerAllelesets.pl : script/assignMarkerAllelesets.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/assignMarkerAllelesets.pl
	$(CP) script/assignMarkerAllelesets.pl $(INST_SCRIPT)/assignMarkerAllelesets.pl
	$(FIXIN) $(INST_SCRIPT)/assignMarkerAllelesets.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/assignMarkerAllelesets.pl

$(INST_SCRIPT)/createHGVmart.pl : script/createHGVmart.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/createHGVmart.pl
	$(CP) script/createHGVmart.pl $(INST_SCRIPT)/createHGVmart.pl
	$(FIXIN) $(INST_SCRIPT)/createHGVmart.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/createHGVmart.pl

$(INST_SCRIPT)/packData.pl : script/packData.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/packData.pl
	$(CP) script/packData.pl $(INST_SCRIPT)/packData.pl
	$(FIXIN) $(INST_SCRIPT)/packData.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/packData.pl

$(INST_SCRIPT)/deploySystem.pl : script/deploySystem.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/deploySystem.pl
	$(CP) script/deploySystem.pl $(INST_SCRIPT)/deploySystem.pl
	$(FIXIN) $(INST_SCRIPT)/deploySystem.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/deploySystem.pl

$(INST_SCRIPT)/markerResultsets.pl : script/markerResultsets.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerResultsets.pl
	$(CP) script/markerResultsets.pl $(INST_SCRIPT)/markerResultsets.pl
	$(FIXIN) $(INST_SCRIPT)/markerResultsets.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerResultsets.pl

$(INST_SCRIPT)/deletedMarkerFromdbSNP.pl : script/deletedMarkerFromdbSNP.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl
	$(CP) script/deletedMarkerFromdbSNP.pl $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl
	$(FIXIN) $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/deletedMarkerFromdbSNP.pl

$(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl : script/hgvbaseg2pweb_fastcgi.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl
	$(CP) script/hgvbaseg2pweb_fastcgi.pl $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl
	$(FIXIN) $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/hgvbaseg2pweb_fastcgi.pl

$(INST_SCRIPT)/liftoverFromAny.pl : script/liftoverFromAny.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/liftoverFromAny.pl
	$(CP) script/liftoverFromAny.pl $(INST_SCRIPT)/liftoverFromAny.pl
	$(FIXIN) $(INST_SCRIPT)/liftoverFromAny.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/liftoverFromAny.pl

$(INST_SCRIPT)/clearOldData.pl : script/clearOldData.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/clearOldData.pl
	$(CP) script/clearOldData.pl $(INST_SCRIPT)/clearOldData.pl
	$(FIXIN) $(INST_SCRIPT)/clearOldData.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/clearOldData.pl

$(INST_SCRIPT)/calculateMarkersImported.pl : script/calculateMarkersImported.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/calculateMarkersImported.pl
	$(CP) script/calculateMarkersImported.pl $(INST_SCRIPT)/calculateMarkersImported.pl
	$(FIXIN) $(INST_SCRIPT)/calculateMarkersImported.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/calculateMarkersImported.pl

$(INST_SCRIPT)/broadT2Dliftover.pl : script/broadT2Dliftover.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/broadT2Dliftover.pl
	$(CP) script/broadT2Dliftover.pl $(INST_SCRIPT)/broadT2Dliftover.pl
	$(FIXIN) $(INST_SCRIPT)/broadT2Dliftover.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/broadT2Dliftover.pl

$(INST_SCRIPT)/assignMarkerIDs.pl : script/assignMarkerIDs.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/assignMarkerIDs.pl
	$(CP) script/assignMarkerIDs.pl $(INST_SCRIPT)/assignMarkerIDs.pl
	$(FIXIN) $(INST_SCRIPT)/assignMarkerIDs.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/assignMarkerIDs.pl

$(INST_SCRIPT)/loadMarkerGFF.pl : script/loadMarkerGFF.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/loadMarkerGFF.pl
	$(CP) script/loadMarkerGFF.pl $(INST_SCRIPT)/loadMarkerGFF.pl
	$(FIXIN) $(INST_SCRIPT)/loadMarkerGFF.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/loadMarkerGFF.pl

$(INST_SCRIPT)/storeXML.pl : script/storeXML.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/storeXML.pl
	$(CP) script/storeXML.pl $(INST_SCRIPT)/storeXML.pl
	$(FIXIN) $(INST_SCRIPT)/storeXML.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/storeXML.pl

$(INST_SCRIPT)/generateImages.pl : script/generateImages.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/generateImages.pl
	$(CP) script/generateImages.pl $(INST_SCRIPT)/generateImages.pl
	$(FIXIN) $(INST_SCRIPT)/generateImages.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/generateImages.pl

$(INST_SCRIPT)/hgvbaseg2pweb_server.pl : script/hgvbaseg2pweb_server.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/hgvbaseg2pweb_server.pl
	$(CP) script/hgvbaseg2pweb_server.pl $(INST_SCRIPT)/hgvbaseg2pweb_server.pl
	$(FIXIN) $(INST_SCRIPT)/hgvbaseg2pweb_server.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/hgvbaseg2pweb_server.pl

$(INST_SCRIPT)/markerGFFFromDatabase.pl : script/markerGFFFromDatabase.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerGFFFromDatabase.pl
	$(CP) script/markerGFFFromDatabase.pl $(INST_SCRIPT)/markerGFFFromDatabase.pl
	$(FIXIN) $(INST_SCRIPT)/markerGFFFromDatabase.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerGFFFromDatabase.pl

$(INST_SCRIPT)/importData.pl : script/importData.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/importData.pl
	$(CP) script/importData.pl $(INST_SCRIPT)/importData.pl
	$(FIXIN) $(INST_SCRIPT)/importData.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/importData.pl

$(INST_SCRIPT)/optimiseMarkerDB.pl : script/optimiseMarkerDB.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/optimiseMarkerDB.pl
	$(CP) script/optimiseMarkerDB.pl $(INST_SCRIPT)/optimiseMarkerDB.pl
	$(FIXIN) $(INST_SCRIPT)/optimiseMarkerDB.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/optimiseMarkerDB.pl

$(INST_SCRIPT)/markerXMLFromUniSTS.pl : script/markerXMLFromUniSTS.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerXMLFromUniSTS.pl
	$(CP) script/markerXMLFromUniSTS.pl $(INST_SCRIPT)/markerXMLFromUniSTS.pl
	$(FIXIN) $(INST_SCRIPT)/markerXMLFromUniSTS.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerXMLFromUniSTS.pl

$(INST_SCRIPT)/validateXML.pl : script/validateXML.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/validateXML.pl
	$(CP) script/validateXML.pl $(INST_SCRIPT)/validateXML.pl
	$(FIXIN) $(INST_SCRIPT)/validateXML.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/validateXML.pl

$(INST_SCRIPT)/backupDatabases.pl : script/backupDatabases.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/backupDatabases.pl
	$(CP) script/backupDatabases.pl $(INST_SCRIPT)/backupDatabases.pl
	$(FIXIN) $(INST_SCRIPT)/backupDatabases.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/backupDatabases.pl

$(INST_SCRIPT)/updateLegacyMarkers.pl : script/updateLegacyMarkers.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/updateLegacyMarkers.pl
	$(CP) script/updateLegacyMarkers.pl $(INST_SCRIPT)/updateLegacyMarkers.pl
	$(FIXIN) $(INST_SCRIPT)/updateLegacyMarkers.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/updateLegacyMarkers.pl

$(INST_SCRIPT)/optimiseStudyDB.pl : script/optimiseStudyDB.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/optimiseStudyDB.pl
	$(CP) script/optimiseStudyDB.pl $(INST_SCRIPT)/optimiseStudyDB.pl
	$(FIXIN) $(INST_SCRIPT)/optimiseStudyDB.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/optimiseStudyDB.pl

$(INST_SCRIPT)/viewWebPerformance.pl : script/viewWebPerformance.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/viewWebPerformance.pl
	$(CP) script/viewWebPerformance.pl $(INST_SCRIPT)/viewWebPerformance.pl
	$(FIXIN) $(INST_SCRIPT)/viewWebPerformance.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/viewWebPerformance.pl

$(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl : script/hgvbaseg2pweb_cgi.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl
	$(CP) script/hgvbaseg2pweb_cgi.pl $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl
	$(FIXIN) $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/hgvbaseg2pweb_cgi.pl

$(INST_SCRIPT)/markerGFFFromAny.pl : script/markerGFFFromAny.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerGFFFromAny.pl
	$(CP) script/markerGFFFromAny.pl $(INST_SCRIPT)/markerGFFFromAny.pl
	$(FIXIN) $(INST_SCRIPT)/markerGFFFromAny.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerGFFFromAny.pl

$(INST_SCRIPT)/serverCheck.pl : script/serverCheck.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/serverCheck.pl
	$(CP) script/serverCheck.pl $(INST_SCRIPT)/serverCheck.pl
	$(FIXIN) $(INST_SCRIPT)/serverCheck.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/serverCheck.pl

$(INST_SCRIPT)/generateBulkdata.pl : script/generateBulkdata.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/generateBulkdata.pl
	$(CP) script/generateBulkdata.pl $(INST_SCRIPT)/generateBulkdata.pl
	$(FIXIN) $(INST_SCRIPT)/generateBulkdata.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/generateBulkdata.pl

$(INST_SCRIPT)/precalculateBins.pl : script/precalculateBins.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/precalculateBins.pl
	$(CP) script/precalculateBins.pl $(INST_SCRIPT)/precalculateBins.pl
	$(FIXIN) $(INST_SCRIPT)/precalculateBins.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/precalculateBins.pl

$(INST_SCRIPT)/generateBrowserDB.pl : script/generateBrowserDB.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/generateBrowserDB.pl
	$(CP) script/generateBrowserDB.pl $(INST_SCRIPT)/generateBrowserDB.pl
	$(FIXIN) $(INST_SCRIPT)/generateBrowserDB.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/generateBrowserDB.pl

$(INST_SCRIPT)/markerXMLFromDBGV.pl : script/markerXMLFromDBGV.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerXMLFromDBGV.pl
	$(CP) script/markerXMLFromDBGV.pl $(INST_SCRIPT)/markerXMLFromDBGV.pl
	$(FIXIN) $(INST_SCRIPT)/markerXMLFromDBGV.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerXMLFromDBGV.pl

$(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl : script/markerMergeXMLFromdbSNP.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl
	$(CP) script/markerMergeXMLFromdbSNP.pl $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl
	$(FIXIN) $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/markerMergeXMLFromdbSNP.pl

$(INST_SCRIPT)/legacyMarkerFromdbSNP.pl : script/legacyMarkerFromdbSNP.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl
	$(CP) script/legacyMarkerFromdbSNP.pl $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl
	$(FIXIN) $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/legacyMarkerFromdbSNP.pl

$(INST_SCRIPT)/optimiseBrowserDB.pl : script/optimiseBrowserDB.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/optimiseBrowserDB.pl
	$(CP) script/optimiseBrowserDB.pl $(INST_SCRIPT)/optimiseBrowserDB.pl
	$(FIXIN) $(INST_SCRIPT)/optimiseBrowserDB.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/optimiseBrowserDB.pl

$(INST_SCRIPT)/scheduleJobs.pl : script/scheduleJobs.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/scheduleJobs.pl
	$(CP) script/scheduleJobs.pl $(INST_SCRIPT)/scheduleJobs.pl
	$(FIXIN) $(INST_SCRIPT)/scheduleJobs.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/scheduleJobs.pl

$(INST_SCRIPT)/indexForTextsearch.pl : script/indexForTextsearch.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/indexForTextsearch.pl
	$(CP) script/indexForTextsearch.pl $(INST_SCRIPT)/indexForTextsearch.pl
	$(FIXIN) $(INST_SCRIPT)/indexForTextsearch.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/indexForTextsearch.pl

$(INST_SCRIPT)/hgvbaseg2pweb_test.pl : script/hgvbaseg2pweb_test.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/hgvbaseg2pweb_test.pl
	$(CP) script/hgvbaseg2pweb_test.pl $(INST_SCRIPT)/hgvbaseg2pweb_test.pl
	$(FIXIN) $(INST_SCRIPT)/hgvbaseg2pweb_test.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/hgvbaseg2pweb_test.pl

$(INST_SCRIPT)/phenoXMLFromdbGaP.pl : script/phenoXMLFromdbGaP.pl $(FIRST_MAKEFILE) $(INST_SCRIPT)$(DFSEP).exists $(INST_BIN)$(DFSEP).exists
	$(NOECHO) $(RM_F) $(INST_SCRIPT)/phenoXMLFromdbGaP.pl
	$(CP) script/phenoXMLFromdbGaP.pl $(INST_SCRIPT)/phenoXMLFromdbGaP.pl
	$(FIXIN) $(INST_SCRIPT)/phenoXMLFromdbGaP.pl
	-$(NOECHO) $(CHMOD) $(PERM_RWX) $(INST_SCRIPT)/phenoXMLFromdbGaP.pl



# --- MakeMaker subdirs section:

# none

# --- MakeMaker clean_subdirs section:
clean_subdirs :
	$(NOECHO) $(NOOP)


# --- MakeMaker clean section:

# Delete temporary files but do not touch installed files. We don't delete
# the Makefile here so a later make realclean still has a makefile to use.

clean :: clean_subdirs
	- $(RM_F) \
	  *$(LIB_EXT) core \
	  core.[0-9] $(INST_ARCHAUTODIR)/extralibs.all \
	  core.[0-9][0-9] $(BASEEXT).bso \
	  pm_to_blib.ts core.[0-9][0-9][0-9][0-9] \
	  $(BASEEXT).x $(BOOTSTRAP) \
	  perl$(EXE_EXT) tmon.out \
	  *$(OBJ_EXT) pm_to_blib \
	  $(INST_ARCHAUTODIR)/extralibs.ld blibdirs.ts \
	  core.[0-9][0-9][0-9][0-9][0-9] *perl.core \
	  core.*perl.*.? $(MAKE_APERL_FILE) \
	  perl $(BASEEXT).def \
	  core.[0-9][0-9][0-9] mon.out \
	  lib$(BASEEXT).def perlmain.c \
	  perl.exe so_locations \
	  $(BASEEXT).exp 
	- $(RM_RF) \
	  blib 
	- $(MV) $(FIRST_MAKEFILE) $(MAKEFILE_OLD) $(DEV_NULL)


# --- MakeMaker realclean_subdirs section:
realclean_subdirs :
	$(NOECHO) $(NOOP)


# --- MakeMaker realclean section:
# Delete temporary files (via clean) and also delete dist files
realclean purge ::  clean realclean_subdirs
	- $(RM_F) \
	  $(MAKEFILE_OLD) $(FIRST_MAKEFILE) 
	- $(RM_RF) \
	  MYMETA.yml $(DISTVNAME) 


# --- MakeMaker metafile section:
metafile :
	$(NOECHO) $(NOOP)


# --- MakeMaker signature section:
signature :
	cpansign -s


# --- MakeMaker dist_basics section:
distclean :: realclean distcheck
	$(NOECHO) $(NOOP)

distcheck :
	$(PERLRUN) "-MExtUtils::Manifest=fullcheck" -e fullcheck

skipcheck :
	$(PERLRUN) "-MExtUtils::Manifest=skipcheck" -e skipcheck

manifest :
	$(PERLRUN) "-MExtUtils::Manifest=mkmanifest" -e mkmanifest

veryclean : realclean
	$(RM_F) *~ */*~ *.orig */*.orig *.bak */*.bak *.old */*.old 



# --- MakeMaker dist_core section:

dist : $(DIST_DEFAULT) $(FIRST_MAKEFILE)
	$(NOECHO) $(ABSPERLRUN) -l -e 'print '\''Warning: Makefile possibly out of date with $(VERSION_FROM)'\''' \
	  -e '    if -e '\''$(VERSION_FROM)'\'' and -M '\''$(VERSION_FROM)'\'' < -M '\''$(FIRST_MAKEFILE)'\'';' --

tardist : $(DISTVNAME).tar$(SUFFIX)
	$(NOECHO) $(NOOP)

uutardist : $(DISTVNAME).tar$(SUFFIX)
	uuencode $(DISTVNAME).tar$(SUFFIX) $(DISTVNAME).tar$(SUFFIX) > $(DISTVNAME).tar$(SUFFIX)_uu

$(DISTVNAME).tar$(SUFFIX) : distdir
	$(PREOP)
	$(TO_UNIX)
	$(TAR) $(TARFLAGS) $(DISTVNAME).tar $(DISTVNAME)
	$(RM_RF) $(DISTVNAME)
	$(COMPRESS) $(DISTVNAME).tar
	$(POSTOP)

zipdist : $(DISTVNAME).zip
	$(NOECHO) $(NOOP)

$(DISTVNAME).zip : distdir
	$(PREOP)
	$(ZIP) $(ZIPFLAGS) $(DISTVNAME).zip $(DISTVNAME)
	$(RM_RF) $(DISTVNAME)
	$(POSTOP)

shdist : distdir
	$(PREOP)
	$(SHAR) $(DISTVNAME) > $(DISTVNAME).shar
	$(RM_RF) $(DISTVNAME)
	$(POSTOP)


# --- MakeMaker distdir section:
create_distdir :
	$(RM_RF) $(DISTVNAME)
	$(PERLRUN) "-MExtUtils::Manifest=manicopy,maniread" \
		-e "manicopy(maniread(),'$(DISTVNAME)', '$(DIST_CP)');"

distdir : create_distdir  
	$(NOECHO) $(NOOP)



# --- MakeMaker dist_test section:
disttest : distdir
	cd $(DISTVNAME) && $(ABSPERLRUN) Makefile.PL 
	cd $(DISTVNAME) && $(MAKE) $(PASTHRU)
	cd $(DISTVNAME) && $(MAKE) test $(PASTHRU)



# --- MakeMaker dist_ci section:

ci :
	$(PERLRUN) "-MExtUtils::Manifest=maniread" \
	  -e "@all = keys %{ maniread() };" \
	  -e "print(qq{Executing $(CI) @all\n}); system(qq{$(CI) @all});" \
	  -e "print(qq{Executing $(RCS_LABEL) ...\n}); system(qq{$(RCS_LABEL) @all});"


# --- MakeMaker distmeta section:
distmeta : create_distdir metafile
	$(NOECHO) cd $(DISTVNAME) && $(ABSPERLRUN) -MExtUtils::Manifest=maniadd -e 'eval { maniadd({q{META.yml} => q{Module meta-data (added by MakeMaker)}}) } ' \
	  -e '    or print "Could not add META.yml to MANIFEST: $${'\''@'\''}\n"' --



# --- MakeMaker distsignature section:
distsignature : create_distdir
	$(NOECHO) cd $(DISTVNAME) && $(ABSPERLRUN) -MExtUtils::Manifest=maniadd -e 'eval { maniadd({q{SIGNATURE} => q{Public-key signature (added by MakeMaker)}}) } ' \
	  -e '    or print "Could not add SIGNATURE to MANIFEST: $${'\''@'\''}\n"' --
	$(NOECHO) cd $(DISTVNAME) && $(TOUCH) SIGNATURE
	cd $(DISTVNAME) && cpansign -s



# --- MakeMaker install section:

install :: pure_install doc_install
	$(NOECHO) $(NOOP)

install_perl :: pure_perl_install doc_perl_install
	$(NOECHO) $(NOOP)

install_site :: pure_site_install doc_site_install
	$(NOECHO) $(NOOP)

install_vendor :: pure_vendor_install doc_vendor_install
	$(NOECHO) $(NOOP)

pure_install :: pure_$(INSTALLDIRS)_install
	$(NOECHO) $(NOOP)

doc_install :: doc_$(INSTALLDIRS)_install
	$(NOECHO) $(NOOP)

pure__install : pure_site_install
	$(NOECHO) $(ECHO) INSTALLDIRS not defined, defaulting to INSTALLDIRS=site

doc__install : doc_site_install
	$(NOECHO) $(ECHO) INSTALLDIRS not defined, defaulting to INSTALLDIRS=site

pure_perl_install :: all
	$(NOECHO) $(MOD_INSTALL) \
		read $(PERL_ARCHLIB)/auto/$(FULLEXT)/.packlist \
		write $(DESTINSTALLARCHLIB)/auto/$(FULLEXT)/.packlist \
		$(INST_LIB) $(DESTINSTALLPRIVLIB) \
		$(INST_ARCHLIB) $(DESTINSTALLARCHLIB) \
		$(INST_BIN) $(DESTINSTALLBIN) \
		$(INST_SCRIPT) $(DESTINSTALLSCRIPT) \
		$(INST_MAN1DIR) $(DESTINSTALLMAN1DIR) \
		$(INST_MAN3DIR) $(DESTINSTALLMAN3DIR)
	$(NOECHO) $(WARN_IF_OLD_PACKLIST) \
		$(SITEARCHEXP)/auto/$(FULLEXT)


pure_site_install :: all
	$(NOECHO) $(MOD_INSTALL) \
		read $(SITEARCHEXP)/auto/$(FULLEXT)/.packlist \
		write $(DESTINSTALLSITEARCH)/auto/$(FULLEXT)/.packlist \
		$(INST_LIB) $(DESTINSTALLSITELIB) \
		$(INST_ARCHLIB) $(DESTINSTALLSITEARCH) \
		$(INST_BIN) $(DESTINSTALLSITEBIN) \
		$(INST_SCRIPT) $(DESTINSTALLSITESCRIPT) \
		$(INST_MAN1DIR) $(DESTINSTALLSITEMAN1DIR) \
		$(INST_MAN3DIR) $(DESTINSTALLSITEMAN3DIR)
	$(NOECHO) $(WARN_IF_OLD_PACKLIST) \
		$(PERL_ARCHLIB)/auto/$(FULLEXT)

pure_vendor_install :: all
	$(NOECHO) $(MOD_INSTALL) \
		read $(VENDORARCHEXP)/auto/$(FULLEXT)/.packlist \
		write $(DESTINSTALLVENDORARCH)/auto/$(FULLEXT)/.packlist \
		$(INST_LIB) $(DESTINSTALLVENDORLIB) \
		$(INST_ARCHLIB) $(DESTINSTALLVENDORARCH) \
		$(INST_BIN) $(DESTINSTALLVENDORBIN) \
		$(INST_SCRIPT) $(DESTINSTALLVENDORSCRIPT) \
		$(INST_MAN1DIR) $(DESTINSTALLVENDORMAN1DIR) \
		$(INST_MAN3DIR) $(DESTINSTALLVENDORMAN3DIR)

doc_perl_install :: all
	$(NOECHO) $(ECHO) Appending installation info to $(DESTINSTALLARCHLIB)/perllocal.pod
	-$(NOECHO) $(MKPATH) $(DESTINSTALLARCHLIB)
	-$(NOECHO) $(DOC_INSTALL) \
		"Module" "$(NAME)" \
		"installed into" "$(INSTALLPRIVLIB)" \
		LINKTYPE "$(LINKTYPE)" \
		VERSION "$(VERSION)" \
		EXE_FILES "$(EXE_FILES)" \
		>> $(DESTINSTALLARCHLIB)/perllocal.pod

doc_site_install :: all
	$(NOECHO) $(ECHO) Appending installation info to $(DESTINSTALLARCHLIB)/perllocal.pod
	-$(NOECHO) $(MKPATH) $(DESTINSTALLARCHLIB)
	-$(NOECHO) $(DOC_INSTALL) \
		"Module" "$(NAME)" \
		"installed into" "$(INSTALLSITELIB)" \
		LINKTYPE "$(LINKTYPE)" \
		VERSION "$(VERSION)" \
		EXE_FILES "$(EXE_FILES)" \
		>> $(DESTINSTALLARCHLIB)/perllocal.pod

doc_vendor_install :: all
	$(NOECHO) $(ECHO) Appending installation info to $(DESTINSTALLARCHLIB)/perllocal.pod
	-$(NOECHO) $(MKPATH) $(DESTINSTALLARCHLIB)
	-$(NOECHO) $(DOC_INSTALL) \
		"Module" "$(NAME)" \
		"installed into" "$(INSTALLVENDORLIB)" \
		LINKTYPE "$(LINKTYPE)" \
		VERSION "$(VERSION)" \
		EXE_FILES "$(EXE_FILES)" \
		>> $(DESTINSTALLARCHLIB)/perllocal.pod


uninstall :: uninstall_from_$(INSTALLDIRS)dirs
	$(NOECHO) $(NOOP)

uninstall_from_perldirs ::
	$(NOECHO) $(UNINSTALL) $(PERL_ARCHLIB)/auto/$(FULLEXT)/.packlist

uninstall_from_sitedirs ::
	$(NOECHO) $(UNINSTALL) $(SITEARCHEXP)/auto/$(FULLEXT)/.packlist

uninstall_from_vendordirs ::
	$(NOECHO) $(UNINSTALL) $(VENDORARCHEXP)/auto/$(FULLEXT)/.packlist


# --- MakeMaker force section:
# Phony target to force checking subdirectories.
FORCE :
	$(NOECHO) $(NOOP)


# --- MakeMaker perldepend section:


# --- MakeMaker makefile section:
# We take a very conservative approach here, but it's worth it.
# We move Makefile to Makefile.old here to avoid gnu make looping.
$(FIRST_MAKEFILE) : Makefile.PL $(CONFIGDEP)
	$(NOECHO) $(ECHO) "Makefile out-of-date with respect to $?"
	$(NOECHO) $(ECHO) "Cleaning current config before rebuilding Makefile..."
	-$(NOECHO) $(RM_F) $(MAKEFILE_OLD)
	-$(NOECHO) $(MV)   $(FIRST_MAKEFILE) $(MAKEFILE_OLD)
	- $(MAKE) $(USEMAKEFILE) $(MAKEFILE_OLD) clean $(DEV_NULL)
	$(PERLRUN) Makefile.PL 
	$(NOECHO) $(ECHO) "==> Your Makefile has been rebuilt. <=="
	$(NOECHO) $(ECHO) "==> Please rerun the $(MAKE) command.  <=="
	$(FALSE)



# --- MakeMaker staticmake section:

# --- MakeMaker makeaperl section ---
MAP_TARGET    = perl
FULLPERL      = /usr/bin/perl

$(MAP_TARGET) :: static $(MAKE_APERL_FILE)
	$(MAKE) $(USEMAKEFILE) $(MAKE_APERL_FILE) $@

$(MAKE_APERL_FILE) : $(FIRST_MAKEFILE) pm_to_blib
	$(NOECHO) $(ECHO) Writing \"$(MAKE_APERL_FILE)\" for this $(MAP_TARGET)
	$(NOECHO) $(PERLRUNINST) \
		Makefile.PL DIR= \
		MAKEFILE=$(MAKE_APERL_FILE) LINKTYPE=static \
		MAKEAPERL=1 NORECURS=1 CCCDLFLAGS=


# --- MakeMaker test section:

TEST_VERBOSE=0
TEST_TYPE=test_$(LINKTYPE)
TEST_FILE = test.pl
TEST_FILES = t/00_compile.t t/00_db.t t/00_pubmed.t t/00_util.t t/01_fileparser.t t/01_podcoverage.t t/02_preprocess.t t/02_schema.t t/03_dataimport.t t/03_markerimport.t t/04_freqimport.t t/05_associmport.t t/06_dataimport_export_db.t t/06_dataimport_logicmaker.t t/06_dataimport_retrievemarker.t t/06_dataimport_rule_allelematch.t t/06_dataimport_rule_pos.t t/06_dataimport_rule_strandflip.t t/10_webapp_basic.t t/10_webapp_pages.t t/11_webapp_json.t t/20_hgvmart_compactor.t t/20_hgvmart_general.t t/30_browser_bin.t t/30_browser_core.t t/30_browser_db.t t/40_search_query.t t/70_search_query.t t/99_phenotype_bdown.t
TESTDB_SW = -d

testdb :: testdb_$(LINKTYPE)

test :: $(TEST_TYPE) subdirs-test

subdirs-test ::
	$(NOECHO) $(NOOP)


test_dynamic :: pure_all
	PERL_DL_NONLAZY=1 $(FULLPERLRUN) "-MExtUtils::Command::MM" "-e" "test_harness($(TEST_VERBOSE), 'inc', '$(INST_LIB)', '$(INST_ARCHLIB)')" $(TEST_FILES)

testdb_dynamic :: pure_all
	PERL_DL_NONLAZY=1 $(FULLPERLRUN) $(TESTDB_SW) "-Iinc" "-I$(INST_LIB)" "-I$(INST_ARCHLIB)" $(TEST_FILE)

test_ : test_dynamic

test_static :: test_dynamic
testdb_static :: testdb_dynamic


# --- MakeMaker ppd section:
# Creates a PPD (Perl Package Description) for a binary distribution.
ppd :
	$(NOECHO) $(ECHO) '<SOFTPKG NAME="$(DISTNAME)" VERSION="4.0">' > $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '    <ABSTRACT></ABSTRACT>' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '    <AUTHOR>Rob Free &lt;rcfree@gmail.com&gt;</AUTHOR>' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '    <IMPLEMENTATION>' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="Catalyst::Action::RenderView" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="Catalyst::Plugin::ConfigLoader" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="Catalyst::Plugin::Static::Simple" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="Catalyst::Runtime" VERSION="5.80029" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="Config::General" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="ExtUtils::MakeMaker" VERSION="6.42" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <REQUIRE NAME="parent::" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <ARCHITECTURE NAME="darwin-thread-multi-2level-5.10" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '        <CODEBASE HREF="" />' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '    </IMPLEMENTATION>' >> $(DISTNAME).ppd
	$(NOECHO) $(ECHO) '</SOFTPKG>' >> $(DISTNAME).ppd


# --- MakeMaker pm_to_blib section:

pm_to_blib : $(FIRST_MAKEFILE) $(TO_INST_PM)
	$(NOECHO) $(ABSPERLRUN) -MExtUtils::Install -e 'pm_to_blib({@ARGV}, '\''$(INST_LIB)/auto'\'', q[$(PM_FILTER)], '\''$(PERM_DIR)'\'')' -- \
	  lib/HGVbaseG2P/DataImport/RetrieveMarker.pm blib/lib/HGVbaseG2P/DataImport/RetrieveMarker.pm \
	  lib/HGVbaseG2P/Database/Xapian.pm blib/lib/HGVbaseG2P/Database/Xapian.pm \
	  lib/HGVbaseG2P/Search/Performer/AutoDetect.pm blib/lib/HGVbaseG2P/Search/Performer/AutoDetect.pm \
	  lib/HGVbaseG2P/Exception.pm blib/lib/HGVbaseG2P/Exception.pm \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_term.pm \
	  lib/HGVbaseG2P/DataImport/DataElement.pm blib/lib/HGVbaseG2P/DataImport/DataElement.pm \
	  lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm blib/lib/HGVbaseG2P/DataImport/Rule/FreqFieldCheck.pm \
	  lib/HGVbaseG2P/Search/Filter/Threshold.pm blib/lib/HGVbaseG2P/Search/Filter/Threshold.pm \
	  lib/Bio/Graphics/Glyph/square.pm blib/lib/Bio/Graphics/Glyph/square.pm \
	  lib/HGVbaseG2P/Schema/Marker/Markeralias.pm blib/lib/HGVbaseG2P/Schema/Marker/Markeralias.pm \
	  lib/Schedule/Depend/Utilities.pm blib/lib/Schedule/Depend/Utilities.pm \
	  lib/HGVbaseG2P/Schema/Hpo/Term_path.pm blib/lib/HGVbaseG2P/Schema/Hpo/Term_path.pm \
	  lib/HGVbaseG2P/Database/Feature.pm blib/lib/HGVbaseG2P/Database/Feature.pm \
	  lib/HGVbaseG2P/Search/Filter/SortBy.pm blib/lib/HGVbaseG2P/Search/Filter/SortBy.pm \
	  lib/HGVbaseG2P/Search/Detector/None.pm blib/lib/HGVbaseG2P/Search/Detector/None.pm \
	  lib/HGVbaseG2P/Search/Filter/PageSize.pm blib/lib/HGVbaseG2P/Search/Filter/PageSize.pm \
	  lib/Catalyst/Plugin/Portal.pm blib/lib/Catalyst/Plugin/Portal.pm \
	  lib/HGVbaseG2P/Search/Retriever/Genes/None.pm blib/lib/HGVbaseG2P/Search/Retriever/Genes/None.pm \
	  lib/Bio/Graphics/Glyph/allele_column_multi.pm blib/lib/Bio/Graphics/Glyph/allele_column_multi.pm \
	  lib/HGVbaseG2P/Search/Query/RegionMarkers.pm blib/lib/HGVbaseG2P/Search/Query/RegionMarkers.pm \
	  lib/HGVbaseG2P/Search/Filter/Page.pm blib/lib/HGVbaseG2P/Search/Filter/Page.pm \
	  lib/HGVbaseG2P/Database/Marker.pm blib/lib/HGVbaseG2P/Database/Marker.pm \
	  lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm blib/lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromNumbers.pm \
	  lib/HGVbaseG2P/Web/Model/SessionDB.pm blib/lib/HGVbaseG2P/Web/Model/SessionDB.pm \
	  lib/HGVbaseG2P/Search/Filter.pm blib/lib/HGVbaseG2P/Search/Filter.pm \
	  lib/Template/Plugin/Wiki.pm blib/lib/Template/Plugin/Wiki.pm \
	  lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm blib/lib/HGVbaseG2P/Schema/Study/Genotypefrequency.pm \
	  lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm blib/lib/HGVbaseG2P/Schema/Study/Studyoverlap.pm \
	  lib/HGVbaseG2P/Search/Filter/QueryString.pm blib/lib/HGVbaseG2P/Search/Filter/QueryString.pm \
	  lib/HGVbaseG2P/Schema/Study/Bundleloci.pm blib/lib/HGVbaseG2P/Schema/Study/Bundleloci.pm \
	  lib/HGVbaseG2P/Schema/Marker/Marker.pm blib/lib/HGVbaseG2P/Schema/Marker/Marker.pm \
	  lib/HGVbaseG2P/Search/Filter/GeneRegion.pm blib/lib/HGVbaseG2P/Search/Filter/GeneRegion.pm \
	  lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm blib/lib/HGVbaseG2P/Schema/Study/Genotypedloci.pm \
	  lib/HGVbaseG2P/Schema/Study/StudyOld.pm blib/lib/HGVbaseG2P/Schema/Study/StudyOld.pm \
	  lib/HGVbaseG2P/Web/Controller/Study.pm blib/lib/HGVbaseG2P/Web/Controller/Study.pm \
	  lib/Bio/Graphics/Glyph/arc.pm blib/lib/Bio/Graphics/Glyph/arc.pm \
	  lib/HGVbaseG2P/Schema/Study/Citationcrossref.pm blib/lib/HGVbaseG2P/Schema/Study/Citationcrossref.pm \
	  lib/HGVbaseG2P/Schema/Ontology.pm blib/lib/HGVbaseG2P/Schema/Ontology.pm \
	  lib/HGVbaseG2P/AccessControl.pm blib/lib/HGVbaseG2P/AccessControl.pm \
	  lib/HGVbaseG2P/hgvbase.conf blib/lib/HGVbaseG2P/hgvbase.conf \
	  lib/XML/Atom/SimpleFeedOS.pm blib/lib/XML/Atom/SimpleFeedOS.pm \
	  lib/HGVbaseG2P/Schema/Study/Pppa.pm blib/lib/HGVbaseG2P/Schema/Study/Pppa.pm \
	  lib/HGVbaseG2P/Schema/Study/Studycrossref.pm blib/lib/HGVbaseG2P/Schema/Study/Studycrossref.pm \
	  lib/HGVbaseG2P/Schema/Marker/Haplotypecrossref.pm blib/lib/HGVbaseG2P/Schema/Marker/Haplotypecrossref.pm \
	  lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm blib/lib/HGVbaseG2P/DataImport/Rule/AllelesMatch.pm \
	  lib/HGVbaseG2P/Schema/Study/Experiment.pm blib/lib/HGVbaseG2P/Schema/Study/Experiment.pm \
	  lib/WWW/Search/PubmedLiteMesh.pm blib/lib/WWW/Search/PubmedLiteMesh.pm \
	  lib/HGVbaseG2P/Search/Performer.pm blib/lib/HGVbaseG2P/Search/Performer.pm \
	  lib/HGVbaseG2P/Schema/Study/Crossref.pm blib/lib/HGVbaseG2P/Schema/Study/Crossref.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Annotation.pm \
	  lib/HGVbaseG2P/Schema/Study/SamplepanelcrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/SamplepanelcrossrefOld.pm \
	  lib/HGVbaseG2P/Database/Browser.pm blib/lib/HGVbaseG2P/Database/Browser.pm \
	  lib/HGVbaseG2P/Database/Mart.pm blib/lib/HGVbaseG2P/Database/Mart.pm \
	  lib/HGVbaseG2P/Deploy.pm blib/lib/HGVbaseG2P/Deploy.pm \
	  lib/HGVbaseG2P/Search/Detector/GeneRegion.pm blib/lib/HGVbaseG2P/Search/Detector/GeneRegion.pm \
	  lib/HGVbaseG2P/Schema/Study/Fcs.pm blib/lib/HGVbaseG2P/Schema/Study/Fcs.pm \
	  lib/HGVbaseG2P/DataImport/PanelRule.pm blib/lib/HGVbaseG2P/DataImport/PanelRule.pm \
	  lib/HGVbaseG2P/Web/Controller/Root.pm blib/lib/HGVbaseG2P/Web/Controller/Root.pm \
	  lib/HGVbaseG2P/Browser/Core.html blib/lib/HGVbaseG2P/Browser/Core.html \
	  lib/HGVbaseG2P/Web/Controller/Browser.pm blib/lib/HGVbaseG2P/Web/Controller/Browser.pm \
	  lib/HGVbaseG2P/Schema/Marker/Build.pm blib/lib/HGVbaseG2P/Schema/Marker/Build.pm \
	  lib/HGVbaseG2P/DataImport/Plugin.pm blib/lib/HGVbaseG2P/DataImport/Plugin.pm \
	  lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm blib/lib/HGVbaseG2P/Schema/Study/Samplepanelcrossref.pm \
	  lib/HGVbaseG2P/Web/Update.pm blib/lib/HGVbaseG2P/Web/Update.pm \
	  lib/HGVbaseG2P/Web.pm blib/lib/HGVbaseG2P/Web.pm \
	  lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm blib/lib/HGVbaseG2P/Schema/Study/Genotypedbundle.pm \
	  lib/HGVbaseG2P/Schema/Marker/Assaymarker.pm blib/lib/HGVbaseG2P/Schema/Marker/Assaymarker.pm \
	  lib/HGVbaseG2P/Browser/Region.pm blib/lib/HGVbaseG2P/Browser/Region.pm \
	  lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm blib/lib/HGVbaseG2P/Search/Filter/RestrictToAnnotation.pm \
	  lib/HGVbaseG2P/Schema/Study/Hotlinker.pm blib/lib/HGVbaseG2P/Schema/Study/Hotlinker.pm \
	  lib/HGVbaseG2P/Schema/Marker/Genotypedef.pm blib/lib/HGVbaseG2P/Schema/Marker/Genotypedef.pm \
	  lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Resultsethotlink.pm \
	  lib/HGVbaseG2P/Schema/Study/PhenotypemethodcrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/PhenotypemethodcrossrefOld.pm \
	  lib/HGVbaseG2P/DataAccess/GAS.pm blib/lib/HGVbaseG2P/DataAccess/GAS.pm \
	  lib/HGVbaseG2P/Search/Retriever/Marker.pm blib/lib/HGVbaseG2P/Search/Retriever/Marker.pm \
	  lib/HGVbaseG2P/Browser/Creator.pm blib/lib/HGVbaseG2P/Browser/Creator.pm \
	  lib/HGVbaseG2P/DataImport/Rule/SplitField.pm blib/lib/HGVbaseG2P/DataImport/Rule/SplitField.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodhotlink.pm \
	  lib/HGVbaseG2P/Search/Filter/MarkerCount.pm blib/lib/HGVbaseG2P/Search/Filter/MarkerCount.pm \
	  lib/HGVbaseG2P/FileParser.pm blib/lib/HGVbaseG2P/FileParser.pm \
	  lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm blib/lib/HGVbaseG2P/DataImport/Rule/RegexFields.pm \
	  lib/HGVbaseG2P/Schema/Marker/Hotlink.pm blib/lib/HGVbaseG2P/Schema/Marker/Hotlink.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Marker.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypeannotation.pm \
	  lib/HGVbaseG2P/Search/Detector/Keyword.pm blib/lib/HGVbaseG2P/Search/Detector/Keyword.pm \
	  lib/HGVbaseG2P/Browser/Genome.pm blib/lib/HGVbaseG2P/Browser/Genome.pm \
	  lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm blib/lib/HGVbaseG2P/Schema/Study/Studysamplepanel.pm \
	  lib/Template/Plugin/XML/RSS/URL.pm blib/lib/Template/Plugin/XML/RSS/URL.pm \
	  lib/Catalyst/Plugin/UploadProgressSizeLimit.pm blib/lib/Catalyst/Plugin/UploadProgressSizeLimit.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypevaluedetail.pm \
	  lib/HGVbaseG2P/DataImport/Core.html blib/lib/HGVbaseG2P/DataImport/Core.html \
	  lib/HGVbaseG2P/Browser/Binning.pm blib/lib/HGVbaseG2P/Browser/Binning.pm \
	  lib/HGVbaseG2P/Schema/Study/Analysismethod.pm blib/lib/HGVbaseG2P/Schema/Study/Analysismethod.pm \
	  lib/DBIx/DBSchema/DBD/mysql.pm blib/lib/DBIx/DBSchema/DBD/mysql.pm \
	  lib/HGVbaseG2P/DataImport/Tool.pm blib/lib/HGVbaseG2P/DataImport/Tool.pm \
	  lib/HGVbaseG2P/Schema/Marker/Allele.pm blib/lib/HGVbaseG2P/Schema/Marker/Allele.pm \
	  lib/HGVbaseG2P/Web/Model/Xapian.pm blib/lib/HGVbaseG2P/Web/Model/Xapian.pm \
	  lib/HGVbaseG2P/Schema/Study/Submission.pm blib/lib/HGVbaseG2P/Schema/Study/Submission.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypepropertycitation.pm \
	  lib/HGVbaseG2P/Search/Retriever/Studies.pm blib/lib/HGVbaseG2P/Search/Retriever/Studies.pm \
	  lib/HGVbaseG2P/Schema/User/User.pm blib/lib/HGVbaseG2P/Schema/User/User.pm \
	  lib/HGVbaseG2P/Search/Filter/ResultSet.pm blib/lib/HGVbaseG2P/Search/Filter/ResultSet.pm \
	  lib/HGVbaseG2P/Schema/Study.pm blib/lib/HGVbaseG2P/Schema/Study.pm \
	  lib/HGVbaseG2P/Schema/Hpo/Synonym.pm blib/lib/HGVbaseG2P/Schema/Hpo/Synonym.pm \
	  lib/HGVbaseG2P/Search/Filter/ViewType.pm blib/lib/HGVbaseG2P/Search/Filter/ViewType.pm \
	  lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm blib/lib/HGVbaseG2P/Web/Controller/PhenotypeMethod.pm \
	  lib/HGVbaseG2P/Database/Study.pm blib/lib/HGVbaseG2P/Database/Study.pm \
	  lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm blib/lib/HGVbaseG2P/Schema/Study/PhenotypepropertyOld.pm \
	  lib/HGVbaseG2P/Web/View/TT.pm blib/lib/HGVbaseG2P/Web/View/TT.pm \
	  lib/Bio/Graphics/Browser.pm blib/lib/Bio/Graphics/Browser.pm \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/File.pm \
	  lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm blib/lib/HGVbaseG2P/Schema/Study/ExperimentTypelist.pm \
	  lib/HGVbaseG2P/Search/Query.pm.old blib/lib/HGVbaseG2P/Search/Query.pm.old \
	  lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm blib/lib/HGVbaseG2P/Search/Query/ResultsetMarkers.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Keyword.pm \
	  lib/HGVbaseG2P/Schema/Study/Resultset.pm blib/lib/HGVbaseG2P/Schema/Study/Resultset.pm \
	  lib/HGVbaseG2P.pm blib/lib/HGVbaseG2P.pm \
	  lib/HGVbaseG2P/Search.pm blib/lib/HGVbaseG2P/Search.pm \
	  lib/HGVbaseG2P/Schema/Marker/AssayTypelist.pm blib/lib/HGVbaseG2P/Schema/Marker/AssayTypelist.pm \
	  lib/HGVbaseG2P/Browser/Upload.pm blib/lib/HGVbaseG2P/Browser/Upload.pm \
	  lib/HGVbaseG2P/Web/Controller/Samplepanel.pm blib/lib/HGVbaseG2P/Web/Controller/Samplepanel.pm \
	  lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm blib/lib/HGVbaseG2P/Schema/Study/Selectioncriteria.pm \
	  lib/HGVbaseG2P/Search/Filter/ID.pm blib/lib/HGVbaseG2P/Search/Filter/ID.pm \
	  lib/HGVbaseG2P/Schema/Study/ResultsetcrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/ResultsetcrossrefOld.pm \
	  lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm blib/lib/HGVbaseG2P/Schema/Study/Usedmarkerset.pm \
	  lib/HGVbaseG2P/Database/Hapmap.pm blib/lib/HGVbaseG2P/Database/Hapmap.pm \
	  lib/HGVbaseG2P/Schema/BrowserUpload.pm blib/lib/HGVbaseG2P/Schema/BrowserUpload.pm \
	  lib/HGVbaseG2P/Schema/Marker/Crossref.pm blib/lib/HGVbaseG2P/Schema/Marker/Crossref.pm \
	  lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm blib/lib/HGVbaseG2P/Schema/Study/Allelefrequency.pm \
	  lib/HGVbaseG2P/Database.pm blib/lib/HGVbaseG2P/Database.pm \
	  lib/HGVbaseG2P/Schema/Marker/Assaymarkergenotype.pm blib/lib/HGVbaseG2P/Schema/Marker/Assaymarkergenotype.pm \
	  lib/HGVbaseG2P/Schema/User.pm blib/lib/HGVbaseG2P/Schema/User.pm \
	  lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm blib/lib/HGVbaseG2P/DataImport/Plugin/AffymetrixLookup.pm \
	  lib/HGVbaseG2P/Search/Query/Genes.pm blib/lib/HGVbaseG2P/Search/Query/Genes.pm \
	  lib/HGVbaseG2P/Search/Query/Marker.pm blib/lib/HGVbaseG2P/Search/Query/Marker.pm \
	  lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm blib/lib/HGVbaseG2P/Search/Retriever/Genes/Keyword.pm \
	  lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm blib/lib/HGVbaseG2P/Schema/Study/Samplepaneloverlap.pm \
	  lib/HGVbaseG2P/Search/Query/MarkerResults.pm blib/lib/HGVbaseG2P/Search/Query/MarkerResults.pm \
	  lib/HGVbaseG2P/Search/Query/Phenotypes.pm blib/lib/HGVbaseG2P/Search/Query/Phenotypes.pm \
	  lib/Bio/Graphics/Glyph/ideogram.pm blib/lib/Bio/Graphics/Glyph/ideogram.pm \
	  lib/HGVbaseG2P/Schema/Marker/Assayedgenotype.pm blib/lib/HGVbaseG2P/Schema/Marker/Assayedgenotype.pm \
	  lib/HGVbaseG2P/Search/Filter/Study.pm blib/lib/HGVbaseG2P/Search/Filter/Study.pm \
	  lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm blib/lib/HGVbaseG2P/Schema/Study/Hotlinkcollection.pm \
	  lib/HGVbaseG2P/Schema/Study/Contribution.pm blib/lib/HGVbaseG2P/Schema/Study/Contribution.pm \
	  lib/HGVbaseG2P/Web/Controller/Search.pm blib/lib/HGVbaseG2P/Web/Controller/Search.pm \
	  lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm blib/lib/HGVbaseG2P/DataImport/Rule/PopulateGenotypesFromAlleles.pm \
	  lib/HGVbaseG2P/Schema/Study/Build.pm blib/lib/HGVbaseG2P/Schema/Study/Build.pm \
	  lib/HGVbaseG2P/Search/Query.pm blib/lib/HGVbaseG2P/Search/Query.pm \
	  lib/HGVbaseG2P/Web/Controller/Phenotype.pm blib/lib/HGVbaseG2P/Web/Controller/Phenotype.pm \
	  lib/HGVbaseG2P/Web/Model/UserDB.pm blib/lib/HGVbaseG2P/Web/Model/UserDB.pm \
	  lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm blib/lib/HGVbaseG2P/Schema/Study/CompositionTypelist.pm \
	  lib/HGVbaseG2P/Schema/Study/Hotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Hotlink.pm \
	  lib/HGVbaseG2P/Search/Query/Studies.pm blib/lib/HGVbaseG2P/Search/Query/Studies.pm \
	  lib/HGVbaseG2P/Schema/Study/Resultsetcrossref.pm blib/lib/HGVbaseG2P/Schema/Study/Resultsetcrossref.pm \
	  lib/HGVbaseG2P/Search/Query/Phenotype.pm blib/lib/HGVbaseG2P/Search/Query/Phenotype.pm \
	  lib/HGVbaseG2P/Web/Controller/Admin.pm blib/lib/HGVbaseG2P/Web/Controller/Admin.pm \
	  lib/HGVbaseG2P/Search/Detector/Annotation.pm blib/lib/HGVbaseG2P/Search/Detector/Annotation.pm \
	  lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm blib/lib/Bio/Das/ProServer/SourceAdaptor/hgvbaseg2p.pm \
	  lib/HGVbaseG2P/Schema/Marker/Genotype.pm blib/lib/HGVbaseG2P/Schema/Marker/Genotype.pm \
	  lib/HGVbaseG2P/Schema/Study/StudycrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/StudycrossrefOld.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodcitation.pm \
	  lib/HGVbaseG2P/Schema/Study/Pvsc.pm blib/lib/HGVbaseG2P/Schema/Study/Pvsc.pm \
	  lib/HGVbaseG2P/XMLStore.pm blib/lib/HGVbaseG2P/XMLStore.pm \
	  lib/HGVbaseG2P/DataImport/Rule.pm blib/lib/HGVbaseG2P/DataImport/Rule.pm \
	  lib/CGI/HGVToggle.pm blib/lib/CGI/HGVToggle.pm \
	  lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Experimenthotlink.pm \
	  lib/HGVbaseG2P/Util.pm blib/lib/HGVbaseG2P/Util.pm \
	  lib/HGVbaseG2P/Browser/Util.pm blib/lib/HGVbaseG2P/Browser/Util.pm \
	  lib/HGVbaseG2P/Search/Aggregator.pm blib/lib/HGVbaseG2P/Search/Aggregator.pm \
	  lib/HGVbaseG2P/Database/Hpo.pm blib/lib/HGVbaseG2P/Database/Hpo.pm \
	  lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm blib/lib/HGVbaseG2P/Schema/Study/Studyanalysismethod.pm \
	  lib/HGVbaseG2P/Search/Retriever/Marker/None.pm blib/lib/HGVbaseG2P/Search/Retriever/Marker/None.pm \
	  lib/HGVbaseG2P/DataImport/Exception.pm blib/lib/HGVbaseG2P/DataImport/Exception.pm \
	  lib/HGVbaseG2P/Web/Util.pm blib/lib/HGVbaseG2P/Web/Util.pm \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_concept.pm \
	  lib/HGVbaseG2P/DataImport/LogicMaker.pm blib/lib/HGVbaseG2P/DataImport/LogicMaker.pm \
	  lib/Catalyst/Plugin/HGVbaseG2P.pm blib/lib/Catalyst/Plugin/HGVbaseG2P.pm \
	  lib/HGVbaseG2P/Schema/Marker/Markercoord.pm blib/lib/HGVbaseG2P/Schema/Marker/Markercoord.pm \
	  lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm blib/lib/HGVbaseG2P/DataImport/Tool/PubmedSearch.pm \
	  lib/HGVbaseG2P/Schema/Session/Sessions.pm blib/lib/HGVbaseG2P/Schema/Session/Sessions.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Feature.pm \
	  lib/HGVbaseG2P/DataImport/Export/Database.pm blib/lib/HGVbaseG2P/DataImport/Export/Database.pm \
	  lib/DBIx/DBStag.pm blib/lib/DBIx/DBStag.pm \
	  lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm blib/lib/HGVbaseG2P/Schema/Study/Assayedpanelcollection.pm \
	  lib/HGVbaseG2P/Schema/Marker/Markercrossref.pm blib/lib/HGVbaseG2P/Schema/Marker/Markercrossref.pm \
	  lib/HGVbaseG2P/Web/Model/OntologyDB.pm blib/lib/HGVbaseG2P/Web/Model/OntologyDB.pm \
	  lib/HGVbaseG2P/Schema/Study/CitationcrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/CitationcrossrefOld.pm \
	  lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm blib/lib/HGVbaseG2P/Schema/Study/Frequencycluster.pm \
	  lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm blib/lib/HGVbaseG2P/Search/Filter/PhenotypeID.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/Concept.pm \
	  lib/HGVbaseG2P/Database/BrowserUpload.pm blib/lib/HGVbaseG2P/Database/BrowserUpload.pm \
	  lib/Schedule/Depend/Execute.pm blib/lib/Schedule/Depend/Execute.pm \
	  lib/HGVbaseG2P/Search/Filter/Variation.pm blib/lib/HGVbaseG2P/Search/Filter/Variation.pm \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_termtree.pm \
	  lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm blib/lib/HGVbaseG2P/Search/Query/MarkerFrequencies.pm \
	  lib/Bio/Graphics/Glyph/myreverseplot.pm blib/lib/Bio/Graphics/Glyph/myreverseplot.pm \
	  lib/Bio/Graphics/Glyph/stackplot.pm blib/lib/Bio/Graphics/Glyph/stackplot.pm \
	  lib/HGVbaseG2P/Schema/Hpo/Term.pm blib/lib/HGVbaseG2P/Schema/Hpo/Term.pm \
	  lib/HGVbaseG2P/Web/Output.pm blib/lib/HGVbaseG2P/Web/Output.pm \
	  lib/Catalyst/Controller/UserRegistration.pm blib/lib/Catalyst/Controller/UserRegistration.pm \
	  lib/HGVbaseG2P/Schema/Hapmap.pm blib/lib/HGVbaseG2P/Schema/Hapmap.pm \
	  lib/HGVbaseG2P/Schema/Session.pm blib/lib/HGVbaseG2P/Schema/Session.pm \
	  lib/HGVbaseG2P/Web/Controller/Marker.pm blib/lib/HGVbaseG2P/Web/Controller/Marker.pm \
	  lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm blib/lib/HGVbaseG2P/Schema/Study/SamplepanelSourceofdnalist.pm \
	  lib/HGVbaseG2P/Schema/Study/Study.pm blib/lib/HGVbaseG2P/Schema/Study/Study.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypeproperty.pm \
	  lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm blib/lib/HGVbaseG2P/Search/Retriever/Studies/StudiesNone.pm \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Custom.pm \
	  lib/HGVbaseG2P/Web/Controller/Dialog.pm blib/lib/HGVbaseG2P/Web/Controller/Dialog.pm \
	  lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm blib/lib/HGVbaseG2P/Search/Retriever/Marker/Keyword.pm \
	  lib/HGVbaseG2P/Search/Filter/Chromosome.pm blib/lib/HGVbaseG2P/Search/Filter/Chromosome.pm \
	  lib/HGVbaseG2P/Web/Model/BrowserDB.pm blib/lib/HGVbaseG2P/Web/Model/BrowserDB.pm \
	  lib/HGVbaseG2P/Browser/Core.pm blib/lib/HGVbaseG2P/Browser/Core.pm \
	  lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm blib/lib/HGVbaseG2P/Web/Controller/Assayedpanel.pm \
	  lib/Bio/Graphics/Glyph/star.pm blib/lib/Bio/Graphics/Glyph/star.pm \
	  lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm blib/lib/HGVbaseG2P/Schema/Ontology/Mesh_heading.pm \
	  lib/HGVbaseG2P/Schema/Study/Studycitation.pm blib/lib/HGVbaseG2P/Schema/Study/Studycitation.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethodcrossref.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethodcrossref.pm \
	  lib/HGVbaseG2P/Search/Filter/Concept.pm blib/lib/HGVbaseG2P/Search/Filter/Concept.pm \
	  lib/HGVbaseG2P/Web/Model/StudyDB.pm blib/lib/HGVbaseG2P/Web/Model/StudyDB.pm \
	  lib/HGVbaseG2P/Schema/Hpo.pm blib/lib/HGVbaseG2P/Schema/Hpo.pm \
	  lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm blib/lib/HGVbaseG2P/Search/Retriever/MarkerResults.pm \
	  lib/HGVbaseG2P/Web/Model/SeqFeature.pm blib/lib/HGVbaseG2P/Web/Model/SeqFeature.pm \
	  lib/HGVbaseG2P/Schema/Study/Researcher.pm blib/lib/HGVbaseG2P/Schema/Study/Researcher.pm \
	  lib/HGVbaseG2P/Schema/Study/Author.pm blib/lib/HGVbaseG2P/Schema/Study/Author.pm \
	  lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm blib/lib/HGVbaseG2P/DataImport/Rule/CalcGenotypeFreqsFromNumbers.pm \
	  lib/HGVbaseG2P/Search/Query/All.pm blib/lib/HGVbaseG2P/Search/Query/All.pm \
	  lib/Schedule/Depend.pm blib/lib/Schedule/Depend.pm \
	  lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm blib/lib/HGVbaseG2P/DataImport/Rule/PositionsMatch.pm \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip/Field.pm \
	  lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Studyhotlink.pm \
	  lib/Schedule/Depend/Config.pm blib/lib/Schedule/Depend/Config.pm \
	  lib/HGVbaseG2P/Schema/Study/Significance.pm blib/lib/HGVbaseG2P/Schema/Study/Significance.pm \
	  lib/HGVbaseG2P/Schema/Marker/AssayMethodlist.pm blib/lib/HGVbaseG2P/Schema/Marker/AssayMethodlist.pm \
	  lib/HGVbaseG2P/Schema/Marker.pm blib/lib/HGVbaseG2P/Schema/Marker.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypevalue.pm \
	  lib/HGVbaseG2P/Search/Detector.pm blib/lib/HGVbaseG2P/Search/Detector.pm \
	  lib/HGVbaseG2P/Search/Retriever/Genes.pm blib/lib/HGVbaseG2P/Search/Retriever/Genes.pm \
	  lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm blib/lib/HGVbaseG2P/Schema/Hpo/Hpo2mesh.pm \
	  lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm blib/lib/HGVbaseG2P/DataImport/Rule/StrandFlip.pm \
	  lib/HGVbaseG2P/Schema/Marker/Haplotype.pm blib/lib/HGVbaseG2P/Schema/Marker/Haplotype.pm \
	  lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm blib/lib/HGVbaseG2P/Search/Retriever/Phenotype/None.pm \
	  lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm blib/lib/HGVbaseG2P/DataImport/Rule/CalcAlleleFreqsFromGenotypeNumbers.pm \
	  lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm blib/lib/HGVbaseG2P/Search/Retriever/Studies/StudiesKeyword.pm \
	  lib/HGVbaseG2P/Web/Controller/Gene.pm blib/lib/HGVbaseG2P/Web/Controller/Gene.pm \
	  lib/HGVbaseG2P/Schema/Marker/Markerrevision.pm blib/lib/HGVbaseG2P/Schema/Marker/Markerrevision.pm \
	  lib/HGVbaseG2P/Schema/Browser.pm blib/lib/HGVbaseG2P/Schema/Browser.pm \
	  lib/HGVbaseG2P/DataImport/Core.pm blib/lib/HGVbaseG2P/DataImport/Core.pm \
	  lib/HGVbaseG2P/Search/Query/Markers.pm blib/lib/HGVbaseG2P/Search/Query/Markers.pm \
	  lib/HGVbaseG2P/Schema/Marker/Datasource.pm blib/lib/HGVbaseG2P/Schema/Marker/Datasource.pm \
	  lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm blib/lib/HGVbaseG2P/Schema/Study/Samplepanelhotlink.pm \
	  lib/HGVbaseG2P/DataImport.pm blib/lib/HGVbaseG2P/DataImport.pm \
	  lib/HGVbaseG2P/Schema/Study/CrossrefOld.pm blib/lib/HGVbaseG2P/Schema/Study/CrossrefOld.pm \
	  lib/HGVbaseG2P/ConfigLogger.pm blib/lib/HGVbaseG2P/ConfigLogger.pm \
	  lib/HGVbaseG2P/Search/Retriever.pm blib/lib/HGVbaseG2P/Search/Retriever.pm \
	  lib/HGVbaseG2P/Web/Controller/User.pm blib/lib/HGVbaseG2P/Web/Controller/User.pm \
	  lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm blib/lib/HGVbaseG2P/Schema/Study/Assayedpanel.pm \
	  lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm blib/lib/HGVbaseG2P/Schema/Study/Phenotypemethod.pm \
	  lib/HGVbaseG2P/Search/DataFeed.pm blib/lib/HGVbaseG2P/Search/DataFeed.pm \
	  lib/HGVbaseG2P/Schema/Study/Properties.pm blib/lib/HGVbaseG2P/Schema/Study/Properties.pm \
	  lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm blib/lib/HGVbaseG2P/Schema/Study/Diseasecategorybkup.pm \
	  lib/HGVbaseG2P/Web/Model/HpoDB.pm blib/lib/HGVbaseG2P/Web/Model/HpoDB.pm \
	  lib/HGVbaseG2P/Web/Model/MarkerDB.pm blib/lib/HGVbaseG2P/Web/Model/MarkerDB.pm \
	  lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm blib/lib/HGVbaseG2P/Search/Detector/ConceptTerm.pm \
	  lib/HGVbaseG2P/Schema/Study/Samplepanel.pm blib/lib/HGVbaseG2P/Schema/Study/Samplepanel.pm \
	  lib/HGVbaseG2P/DataImport/Export.pm blib/lib/HGVbaseG2P/DataImport/Export.pm \
	  lib/Catalyst/Controller/OpenID.pm blib/lib/Catalyst/Controller/OpenID.pm \
	  lib/HGVbaseG2P/Search/Filter/ExportFormat.pm blib/lib/HGVbaseG2P/Search/Filter/ExportFormat.pm \
	  lib/HGVbaseG2P/Database/Ontology.pm blib/lib/HGVbaseG2P/Database/Ontology.pm \
	  lib/HGVbaseG2P/Search/Performer/NoDetect.pm blib/lib/HGVbaseG2P/Search/Performer/NoDetect.pm \
	  lib/HGVbaseG2P/Schema/Study/Citation.pm blib/lib/HGVbaseG2P/Schema/Study/Citation.pm \
	  lib/HGVbaseG2P/Browser/Display.pm blib/lib/HGVbaseG2P/Browser/Display.pm \
	  lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm blib/lib/HGVbaseG2P/Schema/Study/Experimentassayedpanel.pm \
	  lib/HGVbaseG2P/DataImport/Plugin/GSK.pm blib/lib/HGVbaseG2P/DataImport/Plugin/GSK.pm \
	  lib/Bio/Graphics/Glyph/multiline_text.pm blib/lib/Bio/Graphics/Glyph/multiline_text.pm \
	  lib/HGVbaseG2P/Search/Detector/Marker.pm blib/lib/HGVbaseG2P/Search/Detector/Marker.pm \
	  lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm blib/lib/HGVbaseG2P/Search/Retriever/Marker/Annotation.pm \
	  lib/HGVbaseG2P/Schema/Marker/Haplotypeallele.pm blib/lib/HGVbaseG2P/Schema/Marker/Haplotypeallele.pm \
	  lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm blib/lib/HGVbaseG2P/Search/Retriever/Genes/Annotation.pm 
	$(NOECHO) $(TOUCH) pm_to_blib


# --- MakeMaker selfdocument section:


# --- MakeMaker postamble section:


# End.
# Postamble by Module::Install 0.91
# --- Module::Install::Admin::Makefile section:

realclean purge ::
	$(RM_F) $(DISTVNAME).tar$(SUFFIX)
	$(RM_F) MANIFEST.bak _build
	$(PERL) "-Ilib" "-MModule::Install::Admin" -e "remove_meta()"
	$(RM_RF) inc

reset :: purge

upload :: test dist
	cpan-upload -verbose $(DISTVNAME).tar$(SUFFIX)

grok ::
	perldoc Module::Install

distsign ::
	cpansign -s

catalyst_par :: all
	$(NOECHO) $(PERL) -Ilib -Minc::Module::Install -MModule::Install::Catalyst -e"Catalyst::Module::Install::_catalyst_par( '', 'HGVbaseG2P', { CLASSES => [], CORE => 0, ENGINE => 'CGI', MULTIARCH => 0, SCRIPT => '', USAGE => q## } )"
# --- Module::Install::AutoInstall section:

config :: installdeps
	$(NOECHO) $(NOOP)

checkdeps ::
	$(PERL) Makefile.PL --checkdeps

installdeps ::
	$(NOECHO) $(NOOP)

