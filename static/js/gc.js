window.s = [];
window.l = {};
GCSearch.urls = {
		"studies":"/studies",
		"admin.studies":"/admin/studies",
		"phenotypes":"/phenotypes",
		"markers":"/markers",
		"browser.genome":"/browser/genome"
};

GCSearch.headings = {
		"studies":['Access Level','Identifier','Name','Phenotypes','Citations','Marker<br/>Count','Add to <br/>Browser'],
		"admin.studies":['Identifier','Name','Public Level','Private Level']
};
GCSearch.widths = {
		"studies":['5em','10em','20em','30em','20em','5em','5em'],
		"admin.studies":['5em','20em','5em','5em','20em']
};
GCSearch.types = {
		"studies":"table",
		"phenotypes":"table",
		"markers":"table",
		"browser.genome":"html",
		"admin.studies":"table"
};

//var balloon;
function GwasCentral(server) {
	this.server = server;
}

GwasCentral.prototype.search = function(args) {
	args.server = this.server;
	return new GCSearch(args);
}

GwasCentral.prototype.login = function(args) {
	args.server = this.server;
	return new GCLogin(args);
}

GwasCentral.prototype.report = function(type,gcid) {
	var functions = {};
	showDialog(true,functions,"http://192.168.56.4/" + type + "/" + gcid, null,"Report",800,600);
}
function GCLogin(args) {
	GwasCentral.addScript("file:///Users/robf/Projects/GwasCentral/dt.js","text/javascript");
	GwasCentral.addScript(args.server + "/js/balloon.js");
	GwasCentral.addScript(args.server + "/js/balloon.config.js");
	GwasCentral.addScript(args.server + "/gbrowse/js/yahoo-dom-event.js");
	var me = this;
	jQuery.receiveMessage(
		function(e){

			me.determineLogin(e.data);	
		}, this.server
	);
	this.type = args.type;
	this.args = args;
	this.server = args.server;
	
	var loginID=args.loginID;
	if (!loginID) loginID="login";
	this.loginID = loginID;
	window.l = this;
	this.createLogin(loginID);
	
}

GCLogin.prototype.createLogin = function(loginID) {
	var url = this.server + "/login";
	var div = document.getElementById(this.loginID);
	var html = '';
	html=html + '<div id="userpanel"></div>';
	html=html + '<iframe id="gc_iframe" frameborder="0" style="display:none" width="0px" height="0px" src="' + this.server + '/login" />'
	div.innerHTML=html;
	
}

GCLogin.prototype.determineLogin = function(stringData){
	var temp = stringData.split("&");
	alert(stringData);
	var data = {};
	for (var i=0;i<temp.length;i++) {
		var keyval = temp[i].split("=");
		data[keyval[0]]=keyval[1];
	}
	window.loginData=data;
	var status = data.status;
	//log.info("data:" + Log.dumpObject(data));
	if (status == 'loggedin') {
		this._showLoggedIn();
	}
	else if (status == 'notloggedin') {
		this._showLogin()
	}
	else if (status == 'loginfailed') {
		this._showLogin("Username and/or password were not recognised");
	}
}

GCLogin.prototype.logout = function() {
	var url = this.server + "/logout";
	var iframe = document.getElementById('gc_iframe');
	iframe.src=url;
	jQuery.receiveMessage(
		function(e){
			me.determineLogin(e.data);	
		}, this.server
	);
}

GCLogin.prototype._showLogin = function(errorMsg, afterFunction) {
	var div = document.getElementById("userpanel");
	var html = '';
	if (errorMsg) {
		html = html + '<div class="error">' + errorMsg + '</div>';
	}
	html=html + '<form method="post" action="' + this.server + '/login" target="gc_iframe"><label for="gc_user">User:</label><input type="text" name="user"/><label for="gc_password">Password:</label><input type="password" name="pass"/><input type="submit" name="login" value="Login"/></form>';
	this.userID = "gc_user";
	this.passID = "gc_pass";
	div.innerHTML=html;
	this._refreshSearches();
}

GCLogin.prototype._showLoggedIn = function(afterFunction) {
	var div = document.getElementById("userpanel");
	var html = "Logged in as " + window.loginData.nickname + " <a href=\"#\" onclick=\"window.l.logout()\">Log out</a>";
	div.innerHTML=html;
	this._refreshSearches();
}

GCLogin.prototype._refreshSearches = function() {
	var searches = window.s;
	for(var i=0;i<searches.length;i++) {
		searches[i].refresh();
	}	
}

function GCSearch(args) {
	GwasCentral.addScript(args.server + "/js/balloon.js");
	GwasCentral.addScript(args.server + "/js/balloon.config.js");
	GwasCentral.addScript(args.server + "/gbrowse/js/yahoo-dom-event.js");
	//balloon = new Balloon();
	this.type = args.type;
	this.args = args;
	this.server = args.server;
	
	var no = window.s.length;
	
	this.no = no;
	
	var filtersID = args.filtersID;
	var resultsID = args.resultsID;

	if (!filtersID) filtersID="filters";
	if (!resultsID) resultsID="results";
	this.filtersID = filtersID;
	this.resultsID = resultsID;
	
	this.showFilters = args.showFilters;
	window.s[no] = this;
	
	this.refresh();
	
}
GCSearch.prototype.refresh = function() {
	if (this.showFilters) { 
		this.createFilters(this.filtersID);
	}
	else {
		if (this.showResults) {
			this.createResults(this.resultsID);
		}
	}
}
function getQuerystring(key)
{
   key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return null;
  else
    return qs[1];
}

GCSearch.prototype.createResults = function() {
	//log.info("createResults()");

	var div = document.getElementById(this.resultsID);
	var argsType = this.type;
	var url = this.server + GCSearch.urls[argsType];
	
    var html = "";
    var queryString = this.generateQueryString();

    if (GCSearch.types[argsType]=='table') {
		

    	var html = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\"results" + this.no + "\">";
    	    html = html + "<thead><tr>";
    	    
    	    var headings = GCSearch.headings[argsType];
    	    var widths = GCSearch.widths[argsType];
    	    if (!headings) {
    	    	throw("There were no headings for type:" + argsType);
    	    }
    	    //log.info("headings count:" + headings.length);
    	    //log.info("headings:" + Log.dumpObject(headings));
    	    for (var h=0; h<headings.length; h++) {
    	    	html = html + "<th style='width:" + widths[h] + "'>" + headings[h] + "</th>";
    	    }
    	 
    	    html=html + "</tr></thead><tbody><tr><td colspan=\"" + (headings.length) + "\"><img src='" + this.server + "/images/ajax-loader2.gif'></td></tr></tbody></table>";
    	    div.innerHTML=html;
		var dialog = document.getElementById("dialog");
		var report = document.getElementById("report");
		if (!dialog) {
			dialog = document.createElement('div');
			dialog.setAttribute('id','dialog');
			dialog.setAttribute('style','display:none');
			document.body.appendChild(dialog);
		}
		if (!report) {
			report = document.createElement('report');
			report.setAttribute('id','report');
			report.setAttribute('style','display:none');
			document.body.appendChild(report);
		}
		
	    $('#results' + this.no).dataTable( {
	         "sAjaxSource":url + "?" + queryString,
	         "sPaginationType": 'full_numbers',
	         "aoColumns": GCSearch.headings[argsType],
	         "bProcessing": false,
			 "bServerSide": true,
	         "fnServerData": function ( sSource, aoData, fnCallback ) {
	              $.ajax( {
	                   "dataType": 'jsonp',
	                   "type": "GET",
	                   "url": sSource,
	                   "data": aoData,
	                   "success": fnCallback,
	              } );
	         },
			 "sPaginationType": "full_numbers",
			 "bLengthChange":false,
			 "iDisplayLength":jQuery("form#form" + this.no + " select[name='iDisplaySize']").val(),
			 "sDom": '<"top"ip>rt<"bottom"f><"clear">'
	    } );
    }
    if (GCSearch.types[argsType]=='html') {
    	
    	window.me = this;
    	$.ajax({
    		  url: url + "?" + queryString,
    		  dataType: "jsonp",
    		  success: function(data, textStatus,jqXHR){
    			  var fDiv = document.getElementById(window.me.resultsID);
    			  //var html = "";
//    			  var filters = data.aaData;
//    			  for(var i=0;i<filters.length;i++) {
//    				  html = html + filters[i];
//    			  }
    			  fDiv.innerHTML = data.aaData;
    		  }
    		});
    }
}

GCSearch.prototype.generateQueryString = function() {
	//var fnElement = document.getElementById("filterNames" + this.no);
	var qs = new Array();

		jQuery("form#form" + this.no + " input,select").each(function(){
			if (this.name && this.name.size != 0) {
					qs.push(this.name + "=" + this.value);
			}
		});
		qs.push("jsonp=1");
//	}
//	else {
//		var params = this.parameters;
//		for (var p in params) {
//			qs.push(p + "=" + params[p]);
//		}
//	}
	
	return qs.join("&");
}


GCSearch.prototype.createFilters=function() {
	var argsType = this.type;
	var url = this.server + GCSearch.urls[argsType] + "?filters=1&jsonp=1";
	window.me = this;
	$.ajax({
		  url: url,
		  dataType: "jsonp",
		  success: function(data, textStatus,jqXHR){
		  	var no = window.me.no;
			  var fDiv = document.getElementById(window.me.filtersID);
			  var html = "<form onsubmit='window.s[" + no + "].createResults(); return false' id='form" + no + "'>";
			  var filters = data.aaData;
			  for(var i=0;i<filters.length;i++) {
				  html = html + filters[i];
			  }
				html=html + "</form>";
			  fDiv.innerHTML = html;
			  
			  jQuery("form#form" + no + " input,select").each(function(){
					var qs = getQuerystring(this.name);
					if (qs) {
						this.value = qs;
					}
				});
			  
			  if (window.me.args.showResults) {
				  window.me.createResults();
			  }
		  }
		});
}

GwasCentral.addLink = function(href, type, rel) {
    var e = document.createElement('link');
    e.setAttribute('href', href);
    e.setAttribute('type', type);
    e.setAttribute('rel', rel);
    document.getElementsByTagName('head')[0].appendChild(e);
}

GwasCentral.addScript = function(src, type) {
    var e = document.createElement('script');
    e.setAttribute('src', src);
    e.setAttribute('type', type);
    document.getElementsByTagName('head')[0].appendChild(e);
}
function toggleLessMore(identifier) {
    var tag = document.getElementById(identifier + "_tag");
    var image = document.getElementById(identifier + "_image");
    if (image.src.match(/plus/)) {
            image.src = "/images/minus.png";
            image.alt = "-";
			//jQuery("#" + identifier).show('fast');
            document.getElementById(identifier).style.display="block";
    } else {
            image.src = "/images/plus.png";
            image.alt = "+";
			//jQuery("#" + identifier).hide('fast');
            document.getElementById(identifier).style.display="none";
    }
}