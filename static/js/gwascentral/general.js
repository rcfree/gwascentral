// $Id: general.js 1527 2010-09-16 13:48:38Z rcf8 $
/**
 * @description Non UI associated Javascript functions
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

var ajaxTimeout = 30000;

function calcRegionFromDecimal(chrLocation) {
	var locString = new Number(chrLocation).toString();
	var locInfo = locString.split(".", locString);
	if (locInfo[0] == '1') {
		locInfo[1] = locString.substr(2);
	}
	var chr = locInfo[0];
	if (chr == '23') {
		chr = 'X'
	}
	else if (chr == '24') {
		chr = 'Y'
	}
	
	var actualStart;
	if (locInfo[1]) {
		actualStart = zeroSuffix(locInfo[1], 3);
	}
	else {
		actualStart = '000';
	}
	
	var start;
	if (actualStart == '000') {
		start = 1;
	} else {
		start = (actualStart * 1000000) + 1;
	}
	var stop = start + 9000000 - 1;
	// e('rscale_type').value=scaletype;
	// e('r_threshold').value=threshold;
	
	var region = "chr" + chr + ":" + start + ".." + stop;
	return region;
}

function zeroSuffix(numberString, length) {
	var extraZeroes = length - numberString.length;
	var zeroes = '';
	for ( var i = 0; i < extraZeroes; i++) {
		zeroes = zeroes + '0';
	}
	return numberString + zeroes;
}

function isArray(obj) {
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}

function getURIWithParameters(uri,parameters) {
	var queryString=new Array;
	
	for(var p in parameters) {
		if (parameters[p] != null && parameters[p].length>0) {
			if (isArray(parameters[p])) {
				for(var q=0;q<parameters[p].length;q++) {
					var item = p + "=" + parameters[p][q];
					queryString.push(item);
				}
			}
			else {
				var item = p + "=" + parameters[p];
				queryString.push(item);
			}
		}
	}
	return uri + "?" + queryString.join("&");
}

function isIE6() {
	var version = parseInt($.browser.version.substring(0,1));
	if ($.browser.msie && version<7) {
		return true;
	}
	return false;
}

function calculateRows() {
	 var noResultsets = Vars.getResultsets().length;
	 var plotHeight = Vars.getSmallPlotHeight();
	 var chrLength = Vars.getSmallChrLength();
	 var chrNumber = Vars.getChrNumber();
	
	 var screenWidth = getScreenWidth() - 200;
	
	 var noRows;
	
	 if (e('rotate_multiple') && e('rotate_multiple').checked) {
var singleChr = chrLength + 40;
noRows = Math.floor(screenWidth/singleChr);
	 } else {
	 var presentWidth=0;
if (e('genome_present') && e('genome_present').checked) {
presentWidth = 14 * noResultsets;
}
var singleChr = presentWidth + plotHeight + 83;
var allChr = singleChr * chrNumber;
noRows = Math.ceil(allChr / screenWidth);
	 }
	
	 // GBrowse_karyotype needs rows (not columns) if rotated (ie. 2 columns
	 // would be 12 rows)
	 if (e('rotate_multiple') && e('rotate_multiple').checked)
return 24 / noRows;
	 else
return noRows;
}

function resetFilters() {
	var url = window.location;
	window.location = url.pathname;
	return;
	var ff = document.filterform;
	var fields = ff.elements;
	for(var f=0;f<fields.length;f++) {
		fields[f].value="";
	}
	ff.submit();
}