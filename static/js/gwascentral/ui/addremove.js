// $Id: addremove.js 1446 2010-05-26 14:24:28Z rcf8 $
var action;
var identifier;
var checkboxes;
var updateCallback;
var hasPhenotypes;
var isResultset;
var entityCheckboxes;
var added;

function AddRemove(identifier, isResultset, prefixes){
	
	this.checkboxes = new Array();
	this.entityCheckboxes = new Array();
	
	for(var p=0;p<prefixes.length;p++) {
		var chk = new Checkbox(identifier,prefixes[p]);
		this.checkboxes.push(chk);
		chk.showAjax();
	}
	
	var action = new Action(Vars.getResultsets(), Vars.getStudies(), Vars.getPhenotypes());
	this.action = action;
	
	this.identifier = identifier;
	this.isResultset = isResultset;
	
	if (isResultset) {
		if (Vars.hasPhenotypes()) {
			action.phenotypeIdent =  Vars.getResultsetPhenotype(identifier);
			action.entityResultsets = Vars.getAllResultsetsInEntity(action.phenotypeIdent);
			for(var p=0;p<prefixes.length;p++) {
				this.entityCheckboxes.push(new Checkbox(action.phenotypeIdent,prefixes[p]));
			}
		}
		else {
			action.studyIdent =  Vars.getResultsetStudy(identifier);
			action.entityResultsets = Vars.getAllResultsetsInEntity(action.studyIdent);
			for(var p=0;p<prefixes.length;p++) {
				var echk = new Checkbox(action.studyIdent,prefixes[p]);
				this.entityCheckboxes.push(echk);
			}
		}
	}
	else {
		if (Vars.hasPhenotypes()) {
			action.phenotypeIdent = identifier;
			this.dialogURL = "/dialog/add_phenotype/" + identifier;
			this.dialogTitle = "Data Sets with Phenotype " + identifier;
			
		}
		else {
			action.studyIdent = identifier;
			this.dialogURL = "/dialog/add_study/" + identifier;
			this.dialogTitle = "Data Sets in Study " + identifier;
		}
		action.entityResultsets = Vars.getAllResultsetsInEntity(identifier);
	}
}

AddRemove.isTooMany = function(identifier,isResultset,checkedResultsets) {
	var ar = new AddRemove(identifier,isResultset);
	return action.isTooMany(checkedResultsets);
}

AddRemove.toggle = function(identifier,isResultset, prefix) {
	var ar = new AddRemove(identifier,isResultset, prefix);
	ar.toggle();
}

AddRemove.edit = function(identifier) {
	var ar = new AddRemove(identifier);
	ar.add(true);	
}

AddRemove.check = function(identifier,currCheckbox) {
	var ar = new AddRemove(identifier,isResultset,['action']);
	var rsChecked = Vars.getCheckedResultsets(identifier);
	if (ar.action.isTooMany(rsChecked)) {
		alert("Sorry, you can only add 16 Data Sets to the GWAS Central Browser");
		currCheckbox.setChecked(false);
	}
}

AddRemove.prototype.toggle = function() {
	this.panelAjax();
	if (this.checkboxes[0].isChecked()) {
		this.remove();
	}
	else {
		this.add(false);
	}
}

AddRemove.prototype.checkTooMany = function(resultsets) {
	if (this.action.isTooMany(resultsets)) {
		alert("Sorry, you can only add 16 Data Sets to the GWAS Central Browser");
		this.checkbox.setChecked(false);
		return true;
	}
}

AddRemove.prototype.add = function(showPopup) {
	this.added=true;
	if (this.isResultset) {
		if (this.checkTooMany([this.identifier])) return;
		this.action.addResultsets([this.identifier]);
		this.update();
	}
	else {
		if (showPopup) {
			this._showExperiments();
		}
		else {
			if (this.action.entityResultsets.length == 1) {
				if (this.checkTooMany([this.identifier])) return;

				this.action.addEntityResultsets();
				this.update();
			}
			else {
				this._showExperiments();
			}
		}
	}
}

AddRemove.prototype._showExperiments = function() {
	window.me = this;
	var buttons = {
		Accept:function(){
			var checkedResultsets = Vars.getCheckedResultsets(window.me.identifier);
			var uncheckedResultsets = Vars.getUncheckedResultsets(window.me.identifier)
			var checkedRsCount = checkedResultsets.length;
			jQuery("#dialog").dialog("close");
			window.me.action.removeResultsets(uncheckedResultsets);
			window.me.action.addResultsets(checkedResultsets);
			window.me.update();
			
		},
		Cancel: function(){
			jQuery("#dialog").dialog("close");
			var chks = window.me.checkboxes;
			for(var c=0;c<chks.length;c++) {
				chks[c].showCheckbox();
			}
		}
	}
	showDialog(false,buttons, this.dialogURL, null, this.dialogTitle);
}

AddRemove.prototype.remove = function() {
	if (this.isResultset) {
		this.action.removeResultsets([this.identifier]);
	}
	else {
		this.action.removeEntityResultsets();
	}
	this.update();
}

AddRemove.prototype.panelUpdate = function(data) {
	if (e('dataset_dialog').style.display="block") {
		//e('sel_loading').style.display="none";
		e('dataset_dialog').innerHTML=data;
		//e('browser_sel').style.display="block";
	}
}

AddRemove.prototype.panelAjax = function() {
	if (e('browser_sel')) {
		//e('sel_loading').style.display="block";
		e('browser_sel').style.display="none";
	}
}

AddRemove.prototype.update = function() {
	var me = this;
	var successFunction = function(data, textStatus) {
		me.panelUpdate(data);
		
		Vars.populateFromAddRemove(me);
		
		for(var c=0;c<me.checkboxes.length;c++) {
			me.checkboxes[c].showCheckbox();
		}
	}
	var errorFunction = function (XMLHttpRequest, textStatus, errorThrown) {
		if (e('browser_sel')) e('browser_sel').innerHTML="Sorry, an error occurred while trying to store your changes. Please try again later.";
		alert("Sorry, an error occurred while trying to store your changes. Please try again later.");
	};
	
	var store = new Store(this.action);
	store.storeResultsets(successFunction, errorFunction);
}
