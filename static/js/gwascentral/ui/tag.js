// $Id: tag.js 1446 2010-05-26 14:24:28Z rcf8 $
/**
 * @description Tag UI module for dealing with logic of HTML 'tags' (image associated links with labels). 
 * For example allowing them to be changed (e.g. add to remove)
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function Tag(type,id) {
	this.id = id;
	this.type = type;
}

Tag.prototype.image = function(image) {
	var el = e(this.type + "_" + this.id + "_image");
	if (image!=null) {
		el.src = image;
		if (image=="") { el.style.display="none"; } else { el.style.display="inline" }
	}
	else {
		return el.src;
	}
}

Tag.prototype.onclick = function(onclick) {
	var el = e(this.type + "_" + this.id + "_tag");

	if (onclick!=null) {
		el.onclick = onclick;
	}
	else {
		return el.onclick;
	}
}

Tag.prototype.label = function(label) {
	var el = e(this.type + "_" + this.id + "_label");
	if (el!=null) {
		el.innerHTML=label;
	}
	var im = e(this.type + "_" + this.id + "_tag");
	im.title = label;
	return im.title;
}

Tag.prototype.showAjax = function() {
	this.image('/images/small-ajax-loader.gif');
}

Tag.prototype.showRemove = function() {
	this.image('/images/remove.png');
	this.label("Remove from Browser");
	this.className("remove");
}

Tag.prototype.showAdd = function() {
	this.image('/images/add.png');
	this.label("Add to Browser");
	this.className("add");
}

Tag.prototype.showBlank = function() {
	this.image("");
	this.label("");
}

Tag.prototype.showError = function(label, errorFunction) {
	this.image('/images/error.png');
	this.alt('err');
	this.label(label);
	
	this.onclick(errorFunction);
}

Tag.prototype.showEdit = function() {
	this.image('/images/edit.png');
	this.alt('edit');
}

Tag.prototype.className = function(className) {
	var el = e(this.type + "_" + this.id + "_tag");
	if (el != null) {
		if (className) {
			el.className = className;
		}
		
		return el.className;
	}
}
