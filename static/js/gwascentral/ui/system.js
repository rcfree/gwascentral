// $Id: system.js 1554 2010-10-27 14:29:02Z rcf8 $
/**
 * @description HGVbaseG2P specific system UI functions for going to Browser pages, refreshing pages etc.
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */


	function didYouKnow(message,position) {
		if (Vars.getHidePopup()) return;
		var buttonFunction = function(){
			if (e('hide_popup').checked) {
				var st = new Store();
				st.storePopupOff();
			}
			var dyk = jQuery("#dyk");
			dyk.dialog("close");
		};
		showDialog('info', buttonFunction, "<b>" + message + "</b><br/><br/><input type='checkbox' id='hide_popup'/> Don't show me these popups again",{},"Did you know?",650,120,"dyk",position)                                            
	}

	function showExperiment(identifier) {
		showDialog('info', function() {
			jQuery("#dialog").dialog("close");
		}, "/study/experiment/" + identifier,null,"Experiment");
	}
	
	function showHelp(helpPage) {
		var height = screen.availHeight * (0.8);
		var width = 950
		var url = helpPage;
		if (helpPage.substring(0,7)!="http://") {
			url = hgvbaseURL + "/info/" + helpPage;
		}
		var options = "width="
				+ width
				+ ",height="
				+ height
				+ ",location=no,menubar=yes,status=no,toolbar=yes,scrollbars=yes, resizable=yes";
		window.open(url,
				"_hgvbase_help", options);
	}
	
    // tb143 edit start
    function showTree() {
        var height = screen.availHeight * (0.8);
        var width = 950
        var options = "width="
			+ width
			+ ",height="
			+ height
			+ ",location=no,menubar=no,status=no,toolbar=no,scrollbars=yes, resizable=yes";
	newtree=window.open("/phenotypes/meshtree?nowrappers=1#", "_hgvbase_tree",  options);
        if (window.focus) {newtree.focus()}

    }
    
    function showHPOTree() {
        var height = screen.availHeight * (0.8);
        var width = 950
        var options = "width="
			+ width
			+ ",height="
			+ height
			+ ",location=no,menubar=no,status=no,toolbar=no,scrollbars=yes, resizable=yes";
	newtree=window.open("/phenotypes/hpotree?nowrappers=1#", "_hgvbase_tree",  options);
        if (window.focus) {newtree.focus()}

    }
    
        		
    
    // tb143 edit end

	

	function resizeAbout() {
		var panel = e("about_panel");
		var os = jQuery("#about_contents").offset();
		var fullHeaderHeight = os.top;
		var fullContentsWidth = jQuery("#about_contents").width();

		var marginLeft = 15;
		var marginRight = 15;
		var marginTop = 8;
		var marginBottom = 8;
		
		if (jQuery.browser.msie) {
			var ver = jQuery.browser.version;

			if (ver.substring(0, 1) == "7") {
				winWidth = jQuery(window).width();
				winHeight = jQuery(window).height();
			} else {
				winHeight = document.documentElement.clientHeight;
				winWidth = document.documentElement.clientWidth;
			}
		} else {
			winHeight = jQuery(window).height();
			winWidth = jQuery(window).width();
		}

		winWidth = winWidth - (marginLeft + marginRight);
		winHeight = winHeight - (marginTop + marginBottom);

		var panelWidth;
		if (winHeight < 750) winHeight = 750;
		if (winWidth < 900) winWidth = 900;

		var panelHeight = winHeight - fullHeaderHeight - jQuery('#footer').height() - 40;
		var panelWidth = winWidth - fullContentsWidth - 50;
		panel.style.width = panelWidth + "px";
		panel.style.height = panelHeight + "px";
	}
	
	function showAddMarkers() {
		showDialog('info', function() {
			jQuery("#dialog").dialog("close");
		}, hgvbaseURL + "/admin/study/markers",null,"Add Markers");
	}
	
	function showOptionDiv(divName) {
		var optionDivs = document.getElementsByName('options');
		for(var o=0;o<optionDivs.length;o++) {
			optionDivs[o].style.display="none";
		}
		document.getElementById(divName).style.display="block";
	}
	
	
function goToBrowser(type, query) {
	var resultsetList = Vars.getResultsets().join(",");
	var threshold = Vars.getThreshold();

	if (type == 'genome') {
		window.location.href = "/browser/genome?r=" + resultsetList + "&t=" + threshold; 
	}
	else {
		window.location.href = "/browser/region?r=" + resultsetList + "&t=" + threshold + "&q=" + query;
	}
}

/**
 * @description Checks all chromosome checkboxes on a page (with name = 'chr')
 *              and sets gSettingsChanged
 */
function selectAll() {
	var chrs = document.getElementsByName('chr');
	for(var i=0;i<chrs.length;i++) {
		chrs[i].checked=true;
	}
	gSettingsChanged=1;
}

/**
 * @description Unchecks all chromosome checkboxes on a page (with name = 'chr')
 *              and sets gSettingsChanged
 */
function selectNone() {
	var chrs = document.getElementsByName('chr');
	for(var i=0;i<chrs.length;i++) {
		chrs[i].checked=false;
	}
	gSettingsChanged=1;
}



/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String}
 *            tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
function showSigs(markerResultset) {
	var temp = markerResultset.split(";");
	var markerURL = "/marker/" + temp[0] + "/results?v=table&r=" + temp[1];
	window.top.location=markerURL;
}

function showSigsByMarker(markerIdentifier) {
	switchTabFromTop('markerinfo');
	var markerURL = "/marker/";
	ajaxGetHTML(markerURL,{marker: markerIdentifier, resultsets: getResultsets() },'markerview')
}


// function checkMaxAdded() {
// if (getResultsets().length >= maxResultsets) {
// alert("You can only compare a maximum of " + maxResultsets + " Result Sets");
// return true;
// }
// return false;
// }

// function removeAllStudies() {
// _resultsets = '';
// _studies = '';
// successFunction = function(textStatus, data) {
// window.location.reload();
// }
// storeResultsets(successFunction)
// }


function updateGenomeView() {
	var resultsets = "";
	var threshold = Vars.getThreshold();
	var chr = Vars.getChromosomes();
	var numberChrs = Vars.getChrNumber();
	
	var resultsets = Vars.getResultsets().join(',');
	alert("rs:" + resultsets);
	var sessionid = Vars.getSessionID();
	
	var chrlist;
	var noRows = calculateRows();
	if (chr == 'ALL') {
		chrlist = "ALL";
	}
	else if (chr == 'NONE') {
		e('genomeview_display').innerHTML="<ul style='margin-left:none'><li>No chromosomes selected</li></ul>";
		return;
	}
	
	else {
		chrlist = chr.join("+");
	}
	
		var rotate;

		if (e('rotate_multiple').checked)
			rotate = 1;
		else
			rotate = 0;
		var height = Vars.getSmallChrLength();
		if (noRows > numberChrs) {
			noRows = numberChrs;
		}
		// e('scale_type').value=e('gscale_type').value;
		var settings = new Array();
		settings.push("sc|" + e('sc').value);
		settings.push("genomeview_size|" + e('genomeview_size').value);
		
		var tracks = new Array();
		if (e('rotate_multiple').checked) tracks.push("rotate_multiple");
		if (e('genome_present').checked) tracks.push("genome_present");
			
		var pars = {
				band_labels: 0,
				c: chrlist,
				h:height,
				w: parseInt(height/30),
				e: 1,
				rows: noRows,
				rotate: rotate,
				fp: "GenomeView~t="
					+ threshold + "~r=" + resultsets + "~settings="
					+ settings.join(",") + "~tracks=" + tracks.join(",") + "~sessionid=" + Vars.getSessionID()
		};

		ajaxGetHTML(genomeViewURL, pars,'genomeview_display', 0,
				function(data, textStatus){
					e('genomeview_display').innerHTML = data;
					populateResultsetTotals();
				}
		);
}


//function updateRegionView() {
//	var region = getRegion();
//
//	if (region == null || region == "") {
//		region = getLandmark();
//		if (region==null || region =="") {
//			region==null;
//		}
//	}
//	
//	var resultsetid = getResultsets().join(',');
//	var threshold = getThreshold();
//	
//	e('regionview').style.height="10000px";
//
//	var screenWidth;
//	if (e('view_width').value=='auto') {
//		
//		screenWidth = getScreenWidth() - 450;
//	}
//	else {
//		screenWidth = e('view_width').value;
//	}
//	var pars = new Array();
//	pars.push("width=" + screenWidth);
//	// if (e('genes').checked) { pars.push("label=Genes"); }
//	// if (e('ideogram').checked) { pars.push("label=Ideogram:overview"); }
//	// e('scale_type').value=e('rscale_type').value;
//	pars.push("plugin=RegionView;plugin_action=Settings;plugin_config=1");
//	pars.push("name=" + region);
//	pars.push("RegionView.resultsets=" + resultsetid);
//	pars.push("RegionView.threshold=" + threshold);
//	pars.push("RegionView.settings=" + getSettings() );
//	pars.push("RegionView.tracks=" + getTracks());
//	
//	var regionURL = regionViewURL + "?" + pars.join(";");
//	if (region == null) {
//		region = "";
//	}
//	e('regionview').src = regionURL;
//}

function gvToRV(chrLocation) {
	var threshold = Vars.getThreshold();
	
	var region = calcRegionFromDecimal(chrLocation);
	var url = "/browser/region?q=" + region;
	//if (Vars.isNotSession()) {
		url = url + "&r=" + Vars.getResultsets().join(',');
	//}
	url = url + "&t=" + threshold;
	url = url + "&sc=" + e('scale_type').value;
	setTimeout(function()
	{
	    window.location = url;
	}, 0);
}

function switchResultset(resultsetid, study) {
	
	var action;
	if (e("rs" + resultsetid).checked) {
		action = 'add';
		if (checkMaxAdded()) {
			e("rs" + resultsetid).checked = false;
			return;
		}

	} else {
		type = 'remove';
	}
	
	var resultsets = new Array(resultsetid);
	if (action == 'add') {
		if (checkMaxAdded()) return;
			var resultsets = new Array(resultsetid);
			_resultsets = addResultsets(resultsets);
			_studies = getStudies();		
	} else {
		var count = Vars. countCheckedInStudy(study);
		var delStudy;
		if (count == 0) {
			delStudy = confirm("This will remove the study from the list. Are you sure?");
			if (delStudy) {
				removeStudyFromBrowser(study);
				return;
			}
			else {
				e("rs" + resultsetid).checked = true;
				return;
			}
		}
		var resultsets = new Array(resultsetid);
		_resultsets = removeResultsets(resultsets);
		_studies = getStudies();
	}
	
	successFunction = function(data, textStatus) {
		setResultsets(_resultsets);
		setStudies(_studies);
		setBrowserToUpdate();
		updateChosenStudies(study,false);
	}	
	storeResultsets(successFunction);
}

function removeStudyFromBrowser(study) {
	
	var rs = Vars.getChosenStudyResultsets(study);
	_resultsets = removeResultsets(rs);
	_studies = removeStudy(study);
		
	successFunction = function(data, textStatus) {
		setResultsets(_resultsets);
		setStudies(_studies);
		setBrowserToUpdate();
		updateChosenStudies(study,true);
	}
	
	storeResultsets(successFunction);
}

function updateChosenStudies(study,deleted) {
	if (deleted) {
		removeStudyRow(study);
	}
	loadKey();
	updateNumbers();
	updateMarkerReport();
}

function checkStudy(studyid,resultsetid) {
	var count = countCheckedInStudy(studyid);
	if (count + totalResultsets >= 8) {
		alert("You can only select 8 result sets");
		e('rs' + resultsetid).checked = false;
	}
}

function showTopMarkers() {
	document.getElementById('gv_resultsetids').value=getResultsets().join(",");
	document.getElementById('gv_studies').value=getStudies().join(",");
	document.gv_topmarkers.submit();
}


function validateNoMarkers() {
	var no = e('resultsets_with_sig').value;
	var noInt = parseInt(no);
	
	if (isNaN(noInt)) {
		alert("Please enter a number in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='1';
	}
	
	if (noInt<=0) {
		alert("Please enter a number greater than 0 in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='1';
	}
	
	if (noInt>8) {
		alert("Please enter a number less than 9 in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='8';
	}
	rSettingsChanged=1;	
}

function removeStudyRow(identifier) {
	jQuery("#study_" + identifier).hide('slow');
	var studyPanel = e("study_" + identifier);
	studyPanel.parentNode.removeChild(studyPanel);
}

function updateResultset(study, resultsetid) {
	e("image_" + resultsetid).src = "/images/small-ajax-loader.gif";
	
	var rsList = new Array();
	rsList.push(resultsetid);
	addResultsetsToServer(rsList, study, function(textStatus, data) {
		updateResultsetsOnClient(rsList[0], 'add');
		var tagElement = e("action_" + resultsetid)
		var tagImage = e("image_" + resultsetid)
		tagElement.innerHTML = "<span>Remove from Browser</span>";
		tagImage.src = "/images/remove.png";
		tagElement.href = "javascript:resultsetAction('" + study + "','remove')";
	});
}


function populateResultsetTotals() {
	var rs_totals = document.getElementsByName('rs_total_fields');
	//var rs_total0 = rs_totals[0];
	//if (rs_total0==null) return;
	//if (document.getElementById('rs_threshold' + rs_total0.id.substring(2)) == null) return

	for (var i = 0; i <= rs_totals.length; i++) {
		var rs_total = rs_totals[i];
		
		if (rs_total) {
			var rsid = rs_total.id.substring(2);
			var rs_access = document.getElementById('ra' + rsid);
			e('rst' + rsid).innerHTML =  addCommas(rs_total.value);
			if (e('rsa' + rsid)) {
				e('rsa' + rsid).innerHTML = rs_access.value;
			}
		}
	}
}

// check if settings have been changed and set different aspects to be
// updated
function storeAdvancedSettings() {

	var settingElements = document.getElementsByName('settings');

	// loop through each element and push the id and value into an array
	var settingList = new Array();
	for ( var i = 0; i < settingElements.length; i++) {
		var setting = settingElements[i].id + "|" + settingElements[i].value;
		settingList.push(setting);

	}
	// get tracks elements by retrieving all with a name of 'tracks'
	var trackElements = document.getElementsByName('tracks');
	var tracksList = new Array();

	// loop through each element and push the id into an array if it is
	// checked
	for ( var i = 0; i < trackElements.length; i++) {
		var track = trackElements[i];
		if (track.checked) {
			tracksList.push(track.id);
		}
	}

//	// store settings and tracks in the page
//	Vars.setSettings(settingList);
//	Vars.setTracks(tracksList);
	
	var store = new Store();
	
	store.storeSettings(settingList, tracksList);
}

function removeAllStudies() {
	var action = new Action([],[]);
	
	var store = new Store(action);
	
	successFunction = function(textStatus, data) {
		window.location.reload();
	}
	store.storeResultsets(successFunction);
}

//modified from http://calisza.wordpress.com/2008/11/03/javascript-jquery-bookmark-script/
function bookmark(element) {
	var id = element.id;
	// add a "rel" attrib if Opera 7+
	if(window.opera) {
		if (jQuery("#" + id).attr("rel") != ""){ // don't overwrite the rel attrib if already set
			jQuery("#" + id).attr("rel","sidebar");
		}
	}

	//event.preventDefault(); // prevent the anchor tag from sending the user off to the link
	var url = window.location.href;
	var title = top.document.title;

	if (window.sidebar) { // Mozilla Firefox Bookmark
		window.sidebar.addPanel(title, url,"");
	} else if( window.external ) { // IE Favorite
		window.external.AddFavorite( url, title);
	} else if(window.opera) { // Opera 7+
		return false; // do nothing - the rel="sidebar" should do the trick
	} else { // for Safari, Konq etc - browsers who do not support bookmarking scripts (that i could find anyway)
		 alert('Unfortunately, this browser does not support the requested action,'
		 + ' please bookmark this page manually.');
	}
}

function clearCategories() {
	jQuery("input[name=\"pid\"]").remove();
	document.filterform.submit()
}

function removeRFilter() {
	document.filterform.rfilter.value="";
	document.filterform.submit();
}

function submitFromSelect(element,revertTo, formTarget){
	var ff = element.form;
	var prevTarget = ff.target;
	if (formTarget) {
		ff.target = formTarget;
	}
//	//get format
//	var eleVal = element.value;
//	var url = ff.action.split("?");
//	var base = url[0];
//	
//	var fieldpairs = {};
//	
//	if (url[1]) fieldpairs = url[1].split("&");
//	
//	var fields = {};
//	var fieldkeys = new Array();
//	var hasElement = false;
//	
//	for (var f=0; f<fieldpairs.length;f++) {
//		var fieldtemp = fieldpairs[f].split("=");
//		fields[fieldtemp[0]] = fieldtemp[1];
//		fieldkeys.push(fieldtemp[0]);
//		
//		if (fieldtemp[0] == element.id) {
//			hasElement = true;
//		}
//	}
//	
//	
//	
//	if (!hasElement) fieldkeys.push(element.id);
//	
//	fields[element.id] = eleVal;
//	
//	var query = {};
//	for(var f=0; f<fieldkeys.length;f++) {
//		var key = fieldkeys[f];
//		query[key] = fields[key];
//	}
//	
//	for (var f=0; f<ff.elements.length;f++) {
//		var inputtemp = ff.elements[f];
//		if (!inputtemp.name) continue;
//		query[inputtemp.name] = inputtemp.value;
//		if (inputtemp.name == element.id) {
//			hasFormat = true;
//		}
//	}
//	
//	var qvalues = new Array();
//	jQuery.each(query, function (key,value) {
//		qvalues.push(key + "=" + value);
//	})
//
//	var qstring = "?" + qvalues.join("&");
//
	if (revertTo != null) {
		setTimeout("e('" + element.id + "').value='" + revertTo + "';",500);
	}
//	ff.target=formTarget;
	ff.submit();
	ff.target=prevTarget;
//	if (formTarget) {
//		window.open(base + qstring, formTarget);
//	}
//	else {
//		window.location = base + qstring;
//	}
	
}

function showPleaseWait(elementID,value) {
	e(elementID).value="";
	e(elementID).options[0].innerHTML="Loading..";
}

function getForm(formName) {
	var ff;
	
	if (formName) {
		ff = document.getElementsByName(formName)[0];
	}
	else {
		throw "formName not supplied to getForm()";
	}
	
	//get format
	var url = ff.action.split("?");
	var base = url[0];
	
	
	var fieldpairs = {};
	
	if (url[1]) fieldpairs = url[1].split("&");
	
	var fields = {};
	var fieldkeys = new Array();
	
	for (var f=0; f<fieldpairs.length;f++) {
		var fieldtemp = fieldpairs[f].split("=");
		fields[fieldtemp[0]] = fieldtemp[1];
		fieldkeys.push(fieldtemp[0]);
	}	
	
	var query = {};
	for(var f=0; f<fieldkeys.length;f++) {
		var key = fieldkeys[f];
		query[key] = fields[key];
	}
	
	for (var f=0; f<ff.elements.length;f++) {
		var inputtemp = ff.elements[f];
		if (!inputtemp.name) continue;
		
		if (inputtemp.type == 'checkbox') {
			if (inputtemp.checked) {
				query[inputtemp.name] = '1';
			}
			else {
				query[inputtemp.name] = '0';
			}
		}
		else {
			query[inputtemp.name] = inputtemp.value;
		}
	}
	
	var qvalues = new Array();
	jQuery.each(query, function (key,value) {
		qvalues.push(key + "=" + value);
	})
	return qvalues;
}

function exportData(formName){
	var ff;
	
	if (formName) {
		ff = document.getElementsByName(formName)[0];
	}
	else {
		ff = document.filterform;
	}
	
	//get format
	var format = ff.format.value;
	var url = ff.action.split("?");
	var base = url[0];
	
	
	var fieldpairs = {};
	
	if (url[1]) fieldpairs = url[1].split("&");
	
	var fields = {};
	var fieldkeys = new Array();
	var hasFormat = false;
	
	for (var f=0; f<fieldpairs.length;f++) {
		var fieldtemp = fieldpairs[f].split("=");
		fields[fieldtemp[0]] = fieldtemp[1];
		fieldkeys.push(fieldtemp[0]);
		
		if (fieldtemp[0] == 'format') {
			hasFormat = true;
		}
	}
	
	
	
	if (!hasFormat) fieldkeys.push('format');
	
	fields['format'] = format;
	
	var query = {};
	for(var f=0; f<fieldkeys.length;f++) {
		var key = fieldkeys[f];
		query[key] = fields[key];
	}
	
	for (var f=0; f<ff.elements.length;f++) {
		var inputtemp = ff.elements[f];
		if (!inputtemp.name) continue;
		query[inputtemp.name] = inputtemp.value;
		if (inputtemp.name == 'format') {
			hasFormat = true;
		}
	}
	
	var qvalues = new Array();
	jQuery.each(query, function (key,value) {
		qvalues.push(key + "=" + value);
	})

	var qstring = "?" + qvalues.join("&");
	
	ff.format.value="";
	ff.format.innerHTML="<img src='/images/small-ajax-loader.gif'/>";
	window.location = base + qstring;

}

// temporary function until prepereLookup() is IE friendly
function prepareTermLookup2() {
jQuery("#meshTerms").autocomplete("/phenotypes/meshconcept", {
		matchSubset: false,
		width: 460,
		selectFirst: false, 
		delay: 100,
		max: 15,
		
		formatItem: function(row, i, max) {
			return row[0] + "<span class=\"origin\">" + row[1] + "</div>";
		}
	});
}

function prepareLookup(elementID, urlpath, attrs) {
	if (!attrs) attrs = {};
	var luWidth = attrs['width'];
	var align = attrs['align'];
	var hideOrigin = attrs['hideOrigin'];
	if (!luWidth) luWidth="460px";
	if (!align) align = 'left';
	
	jQuery("#" + elementID).autocomplete(urlpath, {
			matchSubset: false,
			width: luWidth,
			selectFirst: false, 
			delay: 100,
			max: 20,
			formatItem: function(row, i, max) {
				var item = row[0];
				if (!hideOrigin) item = item + "<span class=\"origin\">" + row[1] + "</div>";
				return item;
			},
			align:align,
			submitForm: attrs['submitForm']
		});
}

function nextFeaturePanel() {
	var currFPanel = e('curr_feature').value;
	jQuery("#feature" + currFPanel).hide();
	
	currFPanel++;
	if (currFPanel>totalFPanels) currFPanel = 1;
	jQuery("#feature" + currFPanel).show();
	e('curr_feature').value=currFPanel;
}

function prevFeaturePanel() {	
	var currFPanel = e('curr_feature').value;
	jQuery("#feature" + currFPanel).hide();
	
	currFPanel--;
	if (currFPanel<1) currFPanel = totalFPanels;
	jQuery("#feature" + currFPanel).show();
	e('curr_feature').value=currFPanel;
}

function pauseFeaturePanel(element) {
	e('play').style.display="inline";
	element.style.display="none";
	clearTimeout(featTimerID);	
}

function togglePopulation(popLabel) {
	if (e('chk_' + popLabel).checked) {
		e(popLabel).value = "1";
	}
	else {
		e(popLabel).value = "0";
	}
}

function playFeaturePanel(element) {
	e('pause').style.display="inline";
	element.style.display="none";
	featTimerID = setInterval( "nextFeaturePanel()", 5000);
}

function afterUploadAction(progress){
	document.getElementById('progressid').value=window.progressid;
	document.detailform.submit();
}

function toobigUploadAction() {
	document.getElementById('progressid').value=window.progressid;
	detailform.submit();
}

function showNoEntry(){
	alert("We regret this data cannot be viewed while protections are put in place to ensure individuals cannot be identified.");
	return;
}

function gotoPage(pageNo) {
	var urlPath = window.location.pathname;
	var data = retrieveData();
	data['page']=pageNo;
	ajaxGetHTML(urlPath + "/table", data, "results", 0);
}

function retrieveData() {
	var filters = document.getElementById("joined_filter_params").value.split(";");
	var data = {};
	for(var f=0;f<filters.length;f++) {
		var currFilter = filters[f];
		log.info("currFilter:" + currFilter);
		var currFilterElement = document.getElementById(currFilter);
		if (!currFilterElement) continue; 
		if (currFilterElement.type == 'checkbox') {
			if (currFilterElement.checked) {
				data[currFilter] = 1;
			}
			else {
				data[currFilter] = 0;
			}
		}
		else {
			data[currFilter]=currFilterElement.value;
		}
	}
	data['nowrappers']=1;
	return data;
}

function search() {
	var urlPath = window.location.pathname;
	var data = retrieveData();
	ajaxGetHTML(urlPath + "/table", data, "results", 0);
}
