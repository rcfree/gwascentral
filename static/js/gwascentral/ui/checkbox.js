// $Id: CheckboxTag.js 1446 2010-05-26 14:24:28Z rcf8 $
/**
 * @description CheckboxTag UI module for dealing with logic of HTML 'CheckboxTags' (image associated links with labels). 
 * For example allowing them to be changed (e.g. add to remove)
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function Checkbox(identifier,prefix) {
	this.identifier = identifier;
	if (!prefix) {
		prefix = "action";
	}
	this.prefix = prefix;
	this.checkbox =  e(prefix + identifier);
	this.image = e(prefix + "_img" + identifier);
	this.label = e(prefix + "_label" + identifier);
	this.rsCount = e(prefix + "rs_no" + identifier);
	this.rsText = e(prefix + "rs_text" + identifier);
	this.loader = e(prefix + "loader" + identifier);
}

Checkbox.prototype.setCount = function(count) {
	if (this.rsCount) {
		this.rsCount.innerHTML = count;
	}
	else {
		return;
	}
		
	if (count==0) {
		this.setChecked(false);
	}
	else {
		this.setChecked(true);
	}
	
	var rsText = 'Data Set';
	if (count!=1) {
		rsText = rsText + 's';
	}
	if (this.rsText) {
		this.rsText.innerHTML = rsText;
	}
}

Checkbox.prototype.showAjax = function() {
	if (this.image) this.image.style.display="none";
	if (this.loader) this.loader.style.display="inline";
}

Checkbox.prototype.showCheckbox = function() {
	if (this.image) this.image.style.display="inline";
	if (this.loader) this.loader.style.display="none";
}

Checkbox.prototype.isChecked = function() {
	if (this.checkbox) {
		if (this.label.innerHTML=="Remove") {
			return true;
		}
	}
	return false;
}

Checkbox.prototype.setChecked = function(checked) {
	log.info("checkbox " + this.prefix + this.identifier + "?" + this.checkbox);
	if (this.checkbox) {
		
		if (checked) {
			log.info("checked");
			log.info("remove image:" + this.image);
			this.image.src="/images/options/remove.png";
			this.label.innerHTML="Remove";
		}
		else {
			log.info("not checked");
			log.info("add image:" + this.image);
			this.image.src="/images/options/add.png";
			this.label.innerHTML="Add";
		}
	}
}

