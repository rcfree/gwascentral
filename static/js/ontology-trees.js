function render_trees() { 
	
	jQuery("#xml_f").jstree({
		"core" : {
			"animation" : 200
		},
		"plugins" : [ "themes", "xml_data",  "ui", "mycheckbox", "cookies" ],
		
		"themes": {
			"theme" : "classic",
			"icons" : true
		},
		
		"xml_data" : {
			"ajax" : { 
				"url" :  function (n) {
					if(jQuery(n).attr("id") == "A") {
						url="/xml/mesh/meshtree_A.xml"
						return url;
					}
					else if(jQuery(n).attr("id") == "B") {
						url="/xml/mesh/meshtree_B.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "C") {
						url="/xml/mesh/meshtree_C.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "D") {
						url="/xml/mesh/meshtree_D.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "E") {
						url="/xml/mesh/meshtree_E.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "F") {
						url="/xml/mesh/meshtree_F.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "G") {
						url="/xml/mesh/meshtree_G.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "I") {
						url="/xml/mesh/meshtree_I.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "J") {
						url="/xml/mesh/meshtree_J.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "L") {
						url="/xml/mesh/meshtree_L.xml"
						return url; 
					}
					else if(jQuery(n).attr("id") == "N") {
						url="/xml/mesh/meshtree_N.xml"
						return url; 
					}
					else {
					url="/xml/mesh/meshtree_root.xml"
						
						return url; 
					}
				}
			},
			"xsl" : "flat"			
		},
		"cookies" : {
      		"auto_save" : true,
       		"save_opened" : "jstree_mesh_open",
       		"save_selected" : "jstree_mesh_selected"
       }
		
		
	});
	
	jQuery("#xml_hpo").jstree({
		"core" : {
			"animation" : 200
		},
		"plugins" : [ "themes", "xml_data",  "ui", "mycheckbox", "cookies"],
		
		"themes": {
			"theme" : "classic",
			"icons" : true
		},
		
		"xml_data" : {
			"ajax" : { 
				"url" :  function (n) {
					if(jQuery(n).attr("id") == "HP0000005") {
					 	url="/xml/hpo/hpotree_B.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000004") {
						url="/xml/hpo/hpotree_A.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001438") {
						url="/xml/hpo/hpotree_D.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000769") {
					 	url="/xml/hpo/hpotree_E.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000598") {
					 	url="/xml/hpo/hpotree_F.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000478") {
						url="/xml/hpo/hpotree_G.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000924") {
						url="/xml/hpo/hpotree_H.xml";
						return url; 
					}
					
					else if(jQuery(n).attr("id") == "HP0003549") {
					 	url="/xml/hpo/hpotree_I.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001626") {
					 	url="/xml/hpo/hpotree_J.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0000818") {
					 	url="/xml/hpo/hpotree_K.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0000119") {
					 	url="/xml/hpo/hpotree_L.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001507") {
					 	url="/xml/hpo/hpotree_M.xml";
						return url; 
					}
					else if(jQuery(n).attr("id") == "HP0000152") {
					 	url="/xml/hpo/hpotree_N.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001871") {
						url="/xml/hpo/hpotree_O.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0002715") {
						url="/xml/hpo/hpotree_P.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001574") {
						url="/xml/hpo/hpotree_Q.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0003012") {
					 	url="/xml/hpo/hpotree_R.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001939") {
						url="/xml/hpo/hpotree_S.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0000707") {
						url="/xml/hpo/hpotree_T.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0002664") {
						url="/xml/hpo/hpotree_U.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0001197") {
						url="/xml/hpo/hpotree_V.xml"; 
						return url;
					}
					else if(jQuery(n).attr("id") == "HP0002086") {
						url="/xml/hpo/hpotree_W.xml";
						return url; 
					}
					else {
						url="/xml/hpo/hpotree_root.xml";
						return url; 
					}
				}
			},
			"xsl" : "flat"		
		},
		"cookies" : {
    		"auto_save" : true,
  	     	"save_opened" : "jstree_hpo_open",
  	     	"save_selected" : "jstree_hpo_selected"
    	}
	});
	
	
	if(jQuery.browser.msie && jQuery.browser.version=="6.0") alert("I have detected you are using Internet Explorer 6. \n\nUnfortunately the JavaScript used to generate the ontology trees is not well supported for Internet Explorer 6, so you will experience glitches.  \n\nPlease see http://www.jstree.com for more details. ");
	
};

function resettree(treeid){
	jQuery(treeid).jstree("close_all",-1);
	jQuery(treeid).jstree("uncheck_all");
};


function gettree(){
	if(document.filterform.tree_type[1].checked == true){

		if(document.getElementById("meshtree").style.display=="none"){
			document.getElementById("buttons").style.display="none";
			document.getElementById("mesh_button").style.display="block";
		}
		else{
			document.getElementById("buttons").style.display="block";
			document.getElementById("mesh_button").style.display="none";
		}
		if (document.getElementById("hpotree_image")){
			document.getElementById("hpotree_image").id="meshtree_image";
		}
		toggle('meshtree');
	}
	
	else{
		if(document.getElementById("hpotree").style.display=="none"){
			document.getElementById("buttons").style.display="none";
			document.getElementById("hpo_button").style.display="block";
		}
		else{
			document.getElementById("buttons").style.display="block";
			document.getElementById("hpo_button").style.display="none";
		}
		if (document.getElementById("meshtree_image")){
			document.getElementById("meshtree_image").id="hpotree_image";
		}
		toggle('hpotree');
	}
};

function logselected(){
	var bxcheckedt=jQuery("#xml_f").jstree("get_checked");
	var pidv='D0001221';
	var bigarr=new Array();
	var pvalues=new Array();
	var pvaluesraw=new Array();

	function unique(arrayName){
    	var newArray=new Array();
        label:for(var i=0; i<arrayName.length;i++ ){  
        	for(var j=0; j<newArray.length;j++ ){
            	if(newArray[j]==arrayName[i]) 
                continue label;
             }
             newArray[newArray.length] = arrayName[i];
         }
         return newArray;
     }
	
	jQuery(bxcheckedt).each(
		function(){
			if ((jQuery(this).attr("incl"))=='1'){
				pidv = jQuery(this).attr("stringid");
				
				if (pidv.match(",") !=null){
					pvaluesraw=pidv.split(",");
					for (var x = 0; x < pvaluesraw.length; x++) {
						bigarr.push(pvaluesraw[x]);
					}
				}
				
				else{
					bigarr.push(pidv);
				}
			}	
			pvalues=unique(bigarr)
		}
	);
	

	
	var theForm = document.getElementById('meshidform');

	if (pvalues instanceof Array){
    	for (var i = 0, l = pvalues.length; i < l; i++){
			var el = document.createElement("input");
  			el.type = "hidden";
   			el.name = "pid";
   			el.value = pvalues[i];
   			theForm.appendChild(el);
  		}
	}
	theForm.submit();
};


function logselected_hpo(){
	var bxcheckedt=jQuery("#xml_hpo").jstree("get_checked");
	var pidv='D0001221';
	var bigarr=new Array();
	var pvalues=new Array();
	var pvaluesraw=new Array();

	function unique(arrayName){
    	var newArray=new Array();
        label:for(var i=0; i<arrayName.length;i++ ){  
        	for(var j=0; j<newArray.length;j++ ){
            	if(newArray[j]==arrayName[i]) 
                continue label;
             }
             newArray[newArray.length] = arrayName[i];
         }
         return newArray;
     }
	
	jQuery(bxcheckedt).each(
		function(){
			if ((jQuery(this).attr("incl"))=='1'){
				pidv = jQuery(this).attr("stringid");
				
				if (pidv.match(",") !=null){
					pvaluesraw=pidv.split(",");
					for (var x = 0; x < pvaluesraw.length; x++) {
						bigarr.push(pvaluesraw[x]);
					}
				}
				else{
					bigarr.push(pidv);
				}
			}	
			pvalues=unique(bigarr)
			
		}
	);
	
	var theForm = document.getElementById('hpoid');

	if (pvalues instanceof Array){
    	for (var i = 0, l = pvalues.length; i < l; i++){
			var el = document.createElement("input");
  			el.type = "hidden";
   			el.name = "pid";
   			el.value = pvalues[i];
   			theForm.appendChild(el);
  		}
	}
	var el2 = document.createElement("input");
	el2.type = "hidden";
	el2.name = "treeloadhpo";
	el2.value = "hpo";
	theForm.appendChild(el2);
	
	theForm.submit();
};