// $Id: action.js 1527 2010-09-16 13:48:38Z rcf8 $
/**
 * @description Action module for dealing with logic of adding/removing studies and resultsets
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

var resultsets;
var studies;
var phenotypes;
var entityResultsets;
var studyIdent;
var phenotypeIdent;

function Action(resultsets, studies, phenotypes) {
	this.resultsets = resultsets;
	this.studies = studies;
	this.phenotypes = phenotypes;
}

Action.prototype.addResultsets=function(resultsets) {
	this._addResultsets(resultsets);
	
	var rsCount = this.selectedEntityResultsets().length;
	if (rsCount > 0) {
		if (this.studyIdent) {
			this._addStudy(this.studyIdent);
		}
		
		if (this.phenotypeIdent) {
			this._addPhenotype(this.phenotypeIdent)
		}
	}
	this.rsAddedCount = rsCount;
}

Action.prototype.selectedEntityResultsets=function() {
	var selResultsets=[];
	
	var rsetHash = {};
	for(var i=0;i<this.resultsets.length;i++) {
		var rset = this.resultsets[i];
		rsetHash[rset]=1;
	}
	if (this.studyIdent || this.phenotypeIdent) {
		for(var i=0;i<this.entityResultsets.length;i++) {
			var rset = this.entityResultsets[i]
			if (rsetHash[rset]==1) {
				selResultsets.push(rset);
			}
		}
	}
	return selResultsets;
}

Action.prototype.isTooMany=function(resultsets) {
	//var rsWithoutEntity = Action.extractArrays(this.resultsets, this.entityResultsets);
	var finalList = Action.combineArrays(resultsets, this.resultsets);
	if (finalList.length>16) {
		return true;
	}
	return false;
}

Action.prototype.removeResultsets=function(resultsets) {
	this._removeResultsets(resultsets);
	
	//determine if all resultsets have been removed from the entity (study or phenotype)
	var numberPresent = this.selectedEntityResultsets().length;
	
	if (this.studyIdent) {
		if (numberPresent>0) {
			this._addStudy(this.studyIdent);
		}
		else {
			this._removeStudy(this.studyIdent);
		}
	}
	
	if (this.phenotypeIdent) {
		if (numberPresent>0) {
			this._addPhenotype(this.phenotypeIdent);
		}
		else {
			this._removePhenotype(this.phenotypeIdent);
		}
	}
	this.rsAddedCount = numberPresent;
}

Action.prototype.addEntityResultsets=function() {
	this.addResultsets(this.entityResultsets);
}


Action.prototype.removeEntityResultsets=function() {
	this.removeResultsets(this.entityResultsets);
}

Action.prototype._addResultsets=function(resultsets) {
	var finalList = Action.combineArrays(resultsets, this.resultsets);
	this.resultsets = finalList;
}

Action.prototype._removeResultsets=function(resultsets) {
	var finalList = Action.extractArrays(this.resultsets, resultsets);
	this.resultsets = finalList;
}

Action.prototype._addStudy = function(study) {
	var studiesList = this.studies;
	var studies = new Array(study);
	var finalList = Action.combineArrays(studies, studiesList);
	this.studies = finalList;
}

Action.prototype._addStudies = function(studies) {
	var studiesList = this.studies;
	var finalList = Action.combineArrays(studies, studiesList);
	this.studies = finalList;
}

Action.prototype._removeStudy = function(study) {
	var studiesList = this.studies;
	var studies = new Array(study);
	var finalList = Action.extractArrays(studiesList, studies);
	this.studies = finalList;
}

Action.prototype._addPhenotype = function(phenotype) {
	var phenotypesList = this.phenotypes;
	var phenotypes = new Array(phenotype);
	var finalList = Action.combineArrays(phenotypes, phenotypesList);
	this.phenotypes = finalList;
}

Action.prototype._removePhenotype = function(phenotype) {
	var phenotypesList = this.phenotypes;
	var phenotypes = new Array(phenotype);
	var finalList = Action.extractArrays(phenotypesList, phenotypes);
	this.phenotypes = finalList;
}

Action.combineArrays = function(array1, array2) {
	var map = {};
	var finalList = new Array();

	if (array1 && array1.length>0) {
		for(var i = 0;i<array1.length;i++) {
			map[array1[i]]=1;
		}
	}

	if (array2 && array2.length>0) {
		for(var i = 0;i<array2.length;i++) {
			map[array2[i]]=1;
		}
	}	
	
	var mapSize = Action.size(map)
	
	for(var item in map) {
		finalList.push(item);
	}
	
	return finalList;
}

Action.size = function (hashObject) {
	var size = 0;
	for(var item in hashObject) {
		size++;
	}
	return size;
}
 
Action.extractArrays = function(arrayBase, arrayToRemove) {
	var mapToRemove = new Array();
	var finalList = new Array();
	
	for(var rsNo = 0;rsNo<arrayToRemove.length;rsNo++) {
		mapToRemove[arrayToRemove[rsNo]]=1;
	}
	
	for(var rsNo = 0;rsNo<arrayBase.length;rsNo++) {
		var toRemove = arrayBase[rsNo];
		
		
		if (!mapToRemove[toRemove]) {
			finalList.push(arrayBase[rsNo]);
		}
	}

	return finalList;
}

Action.presentInArray = function(arrayBase, arrayPresent) {
	var mapToCheck = new Array();
	var finalList = new Array();
	
	if (arrayBase == null) {
		return finalList;
	}
	
	for(var rsNo = 0;rsNo<arrayPresent.length;rsNo++) {
		mapToCheck[arrayPresent[rsNo]]=1;
	}
	
	for(var rsNo = 0;rsNo<arrayBase.length;rsNo++) {
		var toCheck = arrayBase[rsNo];
		
		if (mapToCheck[toCheck]) {
			finalList.push(arrayBase[rsNo]);
		}
	}

	return finalList;
}

Action.calcLabel = function(array, singular, plural) {
	var count = array.length;
	if (count==null) {
		count = array;
	}
	if (count==0) return { number: 'No', label:plural};
	if (count==1) return { number: count, label: singular};
	return {number:count, label: plural};
}

