// $Id: variables.js 1527 2010-09-16 13:48:38Z rcf8 $
/**
 * @description Variables UI module for retrieving data from form fields (hidden and otherwise)
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function Vars() {}
Vars.getResultsets = function() {
	var resultsetArray = new Array();
	resultsetArray = e('resultsets').value.split(",");
	if (e('resultsets').value=='null' || e('resultsets').value=='none') return new Array();
	if (e('resultsets').value==0) return new Array();
	return resultsetArray;
}

Vars.getStudies = function() {
	var studyArray = new Array();
	studyArray = e('studies').value.split(",");
	if (e('studies').value=='null' || e('studies').value=='none') return new Array();
	if (e('studies').value==0) return new Array();
	return studyArray;
}

Vars.getPhenotypes = function() {
	var studyArray = new Array();
	phenotypeArray = e('phenotypes').value.split(",");
	if (e('phenotypes').value=='null' || e('phenotypes').value=='none') return new Array();
	if (e('phenotypes').value==0) return new Array();
	return phenotypeArray;
}

Vars.setPhenotypes = function(phenotypes) {
	if (isArray(phenotypes)) {
		e('phenotypes').value = phenotypes.join(',');
	}
	else {
		if (phenotypes == 'none') {
			e('phenotypes').value = "";
		} else {
			e('phenotypes').value = phenotypes;
		}
	}
}

Vars.getChromosomes = function() {
	var chrs = document.getElementsByName('chr');
	var chrList=new Array();
	for(var i=0;i<chrs.length;i++) {
		if (chrs[i].checked) chrList.push(chrs[i].value);
	}
	if (chrList.length == 24) return 'ALL';
	else if (chrList.length == 0) return 'NONE';
	else return chrList;
}

Vars.getChrNumber = function() {
	var chrs = document.getElementsByName('chr');
	var chrList=new Array();
	for(var i=0;i<chrs.length;i++) {
		if (chrs[i].checked) chrList.push(chrs[i].id.substr(3));
	}
	return chrList.length;
}


/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
Vars.getAllResultsetsInEntity = function(identifier) {
	var resultsets = e("rs_" + identifier).value;
	return resultsets.split(',');
}


Vars.getSelectedTranscript = function() {
	var sts = document.getElementsByName('transcripts');
	for (var i=0; i < sts.length; i++)
   {
   if (sts[i].checked) return sts[i].value;
   }
}

/**
 * @description Gets genome view size from page
 * @returns {int} Genome view size
 */
Vars.getGenomeViewSize = function() {
	if (e('genomeview_size')) {
		return parseInt(e('genomeview_size').value);
	}
	else {
		return null;
	}
	
}

Vars.getResultsetsInStudy = function(resultsetIdent) {
	var resultsets = e("studies" + resultsetIdent);
	if (resultsets) {
		return resultsets.value.split(",")
	} else {
		return null;
	};
}

/**
 * @description Sets genome view size on page
 * @param {int} size Genome view size
 */
Vars.setGenomeViewSize = function(size) {
	e('genomeview_size').value = parseInt(size);
}

/**
 * @description Gets small (ie. genome view) chromosome length
 * @returns {int} Chromosome length in pixels
 */
Vars.getSmallChrLength = function() {
	return Vars.getGenomeViewSize() * 350;
}

/**
 * @description Gets small (ie. genome view) plot height
 * @returns {int} Plot height in pixels
 */
Vars.getSmallPlotHeight = function() {
	return Vars.getGenomeViewSize() * 20;
}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
Vars.getCheckedResultsets = function(study) {
	var resultsetElements = document.getElementsByName('rs_check' + study);
	var rsList = new Array();
	for ( var i = 0; i < resultsetElements.length; i++) {
		var rs = resultsetElements[i];
		if (rs.checked) {
			rsList.push(rs.id.substring(2));
		}
	}
	return rsList;
}




Vars.getUncheckedResultsets = function(study) {
	var resultsetElements = document.getElementsByName('rs_check' + study);
	var rsList = new Array();
	for ( var i = 0; i < resultsetElements.length; i++) {
		var rs = resultsetElements[i];
		if (!rs.checked) {
			rsList.push(rs.id.substring(2));
		}
	}
	return rsList;
}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
Vars.getChosenStudyResultsets = function(study) {
	var resultsetElements = document.getElementsByName('rs_check' + study);
	var rsList = new Array();
	for ( var i = 0; i < resultsetElements.length; i++) {
		var rs = resultsetElements[i];
		rsList.push(rs.id.substring(2));
	}
	return rsList;
}


Vars.getTagPrefix = function() {
	if (e('assoc_exp_panels').style.display=='none') {
		return 'g';
	}
	else {
		return 'p';
	}
}


Vars.setResultsets = function(resultsets) {
	if (isArray(resultsets)) {
		e('resultsets').value = resultsets.join(',');
	}
	else {
		if (resultsets == 'none') {
			e('resultsets').value = "";
		} else {
			e('resultsets').value = resultsets;
		}
	}
}

Vars.setStudies = function(studies) {
	if (studies == 'none') {
		e('studies').value = "";
	} else {
		e('studies').value = studies;
	}
}


Vars.setSettings = function(settingsList) {
	var settings = settingsList.join(",");
	e('settings').value = settings;
}

Vars.setTracks = function(tracksList) {
	var tracks = tracksList.join(",");
	e('tracks').value = tracks;
}

Vars.getSettings = function() {
	// get settings elements by retrieving all with a name of 'settings'
	var settingElements = document.getElementsByName('settings');

	// loop through each element and push the id and value into an array
	var settingList = new Array();
	for ( var i = 0; i < settingElements.length; i++) {
		var setting = settingElements[i].id + "|" + settingElements[i].value;
		settingList.push(setting);
	}

	return settingList.join(",");
}

Vars.getTracks = function() {
	// get tracks elements by retrieving all with a name of 'tracks'
	var trackElements = document.getElementsByName('tracks');
	var tracksList = new Array();

	// loop through each element and push the id into an array if it is checked
	for ( var i = 0; i < trackElements.length; i++) {
		var track = trackElements[i];
		if (track.checked) {
			tracksList.push(track.id);
		}
	}
	return tracksList.join(",");
}


Vars.getCategories = function() {
	var diseases = new Array();

	var diseaseChecks = document.getElementsByName('category');
	for(var d=0;d<diseaseChecks.length;d++) {
		var disease = diseaseChecks[d];
		if (disease.checked) diseases.push(disease.value);
	}
	return diseases;
}


Vars.getThreshold = function() {
	var threshold = e('t');
	if (threshold) {
		return e('t').value;
	}
	else {
		var topt=top.document.getElementById('t');
		if (topt) {
			return topt.value;
		}
		else {
			return null;
		}
	}
}

Vars.getKeywords = function() {
	return document.getElementById('keywords').value;
}


Vars.setRegion = function(region) {
	top.document.getElementById('region').value=region;
	storeRegion(region,function() {	
	});	
}

Vars.isMaxAdded = function() {
	if (Vars.getResultsets().length >= maxResultsets) {
		alert("You can only compare a maximum of " + maxResultsets + " Data Sets");
		return true;
	}
	return false;
}

Vars.hasPhenotypes = function() {
	if (!e('phenotype_count')) return false;
	return true;
}

Vars.hasStudies = function() {
	if (!e('study_count')) return false;
	return true;
}

Vars.hasResultsets = function() {
	if (!e('resultset_count')) return false;
	return true;
}

Vars.populateFromAddRemove = function(addremove){
	var action = addremove.action;
	var isResultset = addremove.isResultset;
	var stuPresent = Vars.hasStudies();
	var phenoPresent = Vars.hasPhenotypes();
	var rsPresent = Vars.hasResultsets();
	
	Vars.setStudies(action.studies);
	Vars.setResultsets(action.resultsets);
	Vars.setPhenotypes(action.phenotypes);
	
	if (rsPresent) {
		var rinfo = Action.calcLabel(action.resultsets, 'Data Set', 'Data Sets');
		e('resultset_count').innerHTML = rinfo.number;
		e('resultset_label').innerHTML = rinfo.label;
	}
	
	if (phenoPresent) {
		var pinfo = Action.calcLabel(action.phenotypes, 'Phenotype', 'Phenotypes');
		e('phenotype_count').innerHTML = pinfo.number;
		e('phenotype_label').innerHTML = pinfo.label;
	}
	
	if (stuPresent) {
		var sinfo = Action.calcLabel(action.studies, 'Study', 'Studies');
		e('study_count').innerHTML = sinfo.number;
		e('study_label').innerHTML = sinfo.label;
	}
	
	var entityIdent;
	if (action.studyIdent) entityIdent = action.studyIdent;
	if (action.phenotypeIdent) entityIdent = action.phenotypeIdent;
	
	var rsets = action.resultsets;
	var entityRsetsCheckboxes = document.getElementsByName('entity' + entityIdent);
	
	var entityCbox = addremove.entityCheckbox;
	var selRsets = action.selectedEntityResultsets();
	
	if (selRsets.length > 0) {
		if (!isResultset) {
			for (var i = 0; i < rsets.length; i++) {
				var rsetCheckbox = e('action' + rsets[i]);
				if (rsetCheckbox) 
					rsetCheckbox.checked = true;
			}
		}
		if (entityCbox) {
			entityCbox.setChecked(true);
			entityCbox.setCount(selRsets.length)
		}
	}
	else {
		if (!isResultset) {
			for (var i = 0; i < entityRsetsCheckboxes.length; i++) {
				entityRsetsCheckboxes[i].checked = false;
			}
		}
		if (entityCbox) {
			entityCbox.setChecked(false);
			entityCbox.setCount(0)
		}
	}
	addremove.checkbox.setCount(action.rsAddedCount);
}

Vars.countCheckedInStudy = function(studyid) {
	var selectedRs = document.getElementsByName('rs_check' + studyid);
	var count = 0;
	for ( var i = 0; i < selectedRs.length; i++) {
		if (selectedRs[i].checked) {
			count++;
		}
	}
	return count;
}

Vars.isNotSession = function() {
	var is = e('is_not_session').value;
	
	if(is==1) {
		return true;
	}
	return false;
}

Vars.getSessionID = function() {
	return e('sessionid').value;
}

Vars.getHidePopup = function() {
	var hidePopup = top.document.getElementById('hide_popup').value;
	if (hidePopup=='1') return true;
	return false;
}

Vars.getGBrowseRegion = function() {
	var gbrowse_frm = top.window.frames[0];
	if (!gbrowse_frm) return undef;
	var gbrowse_doc = gbrowse_frm.document;
	
	var ref = gbrowse_doc.getElementsByName('ref')[0];
	var start = gbrowse_doc.getElementsByName('start')[0];
	var stop = gbrowse_doc.getElementsByName('stop')[0];
	
	if (ref==null) {
		return null;
	}
	else {
		var region = ref.value + ":" + start.value + ".." + stop.value;
		return region;
	}
}

Vars.getGBrowseLandmark = function() {
	var gbrowse_frm = top.window.frames[0];
	if (!gbrowse_frm) return undef;
	var gbrowse_doc = gbrowse_frm.document;
	
	var ref = gbrowse_doc.getElementsByName('name')[0];
	
	if (ref==null) {
		return null;
	}
	else {
		return ref.value;
	}
}

Vars.getResultsetStudy = function(identifier) {
	return e('study_' + identifier).value;
}