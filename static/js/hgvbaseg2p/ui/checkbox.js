// $Id: CheckboxTag.js 1446 2010-05-26 14:24:28Z rcf8 $
/**
 * @description CheckboxTag UI module for dealing with logic of HTML 'CheckboxTags' (image associated links with labels). 
 * For example allowing them to be changed (e.g. add to remove)
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function Checkbox(identifier) {
	this.identifier = identifier;
	this.checkbox =  e("action" + identifier);
	this.rsCount = e("rs_no" + identifier);
	this.rsText = e("rs_text" + identifier);
	this.loader = e("loader" + identifier);
}

Checkbox.prototype.setCount = function(count) {
	if (this.rsCount) {
		this.rsCount.innerHTML = count;
	}
	else {
		return;
	}
		
	if (count==0) {
		this.setChecked(false);
	}
	else {
		this.setChecked(true);
	}
	
	var rsText = 'Data Set';
	if (count!=1) {
		rsText = rsText + 's';
	}
	if (this.rsText) {
		this.rsText.innerHTML = rsText;
	}
}

Checkbox.prototype.showAjax = function() {
	this.checkbox.style.display="none";
	this.loader.style.display="inline";
}

Checkbox.prototype.showCheckbox = function() {
	this.checkbox.style.display="inline";
	this.loader.style.display="none";
}

Checkbox.prototype.isChecked = function() {
	if (this.checkbox) return this.checkbox.checked;
}

Checkbox.prototype.setChecked = function(checked) {
	if (this.checkbox) {
		this.checkbox.checked = checked;
	}
}

