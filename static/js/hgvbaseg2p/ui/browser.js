// $Id: browser.js 1540 2010-09-22 08:35:36Z rcf8 $
/**
 * @description Browser UI specific methods for adding/removing studies and going to browser pages etc.
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function goToBrowser(type, query, resultsetList) {
	if (!resultsetList) resultsetList = Vars.getResultsets().join(",");
	var threshold = Vars.getThreshold();
	if (!threshold) threshold="";
	if (type == 'genome') {
		goToPage("browser/genome?r[]=" + resultsetList + "&t=" + threshold); 
	}
	else {
		top.window.location.href = "/browser/region?q=" + query + "?r[]=" + resultsetList + "&t=" + threshold;
	}
}


function browseEntity(type, region, identifier){
	var resultsets = Vars.getResultsets();
	var studies = Vars.getStudies();
	
	var action = new Action(resultsets, studies, null);
	action.entityResultsets = Vars.getAllResultsetsInEntity(identifier);
	
	var nonEntityRs = Action.extractArrays(resultsets, action.entityResultsets);
	var entityRsAdded = resultsets.length - nonEntityRs.length;

	var useDialog = true;
	
	var content = "Do you want to view this Study only ";
	if (resultsets.length > 0) {
		content = content + "(removing the previously added " + pluralize(resultsets.length, "Data Set") + "), or in the context of the previously added " + pluralize(studies.length, "Study", "Studies");
		
		if (entityRsAdded == 0) {
			useDialog = true;
		}
		else {
			useDialog = false;
		}
	}
	else {
		useDialog = false;
	}
	
	
	if (useDialog) {
		
		
		var buttons = {
			'View this Study only': function(){
				goToBrowser(type, region, action.entityResultsets);
			},
			'View in context of other Studies': function(){
				action = attemptToAddResultsets(action,entityRsAdded);
				if (action.abortBrowser) return;
				goToBrowser(type, region, action.resultsets);
			}
		};
		var params = {};
		showDialog(true, buttons, content, params, "View Study", 700, 200, "studyadd")
	}
	else {
		action = attemptToAddResultsets(action, entityRsAdded);
		if (action.abortBrowser) return;
		goToBrowser(type, region, action.resultsets);
	}

}

function attemptToAddResultsets(action, entityRsAdded){
	var currentRs = action.resultsets.length;
	if (currentRs == 16) {
		alert("You have already added the maximum 16 Data Sets to the Browser.");
		action.abortBrowser=true;
		return action;
	}
	for (var r = 0; r < action.entityResultsets.length; r++) {
		var rsIdent = action.entityResultsets[r];
		if (action.isTooMany(new Array(rsIdent))) {
			if (action.entityResultsets.length>16) {
				var remainingRs = 16 - currentRs;
				if (currentRs == 0) {
					alert("You can add 16 Data Sets to the Browser. This Study has " + action.entityResultsets.length + ", so only 16 Data Sets will be added.");
				}
				else {
					if (entityRsAdded > 0) {
						alert("You can add 16 Data Sets to the Browser. This Study has " + action.entityResultsets.length + " Data Sets (from which you have added " + currentRs + "). Therefore, only " + remainingRs + " Data Sets will be added.");
					}
					else {
						alert("You can add 16 Data Sets to the Browser. You have already added " + currentRs + ". This Study has " + action.entityResultsets.length + " Data Sets, so only " + remainingRs + " will be added.");
					}
				}
			}
			else {
				alert("You can add 16 Data Sets to the Browser. You have already added " + currentRs + ", so only " + remainingRs + " Data Sets from this Study will be added.");
			}
			return action;
		}
		action.addResultsets(new Array(rsIdent));
	}
	return action;
}

function checkAllResultsets(study) {
	var resultsets = Vars.getResultsets();
	var studies = Vars.getStudies();
	
	var action = new Action(resultsets, studies, null);
	action.entityResultsets = Vars.getAllResultsetsInEntity(study);
	
	var nonEntityRs = Action.extractArrays(resultsets, action.entityResultsets);
	var entityRsAdded = resultsets.length - nonEntityRs.length;
	
	var resultsetElements = document.getElementsByName('rs_check' + study);
	var remainingRs = 16 - resultsets.length;
	
	for ( var i = 0; i < resultsetElements.length; i++) {
		var rs = resultsetElements[i];
		var rsIdent = rs.id.substring(2);
		
		if (action.isTooMany(new Array(rsIdent))) {
			if (resultsets.length > 0) {
				alert("You can add 16 Data Sets to the Browser. You have already added " + resultsets.length + ", so only" + remainingRs + " Data Sets from this Study have been selected.");
				return;
			}
			else {
				alert("You can add 16 Data Sets to the Browser. This Study has " + action.entityResultsets.length + ", so 16 from this Study have been selected.");
				return;
			}
		}
		action.addResultsets(new Array(rsIdent));
		rs.checked=true;
	}
}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
function uncheckAllResultsets(study) {
	var resultsetElements = document.getElementsByName('rs_check' + study);
	for ( var i = 0; i < resultsetElements.length; i++) {
		var rs = resultsetElements[i];
		rs.checked=false;
	}
}


function studyGVToRV(studyIdent, chrLocation) {
	var region = calcRegionFromDecimal(chrLocation);
	browseEntity("region", region, studyIdent);
}
/**
 * @description Checks all chromosome checkboxes on a page (with name = 'chr')
 *              and sets gSettingsChanged
 */
function selectAllChromosomes() {
	var chrs = document.getElementsByName('chr');
	for(var i=0;i<chrs.length;i++) {
		chrs[i].checked=true;
	}
	gSettingsChanged=1;
}

/**
 * @description Unchecks all chromosome checkboxes on a page (with name = 'chr')
 *              and sets gSettingsChanged
 */
function selectNoneChromosomes() {
	var chrs = document.getElementsByName('chr');
	for(var i=0;i<chrs.length;i++) {
		chrs[i].checked=false;
	}
	gSettingsChanged=1;
}



/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String}
 *            tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
function showSigs(markerResultset) {
	var temp = markerResultset.split(";");
	var markerURL = "/marker/" + temp[0] + "/results?v=table&r=" + temp[1] & "&t=" + temp[2];
	window.top.location=markerURL;
}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
function resizeRegionView() {
	top.document.getElementById('regionview_loader').style.display = "none";
	top.document.getElementById('regionview_display').style.height="0px";
	var gbrowseHeight = jQuery(document).height();
	var gbrowseWidth = jQuery(document).width();
	
	var rv = top.document.getElementById('regionview');
	rv.style.height = (gbrowseHeight + 100) + "px";
	top.document.getElementById('regionview_display').style.height=(gbrowseHeight + 100) + "px";
}

function updateGenomeView(resultsets) {
	var chr = Vars.getChromosomes();
	var numberChrs = Vars.getChrNumber();
	var height = Vars.getSmallChrLength();
	if (noRows > numberChrs) {
		noRows = numberChrs;
	}
	
	var chrlist;
	var noRows = calculateRows();
	if (chr == 'ALL') {
		chrlist = "ALL";
	}
	else if (chr == 'NONE') {
		e('genomeview_display').innerHTML="<ul style='margin-left:none'><li>No chromosomes selected</li></ul>";
		populateResultsetTotals();
		return;
	}
	
	else {
		chrlist = chr.join(" ");
	}
	
	
	var pars = {
			band_labels: 0,
			c: chrlist,
			h:height,
			w: parseInt(height/30),
			e: 1,
			rows: noRows,
			fp: "GenomeView",
	};
	var filters = getForm("filterform");

	for(var i=0;i<=filters.length;i++) {
		if (filters[i]) {
			var temp = filters[i].split("=");
			var key = temp[0];
			var val = temp[1];
			if (!pars[key])
				pars[key]=val;
		}
	}

		ajaxGetHTML(genomeViewURL, pars,'genomeview_display', 0,
				function(data, textStatus){
					e('genomeview_display').innerHTML = data;
					populateResultsetTotals();
				},
				function () {
					e('genomeview_display').innerHTML = "Sorry, an error occurred while trying to retrieve this information. Please try again later.";
					populateResultsetTotals();
				}
		);
}

function updateRegionView(url) {
	var finalUrl = url  + ";width=" + (getScreenWidth() - 350);
	e('regionview').src = finalUrl;
}

function gvToRV(chrLocation) {
	var threshold = Vars.getThreshold();
	
	var region = calcRegionFromDecimal(chrLocation);
	var url = "/browser/region?q=" + region;
	//if (Vars.isNotSession()) {
		url = url + "&r[]=" + Vars.getResultsets().join(',');
	//}
	url = url + "&t=" + threshold;
	url = url + "&sc=" + e('sc').value;
	setTimeout(function()
	{
	    window.location = url;
	}, 0);
}

function removeStudyFromBrowser(study) {
	
	var rs = Vars.getChosenStudyResultsets(study);
	_resultsets = removeResultsets(rs);
	_studies = removeStudy(study);
		
	successFunction = function(data, textStatus) {
		setResultsets(_resultsets);
		setStudies(_studies);
		setBrowserToUpdate();
		updateChosenStudies(study,true);
	}
	
	storeResultsets(successFunction);
}

function updateChosenStudies(study,deleted) {
	if (deleted) {
		removeStudyRow(study);
	}
	loadKey();
	updateNumbers();
	updateMarkerReport();
}

function submitSearch(submitType,landmark) {	
	if (!landmark) {
		if (submitType) {
			var start = parseInt(document.mainform.start.value);
			var stop = parseInt(document.mainform.stop.value);
			var ref = document.mainform.ref.value;
			var typeSplit = submitType.split(" ");
			
			var action = typeSplit[0];
			var value = typeSplit[1];
			
			if (typeSplit[0]=='left') {
				start = start - parseInt(value);
				stop = stop - parseInt(value)
			}
			else if (typeSplit[0]=='right') {
				start = start + parseInt(value);
				stop = stop + parseInt(value)
			}
			else if (typeSplit[0]=='zoombar') {
				var range = document.mainform.span.value;
				var midpoint = (start/2) + (stop/2);
				var temp = range/2;
				start = midpoint - temp;
				stop = midpoint + temp;
			}
			else {
				var size = stop-start;
				value = parseInt(value)/100;
				var removeSize = parseInt((size * value)/2);
				
				if (typeSplit[0] == 'in') {
					start = start + removeSize;
					stop = stop - removeSize;
				}
				if (typeSplit[0]=='out') {

					start = start - removeSize;
					stop = stop + removeSize;
				}
			}
			if (start<1) start = 1;
			landmark = ref + ":" + parseInt(start) + ".." + parseInt(stop);
			document.getElementsByName('ref')[0].value=ref;
			document.getElementsByName('start')[0].value=parseInt(start);
			document.getElementsByName('stop')[0].value=parseInt(stop);
		}
		else {
			landmark = Vars.getGBrowseLandmark();
		}
	}
	var url = hgvbaseURL + "/browser/region?q=" + landmark + "&r[]=" + Vars.getResultsets().join(',') + "&t=" + Vars.getThreshold();
	window.top.location= url;
}

function showTopMarkers() {
	document.getElementById('gv_resultsetids').value=getResultsets().join(",");
	document.getElementById('gv_studies').value=getStudies().join(",");
	document.gv_topmarkers.submit();
}


function validateNoMarkers() {
	var no = e('resultsets_with_sig').value;
	var noInt = parseInt(no);
	
	if (isNaN(noInt)) {
		alert("Please enter a number in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='1';
	}
	
	if (noInt<=0) {
		alert("Please enter a number greater than 0 in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='1';
	}
	
	if (noInt>8) {
		alert("Please enter a number less than 9 in the 'Show significant markers across studies present in at least' field");
		e('resultsets_with_sig').value='8';
	}
	rSettingsChanged=1;	
}

function removeStudyRow(identifier) {
	jQuery("#study_" + identifier).hide('slow');
	var studyPanel = e("study_" + identifier);
	studyPanel.parentNode.removeChild(studyPanel);
}

function updateResultset(study, resultsetid) {
	e("image_" + resultsetid).src = hgvbaseURL + "/images/small-ajax-loader.gif";
	
	var rsList = new Array();
	rsList.push(resultsetid);
	addResultsetsToServer(rsList, study, function(textStatus, data) {
		updateResultsetsOnClient(rsList[0], 'add');
		var tagElement = e("action_" + resultsetid)
		var tagImage = e("image_" + resultsetid)
		tagElement.innerHTML = "<span>Remove from Browser</span>";
		tagImage.src = hgvbaseURL + "/images/remove.png";
		tagElement.href = "javascript:resultsetAction('" + study + "','remove')";
	});
}

function removeAllAddedItems() {
	var action = new Action([],[],[]);
	
	var store = new Store(action);
	
	successFunction = function(textStatus, data) {
		window.location.reload();
	}
	store.storeResultsets(successFunction);
}

function addAllItems() {
	var topResultsets = e('top_resultsets').value.split(",");
	var action = new Action(Vars.getResultsets(), Vars.getStudies(), Vars.getPhenotypes());
	
	var isMoreThanMax=e('more_than_max').value;
	var maxPresent = action.resultsets.length;
	
	if (maxPresent == maxResultsets) {
		alert("You cannot add more than the maximum number of Data Sets (" + maxResultsets + "). \nNone were added.");
		return;
	}	
	
	var addedCount = 0;
	for (var rs = 0; rs < topResultsets.length; rs++) {
		action.addResultsets(new Array(topResultsets[rs]));
		addedCount++;
		if (action.resultsets.length >= maxResultsets) {
			break;
		}
		
	}

	var store = new Store(action);
	successFunction = function() {
		window.location.href = window.location.href;
		if (isMoreThanMax=='1') {
			alert("There are more than the maximum number of Data Sets (" + maxResultsets + "). \nOnly " + addedCount + " have been added.");
		}
	}
	
	store.storeResultsets(successFunction);
	
}

function removeCheckedResultsets() {
	var resultsets = document.getElementsByName('resultset_check');
	var checkedResultsets = new Array();
	var checkedUploads = new Array();
	for(var i=0;i<resultsets.length;i++) {
		if (resultsets[i].checked) {
			if (resultsets[i].value.substring(0, 1) == "U") {
				checkedUploads.push(resultsets[i].value);
			}
			else {
				checkedResultsets.push(resultsets[i].value)
			}
		} 
	}
	var removeList = Action.extractArrays(Vars.getResultsets(),checkedResultsets);
	
	if (checkedResultsets.length + checkedUploads.length  > 0) {
		var doRemove = confirm("Are you sure you want to remove the selected Data Sets from the Browser?\n(Any uploaded data sets will be cleared from HGVbaseG2P)");
		if (doRemove) {
			if (removeList.length == 0) {
				document.filterform.r.value = "none";
			}
			else {
				document.filterform.r.value = removeList.join(",");
			}
			document.filterform.remove_uploads.value = checkedUploads.join(",");
		}
		else {
			return;
		}
	}
	else {
		alert("No Data Sets selected to remove!");
	}
	document.filterform.submit();
}
