function displayMart(dataset, filters, attributePage, attributes, noResults) {
		var baseURL = martURL + "?";
		var url = baseURL + 'VIRTUALSCHEMANAME=default';
		var attr_list = new Array();
		var filt_list = new Array();

		if (filters != null) {
			var temp_filts = filters.split(",");
			for ( var filt = 0; filt <= temp_filts.length - 1; filt++) {
				var filt_parts = temp_filts[filt].split('=');
				filt_list.push(dataset + ".default.filters." + filt_parts[0]
						+ ".%22" + filt_parts[1] + "%22");
			}

			url = url + "&FILTERS=" + filt_list.join('|');
		}

		if (attributes != null) {
			var temp_attrs = attributes.split(',');

			for ( var attr = 0; attr <= temp_attrs.length - 1; attr++) {
				attr_list.push(dataset + ".default." + attributePage + "."
						+ temp_attrs[attr]);
			}

			url = url + "&ATTRIBUTES=" + attr_list.join('|');

		}

		if (noResults) {
			url = url + "&STARTPAGE=" + dataset;
		}

		window.open(url, "_self");
	}


function goToMart(dataset) {
	if (dataset == 'marker') {
		displayMart(dataset, null, dataset, dataset + 'id', 1);
	}
	if (dataset == 'study') {
		displayMart(dataset, null, dataset, dataset + 'id', 1);
	}
}

function martAlleleFrequencies(experimentID, studyID) {
	var filters = "af_experimentid=" + experimentID + ",studyid=" + studyID;
	displayMart(
			'study',
			filters,
			'allele_frequency',
			"studyid,markerid,alleleset,chr,start,stop,title,localid,source,variationtype,upstream30bp,downstream30bp,alleleseq,af_numberofgenotypedsamples,af_frequencyasproportion",
			0);
}

function martGenotypeFrequencies(experimentID, studyID) {
	var filters = "gf_experimentid=" + experimentID + ",studyid=" + studyID;
	displayMart(
			'study',
			filters,
			'genotype_frequency',
			"studyid,gf_markerid,gf_chr,gf_start,gf_stop,title,gf_localid,gf_source,gf_variationtype,gf_upstream30bp,gf_downstream30bp,gf_genotypelabel,gf_numberofgenotypedsamples,gf_frequencyasproportion",
			0);
}

function martAlleleAssociations(experimentID, studyID) {
	var filters = "aa_experimentid=" + experimentID + ",studyid=" + studyID;
	// this has frequency fields ->
	// displayMart('study',filters,'genotype_association',
	// "studyid,ga_markerid,ga_alleleset,ga_chr,ga_start,ga_stop,ga_numberofgenotypedsamples,ga_frequencyasproportion,ga_numberofsampleswithgenotype,ga_genotypelabel,ga_localid_assayedpanel,ga_phenotype,ga_unadjustedpvalue,ga_analysismethod,ga_localid,ga_source,ga_variationtype,ga_upstream30bp,ga_downstream30bp,title_experiment,totalmarkerstested",0);
	displayMart(
			'study',
			filters,
			'allele_association',
			"aa_studyid,aa_markerid,aa_alleleset,aa_chr,aa_start,aa_stop,aa_numberofgenotypedsamples,aa_alleleseq,aa_localid_assayedpanel,aa_phenotype,aa_unadjustedpvalue,aa_analysismethod,aa_localid,aa_source,aa_variationtype,aa_upstream30bp,aa_downstream30bp,aa_title_experiment,aa_totalmarkerstested",
			0);
}

function martGenotypeAssociations(experimentID, studyID) {
	var filters = "ga_experimentid=" + experimentID + ",studyid=" + studyID;
	// this has frequency fields ->
	// displayMart('study',filters,'genotype_association',
	// "studyid,ga_markerid,ga_alleleset,ga_chr,ga_start,ga_stop,ga_numberofgenotypedsamples,ga_frequencyasproportion,ga_numberofsampleswithgenotype,ga_genotypelabel,ga_localid_assayedpanel,ga_phenotype,ga_unadjustedpvalue,ga_analysismethod,ga_localid,ga_source,ga_variationtype,ga_upstream30bp,ga_downstream30bp,title_experiment,totalmarkerstested",0);
	displayMart(
			'study',
			filters,
			'genotype_association',
			"ga_studyid,ga_markerid,ga_alleleset,ga_chr,ga_start,ga_stop,ga_numberofgenotypedsamples,ga_genotypelabel,ga_localid_assayedpanel,ga_phenotype,ga_unadjustedpvalue,ga_analysismethod,ga_localid,ga_source,ga_variationtype,ga_upstream30bp,ga_downstream30bp,ga_title_experiment,ga_totalmarkerstested",
			0);

}

function storeAccess(studyIdent) {
	var anonStudy = e("anon_study" + studyIdent).value;
	var userStudy = e("user_study" + studyIdent).value;
	var anonExps = document.getElementsByName("anon_study" + studyIdent + "_exps");
	var userExps = document.getElementsByName("user_study" + studyIdent + "_exps");
	var tag = new Tag('store',studyIdent);
	//tag.showAdd();
	tag.showAjax();

	var pars = {};
	pars['anon_study']=anonStudy;
	pars['user_study']=userStudy;
	pars['study']=studyIdent;
	
	for(var i=0;i<anonExps.length;i++) {
		var anon_expid = anonExps[i].id;
		var user_expid = userExps[i].id;
		
		pars[anon_expid]=anonExps[i].value;
		pars[user_expid]=userExps[i].value;
	}

	successFunction = function(data, textStatus) {
		tag.showBlank();
	}
	
	errorFunction = function (XMLHttpRequest, textStatus, errorThrown) {
		alert("Sorry, an error occurred while trying to store your changes. Please try again later.");
		tag.showError("Try again.." + errorThrown,function() { storeAccess(studyIdent) });
	};
		
	jQuery.ajax({
		url:hgvbaseURL + "/admin/store",
		data:pars,
		dataType: "html",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	});
}

function storeUserAccess(studyIdent, userId, response, successFunction) {
	var userStudy = e("user" + userId + "_study" + studyIdent).value;
	var userExps = document.getElementsByName("user" + userId + "_study" + studyIdent + "_exps");
	var tag = new Tag('store',studyIdent);
	//tag.showAdd();
	tag.showAjax();

	var pars = {};
	pars['study']=studyIdent;
	if (response == 'rejected') {
		pars['study_level']='none';
	}
	else {
		pars['study_level'] = userStudy;
	}
	
	if (!response) { response = null }
	
	pars['response'] = response;
	
	for(var i=0;i<userExps.length;i++) {
		var user_expid = userExps[i].id;
		if (response == 'rejected') {
			pars[user_expid]='none';
		}
		else {
			pars[user_expid]=userExps[i].value;
		}
	}
	pars['user']=userId;
	
	if (!successFunction) {
		successFunction = function(data, textStatus) {
			tag.showBlank();
		}
	}
	
	errorFunction = function (XMLHttpRequest, textStatus, errorThrown) {
		alert("Sorry, an error occurred while trying to store your changes. Please try again later.");
		tag.showError("Try again.." + errorThrown,function() { storeUserAccess(studyIdent) });
	};
		
	jQuery.ajax({
		url:hgvbaseURL + "/admin/store",
		data:pars,
		dataType: "html",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	});
}

function addUserPermissions(studyIdent) {
	showDialog('accept', function() {
		var userId = e('user_list').value;
		jQuery("#dialog").dialog("close");
		window.location = "/admin/users?study=" + studyIdent + "&add_user=" + userId;
	}, hgvbaseURL + "/dialog/add_permissions/" ,{ study_ident: studyIdent} ,"Add a user to the permissions");
}

function selectMarkers(expIdent) {
	var tag = new Tag('limited',expIdent);
	
	showDialog('accept', function() {
		var topNo = e('top').value;
		jQuery("#dialog").dialog("close");
		setLimitedMarkers(expIdent, topNo);	
	}, hgvbaseURL + "/dialog/select_markers/" + this.identifier,null,"Select Markers in Experiment " + expIdent);
}

function setLimitedMarkers(expIdent, topNo) {
	var tag = new Tag('limited',expIdent);
	tag.showAjax();
	successFunction = function(data, textStatus) {
		tag.showEdit();
	}
	
	errorFunction = function (XMLHttpRequest, textStatus, errorThrown) {
		alert("Sorry, an error occurred while trying to store your changes. Please try again later.");
		tag.showEdit();
	};
	var pars = {
		experiment: expIdent,
		top_no: topNo
	};
	
	jQuery.ajax({
		url:hgvbaseURL + "/admin/limited",
		data:pars,
		dataType: "html",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	});
}
function requeryLevels(studyIdent, user) {
	var anonStudy = e("anon_study" + studyIdent);
	var anonExps = document.getElementsByName("anon_study" + studyIdent + "_exps");
	var userStudy = e("user_study" + studyIdent);
	var userExps = document.getElementsByName("user_study" + studyIdent + "_exps");

	if (user == 'user' && userStudy.value == 'none') {
		anonStudy.value = 'none';
	}
	
	if (user == 'anon' && anonStudy.value == 'full') {
		userStudy.value = 'full';
		
	}
	
	if (userStudy.value=='request') {
		for(var i=0;i<userExps.length;i++) {
			userExps[i].value='none';
		}
	}
	
	if (anonStudy.value != 'request') {
		for(var i=0;i<anonExps.length;i++) {
			anonExps[i].value=anonStudy.value;
		}
	}
	
	if (userStudy.value != 'request') {
		for(var i=0;i<userExps.length;i++) {
			userExps[i].value=userStudy.value;
		}
	}
}

function grantRequest(study, userId) {
	var reqform = e("requestform" + study + "_" + userId);
	reqform.action="/admin/grant";
	reqform.submit();
}

function rejectRequest(study, userId) {
	var reqform = e("requestform" + study + "_" + userId);
	reqform.action="/admin/reject";
	reqform.submit();
}


function requestAccess(entity, id) {
	showDialog('info', function() {
		jQuery("#dialog").dialog("close");
		
		var err = e('error');
		
		if (!err) {
			jQuery("#request" + entity + id).html("<font color='#ff8800'>Request Access PENDING</font>");
			jQuery("#requestaccess" + id).html("");
		}
	}, hgvbaseURL + "/user/request",{ entity: entity, id: id },"Request Access");
}

function requeryExperimentLevels(studyIdent, userId) {
	var userStudy = e("user" + userId + "_study" + studyIdent).value;
	var userExps = document.getElementsByName("user" + userId + "_study" + studyIdent + "_exps");
	for(var i=0;i<userExps.length;i++) {
		userExps[i].value=userStudy;
	}
}

function requeryRequestLevels(studyIdent, userId) {
	var userStudy = e("user" + userId + "_study" + studyIdent).value;
	var userExpList = e('study' + studyIdent + 'explist').value.split(';');

	for(var i=0;i<userExpList.length;i++) {
		var expIdent = userExpList[i];
		e('user' + userId + '_' + expIdent).value=userStudy;
	}
}