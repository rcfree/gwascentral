// $Id: addremove.js 1446 2010-05-26 14:24:28Z rcf8 $
var action;
var identifier;
var checkbox;
var updateCallback;
var hasPhenotypes;
var isResultset;
var entityCheckbox;

function AddRemove(identifier, isResultset){
	this.checkbox = new Checkbox(identifier);
	var action = new Action(Vars.getResultsets(), Vars.getStudies(), Vars.getPhenotypes());
	this.action = action;
	
	this.identifier = identifier;
	this.isResultset = isResultset;
	
	if (isResultset) {
		action.studyIdent =  Vars.getResultsetStudy(identifier);
		action.entityResultsets = Vars.getAllResultsetsInEntity(identifier);
		this.entityCheckbox = new Checkbox(action.studyIdent);
	}
	else {
		if (Vars.hasPhenotypes()) {
			action.phenotypeIdent = identifier;
			this.dialogURL = "/dialog/add_phenotype/" + identifier;
			this.dialogTitle = "Data Sets with Phenotype " + identifier;
			
		}
		else {
			action.studyIdent = identifier;
			this.dialogURL = "/dialog/add_study/" + identifier;
			this.dialogTitle = "Data Sets in Study " + identifier;
		}
		action.entityResultsets = Vars.getAllResultsetsInEntity(identifier);
	}
}

AddRemove.isTooMany = function(identifier,isResultset,checkedResultsets) {
	var ar = new AddRemove(identifier,isResultset);
	return action.isTooMany(checkedResultsets);
}

AddRemove.toggle = function(identifier,isResultset) {
	var ar = new AddRemove(identifier,isResultset);
	ar.toggle();
}

AddRemove.edit = function(identifier) {
	var ar = new AddRemove(identifier);
	ar.add(true);	
}

AddRemove.check = function(identifier,currCheckbox) {
	var ar = new AddRemove(identifier,isResultset);
	var rsChecked = Vars.getCheckedResultsets(identifier);
	if (ar.action.isTooMany(rsChecked)) {
		alert("Sorry, you can only add 16 Data Sets to the GWAS Central Browser");
		currCheckbox.checked=false;
	}
}

AddRemove.prototype.toggle = function() {
	if (this.checkbox.isChecked()) {
		this.add(false);
	}
	else {
		this.remove();
	}
}

AddRemove.prototype.checkTooMany = function(resultsets) {
	if (this.action.isTooMany(resultsets)) {
		alert("Sorry, you can only add 16 Data Sets to the GWAS Central Browser");
		this.checkbox.setChecked(false);
		return true;
	}
}

AddRemove.prototype.add = function(showPopup) {
	if (this.isResultset) {
		if (this.checkTooMany([this.identifier])) return;
		this.action.addResultsets([this.identifier]);
		this.update();
	}
	else {
		if (showPopup) {
			this._showExperiments();
		}
		else {
			if (this.action.entityResultsets.length == 1) {
				if (this.checkTooMany([this.identifier])) return;

				this.action.addEntityResultsets();
				this.update();
			}
			else {
				this._showExperiments();
			}
		}
	}
}

AddRemove.prototype._showExperiments = function() {
	window.me = this;
	var buttons = {
		Accept:function(){
			var checkedResultsets = Vars.getCheckedResultsets(window.me.identifier);
			var uncheckedResultsets = Vars.getUncheckedResultsets(window.me.identifier)
			var checkedRsCount = checkedResultsets.length;
			jQuery("#dialog").dialog("close");
			window.me.action.removeResultsets(uncheckedResultsets);
			window.me.action.addResultsets(checkedResultsets);
			window.me.update();
			
		},
		Cancel: function(){
			jQuery("#dialog").dialog("close");
			if (window.me.checkbox.rsCount.innerHTML == '0') {
				window.me.checkbox.setChecked(false);
			}
		}
	}
	showDialog(false,buttons, this.dialogURL, null, this.dialogTitle);
}

AddRemove.prototype.remove = function() {
	if (this.isResultset) {
		this.action.removeResultsets([this.identifier]);
	}
	else {
		this.action.removeEntityResultsets();
	}
	this.update();
}

AddRemove.prototype.update = function() {
	var me = this;
	this.checkbox.showAjax();
	
	var successFunction = function(textStatus, data) {
		Vars.populateFromAddRemove(me);
		me.checkbox.showCheckbox();
	}

	var store = new Store(this.action);
	store.storeResultsets(successFunction);
}
