// $Id: core.js 1527 2010-09-16 13:48:38Z rcf8 $
/**
 * @description Generic core UI methods in HGVbaseG2P for dealing with dialogs, AJAX and collapsible panels
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

function showDialog(cancel, functions, content, params, title, width, height, elementID, position) {
	// if 'content' begins with a / then get using AJAX and use the params hash
	if (!height) height = 305;
	if (!width) width = 550;
	if (!elementID) elementID="dialog";
	if (!position) position=position;
	var dialog = e(elementID);
	if (content.substring(0, 1) == '/' || content.substr(0, 5) == 'http:') {
		// show dialog before getting content and display a loader image
		
		dialog.style.display = "block";

		showAjaxLoader(elementID);
		doShowDialog(cancel, functions, title,width,height,elementID);
		jQuery.ajax({
				url:content,
				data:params,
				dataType: "html",
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					top.document.getElementById(elementID).innerHTML = "Sorry, an error occurred while trying to retrieve this information. Please try again later.";

				},
				success: function(data, textStatus) {
					// replace loader image with data content
					top.document.getElementById(elementID).innerHTML = data;
				},
				timeout: ajaxTimeout,
				type: "get"
		});
	} else {
		dialog.innerHTML = content;
		dialog.style.display = "";
		doShowDialog(cancel, functions, title,width,height,elementID,position);
	}
}


/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
function doShowDialog(cancel, functions, heading,width, height,elementID, position) {
	var buttonHash;

	var dialogElement = jQuery("#" + elementID);

	if (cancel && !functions['Cancel']) {
		functions['Cancel'] = function(){
			dialogElement.dialog("close");
		};
	}
	
	dialogElement.dialog( {
		modal :true,
		draggable :false,
		resizable :false,
		overlay : {
			opacity :0.5,
			background :"black"
		},
		buttons :functions,
		width : width + 50,
		height :height + 100,
		title: heading,
		stack: true,
		position: position
	});
	dialogElement.dialog('open');
	dialogElement.data('title.dialog',heading);
}



function ajaxSimplePost(url, params, errorFunction) {
	jQuery.ajax({
		url:url,
		data:params,
		dataType: "text",
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	});
}

function ajaxPostWithCallback(url, params, successFunction, errorFunction) {
	jQuery.ajax({
		url:url,
		data:params,
		dataType: "text",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	});
}

function ajaxGetHTML(url, params, elementID, small, successFunction, errorFunction){
	showAjaxLoader(elementID, small);
	
	if (!successFunction) {
		successFunction = function(data, textStatus){
			e(elementID).innerHTML = data;
		};
	}
	
	if (!errorFunction) {
		errorFunction = function(xhr, textStatus, errorThrown){
			e(elementID).innerHTML = "Sorry, an error occurred while trying to retrieve this information. Please try again later.";
		};
	}

	jQuery.ajax({
		url:url,
		data:params,
		dataType: "html",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "get"
	}
	
	);
}

function ajaxPostHTML(url, params, elementID, small, successFunction, errorFunction){
	showAjaxLoader(elementID, small);
	
	if (!successFunction) {
		successFunction = function(data, textStatus){
			e(elementID).innerHTML = data;
		};
	}
	
	if (!errorFunction) {
		errorFunction = function(xhr, textStatus, errorThrown){
			e(elementID).innerHTML = "Sorry, an error occurred while trying to retrieve this information. Please try again later.";
		};
	}

	jQuery.ajax({
		url:url,
		data:params,
		dataType: "html",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "post"
	}
	
	);
}

function ajaxGetXML(url, params, elementID,small,successFunction) {
	showAjaxLoader(elementID,small);
	
	if (!successFunction) {
		successFunction = function(data, textStatus){
			e(elementID).innerHTML = data;
		};
	}

	errorFunction = function (xhr, textStatus, errorThrown) {
		e(elementID).innerHTML="Sorry, an error occurred while trying to retrieve this information. Please try again later.";
	};
	
	jQuery.ajax({
		url:url,
		data:params,
		dataType: "xml",
		contentType: "text/xml",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "get"
	});
}

function ajaxGetText(url, params, elementID,small,successFunction,errorFunction) {
	showAjaxLoader(elementID,small);
	
	if (!successFunction) {
		successFunction = function(data, textStatus){
			e(elementID).innerHTML = data;
		};
	}

	if (!errorFunction) {
		errorFunction = function(xhr, textStatus, errorThrown){
			e(elementID).innerHTML = "Sorry, an error occurred while trying to retrieve this information. Please try again later.";
		};
	}
	jQuery.ajax({
		url:url,
		data:params,
		dataType: "text",
		contentType: "text/plain",
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "get"
	});
}


//function goToPage(pageUrl, isGet) {
//	var url;
//	
//	if (pageUrl.substring(0,4) != 'http') {
//		url = "/" + pageUrl;
//	}
//	else {
//		url = pageUrl;
//	}
//	
//	if (isGet) {
//		url = url + "&r=" + Vars.getResultsets().join(",");
//		document.location = url;
//	}
//	else {
//		var ie6form = document.getElementById('ie6_post');
//		ie6form.action=url;
//		ie6form.submit();
//	}
//}

function goToPage(pageUrl) {
	var url;
	
	if (pageUrl.substring(0,4) != 'http') {
		url = "/" + pageUrl;
	}
	else {
		url = pageUrl;
	}
	top.window.location.href = url;
}

function toggle(identifier) {
	var image = e(identifier + "_image");
	if (image.src.match(/plus/)) {
		toggleShow(identifier);
	} else {
		toggleHide(identifier);
	}
}

function toggleShowHide(identifier) {
	var tag = e(identifier + "_tag");
	var taghtml = tag.innerHTML;
	if (taghtml.match(/Show/)) {
		taghtml = "Hide" + taghtml.substring(4);
		jQuery('#' + identifier).show('fast');
	} else {
		taghtml = "Show" + taghtml.substring(4);
		jQuery('#' + identifier).hide('fast');
	}
	tag.innerHTML=taghtml;
}

function toggleShow(identifier) {
	var image = e(identifier + "_image");
	image.src = "/images/minus.png";
		image.alt = "-";
	jQuery('#' + identifier).show('fast');
}

function toggleHide(identifier) {
	var image = e(identifier + "_image");
	image.src = "/images/plus.png";
	image.alt = "+";
	jQuery('#' + identifier).hide('fast');
}

function toggleLessMore(identifier) {
        var tag = e(identifier + "_tag");
        var image = e(identifier + "_image");
        if (image.src.match(/plus/)) {
                image.src = "/images/minus.png";
                image.alt = "-";
				//jQuery("#" + identifier).show('fast');
                e(identifier).style.display="block";
        } else {
                image.src = "/images/plus.png";
                image.alt = "+";
				//jQuery("#" + identifier).hide('fast');
                e(identifier).style.display="none";
        }
}

function toggleShowAll() {
	jQuery("input[name=toggle_row]").each(function(item) {
		var ele = jQuery(this);
		toggleShow(ele.val());
	});
}
function toggleHideAll() {
	jQuery("input[name=toggle_row]").each(function(item) {
		var ele = jQuery(this);
		toggleHide(ele.val());
	});
}

//shorthand getElementById
function e(elementID) {
	var e = document.getElementById(elementID);
	if (e == null) {
		e = top.document.getElementById(elementID);
	}

	if (e == null) {
		return null;
	}
	return e;
}

function showAjaxLoader(elementID,small) {
	var e = document.getElementById(elementID);
	var image="/images/ajax-loader.gif";

	if (small) image = "/images/small-ajax-loader.gif";
	
	if (!e) {
		e = top.document.getElementById(elementID);
	}
	
	if (small==1) {
		_ajaxElementID = elementID;
		_ajaxImage = e.src;
		e.src = suffix + image ;
		return;
	}
	else if (small==2) {
		e.innerHTML = "<center><img style='vertical-align:middle' src='"
			+ image + "></center>";
		return;
	}
		e.innerHTML = "<center><img style='vertical-align:middle' src='"
			+ image + "'></center>";
}

function addBookmark(title, url) {
		if (window.sidebar) { // Mozilla Firefox Bookmark
			window.sidebar.addPanel(title, url,"");
		} else if( window.external ) { // IE Favorite
			window.external.AddFavorite( url, title); }
	 }


function getScreenWidth() {
	var screenWidth;
	if (jQuery.browser.msie) {
		var ver = jQuery.browser.version;

		if (ver.substring(0, 1) == "7") {
			screenWidth = jQuery(window).width();
		} else {
			screenWidth = document.documentElement.clientWidth;
		}
	} else {
		screenWidth = window.innerWidth;
	}
	return screenWidth
}

function toggleSubmit(checkBoxElementId, submitElementId) {
	var submitElementId;
	if (e(checkBoxElementId).checked) {
		e(submitElementId).disabled="";
	}
	else {
		e(submitElementId).disabled="disabled";
	}
}

function addCommas(nStr)
{
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function pluralize(number, singular, plural){
	if (!plural) { plural = singular + "s"; }
	var text = plural;
	if (number == 1) text = singular;
	return number + " " + text;
}
