// $Id: store.js 1527 2010-09-16 13:48:38Z rcf8 $
/**
 * @description Module for storing studies and resultsets via AJAX
 * @author <a href="mailto:rcfree@gmail.com">Rob Free</a>
 * @version 3.0
 */

var resultsets;
var studies;

function Store(action) {
	if (action) {
		this.resultsets = action.resultsets;
		this.studies = action.studies;
	}
}

Store.prototype.storeResultsets = function(successFunction) {
	var rs = this.resultsets;
	
	if (rs.length == 0) rs=['none'];

	errorFunction = function (XMLHttpRequest, textStatus, errorThrown) {
		//if (_ajaxElementID) e(_ajaxElementID).src=_ajaxImage;
		alert("Sorry, an error occurred while trying to store your changes. Please try again later.");
	};
	var data = {
		r: rs
	};
	
	jQuery.ajax({
		url:"/browser/store",
		data:data,
		success: successFunction,
		error: errorFunction,
		timeout: ajaxTimeout,
		type: "get"
	});
}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
//Store.prototype.storeRegion = function(region) {
//	jQuery.get(hgvbaseTopURL + "/browser/store", {
//		region :region
//	}, function(data, textStatus) {
//	}, 'text');
//}

/**
 * @description Switches tab from within an IFrame (not possible through JQuery)
 * @param {String} tabName The name of the tab to switch to
 * @returns {Array} Lines from the file.
 */
//Store.prototype.checkMaxAdded = function() {
//	if (this.resultsets.length >= maxResultsets) {
//		alert("You can only compare a maximum of " + maxResultsets + " Result Sets");
//		return true;
//	}
//	return false;
//}

//Store.prototype.storeSettings = function(settings, tracks) {
//	jQuery.get(hgvbaseURL + "/browser/store", {
//		tracks: tracks,
//		settings: settings
//	}, function(data, textStatus) {
//		alert("saved");
//	}, 'text');
//}
