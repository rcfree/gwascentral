[GENERAL]
description   = HGVbaseG2P Genome Browser

db_adaptor  = Bio::DB::SeqFeature::Store
db_args     = -dsn dbi:mysql:HGVbaseG2P_gff_dev2:viti.gene.le.ac.uk
user        = hgvbaseg2p
pass        = hgvbaseg2p
plugins     = RegionView
balloon tips = 1
hapmap_populations = CEU CHB JPT YRI 

# ROBF: error in IE JS so switch off drag and drop
drag and drop = 0
ensembl_das_url = http://www.ensembl.org/Homo_sapiens/cytoview?
aggregators = gene_density{bin:gene} gt_snps_hires{bin:snp_gtsnp_hires}
analysis_plugins = 1
# Where temporary images are stored
tmpimages   = /gbrowse/tmp

# Mummi: Disable tracking completely for now, until we figure out how to make Rob's plugin
# work with caching on and *not* duplicate the same panel-images across studies.
cache time = 0
nocache=0

# list of tracks to turn on by default
default features = Genes Ideogram:overview

# The class of the objects used to establish the reference coordinates.
reference class  = Sequence

# examples to show in the introduction
examples = Chr10
instructions = 0
reports = 0

# "automatic" classes to try when an unqualified identifier is given
automatic classes =

init_code = 
search_js = submitSearch();


### HTML TO INSERT AT VARIOUS STRATEGIC LOCATIONS ###
# inside the <head></head> section
head = 
	<link rel="stylesheet" href="/css/ui.tabs.css"
	type="text/css" media="print, projection, screen" />
	<script src="/js/log4js.js" type="text/javascript"></script>
	<script src="/js/hgvbaseg2p/ui/variables.js" type="text/javascript"></script>
	<script src="/js/hgvbaseg2p/store.js" type="text/javascript"></script>
	<script src="/js/hgvbaseg2p/ui/core.js" type="text/javascript"></script>
	<script src="/js/hgvbaseg2p/ui/browser.js" type="text/javascript"></script> 
	<script src="/js/hgvbaseg2p/ui/system.js" type="text/javascript"></script> 
	<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
	<script src="/js/jquery.dimensions.pack.js" type="text/javascript"></script>
	<script src="/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
	<script>
	var loc = top.location;
	var hgvbaseURL=loc.protocol + "//" + loc.hostname;
	jQuery(document).ready(function(){
 		resizeRegionView();
	});
	var ajaxTimeout=30000;
	</script>
# at the top...
header = <span></span>
	
# a footer
footer = <div id="markerinfo"></div><script>
	
	</script>
# Various places where you can insert your own HTML -- see configuration docs
html2 =
html3 = 
html4 = 
html5 = 
html6 = 

# what image widths to offer
image widths  = 450 640 800 1024

# default width of detailed view (pixels)
default width = 800

# Web site configuration info
stylesheet  = /gbrowse/hgvbaseg2p.css
buttons     = /gbrowse/images/buttons
tmpimages   = /gbrowse/tmp

# max and default segment sizes for detailed view
max segment     = 300000000
min segment     = 50000
default segment = 3000000

# zoom levels
zoom levels    = 50000 100000 500000 1000000 3000000 10000000 50000000

# colors of the overview, detailed map and key
overview bgcolor = white
detailed bgcolor = white
key bgcolor      = white

label density = 25
bump density  = 100

show sources=0
instructions section = closed
search section = open
overview section = open
region section = open
details section = open
tracks section = closed
display_settings section = closed
upload_tracks section = closed

########################
# Default glyph settings
########################

[TRACK DEFAULTS]
glyph         = xyplot
height        = 10
bgcolor       = lightgrey
fgcolor       = white
font2color    = blue
label density = 25
bump density  = 100
# where to link to when user clicks in detailed view
link          = AUTO
description = 1
group_on = display_name

################## TRACK CONFIGURATION ####################
# the remainder of the sections configure individual tracks
###########################################################

[Ideogram:overview]
feature      = chromosome:ucsc chromosome_band:ucsc centromere:ucsc
glyph        = ideogram
bgcolor      = gneg:white gpos25:#aaaaaa gpos50:#999999 gpos:#999999 gpos75:#444444 gpos100:black gvar:var stalk:#666666
fgcolor      = black
arcradius    = 4
height       = 20
bump         = 0

[Genes]
feature      = mRNA:ucsc
glyph        = processed_transcript
thin_utr     = 1
strand_arrow = 0
decorate_introns = 1
bgcolor      = green
fgcolor      = green
fontcolor    = green
font2color   = green
height       = 8
description  = 1
label density = 15
decorated_introns= 1
link         = http://genome.ucsc.edu/cgi-bin/hgGene?hgg_type=knownGene&hgg_gene=$name
link_target  = _blank
key          = UCSC: RefSeq Genes
citation     = UCSC: RefSeq Genes
category     = Genes
pad top = 10
pad bottom = 10
bump=1		
balloon hover = sub {
					my ($feat) = shift;
					
					if ($feat->type eq 'bin:mRNA') {
						my $density = $feat->score == 1 ? 'gene transcript' : 'gene transcripts';
						return "<h2>Gene Density</h2><h3>".$feat->score." ".$density."<br/>".$feat->ref.":".$feat->start."..".$feat->stop."</h3>"
					}
					else {
						my $html = "";
						$html.="<h2>Gene ".$feat->name."</h2>";
						$html.="<h3>".$feat->ref.":".$feat->start."..".$feat->stop."</h3>";
						
						return $html;
					}
				} 
balloon click = sub {
					my ($feat) = shift;
					
					if ($feat->type eq 'bin:mRNA') {
						my $density = $feat->score == 1 ? 'gene transcript' : 'gene transcripts';
						return "<h2>Gene Density</h2><h3>".$feat->score." ".$density."<br/>".$feat->ref.":".$feat->start."..".$feat->stop."</h3>"
					}
					else {
						my $html = "";
						$html.="<h2>Gene ".$feat->name."</h2>";
						$html.="<h3>".$feat->ref.":".$feat->start."..".$feat->stop."</h3>";
						$html.="<h4>View details in:</h4>";
						$html.="<a href='http://www.ensembl.org/Homo_sapiens/geneview?gene=".$feat->name."' target='_blank'>Ensembl</a> | "; 
						
						my $url = "http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&db=hg18&position=".$feat->ref.":".$feat->start."..".$feat->stop;
						$html.="<a href='".$url."' target='_blank'>UCSC</a>";
						return $html;
					}
				} 

[Genes:8000001]
feature      = bin:mRNA
glyph        = xyplot
graph_type   = boxes
bgcolor      = green
fgcolor      = black
height       = 50
scale_color  = black
scale = none
min_score = 0
max_score = 150
bump=0
key          = UCSC: RefSeq Gene Density
citation     = UCSC: RefSeq Gene Density

[l1]
feature      = chromosome:UCSC
glyph        = line
fgcolor      = lightgrey
linewidth    = 3
bump         = 0
label        = 0
title        = 0
description  = 0
key          = 0
citation     = 0



[l2]
feature      = chromosome:UCSC
glyph        = line
fgcolor      = lightgrey
linewidth    = 3
bump         = 0
label        = 0
title        = 0
description  = 0
key          = 0
citation     = 0



[l3]
feature      = chromosome:UCSC
glyph        = line
fgcolor      = lightgrey
linewidth    = 3
bump         = 0
label        = 0
title        = 0
description  = 0
key          = 0
citation     = 0
