# $Id: RegionView.pm 1515 2010-07-13 13:52:11Z rcf8 $

#ROB's RegionView plugin for hgvbrowse which calls logic in GwasCentral::Browser::Core to draw significance glyphs

package Bio::Graphics::Browser::Plugin::RegionView;
use Env;
my $home = $ENV{GWASCENTRAL_HOME} || die "FATAL ERROR:Need to set GWASCENTRAL_HOME env variable"; 
my $conf = $home."/conf/main.conf";

use lib $home."/lib";

use GwasCentral::Search::Util qw(new_Query);


use strict;
use warnings;

use Bio::Graphics::Browser::Plugin;
use CGI qw(:standard *table);
use Log::Log4perl qw(:easy);
use Data::Dumper qw(Dumper);

use vars '@ISA';

@ISA = qw(Bio::Graphics::Browser::Plugin);

my %SITES;
my $percentid;
my @COLORS = qw(red green blue orange cyan black
  turquoise brown indigo wheat yellow emerald);
  
sub name { return "RegionView"; }

sub description {
	p(
"This plugin displays data related to -log P-values including the maximum and count of significant values as a histogram."
	);
}

sub type { 'annotator' }

sub init {
	my $self = shift;
	my $conf = $self->browser_config;
}

sub config_defaults {
	my $config={};
	my $browser = new_Query( "Browser::Region", { conf_file =>
			  $conf, DataSources=>1
	});
	foreach my $f(values %{$browser->Filters}) {
		if (ref($f->retrieve_from) eq 'HASH') {
			my %checklist = map { $_ => 1} @{$f->_default_checklist};
			foreach my $fv(keys %{$f->retrieve_from}) {
				$config->{$fv}  = $checklist{$fv}
			}
		}
		else {
			$config->{$f->retrieve_from}  = $f->value;
		}
	}
	
	return $config;
}

sub reconfigure {
	my $self = shift;
	my $current_config = $self->configuration;
	
	#set up browser object and options
	my $browser = new_Query( "Browser::Region", { conf_file =>
			  $conf, DataSources=>1
	});
	my @td = ();

	foreach my $f(values %{$browser->Filters}) {
		if (ref($f->retrieve_from) eq 'HASH') {
			foreach my $fv(keys %{$f->retrieve_from}) {
				$current_config->{$fv}  = $self->config_param($fv);
			}
		}
		else {
			$current_config->{$f->retrieve_from}  = $self->config_param($f->retrieve_from);
		}
	}
}

sub configure_form {
	my $self           = shift;
	my @th = ({ -style => 'text-align:right', -style => 'display:none' },"Hidden Fields:");
	
	#set up browser object and options
#	my $browser = new_Query( "Browser::Region", { conf_file =>
#			  $conf
#	});
#	my @td = ();
#	my $current_config = $self->configuration;
#	foreach my $f(values %{$browser->Filters}) {
#		if (ref($f->retrieve_from) eq 'HASH') {
#			foreach my $fv(keys %{$f->retrieve_from}) {
#				push @td,hidden(
#                    -name => $self->config_name($fv),
#                    -default => $current_config->{$fv},
#                );
#			}
#		}
#		else {
#			push @td,hidden(
#                    -name => $self->config_name($f->retrieve_from),
#                    -default => $current_config->{$f->retrieve_from},
#                );
#		}
#	}
#	push @th, td(@td);
#	
#	
#          return table({-style=>'width:'},
#           TR({-class=>'searchtitle'},
#            @th));
}    

#bare minimum logic in annotate method
sub annotate {
	  my ($self,$segment)    = @_;
	  #logic for feature generation is present in GwasCentral::Browser
 		warn "gets to annotated";
		 my %params = %{$self->configuration};

		 $params{q}=$segment->seq_id.":".$segment->start."..".$segment->stop;
	  
	#set up browser object and options
	my $browser = new_Query( "Browser::Region", { conf_file =>
			  $conf, features => $self->new_feature_list,
			  DataSources_from_config=>1,
			  AccessControl_from_config => 1
	});		  
	warn "params:".Dumper(\%params);
	$browser->params(\%params);
	$browser->prepare_Filters();
	my $feature_data = $browser->as_features;
	return $feature_data;
}

1;

__END__

=head1 NAME

Bio::Graphics::Browser::Plugin::RegionView - Calculate variety of P value feature data including count of significant markers, max significance etc.

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 OPTIONS


=head1 BUGS

None known yet.

=head1 SEE ALSO

L<Bio::Graphics::Browser::Plugin>

=head1 AUTHOR

Copyright (c) 2007 University of Leicester.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
