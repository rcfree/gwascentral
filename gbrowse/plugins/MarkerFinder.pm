package Bio::Graphics::Browser::Plugin::MarkerFinder;
# $Id: MarkerFinder.pm 1442 2010-05-26 14:20:55Z rcf8 $
# test plugin
#ROB's MarkerFinder plugin for hgvbrowse which calls logic in GwasCentral::Browser::Core to find markers entered in the search box

use strict;
use Bio::Graphics::Browser::Plugin;
use Bio::Graphics::Feature;
use Text::Shellwords;
use DBI;
use CGI qw(:standard *table);

my $home = $ENV{GWASCENTRAL_HOME} || die "FATAL ERROR:Need to set GWASCENTRAL_HOME env variable"; 

use lib $home."/lib";

use GwasCentral::Search::Browser::Region;
use Data::Dumper qw(Dumper);
use Bio::Graphics::Browser::PageSettings;
use GwasCentral::DataSource::Marker;

use vars '$VERSION','@ISA';
$VERSION = '0.15';

@ISA = qw(Bio::Graphics::Browser::Plugin);

sub name { "Marker finder" }

sub description {
  p("The oligo finder plugin finds oligos between 11 and 50 bp in length.",
    "It does a slow search, making it suitable only for small (<150 MB) genomes.",
    "[NOTE TO SYSADMINS: The browser must use the Bio::DB::GFF schema for this plugin to work.]").
  p("This plugin was written by Lincoln Stein.");
}

sub type { 'finder' }

sub config_defaults {
  my $self = shift;
  return { };
}

# we have no stable configuration
# sub reconfigure { }

sub configure_form {
	my $self           = shift;
	my $current_config = $self->configuration;
	
          return table({-style=>'width:'},
           TR({-class=>'searchtitle'},
            th(
                { -style => 'text-align:right', -style => 'display:none' },
                "Hidden Fields:",
                td(hidden(
                    -name => $self->config_name('resultsets'),
                    -default => $current_config->{'resultsets'},
                )))));

}    
# find() returns undef unless the OligoFinder.searcholigo parameter
# is specified and valid.  Returning undef signals the browser to invoke the
# configure_form() method.
# If successful, it returns an array ref of Bio::SeqFeatureI objects.
sub find {
  my $self     = shift;
  my $segments = shift; # current segments - can search inside them or ignore
                        # In this example we do a global search.
  my $markerid = shift;

  auto_find($markerid);
}

# auto_find() does the actual work
# It is also called by the main page as a last resort when the user
# types something into the search box that isn't recognized.
sub auto_find {
  my $self  = shift;
  my $searchterms = shift;
  
  my $session       = Bio::Graphics::Browser::PageSettings->new($self->browser_config,param('id'));
  
  my @results =();
  my $browser = GwasCentral::Search::Browser::Region->new({
  	 conf_file =>
					$home."/conf/main.conf",
					feature_list=>$self->new_feature_list,
  });
  $browser->params($session->plugin_settings('RegionView'));  
  $browser->DS('Marker',GwasCentral::DataSource::Marker->new({conf_file => $browser->config}));
  my $features = $browser->search_for_markers();
  
  return ($features,$searchterms);
}
1;
