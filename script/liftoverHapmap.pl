#!perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use GwasCentral::DataSource::Hapmap;
use GwasCentral::DataSource::Feature;
use GwasCentral::DataSource::Browser;
use GwasCentral::Browser::Util qw(chr_list);
use GwasCentral::Base qw(open_infile open_outfile);
use Bio::FeatureIO;
use Log::Log4perl qw(:easy);
use Data::Dumper qw(Dumper);
use Getopt::Euclid  qw( :minimal_keys );

Log::Log4perl->easy_init($INFO);

my $tmpdir    = "/scratch/rcf8/tmp";
my $dumpdir =  "/scratch/rcf8/hapmap";
my $lo_dir    = "/srv/data/common/software/liftover";
my $chainfile = "$lo_dir/chain_files/hg18ToHg19.over.chain";
my $program   = "$lo_dir/bin/liftOver";
my $mysql_dir = "/srv/data/mysql/";
my $hds =
  GwasCentral::DataSource::Hapmap->new( { conf_file => 'conf/main.conf' } );
my $fds =
  GwasCentral::DataSource::Feature->new( { conf_file => 'conf/main.conf' } );
 my $bds =
  GwasCentral::DataSource::Browser->new( { conf_file => 'conf/main.conf' } );
my $dbh = $hds->dbh->storage->dbh;
my $BATCH_SIZE=1000000;

update_mapped_markers_in_feature_db($ARGV{'chr'});
#transfer_hapmap_bed_files_and_liftover();
#find_changed_markers_from_file();
#update_mapped_markers_in_hapmap_db($ARGV{'chr'});

#delete_unmapped_markers_in_hapmap();

sub _output_hapmap_snps_from_database {
	my ($chr) = @_;
	mkdir "$tmpdir/bed";
	chdir "$tmpdir/bed";
	chmod 0777, "$tmpdir/bed";
	#foreach my $chr ( keys %{&chr_list} ) {
		INFO "Delete file for chr $chr\n";
		unlink "$tmpdir/bed/markers_chr" . $chr . ".bed";

		# or die "Unable to delete 'liftover_chr".$chr.".bed'" ;
		INFO "Start output SNPs for chr $chr\n";
		eval {
			$bds->dbh->do(
				    "select Chr, Start - 1, Start, Accession, Status, CurrentIdentifier, Identifier 
			INTO OUTFILE '$tmpdir/bed/markers_chr" . $chr . ".bed'
			  FIELDS TERMINATED BY ' '
			  from all_markers
				where Chr = '$chr'
			"
			);
			INFO "End output SNPs for chr $chr\n";
		};
		if ($@) {
			WARN "An error occurred during output of chr $chr\nError:" . $@;
		}
	#}
}

sub transfer_hapmap_bed_files_and_liftover {
	chdir "$tmpdir/bed";
	foreach my $chr ( keys %{&chr_list} ) {
		unlink "liftover_chr" . $chr . ".bed.mapped";
		unlink "liftover_chr" . $chr . ".bed.unmapped";
		system "$program liftover_chr" 
		  . $chr
		  . ".bed $chainfile liftover_chr"
		  . $chr
		  . ".bed.mapped liftover_chr"
		  . $chr
		  . ".bed.unmapped";
	}
}

#sub find_changed_markers_from_file {
#	my $mapped_fh = open_infile("/tmp/bed/liftover_all.bed.mapped");
#	my %mapped_markers;
#	INFO "Loading mapped markers...";
#	my $lcount = 0;
#
#	while( my $line = $mapped_fh->getline) {
#		my ($chr, $start, $stop, $marker) = split(/\s+/,$line);
#		$mapped_markers{$marker}=[$chr, $start, $stop, $marker];
#		$lcount++;
#		if ($lcount%100000==0) { INFO "$lcount mapped markers loaded"; }
#	}
#
#	$mapped_fh->close;
#	INFO "Loading complete";
#	my $orig_fh = open_infile("/tmp/bed/liftover_all.bed");
#	my $counter = 0;
#	my $changed_count = 0;
#	while( my $line = $orig_fh->getline) {
#		my ($chr, $start, $stop, $marker) = split(/\s+/,$line);
#		if (!$mapped_markers{$marker}->[1]) {
#			INFO "Problem with marker: $marker".Dumper($mapped_markers{$marker});
#		}
#		else {
#			if ($start ne $mapped_markers{$marker}->[1]) {
#				#INFO "Marker $marker has changed from $chr:$start to ".$mapped_markers{$marker}->[0].":".$mapped_markers{$marker}->[1]."\n";
#				$changed_count++;
#			}
#		}
#		$counter++;
#		if ($lcount%100000==0) { INFO "$changed_count/$counter markers changed"; }
#		$lcount++;
#	}
#	$orig_fh->close;
#
#}

sub update_mapped_markers_in_feature_db {
	my ($chr) = @_;
	_output_hapmap_snps_from_database($chr);
	_update_feature_db($chr);
	_delete_snp_file($chr);
}

sub _delete_snp_file {
	my ($chr) = @_;
	INFO "Delete file for chr $chr\n";
	unlink "$tmpdir/bed/markers_chr" . $chr . ".bed";
}

sub update_mapped_markers_in_hapmap_db {
	my ($chr) = @_;
	_generate_table_dump($chr);
	_liftover_chr_data($chr);
	_copy_chr_data($chr);
	#_drop_temp_table("TEMP_chr".$chr."_ldscore");
}

sub _generate_table_dump {
	my ($chr) = @_;
	my $table     = "chr" . $chr . "_ldscore";
	chdir "$dumpdir";
	INFO "Generating $table dump in $dumpdir";
	run_command("mysqldump ".$hds->name." $table --tab=$dumpdir");
}

sub _drop_temp_table {
	my ($tmp_table) = @_;
	$hds->dbh->do("drop table $tmp_table");
}

sub _liftover_chr_data {
	my ( $chr ) = @_;
	INFO "Updating chr$chr";
	my %unmapped_markers = _get_unmapped_markers($chr);
	my %mapped_markers   = _get_mapped_markers($chr);
	
	my $table     = "chr" . $chr . "_ldscore";
	#run_command("mysqldump ".$hds->name." $table --tab=$tmpdir");
	
	#create temporary table TEMP_chrNN_ldscore
	my $infile = $dumpdir."/".$table.".txt";
	my $outfile = $dumpdir."/".$table.".txt.liftover";
	#use File::CountLines qw(count_lines);
	#my $no_of_lines = `wc -l $infile`;
	INFO "Processing $infile";
	my $infh = open_infile($infile);
	my $outfh = open_outfile($outfile);
	#loop through each page of records
	my @batch = ();
	my $counter=1;
	my $batch_no = 1;
	#create pager using number of records
	#my $pager = Data::Page->new();
	#$pager->total_entries($no_of_lines);
	my $batch_start = time;
	while ( my $record = $infh->getline ) {
		chomp($record);
		my @d = split(/\t/,$record);
		
		my $lo_fstart = $mapped_markers{ $d[3] };
		my $lo_fstop  = $mapped_markers{ $d[4] };
#		INFO "marker1:" . $m1 . " = " . $fstart . "=>" . $lo_fstart;
#		INFO "marker2:" . $m2 . " = " . $fstop . "=>" . $lo_fstop;
		
		#if not there check the unmapped markers -> go to next record
	
		if ( !$lo_fstart ) {
			$unmapped_markers{ $d[3] }
			  ? INFO "marker1 " . $d[3] . " unmapped"
			  : INFO "marker1 "
			  . $d[3]
			  . " not found in mapped or unmapped";
			next;
		}
		if ( !$lo_fstop ) {
			$unmapped_markers{ $d[4] }
			  ? INFO "marker2 " . $d[4] . " unmapped"
			  : INFO "marker2 "
			  . $d[4]
			  . " not found in mapped or unmapped";
			next;
		}

#update the fstart and fstop positions with the coords in the mapped markers hash
		$d[0] = $lo_fstart;
		$d[1] = $lo_fstop;

		#add changed row to batch array
		push @batch, join("\t",@d);
		if ($counter % $BATCH_SIZE == 0) {
			foreach my $row(@batch) {
				$outfh->print($row."\n");
			}
			my $batch_end = time;
			my $ttaken = $batch_end-$batch_start;
			INFO "Processed $counter lines (batch $batch_no) - took $ttaken secs";
			#$pager->current_page($pager->next_page());
			$batch_no++;
			@batch=();
			$batch_start = time;
		}
		$counter++;
	}
	INFO "Processed $counter lines in total - outputting final batch to file";
	foreach my $row(@batch) {
		$outfh->print($row."\n");
	}
	$infh->close;
	$outfh->close;
}

sub _copy_chr_data {
	my ($chr) = @_;
	my $table     = "chr" . $chr . "_ldscore";
	#my $tmp_table = "TEMP_" . $table;
	
   #unpack the chrNN_ldscore table
	INFO "Unpacking table $table";
	_unpack_table($table);
	
	INFO "Flush table $table";
	$hds->do("flush table $table");
	
	#truncate the chrNN_ldscore table
	#disable keys on chrNN_ldscore table
	$hds->disable_ld_table_keys("chr$chr");

	#insert all from TEMP_chrNN_ldscore into chrNN_ldscore
	INFO "Truncating table $table";
	$hds->do("truncate table $table");
	
	my $importfile = $table.".txt.liftover";
	
	INFO "Importing file $importfile => $table";
	chdir "$dumpdir";
	run_command("mysqlimport ".$hds->name." -v -v --local $importfile");
	#$hds->do("insert into $table (fstart, fstop, pop_code, marker1, marker2, dprime, rsquare, lod, fbin) select fstart, fstop, pop_code, marker1, marker2, dprime, rsquare, lod, fbin from $tmp_table");

	$hds->enable_ld_table_keys("chr$chr");

	INFO "Optimizing table $table";
	$hds->do("optimize table $table");

	INFO "Packing table $table";
	_pack_table($table);
	my $dbh = $hds->dbh->storage->dbh;
	my $sth = $dbh->prepare("select count(*) from $table");
	$sth->execute;
	my $row = $sth->fetchrow_arrayref;
	INFO "Successfully imported ".$row->[0]." rows into table $table";
}


#sub _process_page {
#	my ( @data ) = @_;
##	INFO "Page:" . $pager->current_page . "/" . $pager->last_page;
##	my $sth = $hds->prepare(
##"select fstart, fstop, pop_code, marker1, marker2, dprime, rsquare, lod, fbin from $table order by fstart limit "
##		  . ( $pager->first - 1 ) . ","
##		  . $pager->entries_on_this_page );
##	$sth->execute;
#
#	#get each record as an arrayref/hashref
#	my @batch = ();
#	while ( my $record = $sth->fetchrow_arrayref ) {
#		my $fstart = $mapped_ref->{ $record->[3] };
#		my $fstop  = $mapped_ref->{ $record->[4] };
#		#INFO "marker1:" . $record->[3] . " = " . $record->[0] . "=>" . $fstart;
#		#INFO "marker2:" . $record->[4] . " = " . $record->[1] . "=>" . $fstop;
#
#		#if not there check the unmapped markers -> go to next record
#		if ( !$fstart ) {
#			$unmapped_ref->{ $record->[3] }
#			  ? INFO "marker " . $record->[3] . " unmapped"
#			  : INFO "marker "
#			  . $record->[3]
#			  . " not found in mapped or unmapped";
#			next;
#		}
#		if ( !$fstop ) {
#			$unmapped_ref->{ $record->[4] }
#			  ? INFO "marker " . $record->[4] . " unmapped"
#			  : INFO "marker "
#			  . $record->[4]
#			  . " not found in mapped or unmapped";
#			next;
#		}
#
##update the fstart and fstop positions with the coords in the mapped markers hash
#		$record->[0] = $fstart;
#		$record->[1] = $fstop;
#
#		#add changed row to batch array
#		push @batch, [ @{$record} ];
#	}
#
#	#insert batch array of new records into the temporary table
#	$sth = $hds->prepare(
#"insert into $tmp_table (fstart, fstop, pop_code, marker1, marker2, dprime, rsquare, lod, fbin) values(?,?,?,?,?,?,?,?,?)"
#	);
#
#	foreach my $item (@batch) {
#		$sth->execute( @{$item} );
#	}
#}

sub _pack_table {
	my ($table) = @_;
	my $status = $hds->get_table_status($table);
	if ( $status->{Row_format} ne "Compressed" ) {
		INFO "Table '$table' is not compressed - running compress";
		#;
		chdir $mysql_dir . "/" . $hds->name;
		INFO "lock table $table";
		$dbh->do("lock table $table write");
		INFO("flush table $table");
		$dbh->do("flush table $table");
		INFO("change permissions on table files");
		#run_command("chmod o+rw $table*");
		INFO("check table $table");
		run_command("myisamchk --check --fast $table");
		INFO("pack table $table");
		run_command("myisampack -v -f --tmpdir=$tmpdir $table");
		INFO("reindex table $table");
		run_command("myisamchk -raqS --tmpdir=$tmpdir $table");
		INFO("unlock tables");
		$dbh->do("unlock tables");
		INFO("flush table $table");
		$dbh->do("flush table $table");
	}
}

sub _unpack_table {
	my ($table) = @_;
	my $status = $hds->get_table_status($table);
	if ( $status->{Row_format} eq "Compressed" ) {
		INFO "Table '$table' is compressed - running uncompress";
		chdir $mysql_dir . "/" . $hds->name;
		run_command("myisamchk --unpack $table.MYI --tmpdir=$tmpdir -v");
	}
}

sub run_command {
	my ($cmd) = @_;
	INFO "Running $cmd";
	system $cmd;
}

sub _get_markers {
	my ($chr) = @_;
	my %mapped_markers;
	my $mapped_fh =
	  open_infile( "$tmpdir/bed/markers_chr" . $chr . ".bed" );
	INFO "Loading mapped markers...";
	my $lcount = 0;
	my %ident_to_accession = ();
	while ( my $line = $mapped_fh->getline ) {
		my ( $chr, $start, $stop, $marker, $status, $current_ident, $identifier ) = split( /\s+/, $line );
		$mapped_markers{$marker} = [$stop,$status, $current_ident];
		$ident_to_accession{$identifier}=$marker;
		$lcount++;
		if ( $lcount % 100000 == 0 ) {
			INFO "$lcount mapped markers loaded";
		}
	}
	$mapped_fh->close;
	
	my %markers = ();
	foreach my $accession (keys %mapped_markers) {
		my ( $start, $status, $current_ident ) = @{$mapped_markers{$accession}};
		
		if ($status eq 'dead') {
			my $current_accession = $ident_to_accession{$current_ident};
			INFO "Marker $accession is dead.";
			if ($current_accession) {
				INFO "Current marker is $current_accession";
				($start,$status, $current_ident) = @{$mapped_markers{$current_accession}};
			}
			else {
				INFO "No current marker";
				next;
			}
			
		}
		$markers{$accession}=[$start, $status];
	}
	
	INFO "$lcount mapped markers loaded in total";
	return %markers;
}

#sub _get_unmapped_markers {
#	my ($chr) = @_;
#	my %unmapped_markers;
#	my $unmapped_fh =
#	  open_infile( "$tmpdir/bed/marker_chr" . $chr . ".bed.unmapped" );
#	INFO "Loading unmapped markers...";
#	my $lcount = 0;
#
#	while ( my $line = $unmapped_fh->getline ) {
#		next if $line =~ /^#/;
#		my ( $chr, $start, $stop, $marker ) = split( /\s+/, $line );
#		$unmapped_markers{$marker} = $stop;
#		$lcount++;
#		if ( $lcount % 100000 == 0 ) {
#			INFO "$lcount unmapped markers loaded";
#		}
#	}
#	$unmapped_fh->close;
#	return %unmapped_markers;
#}

sub _update_feature_db {
	my ($chr) = @_;

		my %mapped_markers   = _get_markers($chr);
		#my %unmapped_markers = _get_unmapped_markers($chr);
		INFO "Getting features for chr $chr";
		my @features         = $fds->dbh->features(
			-type => 'snp:HapMap_gt',
			-ref  => "chr$chr",
		);
		INFO "Number of features:" . scalar(@features);
		my $active_count = 0;
		my $dead_count = 0;
		my $unknown_count = 0;
		my $counter = 1;
		foreach my $f (@features) {
			my @load_id  = $f->attributes('load_id');
			my $f_marker = $load_id[0];
			$f_marker =~ s/SNP\://;
			#INFO "marker:$f_marker";
			my ($start,$status) = @{$mapped_markers{$f_marker} || []};
			!$status and $status = "unknown";
			if ($status eq 'active') {
				$f->start( $start );
				$f->stop( $start );
				if ( $f->has_tag("build") ) {
					$f->remove_tag("build");
				}
				$f->add_tag_value( "build", "GRCh37" );
				$f->update;
				$active_count++;
			}
			elsif ($status eq 'dead') {
				INFO "Deleted dead marker $f_marker";
				$dead_count++;
				$fds->dbh->delete($f);
				next;
			}
			elsif ($status eq 'unknown') {
				INFO "Deleted unknown marker $f_marker";
				$unknown_count++;
				$fds->dbh->delete($f);
			}
			else {
				INFO "Unexpected status $status for marker $f_marker";
				$unknown_count++;
				$fds->dbh->delete($f);
			}
			#INFO "marker:" . Dumper($mapped);
			#INFO "f_marker:" . Dumper($f);
			
			if ($counter % 10000 == 0) { INFO "$counter/".scalar(@features)." features examined"; }
			$counter++;
		}
		INFO "$active_count active features";
		INFO "$dead_count dead features";	
		INFO "$unknown_count unknown features";	
}

exit 0;

__END__


=head1 NAME

  - Liftover Hapmap database from build 36 to 37

=head1 VERSION

  0.1

=head1 DESCRIPTION




=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over


=item <chr>

  Specify chromosome number to use

=back

=head1 OPTIONS

=over

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=cut
