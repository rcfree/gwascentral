#!/usr/local/bin/perl -w

# $Id: importHapmap.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Pack databases using myisampack

use strict;
use warnings;
use English qw( -no_match_vars );

use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Data::Dumper;

use HGVbaseG2P::Core qw(init_logger load_module);
use HGVbaseG2P::Deploy;
use HGVbaseG2P::Database;

use Benchmark;
use List::MoreUtils qw(any);

use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code (where the options are specified)
use Getopt::Euclid qw( :minimal_keys );

# Initialize configuration & logger
my $log = init_logger( "$FindBin::Bin/../conf/log4perl.conf", basename($0) );
my $deploy = HGVbaseG2P::Deploy->new(
	{ conf_file => $ARGV{c} } );

	  my $db = HGVbaseG2P::Database->new( { conf_file => $deploy->config } );

	  foreach my $dbname ( split( " ", $ARGV{d} ) ) {
		$deploy->databases->{$dbname} = $db->new_instance($dbname);
		$deploy->pack_database($dbname);
	}

	exit 0;

__END__


=head1 NAME

  - Import data in text file into database using specified template and configuration

=head1 VERSION

  0.1

=head1 DESCRIPTION


  $Id: importData.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=back

=head1 OPTIONS

=over

=item  -d[atabases] <databases>

  Database label names (separated by space)
  
=item -c[onfig] <file>

  Path to config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/hgvbase.conf'

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=cut
