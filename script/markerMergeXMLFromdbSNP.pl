#!/usr/local/bin/perl -w

# $Id: markerMergeXMLFromdbSNP.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Transform dbSNP tab-delimited merge history table to MarkerRevision XML, for 
# loading into our database.

use strict;
use warnings;

use File::Basename;
use FindBin;
use Data::Dumper;
use Data::Stag qw(:all);
use Data::Stag::XMLWriter;

use HGVbaseG2P::Core qw(init_logger open_outfile open_infile);

# Initialize configuration & logger
my $logger = initLogger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Configure commandling-option processing (see POD below the code)
use Getopt::Euclid qw( :minimal_keys );

# Adjust logging level to verbose if required
use Log::Log4perl::Level;
$ARGV{v} and $logger->level($DEBUG);

# Open up input & output files
my $infh   = openInfile($ARGV{infile});
my $outfn = "mergehistory" . ($ARGV{build} and "_$ARGV{build}") . ".xml.gz";
$logger->info("Opening input file $ARGV{infile}, writing XML to output file $outfn");
my $outfh = openOutfile($outfn);

my $writer = Data::Stag::XMLWriter->new(-fh => $outfh);
$writer->start_event('top');
my %stats;
$ARGV{build} =~ s/^b//;
while(<$infh>) {
    #print;
    $stats{rscount}++;
    $stats{rscount} % 100 == 0 and print STDERR "\r$stats{rscount} SNPs processed";
    chomp;
    my ($rshigh, $rslow, $build, $orient, $created, $updated, $rscurrent, $orient2current) = split /\t/;
    if($ARGV{build} && $build != $ARGV{build}) {
	$logger->debug("Only build $ARGV{build} requested, so skipping line $_");
	$stats{'Skipped, wrong build'}++;
	next;
    }
    $stats{'Total SNPs to XML'}++;
    my $strandflip = $orient2current == 1 ?  [StrandFlipped => ''] : [];
    $updated =~ s/\s.+$//;
    my $markernode = stag_nodify(
[Marker => [
  [ Source               => 'dbSNP' ],
  [ LocalID              => 'rs'.$rshigh ],
  [ Status               => 'dead'],
  [ TimeLastTouched      => $updated],
  [MarkerRevision => [
    [ Comment       => 'Merge-info imported from RsMergeArch (dbSNP tabledump)'],
    [ ChangeType    => 'Merging' ],
    [ ChangeTrigger => 'b'.$build ],
    $strandflip,
    [ ReplacedByMarker => [
      [Marker => [
        [ Source               => 'dbSNP' ],
        [ LocalID              => 'rs'.$rscurrent ],
        #[ TimeLastTouched      => $updated],
      ]]
    ]]
  ]]
]]);
    $logger->is_debug and $logger->debug($markernode->itext);

    $markernode->events($writer);
}
$writer->end_event('top');

$logger->info(Dumper(\%stats));

exit 0;

__END__


=head1 NAME

  - Transform dbSNP's merge history file into XML for loading into our database

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script takes dbSNP's RsMergeArch.bcp.gz file and transforms each merge-event into
  a MarkerRevision XML-node. The resulting XML-files is loaded into the HGVBaseG2P master
  database, effectively duplicating dbSNP's merge-history into our database so we can use
  it for lookups if people give us rs#'s for markers that are no longer in dbSNP.

  $Id: markerMergeXMLFromdbSNP.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <infile>

  Tab-delimited file to process (RsMergeArch.bcp.gz from dbSNP's FTP-site at the time of writing)

=back

=head1 OPTIONS

=over

=item -b[uild] <build>

  Take only merge-events that happened in this build (default is to take the entire file)

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut

