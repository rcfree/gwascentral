#!/usr/bin/perl

# $Id: updateLegacyMarkers.pl 1515 2010-07-13 13:52:11Z rcf8 $

# Find in the Marker database all 'legacy' markers, which are no longer 
# present in the source database, and flag them as 'dead'. 

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper qw(Dumper);
use Data::Stag qw(:all);
use Getopt::Euclid qw( :minimal_keys );
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);
use Log::Log4perl::Level;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;

use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::MarkerImport::Marker;


my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));
$ARGV{verbose} and $logger->level($DEBUG);

my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;

if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}


$logger->debug("Initializing connection to database"); 
my $mimport = HGVbaseG2P::MarkerImport::Marker->new(conf_file => $ARGV{c},
						       dsn       => $dsn,
						       user      => $dbuser, 
						       passw     => $dbpassw,
                               source	  => $ARGV{source},
						       ); 
$mimport->marker_dbh->storage->dbh->{'AutoCommit'} = 0;

$logger->info("Retrieving markers from source ".$ARGV{source}." (".$ARGV{label}.") with timestamp < '". $ARGV{time} . "'");
$mimport->update_legacy_markers($ARGV{time}, $ARGV{label});

if($ARGV{n}) {
    $logger->info("No errors encountered during load, but in dry-run mode so NOT committing database transaction");
    $mimport->marker_dbh->storage->dbh->rollback();
}
else {
    $logger->info("No errors encountered during load, committing clean database transaction");
    $mimport->marker_dbh->storage->dbh->commit();
}

exit 0; 

#------------------------------------
# END OF MAIN SCRIPT
#------------------------------------



__END__

=head1 NAME

  - Produce 'legacy' marker list for the specified marker datasource
    
=head1 VERSION

  0.1

=head1 DESCRIPTION

  This tool queries the Marker database for all marker entries from the specified datasource
  with a timestamp older than that of that of the last marker-import job. The inference is
  as follows: given that these markers are not in the original source database anymore, they
  have been deleted for some reason or the other. Notably, these markers are NOT deleted in
  our database, but instead flagged as 'dead' and kept for posterity.

  Importantly, the marker  data output files from the processing step *must* be loaded into
  the Marker database *before* this script is run, so the timestamp is updated for new, 
  changed and unchanged marker entries.

  	
=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item -t[ime] [=] <time> 

  State the date to use for the query. Usually needs to be merely the day that the import-job
  was run (e.g. '2008-01-22'), but can be in full datetime format (e.g. '2008-05-09 12:33:13'
  if more precision is needed, e.g. for testing purposes.

=item -s[ource] [=] <source> 
  
  The name of the marker datasource (e.g. dbSNP)
  
=item -label [=] <label> 

  Label or name for the specific marker datasource build to be processed (e.g. 'b129' for dbSNP)
  
=back

=head1 OPTIONS
   
=over

=item -n 

  Run in 'dry-run' mode w/o writing anything to the database. This will go through the entire
  data processing procedure and do all database operations, but will not commit the transaction 
  at the end. Useful for debugging.

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use
	
=item -v[erbose]

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>, Pallavi Sarmah <pallabi2k@gmail.com>

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbaseG2P project

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: updateLegacyMarkers.pl 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut
