#!/usr/local/bin/perl -w

# $Id: scheduleJobs.pl 1445 2010-05-26 14:23:50Z rcf8 $

use strict;
use warnings;

use Schedule::Depend;
use File::Basename;
use File::Path;

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

use Data::Dumper; print Dumper(%ARGV);

# Initialize logger
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init( { level  => $ARGV{verbose} ? $DEBUG : $INFO,,
			    layout => "%L:%p> %m%n",			    
			    file => 'STDERR' } );



my $jobs;
my $sched_dep ;
my $max = $ARGV{max} || 2;

$ARGV{cmd} or die "Need cmd as -cmd or -c argument ";
my @input_files = @{$ARGV{input_files}};
@input_files == 0 and die "No input files provided in ARGV, aborting";

INFO("Running cmd '$ARGV{cmd}' on ",scalar(@input_files), " input files, max $max jobs concurrently");
foreach(@input_files){
    my $name = basename($_);
    $jobs .= qq{$name = $ARGV{cmd} $_\n};
    $sched_dep .= " $name";
}

my $sched = <<EOF;

maxjob % $max
done = echo Finished with all jobs at `date`;

$jobs

EOF
;

$sched .= "done : $sched_dep\n";

-d 'log' || mkpath('log',1,0775);
-d 'pid' || mkpath('pid',1,0775);

my $que = Schedule::Depend->prepare( sched   => $sched,
				     rundir  => '.',
				     logdir  => 'log',
				     rundir  => 'pid');

$que->validate;

DEBUG("Running jobs");
$que->execute;

INFO("Done");

__END__


=head1 NAME

  - Run commandline-jobs in parallel

=head1 VERSION

  0.2

=head1 DESCRIPTION

  This script uses the Schedule::Depend module to run a given
  commandline in parallel, on the provided input files. log/
  and pid/ directories are created in the current directory
  and can be monitored for job status and STDOUT/STDERR output.
  (Any output files from the command are the user's responsibility).

    In in principle, any command that takes one or more input
  files via @ARGV can be run in this simple fashion.

  $Id: scheduleJobs.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item -c[md] <cmd>

  Commandline to run, without an input-file list (see below)

=item <input_files>

  One or more input files to run commandline on

=for Euclid:
  repeatable  
    
=back

=head1 OPTIONS

=over

=item -m[ax] <max>

  Maximum number of jobs to run concurrently (defaults to 2x)

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
