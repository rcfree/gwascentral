#!/usr/local/bin/perl -w

# $Id: phenoXMLFromdbGaP.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Extract dbGaP group-level phenotype from variable summary XML-files

use strict;
use warnings;


use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Data::Stag qw(:all);
use Data::Stag::XMLWriter;
use Data::Dumper;
use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::FileParser;


# Configure commandline-interface (from POD section below code)
use Getopt::Euclid qw( :minimal_keys );

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Initialize XML-writer
my $writer = Data::Stag::XMLWriter->new(-fh => \*STDOUT);

# Define regexp which determine the variables we want to incorporate
my $VARS2TAKE = join('|', qw(rnucbase lnucbase rnucscore lnucscore rcorbase lcorbase rcorscore lcorscore rPSCbase lPSCbase cataract rnuc lnuc nuc lcor rcor cor rpsc lpsc psc agequal agerand agephot agevis agecont ZZRCAMDC trt height\d+ weight\d+ bmi\d+ syst\d+ dias\d+ diab\d+ cancer\d+ angina\d+ amdstat));

my %stats;
my $h = Data::Stag->makehandler(variable => sub {
    my ($handler, $node) = @_;
    my $atts = $node->get('@');
    my $name = $atts->get('name');
    return if $name !~ /\A($VARS2TAKE)\Z/xms; # Only take certain entries
    my $desc = $node->get('description');
    warn "var name '$name': $desc\n";

    # Create phenotype method entry
    my $phenmethod_node = stag_nodify([
PhenotypeMethod => [
  [MethodName => $name],
  [Details    => $desc],
]]);

    # Create phenotype values for each group that they provide stats for
    foreach my $stat_node_path(qw(total/stats cases/stats controls/stats)) {
	my $stat_node = $node->get($stat_node_path);
	if($stat_node) {
	    my ($phenval_node, $vartype) = transformStat2Phenvalue($phenmethod_node, $stat_node);
	    my ($panelname) = $stat_node_path =~ m|\A(\w+)/stats|xms;
	    $phenval_node->set('SamplepanelID', $panelname); # need to fix for proper lookup!
	    $phenmethod_node->addchild($phenval_node);
	    if(my $vartype_existing = $phenmethod_node->get('VariableType')) {
		# Check if we have inconsistent variable types for this variable. We should
		# never get to this stage if everything is OK
		die if $vartype_existing ne $vartype;	    
	    }
	    else {
		$phenmethod_node->set('VariableType',$vartype);
	    }
	}
	else {
	    warn "No statnode for path $stat_node_path";
	    next;
	}
    }
    warn $phenmethod_node->itext,"\n";
    $phenmethod_node->events($writer);
    
}
				);

# Set up the required XML-bits at the top
$writer->start_event('top');
# ToDo! <Study> container, see t/data/study.xml for examples
# ToDo! <Samplepanel> lookup, see t/data/study_phenotypes.xml for examples

# Set up & do the actual parser
my $parser = HGVbaseG2P::FileParser->new(log4perl_conf => './conf/log4perl.conf');
$logger->info("Parsing file $ARGV{varsummary_xmlfile}");
$parser->parseFile(filename => $ARGV{varsummary_xmlfile},
		   filetype => 'xml',
		   handler  => $h);
$writer->end_event('top');

exit 0;

### END OF MAIN PROGRAM ###


sub transformStat2Phenvalue {
    my ($phenmethod_node, $stat_node) = @_;

    my $stat_atts = $stat_node->get('stat')->get('@');

    # Set up the initial phenotype value node and its attributes common to all variable types
    my $phenval_node = stag_nodify([
PhenotypeValue => [
  [NumberOfIndividuals => $stat_atts->get('n')],
]]);
    my $vartype;

    # Handle continuous variable
    if(my $mean = $stat_atts->get('mean')) {
	#  <stat n="530" nulls="70" mean="172.2" sd="34.36" median="170" min="105" max="334"/>
	#    <example count="14">180</example>
	$vartype = 'Continous';
	my ($sd, $median, $min,$max) =
	    $stat_atts->getlist(qw(sd median min max));
	warn "Got continous variable with mean $mean stdev=$sd\n";	
	$phenval_node->set('Value', $mean);
	$phenval_node->set('IsMean', 'yes');
	$phenval_node->set('StdDev', $sd);

    }
    elsif(my $mean_count = $stat_atts->get('mean_count')) {
								    
    # Handle discrete variable
    #    <stat n="600" nulls="0" mean_count="60" sd="47" median_count="41" min_count="19" max_count="144"/>
    #      <example count="144">Absent</example>
       }
    return ($phenval_node, $vartype);
}


__END__


=head1 NAME

  - Extra phenotype information from dbGaP variable summary XML-files

=head1 VERSION

  0.1

=head1 DESCRIPTION

  [more details about the process]

  $Id: phenoXMLFromdbGaP.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <varsummary_xmlfile>

  Path to variable summary file to process
    
=back

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
