#!perl

# $Id: calculateMarkersImported.pl 1528 2010-09-16 13:48:46Z rcf8 $

# create Browser marker data table and binned data from Study/Marker database

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use GwasCentral::DataSource::Study;
use GwasCentral::DataSource::Browser;
use GwasCentral::Base qw(load_config);
use GwasCentral::Browser::Util qw(neglog);
use Log::Log4perl qw(:easy);
use Math::Round qw(nearest_floor);

Log::Log4perl->easy_init($INFO);

use Data::Dumper qw(Dumper);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

my $conf_file  = $ARGV{c};

my %config     = load_config($conf_file);
my $SUMMARY_NO = 100000;

my $study_db   = GwasCentral::DataSource::Study->new(
	{ conf_file=>\%config }
);

my $browser_db = GwasCentral::DataSource::Browser->new( { conf_file => \%config } );
my @studies = $study_db->dbh->resultset('Study')->all;

foreach my $study(@studies) {
	my @rset_ids=();
	
	foreach my $experiment($study->experiments->all) {
		my $count = $experiment->usedmarkersets->count;
		print $experiment->identifier.":".$count."\n";
		$experiment->update({ totalmarkersimported => $count});
		
		@rset_ids = (@rset_ids,map { $_->resultsetid } $experiment->resultsets->all);
	}
	my $top = $study_db->dbh->resultset('Significance')->search({
			resultsetid=>{-in=>\@rset_ids}, unadjustedpvalue=>{'>'=>0}
		},{ page=>1, rows=>1, order_by=>'unadjustedpvalue'})->single;
	my $neglog='0';
	if ($top) {
		#print Dumper({$top->get_columns()});
		my $neglog = neglog($top->unadjustedpvalue);
		$neglog>$config{max_threshold} and $neglog=$config{max_threshold};
		$neglog=nearest_floor(1,$neglog);
		$neglog==0 and $neglog='0';
		print "study:".$study->identifier." = ".$neglog."\n";
		$study->update({ significancelevel=>$neglog });
	}
	
	
	
}
$study_db->dbh->storage->dbh->do("UPDATE Experiment SET GenotypedBundle = (SELECT GROUP_CONCAT(Platform.PlatformName SEPARATOR ';') FROM Platform NATURAL JOIN ExperimentPlatform WHERE Experiment.ExperimentID = ExperimentPlatform.ExperimentID)");


__END__


=head1 NAME

  - Calculate markers imported for each experiment info Study DB

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script calculates markers imported for eache experiment in Study DB
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onfigfile] <file>
  
  GC config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/main.conf'
         
=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
