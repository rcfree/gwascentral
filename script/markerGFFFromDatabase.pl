#!/usr/local/bin/perl -w

# $Id: markerGFFFromDatabase.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Export marker data from database as GFF, for loading into the feature database

use strict;
use warnings;
use English qw( -no_match_vars );

use Data::Dumper;
use FindBin;
use File::Basename;

use HGVbaseG2P::Core qw(init_logger open_outfile open_infile);
use HGVbaseG2P::Database;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# Initialize configuration & logger
my $logger = initLogger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Configure commandling-option processing (see POD below the code)
use Getopt::Euclid qw( :minimal_keys );

# Adjust logging level to verbose if required
use Log::Log4perl::Level;
$ARGV{v} and $logger->level($DEBUG);

# Build arguments to pass to database module. 
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

# Initialize database handle
$logger->info("Initializing database handle");
my $db;
$db = HGVbaseG2P::Database->new(conf_file => $ARGV{c},
				   dsn       => $dsn,
				   user      => $dbuser, 
				   passw     => $dbpassw) || die "Could not initialize database handle";


my @chroms;
if($ARGV{chrom} eq 'all') {
    @chroms = (1..22,'X','Y','MT');
}
else {
    @chroms = split(',', $ARGV{chrom});
}

# Prepare SQL-query. Get only markers that are NOT flagged as dead, and their alleles that are 
# also NOT flagged as dead.
my $sth = $db->dbh->prepare(qq{
select MarkerID, 
       Source,
       LocalID,
       VariationType,
       Upstream30bp, 
       Downstream30bp, 
       Chr,
       Start,
       Stop,
       Strand,
       AssemblyName,
       group_concat(AlleleSeq) as Alleleset
from Marker 
     left join Allele using(AutoMarkerID) 
     join MarkerCoord using(AutoMarkerID)
where Marker.Status = 'active'
      and Allele.Status = 'active'
      and AssemblyName in ('reference', 'PAR','mitochondrial genome')
      and Chr = ?
group by MarkerID, Source, LocalID,VariationType, Upstream30bp, Downstream30bp, Chr, Start, Stop, Strand, AssemblyName });

my %stats;
$logger->info("Dumping marker features chromosomes ", join(',', @chroms));
foreach my $chrom(@chroms) {    
    my $gff_filename = "markers_HGVbaseG2P_chr$chrom\.gff3.gz";
    my $outfh = openOutfile($gff_filename);
    $logger->info("Printing marker GFF to $gff_filename");
    print $outfh "##gff-version 3\n";

    # Execute query for this chromosome
    $sth->execute($chrom);

    while(my $r = $sth->fetchrow_hashref()) {
	$stats{markercount}++;
	$stats{'Chr'.$chrom}->{markercount}++;
	$logger->debug("#$stats{markercount} $r->{MarkerID}");
	$stats{markercount} % 1000 == 0 and $logger->info("Total $stats{markercount} markers printed as GFF3");
	
	my $att_name   = 'Name='.$r->{MarkerID};
	my $att_dbxref = 'Dbxref='.$r->{Source}.':'.$r->{LocalID};
	my $att_alias  = 'Alias='.$r->{LocalID};
	
	# Prepare marker alleles
	my $att_alleles = '';
	if(my $alleleset = $r->{Alleleset}) {
	    my @alleles;
	    foreach my $alleleseq(sort(split(',', $alleleset))) {
		my $alleleseq_length = length($alleleseq);
		if($alleleseq_length > 20) {
		    push(@alleles, '['.unit_label($alleleseq_length).' seq]');
		}
		else {
		    push(@alleles, $alleleseq);
		}
	    }
	    $att_alleles .= 'alleles='.substr($r->{'Upstream30bp'}, -10, 10)
	                              .'('.join('/',sort(@alleles)). ')'
				      .substr($r->{'Downstream30bp'}, 0, 10);
	}
    
	my $strand = $r->{Strand} == 1   ? '+' 
	           : $r->{Strand} == -1  ? '-' 
		   :                       '.';
	print $outfh join("\t",
			  'Chr'.$r->{'Chr'},
			  'HGVbaseG2P',
			  $r->{'VariationType'},
			  $r->{'Start'},
			  $r->{'Stop'},
			  '.',
			  $strand,
			  '.',
			  $att_name.';'.$att_alias.';'.$att_alleles
			  #$att_name.';'.$att_alias.';'.$att_dbxref.';'.$att_alleles
			  ), "\n";
    }
}
$db->disconnect();
$logger->info(Dumper(\%stats));

exit 0;



# convert bp into nice Mb/Kb units (borrowed from GBrowse CGI-script!!)
sub unit_label {
    my $value = shift;
    my $unit     =  'bp';
    my $divider  =  1;
    $value /= $divider;
    my $abs = abs($value);
    
    my $label;
    $label = $abs >= 1e9  ? sprintf("%.4g G%s",$value/1e9,$unit)
	: $abs >= 1e6  ? sprintf("%.4g M%s",$value/1e6,$unit)
		: $abs >= 1e3  ? sprintf("%.4g k%s",$value/1e3,$unit)
		: $abs >= 1    ? sprintf("%.4g %s", $value,    $unit)
		: $abs >= 1e-2 ? sprintf("%.4g c%s",$value*100,$unit)
		: $abs >= 1e-3 ? sprintf("%.4g m%s",$value*1e3,$unit)
		: $abs >= 1e-6 ? sprintf("%.4g u%s",$value*1e6,$unit)
		: $abs >= 1e-9 ? sprintf("%.4g n%s",$value*1e9,$unit)
		: sprintf("%.4g p%s",$value*1e12,$unit);
    if (wantarray) {
	return split ' ',$label;
    } else {
	return $label;
    }
}    

__END__


=head1 NAME

  - Export marker data as GFF

=head1 VERSION

  0.3

=head1 DESCRIPTION

  Connects to master database and executes SQL-query to extract
  marker and genomic coordinates as GFF3, for loading into a Bioperl
  feature database (primarily Bio::DB::SeqFeature::Store).
    

  $Id: markerGFFFromDatabase.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item -chr[om] <chrom>

  Dump only data for this chromosome or chromosomes (multiple values allowed, seperated by 
  a comma). Use ' -chr all ' to dump all chromosomes in one go.

=back
    
=head1 OPTIONS

=over

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut

