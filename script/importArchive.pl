use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Archive::Extract;
use Getopt::Euclid qw(:minimal_keys);
use File::Spec;
use Data::Dumper qw(Dumper);
use XML::Simple;
use Log::Log4perl qw(:easy);
use HGVbaseG2P::Core qw(load_config);
use HGVbaseG2P::DataImport::Core;
use HGVbaseG2P::Browser::Util qw(neglog);
use Math::Round qw(nearest_floor);
use HGVbaseG2P::Browser::Creator;
use HGVbaseG2P::Database::Study;
use GwasCentral::AccessControl::GAS;
use GwasCentral::Connect;

my $archive = $ARGV{archive};
my $conf_file = File::Spec->rel2abs($ARGV{c});
my $script_location = "$FindBin::Bin/../";

Log::Log4perl->easy_init();
my $extracted = 0;
my $path;

if (-d $archive) {
	INFO "Archive $archive is directory";
	$path = File::Spec->rel2abs($archive);
}
else {
	INFO "Archive $archive is file. Unzip it to /tmp";
	my $ae = Archive::Extract->new( archive => $archive );
	my $ok = $ae->extract( to => '/tmp' ) or die $ae->error;
	$path = $ae->extract_path;
	$extracted = 1;
}
INFO "Archive path is $path";
INFO "Config file is $conf_file";
die "No study.xml metadata file provided in archive!" if !-e "$path/study.xml";
my $xml = XMLin("$path/study.xml");
my $study_ident = $xml->{Study}->{StudyInfo}->{Identifier};
my $study_db = HGVbaseG2P::Database::Study->new({conf_file=>$conf_file});

if ($ARGV{d}) {
	INFO "Removing all data from study $study_ident";
	my $study = $study_db->get_study_by_identifier($study_ident);
	goto no_study if !$study;
	
	my @experiments = $study->experiments;

	foreach my $exp(@experiments) {
		$exp->usedmarkersets->delete_all;
		$exp->experimentassayedpanels->delete_all;
		
		
		foreach my $rset($exp->resultsets) {
			$rset->delete;
		}
		
		$exp->delete;
	}
	
	$study->studysamplepanels->delete_all;
	$study->assayedpanels->delete_all;
	$study->phenotypemethods->delete_all;
	$study->studycitations->delete_all;
	$study->studyhotlinks->delete_all;
	$study->delete;
}

no_study:

INFO "Importing study.xml file using study identifier $study_ident";

chdir $script_location;
my $study_import = "perl script/storeXML.pl $path/study.xml -c $conf_file";
INFO "study_import:$study_import";
my $fail = system "$study_import";
$fail and die "Metadata import failed";

chdir $path;
my @template_files = <*.tpl>;
@template_files = map { $path."/".$_ } @template_files;
INFO "template_files:".Dumper(\@template_files);
chdir $script_location;
foreach my $template_file(@template_files) {
	$template_file = $template_file;
	my $data_file = $template_file;
	$data_file =~ s/\.tpl/\.txt/;
	my %template = load_config($template_file);
	#replace/put study from study.xml into template
	$template{study_identifier}=$study_ident;
	my $di = HGVbaseG2P::DataImport::Core->new({conf_file => $conf_file,
				 argv      => \%ARGV,
				 template_file => \%template,
	});
	
	# Process source input file
	INFO("Processing input file $data_file");
	$di->process_file($data_file);
	
	INFO("lines successful:\n".$di->marker_success);
	
	my $marker_stats  = $di->marker_failed;
	INFO("lines failed:\n".scalar(@{$marker_stats}));
	
	INFO("stats/errors:\n");
	INFO("KEY: n - no marker found, s - strand not available, r - replacement dead, m - merged, d - deleted, f - flipped, a - no-match alleles");
	foreach my $failed(@{$marker_stats}) {
		INFO $failed->line_no.": ".$failed->line->{accession}."\t".join("",map { "$_" } keys %{$failed->status})."\n";
	}
}

#update marker counts, sig level etc.
INFO "Updating marker counts, significance level and genotype bundles";
my %config = %{$study_db->config};
my $study = $study_db->get_study_by_identifier($study_ident);
my @rset_ids = ();
foreach my $experiment($study->experiments->all) {
	my $count = $experiment->usedmarkersets->count;
	INFO("Number of markers in experiment ". $experiment->identifier.":".$count);
	$experiment->update({ totalmarkersimported => $count});
	
	@rset_ids = (@rset_ids,map { $_->resultsetid } $experiment->resultsets->all);
}
my $top = $study_db->dbh->resultset('Significance')->search({
		resultsetid=>{-in=>\@rset_ids}, unadjustedpvalue=>{'>'=>0}
	},{ page=>1, rows=>1, order_by=>'unadjustedpvalue'})->single;
my $neglog='0';
if ($top) {
	#print Dumper({$top->get_columns()});
	my $neglog = neglog($top->unadjustedpvalue);
	$neglog>$config{max_threshold} and $neglog=$config{max_threshold};
	$neglog=nearest_floor(1,$neglog);
	$neglog==0 and $neglog='0';
	INFO("Significance level in study:".$study->identifier." = ".$neglog);
	$study->update({ significancelevel=>$neglog });
}
$study_db->dbh->storage->dbh->do("UPDATE Experiment SET GenotypedBundle = (SELECT GROUP_CONCAT(BundleLoci.BundleName SEPARATOR ';') FROM BundleLoci NATURAL JOIN GenotypedBundle WHERE Experiment.ExperimentID = GenotypedBundle.ExperimentID and Experiment.StudyID = ".$study->id.")");

#update browser database
INFO "Creating browser data";
my $study_no = $study_ident;
$study_no =~ s/HGVST//;

my $creator = HGVbaseG2P::Browser::Creator->new({
	conf_file=>$conf_file,
	studies=>$study_no,
	calc_region=>1,
	calc_genome=>1,
	copy_markers=>1,
	marker_sigs=>1,
});

#$creator->create();

my $ac = GwasCentral::AccessControl::GAS->new({
	conf_file => $conf_file,
	
});
my $connect = GwasCentral::Connect->new({
	conf_file => $conf_file,
});
$ac->identity($ac->config->{GAS}->{admin_user});
$ac->connect($connect);

my $study_gcid = $ac->config->{Connect}->{label}.".study.".$study_ident;
$ac->create_resource_as_admin($study_gcid);

foreach my $experiment($study->experiments->all) {
	my $exp_gcid = $ac->config->{Connect}->{label}.".experiment.".$experiment->identifier;
	$ac->create_resource_as_admin($exp_gcid);
}

$extracted and unlink $path;


__END__


=head1 NAME

  - Load HGVbase XML-files into database

=head1 VERSION

  0.3

=head1 DESCRIPTION

  This script uses the Data::Stag / DBIx::DBStag framework to parse XML-data 
  corresponding to the HGVbase relational schema and load the data into the
  database. If top-level <Study> and second-level <Experiment> are present, 
  these will be stored first and child-elements linked to their parents one
  by one, to avoid loading the entire container-elements into memory.

  The single input XML-file will constitute a transaction, so either all of
  the data in the file will be inserted & committed, OR if there is an error
  during the load, the whole transaction will be rolled back and aborted.

  PS DBI_DSN and/or DBI_USER and/or DBI_PASS will be used if present, a la DBI

  $Id: storeXML.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <archive>

  Path to GWAS Central archive file or directory.
    
=back

=head1 OPTIONS

=over

=item -d[elete]

  Delete all traces of study from database before loading

=item -c[onfig] <config>

  Path to config file to use

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut