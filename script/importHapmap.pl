#!/usr/local/bin/perl -w

# $Id: importHapmap.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Import HapMap pair-wise and SNP data from downloaded files

use strict;
use warnings;
use English qw( -no_match_vars );

use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Data::Dumper;
use HGVbaseG2P::Core qw(init_logger load_module);
use HGVbaseG2P::Database::Hapmap;
use HGVbaseG2P::Database::Feature;
use Bio::DB::SeqFeature::Store::GFF3Loader;
use Benchmark;
use List::MoreUtils qw(any);

use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code (where the options are specified)
use Getopt::Euclid qw( :minimal_keys );

# Initialize configuration & logger
my $log = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));
my $tmpdir = "/srv/data/projects/hgvbaseg2p/v4_data";
my $ld_location = $ARGV{l};
my $ld_temp_file = ($ld_location || '/tmp')."/ld_data.txt";

my $snp_location = $ARGV{s};
my $gff_temp_file = ($snp_location || '/tmp')."/snp_data.gff";
my $db_name = "HGVbaseG2P_hapmap";
my @panels= qw(CHB);

my $ref = $ARGV{r} ? $ARGV{r}."_*" : '';
my $t1_all = Benchmark->new;

my $pack = $ARGV{p};
my $hapmap_db = GwasCentral::DataSource::Hapmap->new({ conf_file => $ARGV{c} });
	
if ($ld_location) {
	load_ld_data();
	
	
}

if ($snp_location) {
	load_snp_data();
}

if ($pack) {
	my $database = $ARGV{d};
	
	$log->info("Optimise tables in HapMap database");
	chdir "/srv/data/mysql/HGVbaseG2P_hapmap";
	my $dbh = $hapmap_db->dbh->storage->dbh;
	my $sth = $dbh->table_info( '', '', "%", '' );
	my @tables = map { $_->[2] } @{$sth->fetchall_arrayref};
	$sth->finish;
	foreach my $table(@tables) {
		$log->info("lock table $table");
		$dbh->do("lock table $table write");
		$log->info("flush table $table");
		$dbh->do("flush table $table");
		$log->info("check table $table");
		system "myisamchk --check --fast --update-state $table";
		$log->info("pack table $table");
		system "myisampack -f --tmpdir=$tmpdir $table ";
		$log->info("reindex table $table");
		system "myisamchk -raqS --tmpdir=$tmpdir $table";
		$dbh->do("unlock tables");
	}
}

my $t2_all = Benchmark->new;

my $td_all = timediff($t2_all, $t1_all);

$log->info("Completed importing data - took ".timestr($td_all));

sub load_ld_data {	
	my %keys_disabled_for=();
		
	foreach my $panel(@panels) {
		my @files = glob($ld_location."/*".$ref."_$panel*.gz");
		foreach my $file(@files) {
			my $t1 = Benchmark->new;
			$file =~ /.+(chr\d+).+/;
			my $chr = $1;
			next if !$chr;
			
			if (!$keys_disabled_for{$chr}) {
				$hapmap_db->create_ld_table($chr);
				$hapmap_db->disable_ld_table_keys($chr);
				$keys_disabled_for{$chr}=1;
			}
			
			my $trimmed_file = $file;
			$trimmed_file =~ s/$ld_location\///;
			$log->info("uncompress '$trimmed_file'");
			system "gunzip -c -f $file > $ld_temp_file";
			chmod 0755, $ld_temp_file;
			$log->info("import file '$trimmed_file'");
			$hapmap_db->import_ld_file($ld_temp_file,$chr);
			unlink $ld_temp_file;
			my $t2 = Benchmark->new;
			my $td = timediff($t2, $t1);
			$log->info("time taken to import LD file:".timestr($td));
			
		}
	}
	foreach my $chr(keys %keys_disabled_for) {
		$hapmap_db->enable_ld_table_keys($chr);
	}
}

sub load_snp_data {
	my $feature_db = HGVbaseG2P::Database::Feature->new({ conf_file => $ARGV{c} });
	my $loader = Bio::DB::SeqFeature::Store::GFF3Loader->new(-store    => $feature_db->dbh,
							   -verbose  => 1,
							   -fast     => 1);
	$loader->create_load_data;
	foreach my $panel(@panels) {
		my @files = glob($snp_location."/*".$ref."_$panel*.gz");
		
		foreach my $file(@files) {
			my $t1 = Benchmark->new;
			my $trimmed_file = $file;
			$trimmed_file =~ s/$snp_location\///;
			$log->info("import file '$trimmed_file'");
			system "gunzip -c -f $file > $gff_temp_file";
			chmod 0755, $gff_temp_file;
			#$feature_db->import_gff_file($gff_temp_file);
			
			open GFFIN, $gff_temp_file;
						
			my $old_feature = 0;
			my $new_feature = 0;
			
			while (my $line = <GFFIN>) {
				next if $line =~ /^#/;
				my @columns = split (/\s+/,$line);
				my ($id) = split(";",$columns[8]);
				my ($key,$value) = split("=",$id);
				my @features = $feature_db->dbh->get_features_by_alias($value);
				my $feature = $features[0];
	
				if (!$feature) {
					$line=~s/;$//;
					$loader->handle_feature($line);
					$new_feature++;
				}
				else {
					  local $^W = 0;
					  if (@columns > 9) { #oops, split too much due to whitespace
						  $columns[8] = join(' ',@columns[8..$#columns]);
					  }
					  
					  my ($refname,$source,$method,$start,$end, $score,$strand,$phase,$attributes) = @columns;
					  my %attrs = map { split("=",$_) } split(";",$attributes);
				      
					  if (any { $_ eq $attrs{acounts}} $feature->get_tag_values) {
					  	$log->info("feature $value not updated");
					  	next;
					  }
					  $feature->add_tag_value('acounts',$attrs{acounts});
				      $feature->add_tag_value('gtcounts', $attrs{gtcounts});
				      $feature->update;
				      $old_feature++;
				}
				
				if (($old_feature + $new_feature) % 5000 == 0) {
			    	$log->info($old_feature + $new_feature ." features added ($new_feature) or updated ($old_feature)");
			    }
				
			}
			$log->info("$new_feature features added, $old_feature features updated");
			close GFFIN;
			my $t2 = Benchmark->new;
			my $td = timediff($t2, $t1);
			$log->info("time taken to import GFF file:".timestr($td));
		}
	}
}

exit 0;

__END__


=head1 NAME

  - Import data in text file into database using specified template and configuration

=head1 VERSION

  0.1

=head1 DESCRIPTION


  $Id: importData.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=back

=head1 OPTIONS

=over

=item  -l[dloc] <ldlocation>

  Directory containing LD files

=item  -s[nploc] <snplocation>

  Directory containing SNP Genotype files

=item -r[ef] <reference>

  Limit to specific reference chromosome in file
 
=item -p[ack]

  Use myisampack to compress HapMap database
  
=item -c[onfig] <file>

  Path to config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/hgvbase.conf'

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=cut
