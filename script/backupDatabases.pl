#!/usr/local/bin/perl -w 

# $Id: backupDatabases.pl 1445 2010-05-26 14:23:50Z rcf8 $

use strict;
use warnings;
use File::Basename;
use File::Path;
use Number::Format qw/:subs/;
use POSIX qw(strftime);
use Proc::PID::File;
use Config::General;
use Cwd;

# NB this module handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );


my $self = basename($0);
my $self_dir = dirname($0);
$self_dir = getcwd . '/' . $self_dir unless $self_dir =~ /^\//;

# Inititalize Log4perl
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init( { level  => $ARGV{v} ? $DEBUG : $INFO,
                layout => "%L:%p> %m%n",
                file => 'STDERR' } );

# Make sure that an instance of this program is not running already
die "ERROR: instance already running!" if Proc::PID::File->running(dir=>'/var/tmp', verify=>1);

# Load configuration profile from file
my %CONFIG;
if(exists $ARGV{c} && -f $ARGV{c}) {
    DEBUG("Loading config options from $ARGV{c}");
    my $config = new Config::General($ARGV{c});# || die("ERROR: cannot load config file $conf_file: $!");
    %CONFIG = $config->getall();
}

# Load defaults from config file if not provided on commandline
$ARGV{dblist}     ||= $CONFIG{dbs2backup} || die "Need comma-seperated list of db-names to backup";
$ARGV{output_dir} ||= $CONFIG{backup_dir} || die "Need -o|output_dir option, for path to output dir";

# Prepare backup directory in the proper place
-d $ARGV{output_dir} || die "Output dir $ARGV{output_dir} does not exist";
my $datestamp   = strftime("%Y-%m-%d",localtime(time));
my $backup_dir  = $ARGV{output_dir} . '/' . $datestamp;
if(!-d $backup_dir) {
    DEBUG("Creating backup dir $backup_dir");
    mkpath($backup_dir,0,0770);
    chdir($backup_dir);
}

# For each db, run the backup command to create the dump
my $err = 0;
$ARGV{dblist} =~ s/,/ /g;
my $btime = time();
DB:
foreach my $dbname(split(/\s+/, $ARGV{dblist})) {
    my $backup_file = $backup_dir. "/$dbname\.sql.gz";
    if(-f $backup_file)
    {
        unless($ARGV{force})
        {
            WARN("backup file $backup_file already exists, skipping this database (NB see -f option)");
            next DB;
        }
    }
    
    my $cmd = "mysqldump --opt --single-transaction --master-data=2  --quick   $dbname | gzip -c > $backup_file ";
    DEBUG("For db $dbname, running backup command $cmd");
    my $cmd_returncode = system($cmd);
    if($cmd_returncode != 0) {
        ERROR("backup cmd returned non-zero($cmd_returncode)");
        $err = 1;
    }
}
my $time2run = format_number((time() - $btime)/60);
if($err) {
    INFO("Backup to $backup_dir had some errors");
    exit $err;
}
else {
    INFO("Backup to $backup_dir completed in $time2run minutes");
}
exit 0;

## END OF MAIN PROGRAM ## 

__END__

=head1 NAME
 
  - Back up multiple databases

=head1 VERSION

  0.3

=head1 DESCRIPTION

  Backs up all databases listed in the config file to a datestamped
directory. If the directory already exists, the script will abort
unless the -f option is used, to force deletion of the directory
and its contents. The script is intended to be run via cron every
few days, to ensure we have a running series of database archives
to revert to if disaster strikes

  $Id: backupDatabases.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over


=back

=head1 OPTIONS

=over

=item <dblist>

  Comma-seperated list of databases to back up (overrides cfgfile)

=item -c[onfig] <config>

  Path to config file to use in place of the default one.

=item -o[utput_dir] <output_dir>

  Directory to dump databases to (overrides cfgfile)

=item -f[orce]

  Force removal of dump-directory if it exists    

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
