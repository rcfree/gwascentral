#!perl

# $Id: calculateMarkersImported.pl 1465 2010-06-07 11:11:46Z rcf8 $

# create Browser marker data table and binned data from Study/Marker database

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use HGVbaseG2P::Database::Study;
use HGVbaseG2P::Deploy;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);

use Data::Dumper qw(Dumper);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );


my $deploy = HGVbaseG2P::Deploy->new( {
	conf_file=>$ARGV{c}, 
	force => $ARGV{f},
	study => $ARGV{s},
	marker=> $ARGV{m},
	generate_config => $ARGV{g},
	optimise => $ARGV{o},
	transfer => $ARGV{t}
});
$deploy->do;


#calc markers imported
#populate gt bundle
#index for text search
#optimise database(s)


#$deploy->generate_das_config();

__END__


=head1 NAME

  - Deploy a new version of HGVbaseG2P. 

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script deploys HGVbaseG2P. Without any options it will simply index data and calculate markers imported.
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onfigfile] <file>
  
  HGVbaseG2P config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/hgvbase.conf'

=item -s[tudy]

  Deploy Study database
  
=item -m[arker]

  Deploy Marker database

=item -t[ransfer]

  Transfer database between servers
  
=item -o[ptimise]

  Optimise database(s)
  
=item -f[orce]

  Force deletion of all files in dump location
  
=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
