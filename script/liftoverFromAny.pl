#!/usr/local/bin/perl -w

# $Id: liftoverFromAny.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Process frequency data from any source and print out as XML for loading into our db

use strict;
use warnings;
use English qw( -no_match_vars );

use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib/";
use lib "$FindBin::Bin/../";
use File::Basename;
use Data::Dumper;
use HGVbaseG2P::Core qw(init_logger);
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

#sql templates
$ENV{DBSTAG_TEMPLATE_DIRS} = "./sqltemplates";

# NB this module here handles commandline-option processing, according
# to the POD documention below the code (where the options are specified)
use Getopt::Euclid qw( :minimal_keys );

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

my $infile = $ARGV{sourcefile};
my $chainfile = $ARGV{chainfile};
my $source_class = $ARGV{source};
my $conf = $ARGV{config};

# Initialize importer class
$logger->info("Initializing processing module for source $source_class");
my $importclass = 'HGVbaseG2P::PreProcess::LiftOver::Broad';
eval "require $importclass";
#print Dumper (\@INC);
my $importer = $importclass->new(conf_file => $conf,
				 source    => $source_class,
				 argv      => \%ARGV ) || die "Could not initialize importer";

# Process source input file
$logger->info("Processing input file $infile");

$importer->run_liftover($infile,$chainfile);


#my $marker_stats  = $importer->stats;
#$logger->info("stats/errors:\n".Dumper($marker_stats));
exit 0;

__END__


=head1 NAME

  - liftover the co-ordinates 

=head1 VERSION

  0.1

=head1 DESCRIPTION


  $Id: liftoverFromAny.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over
    
=item <sourcefile>

  Path to source datafile to transform

=item -s[ource] <source>

  Name of source (e.g. dbSNP, CGEMS, Broad)

=item -ch[ainfile] <chainfile>

  Path to the chain file for liftover 
    
=back

=head1 OPTIONS

=over

=item -c[onfig] <config>

  Path to config file to use
  


=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item -n

  Process data and do SQL-inserts, but do NOT commit transaction at the end

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Robert Hastings <rkh7@le.ac.uk>

=head1 BUGS

  None known, but please report any to the author
    
=cut
