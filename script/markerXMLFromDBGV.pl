#!/usr/local/bin/perl -w 

# $Id: markerXMLFromDBGV.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Reformat XRT-table from Db of Genomic Variants to HGVbase XML

use strict;
use warnings;

use Data::Stag qw(:all);
use Data::Stag::XMLWriter;
use File::Basename;

# Initialize logger
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init( { level  => $ARGV{verbose} ? $DEBUG : $INFO,
			    layout => "%L:%p> %m%n",
			    file => 'STDERR' } );

# Connect to feature database
use Bio::DB::SeqFeature::Store;
my $db = Bio::DB::SeqFeature::Store->new(-adaptor => 'DBI::mysql',
					 -dsn     => 'dbi:mysql:biodbgff_test;user=mummi;password=Dusill') || die $@;

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

# Set up XML-writer and start outputing
my $writer = Data::Stag::XMLWriter->new(-fh => \*STDOUT);
$writer->start_event('top');
$writer->start_event('dbstag_metadata');
$writer->event('map', 'SourceHotlink/Marker.HotlinkID=Hotlink.HotlinkID');
$writer->end_event('dbstag_metadata');

my %so_type_of = (CopyNumber =>         'CNV',
		  Inversion  =>          'chromosome_inversion',
		  InversionBreakpoint => 'inversion_breakpoint');

my $hotlinknode = stag_nodify(
[SourceHotlink__ => [
         ['@' => [[id=>'dbgv_hotlink'],[op=>'lookup']]],
	 [Hotlink => [
            [HotlinkLabel => 'DBGV Variation hg18'],
            [UrlPrefix    => 'http://projects.tcag.ca/variation/cgi-bin/tbrowse/xview?source=hg18&view=variation&id='],
        ]]
]]);
$hotlinknode->events($writer);

open IN, $ARGV{xrtfile} || die $!;
my $i = 0;
while(<IN>) {
	next if /VariationType/;
#head  /data/mummi/db_of_genomic_variants/variation.hg18.xrt
#    ID      VariationID     Chr     Start   Stop    Landmark        VariationType   Cytoband        PositionMb      Method  HasDup       CloseToGap      KnownGenes      Comment LiteratureReference     PubMedID        Individual      LocusID
#    Variation_0001  1       chr1    1083805 1283805 CTC-232B23      CopyNumber      1p36.33 1.1     Array CGH       Yes     No  TTLL10\tMXRA8\tCENTB5\tCPSF3L\tB3GALT6\tTAS1R3\tSDF4\tTNFRSF4\tSCNN1D\tMGC10334\tDVL1\tPUSL1\tTNFRSF18\tUBE2J2           Iafrate et al. (2004)        15286789
	my ($id,$varid,$chr, $start,$stop,$landmark,$vartype,undef,undef,$method,$hasdup,undef,undef,$comment,$authors,$pubmed,$ind,$locusid) = split(/\t/);

	$i++;
	$i % 100 == 0 and print STDERR "\r$i entries processed";

	$chr =~ s/\Achr//ix;
	my $so_term = $so_type_of{$vartype};
	my $span = $start == $stop ? '@' : '..';
	
	DEBUG("# $i: Doing structvar $id at Chr$chr:$start\..$stop");
	
	# Retrieve flanking seqs from assembly
        my $seg = $db->segment('Chr'.$chr, $start-30 => $stop+30);
            my $seq        = $seg->seq;
            my $seq_length = $seq->length();
            DEBUG("Got segment $seg, length $seq_length");
            my $upstream_seq = lc($seq->subseq(1, 30));
            my $variant_seq = uc($seq->subseq(31, $seq_length-30));
            my $downstream_seq = lc($seq->subseq($seq_length-29, $seq_length));
            #print STDERR "allele def, using nomenclature: $upstream_seq($variant_seq)$upstream_seq\n";
my $allele_node =
[Allele => [
  ['@'       => [[op =>'insert']]],
  [Upstream30bp  => $upstream_seq],
  [AlleleSeq     => $variant_seq],
  [Downstream30bp => $downstream_seq],
  [SeqType       => 'G'],
]]
;

	#   upstream seq, downstream seq, muna 30bp context
	# nnnnnnnn(X)nnnn

	my $marker_node = stag_nodify(
[Marker => [
   ['@' => [[op=>'insert']]],
   [Source  => 'DBGV'],
   [LocalID => $id],
   [VariationType => $so_term],
   #[HotlinkID => 'dbgv_hotlink'],
  [SourceHotlink => [
    [Hotlink => [
         ['@' => [[op=>'lookup']]],
            [HotlinkLabel => 'DBGV Variation hg18'],
            [UrlPrefix    => 'http://projects.tcag.ca/variation/cgi-bin/tbrowse/xview?source=hg18&view=variation&id='],
        ]]
  ]],
   $allele_node,
   [MarkerCoord => [
      ['@' => [[op=>'insert']]],
      [Chr          => $chr],
      [Start        => $start],
      [Stop         => $stop],
      [Strand       => 0],
      [Span         => $span],
      [MapWeight    => 'unique-in-contig'],
      [GenomeBuild  => $ARGV{assembly}],
      [AssemblyType => 'ref_assembly'],
      [AssemblyName => 'reference'],
		    
   ]],
]]
);
$marker_node->events($writer);
	
}    
close IN;
$writer->end_event('top');


__END__

=head1 NAME
 
  - Transform XRT dump of structural variants from Database of Genomic Variants

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script uses the Data::Stag structured-tags framework to extract marker
  information from XRT dumps from the  Database of Genomic Variants.  The output 
  XML conforms to the HGVbase database schema and is intended for direct 
  loading into the database via DBIx::DBStag. The XML is printed to STDOUT

  See also http://projects.tcag.ca/variation/download.html

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <xrtfile>

  Path to variation XRT file to transform

=item -a[ss[embly]] <assembly>

  Version of the reference sequence assembly (currently NCBI build 36 aka hg18)

=back

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
