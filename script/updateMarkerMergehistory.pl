#!/usr/local/bin/perl -w

# $Id: updateMarkerMergehistory.pl 1515 2010-07-13 13:52:11Z rcf8 $

# Process the dbSNP merge-history table, figure out which rs#'s have been 
# merged into which rs#, and populate our own MarkerRevision table accordingly.

use strict;
use warnings;

use File::Basename;
use FindBin;
use Data::Dumper;

use HGVbaseG2P::Database::Marker;
use HGVbaseG2P::Core qw(init_logger open_outfile open_infile);

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Configure commandling-option processing (see POD below the code)
use Getopt::Euclid qw( :minimal_keys );

# Adjust logging level to verbose if required
use Log::Log4perl::Level;
$ARGV{v} and $logger->level($DEBUG);

# Open up input & output files
my $infh   = open_infile($ARGV{infile});

# Connect to target database
my $markerdbh = HGVbaseG2P::Database::Marker->new({ conf_file => $ARGV{config}});
$markerdbh->storage->dbh->{'AutoCommit'} = 0;


my %stats;
$ARGV{build} =~ s/^b//;
while(<$infh>) {
    #print;
    $stats{rscount}++;
    $stats{rscount} % 100 == 0 and print STDERR "\r$stats{rscount} SNPs processed";
    chomp;

    my ($rshigh, $rslow, $build, $orient, $created, $updated, $rscurrent, $orient2current) = split /\t/;
    if($ARGV{build} && $build != $ARGV{build}) {
        $logger->debug("Only build $ARGV{build} requested, so skipping line $_");
        $stats{'Skipped, wrong build'}++;
        next;
    }
    $stats{'Total SNPs to add merge-history for'}++;

    $rshigh = 'rs'.$rshigh;
    $rslow  = 'rs'.$rslow;
    $logger->info("#" . $stats{'Total SNPs to add merge-history for'}. " $rshigh => $rslow (b$build)");
    my $marker = $markerdbh->get_marker_by_accession_and_source($rshigh);
    my $marker_replacement = $markerdbh->get_marker_by_accession_and_source($rslow);
    
    if(!$marker) {
        $logger->error("didn't find merge-donor $rshigh in database!!");
        $stats{DonorMissing}++;
        next;
    }

    # Create new marker revision entry, linked to the marker which replaced the rshigh one in the merger
    $logger->debug("Creating revision entry for dbSNP:$rshigh as " . $marker->identifier);
    my $revinfo = { changetype    => 'Merging',
                    changetrigger => 'b'.$build,
                    comment       => "Merge-info: $rshigh => $rslow.imported from RsMergeArch for b$build (dbSNP tabledump)"
    };
    if($marker_replacement) {
        $revinfo->{replacedbymarkerid} = $marker_replacement;
    } else {
        $logger->error("didn't find merge-recipient $rslow in database!!");
        $revinfo->{comment} .= ". ERROR merge-recipient $rslow not found in database.";
    }
    
    $orient2current == 1 and $revinfo->{strandflipped} = 'yes'; # check if there was a strand flip during this merge    
    $logger->is_debug and 
        $logger->debug("revinfo: ". Dumper($revinfo));
    my $new_mrev = $marker->create_related('markerrevision_markerids', $revinfo);
    $new_mrev->update();

    # Update the timestamp for the marker we're killing off
    $updated =~ s/\s.+$//;
    $marker->update({status=>"dead",
                     timelasttouched => $updated});

    

    
    
#     my $markernode = stag_nodify(
# [Marker => [
#   [ Source               => 'dbSNP' ],
#   [ LocalID              => 'rs'.$rshigh ],
#   [ Status               => 'dead'],
#   [ TimeLastTouched      => $updated],
#   [MarkerRevision => [
#     [ Comment       => 'Merge-info imported from RsMergeArch (dbSNP tabledump)'],
#     [ ChangeType    => 'Merging' ],
#     [ ChangeTrigger => 'b'.$build ],
#     [ ReplacedByMarker => [
#       [Marker => [
#         [ Source               => 'dbSNP' ],
#         [ LocalID              => 'rs'.$rscurrent ],
#         #[ TimeLastTouched      => $updated],
#       ]]
#     ]]
#   ]]
# ]]);
#     $logger->is_debug and $logger->debug($markernode->itext);

}

if($ARGV{n}) {
    $logger->info("No errors encountered during load, but in dry-run mode so NOT committing database transaction");
    $markerdbh->storage->dbh->rollback();
}
else {
    $logger->info("No errors encountered during load, committing clean database transaction");
    $markerdbh->storage->dbh->commit();
}



$logger->info(Dumper(\%stats));

exit 0;

__END__


=head1 NAME

  - Extract dbSNP merge-history information and add to our database

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script takes dbSNP's RsMergeArch.bcp.gz file, extracts all the rs# to rs# merge 
  events listed and add these to our MarkerRevision table. This effectively duplicates 
  bSNP's merge-history into our database so we can useit for lookups, e.g. if people 
  give us data references rs#'s for markers that are no longer in dbSNP.

  $Id: updateMarkerMergehistory.pl 1515 2010-07-13 13:52:11Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <infile>

  Tab-delimited file to process (RsMergeArch.bcp.gz from dbSNP's FTP-site at the time of writing).

=item -c[onfig] <config>

  Path to config file to get db-connection parameters from.

=back

=head1 OPTIONS

=over

=item -n 

  Run in 'dry-run' mode w/o writing anything to the database. This will go through the entire
  data processing procedure and do all database operations, but will not commit the transaction 
  at the end. Useful for debugging.

=item -l[abel] [=] <label> 

  Optional label for this processing job, typically the build no. of the source database (optional).  

=item -b[uild] <build>

  Take only merge-events that happened in this dbSNP build (default is to take the entire file).

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=head1 CONTACT

This module is part of the HGVbaseG2P project

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$id: Database.pm 1272 2009-08-21 15:08:35Z rcf8 $ 

=cut

