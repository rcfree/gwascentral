#!/usr/local/bin/perl -w 

# $Id: indexForTextsearch.pl 1481 2010-06-21 16:17:31Z rcf8 $

# Refresh/generate DBIx::Class schema classes

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use GwasCentral::Base qw(load_config);
use GwasCentral::DataSource::Util qw(new_DataSource);
# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

# Initialize logger
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);
use DBIx::Class::Schema::Loader qw(make_schema_at);
INFO("Using config file ".$ARGV{c});
my %config = load_config($ARGV{c});
my $libdir = $config{basedir}."/lib";
DBIx::Class::Schema::Loader->dump_to_dir($libdir);
exit if !$ARGV{databases};
my @schemas = split(/\ /,$ARGV{databases});

foreach my $schema(@schemas) {
	generate_schema(ucfirst($schema));
}

sub generate_schema {
	my ($dblabel) = @_;
	INFO "Generating $dblabel schema modules in ".$libdir;
	my $ds = new_DataSource($dblabel, \%config, {});
	my $dsn = $ds->dsn;
	INFO "Using schema from ".$dsn;
	make_schema_at("GwasCentral::Schema::".$dblabel, { debug => 0, overwrite_modifications => $ARGV{i} }, [ $ds->dsn,$ds->user,$ds->pass ]);
}

exit 0;

__END__


=head1 NAME

  refreshSchema.pl

=head1 VERSION

  1.0

=head1 DESCRIPTION

  Use this script to refresh the DBIx::Class Schema classes representing the Study and Marker databases

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]


=head1 OPTIONS

=over

=item <databases>

  List of databases to update

=item -c[onfig] <config>

  Path to config file to use in place of the default one.

=for Euclid:
        config.default: 'conf/main.conf'

=item -i[nit]

  Initialise schema files that have changed (careful with this option it will remove any manual additions!)
  

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>

=head1 BUGS

  None known, but please report any to the author
    
=cut