#! perl

use strict;
use warnings;
use WWW::Mechanize;
use Data::Dumper qw(Dumper); 
use Log::Log4perl qw(:easy);
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Database::Study;
use HGVbaseG2P::Browser::Creator;
Log::Log4perl->easy_init($INFO);

my $mech = WWW::Mechanize->new();
#$mech->credentials("http://hgvbaseg2p-dev2.gene.le.ac.uk:80", "HGVbaseG2P-dev is currently restricted access", "hgvbaseg2p", "h0listic");
my $studyid=$ARGV[0];

my $db = HGVbaseG2P::Database::Study->new({conf_file=>'conf/hgvbase.conf'});
my $config = $db->config;
#my $methods = $db->get_resultsets_in_study_by_identifier({identifier=>$studyid});

INFO("Using $studyid");

my $src_gbrowse_url = $config->{hgvbrowse_url};
my $tgt_gbrowse_url = "[%- c.config.hgvbrowse_url -%]";
my $hgvbase_url = $config->{WebSite}->{browser_gv_baseurl};
my $bc = HGVbaseG2P::Browser::Creator->new({conf_file=>'conf/hgvbase.conf'});
$bc->studies($ARGV[0]);
$bc->_get_studies();

foreach my $study(@{$bc->study_list}) {
	my $replace = "studyGVToRV\('" . $study->identifier ."',";
	my @resultsets = $db->get_resultsets_in_study_by_identifier($study->identifier);
	INFO("Generating imagemaps and images for study ".$study->identifier);
	my $relative_map_path = $config->{genomeview_map_url}."/".$study->identifier."/";
	my $actual_map_path = $config->{genomeview_map_path}."/".$study->identifier."/";
	my $relative_img_path = $config->{genomeview_img_url}."/".$study->identifier."/";
	my $actual_img_path = $config->{genomeview_img_path}."/".$study->identifier."/";
	
	for my $threshold (qw(3)) {
		my $url = $hgvbase_url ."?band_labels=0&c=ALL&h=350&w=11&e=1&rows=3&rotate=0&fp=GenomeView~threshold=$threshold~resultsets=".join(",",map { $_->identifier } @resultsets)."~settings%3Dscale_type|Linear%2Cgenomeview_size|1~tracks%3D";
	
		INFO("Threshold $threshold ($url)");
		#my $iframe_url;
		$mech->get($url);
		
		#$mech->submit_form(form_name=>'karyotype');
		my $iframe_content = $mech->response->content;
		
		my @images = $mech->images;
		my $old_image_src = $images[0]->url; 
		my $new_image_src = $relative_img_path."threshold$threshold.png";
		
		#replace iframe content image src with new image src
		$iframe_content =~ s/$old_image_src/$new_image_src/;
		
		#replace iframe content gbrowse links with correct links
		
		$iframe_content =~ s/gvToRV\(/$replace/g;
		$iframe_content =~ s/<br\/><b>.+<\/b>//;
		$iframe_content =~ s/<br\/><input/<input/;
		
#		if ($src_gbrowse_url ne $tgt_gbrowse_url) {
#			$iframe_content =~ s/$src_gbrowse_url/$tgt_gbrowse_url/g;
#		}
		
		#get actual image content
		$mech->get($old_image_src);
		my $img_content = $mech->response->content;
		
		if (! -e $actual_img_path) {
			mkdir $actual_img_path or die "Unable to mkdir:$actual_img_path";
		}
		
		if (! -e $actual_map_path) {
			mkdir $actual_map_path or die "Unable to mkdir:$actual_map_path";
		}
		
		open HTMLFILE, ">", $actual_map_path."threshold$threshold.tt2" or die "Unable to open:" . $actual_map_path."threshold$threshold.tt2";
		print HTMLFILE "[%- nowrappers=1 -%]\n";
		print HTMLFILE $iframe_content;
		close HTMLFILE;
		
		open PNGFILE, ">", $actual_img_path."threshold$threshold.png" or die "Unable to open:" . $actual_img_path."threshold$threshold.png";
		print PNGFILE $img_content;
		close PNGFILE;
	}
}
