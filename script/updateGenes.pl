use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use GwasCentral::DataSource::Feature;
use GwasCentral::Browser::Util qw(chr_list);
use GwasCentral::Base qw(open_infile open_outfile);
use Bio::FeatureIO;
use Log::Log4perl qw(:easy);
use Data::Dumper qw(Dumper);
Log::Log4perl->easy_init($INFO);

my $fds = GwasCentral::DataSource::Feature->new({conf_file => 'conf/main.conf'});

#http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/
my %chr_info = ();
my $tmp_dir = "/tmp";
my $chr_file = "chromInfo.txt";
my $band_file = "cytoBand.txt";
my $gene_file = "refGene.txt";
my $ucsc_site = "ftp://hgdownload.cse.ucsc.edu/goldenPath/hg19/database";
my $temp_gff_db = "temp_gff_db";
my $temp_gff_user = "hgvbaseg2p_admin";
my $temp_gff_pass = "hgvbaseg2p";

update_chrs_in_database();
#delete_features_in_database();
#get_gff_files_for_database();
#generate_gene_density_gff();
#load_gff_into_database();

sub update_chrs_in_database {
	_wget_and_gunzip($chr_file);
	my @features = $fds->dbh->get_features_by_type("chromosome:UCSC");
	my $chr_fh = open_infile($chr_file);
	while(my $line = $chr_fh->getline) {
		my @temp = split(/\s+/,$line);
		$chr_info{$temp[0]}=$temp[1];
	}
	$chr_fh->close;
	foreach my $f(@features) {
		my $chr = $f->ref;
		$chr=~s/Chr//;
		$chr=~s/chr//;
		my $new_length = $chr_info{$chr};
		if (!$new_length) {
			INFO "deleting chr:".$f->ref;
			#$fds->dbh->delete($f);
			next;
		}
		INFO "length of chr:".$f->ref." ".$f->stop." is now ".$new_length;
		$f->stop($new_length);
		$f->update($f);
	}
	
}

sub delete_features_in_database {
	_delete_feature_type(['cytoband:UCSC','centromere:UCSC','mRNA:UCSC','three_prime_utr:UCSC','five_prime_utr:UCSC']);
}

sub get_gff_files_for_database {
	_get_cytoband_from_ucsc();
	_wget_and_gunzip($gene_file);
	system "wget http://cpansearch.perl.org/src/LDS/GBrowse-2.39/bin/ucsc_genes2gff.pl";
	chdir "$tmp_dir";
	system "perl ucsc_genes2gff.pl -src UCSC $gene_file > $gene_file"."_with_hap.gff3";
	my $cmd = "sed 's/_hap//' $gene_file"."_with_hap.gff3 > $gene_file.gff3";
	INFO "cmd:$cmd";
	system $cmd ;
	
}

sub generate_gene_density_gff {
	system "mysqladmin drop $temp_gff_db -u$temp_gff_user -p$temp_gff_pass";
	system "mysqladmin create $temp_gff_db -u$temp_gff_user -p$temp_gff_pass";
	system "bp_load_gff.pl -c -d $temp_gff_db -u $temp_gff_user -p $temp_gff_pass $gene_file.gff3";
	system "bp_generate_histogram.pl -merge -d $temp_gff_db -u $temp_gff_user -p $temp_gff_pass -bin 1000000 mRNA > gene_density.gff2";
	system "sed 's/bin Chr/Name=bin_Chr/g' gene_density.gff2 > gene_density.gff3";
}

sub load_gff_into_database {
	chdir "$tmp_dir";
	_load_gff($band_file);
	_load_gff($gene_file);
	_load_gff("gene_density");
}

sub _load_gff {
	my ($file) = @_;
	my $sys = "bp_seqfeature_load.pl -d ".$fds->name.":".$fds->host." -u ".$fds->user." -p ".$fds->pass." $file".".gff3";
	system $sys;
}

sub _delete_feature_type {
	my ($type) = @_;
	my @features = $fds->dbh->get_features_by_type($type);
	$fds->dbh->delete(@features);
	INFO "Deleted ".scalar(@features)." features of type $type";
}

#adapted Mummi's fetchideogram.pl from Bio::Graphics
sub _get_cytoband_from_ucsc {
	my %stains; my %centros; my %chrom_ends;
	_wget_and_gunzip($band_file);
	my $in_fh = open_infile("$band_file");
	my $out_fh = open_outfile("$band_file.gff3");
		print $out_fh "##gff-version 3\n"; while(<$in_fh>) { 
		chomp; my($chr,$start,$stop,$band,$stain) = split /\t/; 
		$start++; 
		$chr = ucfirst($chr); 
		if(!(exists($chrom_ends{$chr})) || $chrom_ends{$chr} < $stop) { $chrom_ends{$chr} = $stop; } 
		my ($arm) = $band =~ /(p|q)\d+/; 
		$stains{$stain} = 1; 
		if ($stain eq 'acen') { $centros{$chr}->{$arm}->{start} = $stop; $centros{$chr}->{$arm}->{stop} = $start; next; } $chr =~ s/chr//i; print $out_fh  qq/$chr\tUCSC\tcytoband\t$start\t$stop\t.\t.\t.\tParent=$chr;Name=$chr;Alias=$chr$band;stain=$stain;\n/; }

	foreach my $chr(sort keys %chrom_ends) { 
		my $chr_orig = $chr; $chr =~ s/chr//i; 
		print $out_fh  qq/$chr\tUCSC\tcentromere\t$centros{$chr_orig}->{p}->{stop}\t$centros{$chr_orig}->{q}->{start}\t.\t+\t.\tParent=$chr;Name=$chr\_cent\n/; 
	}
	$out_fh->close;
	$in_fh->close;
	INFO "File $band_file.gff3 written for cytobands";
}

sub _wget_and_gunzip {
	my ($file) = @_;
	chdir $tmp_dir;
	unlink "$file.gz";
	unlink "$file";
	system "wget $ucsc_site/$file.gz";
	system "gunzip $file.gz";
}

