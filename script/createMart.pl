#!perl

# $Id: createHGVmart.pl 1445 2010-05-26 14:23:50Z rcf8 $


# create BioMart database using HGVbaseG2Pmart/HGVbaseG2P modules

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use GwasCentral::Mart::Creator;
use Data::Dumper qw(Dumper);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

if ($ARGV{test}) {
	$ARGV{conf_file} = '../t/conf/main.conf'
}


my $creator = GwasCentral::Mart::Creator->new(\%ARGV);

$creator->generate;

__END__


=head1 NAME

  - Create mart from HGVbaseG2P database using a mart config script.

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script creates a BioMart database using the data in the live HGVbaseG2P database.
  It uses the SQL files in martsql and XML config files in martxml to generate this.
  Logic for the creation is in the HGVbaseG2Pmart::MartCreator and associated classes.
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onf_file] <file>
  
  Mart generating config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/main.conf'

=item -t[est]
	
  Use the 'hgvmart.conf' config file in 'conf/test' rather that 'conf'
	
=item -i[nit]

  Initialise mart before beginning import etc

=item -s[tudies] <file>

  List of studies to build mart for
  
=item -r[efreshconfig]

  Refresh config only (do not generate datasets)
  
=item -n[oconfirm]

  Removes need to confirm database drop

=item -d[atasets] <datasets>

  List of datasets to use (default is to use all in conf/mart.conf without 'ignore' flag)
  
=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
