use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use DBI;
use LWP::UserAgent;
use Filesys::DfPortable;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::Core qw(load_config);
use MIME::Lite;
use File::Basename;
use Log::Log4perl::Level;

use Sys::Hostname;
use Getopt::Euclid qw(:minimal_keys);

my %config = load_config($ARGV{c});
my $emails = $config{'ServerCheck'}->{email};
my @filesystems = @{$config{'ServerCheck'}->{filesys}};
my @ws_errors=();
my @db_errors=();
my @fs_errors=();
my $FROM = 'help@hgvbaseg2p.org';
my $SEND_TYPE = "smtp";
my $SEND_SERVER = "mailsend.le.ac.uk";
my $MIN_SPACE = 80;
my $host = hostname();
use HGVbaseG2P::Util qw(init_logger);
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

if ($config{'ServerCheck'}->{check_filesys}) {
	for my $fs (@filesystems) {
		eval {
				check_space($fs);
		};
		if ($@) {
			push @fs_errors, "Filesystem $fs is greater than ".$MIN_SPACE."% full";
		}
	}
}

foreach (qw(Study Marker Browser Feature)) {
	eval {
			check_database($_);
	};
	if ($@) {
		push @db_errors, $@;
	}
}
my $base_url = $config{ServerCheck}->{baseurl};
 my @urls = @{$config{ServerCheck}->{url}};

my $ua = LWP::UserAgent->new;
	for my $url (@urls) {
	eval {
			my $resp = $ua->get($base_url.$url);
			$logger->info("get url:$base_url$url");
			push @ws_errors, "The URL '$base_url$url' returned a ".$resp->code ." response" if $resp->code !~ /^2/ 
	};
	if ($@) {
		push @ws_errors, "Unable to access URL '$base_url$url'.\n \t\tReason:$@";
	}
}
if ($ARGV{p}) {
	my $title = "HGVbaseG2P Web Site (Server:'$host') check report.";
		print "$title\n".("-" x length($title))."\n";
}
if (scalar(@fs_errors>0) or scalar(@ws_errors > 0) or scalar(@db_errors) > 0) {
	my $body = "The following errors occurred during the Server Check:\n\n";
	if (scalar(@fs_errors) > 0) {
		$body.="FileSystem Errors:\n\t* ".join("\n\t* ",@fs_errors)."\n\n";
	}
	if (scalar(@db_errors) > 0) {
		$body.="Database Errors:\n\t* ".join("\n\t* ",@db_errors)."\n\n";
	}
	if (scalar(@ws_errors) > 0) {
		$body.="Web Site Errors:\n\t* ".join("\n\t* ",@ws_errors)."\n\n";
	}
	
	if ($ARGV{p}) {
		print $body;
	}
	else {
		send_email($emails, "HGVBASEG2P SERVER CHECK ERROR (from $host)",$body, $ARGV{p});
	}
}
else {
	if ($ARGV{p}) { print "There were NO ERRORS\n\n"; }
}

sub check_database {
	my ($dbname)=@_;
	my $dbconfig = $config{Database}->{ucfirst($dbname)};
	eval {
		my $idb = DBI->connect($dbconfig->{dsn}, $dbconfig->{user}, $dbconfig->{pass}, {
	                                   RaiseError => 1, # do die on error
	                               })
	};
	if ($@) { die "Unable to connect to $dbname database ".$dbconfig->{dsn}."\n \t\tReason:$@" };
}

sub check_space {
	my ($file_system) = @_;
	my $fs_info = dfportable($file_system);
	return if !$fs_info;
	die if $fs_info->{per}>$MIN_SPACE;
}

sub send_email {
		my ($to, $subject, $data,$print)=@_;
		my $to_list = $to;
		
		if (ref($to) eq "ARRAY") {
			$to_list = join(";",@{$to});
		}

		my $msg = new MIME::Lite;
	
	   $msg->attr("content-type"         => "text/html");
	   $msg->attr("content-type.charset" => "latin1");
	   $msg->attr("content-type.name"    => "homepage.html");
	   $msg->build(
	       From     =>$FROM,
	       To       =>$to_list,
		   Encoding =>'quoted-printable',
	       Subject  =>$subject,
	       Data     =>$data
	   );
	
	   MIME::Lite->send($SEND_TYPE,$SEND_SERVER,Debug=>0);
	   $msg->send;
	}

__END__


=head1 NAME

  - Deploy a new version of HGVbaseG2P

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script calculates markers imported for eache experiment in Study DB
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onfigfile] <file>
  
  HGVbaseG2P config file to use

=for Euclid:
        file.type:    readable
        file.default: '/var/www/hgvbaseg2p_v4/conf/hgvbase.conf'

=item -p[rint]

  Output server check results to STDOUT rather than email
  
=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back
