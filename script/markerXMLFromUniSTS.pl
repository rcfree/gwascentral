#!/usr/local/bin/perl -w

# $Id: markerXMLFromUniSTS.pl 1445 2010-05-26 14:23:50Z rcf8 $

# XML-parser to extract relevant info from UniSTS XML-dumps and print out as HGVbase XML

use strict;
use warnings;

use Data::Dumper;
use Data::Stag qw(:all);
use Data::Stag::XMLWriter;
use File::Basename;
use List::MoreUtils qw( uniq );

# Initialize logger
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init( { level  => $ARGV{verbose} ? $DEBUG : $INFO,
			    layout => "%L:%p> %m%n",
			    file => 'STDERR' } );

# Connect to feature database
use Bio::DB::SeqFeature::Store;
my $db = Bio::DB::SeqFeature::Store->new(-adaptor => 'DBI::mysql',
					 -dsn     => 'dbi:mysql:biodbgff_test;user=mummi;password=Dusill') || die $@;


# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

# Prepare the input datafile, set up decompression on the fly if appropriate
my $fn = $ARGV{xmlfile};
-f $fn or die "Must specify valid path to XML-file to transform";
$fn =~ /gz\Z/xms and $fn = "zcat $fn |";
my ($chunkno) = $fn =~ /xml\.(\w+)/;

# Same for the output file, compress that on the fly to save space
my $outfn = "| gzip -c > unists_markers_$chunkno\.xml.gz";
my $outfh = IO::File->new($outfn) || die $!;
$ARGV{stdout} and $outfh = \*STDOUT;
INFO("Processing input stream $fn, writing to output stream $outfn\n");

# Set up the Data::Stag parsing framework
my $parser = Data::Stag->parser(-format  => 'xml') ;    # The XML-parser itself
my $handler = Data::Stag->makehandler(sts => \&handle_sts); # generic handler with callback-routines
my $writer = Data::Stag::XMLWriter->new(-fh => $outfh); # writer to output our XML
$parser->handler($handler);

# Print any container elements and auxiliary information needed in the output XML,
# parse the input and then close any open container elements.
my %stats;
my $stime = time();
$writer->start_event('top');
my $hotlinknode = stag_nodify(
	 [Hotlink => [
            ['@' => [[id=>'unists_hotlink']]],
            [HotlinkLabel => 'UniSTS marker report'],
            [UrlPrefix    => 'http://www.ncbi.nlm.nih.gov/genome/sts/sts.cgi?uid='],
         ]]);
$hotlinknode->events($writer);

$parser->parse($fn);
$writer->end_event('top');

# Print out some stats
my $totaltime = time() - $stime;
$stats{totaltime} = $totaltime;
$stats{'Performance(STSs/s)'} = int($stats{stscount}/$totaltime);
INFO("\n\nSTATS:\n",Dumper(\%stats),"\n");

exit;

#-------------------------------------------
#  END OF MAIN SCRIPT
#-------------------------------------------

sub handle_sts {
    my ($handler,$node) = @_;
    
    # Make sure we only get markers which map to the human genome
    my ($orgnode) = $node->qmatch('org','taxname', 'Homo sapiens');
    defined($orgnode) or return;
    my $primary_name = $orgnode->get('name');
    defined($primary_name) or return;
    DEBUG($orgnode->itext);
    $stats{stscount}++;
    $stats{stscount} % 100 == 0 and print STDERR "\r$stats{stscount} STSs processed";
    
    my $id = $node->get('uid');
    DEBUG("#$stats{stscount} UniSTS:$id");

    # Get aliases
    my $alias_string = $orgnode->get('alias');
    my @aliases = defined($alias_string) ? split(';', $alias_string) : ();
    unshift(@aliases, $primary_name);
    DEBUG("id=$id, pname='$primary_name', aliases=@aliases");

    my @alias_nodes = ();
    if(@aliases > 0) {
	foreach (uniq(@aliases)) {
	    next if !defined($_);
	    my $is_primary  = $_ eq $primary_name ? [IsPrimary => '1'] : [];
	    push @alias_nodes, 
[MarkerAlias => [
   ['@'       => [[op =>'insert']]],
   [Alias   => $_ ],
   $is_primary
]];     
        }
}

    # Check for warning flags, possibly non-unique mapping info
    #if(my $wctg = $orgnode->get('wctg')) {
    #    DEBUG("Got wctg flag '$wctg'");
    #}

    # Get genome assembly coordinates AND the sequence at those coordinates
    my @coord_nodes;
    my %alleles;
    foreach my $mapnode($orgnode->qmatch('mappos', 'map', 'Sequence')) {	
	DEBUG("Got maploc: ", $mapnode->get('chr'),':', $mapnode->get('coord'));
	my ($chr,$assembly_name);
	($chr, $assembly_name) = split(/\|/, $mapnode->get('chr'));
	$chr           ||= $mapnode->get('chr');
	$assembly_name ||= 'reference';
	
	my ($start, $stop) = split('-', $mapnode->get('coord'));
        #my $mapweight = $mapnode->get('wloc') ? 'multiple-hits' : 'unique-in-contig';
        my $mapweight = 'unique-in-contig';
	
	my $coord_node = [MarkerCoord => [
      ['@'       => [[op =>'insert']]],
      [Chr          => $chr],
      [Start        => $start],
      [Stop         => $stop],
      [Span         => '..'],
      [MapWeight    => $mapweight],
      [GenomeBuild  => $ARGV{assembly}],
      [AssemblyType => '?? (for now)'],
      [AssemblyName => $assembly_name],		    
 ]];

	push @coord_nodes, $coord_node;

        # Get allele sequence for this coordinate, but ONLY if it's the reference
        # NB 30bp sequence context f. allele nnnn(PCR product)nnnn , according to HGVbaseG2P nomenclature
        if($assembly_name eq 'reference') {
            $chr = 'M' if $chr eq 'MT';
            my $seg = $db->segment('Chr'.$chr, $start-30 => $stop+30);
            DEBUG("Got segment $seg");
#if( my @feats = $seg->features('tandem_repeat:UCSC')) {
#    map {print STDERR "   for marker $primary_name, got tr feat=$_\n"} @feats; 
#}
            my $seq        = $seg->seq;
            my $seq_length = $seq->length();
            #print "total seq_length=$seq_length\n";
            my $upstream_seq = lc($seq->subseq(1, 30));
            #print "upstream 30bp=$upstream_seq, length",length($upstream_seq) , "\n";
            my $product_seq = uc($seq->subseq(31, $seq_length-30));
            #print "STS seq for $id=$product_seq, length",length($product_seq), "\n";    
            my $downstream_seq = lc($seq->subseq($seq_length-29, $seq_length));
            #print "downstream 30bp=$downstream_seq, length=", length($downstream_seq), "\n";
            #print "allele def, using nomenclature: $upstream_seq($product_seq)$upstream_seq\n";

            # NOTE TO SELF: check feature-db for tandem repeats, and construct allele-seq from this (e.g. (AT)5)
            $alleles{$product_seq} = [$upstream_seq, $downstream_seq];
         }

}
my @allele_nodes;
while(my ($allele, $flanks) = each %alleles) {
push @allele_nodes, 
[Allele => [
  ['@'       => [[op =>'insert']]],
  [Upstream30bp  => $flanks->[0]],
  [AlleleSeq     => $allele],
  [Downstream30bp => $flanks->[1]],
  [SeqType       => 'G'],
]]
;
}

DEBUG("Got ",scalar(@allele_nodes), " alleles for $id");
    my $marker_node = stag_nodify(
[Marker => [
   ['@'       => [[op =>'insert']]],
   [Source  => 'UniSTS'],
   [LocalID => $id],
   [VariationType => 'STS'],
   [HotlinkID => 'unists_hotlink'],
   @alias_nodes,
   @allele_nodes,
   @coord_nodes,
]]);

$marker_node->events($writer);

}

__END__

=head1 NAME
 
  - Transform UniSTS marker XML-dumps to HGVbase XML

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script uses the Data::Stag structured-tags framework to extract marker
  information from NCBI's UniSTS XML-dumps and print out as XML. The output 
  XML conforms to the HGVbase database schema and is intended for direct 
  loading into the database via DBIx::DBStag.

  See also http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=unists


=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <xmlfile>

  Path to XML-file to transform

=item -a[ss[embly]] <assembly>

  Version of the reference sequence assembly (currently NCBI build 36 aka hg18)

=back

=head1 OPTIONS

=over

=item -stdout

  Print XML to STDOUT instead of compressed file (for debugging)

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
