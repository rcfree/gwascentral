#!/usr/local/bin/perl -w

# $Id: assignMarkerAllelesets.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Create the 'shorthand' alleleset-string in the Marker table by getting the
# list of AlleleSeq values from the child Allele table.

use strict;
use warnings;
use English qw( -no_match_vars );

use Data::Dumper;
use FindBin;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Data::Stag qw(:all);
use Data::Stag::XMLWriter;

use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::Database;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Configure commandling-option processing (see POD below the code)
use Getopt::Euclid qw( :minimal_keys );

# Adjust logging level to verbose if required
use Log::Log4perl::Level;
$ARGV{v} and $logger->level($DEBUG);

# Build arguments to pass to database module. 
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

# Initialize database handle
$logger->info("Initializing database handle");
my $db;
$db = HGVbaseG2P::Database->new(conf_file => $ARGV{c},
                                dsn       => $dsn,
                                user      => $dbuser, 
                                passw     => $dbpassw) || die "Could not initialize database handle";


# Prepare SQL-query. Get only markers that are NOT flagged as dead, and their alleles that are 
# also NOT flagged as dead.
my $sth = $db->dbh->prepare(qq{
select MarkerID,Source,LocalID, GROUP_CONCAT( AlleleSeq SEPARATOR "|") as AlleleSet 
from Marker join Allele using(AutoMarkerID)  
where Allele.Status = 'active' 
group by AutoMarkerID 
});
#limit 10000;


my $writer = Data::Stag::XMLWriter->new(-fh => \*STDOUT);
$writer->start_event('top');
my %stats;
$sth->execute();

while(my $r = $sth->fetchrow_hashref()) {
    $stats{markercount}++;
    $logger->debug("#$stats{markercount} $r->{MarkerID}");
    $stats{markercount} % 1000 == 0 and $logger->info("Total $stats{markercount} markers done");
    my @alleles = split(/\|/, $r->{AlleleSet});

    # Reformat each allele-string, including handling longer sequences
    foreach(sort(@alleles)) {
        my $alength = length($_);
        $_ =~ /^[ATCGN]+$/ixms and $alength > 20 and $_ = q{"}.unit_label($alength).q{ seq"};
        $_ = "($_)";
    }
    stag_nodify([
Marker => [
  [ '@' => [[op => 'update']] ],
  [ MarkerID  => $r->{MarkerID} ],
  [ AlleleSeqsShorthand => join(':', @alleles) ],
]])->events($writer);
}

$writer->end_event('top');

$db->disconnect();
$logger->info(Dumper(\%stats));

exit 0;


sub unit_label {
	my ($value) = @_;
	return $value;
}


__END__


=head1 NAME

  - Update 'shorthand' alleleset-string for Marker entries

=head1 VERSION

  0.2

=head1 DESCRIPTION

  Connects to master database and executes SQL-query which groups together AlleleSeq
  values from the Allele table by their parent Marker table record (SQL: GROUP_CONCAT()).
  Only Allele entries with Status = 'active' are included, and long sequence-strings are 
  truncated. The resulting string corresponds to our nomenclature (e.g. (A):(T)) and is
  suitable for embedding in flanking sequence strings like this: ATGAT[(A):(T)]CCTAggat.
    As usual, the output is in XML suitable for loading into the datbase.

    The shorthand string is useful for scenarios where we only need the string, and no 
  other info from the Allele table. This saves the table-join and enables more efficient,
  compact retrieval of Marker info from the database.

  Note that this script needs to be rerun if the marker-level data in the database changes.
  Or possibly only on a particular marker source, or marker type.
    

  $Id: assignMarkerAllelesets.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]
    
=head1 OPTIONS

=over

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut

