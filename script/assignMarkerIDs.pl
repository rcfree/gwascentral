#!/usr/local/bin/perl -w

# $Id: assignMarkerIDs.pl 1515 2010-07-13 13:52:11Z rcf8 $

# Query database for marker entries with HGVbbaseG2P identifier, iterate
# over the list and assign a stable, unique HGVMxx to each entry.

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use File::Basename;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Database::Marker;
use HGVbaseG2P::Core qw(init_logger);

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Configure commandling-option processing (see POD below the code)
use Getopt::Euclid qw( :minimal_keys );

# Adjust logging level to verbose if required
use Log::Log4perl::Level;
$ARGV{v} and $logger->level($DEBUG);

# Connect to target database
my $markerdbh = HGVbaseG2P::Database::Marker->new({ conf_file => $ARGV{config}});
$markerdbh->storage->dbh->{'AutoCommit'} = 0;


# Query for currently highest HGVMxx ID in the Marker table, so we know where to 
# start our sequence of new IDs.
#my $r = $markerdbh->storage->dbh->selectall_arrayref("select Identifier from Marker where MarkerID = (select max(MarkerID) from Marker where Identifier is not null)");
#my $max_currentid = $r->[0]->[0];

my $rs_idmarkers = $markerdbh->resultset('Marker')->search({'identifier' => { '!=', undef }});
my $max_pkeyvalue = $rs_idmarkers->get_column('MarkerID')->max();
$logger->debug("Got current max PK MarkerID value $max_pkeyvalue. Now to find marker w/ this PK value");
my $last_idmarker = $markerdbh->resultset('Marker')->find($max_pkeyvalue);

my $max_currentid = $last_idmarker->identifier;
$max_currentid =~ s/^HGVM//;
$logger->info("Got last marker ID ".$last_idmarker->identifier);

# Now qery for all markers with no MarkerID to get our target list to slap IDs on.
my $rs_noidmarkers = $markerdbh->resultset('Marker')->search({'identifier' => undef,
                                                             },
                                                             #{ rows => 200 }
                                                             );

# Iterate over the list and update each entry with a spanking new ID.
my %stats = ();
while ( my $marker = $rs_noidmarkers->next() ) {
    $max_currentid++;
    my $new_markerid = 'HGVM'.$max_currentid;
    $stats{markers_updated}++;
    $logger->info("#$stats{markers_updated} ".$marker->accession . " => $new_markerid");
    $stats{markers_updated} % 1000 == 0 and
        $logger->info("Total $stats{markers_updated} markers assigned a stable ID");
    
    $marker->update({identifier => $new_markerid});
}


if($ARGV{n}) {
    $logger->info("No errors encountered during load, but in dry-run mode so NOT committing database transaction");
    $markerdbh->storage->dbh->rollback();
}
else {
    $logger->info("No errors encountered during load, committing clean database transaction");
    $markerdbh->storage->dbh->commit();
}

print STDERR Dumper(\%stats);


exit 0;

__END__


=head1 NAME

  - Assign stable, unique HGVMxxx identifiers to markers which do not already have them

=head1 VERSION

  0.2

=head1 DESCRIPTION

  This script queries the database for all entries in the Marker table which do not have
  an HGVbaseG2P-assigned identifier (HGVMxxxx), as well as the current highest-numbered
  MarkerID. It will print out XML which can be loaded into the master database to update
  these marker entries with a new MarkerID.

  $Id: assignMarkerIDs.pl 1515 2010-07-13 13:52:11Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item -c[onfig] <config>

  Path to config file to get db-connection parameters from.

=back

=head1 OPTIONS

=over

=item -n 

  Run in 'dry-run' mode w/o writing anything to the database. This will go through the entire
  data processing procedure and do all database operations, but will not commit the transaction 
  at the end. Useful for debugging.

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=head1 CONTACT

This module is part of the HGVbaseG2P project

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: assignMarkerIDs.pl 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut

