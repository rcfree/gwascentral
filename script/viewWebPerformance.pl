use FindBin;
use lib "$FindBin::Bin/../lib";
use WebPerformance;

#my $base_url =  "http://www.hgvbaseg2p.org";
# my @urls = (
# 	'/studies',
# 	'/phenotypes',
# 	'/markers',
# 	'/study/HGVST1',
# 	'/study/markers/HGVST76',
# 	'/marker/HGVM510',
# 	'/marker/results/HGVM510?threshold=ZERO',
# 	'/marker/results/HGVM510?threshold=5',
# 	'/marker/frequencies/HGVM510',
# 	'/cgi-perl/hgvbrowse_karyotype/genome?band_labels=0&c=&h=350&w=11&e=1&rows=2&rotate=0&fp=GenomeView~threshold=3~resultsetid=449,508,445,462,477,501,497,470~settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2~tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace&guid=1252514601187',
#);
#my $perf = WebPerformance->new({
#	url_list=>\@urls,
#	base_url=>$base_url
#});

  use LWP::UserAgent;
  use Parallel::ForkManager;

 use Log::Log4perl qw(:easy);
 Log::Log4perl->easy_init($DEBUG);
 use Statistics::Basic::Mean;
 use List::Util qw(sum);
 use Data::Dumper qw(Dumper);
 
 my $base_url =  "http://hgvbaseg2p-dev.gene.le.ac.uk";
 my @urls = (
 	'/studies',
 	'/phenotypes',
 	'/markers',
 	'/study/HGVST1',
 	'/study/markers/HGVST76',
 	'/marker/HGVM510',
 	'/marker/results/HGVM510?threshold=ZERO',
 	'/marker/results/HGVM510?threshold=5',
	 '/marker/frequencies/HGVM510',
	 '/cgi-perl/hgvbrowse_karyotype/genome?band_labels=0&c=&h=350&w=11&e=1&rows=2&rotate=0&fp=GenomeView~threshold=3~resultsetid=524,522,502,475,434,514,489,435~settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2~tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace&guid=1253698739767',
	#'/cgi-perl/hgvbrowse_karyotype/genome?band_labels=0&c=&h=350&w=11&e=1&rows=2&rotate=0&fp=GenomeView~threshold=3~resultsetid=19,17,12,16,14,9,10,1~settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2~tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace&guid=1253698095183',
	'/cgi-perl/hgvbrowse/region?width=990;label=Genes;label=Ideogram:overview;plugin=RegionView;plugin_action=Settings;plugin_config=1;name=Chr6:27000001..36000000;RegionView.resultsetid=524,522,502,475,434,514,489,435;RegionView.threshold=3;RegionView.settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2;RegionView.tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace',
	#'/cgi-perl/hgvbrowse/region?width=990;label=Genes;label=Ideogram:overview;plugin=RegionView;plugin_action=Settings;plugin_config=1;name=Chr6:30000001..39000000;RegionView.resultsetid=19,17,12,16,14,9,10,1;RegionView.threshold=3;RegionView.settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2;RegionView.tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace',
	'/cgi-perl/hgvbrowse/region?width=990;label=Genes;label=Ideogram:overview;plugin=RegionView;plugin_action=Settings;plugin_config=1;name=Chr6:27000001..36000000;RegionView.resultsetid=524,522,502,475,434,514,489,435;RegionView.threshold=3;RegionView.settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2;RegionView.tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace',
	#'/cgi-perl/hgvbrowse/region?width=990;label=Genes;label=Ideogram:overview;plugin=RegionView;plugin_action=Settings;plugin_config=1;name=Chr6:30000001..39000000;RegionView.resultsetid=19,17,12,16,14,9,10,1;RegionView.threshold=3;RegionView.settings=gscale_type|Linear,genomeview_size|1,rscale_type|Linear,lowres_binsize|1000000,highres_binsize|15000,scale_type|Linear,rows_chromosomes|auto,resultsets_with_sig|2;RegionView.tracks=ideogram,genes,sig_markers_single,sig_markers_multiple,highres_stacked,lowres_stacked,highres_present,lowres_present,line_trace'
);


  # Max no_users processes for parallel download
  
  my $counter;
 open FILE, ">perflog.out";
 print FILE "url\tno_users\tstart\tend\trange\n";
  for my $no_users(qw(1 5 10 20)) {
  	INFO "no_users:$no_users";
  	my $pm = new Parallel::ForkManager($no_users); 
	  for my $url (@urls) {
	  	my $overall_start = time;
		  
		  for my $user (1..$no_users) {
		    $pm->start and next; # do the fork
			
			#this code is child process
			my $start = time;
		    my $ua = LWP::UserAgent->new;
		    my $resp = $ua->get($base_url.$url);
		    my $end = time;
		    my $range = $end-$start;
		    
		    print FILE substr($url,0,20)."\t$no_users\t$start\t$end\t$range\n";
			
			
		    $pm->finish; # do the exit in the child process
		  }
	  $pm->wait_all_children;

	  my $overall_end = time;
	  

	  my $total = $overall_end - $overall_start;
	  my $avg = $total/$no_users;
 	  INFO "url:$url\n";
	  }

  }
  	  	  close FILE;

  