use FindBin;
use lib "$FindBin::Bin/../lib";
use GwasCentral::DataSource::Util qw(DataSources_from_list);
use Data::Dumper qw(Dumper);
my $ds = DataSources_from_list(['Feature','Mart'],"conf/main.conf");
my $feat_ds = $ds->{Feature};
my @features = $feat_ds->dbh->features(
			-seqid => 'chr1',
	    	-type => ['mRNA','CDS','three_prime_utr','five_prime_utr']
	    );
warn "features in chr1:" . scalar(@features);
my @feature_list = ();
my @gene_list = ();
foreach my $feat(@features) {
	my $type = $feat->type;
	$type=~s/:.+//g;
	my $gene_name;
	my $id;
	if ($feat->has_tag('parent_id')) {
		my @parent_ids = $feat->get_tag_values('parent_id');
		if ($parent_ids[0]) {
			my @par_f = $feat_ds->search_id($parent_ids[0]);
			$gene_name = $par_f[0]->display_name;
			$id = $par_f[0]->primary_id;
			if ($par_f[0]->has_tag('Alias')) {
				my @aliases = $par_f[0]->get_tag_values('Alias');
				foreach my $alias(@aliases) {
					push @gene_aliases, [$id, $alias];
				}
			}
			
		}

	}
	else {
		$gene_name = $feat->display_name;
		$id = $feat->primary_id;
		if ($feat->has_tag('Alias')) {
			my @aliases = $feat->get_tag_values('Alias');
			foreach my $alias(@aliases) {
				push @gene_aliases, [$id, $alias];
			}
		}
	}
	if ($type eq 'mRNA') {
		push @gene_list, [$id, "chr1", $gene_name, $feat->start, $feat->stop ];
	}
	else {
		push @feature_list, [$id, "chr1", $feat->start, $feat->stop, $type ];
	}
}

my $mart_ds = $ds->{Mart};
my $sql = "insert into hsapiens_gene_ucsc__gene__main(gene_id_key, ref, hgnc_label, start, stop) values(?,?,?,?,?)";
my $sth = $mart_ds->dbh->prepare($sql);
foreach my $g(@gene_list) {
	$sth->execute(@{$g});
}

$sql = "insert into hsapiens_gene_ucsc__feature__dm(gene_id_key, ref, start, stop, feature) values(?,?,?,?,?)";
$sth = $mart_ds->dbh->prepare($sql);
foreach my $f(@feature_list) {
	$sth->execute(@{$f});
}


