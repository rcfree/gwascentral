#!perl

# $Id: optimiseStudyDB.pl 1445 2010-05-26 14:23:50Z rcf8 $

# create Browser marker data table and binned data from Study/Marker database

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";

use HGVbaseG2P::Database::Study;
use HGVbaseG2P::Core qw(load_config);
use HGVbaseG2P::Browser::Util qw(neglog chr_list chr_length split_data_by_field);

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);

use Data::Dumper qw(Dumper);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

my $conf_file  = $ARGV{c};

my %config     = load_config($conf_file);

my $db =
  HGVbaseG2P::Database::Study->new( { conf_file => \%config } );

my @sources = $db->dbh->sources;
foreach my $src_name(@sources) {
		my $src = $db->dbh->source($src_name);
		print "Optimize table ". $src->from."\n";
		$db->dbh->storage->dbh->do("OPTIMIZE TABLE ". $src->from.";");
}
$db->dbh->storage->disconnect;
__END__


=head1 NAME

  - Create mart from HGVbaseG2P database using a mart config script.

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script optimises the Browser DB
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onfigfile] <file>
  
  HGVbaseG2P config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/hgvbase.conf'

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
