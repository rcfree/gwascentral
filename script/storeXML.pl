#!/usr/local/bin/perl -w

# $Id: storeXML.pl 1445 2010-05-26 14:23:50Z rcf8 $

use strict;
use warnings;
use English qw( -no_match_vars );

use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::XMLStore;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code (where the options are specified)
use Getopt::Euclid qw( :minimal_keys );

# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Adjust logging level to verbose
$ARGV{v} and $logger->level($DEBUG);


# Build arguments to pass to database module. 
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

# TODO have flag indicating whether to continue on errors or not? 

# Initialize database handle
$logger->info("Initializing database handle");
my $xmldb;
eval {
    $xmldb = HGVbaseG2P::XMLStore->new({conf_file => $ARGV{c},
				       dsn       => $dsn,
				       user      => $dbuser, 
				       passw     => $dbpassw,
				       stagmap   => $ARGV{s} }) || die "Could not initialize database handle";
};

if($EVAL_ERROR) {
    $logger->fatal("Caught exception: $EVAL_ERROR");
    exit 1;
}

# Attempt to store XML-file
eval {
    $xmldb->store_file($ARGV{hgvbase_xmlfile});
};

# Catch exception and only commit if either no errors or -force x
if($EVAL_ERROR) {
    if($ARGV{force}) {
	$logger->warn("Encountered errors during load, but committing transaction all the same, because -force is in effect");
	$xmldb->dbstag->commit();
    }
    else {
	$logger->warn("Encountered errors during load so NOT committing transaction! $EVAL_ERROR");
    }
    exit 1; # Exit with non-zero to indicate error
}
else {
    if($ARGV{n}) {
	$logger->info("No errors encountered during load, but in dry-run mode so NOT committing transaction");
    }
    else {
	$logger->info("No errors encountered during load, committing clean transaction");
	$xmldb->dbstag->commit();
    }
    exit 0; # Exit cleanly 
}


__END__


=head1 NAME

  - Load HGVbase XML-files into database

=head1 VERSION

  0.3

=head1 DESCRIPTION

  This script uses the Data::Stag / DBIx::DBStag framework to parse XML-data 
  corresponding to the HGVbase relational schema and load the data into the
  database. If top-level <Study> and second-level <Experiment> are present, 
  these will be stored first and child-elements linked to their parents one
  by one, to avoid loading the entire container-elements into memory.

  The single input XML-file will constitute a transaction, so either all of
  the data in the file will be inserted & committed, OR if there is an error
  during the load, the whole transaction will be rolled back and aborted.

  PS DBI_DSN and/or DBI_USER and/or DBI_PASS will be used if present, a la DBI

  $Id: storeXML.pl 1445 2010-05-26 14:23:50Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <hgvbase_xmlfile>

  Path to HGVbase XML-file to load.
    
=back

=head1 OPTIONS

=over

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use

=item -s[tagmap] <stagmap>

  Path to stagmap-file to use, if required (see DBIx::DBStag POD)

=item -f[orce]

  Commit transaction even if errors were encountered during load (usually not advisable!)

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item -n

  Process data and do SQL-inserts, but do NOT commit transaction at the end

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
