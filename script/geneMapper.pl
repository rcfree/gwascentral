use GwasCentral::Base qw(load_config);
use GwasCentral::DataSource::Util qw(DataSources_from_list);
use strict;
use warnings;
my %config = load_config('t/conf/test.conf');

my $ds = DataSources_from_list(['Marker','Feature'],\%config);

my $mds = $ds->{Marker};
my $fds = $ds->{Feature};

my $marker_rs = $mds->dbh->resultset('Marker')->search({status => 'active'});
my $mcount = $marker_rs->count;
my $page_size = 5000;
my $page = Data::Page->new();
$page->total_entries($mcount);
$page->entries_per_page($page_size);

for(my $pcount = 1;$pcount<=$page->last_page;$pcount++) {
	warn "Processing page $pcount/".$page->last_page;
	my $marker_rs = $mds->dbh->resultset('Marker')->search({status => 'active'},{rows=>$page_size, page => $pcount});
	my @markers = $marker_rs->all;
	foreach my $marker(@markers) {
		my $coord = $marker->markercoords->search({'AssemblyName'=>$config{assembly_name} })->single;
		next if !$coord;
		my $mg = $fds->get_mapped_gene("chr".$coord->chr, $coord->start, $coord->stop);
		$coord->update({'mappedgene'=>$mg});
	}
}
