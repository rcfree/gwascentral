#!/usr/bin/perl

# $Id: markerXMLFromdbSNP.pl 1445 2010-05-26 14:23:50Z rcf8 $

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper qw(Dumper);
use Data::Stag qw(:all);
use Getopt::Euclid qw( :minimal_keys );
use Date::Calc qw(Delta_YMDHMS);
use Log::Log4perl::Level;
use FindBin;
use Number::Format qw(format_number);
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::DataImport::Marker::dbSNP;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

my ($day1, $month1,$year1,$hour1,$min1,$sec1) = &getMeanTime;

my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

my $markerImport = HGVbaseG2P::DataImport::Marker::dbSNP->new(
				conf_file => $ARGV{c},				       
				dsn       => $dsn,
				user      => $dbuser, 
				passw     => $dbpassw,
				source 	  => 'dbSNP'
				); 

my $stime = time();				      
$markerImport->process_file($ARGV{xml},$ARGV{trigger});
my $totaltime = time() - $stime;


my ($day2, $month2,$year2,$hour2,$min2,$sec2) = &getMeanTime;
my %marker_stats  = $markerImport->markersProcessed();
my ($D_y,$D_m,$D_d, $Dh,$Dm,$Ds) = Delta_YMDHMS(
             $year1,$month1,$day1, $hour1,$min1,$sec1,  
		     $year2,$month2,$day2, $hour2,$min2,$sec2);

$logger->info("Time taken to process $marker_stats{Total_Markers} Markers = ");
$logger->info(sprintf("%02d", $D_d).'/'
    . sprintf("%02d", $D_m).'/'
    . sprintf("%04d", $D_y).' '
    . sprintf("%02d", $Dh) . ':'
    . sprintf("%02d", $Dm) . ':'
    . sprintf("%02d", $Ds));

$marker_stats{'Performance'} = format_number(($marker_stats{Total_Markers}||0)/$totaltime, 1).' markers/s';
$logger->info( "Stats:\n".Dumper(\%marker_stats));

exit 0; 

#------------------------------------
# END OF MAIN SCRIPT
#------------------------------------

 
sub getMeanTime {
    my $time =time;
    my ($Second, $Minute, $Hour, $Day, $Month, $Year, $WeekDay, $DayofYear, $IsDST) = gmtime($time);
    $Year+=1900; $Month+=1;
    
    return $Day, $Month, $Year, $Hour, $Minute, $Second;
}

__END__

=head1 NAME
 	- parses XMl files
 	- fetches data from DB using DBIx::DataStag
 	- compares the data
 	- writes into XML
    
=head1 VERSION

  0.1

=head1 DESCRIPTION

	- parses the genomic information for each marker.
  	- for each marker grabs the information present in the DB (HGVbase release 16) 
  	- if it exists compares data and then writes to an XML flagging it appropiately
  	- if it doesnt exists it writes it into an XML for it to be inserted into the DB
  	
=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item -xml[n] [=] <xml> 

  takes one dbSNP (.xml.gz) file for processing the file name should have 'ch'
  
=item -t[rigger] [=] <trigger> 

  dbSNP build to be processed 
  
=back

=head1 OPTIONS

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use
  
=over

=item -v[erbose]

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Pallavi Sarmah <pallabi2k@gmail.com>>

=head1 BUGS

  !!!!!
    
=cut

