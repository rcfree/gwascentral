#!/usr/local/bin/perl -w

# $Id: importData.pl 1558 2010-10-28 10:08:26Z rkh7 $

# Process frequency/association experiment data from a source using specified template and load into DB

use strict;
use warnings;
use English qw( -no_match_vars );

use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Data::Dumper;
use HGVbaseG2P::Core qw(init_logger load_module);
use HGVbaseG2P::DataImport::Core;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code (where the options are specified)
use Getopt::Euclid qw( :minimal_keys );



# Initialize configuration & logger
my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

# Adjust logging level to verbose
#$ARGV{v} and $logger->level($DEBUG);


# Build arguments to pass to database module.
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

# TODO have flag indicating whether to continue on errors or not?

# Initialize importer class
$logger->info("Initializing processing module for source $ARGV{template}");
my $importclass = 'HGVbaseG2P::Validation::Core';
#print Dumper (\@INC);

#TODO: Use config setting for location of template files
my $importer = HGVbaseG2P::DataImport::Core->new({conf_file => $ARGV{c},
				 sourcefile => $ARGV{sourcefile},
				 argv      => \%ARGV,
				 template_file => $ARGV{t},
});

# Process source input file
$logger->info("Processing input file $ARGV{sourcefile}");
$importer->process_file($ARGV{sourcefile});

$logger->info("lines successful:\n".$importer->marker_success);

my $marker_stats  = $importer->marker_failed;
$logger->info("lines failed:\n".scalar(@{$marker_stats}));

$logger->info("stats/errors:\n");
$logger->info("KEY: n - no marker found, s - strand not available, r - replacement dead, m - merged, d - deleted, f - flipped, a - no-match alleles");
foreach my $failed(@{$marker_stats}) {
	print $failed->line_no.": ".$failed->line->{accession}."\t".join("",map { "$_" } keys %{$failed->status})."\n";
}

exit 0;

__END__


=head1 NAME

  - Import data in text file into database using specified template and configuration

=head1 VERSION

  0.1

=head1 DESCRIPTION


  $Id: importData.pl 1558 2010-10-28 10:08:26Z rkh7 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over


=item <sourcefile>

  Path to source datafile to transform

=item -t[emplate] <source>

  Name of source (e.g. dbSNP, CGEMS, Broad). These templates are currently stored in conf/import/

=back

=head1 OPTIONS

=over

=item  -st[udyid] <studyid>

  StudyID for the study this case-control experiment should be added to

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item -n

  Process data and do SQL-inserts, but do NOT commit transaction at the end

=item -l

  Show input and output line Dumps for each line

=item -r[ef] <ref>

  Limit by reference ie. chromosome number

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author

=cut
