#!/usr/local/bin/perl -w 

# $Id: indexForTextsearch.pl 1481 2010-06-21 16:17:31Z rcf8 $

# Create indexes for full-text searching

use strict;
use warnings;

use Carp qw(cluck croak confess);
use File::Basename;
use File::Path;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Core qw(load_config);
use Data::Dumper;
use Storable qw(freeze);
use Search::Xapian qw(:standard);
use Term::ReadKey;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::DataSource::Study;
use HGVbaseG2P::AccessControl::None;

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

# Initialize logger
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init(
	{
		level => $ARGV{verbose} ? $DEBUG : $INFO,
		layout => "%L:%p> %m%n",
		file   => 'STDERR'
	}
);

my %CONFIG;
if ( exists $ARGV{c} && -f $ARGV{c} ) {
	INFO("Loading config options from $ARGV{c}");
	%CONFIG = load_config( $ARGV{c} );
}
else {
	croak("Must have config file to get info on how to index databases");
}

# Print out list of databases if asked
my $searchconfig  = $CONFIG{'Search'};
my @all_databases = keys %{ $searchconfig->{Query} };
if ( $ARGV{list} ) {
	print "Databases configured in $ARGV{conf}:\n";
	foreach my $dbname (@all_databases) {
		printf( "  %s (type=%s)\n",
			$dbname, $searchconfig->{Query}->{$dbname}->{type} );
	}
	exit 0;
}

my $dbpassw = $ARGV{passw};
if ( !$CONFIG{Database}->{Study}->{pass} && $ARGV{passw} ) {
	INFO("No password provided in config file, reading one from prompt");
	print STDOUT "Enter password:";
	ReadMode('noecho');
	$dbpassw =
	  ReadLine(0);  # Read input from user, keystrokes are NOT shown on terminal
	ReadMode(0);
	chomp($dbpassw);
}

my $schema = HGVbaseG2P::DataSource::Study->new( { conf_file => \%CONFIG } );
$schema->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>\%CONFIG}));
# Get list of databases to do. If not provided on cmdline, -a option will do all db's in configfile.
my $index_basedir = $ARGV{dbdir}
  || "$FindBin::Bin/../" . $searchconfig->{index_basedir};
  INFO "Index basedir:$index_basedir";
-d $index_basedir or die "index_basedir '$index_basedir' not valid";
my @databases = @{ $ARGV{databases} || [] };
map { $_ = ucfirst($_) } @databases;
if ( @databases == 0 ) {
	if ( $ARGV{all} ) {
		INFO(
"No dblist provided, but -a option present so doing all dbs in configfile"
		);
		@databases = @all_databases;
	}
	else {
		WARN("No dblist provided and no -a option, so aborting mission");
		exit 0;
	}
}
INFO( "Creating/updating ", scalar(@databases), " databases" );
DB:
foreach my $dbname (@databases) {
	my $conf = $searchconfig->{Query}->{$dbname}
	  || die "No config available for database '$dbname', aborting";
	unless ( $conf->{type} eq 'Xapian' ) {
		DEBUG(
"Database type '$conf->{type}' not handled by this script, skipping database '$dbname'"
		);
		next DB;
	}
	INFO("Doing Xapian database $dbname: '$conf->{display_plural}'");

	# Configure search engine
	my $stemmer = Search::Xapian::Stem->new('english');
	my $dbdir   = $index_basedir . '/' . lc($dbname);
	DEBUG("Creating database index in $dbdir");
	-d $dbdir or mkpath( $dbdir, 0, 0775 );
	my $db =
	  Search::Xapian::WritableDatabase->new( $dbdir, DB_CREATE_OR_OVERWRITE );

	# Crawl the given website URL and index all pages
	if ( my $url = $conf->{url2crawl} ) {
		DEBUG("Crawling website at $url");
		my $mech = WWW::Mechanize->new( autocheck => 1 );
	}

	# Retrieve data via DBIx::Class middleware
	elsif ( my $dbclass = $conf->{class} ) {
		my $method_name =  $conf->{retrieve_method} || 'get_all_'.lc($conf->{display_plural});
		INFO "Using '$method_name' method to get data";
		my @rset = $schema->$method_name;
		my $i    = 0;
		foreach my $obj (@rset) {
#			INFO( "Object identifier:".($obj->identifier ? $obj->identifier : " none"));
			$i++;
			$i % 100 == 0 and print STDERR "\r$i entries processed";
#			INFO( "Got node #$i: " . $obj->identifier() . ":" . $obj->name );

			my $doc = Search::Xapian::Document->new();
			foreach my $field ( split( /\s/, $conf->{fields2index} ) ) {
				my $pos = 0;

		# Populate db-document with terms, after splitting sentences into tokens
				my $value = $obj->$field;
				
				#recursive processing of classes from database
				process_data($value,$field,$conf,$stemmer, $doc, $pos);
			}
			
			#only store base class identifier
			$doc->set_data( $obj->identifier );
			$db->add_document($doc);
		}
		$db->flush();
		print STDERR "\n";
		undef $db;
		INFO("Indexed $i entries for database $dbname");
	}
	else {
		ERROR("Cannot deal with database $dbname");
	}
}


sub process_data {
	my ($value,$field,$conf,$stemmer, $doc, $pos)=@_;
	return if !$value;
	
	#print ref($value)."\n";
	if (ref($value) =~ /DBIx/ || ref($value) =~ /HGVbaseG2P/) {
		if ($value->isa('DBIx::Class')) {
#			INFO("  Got related table ".$field);
			if (!$conf->{$field."_fields"}) {
				die "Fields table:'$field"."_fields not found";
			}
			my @fields = split (/\ /,$conf->{$field."_fields"});
			
			if (!$value->can('next')) {
				foreach my $temp_field(@fields) {
					my $value2 = $value->$temp_field;
					process_data($value2,$temp_field,$conf,$stemmer, $doc, $pos);
				}
			}
			else {
				while (my $related = $value->next) {
					
					foreach my $temp_field(@fields) {
						my $value2 = $related->$temp_field;
						process_data($value2,$temp_field,$conf,$stemmer, $doc, $pos);
					}
				}
			}
		}
	}
	else {
#		INFO("  Got $field => $value");
		parse_words($stemmer, $doc, $value ,$pos);
	}
}
	
sub parse_words {
	my ($stemmer, $doc, $value, $pos)=@_;
	foreach my $w ( split( /[\s+,\|,\n+,\,,\),\(,-]/, $value ) )
				{
					$pos++;
					next if $w eq '' || $w =~ /(NULL|\||\s)/;
					my $term = $stemmer->stem_word( lc($w) );
					#INFO(
#"    Indexing term '$term' (lc+stemmed '$w'), at pos $pos"
#					);
					$doc->add_posting( $term, $pos );
				}
}

exit 0;

__END__


=head1 NAME

  - Create/update the boolean keyword search indexes with data from database

=head1 VERSION

  0.3

=head1 DESCRIPTION

  This script retrieves objects of several types from the master database, 
  extracts certain pieces of data from them and creates/updates the free-
  text indexes with this information.

    The search engine is currently Xapian  (http://www.xapian.org),
  but this may change in the future.

  $Id: indexForTextsearch.pl 1481 2010-06-21 16:17:31Z rcf8 $

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]


=head1 OPTIONS

=over

=item <databases>

  List of databases to update

=for Euclid:
  repeatable  

=item -a[ll]

  If no db-list provided, then create/update all database-indexes in configfile

=item -db[dir] <dbdir>

  Basedir for text-search indexes (each db goes in its own subdir)  

=item -l[imit] <limit>

  Limit results from SQL-query to database to this many rows (for debugging purposes)

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onf[ig]] <config>

  Path to config file to use in place of the default one.

=for Euclid:
        config.default: 'conf/hgvbase.conf'

=item -l[ist]

  List all available databases in config file

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
