#!/usr/bin/perl

# Tab-delimited data for Type 2 Diabetes from Broad mapped to desired genome build using liftover

#Script starts here
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use Data::Dumper;
# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

use HGVbaseG2P::Core qw(initLogger openInfile openOutfile);
# Initialize logger
use Log::Log4perl qw(:easy);

# Initialize configuration & logger
my $logger = initLogger("$FindBin::Bin/../conf/log4perl.conf");


#the variable for the file
my $infile = $ARGV{infile};

#variable for chain file
my $chain_file = $ARGV{chainfile};

#open the file using the method from Util.pm
my $fh = openInfile($infile);
my (@liftover_results);
while (<$fh>)
{
	next if /^CHR/;
	chomp;


	my (
		 $chr,     $pos,    $rsid,     $z_pval,   $z_score, $or_cmh,
		 $l95,     $u95,    $a1,       $a2,       $maf,     $f_a,
		 $f_u,     $geno,   $geno_a,   $geno_u,   $p_hwd,   $p_hwd_a,
		 $p_hwd_u, $f_miss, $f_miss_a, $f_miss_u, $p_miss,  $gene_list
	) = split( /\s+/, $_ );




	#check to only process rs id at the moment (other values are present)
	if ( $rsid =~ /\w_\w+/ )
	{
		next;
	} else
	{

		#start stop for the lift over
		my $start = $pos;
		my $stop  = $pos + 1;

		#$liftover line);
		my $liftover_line = "chr$chr\t$start\t$stop\t$rsid\n";    
		push @liftover_results, $liftover_line;
	}
}
my ($liftover_mapped,$liftover_unmapped) = liftover( \@liftover_results, $infile,$chain_file );

#parse the results mapped
my $mapped_liftover = liftover_parse($liftover_mapped);
my %mapped_hash = %$mapped_liftover;


#parse the results unmapped
my $unmapped_liftover = liftover_parse($liftover_unmapped);
my %unmapped_hash = %$unmapped_liftover;


#open the file using the method from Util.pm
my $fh2 = openInfile($infile);

#open an outfile
my $outfh = openOutfile("$infile".".liftover");
my $errorfh = openOutfile("$infile".".liftover_err");
my $skippedfh = openOutfile("$infile".".liftover_skipped");

#arrays for printing
my (@new_file,@error_file,@skipped);

#set the counters 
my $skipped_count = 0;
my $liftover_count =0;
my $liftover_error = 0;
while (<$fh2>)
{
	next if /^CHR/;
	chomp;


	my (
		 $chr,     $pos,    $rsid,     $z_pval,   $z_score, $or_cmh,
		 $l95,     $u95,    $a1,       $a2,       $maf,     $f_a,
		 $f_u,     $geno,   $geno_a,   $geno_u,   $p_hwd,   $p_hwd_a,
		 $p_hwd_u, $f_miss, $f_miss_a, $f_miss_u, $p_miss,  $gene_list
	) = split( /\s+/, $_ );


	#check to only process rs id at the moment (other values are present)
	if ( $rsid =~ /\w_\w+/ )
	{
		push @skipped, "$_\n";
		$skipped_count++;
	
	} else
	{

		if (exists ($mapped_hash{$rsid})){
			
			$liftover_count++;
		my 	$value = $mapped_hash{$rsid};
			#print "$value\n";
			#print "exists";
			$logger->debug("$rsid has been mapped");
			my ($liftover_chr,$liftover_start)=split (/\s+/,$value);
			
			$liftover_chr=~/chr(\S+)/;
			$liftover_chr=$1;
			#print $outfh "$liftover_chr\n";
			
		push @new_file, "$liftover_chr\t$liftover_start\t$rsid\t$z_pval\t$z_score\t$or_cmh\t$l95\t$u95\t$a1\t$a2\t$maf\t$f_a\t$f_u\t$geno\t$geno_a\t$geno_u\t$p_hwd\t$p_hwd_a\t$p_hwd_u\t$f_miss\t$f_miss_a\t$f_miss_u\t$p_miss\t$gene_list\n";
			
		}else{

			
			if (exists ($unmapped_hash{$rsid})){
				$logger->info("$rsid has been unmapped in the current build");
				$liftover_error++;
		   push @error_file, "$chr\t$pos\t$rsid\t$z_pval\t$z_score\t$or_cmh\t$l95\t$u95\t$a1\t$a2\t$maf\t$f_a\t$f_u\t$geno\t$geno_a\t$geno_u\t$p_hwd\t$p_hwd_a\t$p_hwd_u\t$f_miss\t$f_miss_a\t$f_miss_u\t$p_miss\t$gene_list\n";
			
				
			}
		
		
		}

		
		
	}

}

print $outfh @new_file;
print $errorfh @error_file;
print $skippedfh @skipped;

$logger->info("\n###liftover stats###\nliftover => $liftover_count\nliftover unmapped => $liftover_error\nskipped liftover => $skipped_count\n###liftover stats###");


sub liftover{
	
	my ($liftover_line,$infile,$chain_file)=@_;
my @liftover = @$liftover_line;

	my $new_file ="$infile".".mapped";
	my $unmapped = "$infile".".unmapped";
	my $input = "$infile".".input";
	open FILE, ">$input" or die "cannot open $input";
	print FILE @liftover;
	close FILE;
	
	system ("/Users/rob/Documents/tools/liftover/liftOver $input $chain_file $new_file $unmapped");
	
	return ($new_file,$unmapped);
	
}


sub liftover_parse{
	
	my ($liftover_file)=@_;
	
my %results_hash;
	
	open FILE, $liftover_file or die "cannot open $liftover_file\n";
	
	while (<FILE>){
		my ($chr,$start,$stop,$rsid)=split( /\s+/, $_ );
		#create a hash
		$results_hash{$rsid}="$chr\t$start";
		
	}
	
return \%results_hash;
}

__END__


=head1 NAME

  - Tab-delimited data for Type 2 Diabetes from Broad lifted over to the desired build

=head1 VERSION
  
  
  0.1

=head1 DESCRIPTION

  This script reformats the tab-delimited allele and genotype frequency datafiles
  from Broad WGA Type 2 Diabetes Scan with the new liftover position and chr
  
  
  writes the tsv files to a *.liftover file
  writes the liftover errors to *.liftover_err
  writes the rsid skipped to *.liftover_skipped (these marker rsids were not lifted over)

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <infile>

  Tab-delimited freq-file to process
  
=item <chainfile>

	ucsc liftover chain file


=back

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -o[ptionfile] <file>
  
  File option

=for Euclid:
        file.type:    readable
        file.default: '?'

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Robert Hastings (rkh7@le.ac.uk)

=head1 BUGS

  None known, but please report any to the author
    
=cut