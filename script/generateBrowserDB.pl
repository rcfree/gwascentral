#!perl

# $Id$

# create Browser marker data table and binned data from Study/Marker database

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use GwasCentral::Browser::Creator;
use Env;
$ENV{DBIC_TRACE}=0;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

#LOGDIE "No study ID or ID range supplied" if !$ARGV{study};

my $creator = GwasCentral::Browser::Creator->new({
	conf_file=>$ARGV{c},
	studies=>$ARGV{studies},
	region=>$ARGV{r},
	genome=>$ARGV{g},
	markers=>$ARGV{m},
	list=>$ARGV{l},
	populate => $ARGV{p},
	all => $ARGV{a},
	resultsets=>$ARGV{rs},
	init => $ARGV{i},
	noaction=>$ARGV{n}
});
$creator->create();

__END__


=head1 NAME

  - Create mart from HGVbaseG2P database using a mart config script.

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script creates or updates the Browser DB
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item -s[tudies] <studies>

  Study identifier or identifier range for study (enter just number not HGVST.. e.g. 4-50). Automatically switches on -g -r -m and -l
  
=item -c[onfigfile] <file>
  
  HGVbaseG2P config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/main.conf'

=item -i[nit]

  Initialise database (ie. drop tables and remove data!)
       
=item -m[arker]
  
  Copy sig data to browser DB
  
=item -r[egion]

  Precalculate 1mb binned region data in browser DB

=item -g[enome]

  Precalculate 3mb binned genome chromosome data in browser DB

=item -l[ist]

  Generate marker significances list

=item -a[ll]
  
  All markers generated - not just those changed in selected studies
  
=item -p[opulate]

  Populate marker significances list
 
=item -rs[et] <rset>

  Resultset identifiers (separated by comma) or identifier range (separated by -)

=item -n[oaction]

  Do not actually do anything just test!

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
