#!/usr/local/bin/perl -w 

# $Id: generateMarkerSummaries.pl 1445 2010-05-26 14:23:50Z rcf8 $

# Create various summaries of marker data in the database, such as 
# total tallies per source, and per marker type and such

use strict;
use warnings;

use lib 'lib';
use HGVbaseG2P::DataImport::Marker::dbSNP;
use HGVbaseG2P::Core qw(loadConfig initLogger openOutfile);

use Number::Format qw/:subs/;
use POSIX qw(strftime);
use Config::General;
use Cwd;
use Log::Log4perl::Level;
use FindBin;
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

# NB this module handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );

my $logger = initLogger("$FindBin::Bin/../conf/log4perl.conf", basename($0));

my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $host 	= ";host=hekla.bioc.le.ac.uk";
my $dbpassw = $DBI_PASS;

if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}
$logger->info("Connecting to Database") ;

my $markerImport = HGVbaseG2P::DataImport::Marker::dbSNP->new(
			      conf_file => $ARGV{c},
				dsn       => $dsn,
				user      => $dbuser, 
				passw     => $dbpassw,
				); 

my $sql = "select VariationType, count(*) from Marker  where Source = 'dbSNP' and Status = 'active' group by VariationType";

my $outputFH 	= openOutfile("MarkerSummaries.html") ;
my $statement 	= $markerImport->dbh->prepare($sql);
$statement->execute();
my $n=0;
print $outputFH "<html><head><title>Marker Summary</title></head><table border=\"1\">";
print $outputFH "<tr><td>Variation Type </td><td> Count </td></tr>";

while ( my @markers = $statement->fetchrow_array ) {
	$logger->debug("$markers[0]=>$markers[1]");
	my $marker = $markers[0] eq 'tandem_repeat' ? 'Microsatellite'
				   : $markers[0] eq 'MNP' ? 'Multinucleotide-Polymorphism'
				   : $markers[0] eq 'CNV' ? 'Copy Number Variation'
				   : $markers[0] eq 'complex_substitution' ? 'Heterozygous, Named-Locus, Mixed'
				   : $markers[0] eq 'indel' ? 'In-Del'
				   : $markers[0];
	print $outputFH "<tr><td>$marker</td><td>".$markers[1]."</td></tr>";
}
print $outputFH "</table></html>";

$logger->info("Selection done");

exit 0;

__END__

=head1 NAME
 
  - Back up multiple databases

=head1 VERSION

  0.1

=head1 DESCRIPTION

  Generate summaries of marker data in the HGVBaseG2P database.

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=item -c[onfig] <config>

  Path to config file to use

=over


=back

=head1 OPTIONS

=item -d[sn] <dsn>

  Database string to connect to (overrides the one given in cfgfile)

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file
  
=item -o[output_dir]

  Directory to create and dump database to (overrides cfgfile)

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
