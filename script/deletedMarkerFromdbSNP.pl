#!/usr/local/bin/perl -w

# To find the Markers which are not present in the current database ie the markers which got deleted in between b126-b129

use strict;
use warnings;

use lib 'lib';
use HGVbaseG2P::DataImport::Marker::dbSNP;

use Data::Dumper qw(Dumper);
use Data::Stag qw(:all);
use Getopt::Euclid qw( :minimal_keys );
use Date::Calc qw(Delta_YMDHMS);
use Log::Log4perl::Level;
use FindBin;
use Number::Format qw(format_number);
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use File::Basename;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);
use Data::Stag::XMLWriter;
use HGVbaseG2P::Core qw(initLogger openOutfile openInfile);
use Log::Log4perl::Level;
use POSIX qw(strftime);

# Initialize configuration & logger
my $logger = initLogger("$FindBin::Bin/../conf/log4perl.conf", basename($0));
# Adjust logging level to verbose if required
$ARGV{v} and $logger->level($DEBUG);

# Initialize configuration & logger


# database connectivity if configuration file is not used
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;

if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

my $time = strftime "%Y-%m-%d %H:%M:%S", localtime ;
my $markerImport = HGVbaseG2P::DataImport::Marker::dbSNP->new(
							conf_file => $ARGV{c},       
							dsn       => $dsn,
							user      => $dbuser, 
							passw     => $dbpassw,
							source   => 'dbSNP'
							); 




# Open output file 

my $outfn = "./deletedMarkers/markers_dummyDeleted".($ARGV{build} and "_$ARGV{build}")."_".basename($ARGV{xml});
my $outfh = openOutfile($outfn);


my $writer = Data::Stag::XMLWriter->new(-fh => $outfh);
$writer->start_event('top');

$writer->start_event("Hotlink");
$writer->start_event("@");
$writer->event(id =>'dbsnp_hotlink');
$writer->end_event("@");
$writer->event("HotlinkLabel", "dbSNP refSNP");
$writer->event('UrlPrefix', 'http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs=');
$writer->end_event("Hotlink");

my %stats;

my $h = Data::Stag->makehandler(Rs => sub {
    my ($handler, $node) = @_;
    my $rsid = "rs".$node->get('@')->get("rsId");

    my $markerset = $markerImport->retrieve_marker_from_DB($rsid);
    my $marker = $markerset->get("Marker");
    $logger->debug("select done !!");
    if ( !defined $marker ) {
        $logger->debug("$rsid not found in the database");
	$stats{Deleted}++;
	 $logger->info("the Marker is not present in the database hence a dummy deleted marker,". 
		       "grab the whole information from the XML and create a marker and ".
		       "markerRevision node and write to XML");
	 my $marker_XML = $markerImport->parse_genomic_data($node); 
	$marker_XML->set("AddedFromSourceBuild", $ARGV{trigger});
	$marker_XML->set("TimeLastTouched", $time);
	$marker_XML->set("Status", "dead");
	 my $markerRevision = stag_nodify(
					  [
					   MarkerRevision => [
							      [ '@' => [ [ op => 'insert' ] ] ],
							      [ ChangeType    => "Deletion" ],
							      [ ChangeTrigger => $ARGV{trigger}],
							      [ TimeCreated   => $time ]
							      ]]);
	 $marker_XML->addkid($markerRevision);
	 $marker_XML->events($writer);
     }else {
	 $stats{TotalMarkers}++;
	 $logger->debug("skipping Marker $rsid - already present in the database");
     }
});

my $fn = $ARGV{xml};
$logger->debug("filename $fn");
my $stime = time();


$markerImport->parseFile(filename => $fn,
                  filetype => 'xml',
                  handler  => $h);		

$writer->end_event('top');

my $totaltime = (time() - $stime);
$stats{'Performance'} = format_number(($stats{TotalMarkers}||0)/$totaltime, 1).' markers/s';
$logger->info(Dumper(\%stats));

exit 0;

__END__


=head1 NAME

  - To find the deleted markers from b126-b129 and create a XML for loading into the database

=head1 VERSION

  0.1

=head1 DESCRIPTION

  To iterate through each chr for b126- b129 and find the markers which are no longer active in 
  the current database. Create an XMl file with Marker node and Marker Revision node for loading 
  into the database

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=item -xml[n] [=] <xml>

  takes one dbSNP (.xml.gz) file for processing the file name should have 'ch'

=item -t[rigger] [=] <trigger>

  current dbSNP buiild (b129 as on Apr 2008)

=item -b[uild] [=] <build>

  dbSNP build to be processed

=back

=head1 OPTIONS

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use

=over

=item -v[erbose]

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Pallavi Sarmah <pallabi2k@gmail.com>>

=head1 BUGS

  !!!!!

=cut
