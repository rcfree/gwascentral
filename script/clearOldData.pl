#!perl

# $Id: clearOldData.pl 1528 2010-09-16 13:48:46Z rcf8 $

# clears uploads older than 2 days

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use HGVbaseG2P::Database::BrowserUpload;
use HGVbaseG2P::Core qw(load_config);
use HGVbaseG2P::Browser::Util qw(neglog);
use Log::Log4perl qw(:easy);
use Math::Round qw(nearest_floor);

Log::Log4perl->easy_init($INFO);

use Data::Dumper qw(Dumper);

# NB this module here handles commandline-option processing, according
# to the POD documention below the code.
use Getopt::Euclid qw( :minimal_keys );
my $days = 2;
my $conf_file  = $ARGV{c};

my %config     = load_config($conf_file);
my $SUMMARY_NO = 100000;

my $upload_db   = HGVbaseG2P::Database::BrowserUpload->new(
	{ conf_file=>\%config }
);

$upload_db->delete_old_uploads($days);


my $dir = $config{'tempdir'}."/uploads";
opendir(DIR,$dir) || die "Can't open $dir : $!\n";
print "Deleting uploads from ".$dir."\n";
my @files = readdir(DIR); # you may want to grep only certain files here
close(DIR);

my $seconds = $days * 86400;
foreach my $file(@files)
{
	my $now = time;
	my @stat = stat("$dir/$file");
	if ($stat[9] < ($now - $seconds))
	{
		print "Deleting $dir/$file...";
		unlink("$dir/$file");
		print "Done.\n";
	}

}

__END__


=head1 NAME

  - Clear old uploaded data more than 2 days old

=head1 VERSION

  0.1

=head1 DESCRIPTION

  This script removes uploaded data (both database and temporary files) more than 2 days old (generally run as a cron job)
 
  $Id

=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 OPTIONS

=over

=item -v[erbose]

  Set logging to verbose (Log4perl level of 'DEBUG')
  
=item -c[onfigfile] <file>
  
  HGVbaseG2P config file to use

=for Euclid:
        file.type:    readable
        file.default: 'conf/hgvbase.conf'
         
=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Rob Free <rcfree@gmail.com>>

=head1 BUGS

  None known, but please report any to the author
    
=cut
