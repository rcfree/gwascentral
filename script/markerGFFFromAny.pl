#!/usr/bin/perl

# $Id: markerGFFFromAny.pl 1515 2010-07-13 13:52:11Z rcf8 $

# Transform a marker source datafile into standard marker GFF feature format 

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";
use Data::Dumper qw(Dumper);
use Data::Stag qw(:all);
use Getopt::Euclid qw( :minimal_keys );
use Date::Calc qw(Delta_YMDHMS);
use Log::Log4perl::Level;
use FindBin;
use Number::Format qw(format_number);
use Time::HiRes qw( time );
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/lib";
use File::Basename;

use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::MarkerImport::dbSNP;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

my ($day1, $month1,$year1,$hour1,$min1,$sec1) = &getMeanTime;

my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));
$ARGV{verbose} and $logger->level($DEBUG);


my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}


# Assume naive mode if no database connection params nor config file are provided
if(!($dsn || $ARGV{c})) {
    $logger->info("No database connection params or config file given, so defaulting to naive mode.");
    $ARGV{naive} = 1;
}

# ToDo: replace w/ load class dynamically, depending on db type OR plugin mechanism
# modelled after GBrowse and friends OR use same strategy as RobF's DataImport plugins.

my $markerImport = HGVbaseG2P::MarkerImport::dbSNP->new(
				conf_file => $ARGV{c},				       
				naive     => $ARGV{naive},				       
				dsn       => $dsn,
				user      => $dbuser, 
				passw     => $dbpassw,
                source 	  => $ARGV{source},
				); 

my $stime = time();
$markerImport->process_file($ARGV{infile},$ARGV{label});
my $totaltime = time() - $stime;


my ($day2, $month2,$year2,$hour2,$min2,$sec2) = &getMeanTime;
my %marker_stats  = $markerImport->markersProcessed();
my ($D_y,$D_m,$D_d, $Dh,$Dm,$Ds) = Delta_YMDHMS(
             $year1,$month1,$day1, $hour1,$min1,$sec1,  
		     $year2,$month2,$day2, $hour2,$min2,$sec2);

$logger->info("Time taken to process $marker_stats{Total_Markers} Markers = ");
$logger->info(sprintf("%02d", $D_d).'/'
    . sprintf("%02d", $D_m).'/'
    . sprintf("%04d", $D_y).' '
    . sprintf("%02d", $Dh) . ':'
    . sprintf("%02d", $Dm) . ':'
    . sprintf("%02d", $Ds));

$marker_stats{'Performance'} = format_number(($marker_stats{Total_Markers}||0)/$totaltime, 1).' markers/s';
$logger->info( "Stats:\n".Dumper(\%marker_stats));

exit 0; 

#------------------------------------
# END OF MAIN SCRIPT
#------------------------------------

 
sub getMeanTime {
    my $time =time;
    my ($Second, $Minute, $Hour, $Day, $Month, $Year, $WeekDay, $DayofYear, $IsDST) = gmtime($time);
    $Year+=1900; $Month+=1;
    
    return $Day, $Month, $Year, $Hour, $Minute, $Second;
}

__END__

=head1 NAME

  - Transform a marker source datafile into standard marker GFF feature format
    
=head1 VERSION

  0.1

=head1 DESCRIPTION

  This application is one-half of the successor to the DBIx::DBStag based dbSNP marker 
  processing pipeline. It takes as input a datafile from a source marker database, 
  such as dbSNP, and transform into a minimal or "lite" version in standard GFF3 
  feature format. Optionally, if database connection parameters are provided, the
  application will connect to a target HGVbaseG2P marker database and perform a
  comparison of the input data with existing data in the database. In this case, 
  multiple GFF files are produced, one for each of the following: new, changed, 
  unchanged or deleted markers.

  The resulting GFF3 files are suitable for loading into the target HGVbaseG2P 
  database with a seperate GFF-loader [no name yet].


  One major motivation for the strategy above is that we want to produce a set of
  marker datafiles which are useful to others (notably 'dbSNP Lite' as requested by 
  the UMAN group in GEN2PHEN). Furthermore, this tool is intended to be used by 
  others to extend and use on other marker data sources which we are not interested
  in. The previous marker import pipeline and XML intermedia formats did not lend
  themselves to either of these things.

  [ToDo: add notes on GFF output files]


  	
=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item  <infile> 

  Input datafile to process. Most compression formats will be handled automatically.

=item -s[ource] [=] <source> 
  
  The name of the marker datasource (e.g. dbSNP).

=back

=head1 OPTIONS

=over
  
=item -n[aive] 

  Run in naive mode and do not connect to a target database.

=item -l[abel] [=] <label> 

  Optional label for this processing job, typically the build no. of the source database (optional).  

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -c[onfig] <config>

  Path to config file to use
  
=item -v[erbose]

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to the author.

=head1 CONTACT

This module is part of the HGVbaseG2P project

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: markerGFFFromAny.pl 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut
