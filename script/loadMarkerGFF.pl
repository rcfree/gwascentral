#!/usr/bin/perl

# $Id: loadMarkerGFF.pl 1515 2010-07-13 13:52:11Z rcf8 $

# Load a set of standard marker GFF3 feature file into the Marker database.

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper qw(Dumper);
use Getopt::Euclid qw( :minimal_keys );
use Log::Log4perl::Level;
use FindBin;
use Time::HiRes qw( time );
use Term::ReadKey;
use lib "$FindBin::Bin/../lib";
use Number::Format qw(format_number);
use File::Basename;
use File::Spec;

# Bring in my modified class
use lib "/home/gt50/cvswork/bioperl-live";
use HGVbaseG2P::GFF3Loader;
use Bio::DB::SeqFeature::Store;
use HGVbaseG2P::Core qw(init_logger);
use HGVbaseG2P::Database::Marker;
use Env qw($DBI_DSN $DBI_USER $DBI_PASS);

my $logger = init_logger("$FindBin::Bin/../conf/log4perl.conf", basename($0));
$ARGV{verbose} and $logger->level($DEBUG);



# Get database connection parameters.
my $dsn     = $ARGV{d} || $DBI_DSN;
my $dbuser  = $ARGV{u} || $DBI_USER;
my $dbpassw = $DBI_PASS;
if($ARGV{p}) {
    $logger->info("Reading password from terminal");
    print STDOUT "Enter password for MySQL user $dbuser:";
    ReadMode('noecho');
    $dbpassw = ReadLine(0); # Read input from user, keystrokes are NOT shown on terminal
    ReadMode(0);
    chomp($dbpassw);
}

# Initialize feature store connection which will get used by the loader
my $tmpdir = $ARGV{tmpdir} || File::Spec->tmpdir();
-d $tmpdir && -w $tmpdir or die "Fast loading is requested, but I cannot write into the directory $tmpdir" ;
$logger->info("Initializing feature store for the '".$ARGV{assembly}."' sequence assembly");
my $store = Bio::DB::SeqFeature::Store->new(-dsn     => $dsn,
                                            -adaptor => 'DBI::mysql',
                                            -tmpdir  => $tmpdir,
                                            # ToDo: config option OR auto-detect somehow
                                            -namespace => lc($ARGV{assembly}), 
                                            -user    => $dbuser,
                                            -pass    => $dbpassw,
                                            -write    => 1,
                                            -create   => $ARGV{create},
                                            #-compress => $COMPRESS,
    )
    or die "Couldn't create connection to the database";

# try no-blobs option
$store->no_blobs(1);


# We probably want to enable this at some point, to conveniently wipe the feature-db prior to loading
# $store->init_database('erase') if $CREATE;


# Initialize Marker database connection which will also get used by the loader.
$logger->info("Initialiazing marker database connection");
my $markerdb = HGVbaseG2P::Database::Marker->new({ conf_file => $ARGV{config}});

$markerdb->storage->dbh->{'AutoCommit'} = 0;
$store->dbh->{'AutoCommit'} = 0;
if ($markerdb->storage->dbh->{'AutoCommit'}) {
    $logger->logdie("error when setting dbh->{'AutoCommit'} to 0");
}


# Initialize the modified GFF3 loader which will to the heavy lifting to process the feature data.
$logger->info("Initializing GFF3 loader");
my $loader = HGVbaseG2P::GFF3Loader->new(-store    => $store,
                                         -verbose  => $ARGV{verbose},
                                         -tmpdir   => $tmpdir,
                                         -fast     => 1,
                                         -markerdb => $markerdb,
                                         ## ToDo: autodetect this from GFF header
                                         -type      => $ARGV{type},
                                         -index_subfeatures => 0,
    )
  or die "Couldn't create GFF3 loader";

# on signals, give objects a chance to call their DESTROY methods
$SIG{TERM} = $SIG{INT} = sub {  undef $loader; undef $store; die "Aborted..."; };

# Send the GFF files off to be loaded by the loader
my @gffs = @{$ARGV{gff}};
$logger->info("Loading the following feature files with '".$ARGV{type}."' marker entries:\n   ".join("\n   ", @gffs));
my $starttime = time();
$loader->load(@gffs);
my $totaltime =  time - $starttime;
my %stats = $loader->stats();
$stats{'Performance'} = format_number(($stats{Total}||0)/$totaltime, 1).' markers/s';
$logger->info(Dumper(\%stats));

if($ARGV{n}) {
    $logger->info("No errors encountered during load, but in dry-run mode so NOT committing database transaction");
    $markerdb->storage->dbh->rollback();
    $store->dbh->rollback();
}
else {
    $logger->info("No errors encountered during load, committing clean database transaction");
    $markerdb->storage->dbh->commit();
    $store->dbh->commit();
}



exit 0; 

#------------------------------------
# END OF MAIN SCRIPT
#------------------------------------


=head1 NAME

  - Load a set of standard marker GFF3 feature file into the Marker database
    
=head1 VERSION

  0.1

=head1 DESCRIPTION

  This utility is a thin wrapper around a modified version of the GFF3 loader module
  from the Bio::DB::SeqFeature::Store package. It is inspired by the BioPerl 
  bp_seqfeature_load.pl which drives the original GFF3 loader, but since it is 
  optimized for our use it offers far fewer runtime options and harcodes a  a number 
  of configuration options for the feature store and loader itself.

  [ToDo: say more about the modded Bio::DB::SeqFeature::Store::GFF3Loader module]

  	
=head1 USAGE

  [ignored, usage line is built by Getopt::Euclid from interface-description below]

=head1 REQUIRED ARGUMENTS

=over

=item <gff>...

  One or more input GFF3 feature files to process.

=item -a[ssembly] <assembly>

  Label for the sequence assembly that these features are associated with.

=item -t[ype] <type>

  Type of marker entries in the input GFFs: new, changed, unchanged or deleted. This 
  controls whether the entries are added to the database or existing entries updated.

=back

=head1 OPTIONS

=over
  
=item -n 

  Run in 'dry-run' mode w/o writing anything to the database. This will go through the entire
  data processing procedure and do all database operations, but will not commit the transaction 
  at the end. Useful for debugging.

=item -create

  Create the feature database tables and reinitialize them (will erase contents)


=item -l[abel] [=] <label> 

  Optional label for this processing job, typically the build no. of the source database (optional).  

=item -d[sn] <dsn>

  Database connection string to use instead of the one from config file

=item -u[ser] <dbuser>

  Database username to use instead of the one from config file. If not provided, DBI_USER
  will be taken from the environment if available.

=item -p[assw[ord]]

  Prompt for db-password to use, instead of password from config file

=item -t[mpdir] <tmpdir>

  Temporary directory to write tablefile to during fast load (

=item -c[onfig] <config>

  Path to config file to use
  
=item -v[erbose]

=item --version

=item --usage

=item --help

=item --man

  Print the usual program information

=back

=head1 AUTHOR

  Gudmundur A. Thorisson <gthorisson@gmail.com>

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbaseG2P project

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2009> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: loadMarkerGFF.pl 1515 2010-07-13 13:52:11Z rcf8 $ 

=cut
