#!perl

# $Id: 20_hgvmart_compactor.t 1072 2008-11-03 11:32:17Z mummi $

=head1 NAME

20_hgvmart_compactor.t - Test BioMart Compactor and COMPACT formatter logic


=head1 SYNOPSIS

    prove 20_hgvmart_compactor.t

=head1 DESCRIPTION

This script tests the logic behind the custom Compactor and COMPACT formatter classes in BioMart by
creating 'mock' data and compacting it. Then it examines the compacted data to ensure it is returned in the correct format.
Due to the way BioMart works, it is necessary to actually retrieve data from an actual mart and then replace this with the generated
data.

=cut

package HGVbaseG2Pmart::Test::Compactor;
use strict;
use warnings;
use Data::Dumper qw(Dumper);
use FindBin;
use lib "$FindBin::Bin/../lib";

use BioMart::Initializer;
use BioMart::Query;
use BioMart::QueryRunner;
use Log::Log4perl qw(:easy);
use Clone::Fast qw(clone);
use Test::More qw/no_plan/;

my %config=(); 
Log::Log4perl->easy_init($WARN);
	
my $confFile = "/Users/robf/Projects/hgvbaseG2P/perl/conf/test/martDB.xml";

my $action=$ARGV[1] || 'lazyload';
my $initializer = BioMart::Initializer->new('registryFile'=>$confFile, 'action'=>$action);
my $registry = $initializer->getRegistry;
my $query = BioMart::Query->new('registry'=>$registry,'virtualSchemaName'=>'default');

$query->setDataset("marker");
$query->addAttribute('markerid');
$query->addFilter('hgvmarkerid',['HGVM550']);
	
	
$query->formatter("COMPACT-CSV");
my $runner = BioMart::QueryRunner->new;
	
my $att_page = "marker_genotype_associations";
$runner->execute($query,$att_page);

my $runner_copy;
my $formatter;
my $compactor;

my $mock_rtable_parameters={
	no_in_group1=>0,
	random_group2=>0,
	max_no_in_group2=>3,
	no_in_special_fields=>3,
};
	
test_mock_compaction($mock_rtable_parameters,"check with zero lines");
	
for(my $special_fields=2; $special_fields<=3;$special_fields++) {
				
	my $mock_rtable_parameters={
		no_in_group1=>1,
		random_group2=>1,
		max_no_in_group2=>3,
		no_in_special_fields=>$special_fields,
	};
	test_mock_compaction($mock_rtable_parameters,"check 1-3 assayed-panels with 1 line, $special_fields alleles/genotypes");
	
	for(my $size = 5;$size<=20;$size+=5) {
		
		my $mock_rtable_parameters={
			no_in_group1=>$size,
			random_group2=>1,
			max_no_in_group2=>3,
			no_in_special_fields=>$special_fields,
		};
		test_mock_compaction($mock_rtable_parameters,"check 1-3 assayed-panels with $size lines, $special_fields alleles/genotypes");
	}
		
	for(my $size = 40;$size<=100;$size+=20) {
	
		my $mock_rtable_parameters={
			no_in_group1=>$size,
			random_group2=>1,
			max_no_in_group2=>3,
			no_in_special_fields=>$special_fields,
		};
		test_mock_compaction($mock_rtable_parameters,"check 1-3 assayed-panels with $size lines, $special_fields alleles/genotypes");
	}
}


sub create_query {
	$runner_copy = clone($runner);
	$formatter = $runner_copy->get('formatter');
	$compactor = $formatter->get('compactor');
}

sub test_mock_compaction {
	my ($parameters,$description) = @_;
	
	&create_query;
	my ($data,$counts) = &generate_mock_rtable($parameters);
	
	$formatter->get('result_table')->setRows($data);
	
	ok is_mock_compacted_correctly($parameters,$counts),$description;
}

sub is_mock_compacted_correctly {
	my ($attrs,$counts) = @_;
	my @rtable = ();
				
	my ($results_fh, $results_text);
	
	my @results = ();

	while(my $row = $formatter->nextRow) {
		chomp($row);
		push @results, $row;
	}
	
	my $row_number=0;
	my $expected_values=['HGVbase Marker ID','Chromosome','Marker Start','Marker Stop','P-value','upstream10bp','downstream10bp'];
	foreach my $result (@results){
		#print "<".($counts->[$row_number]).">".$result."\n";
		$row_number++;
		my @row = split(',',$result);
		my $column = 0;
		
		foreach my $column_value(@{$expected_values}) {
			&check_column($row[$column], "{".$column_value.$row_number."}", $column, $row_number);
			$column++;
		}
		
		my @label=();
		for (1..$attrs->{no_in_special_fields}) {
			push @label, "[Genotype Label$row_number.1.$_]";
		}
		&check_column($row[$column], "{upstream10bp".$row_number."}".join(':',@label)."{downstream10bp".$row_number."}", $column, $row_number,0,0);
		$column++;
		
		for my $counter(1..$counts->[$row_number-1]) {
			&check_column($row[$column], "HGVbase Assayed-panel Name".$row_number.".".$counter, $column, $row_number);
			$column++;
			my @label=();
			for (1..$attrs->{no_in_special_fields}) {
				push @label, "Frequency (proportion)$row_number.$counter.$_";
			}
			&check_column($row[$column], join(':',@label), $column, $row_number,0,0);
			$column++;
		}
	}
	return 1;
}

sub check_column {
	my ($actual_value, $column_value, $column_no,$row_number,$prefix,$suffix)=@_;
	
	my $expected_value = $column_value;
	!$actual_value and die "Actual value is null (expected:$expected_value, row: $row_number, col: $column_no)";
	!$expected_value and die "Expected value is null";
	
	if ($actual_value ne $expected_value) { 
		die "FAILED: row: $row_number, col: $column_no ".$actual_value." ne $expected_value\n"; 
	}
}

sub generate_mock_rtable {
	my ($attrs) = @_;
	my @rtable = ();
	
	my $inner_formatter = $formatter->get('formatter');
	my @display_names = $inner_formatter->getTextDisplayNames;
	$compactor->set('full_titles', \@display_names);
	$compactor->generate_field_pos_hash;
	
	my $fields = $compactor->get('field_pos');
	
	my $group_by = $compactor->get('group_by');
	my $group1=$group_by->[0];
	my $group2=$group_by->[1];
	
	my $field_group = $compactor->get('field_groups');
	
	my $special_fields=$compactor->get('special_fields');
	
	if (!$attrs->{no_in_group1}) { return []; };
	my @group2_counts=();
	for my $counter1(1..$attrs->{no_in_group1}) {
		
		my $no_in_group2 = $attrs->{random_group2} ? int(rand($attrs->{max_no_in_group2}))+1 : $attrs->{max_no_in_group2};
		
		for my $counter2(1..$no_in_group2) {
			for my $counter3(1..$attrs->{no_in_special_fields}) {
				my @row=();
				foreach my $column (0..@display_names-1) {
					#print "column:".$column unless $column;
					$row[$column] = "{".$display_names[$column].$counter1."}";
				}
				
				$row[$fields->{$group1}]=$group1."_".$counter1;
				$row[$fields->{$group2}]=$group2."_".$counter2;
				
				foreach my $gfield (@{$field_group->{$group2}}) {
					if ($special_fields->{$gfield}) {
						$row[$fields->{$gfield}]=$gfield.$counter1.".".$counter2.".".$counter3;
					}
					else {
						$row[$fields->{$gfield}]=$gfield.$counter1.".".$counter2;
					}
				}
				push @rtable, \@row;
			}
		}
		push @group2_counts, $no_in_group2;
	}

	return \@rtable, \@group2_counts;
}

1;

=head1 SEE ALSO

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob Free <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob Free <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: 20_hgvmart_compactor.t 1072 2008-11-03 11:32:17Z mummi $

=cut
