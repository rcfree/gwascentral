#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - allelematch rule

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::DataImport::DataElement;
use HGVbaseG2P::DataImport::Rule::AllelesMatch;
use Bio::SeqFeature::Generic;

my $marker2a = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '1',
	-source => 'dbSNP',
	-score => '123',
	-display_name => 'rs123',
	-tag          => {chr => '3', alleles=>[qw(A C)], upstream30bp =>'', downstream30bp =>'' }
);

my $match_de = HGVbaseG2P::DataImport::DataElement->new;
$match_de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'A','allele2'=>'C','strand'=>'+'});

my $nomatch_de = HGVbaseG2P::DataImport::DataElement->new;
$nomatch_de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'T','allele2'=>'C','strand'=>'+'});

my $r1 = HGVbaseG2P::DataImport::Rule::AllelesMatch->new;
$match_de->marker($marker2a);
$r1->run($match_de);
ok !$match_de->status->{a}, "Matching alleles detected ok in 2 alleles";

$nomatch_de->marker($marker2a);
$r1->run($nomatch_de);
ok $nomatch_de->status->{a}, "2 non-matching alleles detected ok in 2 alleles";

$nomatch_de->marker($marker2a);
$nomatch_de->line->{allele1}='G';
$r1->run($nomatch_de);
ok $nomatch_de->status->{a}, "1 non-matching alleles detected ok in 2 alleles";

my $marker3a = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '1',
	-source => 'dbSNP',
	-score => '123',
	-display_name => 'rs123',
	-tag          => {chr => '3', alleles=>[qw(A C T)], upstream30bp =>'', downstream30bp =>'' }
);

$nomatch_de->marker($marker3a);
$r1->run($nomatch_de);
ok $nomatch_de->status->{a}, "1 non-matching allele detected ok in 3 alleles";

$match_de = HGVbaseG2P::DataImport::DataElement->new;
$match_de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'A','allele2'=>'C','strand'=>'+'});

my $match_de2 = HGVbaseG2P::DataImport::DataElement->new;
$match_de2->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'T','allele2'=>'C','strand'=>'+'});
$nomatch_de->status({});

$match_de->marker($marker3a);
$r1->run($match_de);
ok !$match_de->status->{a}, "2 matching alleles detected ok in 3 alleles";

$match_de2->marker($marker3a);
$r1->run($match_de2);
ok !$match_de2->status->{a}, "2 matching alleles detected ok in 3 alleles";

$nomatch_de->marker($marker3a);
$r1->run($nomatch_de);
ok $nomatch_de->status->{a}, "1 non-matching allele detected ok in 3 alleles";


