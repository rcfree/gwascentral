
use FindBin;
use lib "$FindBin::Bin/../../lib";
use Test::Base;
use HGVbaseG2P::Test::Web;

validate_html 'input';

__END__
    
=== Test 1 (index)
--- input chomp
index

=== Test 2 (studies)
--- input chomp
studies

=== Test 3 (phenotypes)
--- input chomp
phenotypes

=== Test 4 (markers)
--- input chomp
markers

=== Test 5 (study/HGVST316)
--- input chomp
study/HGVST316

=== Test 6 (study/HGVST316/panels)
--- input chomp
study/HGVST316/panels

=== Test 7 (study/HGVST316/phenotypes)
--- input chomp
study/HGVST316/phenotypes

=== Test 8 (study/HGVST316/markers)
--- ONLY
--- input chomp
study/HGVST316/markers
