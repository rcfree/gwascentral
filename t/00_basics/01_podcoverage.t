#!perl -w

# $Id: 01_podcoverage.t 1072 2008-11-03 11:32:17Z mummi $

# Test whether any parts of the codebase remain undocumented

use strict;
use warnings;
use Test::More;

eval "use Test::Pod::Coverage 1.04";
plan skip_all => 'Test::Pod::Coverage 1.04 required' if $@;
plan skip_all => 'set TEST_AUTHOR to enable this test' unless $ENV{TEST_AUTHOR};

all_pod_coverage_ok();
