#!perl -w

# $Id: 01_fileparser.t 1072 2008-11-03 11:32:17Z mummi $

# Test all Perl modules compile successfully

use FindBin;
use Data::Dumper qw(Dumper);
use lib "$FindBin::Bin/../../lib";
use Cwd qw(cwd);
use Test::More;
use Env;
$ENV{GWASCENTRAL_HOME}="$FindBin::Bin/../../";
eval "use Test::Compile";
Test::More->builder->BAIL_OUT("Test::Compile required for testing compilation - $@") if $@;

test_perl_modules();
#test_perl_scripts();

sub test_perl_modules {
	my $path = "$FindBin::Bin/../../lib/";
	chdir $path;
	my @files = all_pm_files($path);
	my @modules;
	
	foreach my $file(@files) {
		$file =~ s/$path//;
		push @modules, $file;
	}
	all_pm_files_ok(@files);
}

sub test_perl_scripts {
	my $path = "$FindBin::Bin/../../script/";
	chdir $path;
	my @files = all_pl_files($path);
	my @modules;

	foreach my $file(@files) {
		$file =~ s/$path//;
		push @modules, $file;
	}

	all_pl_files_ok(@files);
}
1;