#!perl -w

# $Id: 00_util.t 1274 2009-08-21 15:15:08Z rcf8 $
 
# Test utility module subroutines

use strict;
use warnings;
use English qw( -no_match_vars );
use Data::Dumper;
use FindBin;
use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use FindBin;
use lib "$FindBin::Bin/../../lib";

use_ok 'GwasCentral::Base', qw(init_logger load_config open_infile open_outfile load_module);

test_init_logger();
test_load_config();
test_open_infile();
test_open_outfile();
test_load_module();

sub test_init_logger {
	# Test logging setup routine
	my $logger;
	lives_ok { $logger = init_logger('./conf/log4perl.conf', 'FooBar')}
	         "Initialize logger from file";
	warn ref $logger;
	isa_ok $logger, 'Log::Log4perl::Logger' , "Logger object is of the proper class";
}

sub test_load_config {
	# Test configuration loading routine
	my %config;
	lives_ok { %config = load_config('./conf/hgvbase.conf'); }
	         "Loaded config from file";
	ok %config, "Config hash is defined";
	ok exists($config{dsn}), "'dsn' option exists in config-hash";
}

sub test_open_infile {
	# Test input-file opening routine
	my $infh_reg;
	lives_ok {  $infh_reg = open_infile("$FindBin::Bin/data/test1.csv") } "Open regular file for reading";
	isa_ok $infh_reg, 'IO::File',"Filehandle is of the proper class";
	can_ok $infh_reg, qw(getline);
	my $infh_gzip;
	lives_ok {  $infh_gzip = open_infile("$FindBin::Bin/data/test2.csv.gz") } "Open gzipped file for reading";
	isa_ok $infh_gzip, 'IO::File',"Filehandle is of the proper class";
	can_ok $infh_gzip, qw(getline);
}

sub test_open_outfile {
	# Test output-file opening routine
	my $outfh_reg;
	lives_ok {  $outfh_reg = open_outfile('/tmp/foobarfoo.dat') } "Open regular file for writing";
	isa_ok $outfh_reg, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_reg, qw(getline);
	lives_ok { print $outfh_reg "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
	my $outfh_gzip;
	lives_ok {  $outfh_gzip = open_outfile('/tmp/foobarfoo.dat.gz') } "Open gzipped file for writing";
	isa_ok $outfh_gzip, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_gzip, qw(getline);
	lives_ok { print $outfh_gzip "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
	my $outfh_zip;
	lives_ok {  $outfh_zip = open_outfile('/tmp/foobarfoo.dat.zip') } "Open zipped file for writing";
	isa_ok $outfh_zip, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_zip, qw(getline);
	lives_ok { print $outfh_zip "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
}

sub test_load_module {
	#test load_module method
	my $modname1 = 'GwasCentral::DataSource::Study';
	my $module;
	lives_ok { $module = load_module($modname1,[conf_file=>"./conf/hgvbase.conf"]); } "Load GwasCentral::DataSource::Study module";
	isa_ok $module, 'GwasCentral::DataSource::Study', "Loaded module is of correct class";
	
	my $modname2 = 'GwasCentral::DataSource::Nonexisting';
	dies_ok { $module = load_module($modname2,[conf_file=>"./conf/hgvbase.conf"]); } "Dies trying to load non-existing module";
}