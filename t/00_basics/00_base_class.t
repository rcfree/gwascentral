#!perl -w

# $Id: 00_util.t 1274 2009-08-21 15:15:08Z rcf8 $
 
# Test utility module subroutines

use strict;
use warnings;
use English qw( -no_match_vars );
use Data::Dumper;
use FindBin;
use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");

use_ok 'MockBase';

my $mock;
lives_ok { $mock = MockBase->new; } "Creation of MockBase works";

test_config();
test_log();
test_throw();
test_init_logger();
test_load_config();
test_open_infile();
test_open_outfile();
test_load_module();

sub test_config {
	can_ok $mock, qw(config);
	is scalar(keys %{$mock->config}),0,"Config is empty when no config file provided";
	$mock = MockBase->new({conf_file=>'conf/main.conf'});
	ok scalar(keys %{$mock->config})>0,"Config is not empty when config file provided";
}

sub test_log {
	can_ok $mock, qw(log);
	isa_ok $mock->log, 'Log::Log4perl::Logger' , "Log method returns proper class";
#	is scalar(keys %{$mock->config}),0,"Log is empty when no config file provided";
#	$mock = MockBase->new({conf_file=>'conf/main.conf'});
#	ok scalar(keys %{$mock->config})>0,"Config is not empty when config file provided";
}

sub test_init_logger {
	# Test logging setup routine
	my $logger;
	lives_ok { $logger = $mock->init_logger('./conf/log4perl.conf', 'FooBar')}
	         "Initialize logger from file";
	warn ref $logger;
	isa_ok $logger, 'Log::Log4perl::Logger' , "Logger object is of the proper class";
}

sub test_load_config {
	# Test configuration loading routine
	my %config;
	lives_ok { %config = $mock->load_config('conf/main.conf'); }
	         "Loaded config from file";
	ok %config, "Config hash is defined";
	ok exists($config{dsn}), "'dsn' option exists in config-hash";
}

sub test_open_infile {
	# Test input-file opening routine
	my $infh_reg;
	lives_ok {  $infh_reg = $mock->open_infile("$FindBin::Bin/data/test1.csv") } "Open regular file for reading";
	isa_ok $infh_reg, 'IO::File',"Filehandle is of the proper class";
	can_ok $infh_reg, qw(getline);
	my $infh_gzip;
	lives_ok {  $infh_gzip = $mock->open_infile("$FindBin::Bin/data/test2.csv.gz") } "Open gzipped file for reading";
	isa_ok $infh_gzip, 'IO::File',"Filehandle is of the proper class";
	can_ok $infh_gzip, qw(getline);
}

sub test_open_outfile {
	# Test output-file opening routine
	my $outfh_reg;
	lives_ok {  $outfh_reg = $mock->open_outfile('/tmp/foobarfoo.dat') } "Open regular file for writing";
	isa_ok $outfh_reg, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_reg, qw(getline);
	lives_ok { print $outfh_reg "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
	my $outfh_gzip;
	lives_ok {  $outfh_gzip = $mock->open_outfile('/tmp/foobarfoo.dat.gz') } "Open gzipped file for writing";
	isa_ok $outfh_gzip, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_gzip, qw(getline);
	lives_ok { print $outfh_gzip "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
	my $outfh_zip;
	lives_ok {  $outfh_zip = $mock->open_outfile('/tmp/foobarfoo.dat.zip') } "Open zipped file for writing";
	isa_ok $outfh_zip, 'IO::File',"Filehandle is of the proper class";
	can_ok $outfh_zip, qw(getline);
	lives_ok { print $outfh_zip "foobar\n" } "write something to file";
	unlink '/tmp/foobarfoo.dat';
}

sub test_load_module {
	#test load_module method
	my $modname1 = 'GwasCentral::DataSource::Study';
	my $module;
	lives_ok { $module = $mock->load_module($modname1,[conf_file=>"conf/main.conf"]); } "Load GwasCentral::DataSource::Study module";
	isa_ok $module, 'GwasCentral::DataSource::Study', "Loaded module is of correct class";
	
	my $modname2 = 'GwasCentral::DataSource::Nonexisting';
	dies_ok { $module = $mock->load_module($modname2,[conf_file=>"conf/main.conf"]); } "Dies trying to load non-existing module";
}
