#!perl -w

# $Id: 01_fileparser.t 1274 2009-08-21 15:15:08Z rcf8 $

# Test input file processing subsystem

use strict;
use warnings;
use English qw( -no_match_vars );
use Data::Dumper;
use FindBin;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use Log::Log4perl;
# qw(get_logger);
#use Log::Log4perl::Level;
my $confdir = './conf';
use FindBin;
use lib "$FindBin::Bin/../../lib";

# Get less logging output by altering the root loglevel
use Log::Log4perl;
Log::Log4perl->init("$confdir/log4perl.conf");
Log::Log4perl->appender_thresholds_adjust(+2);

use_ok 'HGVbaseG2P::FileParser';

my $parser = HGVbaseG2P::FileParser->new(log4perl_conf => "$confdir/log4perl.conf");
ok defined($parser), "Parser object is defined";
isa_ok $parser, 'HGVbaseG2P::FileParser', "parser object is of the proper class";
#print Dumper($parser->dump());
foreach my $m(qw( parse_file 
		  parse_xml 
		  parse_delimited )) {
    can_ok $parser, $m;
}

# Make subroutine closure that will process each line of the 
my $record_handler = sub {
    my $l = shift;
    ok @$l == 3, "test CSV file has proper number of fields";
    my ($c1, $c2, $c3) = @$l;
    ok $c1 eq 'foobar1', "value in first field is correct";
};
lives_ok {$parser->parse_file(filename => './data/test1.csv', 
			     filetype => 'delimited', 
			     handler  => $record_handler)}
          "calling parse_file() on regular text CSV-file";

# Try it again on gzipped file, to make sure code handles those files also
lives_ok {$parser->parse_file(filename => './data/test2.csv.gz', 
			     filetype => 'delimited', 
			     handler  => $record_handler)}
          "calling parse_file() on gzipped CSV-file";

# Test an XML-file with tiny little class as the SAX-handler
my $handler = MiniXMLHandler->new();
lives_ok {$parser->parse_file(filename => './data/markers.xml', 
			     filetype => 'xml', 
			     handler  => $handler)}
          "calling parse_file() on XML-file";
	  
exit 0;


# Simple class to test XML-parser
package MiniXMLHandler;
use Object::InsideOut qw( Data::Stag::BaseHandler );
#use base qw(Data::Stag::XMLWriter);
use Digest::MD5 qw( md5_hex );
use Data::Dumper;
use strict;
use Test::More;

sub _init :Init {
    my $self = shift;
    my $args = @_;

    # Inherit from foreign (non-OIO) objects
    my $handler = Data::Stag::BaseHandler->new();
    $handler->trap_h({Allele    =>  sub { $self->handleAllele(@_) },
		  });
    #*Data::Stag::BaseHandler::catch_end = sub {$self->catch_end(@_)};
    $self->inherit($handler);
}

sub Marker {
    my ($self, $handler, $node) = @_;
    isa_ok $node, 'Data::Stag::StagImpl', "tree node ".$node->name." is proper class";
    warn $node->itext;
}

# Create digest from allele-seq before storing
sub handleAllele {
    my ($self, $handler, $node) = @_;
    
    my $aseq_digest = md5_hex($node->get('AlleleSeq'));
    #warn("Creating digest $aseq_digest from allele ",$node->get('AlleleSeq'));
    $node->add('AlleleSeqDigest', $aseq_digest);
    return $node;
}

sub catch_end {
    my ($self, $handler, $event, $node) = @_;
    return if $event eq 'op' || $event eq '@' || !$node->isanode();    
    #warn "in catch_end(), \$event=$event, \@_=", join("|", @_);
    #warn $node->itext;
}
1;
