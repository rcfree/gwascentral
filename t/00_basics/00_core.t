#!perl -w

# $Id: 00_util.t 1274 2009-08-21 15:15:08Z rcf8 $
 
# Test utility module subroutines

use strict;
use warnings;
use English qw( -no_match_vars );
use Data::Dumper;
use FindBin;
use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use FindBin;
use lib "$FindBin::Bin/../../lib";

use_ok 'HGVbaseG2P::Core', qw(init_logger load_config open_infile open_outfile load_module load_configs);
#
## Test logging setup routine
#my $logger;
#lives_ok { $logger = init_logger('./conf/log4perl.conf', 'FooBar')}
#         "Initialize logger from file";
#warn ref $logger;
#isa_ok $logger, 'Log::Log4perl::Logger' , "Logger object is of the proper class";

# Test configuration loading routine
my $config;
#lives_ok { 
	$config = load_configs("$FindBin::Bin/../../conf"); #}
#         "Loaded configs from file";
ok $config, "Config hash is defined";
warn "configs:".Dumper($config);

#
## Test input-file opening routine
#my $infh_reg;
#lives_ok {  $infh_reg = open_infile("$FindBin::Bin/data/test1.csv") } "Open regular file for reading";
#isa_ok $infh_reg, 'IO::File',"Filehandle is of the proper class";
#can_ok $infh_reg, qw(getline);
#my $infh_gzip;
#lives_ok {  $infh_gzip = open_infile("$FindBin::Bin/data/test2.csv.gz") } "Open gzipped file for reading";
#isa_ok $infh_gzip, 'IO::File',"Filehandle is of the proper class";
#can_ok $infh_gzip, qw(getline);
#
## Test output-file opening routine
#my $outfh_reg;
#lives_ok {  $outfh_reg = open_outfile('/tmp/foobarfoo.dat') } "Open regular file for writing";
#isa_ok $outfh_reg, 'IO::File',"Filehandle is of the proper class";
#can_ok $outfh_reg, qw(getline);
#lives_ok { print $outfh_reg "foobar\n" } "write something to file";
#unlink '/tmp/foobarfoo.dat';
#my $outfh_gzip;
#lives_ok {  $outfh_gzip = open_outfile('/tmp/foobarfoo.dat.gz') } "Open gzipped file for writing";
#isa_ok $outfh_gzip, 'IO::File',"Filehandle is of the proper class";
#can_ok $outfh_gzip, qw(getline);
#lives_ok { print $outfh_gzip "foobar\n" } "write something to file";
#unlink '/tmp/foobarfoo.dat';
#my $outfh_zip;
#lives_ok {  $outfh_zip = open_outfile('/tmp/foobarfoo.dat.zip') } "Open zipped file for writing";
#isa_ok $outfh_zip, 'IO::File',"Filehandle is of the proper class";
#can_ok $outfh_zip, qw(getline);
#lives_ok { print $outfh_zip "foobar\n" } "write something to file";
#unlink '/tmp/foobarfoo.dat';
#
##test load_module method
#my $modname1 = 'HGVbaseG2P::Browser::Core';
#my $module;
#lives_ok { $module = load_module($modname1,[conf_file=>"./conf/hgvbase.conf"]); } "Load HGVbaseG2P::Browser::Core module";
#isa_ok $module, 'HGVbaseG2P::Browser::Core', "Loaded module is of correct class";
#
#my $modname2 = 'HGVbaseG2P::Browser::None';
#dies_ok { $module = load_module($modname2,[conf_file=>"./conf/hgvbase.conf"]); } "Dies trying to load non-existing module";
