#!perl -w

# $Id: 01_fileparser.t 1072 2008-11-03 11:32:17Z mummi $

# Test live databases connect ok

use strict;
use warnings;

use FindBin;
use lib ( "$FindBin::Bin/../lib" );
use Data::Dumper qw(Dumper);

use HGVbaseG2P::Database::Study;
use HGVbaseG2P::Database::Browser;
use HGVbaseG2P::Database::Marker;
use HGVbaseG2P::Database::Feature;
use Test::More qw(no_plan);
use Test::Exception;
#plan tests=>'no_plan';

#connection through config file works to each database type
my $db;
lives_ok (sub { $db = HGVbaseG2P::Database::Study->new({
				  conf_file =>
					"conf/hgvbase.conf",
	  })} ,'Study database is ok');

lives_ok (sub { HGVbaseG2P::Database::Marker->new({
				  conf_file =>
					"conf/hgvbase.conf",
	  })} ,'Marker database is ok');

lives_ok (sub { $db = HGVbaseG2P::Database::Browser->new({
				  conf_file =>
					"conf/hgvbase.conf",
	  })} ,'Browser database is ok');
$db->disconnect;

lives_ok (sub { $db = HGVbaseG2P::Database::Feature->new({
				  conf_file =>
					"conf/hgvbase.conf",
	  })} ,'Feature database is ok');
	  