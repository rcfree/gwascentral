#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - export module

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::DataImport::Target::Database;
use GwasCentral::DataImport::DataElement;
use Bio::SeqFeature::Generic;
use GwasCentral::Test::MockDB;
use GwasCentral::DataImport::RetrieveMarker;

my $mockdb = GwasCentral::Test::MockDB->new({conf_file=>"$FindBin::Bin/../conf/test.conf"});
#$mockdb->create_study_db;


#setup test template for multi panel 
my $template = {
	limit=>
	plugins=>
	study_identifier=>'HGVST1',
	experiment_identifier=>'HGVE1',
	resultset_identifier=>'HGVRS1',
	head=>'1',
	accession_db=>'dbSNP',
	comment_prefix=>'#',
	frequencies=>'1',
	associations=>'1',
	allele_order=>'1 2',
	gt_order=>'11 12 22',
	Fields => {
		1=>'accession',
		2=>'chromosome',
		3=>'position',
		4=>'allele1',
		5=>'allele2',
		6=>'maf',
		7=>'genotype11_number:CASE',
		8=>'genotype12_number:CASE',
		9=>'genotype22_number:CASE',
		10=>'genotype11_number:CONTROL',
		11=>'genotype12_number:CONTROL',
		12=>'genotype22_number:CONTROL',
		13=>'oddsratio',
		14=>'lowerci',
		15=>'upperci',
		16=>'pvalue',
	}
};

my $export = GwasCentral::DataImport::Target::Database->new;
$export->use_DS($mockdb);
$export->template($template);
$export->frequencies(0);
$export->associations(1);
#my ($study,$experiment) = $export->get_study_and_experiment($template{study_identifier}, $template{experiment_identifier});
$export->resultset_lookup('identifier');
#my ($resultset) = $export->get_resultset($template{resultset_identifier});

#$export->db->dbh->resultset('Usedmarkerset')->search({'experimentid'=>$experiment->experimentid})->delete;
#$export->db->dbh->resultset('Significance')->search({'resultsetid'=>$resultset->resultsetid})->delete;


my @des = ();
$des[0] = GwasCentral::DataImport::DataElement->new;
$des[0]->line({
          'genotype22' => '(T)',
          'genotype11' => '(C)',
          'position' => '36746853',
          'oddsratio' => '0.94059',
          'ignore' => '404',
          'lowerci' => '0.67759',
          'accession' => 'rs10000068',
          'allele2' => 'T',
          'chromosome' => '4',
          'pvalue' => '0.7142',
          'allele1' => 'C',
          'strand' => '+',
          'genotype12' => '(C)+(T)',
          'upperci' => '1.3057'
        });
my $marker = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '1',
	-source => 'rs100000068',
	-score => '123',
	-display_name => 'HGVM35654',
	-tag          => {chr => '3', alleles=>[qw(A C)], upstream30bp =>'', downstream30bp =>'' }
);


$des[0]->marker($marker);  
$des[1] = GwasCentral::DataImport::DataElement->new;
$des[1]->line({
          'genotype22' => '(T)',
          'genotype11' => '(C)',
          'position' => '36746853',
          'oddsratio' => '0.94059',
          'ignore' => '404',
          'lowerci' => '0.67759',
          'accession' => 'rs100066',
          'allele2' => 'T',
          'chromosome' => '4',
          'pvalue' => '0.7142',
          'allele1' => 'C',
          'strand' => '+',
          'genotype12' => '(C)+(T)',
          'upperci' => '1.3057'
        });
$des[1]->marker(Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '1',
	-source => 'rs100066',
	-score => '123',
	-display_name => 'HGVM3562',
	-tag          => {chr => '3', alleles=>[qw(A C)], upstream30bp =>'', downstream30bp =>'' }
));

$export->output(\@des);

$des[1]->line->{'pvalue'}='0.1345';
$export->output(\@des);
