#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - logic maker module

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::DataImport::Rulebook;

my $rb = GwasCentral::DataImport::Rulebook->new({conf_file=>"$FindBin::Bin/../conf/test.conf"});

#setup test template for multi panel 
my $template = {
	limit=>
	plugins=>
	study_id=>'HGVST1',
	experiment_id=>'HGVE1',
	resultset_id=>'HGVRS1',
	head=>'1',
	accession_db=>'dbSNP',
	comment_prefix=>'#',
	frequencies=>'1',
	associations=>'1',
	allele_order=>'1 2',
	gt_order=>'11 12 22',
	Fields => {
		1=>'accession',
		2=>'chromosome',
		3=>'position',
		4=>'allele1',
		5=>'allele2',
		6=>'maf',
		7=>'genotype11_number:CASE',
		8=>'genotype12_number:CASE',
		9=>'genotype22_number:CASE',
		10=>'genotype11_number:CONTROL',
		11=>'genotype12_number:CONTROL',
		12=>'genotype22_number:CONTROL',
		13=>'oddsratio',
		14=>'lowerci',
		15=>'upperci',
		16=>'pvalue',
	}
};
$rb->template($template);
$rb->prepare_fields;

#check prepare_fields and field_exists works
ok $rb->field_exists('accession'), "Got accession field";
ok $rb->field_exists('pvalue'), "Got pvalue field";
ok $rb->field_exists('genotype11_number:CASE'), "Got genotype11_number:CASE field";
ok !$rb->field_exists('peterotoole'),"Didn't get peterotoole field";

#check panelnames from fields
my $panelnames = $rb->get_preset_apanel_names;
my $lc = List::Compare->new( $panelnames, [qw(CASE CONTROL)] );
ok $lc->get_intersection == 2,"Panels ".join(",",@{$panelnames})." retrieved";




