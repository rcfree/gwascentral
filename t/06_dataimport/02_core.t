#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - logic maker module

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use Log::Log4perl qw(:easy);
use Log::Log4perl::Level;
Log::Log4perl->easy_init($INFO);

use GwasCentral::DataImport::Core;
use GwasCentral::Test::MockDB;
my $mockdb = GwasCentral::Test::MockDB->new({conf_file=>"$FindBin::Bin/../conf/test.conf"});
$mockdb->create_marker_db();
$mockdb->populate_browser_markers();
$mockdb->create_study_db();
$mockdb->populate_study_db();

my $core = GwasCentral::DataImport::Core->new(
{conf_file=>"$FindBin::Bin/../conf/test.conf",
argv      => {},
template_file => "$FindBin::Bin/../templates/HGVST1.tpl",
line_dump=>1
}
);

$core->process_file("$FindBin::Bin/../data/import/HGVST1.txt");

INFO("lines successful:\n".$core->marker_success);

my $marker_stats  = $core->marker_failed;
INFO("lines failed:\n".scalar(@{$marker_stats}));

INFO("stats/errors:\n");
INFO("KEY: n - no marker found, s - strand not available, r - replacement dead, m - merged, d - deleted, f - flipped, a - no-match alleles");
foreach my $failed(@{$marker_stats}) {
	INFO( $failed->line_no.": ".$failed->line->{accession}."\t".join("",map { "$_" } keys %{$failed->status}));
}







