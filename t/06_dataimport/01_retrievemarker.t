#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - retrieve marker module

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use FindBin;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::Test::MockDB;
use GwasCentral::DataImport::RetrieveMarker;

my $mockdb = GwasCentral::Test::MockDB->new({conf_file=>"$FindBin::Bin/../conf/test.conf"});
$mockdb->create_marker_db();
$mockdb->populate_browser_markers();

my $rm = GwasCentral::DataImport::RetrieveMarker->new();
$rm->accession_source('dbSNP');
$rm->use_DS($mockdb);

#check active marker
check_marker("rs501","rs501",0,0,0,0);
#
##check dead marker with no pointer
check_marker("rs5014",undef,0,1,0,0);
#
##check dead marker with pointer
check_marker("rs505","rs504",0,0,1,0);
#
##check dead marker with dead pointer
check_marker("rs505","rs504",0,0,1,0);
#
##check strand flipped, and merged marker
check_marker("rs502","rs504",1,0,1,0);

##check multi-merged marker
check_marker("rs508","rs5010",0,0,1,0);

#check marker with dead replacement
check_marker("rs5012",undef,0,0,1,1);

my ($marker, $status) = $rm->get_validated_marker('rsXYZ');
ok !$marker,"Marker rsXYZ not present";

sub check_marker {
	my($accession, $latest_accession, $strandflip, $dead, $merged, $replacementdead) = @_;
	
	my ($marker, $status) = $rm->get_validated_marker($accession);
	$marker and ok $marker->source_tag eq $latest_accession,"Latest accession expected from $accession is correct (".$marker->source_tag .")";
	SKIP: {
		skip "Strand flip not implemented in code";
		if ($strandflip) {
			ok $status->{f},"Marker replacement strand flip";
		}
		else {
			ok !$status->{f},"Marker replacement NO strand flip";
		}
	}
	
	if ($dead) {
		ok $status->{d},"Marker is dead with no replacement";
	}
	else {
		ok !$status->{d},"Marker is NOT dead with no replacement";
	}
	
	if ($merged) {
		ok $status->{m},"Marker is merged";
	}
	else {
		ok !$status->{m},"Marker is NOT merged";
	}
	
	if ($replacementdead) {
		ok $status->{r},"Marker replacement is dead";
	}
	else {
		ok !$status->{r},"Marker replacement NOT is dead";
	}
}