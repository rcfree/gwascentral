package MockGenomicRegion;
use Moose;
extends qw(GwasCentral::Search::Filter);
with qw(GwasCentral::Search::Filter::Nature::GenomicRegion);
has '+retrieve_from' => ('default' => 'q');
1;