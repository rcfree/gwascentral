package MockStudyDataSource;
use Moose;
use Data::Pageset;
use HGVbaseG2P::Schema::Study::Phenotypemethod;
use HGVbaseG2P::Schema::Study::Study;
use HGVbaseG2P::DataSource::Study::Database;
use HGVbaseG2P::Core qw(load_module);

extends qw(HGVbaseG2P::DataSource::Study);
has 'db'=>('is'=>'rw');

sub BUILD {
	my ($self) = @_;
	my $db = HGVbaseG2P::DataSource::Study::Database->new({conf_file=>$self->config});
	$self->db($db);
}

sub _mock_orm {
	my ($self, $name, $fields) = @_;
	my $module = load_module("HGVbaseG2P::Schema::Study::$name");
	$module->result_source($self->db->dbh->source($name));
	foreach my $field(keys %{$fields}) {
		$module->$field($fields->{$field});
	}
	return $module;
}

augment 'get_paged_phenotypemethods_rs' => sub {
	my ( $self, $args ) = @_;
	my $db = HGVbaseG2P::DataSource::Study::Database->new({conf_file=>$self->config});
	
	my $unique_ids = $args->{phenotype_ids};
	my $order_by   = $args->{sortfield};
	my $page       = $args->{page};
	my $page_size  = $args->{page_size};
	my $threshold  = $args->{threshold};
	
	my $pp = $self->_mock_orm('Phenotypeproperty',{
		name=>'Phenotype property 1'
	});
	my $pa = $self->_mock_orm('Phenotypeannotation',{
		'annotationorigin'=>'mesh',
		'phenotypeidentifier'=>'P00001',
		'exactmatch'=>'yes'
	});
	$pp->phenotypeannotations([$pa]);
	
	
	my $pm = $self->_mock_orm('Phenotypemethod',{
		identifier=>'HGVPM1',
		name => 'Phenotype method 1'
	});
	$pm->phenotypepropertyid($pp);
	
	my $s = $self->_mock_orm('Study', {
		identifier=>'HGVST1',
		name => 'Study 1',
		'isdisplayed'=>'yes'
	});
	$pm->studyid($s);
	
	my @pmethods = ($pm);

	my $pager = Data::Pageset->new(
		{
			'total_entries'    => 1,
			'entries_per_page' => $args->{page_size},
			'current_page'     => 1,
			'pages_per_set'    => 10,
			'mode'             => 'fixed',
		});
		  my $count = 1;
		  return ( [@pmethods], $count, $pager );
	};

	1;
