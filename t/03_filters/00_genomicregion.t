use Test::More;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/lib";
use MockGenomicRegion;
use Data::Dumper qw(Dumper);

check_coords("10",1,135374737);
check_coords("8",1,146274826);
check_coords("X",1,154913754);
check_coords("Y",1,57772954);
#
check_coords("10",345345,16446456);
check_coords("8",346467,1456466);
check_coords("X",24234,1543554);
check_coords("Y",56456,5364654);

my $filter = MockGenomicRegion->new;
$filter->populate({q=>"Diabetes"});
is $filter->is_region,undef,"Keyword not beginning with C is not region";

$filter->populate({q=>"Cancer"});
is $filter->is_region,undef,"Keyword beginning with C is not region";

$filter->populate({q=>"Chronic leukemia"});
is $filter->is_region,undef,"Keyword beginning with Chr is not region";

$filter->populate({q=>"12p13.31"});
is $filter->is_region,undef,"Genomic region feature is not region";

sub check_coords {
	my ($chr_no,$start,$stop) = @_;
	check_segment(($start == 1 ? "chr$chr_no" : "chr$chr_no:$start..$stop"), $chr_no, $start, $stop);
	check_segment(($start == 1 ? "Chr$chr_no" : "Chr$chr_no:$start..$stop"), $chr_no, $start, $stop);
	check_segment(($start == 1 ? "$chr_no" : "$chr_no:$start..$stop"), $chr_no, $start, $stop);
}

sub check_segment {
	my ($query, $chr_no, $start, $stop) = @_;
	my $filter = MockGenomicRegion->new;
	$filter->populate({q=>$query});
	is $filter->chr, "chr$chr_no", "$query recognised (chr$chr_no)";
	is $filter->chr_no, $chr_no, "Chromosome number ($chr_no) returned for $query";
	is $filter->start, $start, "Segment start  ($start) returned";
	is $filter->stop, $stop, "Segment end ($stop) returned";
}
