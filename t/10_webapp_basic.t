#!perl

# $Id: 10_webapp_basic.t 1274 2009-08-21 15:15:08Z rcf8 $

=head1 NAME

03_webapp_basic.t


=head1 SYNOPSIS

    #in shell
    prove 03_webapp_basic.t
    
    #in shell if you want to use a non-local <web-server>
    CATALYST_SERVER=<web-server> prove 03_webapp_basic.t

=head1 DESCRIPTION
Test whether web application is up and can handle various requests for pages
and JSON data sources; and submission of search forms.

See 'Website Test Breakdown' document for automated tests performed.
Manual tests are present in the 'Website Manual Testing' document.

=cut

# NB it's usually good to disable Catalyst debugging to keep output from
# the test-run uncluttered. Try this (in bash shell):
#   export CATALYST_DEBUG=0

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use Test::More 'no_plan';
use Test::WWW::Mechanize::Catalyst 'HGVbaseG2P::Web';
use JSON::XS;
use Data::Dumper qw(Dumper);

my $ua = Test::WWW::Mechanize::Catalyst->new;


# Check site is up
$ua->get_ok('/', "Check root (ie. that indexed page is automatically served");

#test data
my $studyid='HGVST00001';
my $experimentid=3;
my $sigthreshold=0.0001;

#test subroutines to increase code legibility
&home_page;
#&about_page;
#&data_page;
#&study_list_page;
#&marker_page;
#&general_search_page;
#&study_details_tab;

sub home_page {
	&test_common;
	test_page(
		'/index',
		"HGVbaseG2P: a Human Genome Variation and Genotype/Phenotype Database",
		"Home",
		"Home"
	);
	
	&test_all_links;
	&test_full_search;
}

sub about_page {
	&test_common;
	test_page('/info/about',"About","About","About");

	&test_all_links;
}


sub data_page {
	&test_common;
	test_page('/info/data',"HGVbase: Data","Data","Data");

	
	&test_all_links;
	
	test_defined_links({
		Studies=>'/studies',
		Markers=>'/marker',
	});
	
	&test_full_search;
}

sub study_list_page {
	test_page('/study/list',"Studies","Studies","Study list");
	test_common();
	
	
	test_all_links();
	
	test_defined_links({
		Markers=>'/marker',
	});
	#	MartServices=>'/data/biomart_services',
	#	Studies=>'/study/list',
	#	Phenotypes=>'/phenotypeproperty/list',
	
	test_form("searchform","search");
	
	ok(is_json_format('/study/list?format=json'),"JSON format obtained for study list");
}

sub study_details_tab {
	$ua->get_ok('/study/view/HGVST00001#studypanels',"Get study panels tab");
	&test_all_links;	
}

sub marker_page {
	test_page('/marker',"Markers","Marker","Marker page");
	&test_common;
	
	
	&test_all_links;
	
	test_defined_links({
		Markers=>'/marker',
	});
	#	MartServices=>'/data/biomart_services',
	#	Studies=>'/study/list',
	#	Phenotypes=>'/phenotypeproperty/list',
	
	test_form("searchform","search");
}

sub general_search_page {
	$ua->get_ok('/','Check general search root');
	test_form("searchform_header","search",'submit');
	&test_all_links;
}	



#common methods used by page/tab tests

=head1 SUBROUTINES/METHODS 

=cut

=head2 is_json_format

  Usage      : $boolean = is_json_format('/file.json');
  Purpose    : Determines if a URI returns data in JSON format
  Returns    : 1 if JSON or 0 if not
  Arguments  : A URI to test
  Throws     : 
  Status     : 
  Comments   : 

=cut


sub is_json_format {
	my $json_uri=shift;
	my $response = $ua->get($json_uri);
	eval {
		my $perl_scalar = decode_json($response->content);
	};
	return $@ ? 0 : 1;
}

=head2 test_page

  Usage      : test_page($url, $title,$content,$description);
  Purpose    : Tests that a given URL exists and that it matches specific features.
  Returns    : Nothing
  Arguments  : URL to try to get
               Title to compare URL's title with
               Content to compare with URL's content
               Description of the page
  Throws     : 
  Status     : 
  Comments   : Also used to set the current page to the given URL

=cut
	
sub test_page {
	my ($url, $title, $content, $description) = @_;
	$ua->get_ok($url, "Check can get $description page");
	is($ua->ct, "text/html", "Check content type of $description page");
	$ua->title_is($title, "Check title of $description page");
	$ua->content_contains($content, "Check content $description page");
}

=head2 test_common

  Usage      : &test_common;
  Purpose    : Tests common features of each page in the web-site including
               defined links and the search form which are present in the header.
  Returns    : Nothing
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : Uses current page selected through WWW::Mechanize

=cut

sub test_common {
	test_defined_links({
		'Common "Data" Link'=>"/info/data",
		'Common "About" Link'=>"/info/about",
		'Common "Help" Link'=>"/info/help",
		'Common "Contact" Link'=>"/info/contact",
	});
	
	test_form("searchform","search",undef);
}

=head2 test_full_search

  Usage      : test_full_search;
  Purpose    : Tests the searchform search with all different types and results sizes
  Returns    : Nothing
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : Uses current page selected through WWW::Mechanize

=cut
sub test_full_search {
	$ua->form_name("searchform");
	
	my @types=("Studies","Markers","Phenotypes");
	my @resultsizes= qw(5 10 20 50 100);
	
	foreach my $resultsize(@resultsizes) {
		foreach my $type(@types) {
			$ua->get_ok('/','Check general search root');
			my $response = $ua->submit_form(form_name => 'searchform_'.lc($type),			
				fields => {
					itemsperpage => $resultsize,
				}
			);
			ok($response->is_success,"Search form submitted ok (db=$type, size=$resultsize)");
		}
	}
}

=head2 test_form

  Usage      : test_form($formname,$expected_action,[$submit_name]);
  Purpose    : Tests features of a specific form.
  Returns    : Nothing
  Arguments  : Form name attribute in HTML page
               Expected action (without '/')
               Submit button name [optional]
  Throws     : 
  Status     : 
  Comments   : Uses current page selected through WWW::Mechanize

=cut

sub test_form {
	my ($formname,$expected_action,$submit_name) = @_;
	my @action = ();
	my $found = 0;
	$ua->form_name($formname);
	
	#check actions are the same
	@action = split('/',$ua->current_form->action);
	ok($action[$#action] eq $expected_action,"Search form action is correct ($formname)"); 
	
	#check submit is successful
	my $response;
	if ($submit_name) {
		$response = $ua->click_button(name=>$submit_name);
	}
	else {
		$response = $ua->submit;
	}
	ok($response->is_success,"Blank search form submitted ok ($formname)");
}

=head2 test_defined_links

  Usage      : test_defined_links($links);
  Purpose    : Tests that a list of defined links to URLs are present
  Returns    : Nothing
  Arguments  : A hashref containing defined links.
                 The values are descriptive
                 The keys are the URLs to test.
  Throws     : 
  Status     : 
  Comments   : Uses current page selected through WWW::Mechanize

=cut

sub test_defined_links {
	my ($links) = shift;
	
	#loop through each link, look for it in the page and if present check page exists
	foreach my $key(keys %{$links}) {
		my $value = $links->{$key};
		my $link = $ua->find_link(url=> $value);
		ok($link,"Defined link found in page ".$ua->uri . " ($key)");
		SKIP: {
			skip "Defined link not found\n".$ua->content,1 if !$link;
			$ua->links_ok($link->url,"Defined link returns correct status ($key):".$link->url);
		}
	}
}

=head2 test_all_links

  Usage      : test_all_links;
  Purpose    : Tests all links present return a page
  Returns    : Nothing
  Arguments  : None
  Throws     : 
  Status     : 
  Comments   : Uses current page selected through WWW::Mechanize

=cut

sub test_all_links {
	$ua->page_links_ok("Links return correct status on ".$ua->uri);
}
