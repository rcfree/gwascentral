use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Search::Query::Marker;
use HGVbaseG2P::Database;
use Env;
use Data::Dumper qw(Dumper);
$ENV{DBIC_TRACE}=0;
my $pquery = HGVbaseG2P::Search::Query::Marker->new({conf_file=>'conf/hgvbase.conf'});
my $db = HGVbaseG2P::Database->new({conf_file=>'conf/hgvbase.conf'});
$pquery->databases({
	Feature => $db->new_instance('Feature'),
	Marker => $db->new_instance('Marker'),
	Study => $db->new_instance('Study'),
	Ontology => $db->new_instance('Ontology'),
	HPO => $db->new_instance('Hpo'),
	Xapian => $db->new_instance('Xapian'),
	Browser => $db->new_instance('Browser')
});
my $ac = HGVbaseG2P::AccessControl->new({conf_file=>'conf/hgvbase.conf'});

#$pquery->sortfield('identifier');
$pquery->access_control($ac);
my @data = @{ $pquery->results({'q'=>'Cancer of Ovary','t'=>'8', page => 1, 'page_size'=>50}) };
print scalar(@data)." results returned";

print Dumper(\@data);

#query
sub set_pager_given_number_returns_Data_Pageset_object { }
sub set_pager_given_Data_Page_returns_Data_Pageset_object { }
sub get_pager_returns_Data_Pageset_object { }

sub get_page_of_ids_returns_zero_values_for_undef_pager { }
sub get_page_of_ids_returns_correct_values_for_pager { }


#detector

#Detector::GeneRegion
sub null_value_returns_undef { }
sub genomic_region_detected_as_region { }
sub gene_present_in_db_detected_as_feature { }
sub gene_not_present_in_db_returns_undef
sub non_gene_present_in_db_detected_as_feature { }

#Detector::Marker
sub null_value_returns_undef { }
sub marker_accession_detected_as_marker { }
sub marker_identifier_detected_as_marker { }
sub marker_not_present_returns_undef { }

#Detector::Annotation
sub null_value_returns_undef { }
sub mesh_term_detected_as_annotation { }
sub hpo_term_detected_as_annotation { }
sub term_not_in_db_returns_undef { }

#Detector::ConceptTerm
sub null_value_returns_undef { }
sub null_PhenotypeID_returns_undef { }
sub presence_of_PhenotypeID_sets_Concept { }

#Query::QueryString
sub populate_no_q_param_returns_no_value { }
sub populate_q_param_returns_value { }

#Query::Threshold
sub populate_no_t_param_returns_default_value { }
sub populate_t_param_in_list_returns_value { }
sub populate_t_param_not_in_list_throws_exception { }

#Query::PageSize
sub populate_no_page_size_param_returns_default_value { }
sub populate_page_size_param_in_list_returns_value { }
sub populate_page_size_param_not_in_list_throws_exception { }

#Query::SortBy
sub populate_no_page_size_param_returns_default_value { }
sub populate_page_size_param_in_list_returns_value { }
sub populate_page_size_param_not_in_list_throws_exception { }

#Query::RestrictToAnnotation
sub populate_no_rt_param_returns_default_value { }
sub populate_rt_param_equals_true_returns_value { }
sub populate_rt_param_equals_true_sets_detector_and_lock { }
sub populate_rt_param_equals_false_returns_value { }
sub populate_rt_param_equals_false_does_not_set_detector_and_lock { }
