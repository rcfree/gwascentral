use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Search::Query::Genes;
use HGVbaseG2P::Database;
use Env;
use Data::Dumper qw(Dumper);
$ENV{DBIC_TRACE}=0;

my $db = HGVbaseG2P::Database->new({conf_file=>'conf/hgvbase.conf'});
my %db=(
	Feature => $db->new_instance('Feature'),
	Marker => $db->new_instance('Marker'),
	Study => $db->new_instance('Study'),
	Ontology => $db->new_instance('Ontology'),
	HPO => $db->new_instance('Hpo'),
	Xapian => $db->new_instance('Xapian'),
	Browser => $db->new_instance('Browser')
);
my $term = $db{Ontology}->get_mesh_term_from_name('Cancer')->single;
print Dumper($term);