#!perl
use strict;
use warnings;

use FindBin;
use lib ( "$FindBin::Bin/../lib" );

my $path = "$FindBin::Bin/../";
use Data::Dumper qw(Dumper);

use HGVbaseG2P::Browser::Core;
use HGVbaseG2P::Database::Browser;
use Test::More qw(no_plan);
use Array::Compare;
#plan tests=>'no_plan';

#manual connection works
my $browser = HGVbaseG2P::Browser::Core->new({
				  conf_file =>
					"$path/conf/hgvbase.conf",
	  });
my $db = $browser->get_db;
isnt(defined($browser->get_db),"Browser DB not initialised");	

#autoconnection reconnects when disconnected
$browser = HGVbaseG2P::Browser::Core->new({
				  conf_file =>
					"$path/conf/hgvbase.conf",
					autodb=>1
	  });
isa_ok($browser->get_db,"HGVbaseG2P::Database::Browser", "Browser DB object initialised");	
isa_ok($browser->get_db->dbh,"DBI::db","Browser DB DBI handle present");
$browser->get_db->disconnect;
isa_ok($browser->get_db,"HGVbaseG2P::Database::Browser", "Browser DB object still present");	
isa_ok($browser->get_db->dbh,"DBI::db","Browser DB DBI handle reconnected");

#get binned data from Browser DB using utility methods

my $data;

$browser->resultsetid(['1']);

my $bin_size = 0;
test_1mb_bins();

	$browser->threshold(undef);
	$browser->round_start(undef);
	$browser->round_stop(undef);
	$browser->chr(undef);
	
test_3mb_bins();


sub test_1mb_bins {
	$bin_size = 1;
	$browser->threshold(undef);
	$browser->round_start(undef);
	$browser->round_stop(undef);
	$browser->chr(undef);
	
#return data even if no threshold (default to ZERO)
test_get_binned_data({
	expected_rows => 24,
	test_bin => 0,
	test_bin_start => 1,
	test_bin_counts => 3,
	test_chr => 1
});

#threshold 2

$browser->threshold(2);
test_get_binned_data({
	expected_rows => 24,
	test_bin => 7,
	test_bin_start => 7000001,
	test_bin_counts => 2,
	test_chr => 1
});

#threshold 3
$browser->threshold(3);
test_get_binned_data({
	expected_rows => 24,
	test_bin => 21,
	test_bin_start => 21000001,
	test_bin_counts => 2,
	test_chr => 1
});

#no threshold and chr 2 only
$browser->threshold(undef);
$browser->chr('2');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 7,
	test_bin_start => 7000001,
	test_bin_counts => 203,
	test_chr => 2
});


#threshold 3 and chr 1 only
$browser->threshold(3);
$browser->chr('1');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 21,
	test_bin_start => 21000001,
	test_bin_counts => 2,
	test_chr => 1
});


#test chr region retrieval
$browser->threshold(2);
$browser->round_start('2000000');
$browser->round_stop('100000000');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 3,
	test_bin_start => 5000001,
	test_bin_counts => 2,
	test_chr => 1,
	no_bins => 98
});

#test small chr region retrieval
$browser->threshold(2);
$browser->round_start('20000');
$browser->round_stop('100000');
test_get_binned_data({
	expected_rows => 0,
	test_bin => 0,
	test_bin_start => undef,
	test_bin_counts => undef,
	test_chr => 1,
	no_bins => 0
});
}

sub test_3mb_bins {

$bin_size = 3;

#return data even if no threshold (default to ZERO)
test_get_binned_data({
	expected_rows => 24,
	test_bin => 0,
	test_bin_start => 1,
	test_bin_counts => 96,
	test_chr => 1
});

#threshold 2

$browser->threshold(2);
test_get_binned_data({
	expected_rows => 24,
	test_bin => 7,
	test_bin_start => 21000001,
	test_bin_counts => 6,
	test_chr => 1
});

#threshold 3
$browser->threshold(3);
test_get_binned_data({
	expected_rows => 24,
	test_bin => 7,
	test_bin_start => 21000001,
	test_bin_counts => 2,
	test_chr => 1
});

#no threshold and chr 2 only
$browser->threshold(undef);
$browser->chr('2');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 7,
	test_bin_start => 21000001,
	test_bin_counts => 403,
	test_chr => 2
});


#threshold 3 and chr 1 only
$browser->threshold(3);
$browser->chr('1');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 7,
	test_bin_start => 21000001,
	test_bin_counts => 2,
	test_chr => 1
});


#test chr region retrieval
$browser->threshold(2);
$browser->round_start('2000000');
$browser->round_stop('100000000');
test_get_binned_data({
	expected_rows => 1,
	test_bin => 0,
	test_bin_start => 3000001,
	test_bin_counts => 2,
	test_chr => 1,
	no_bins => 32
});

#test small chr region retrieval
$browser->threshold(2);
$browser->round_start('20000');
$browser->round_stop('100000');
test_get_binned_data({
	expected_rows => 0,
	test_bin => 0,
	test_bin_start => undef,
	test_bin_counts => undef,
	test_chr => 1,
	no_bins => 0
});
}



#$browser->major_bin('3000000');
#$browser->seg_start('55000000');
#$browser->seg_stop('58000000');
#$browser->round_positions();
#$browser->threshold('2');
#$browser->chr('1');

	
sub test_get_binned_data {
	my $attrs=shift;
	my $data = $browser->get_marker_binned_data( { bin_size => $bin_size });
	is scalar(keys %{$data}), $attrs->{expected_rows},"Returned expected rows";
	my $test_item = $data->{$attrs->{test_chr}}->[$attrs->{test_bin}]; 
	is $test_item->{bin_start}, $attrs->{test_bin_start},"Test item bin_start is correct";
	is $test_item->{bin_counts},$attrs->{test_bin_counts},"Test item bin_count is correct";
	$attrs->{no_bins} and is scalar(@{$data->{$attrs->{test_chr}}}), $attrs->{no_bins}, "Expected number of bins returned";
}

