#!perl -w

# $Id: 03_markerimport.t 592 2008-05-06 11:52:43Z rcf8 $

# Test marker import pipeline

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use HGVbaseG2P::Test::DataImport::Marker;
HGVbaseG2P::Test::DataImport::Marker->runtests();
