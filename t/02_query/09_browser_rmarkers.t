#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=3;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";

my $query = new_Query( 'Browser::RegionMarkers', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'q' => 'chr10:1000000..5000000', 
	'r[]'=>'HGVRS853,HGVRS854,HGVRS855,HGVRS856,HGVRS857,HGVRS858,HGVRS1470,HGVRS1473,HGVRS1480,HGVRS1498', 
	't'=>'ZERO', 
	'sc' => 'log10', 
	'format'=>'bed', 'stacked' => 1, line_trace=>1, 'HapMapLDPlot'=>1, 'CEU'=>1 });
$query->prepare_Filters;
#print Dumper($query->f('AddedResultsets')->resultsets);
print $query->output;
