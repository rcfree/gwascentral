#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=3;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";

my $query = new_Query( 'Genes', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'q'=>'cancer', 'qfilter'=>'Gene'});
$query->prepare_Filters;
print Dumper($query->results);
my $gmarkers = $query->Queries->{Genes_Markers};
print Dumper($gmarkers->Filters);
print "no_results:".$query->no_results;
print $gmarkers->text;