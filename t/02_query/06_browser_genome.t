#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=3;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";

my $query = new_Query( 'Browser::Genome', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'r[]'=>'HGVRS137,HGVRS618,HGVRS176,HGVRS1118', 't'=>'ZERO', 'sc' => 'log10'});
$query->prepare_Filters;

print Dumper($query->as_features);
#$query->f('AddedResultsets')->upload_data({
#	'U13' => {
#      'resultset_identifier' => 'U13',
#      'changed_ids' => [],
#      'resultset_name' => 'Uploaded data set 1',
#      'study_name' => 'Uploaded Data',
#      'no_coords_ids' => [
#        'rs883635'
#      ],
#      'dead_ids' => [
#        'rs11155818'
#      ],
#      'failed_pvalues' => [],
#      'success_count' => '2998',
#      'study_ident' => 'UPLOAD',
#      'failed_ids' => []
#    }
#});

#print $query->output;
