#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=2;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');

#known study
check_phenotype("HGVPM983","GWAS of type II diabetes mellitus");

#unknown study
#throws_ok { check_study('HGVSTXYZ',undef); } qr/not found/, "Study not found throws error";
#throws_ok { check_study(undef,undef); } qr/not supplied/, "Study not supplied throws error";
#throws_ok { check_study("",undef); } qr/not supplied/, "Study not supplied throws error";
#throws_ok { check_study(" ",undef); } qr/not found/, "Study not supplied throws error";

sub check_phenotype {
	my ($exp_ident, $exp_name) = @_;
	my $query = new_Query( 'Phenotype', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );
	$query->params({'id'=>$exp_ident});
	$query->prepare_Filters;
	my $results = $query->results;
	my $study = $query->results->{data};
	isa_ok $study, "GwasCentral::Schema::Study::Study", "Result item";
	is $results->{identifier}, $exp_ident, "Study identifier is correct";
	is $study->identifier, $exp_ident, "Study identifier is correct";
	is $study->name, $exp_name, "Study name is correct";
}

