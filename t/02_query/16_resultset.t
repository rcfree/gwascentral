#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=3;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";

my $query = new_Query( 'ResultsetMarkers', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'rid' => 'HGVRS1507', 'format' => 'rss'});
$query->prepare_Filters;
#$query->results;
print $query->output;