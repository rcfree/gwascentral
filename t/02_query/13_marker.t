#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=4;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');

#known markers
check_marker('dbSNP:rs61743044',"rs61743044", "HGVM12182750");
check_marker('rs617430',"rs617430","HGVM176514");
check_marker('HGVM6556',"rs6174","HGVM6556");

#unknown markers and missing coords
throws_ok { check_marker('dbSNP:rs6174302',undef,undef); } qr/not found/, "Marker not found throws error";
check_marker('HGVM12',"rs1046911","HGVM12");

sub check_marker {
	my ($id, $exp_accession, $exp_ident) = @_;
	my $query = new_Query( 'Marker', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );
	
	$query->params({'id'=>$id});
	$query->prepare_Filters;
	my $results = $query->results;
	my $marker = $query->results->{item};
	print Dumper($marker->markerrevision_markerids);
	isa_ok $marker, "GwasCentral::Schema::Marker::Marker", "Result item";
	is $results->{identifier}, $exp_ident, "Marker identifier is correct";
	is $marker->identifier, $exp_ident, "Marker identifier is correct";
	is $marker->accession, $exp_accession, "Marker accession is correct";
}