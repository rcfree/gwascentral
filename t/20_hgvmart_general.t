#!perl

# $Id: 20_hgvmart_general.t 1274 2009-08-21 15:15:08Z rcf8 $

=head1 NAME

00_g2pmart.t - Test BioMart functionality. 


=head1 SYNOPSIS

    prove 00_g2pmart.t

=head1 DESCRIPTION

This script acts as a test harness for Test::Class based HGVbaseG2Pmart::Test
modules. It attempts to run all Test::Class modules (*.pm) in lib/HGVbaseG2Pmart/Test

=head1 SUBROUTINES/METHODS 

=cut

use FindBin;
use lib "$FindBin::Bin/../lib";

use HGVbaseG2Pmart::DbCreator;
use HGVbaseG2Pmart::MartCreator;

use HGVbaseG2Pmart::Test::Study;
use HGVbaseG2Pmart::Test::Study::AlleleFrequency;
use HGVbaseG2Pmart::Test::Study::GenotypeFrequency;
use HGVbaseG2Pmart::Test::Study::AlleleAssociation;
use HGVbaseG2Pmart::Test::Study::GenotypeAssociation;
use HGVbaseG2Pmart::Test::Study::AlleleAssociationComp;
use HGVbaseG2Pmart::Test::Study::GenotypeAssociationComp;

use HGVbaseG2Pmart::Test::Marker;
use HGVbaseG2Pmart::Test::Marker::AlleleFrequency;
use HGVbaseG2Pmart::Test::Marker::GenotypeFrequency;
use HGVbaseG2Pmart::Test::Marker::AlleleAssociation;
use HGVbaseG2Pmart::Test::Marker::GenotypeAssociation;
use HGVbaseG2Pmart::Test::Marker::AlleleAssociationComp;
use HGVbaseG2Pmart::Test::Marker::GenotypeAssociationComp;

use Test::Exception;
use Test::More 'no_plan';

&generate_db;
&create_mart;
#
#HGVbaseG2Pmart::Test::Study->runtests;
#HGVbaseG2Pmart::Test::Marker->runtests;
1;

sub generate_db {
	my $db = HGVbaseG2Pmart::DbCreator->new({conf_file=>'./conf/test/hgvbase.conf'});

	lives_ok { $db->create_db; } "Test HGVbaseG2P database created";
	
	lives_ok { 
		$db->store_xml('./t/data/mart/markers.xml');
		$db->store_xml('./t/data/mart/study_broad.xml');
		$db->store_xml('./t/data/mart/study_CGEMS.xml'); 
	} "Pre-generated XML stored ok";
	
	lives_ok { 
		$db->generate_xml('Frequency', 'Broad', 'HGVST1', './t/data/mart/frequency_broad.txt'); 
		$db->store_xml('freqs_Broad_frequency_broad.xml');
		
		$db->generate_xml('Frequency', 'CGEMS', 'HGVST2', './t/data/mart/frequency_cgems.txt', 'CASE'); 
		$db->store_xml('freqs_CGEMS_frequency_cgems.xml');
	} "Frequency XML generated and stored ok";
	
	lives_ok { 
		$db->generate_xml('Association', 'Broad', 'HGVST1', './t/data/mart/frequency_broad.txt'); 
		$db->store_xml('assocs_Broad_frequency_broad.xml');
		
		$db->generate_xml('Association', 'CGEMS', 'HGVST2', './t/data/mart/assoc_cgems.txt','CASE'); 
		$db->store_xml('assocs_CGEMS_assoc_cgems.xml');
	} "Association XML generated and stored ok";
	
	$db->db_cleanup;

}

sub create_mart {
	my $creator = new HGVbaseG2Pmart::MartCreator({conf_file=>"./conf/test/hgvmart.conf"});
	isa_ok($creator, "HGVbaseG2Pmart::MartCreator","Creator is the correct class");
	lives_ok {
		$creator->create_mart;
	} "Mart created successfully";
}
=head1 SEE ALSO

L<HGVbaseG2Pmart::Test>, L<HGVbaseG2Pmart::Test::Study>, L<HGVbaseG2Pmart::Test::Database> and related classes

=head1 BUGS AND LIMITATIONS

There are no known bugs in this module.
Please report problems to Rob <rcfree@gmail.com>

=head1 CONTACT

This module is part of the HGVbase-G2P project

=head1 AUTHOR

Rob <rcfree@gmail.com>

=head1 LICENCE AND COPYRIGHT

Copyright (c) <2007> University of Leicester, Leicester, UK. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

=head1 CVSINFO

$Id: 20_hgvmart_general.t 1274 2009-08-21 15:15:08Z rcf8 $

=cut

