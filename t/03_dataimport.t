#!perl -w

# $Id: 03_dataimport.t 1072 2008-11-03 11:32:17Z mummi $

# Test marker import pipeline

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use HGVbaseG2P::Test::DataImport::Common;
HGVbaseG2P::Test::DataImport::Common->runtests();
