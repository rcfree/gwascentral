#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);

use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);

my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";
my $query = new_Query( 'Phenotypes', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );
use Env;
$ENV{DBIC_TRACE}=1;
$query->params({'q'=>'Cancer', 'qfilter' => 'Annotation', 't'=>'3', 'treetype' => 'mesh', 'toggle_row' => 'meshtree'});
$query->prepare_Filters;
print Dumper($query->results);
print Dumper($query->pager);