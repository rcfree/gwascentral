#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;
use GwasCentral::Test::Search qw(check_results get_query);

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=2;
my $sm = get_query('StudyMarkers',{'page_size'=>50, 'id'=>'HGVST310'});
lives_ok { $sm->results } "StudyMarkers results lives";

check_results($sm->Queries->{'HGVRS575'},undef,{ total => 419368, no_results => undef, no_display => undef } );

my $q1 = $sm->Queries->{'HGVRS577'};
$q1->params->{page}=2;
$q1->prepare_Filters;
throws_ok { $q1->results } qr/top markers can be accessed/, "Prevents access to other pages of markers";

check_results($sm->Queries->{'HGVRS577'},undef,{ total => 11210, no_results => undef, no_display => undef } );
$sm = get_query('StudyMarkers',{'page_size'=>50, 'id'=>'HGVST310', 'page'=>2});
throws_ok { $sm->results; } qr/top markers can be accessed/, "Prevents access to other pages of markers";
