#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use Test::More;
use Test::Exception;
use Test::Base;
use Data::Dumper qw(Dumper);

filters { 
	input => [qw(lines chomp hash)]
};

use Test::Base;
use HGVbaseG2P::Test::Search;

test_search_count 'input' => 'expected','studies';

__END__

=== Test 1 (Query string 'cancer' and threshold 'ZERO')

--- input
q
cancer
t
ZERO

--- expected chomp
47

=== Test 2 (Query string 'cancer' and threshold '3')

--- input
q
cancer
t
3

--- expected chomp
46

=== Test 2 (Query string 'cancer' and threshold '6')
--- ONLY
--- input
q
cancer
t
6

--- expected chomp
45

=== Test 3 (Empty string recognised as 'None')

--- input
q

t
3

--- expected chomp
None

=== Test 4 (Undef string recognised as 'None')

--- input lines chomp hash
q
undef
t
3

--- expected chomp
None

