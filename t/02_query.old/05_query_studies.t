#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;
use GwasCentral::Test::Search qw(test_query_results test_query_content test_page_sizes);

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=2;
my %formats = (
	'excel' => 'application/vnd.ms-excel',
	'rss' => 'application/rss',
	'atom' => 'application/atom',
	'json' => 'application/json',
	'csv' => 'text/csv',
	'tsv' => 'text/tsv',
	'ssv' => 'text/ssv',
);
my @page_sizes = qw(20 50 100 all);

#test_query_results('Studies',{},{ total => 745, no_results => undef, no_display => undef, recognised_as => 'None' } );
#test_query_results('Studies',{'q'=>'cancer'},{ total => 86, no_results => undef, no_display => undef, recognised_as => 'Keyword' } );
#test_query_results('Studies',{'q'=>'cancer', 't'=>5},{ total => 85, no_results => undef, no_display => undef, recognised_as => 'Keyword' } );
#test_query_results('Studies',{'q'=>'BRCA1', 't'=>3},{ total => 1, no_results => undef, no_display => undef, recognised_as => 'GeneRegion' } );
#test_query_results('Studies',{'q'=>'BRCA1', 'qfilter'=>'GeneRegion', 't'=>3},{ total => 1, no_results => undef, no_display => undef, recognised_as => 'GeneRegion' } );
#my $q = test_query_results('Studies',{'q'=>'BRCA1', 't'=>3, 'qfilter' => 'Keyword'},{ total => 3, no_results => undef, no_display => undef, recognised_as => 'Keyword' } );
#my $qf = $q->f('QueryFilter');
#is $qf->pagers->{Keyword}->total_entries,3,"Keyword query filter pager is correct";
#is $qf->pagers->{GeneRegion}->total_entries,1,"GeneRegion query filter pager is correct";
#test_query_results('Studies',{'q'=>'flibble', 'qfilter'=>'GeneRegion', 't'=>3},{ total => 0, no_results => 1, no_display => undef, recognised_as => 'GeneRegion' } );
#test_query_results('Studies',{'q'=>'flibble', 't'=>3},{ total => 0, no_results => 1, no_display => undef, recognised_as => 'Keyword' } );
#test_query_results('Studies',{'q'=>'', 't'=>5},{ total => 737, no_results => undef, no_display => undef, recognised_as => 'None' } );

throws_ok { 
	test_query_results('Studies',{'page_size'=>250, 'q'=>'cancer', 't'=>'6'},{ total => 5, no_results => 0, no_display => 0 } );
} qr/not in the available list/, "Page size is not allowed";

test_query_content('Studies',{'q'=>'cancer', 't' => 5},\%formats);
test_page_sizes('Studies',{'q'=>'cancer', 't' => 5},\@page_sizes);

#test_query_results('Studies',{'page_size'=>50, 'q'=>'BRCA2', 't'=>'4', 'o'=>'Identifier (ascending)'},{ total => 5, no_results => 0, no_display => 0 } );
#test_filter_validation('Studies','
#throws_ok { 
#	check_results('Studies',{'page_size'=>200, 'q'=>'BRCA2', 't'=>'6', 'o'=>'Identifier (ascending)'},{ total => 5, no_results => 0, no_display => 0 } );
#} qr/not in the available list/, "Page size is not allowed";

#check_formats($query,[]);
#check_content_types($query,[]);

