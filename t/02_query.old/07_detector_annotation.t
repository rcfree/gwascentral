use FindBin;
use lib "$FindBin::Bin/lib";
use SearchObjectMaker;

use Test::More;
use Test::Exception;
use Test::Deep;
my $som = SearchObjectMaker->new({conf_file=>'conf/hgvbase.conf'});
my $detector = $som->make_Detector({
	name => 'Annotation',
	filter_list => ['PhenotypeID']
});
my $query = $detector->query;
is $detector->detect(),undef,"No specified term returns undef";
is $detector->detect('BRCA1'),undef,"Gene term returns undef"; 
is $detector->detect('rs699'),undef,"Marker term returns undef"; 
is $detector->detect('cincer'),undef,"Incorrectly spelt ontology term returns undef"; 
is $detector->detect('cancer'),"Annotation","Phenotype ontology term returns 'Annotation'";
my $cancer_pids = $query->fval('PhenotypeID');

$detector = $som->make_Detector({
	name => 'Annotation',
	filter_list => ['PhenotypeID']
});
$query = $detector->query;
is $detector->detect('neoplasm'),"Annotation","Phenotype ontology term synonym returns 'Annotation'"; 
my $neoplasm_pids = $query->fval('PhenotypeID');

$detector = $som->make_Detector({
	name => 'Annotation',
	filter_list => ['PhenotypeID']
});
$query = $detector->query;
is $detector->detect('neoplasms'),"Annotation","Phenotype ontology term synonym returns 'Annotation'"; 
my $neoplasms_pids = $query->fval('PhenotypeID');

cmp_deeply($cancer_pids, bag(@{$neoplasm_pids}), "Phenotype IDs returned by synonyms are identical");
cmp_deeply($neoplasms_pids, bag(@{$neoplasm_pids}), "Phenotype IDs returned by synonyms are identical");

#test actual PIDs = correct PIDs

#test other values

#test mesh vs hpo
