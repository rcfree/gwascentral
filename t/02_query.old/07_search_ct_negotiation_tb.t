#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z rcf8 $
use strict;
use warnings;
# Test search module
use FindBin;
use lib "$FindBin::Bin/";
use HGVbaseG2P::Test;
use Test::Exception;

run_is 'input' => 'expected';

__END__
    
=== Test 1 (RSS content type negotiation)
--- input is_expected_content
application/rss+xml
/studies
rss
--- expected
1

=== Test 2 (Atom content type negotiation)
--- input is_expected_content
application/atom+xml
/studies
atom
--- expected
1
===







test_html_content_type_works();
test_atom_content_type_works();
test_json_content_type_works();
test_rss_content_type_works();
test_html_format_works();
test_atom_format_works();
test_json_format_works();
test_rss_format_works();

sub test_atom_content_type_works {
	$lwp->default_header('Content-type' => 'application/atom+xml');
	my $res = $lwp->get($url."/studies");
	my $content = $res->content;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "Atom feed returned by content-type 'application/atom+xml'"
}

sub test_atom_format_works {
	$lwp->default_headers( HTTP::Headers->new);
	$lwp->default_header('Content-type' => 'application/atom+xml');
	my $res = $lwp->post($url."/studies",{format=>'atom'});
	my $content = $res->content;
	lives_ok { 
		XML::Atom::Feed->new(\$content);
	} "Atom feed returned by format 'atom'"
}

sub test_rss_format_works {
	$lwp->default_headers( HTTP::Headers->new);
	$lwp->default_header('Content-type' => 'application/atom+xml');
	my $res = $lwp->post($url."/studies",{format=>'rss'});
	my $content = $res->content;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "Atom feed returned by format 'rss'"
}

sub test_rss_content_type_works {
	$lwp->default_header('Content-type' => 'application/rss+xml');
	my $res = $lwp->get($url."/phenotypes?q=cancer");
	my $content = $res->content;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "RSS feed returned by content-type 'application/rss+xml'"
}


sub test_json_content_type_works {
	$lwp->default_header('Content-type' => 'text/json');
	my $res = $lwp->get($url."/markers?q=BRCA1");
	my $content = $res->content;
	lives_ok { 
		decode_json($content);
	} "JSON returned by content-type 'text/json'"
}

sub test_json_format_works {
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url."/markers?q=BRCA1",{format=>'json'});
	my $content = $res->content;
	lives_ok { 
		decode_json($content);
	} "JSON returned by format 'json'"
}

sub test_html_content_type_works {
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->get($url."/studies?q=BRCA1");
	my $content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by no content-type";
	
	$lwp->default_header( 'Content-type' => 'text/html' );
	$res = $lwp->get($url."/studies?q=BRCA1");
	$content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by content-type 'text/html'"
}

sub test_html_format_works {
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url."/studies?q=BRCA1",{});
	my $content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by no format";
	
	$res = $lwp->get($url."/studies?q=BRCA1",{format=>'html'});
	$content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by format 'html'"
}