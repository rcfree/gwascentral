#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z rcf8 $
use strict;
use warnings;
# Test search module
use FindBin;
use lib "$FindBin::Bin/../lib";
use LWP::UserAgent;
use Data::Dumper qw(Dumper);
#use HGVbaseG2P::Test;
use Test::More qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use JSON qw(decode_json);
use XML::Simple qw(XMLin);
use XML::Atom::Feed;
use XML::RSS;
use YAML;
use HTML::TreeBuilder;
use HTTP::Headers;
use HGVbaseG2P::Util qw(load_config);
#use Test::Template;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($DEBUG);
my %config = load_config('conf/hgvbase.conf');
my $lwp = LWP::UserAgent->new;
my $url = $config{baseurl};
my $host = $url;
$host =~ s/http:\/\///;
$config{realm} and $lwp->credentials("$host:80",$config{realm},$config{user},$config{pass});

my @urls = (
"/study/HGVST709", 
#"/study/HGVST315", 
#"/marker/dbSNP:rs699", 
#"/marker/dbSNP:rs16940",
#"/marker/HGVM512"
);
my %template;
run_test_template('t/template/content-types.conf');
#test_content_types('Paths');

sub run_test_template {
	my ($template_file) = @_;
	%template = load_config($template_file);
	my $method = $template{test_method};
	__PACKAGE__->$method;
}

sub test_content_types {
	my @paths = keys %{$template{Path}};
	
	foreach my $path(@paths) {
		test_unrecognised_content_type_fails($path);
		test_unrecognised_format_fails($path);
		test_html_content_type_works($path);
		test_xml_content_type_works($path);
		test_json_content_type_works($path);
		test_yaml_content_type_works($path);
		test_html_format_works($path);
		test_yaml_format_works($path);
		test_json_format_works($path);
		test_xml_format_works($path);
	}
}

sub test_unrecognised_content_type_fails {
	my ($path) = @_;
	$lwp->default_header('Content-type' => 'text/nothing');
	my $res = $lwp->get($url.$path);
	ok !$res->is_success, "Response status not ok using unrecognised content_type for $path - ".$res->status_line;
}

sub test_unrecognised_format_fails {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url.$path,{format=>'nothing'});
	ok !$res->is_success, "Response status 'post' not ok using unrecognised format for $path - ".$res->status_line;
	$res = $lwp->get($url.$path."?format=nothing");
	ok !$res->is_success, "Response status 'get' not ok using unrecognised format for $path - ".$res->status_line;
}


sub test_xml_content_type_works {
	my ($path) = @_;
	$lwp->default_header('Content-type' => 'text/xml');
	my $res = $lwp->get($url.$path);
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "XML returned by content-type 'text/xml' for $path"
}

sub test_yaml_format_works {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url.$path,{format=>'yaml'});
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;
	lives_ok { 
		YAML::Load($content);
	} "YAML returned by format 'yaml' for $path";
}

sub test_xml_format_works {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url.$path,{format=>'xml'});
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;
	lives_ok { 
		my $rss = XML::RSS->new();
		$rss->parse($content);
	} "XML returned by format 'xml' for $path";
}

sub test_yaml_content_type_works {
	my ($path) = @_;
	$lwp->default_header('Content-type' => 'text/yaml');
	my $res = $lwp->get($url.$path);
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;
	lives_ok { 
		Load($content);
	} "YAML returned by content-type 'text/yaml' for $path"
}

sub test_json_content_type_works {
	my ($path) = @_;
	$lwp->default_header('Content-type' => 'text/json');
	
	my $res = $lwp->get($url.$path);
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;

	lives_ok { 
		decode_json($content);
	} "JSON returned by content-type 'text/json' for $path"
}

sub test_json_format_works {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url.$path,{format=>'json'});
	ok $res->is_success, "Response status ok for $path";
	my $content = $res->content;
	lives_ok { 
		decode_json($content);
	} "JSON returned by format 'json' for $path";
}

sub test_html_content_type_works {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->get($url.$path);
	ok $res->is_success, "Response status ok for $path - ".$res->status_line;
	my $content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by no content-type for $path";
	
	$lwp->default_header( 'Content-type' => 'text/html' );
	$res = $lwp->get($url.$path);
	ok $res->is_success, "Response status ok for $path - ".$res->status_line;
	$content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by content-type 'text/html' for $path"
}

sub test_html_format_works {
	my ($path) = @_;
	$lwp->default_headers( HTTP::Headers->new);
	my $res = $lwp->post($url.$path,{});
	ok $res->is_success, "Response status ok for $path - ".$res->status_line;
	my $content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by no format for $path";
	
	$res = $lwp->get($url.$path,{format=>'html'});
	ok $res->is_success, "Response status ok for $path - ".$res->status_line;
	$content = $res->content;
	lives_ok { 
		HTML::TreeBuilder->new_from_content($content);
	} "HTML returned by format 'html' for $path"
}
