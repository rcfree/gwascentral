use FindBin;
use lib "$FindBin::Bin/lib";
use SearchObjectMaker;
use Data::Dumper qw(Dumper);
use Test::More;
use Test::Exception;
use Test::Deep;
my $som = SearchObjectMaker->new({conf_file=>'conf/hgvbase.conf'});
my $detector = $som->make_Detector({
	name => 'GeneRegion',
	filter_list=>['QueryString']
});
my $query = $detector->query;
test_terms_that_are_not_features_are_undetected();
test_gene_is_detected();
test_capitalised_chr_region_is_detected();
test_lower_case_chr_region_is_detected();
test_no_prefix_chr_region_is_detected();
test_capitalised_chr_is_detected();
test_lower_case_chr_is_detected();
test_no_prefix_chr_char_is_detected();
test_no_prefix_chr_number_is_detected();

sub test_terms_that_are_not_features_are_undetected {
	is $detector->detect(),undef,"No specified term returns undef";
	is $detector->detect(""),undef,"Empty term returns undef";
	is $detector->detect('rs699'),undef,"Marker term returns undef"; 
	is $detector->detect('cancer'),undef,"Keyword term returns undef";
}

sub test_gene_is_detected {
	is $detector->detect('BRCA1'),"Feature","Gene term returns Feature";
	my $sorted_features = $detector->sorted_features;
	is scalar(@{$sorted_features}),38,"Correct number of sorted BRCA1 features returned";
	my $big_feat = $sorted_features->[0];
	my $sml_feat = $sorted_features->[scalar(@{$sorted_features})-1];
	is $big_feat->{size},81154,"Correct size of biggest BRCA1 feature";
	is $sml_feat->{size},13521,"Correct size of smallest BRCA1 feature";
	is $sml_feat->{feat}->name,"BRCA1","Correct name of smallest BRCA1 feature";
}

sub test_capitalised_chr_region_is_detected {
	my $qs = $detector->query->f('QueryString');
	is $detector->detect('Chr1:11..4234234'),"Region","Genomic region term returns Region";
	$sorted_features = $detector->sorted_features;
	is scalar(@{$sorted_features}),0,"No sorted features for Region";
	
	is $qs->chr,"Chr1","Correct chromosome returned for Chr1:11..4234234";
	is $qs->chr_no,"1","Correct chromosome no returned for Chr1:11..4234234";
	is $qs->start,11,"Correct start returned for Chr1:11..4234234";
	is $qs->stop,4234234,"Correct stop returned for Chr1:11..4234234";
}

sub test_lower_case_chr_region_is_detected {	
	is $detector->detect('chrX:4535..1325356456'),"Region","Lower case genomic region term returns Region";
	$sorted_features = $detector->sorted_features;
	is scalar(@{$sorted_features}),0,"No sorted features for Region";
	$qs = $detector->query->f('QueryString');
	is $qs->chr,"chrX","Correct chromosome returned for chrX:11..4234234";
	is $qs->chr_no,"X","Correct chromosome no returned for chrX:11..4234234";
	is $qs->start,4535,"Correct start returned for chrX:11..4234234";
	is $qs->stop,1325356456,"Correct stop returned for chrX:11..4234234";
}

sub test_no_prefix_chr_region_is_detected {		
	is $detector->detect('22:4566..46362222'),"Region","Chr number genomic region term returns Region";
	$sorted_features = $detector->sorted_features;
	is scalar(@{$sorted_features}),0,"No sorted features for Region";
	$qs = $detector->query->f('QueryString');
	is $qs->chr,"22","Correct chromosome returned for 22:4566..46362222";
	is $qs->chr_no,"22","Correct chromosome no returned for 22:4566..46362222";
	is $qs->start,4566,"Correct start returned for 22:4566..46362222";
	is $qs->stop,46362222,"Correct stop returned for 22:4566..46362222";
}

sub test_capitalised_chr_is_detected {
	is $detector->detect('Chr4'),"Feature","Chromosome term returns Feature";
	$sorted_features = $detector->sorted_features;
	SKIP: {
		skip "More than 1 feature in my GFF database - this should be fixed",1 ;
		is scalar(@{$sorted_features}),1,"One sorted Feature for chromosome";
	};
	my $feat = $sorted_features->[0];
	is $feat->{size},191273062,"Chromosome feature is correct size";
}

sub test_lower_case_chr_is_detected {
	is $detector->detect('chr6'),"Feature","Chromosome term returns Feature";
	$sorted_features = $detector->sorted_features;
	SKIP: {
		skip "More than 1 feature in my GFF database - this should be fixed",1 ;
		is scalar(@{$sorted_features}),1,"One sorted Feature for chromosome";
	};
	my $feat = $sorted_features->[0];
	is $feat->{size},170899991,"Chromosome feature is correct size";
}

sub test_no_prefix_chr_char_is_detected {
	is $detector->detect('Y'),"Feature","Chromosome term returns Feature";
	$sorted_features = $detector->sorted_features;
	SKIP: {
		skip "More than 1 feature in my GFF database - this should be fixed",1 ;
		is scalar(@{$sorted_features}),1,"One sorted Feature for chromosome";
	};
	my $feat = $sorted_features->[0];
	is $feat->{size},57772953,"Chromosome feature is correct size";
}

sub test_no_prefix_chr_number_is_detected {
	is $detector->detect('11'),"Feature","Chromosome term returns Feature";
	$sorted_features = $detector->sorted_features;
	SKIP: {
		skip "More than 1 feature in my GFF database - this should be fixed",1 ;
		is scalar(@{$sorted_features}),1,"One sorted Feature for chromosome";
	};
	my $feat = $sorted_features->[0];
	is $feat->{size},134452383,"Chromosome feature is correct size";
}

#
#
#my $cancer_pids = $query->fval('PhenotypeID');
#
#$detector = $som->make_Detector({
#	name => 'Annotation',
#	filter_list => ['PhenotypeID']
#});
#$query = $detector->query;
#is $detector->detect('neoplasm'),"Annotation","Phenotype ontology term synonym returns 'Annotation'"; 
#my $neoplasm_pids = $query->fval('PhenotypeID');
#
#$detector = $som->make_Detector({
#	name => 'Annotation',
#	filter_list => ['PhenotypeID']
#});
#$query = $detector->query;
#is $detector->detect('neoplasms'),"Annotation","Phenotype ontology term synonym returns 'Annotation'"; 
#my $neoplasms_pids = $query->fval('PhenotypeID');
#
#cmp_deeply($cancer_pids, bag(@{$neoplasm_pids}), "Phenotype IDs returned by synonyms are identical");
#cmp_deeply($neoplasms_pids, bag(@{$neoplasm_pids}), "Phenotype IDs returned by synonyms are identical");

#test actual PIDs = correct PIDs

#test other values

#test mesh vs hpo
