package MockTextFilter;
use Moose;
extends qw(HGVbaseG2P::Search::Filter);
with qw(HGVbaseG2P::Search::Filter::Fieldable);
has '+retrieve_from' => ('default'=>'test');
1;