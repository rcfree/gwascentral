package HGVbaseG2P::Test::Search;
use Test::Base -Base;
our @EXPORT = qw(test_search_recognised_as get_search test_search_count);
field 'ds';
#filters 'recognised_as';

use Data::Dumper qw(Dumper);
use GwasCentral::DataSource::Util qw(DataSources_from_config);
use HGVbaseG2P::AccessControl::None;
use HGVbaseG2P::Core qw(load_module);

sub get_query {
	my $search_type = shift;
	my $ds = DataSources_from_config({
		conf_file => 'conf/hgvbase.conf',
		AccessControl => HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'})
	});
	my $q_modname = "HGVbaseG2P::Search::Query::".ucfirst($search_type);
	my $q = load_module($q_modname,{conf_file=>'conf/hgvbase.conf'});
	$q->DataSources( $ds );
	return $q;
}

sub test_search_recognised_as {
	
	my @data = @_;
	my $q = get_query(@data);
	run {
		my $block = shift;
		$q->reset;
		$q->prepare_filters($block->input);
		$q->results;
		
		is($q->recognised_as, $block->expected, $block->name);
	};
}

sub test_search_count {
	
	my @data = @_;
	my $q = get_query(@data);
	run {
		my $block = shift;
		$q->reset;
		$q->prepare_filters($block->input);
		$q->results;
		
		is($q->pager->total_entries, $block->expected, $block->name);
	};
}

#sub Test::Base::Filter::search {
#	my $self = shift;       # The Test::Base::Filter object
#    my $data = shift;
#    my $args = $self->current_arguments;
#    my $current_block_object = $self->block;
#    print "self:".Dumper($self);
#    print "data:".Dumper($data);
#    print "args:".Dumper($args);
#    print "cbo:".Dumper($current_block_object);
#	my $ds = GwasCentral::DataSource->new({conf_file=>'conf/hgvbase.conf'});
#	$ds->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));
#	my $q_modname = "HGVbaseG2P::Search::Query::".ucfirst($args);
#	my $q = load_module($q_modname,{conf_file=>'conf/hgvbase.conf'});
#	$q->prepare_filters($data);
#	return $q;
#}

#sub Test::Base::Filter::recognised_as {
#	my $query = shift;
#	return "Markers";
#}

