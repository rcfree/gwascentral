package MockQuery;
use Moose;
extends qw(HGVbaseG2P::Search::Query);

use English qw( -no_match_vars );
use Data::Dumper qw(Dumper);
use Text::ParseWords;

has '+possible_Filters' => (default => sub { [
	qw(PageSize Page SortBy QueryString Threshold)]
});
	
has '+possible_DetectTypes' => ('default'=>sub {
	[qw(GeneRegion Marker Keyword None)];
});

has '+Filter_options' => ('default'=> sub {
	 return {
		'SortBy' => {
			'list' => {
				'key1'=>'value1',
				'key2'=>'value2',
				'key3'=>'value3',
				'key4'=>'value4',
				'key5'=>'value5'	 
			},
			'default' => 'value1'
		},
		'PageSize' => {
			'list' => {
				'20'=>'20',
				'50'=>'50',
				'100'=>'100',
				'all'=>'all',
			},
			'default' => '50'
		},
		'MarkerCount' => {
			'filter_type'=>'hidden'
		},
		'QueryString' => {
			'autocomplete_url'=>'/phenotypes/meshconcept',
		}
		
	}
});

sub retrieve_annotation {
	return "annotation";
}

sub retrieve_marker {
	return "marker";
}

sub retrieve_region {
	return "region";
}
sub retrieve_feature {
	return "feature";
}
sub retrieve_keyword {
	return "keyword";
}

sub retrieve_none {
	return "none";
}
1;