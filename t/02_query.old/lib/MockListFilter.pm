package MockListFilter;
use Moose;
extends qw(HGVbaseG2P::Search::Filter);
with qw(HGVbaseG2P::Search::Filter::Listable);
use Tie::Hash::Indexed;
has '+retrieve_from' => ('default'=>'list');

tie my %list, 'Tie::Hash::Indexed';
sub _list_builder { 
	%list = (
		'ZERO' => '-log p &ge; 0',
		'1' => '-log p &ge; 1',
		'2' => '-log p &ge; 2',
		'3' => '-log p &ge; 3',
		'4' => '-log p &ge; 4',
		'5' => '-log p &ge; 5',
		'6' => '-log p &ge; 6',
		'7' => '-log p &ge; 7',
		'8' => '-log p &ge; 8',
		'9' => '-log p &ge; 9',
		'10' => '-log p &ge; 10',
	);
	return \%list;
};

1;