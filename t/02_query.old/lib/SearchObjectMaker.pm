package SearchObjectMaker;

use Moose;
use FindBin;
use lib "$FindBin::Bin/../../lib";

use HGVbaseG2P::Search::Query;
use GwasCentral::DataSource;
extends qw(HGVbaseG2P::Core);
no Moose;

sub make_DetectType {
	my ( $self, $args ) = @_;
	my $query =
	  HGVbaseG2P::Search::Query->new( { conf_file => $self->config } );
	my $ds = GwasCentral::DataSource->new( { conf_file => $self->config } );
	$query->databases( $ds->new_DataSources_from_config() );
	foreach my $filter_name ( @{ $args->{filter_list} } ) {
		my $filter = $query->new_Filter($filter_name);
		$query->filters->{$filter_name} = $filter;
	}
	return $query->new_DetectType( $args->{name} );
}

sub make_Detect {
	my ( $self, $args ) = @_;
	my $query =
	  HGVbaseG2P::Search::Query->new( { conf_file => $self->config } );
	my $ds = GwasCentral::DataSource->new( { conf_file => $self->config } );
	$query->DataSources( $ds->new_DataSources_from_config() );
	if ($args->{filter_list}) {
		foreach my $filter_name ( @{ $args->{filter_list} } ) {
			my $filter = $query->new_Filter($filter_name);
			$query->filters->{$filter_name} = $filter;
		}
		$query->fval( 'QueryString', $args->{querystring} );
	}
	my $perf = $query->new_Detect( $args->{name} );
	

	$query->possible_DetectTypes(
		[qw(ConceptTerm GeneRegion Marker Annotation Keyword None)] );
	$query->possible_Filters(
		[qw(QueryString PhenotypeID RestrictToAnnotation Concept)]);
	return $perf;
}
1;
