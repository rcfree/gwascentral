#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use MockQuery;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw(DataSources_from_config);
use HGVbaseG2P::AccessControl::GAS;
my $ac = HGVbaseG2P::AccessControl::GAS->new({conf_file=>'conf/hgvbase.conf'});
$ac->identity('admin@hgvbaseg2p.org');
my $ds = DataSources_from_config('conf/hgvbase.conf', $ac);

my $qry = new_Query('AdminStudies',{ 
	params => {'q'=>'cancer'}, 
	DataSources => $ds,
	conf_file=>'conf/hgvbase.conf'
});
$qry->prepare_filters;
my $results = $qry->results;
warn "results:".Dumper($results);

