#!perl -w

# $Id: 02_schema.t 1048 2008-10-30 12:53:11Z mummi $

# Test the object-relational model (ORM) layer that we use to get data into and
# out of the database.

use strict;
use warnings;


use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use HGVbaseG2P::Test::SchemaBasicOps;
HGVbaseG2P::Test::SchemaBasicOps->runtests();

