# An example script demonstrating the use of BioMart API.
# This perl API representation is only available for configuration versions >=  0.5 
use strict;
use FindBin;
use lib "$FindBin::Bin/../../../biomart-perl/lib";
use BioMart::Initializer;
use BioMart::Query;
use BioMart::QueryRunner;

my $confFile = "$FindBin::Bin/../../../biomart-perl/conf/martDB.xml";
#
# NB: change action to 'clean' if you wish to start a fresh configuration  
# and to 'cached' if you want to skip configuration step on subsequent runs from the same registry
#

my $action='clean';
my $initializer = BioMart::Initializer->new('registryFile'=>$confFile, 'action'=>$action);
my $registry = $initializer->getRegistry;

my $query = BioMart::Query->new('registry'=>$registry,'virtualSchemaName'=>'default');

		
	$query->setDataset("study");
	
	$query->addAttribute("identifier");
	$query->addAttribute("aa_markerid");
	$query->addAttribute("aa_unadjustedpvalue");
	$query->addAttribute("annotationname");
	$query->addAttribute("aa_hgnc_label");
	#$query->setDataset("hsapiens_gene_ensembl");
	#$query->addFilter("chromosome_name",['10']);
	#$query->addFilter("start",['50000000']);
	#$query->addFilter("end",['70000000']);
	$query->addFilter("hgnc_symbol",["PCDH15"]);
	#$query->addFilter("biotype",[qw(C_segment D_segment J_segment miRNA miRNA_pseudogene misc_RNA misc_RNA pseudogene Mt_rRNA Mt_tRNA Mt_tRNA_pseudogene protein_coding)]);

$query->formatter("CSV");

my $query_runner = BioMart::QueryRunner->new();
############################## GET COUNT ############################
# $query->count(1);
# $query_runner->execute($query);
# print $query_runner->getCount();
#####################################################################


############################## GET RESULTS ##########################
# to obtain unique rows only
# $query_runner->uniqueRowsOnly(1);

$query_runner->execute($query);
exit;
$query_runner->printHeader();
$query_runner->printResults();
$query_runner->printFooter();
#####################################################################
