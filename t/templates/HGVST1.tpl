limit=
plugins=
study_identifier=HGVST1
experiment_identifier=HGVE1
resultset_identifier=HGVRS1
head=
accession_db=dbSNP
comment_prefix=#
frequencies=1
associations=1
allele_order=1 2
gt_order=11 12 22
field_separator=\s
<Fields>
	1=accession
	2=chromosome
	3=position
	4=allele1
	5=allele2
	6=genotype11_number:Case
	7=genotype12_number:Case
	8=genotype22_number:Case
	9=genotype11_number:Control
	10=genotype12_number:Control
	11=genotype22_number:Control
	12=pvalue
</Fields>