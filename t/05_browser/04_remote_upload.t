#!perl -w

# $Id: 01_fileparser.t 1072 2008-11-03 11:32:17Z mummi $

# Test live databases connect ok

use strict;
use warnings;
use Class::Date qw(now);

use FindBin;
use lib ( "$FindBin::Bin/../../lib" );
use Data::Dumper qw(Dumper);
use HGVbaseG2P::Browser::Upload;
use HGVbaseG2P::Database::Browser;
use Test::More qw(no_plan);
use Test::Exception;
my $browser_db = HGVbaseG2P::Database::Browser->new({conf_file=>'conf/hgvbase.conf'});
my $upload_db = HGVbaseG2P::Database::BrowserUpload->new({conf_file=>'conf/hgvbase.conf'});

my $upload = HGVbaseG2P::Browser::Upload->new({conf_file=>'conf/hgvbase.conf'});
$upload->browser_db($browser_db);
$upload->config->{tempdir}="$FindBin::Bin/tmp";

#test throws exception if no file and/or no title given
throws_ok {
	$upload->remote_upload({
		file => undef
	})
} qr/File not provided/, "Throws exception if file not provided";

throws_ok {
	$upload->remote_upload({
		file => "file://".$FindBin::Bin."/data/none.txt"
	})
} qr/Title not provided/, "Throws exception if title not provided";

#check throws exception if no file present
throws_ok {
	$upload->remote_upload({
		file => "file://".$FindBin::Bin."/data/none.txt",
		title => "My title"
	})
} qr/Unable to get data from/, "Throws exception if file not present";

my $curr_date = now;

#check that files are uploaded, but also does not go beyond the config limit (2 usually)
ok scalar(keys %{$upload->upload_data})==0, "No uploaded data in memory";
lives_ok {
	$upload->remote_upload({
		file => "file://".$FindBin::Bin."/data/upload.txt",
		title => "Test Remote Upload ".$curr_date,
	});
} "Lives if file is present";


my $curr_date2 = now;
my $upload_data2;

lives_ok {
	$upload_data2 = $upload->remote_upload({
			file => "file://".$FindBin::Bin."/data/upload.txt",
			title => "Test Remote Upload ".$curr_date2,
	});
} "Lives to upload second file";

my @rsids = sort keys %{$upload_data2};
print "rsids:".Dumper(\@rsids);
my $rs_upload = $upload_db->get_upload_data_for_resultset_ident($rsids[0]);
ok $rs_upload->{Resultset_Name} eq "Test Remote Upload ".$curr_date, "Remote upload 1 is present in Upload DB";

my $rs_upload2 = $upload_db->get_upload_data_for_resultset_ident($rsids[1]);
ok $rs_upload2->{Resultset_Name} eq "Test Remote Upload ".$curr_date2, "Remote upload 2 is present in Upload DB";

my $curr_date3 = now;
my $upload_data3;
throws_ok {
	$upload_data3 = $upload->remote_upload({
			file => "file://".$FindBin::Bin."/data/upload.txt",
			title => "Test Remote Upload ".$curr_date3,
	});
} qr/Sorry you have reached the maximum number of uploads/, "Throws exception if attempt to upload third file";

#delete uploads from memory
$upload->upload_data({});
ok scalar(keys %{$upload->upload_data})==0, "No uploaded data in memory";

my $upload_data4;
#check gzipped file is uploaded
print "starts:".now;
lives_ok {
	$upload_data4 = $upload->remote_upload({
			file => "file://".$FindBin::Bin."/data/large.txt.gz",
			title => "Test GZipped Remote Upload ".$curr_date2,
	});
} "Lives to upload large gzipped file";
print "ends:".now;
print Dumper($upload_data4);