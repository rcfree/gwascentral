use FindBin;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::Browser::Genome;
use GwasCentral::DataSource::Browser;
use GwasCentral::Browser::Upload;
use File::Temp qw(tempfile);
#use Test::More plan tests=>1;
use Array::Compare;
use Data::Dumper qw(Dumper);
use GwasCentral::Base qw(open_infile);

my $sessionid="c29390b47a8607bda7cbb8e2c4f2d307a0717b58";
my ($fh,$filename) = tempfile('hgvtempXXXXXXX');
$fh->close;
#system "cp sirisha_and_tims_mad_file.txt $filename";

my $db = GwasCentral::DataSource::Browser->new({ conf_file=>"conf/main.conf"});

my $bupload = GwasCentral::Browser::Upload->new({ conf_file => "conf/main.conf"});
$bupload->DS('Browser',$db);

for(my $i=0;$i<2;$i++) {
   eval {
    $bupload->do_upload({
		file => "t/data/simple_upload.txt",
		study_name => 'Test study',
		resultset_name => 'Test resultset',
		sessionid => $sessionid,
	});
   };
   if ($@) {
   	warn "$@";
   }
}
print "upload_data:".Dumper($bupload->upload_data);


#my @upload_ids = keys %{$upload_data};
#my $data = $db->get_marker_binned_by_resultsets_and_threshold({
#	bin_size=>'3',
#	rset_access_levels=>{ upload => ['U48'] },
#	threshold=>'ZERO',
#});

#my $webapp_session = $browser->get_webapp_session($sessionid);
#warn "session:".Dumper($webapp_session);
#$webapp_session->{upload_data} = $upload_data;
#$webapp_session->{resultsets}=[];
#$webapp_session->{studies}={};
#$browser->set_webapp_session($sessionid,$webapp_session);
#
#
#$browser->db($db);
#print "data:".Dumper($data->{'18'});

