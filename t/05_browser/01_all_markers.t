use FindBin;
use strict;
use warnings;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::Browser::Creator;
use GwasCentral::Test::MockDB;
use Test::More;
use GwasCentral::Base qw(load_config);
use GwasCentral::DataSource::Util qw(DataSources_from_list);
use Log::Log4perl qw(:easy);
use Log::Log4perl::Level;
Log::Log4perl->easy_init($INFO);

my $mockdb = GwasCentral::Test::MockDB->new({conf_file=>"$FindBin::Bin/../conf/test.conf"});
$mockdb->create_marker_db();
$mockdb->populate_browser_markers();

my $browser_ds = $mockdb->DS('Browser');

test_marker_with_no_revisions();
test_marker_with_1_revision();
test_marker_with_2_revisions();
test_marker_with_3_revisions_with_strandflip();
test_marker_with_revisions_in_2_markers_with_strandflip();
test_marker_with_revisions_in_multiple_markers();
test_dead_marker_with_no_pointer();
test_marker_extra_data();

sub test_marker_extra_data {
	my $marker = $browser_ds->get_marker_by_id("HGVM999");
	is $marker->{Start}, "3792876", "Marker coordinates are correct";
	is $marker->{Alleles},"(C):(A)", "Marker alleles are correct";
	is $marker->{Upstream30bp},"CATCTGGAGGATAAAGAGATCCCTCTGGGG","Upstream 30bp flank is correct";
	is $marker->{Downstream30bp},"CTGTGTCAGGGCAGGACTGACTCAAGGCCT","Downstream 30bp flank is correct";
	is $marker->{MappedGene},"CSMD1","MappedGene is correct";
}

sub test_marker_with_revisions_in_2_markers_with_strandflip {
	my $marker = $browser_ds->get_marker_by_id("HGVM6");
	is $marker->{Accession}, "rs506", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, "HGVM7", "Marker does not have a CurrentIdentifier";
	is $marker->{Status},"dead","Marker is active";
}

sub test_marker_with_revisions_in_multiple_markers {
	my $marker = $browser_ds->get_marker_by_id("HGVM8");
	is $marker->{Accession}, "rs508", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, "HGVM9", "Marker does not have a CurrentIdentifier";
	is $marker->{Status},"dead","Marker is dead";
}

sub test_marker_with_no_revisions {
	my $marker = $browser_ds->get_marker_by_id("HGVM1");
	is $marker->{Accession}, "rs501", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, undef, "Marker does not have a CurrentIdentifier";
	is $marker->{Status},"active","Marker is active";
}

sub test_marker_with_1_revision {
	my $marker = $browser_ds->get_marker_by_id("HGVM11");
	is $marker->{Accession}, "rs5011", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, "HGVM10", "Marker has correct CurrentIdentifier";
	is $marker->{Status},"dead","Marker is dead";
	$marker = $browser_ds->get_marker_by_id($marker->{CurrentIdentifier});
	is $marker->{Accession}, "rs5010", "Marker retrieved successfully";
	is $marker->{Status},"active","Marker is active";
}
sub test_marker_with_2_revisions {
	my $marker = $browser_ds->get_marker_by_id("HGVM5");
	is $marker->{Accession}, "rs505", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, "HGVM4", "Marker has correct CurrentIdentifier";
}
sub test_marker_with_3_revisions_with_strandflip {
	my $marker = $browser_ds->get_marker_by_id("HGVM2");
	is $marker->{Accession}, "rs502", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, "HGVM4", "Marker has correct CurrentIdentifier";
}

sub test_dead_marker_with_no_pointer {
	my $marker = $browser_ds->get_marker_by_id("HGVM14");
	is $marker->{Accession}, "rs5014", "Marker retrieved successfully";
	is $marker->{CurrentIdentifier}, undef, "Marker has no CurrentIdentifier";
}



