use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Browser::Upload;
use HGVbaseG2P::Database;
use HGVbaseG2P::Util qw(load_config);
my %conf = load_config('conf/hgvbase.conf');
my $db = HGVbaseG2P::Database->new({conf_file=>\%conf});
my $browser_upload = HGVbaseG2P::Browser::Upload->new({conf_file=>\%conf});
$browser_upload->upload_data({});
$browser_upload->browser_db($db->new_instance('browser'));
my %params = (
	file => 'https://212.68.17.106/bcospub/large_data.gz',
	title => 'remote upload test',
);


my @rs_idents = $browser_upload->remote_upload(\%params);