use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Search::Query::Marker;
use HGVbaseG2P::Database;
use Env;
use Data::Dumper qw(Dumper);
$ENV{DBIC_TRACE}=0;
my $pquery = HGVbaseG2P::Search::Query::Marker->new({conf_file=>'conf/hgvbase.conf'});
my $db = HGVbaseG2P::Database->new({conf_file=>'conf/hgvbase.conf'});
$pquery->databases({
	Feature => $db->new_instance('Feature'),
	Marker => $db->new_instance('Marker'),
	Study => $db->new_instance('Study'),
	Ontology => $db->new_instance('Ontology'),
	HPO => $db->new_instance('Hpo'),
	Xapian => $db->new_instance('Xapian'),
	Browser => $db->new_instance('Browser')
});
my $ac = HGVbaseG2P::AccessControl->new({conf_file=>'conf/hgvbase.conf'});

#$pquery->sortfield('identifier');
$pquery->access_control($ac);
my @data = @{ $pquery->results({'q'=>'Cancer of Ovary','t'=>'8', page => 1, 'page_size'=>50}) };
print scalar(@data)." results returned";

print Dumper(\@data);