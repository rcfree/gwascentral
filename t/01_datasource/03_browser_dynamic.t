#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/lib";
use HGVbaseG2P::DataSource::Browser::Dynamic;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::DataSource::Study::DatabaseMock;
use HGVbaseG2P::DataSource::Study::WebServiceMock;
use HGVbaseG2P::AccessControl::None;

#test_init_dynamic_datasources();
test_get_binned_genome_data();

sub test_init_dynamic_datasources {
	#test dynamic Study DataSource
	my $dynamic_ds = HGVbaseG2P::DataSource::Browser::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
	
	#$dynamic_ds->server(
	#test dynamic DS has pre-initialized DataSources
	is scalar(keys %{$dynamic_ds->DataSources}),2,"DataSources pre-initialized";
	is ref($dynamic_ds->DataSources->{Local}), 'HGVbaseG2P::DataSource::Browser::Local', "Database DataSource for Study initialized";
	is ref($dynamic_ds->DataSources->{Remote}), 'HGVbaseG2P::DataSource::Browser::Remote', "WebService DataSource for Study initialized";
	#
	##test dynamic Study DataSource
	my $dynamic_ds2 = HGVbaseG2P::DataSource::Browser::Dynamic->new({conf_file=>'conf/hgvbase.conf', no_DataSources=>1});
	#
	##test dynamic DS does have initialized DataSources
	is scalar(keys %{$dynamic_ds2->DataSources}),0,"DataSources NOT pre-initialized";
}

sub test_get_binned_genome_data {
	my $dynamic_ds2 = HGVbaseG2P::DataSource::Browser::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
	my $ac = HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'});
#	
#	##replace DataSources with mock ones!
#	$dynamic_ds2->DataSources({
#		Remote=>HGVbaseG2P::DataSource::Study::WebServiceMock->new({conf_file=>'conf/hgvbase.conf'}),
#		Local=>HGVbaseG2P::DataSource::Study::DatabaseMock->new({conf_file=>'conf/hgvbase.conf'}),
#	});
#	#
	#isa_ok($dynamic_ds2,"HGVbaseG2P::DataSource::Study::Dynamic");
	#$dynamic_ds2->server("gwascentral.localhost");
	$dynamic_ds2->local_server("gwascentral.org");
	my ($data,$counts) = $dynamic_ds2->get_marker_binned_by_resultsets_and_threshold({
		resultsets => [
			{identifier => 'HGVRS102', name => 'test3', accesslevel => 'full'},
			{identifier => 'HGVRS103', name =>'test4', accesslevel => 'full'}
		],
		threshold => 3,
		bin_size => '3',
	});
	$dynamic_ds2->log->info("data:".Dumper($data));
	#
	#check recognition of URI rather than ident (uses mock object)
#	$study = $dynamic_ds2->get_study_by_identifier("doggwas.org/study/HGVST452");
#	is $study->name, "WebService Study HGVST452", "Study returned by WebServiceMock rather than Database";
#	is $study->accesslevel, undef, "Study access level undefined"; 
	##
	#throws_ok { 
	#	$dynamic_ds2->is_controlled(1);
	#} qr/AccessControl module not present/, "Setting 'is_controlled' without an AccessControl module is not allowed";
	
	
	$dynamic_ds2->AccessControl($ac);
}
#lives_ok {
#	$dynamic_ds2->is_controlled(1);
#} "Setting 'is_controlled' with an AccessControl module is allowed";
##
#my $dynamic_ds3 = HGVbaseG2P::DataSource::Study::Dynamic->new({
#	conf_file=>'conf/hgvbase.conf', AccessControl=>$ac, no_DataSources => 1
#});
#
#$dynamic_ds3->DataSources({
#	Remote=>HGVbaseG2P::DataSource::Study::WebServiceMock->new({conf_file=>'conf/hgvbase.conf'}),
#	Local=>HGVbaseG2P::DataSource::Study::DatabaseMock->new({conf_file=>'conf/hgvbase.conf'}),
#});
##
##get study with access level as expected
#$study = $dynamic_ds3->get_study_by_identifier("HGVST307");
#my $al = $ac->get_study_levels($study);
#is $al->{'HGVST307'}, "mock","Mock Access level returns 'mock'";
#
#is $study->name, "Database Study HGVST307", "Study returned by Database rather than WebServiceMock";
#is $study->access_level, "mock", "Study access level is 'mock'"; 
##
#$study = $dynamic_ds3->get_study_by_identifier("doggwas.org/study/HGVST567");
#is $study->name, "WebService Study HGVST567", "Study returned by WebService rather than DatabaseMock";
#is $study->access_level, "mock", "Study access level is 'mock'";
#my $server = URI->new($dynamic_ds3->DataSources->{Remote}->server);
#is $server->authority, 'doggwas.org', "WebService server is set";
#$dynamic_ds->AccessControl($ac);
#
#use HGVbaseG2P::Search::Filter::QueryString;
#my %filters = (
#	'QueryString' => HGVbaseG2P::Search::Filter::QueryString->new({value=>'Cancer'})
#);
#$dynamic_ds->server('localhost:8000');
#
#warn Dumper($dynamic_ds->get_study_list({'filters'=>\%filters}));




