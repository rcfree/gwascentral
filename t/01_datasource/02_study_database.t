#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use HGVbaseG2P::DataSource::Study::Local;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::AccessControl::None;

my $ds = HGVbaseG2P::DataSource::Study::Local->new({conf_file=>'conf/hgvbase.conf'});
isa_ok($ds,"HGVbaseG2P::DataSource::Study::Local");
$ds->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));

test_get_study_by_identifier();
#test_count_phenotypeannotations_by_phenotypeidentifier();

sub test_count_phenotypeannotations_by_phenotypeidentifier {
	$ds->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));
	$ds->AccessControl->identity(undef);
	$ds->portal('cancergwas');
	$ds->is_controlled(1);
	my ($count) = $ds->count_phenotypeannotations_by_phenotypeidentifier('D010051');
	is $count, 1,"Count of phenotypeannotation by phenotypeidentifier is correct";
}

sub test_get_paged_phenotypemethods_rs {
	$ds->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));
	$ds->AccessControl->identity(undef);
	$ds->is_controlled(1);
		my ($pmethods,$count,$pager) = $ds->get_paged_phenotypemethods_rs({
			pmethod_idents => ['HGVPM1','HGVPM200','HGVPM201','HGVPM202'],
			phenotype_ids => '',
			threshold => '0',
			page => 1,
			page_size => 20,
			order_by => 'me.identifier ASC',
		});
		
	warn "access_level:".Dumper([map { $_->identifier . " = " . $_->access_level } @{$pmethods}]);
	warn "response:".Dumper([map { $_->identifier . " = " . $_->response_status } @{$pmethods}]);
	warn "child_access_level:".Dumper([map { $_->identifier . " = " . $_->child_access_level } @{$pmethods}]);
}

sub test_get_study_by_identifier {
	##test non dynamic Study DataSource
	throws_ok {
		my $null_study = $ds->get_study_by_identifier(undef);
	} qr/Study identifier not supplied/, "Null study throws exception";
	throws_ok {
		my $null_study = $ds->get_study_by_identifier("HGVSTXXX");
	} qr/Study with identifier '.*' does not exist/, "Non-existing study throws exception";
	lives_ok {
	my $study = $ds->get_study_by_identifier("HGVST306");
	} "Gets existing study";
}