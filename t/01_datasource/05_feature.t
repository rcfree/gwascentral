use GwasCentral::DataSource::Util qw(new_DataSource);
use Test::More;
my $ds = new_DataSource('Feature','t/conf/test.conf');

is $ds->get_mapped_gene("chr8",3792876,3792876),"CSMD1","Mapped gene is correct";
