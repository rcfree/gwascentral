package HGVbaseG2P::DataSource::Study::DatabaseMock;
use Moose;

extends qw(HGVbaseG2P::DataSource::Study);
with 'HGVbaseG2P::DataSource::Servable';

augment 'get_study_by_identifier' => sub {
	my ($self,$identifier) = @_;

	return HGVbaseG2P::Schema::Study::Study->new(
	{identifier=> $identifier,
		name => "Database Study $identifier"
	});
};

no Moose;

sub get_study_by_name {
	my ($self,$name) = @_;
	return HGVbaseG2P::Schema::Study::Study->new(
	{identifier=> 'HGVST555',
		name => "Database Study name $name"
	});
}

1;