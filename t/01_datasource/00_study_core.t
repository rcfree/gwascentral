#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use HGVbaseG2P::DataSource::Connect;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use Log::Log4perl qw(:easy);
use Digest::MD5 qw(md5_hex);
use GwasCentral::Search::Util qw(Query_from_config new_Federate);
use GwasCentral::DataSource::Util qw(DataSources_from_config);
use GwasCentral::Base qw(load_module);
use GwasCentral::Search::Federate;
use Env;
$ENV{"HGVBASEG2PWEB_HOME"}="/var/www/gwascentral";
$Data::Dumper::Maxdepth=2;
Log::Log4perl->easy_init($DEBUG);
##test non dynamic Study DataSource
my $conf_file = "$FindBin::Bin/../../conf/viti";

my $connect = GwasCentral::Connect->new({conf_file=>$conf_file, local_url => 'http://192.168.56.4/'});
my $ac = load_module("GwasCentral::AccessControl::Simple",{conf_file=>$conf_file});
$ac->Connect($connect);	
$connect->db( HGVbaseG2P::DataSource::Connect->new({conf_file=>$conf_file}));

#test_get_study_by_identifier();
test_get_study_list();


#$ac->identity('admin@hgvbaseg2p.org');
#$ac->Connect($connect);
#
#my $ds = DataSources_from_config($conf_file, $ac, $connect);
#my $sf = GwasCentral::Search::Federate->new({Query_name=>'Studies', conf_file=>$conf_file, Connect => $connect, DataSources=>$ds});
#$sf->params({'page'=>1, 'page_size'=>50, 'o'=>'Identifier (ascending)', 't'=>'ZERO'});
##warn "results:".$sf->output('yaml');
#warn "content_type:".load_module("GwasCentral::Search::Format::atom")->content_type;

sub test_get_study_list {
	my $f = new_Federate('Studies',{conf_file=>'conf/main.conf', AccessControl=>$ac, DataSources => DataSources_from_config('conf/main.conf',$ac, $connect) });
	$f->Connect($connect);
	$f->prepare_Filters({'t'=>'ZERO', 'q'=>'bipolar disorder', 'jsonp'=>1});
	warn "format:".$f->Format;
	warn "output:".$f->output;
#	my $instance = $connect->db->get_instance_by_label('gwascentral');
#	my ($studies,$pager) = $ds->get_study_list({
#		Query=>$q,
#		instance => $instance,
#		identifiers => 'HGVST316'
#	});

#	#my $instance = $connect->db->get_instance_by_label('gconnect1');
#	my ($studies,$pager) = $ds->get_study_list({
#		Query=>$q,
#		instance => $instance,
#		identifiers => 'HGVST316'
#	});
	INFO "studies:".Dumper($studies);
}

sub test_get_study_by_identifier {
	throws_ok {
		my $null_study = $ds->get_study_by_identifier(undef);
	} "GwasCentral::Exception::DataSource" , "Null study throws exception";
	
	throws_ok {
		my $not_present_study = $ds->get_study_by_identifier('HGVST99');
	} "GwasCentral::Exception::DataSource", "Study not present throws exception";
	
	my $study = $ds->get_study_by_identifier('HGVST316');
	isa_ok $study, "HGVbaseG2P::Schema::Study::Study","Study present is returned";
	is $study->identifier, 'HGVST316',"Local study returned when no remote connect label";
	is $study->gcid, 'gwascentral.study.HGVST316', "Local study has expected gcid";
	INFO "study access:".$study->accesslevel;
	
	my $remote_study = $ds->get_study_by_identifier('gconnect1.study.HGVST316');
	is $remote_study->{identifier},"HGVST316","ConnectID returns study from gconnect1";
	is $remote_study->{accesslevel},"admin","Access level is admin for this study";
	is $remote_study->{gcid}, 'gconnect1.study.HGVST316', "Remote study has expected gcid";
	
	throws_ok {
		my $remote_study = $ds->get_study_by_identifier('gconnect1.study.HGVST99');
	} "GwasCentral::Exception::DataSource", "Study not present in gconnect1 throws exception";
	
	$ds->Connect->remote('gconnect1');
	$remote_study = $ds->get_study_by_identifier('HGVST316');
	is $remote_study->{identifier},"HGVST316","Study is remote when connect set to gconnect1";
	is $remote_study->{gcid}, 'gconnect1.study.HGVST316', "Remote study has expected gcid";
	
	$ds->Connect->remote('gwascentral');
	$remote_study = $ds->get_study_by_identifier('HGVST316');
	isa_ok $remote_study, "HGVbaseG2P::Schema::Study::Study","Study is local when remote connect set to gwascentral";
	is $remote_study->gcid, 'gwascentral.study.HGVST316', "Study has expected gcid";
	
	$ds->Connect->remote('gconnect1');
	$remote_study = $ds->get_study_by_identifier('gwascentral.study.HGVST316');
	isa_ok $remote_study, "HGVbaseG2P::Schema::Study::Study","ConnectID with gprimary connect overrides remote gconnect1";
	is $remote_study->gcid, 'gwascentral.study.HGVST316', "Study has expected gcid";
}