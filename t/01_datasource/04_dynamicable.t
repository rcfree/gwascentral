#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/lib";
use HGVbaseG2P::DataSource::Study::Dynamic;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::DataSource::Study::DatabaseMock;
use HGVbaseG2P::DataSource::Study::WebServiceMock;
use HGVbaseG2P::AccessControl::None;

#test dynamic Study DataSource
my $dynamic_ds = HGVbaseG2P::DataSource::Study::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
my $ac = HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'});
$dynamic_ds->AccessControl($ac);
my ($ident, $server) = $dynamic_ds->get_server_from_identifier("HGVST5");
warn "ident:$ident, server:".($server || 'undef');

($ident, $server) = $dynamic_ds->get_server_from_identifier("doggwas.org/HGVST5");
warn "ident:$ident, server:".($server || 'undef');


($ds,$new_identifier) = $dynamic_ds->source_using_identifier("HGVST5");
warn "ds:$ds, new_ident:$new_identifier, server:".($dynamic_ds->current_server || 'undef');

($ds,$new_identifier) = $dynamic_ds->source_using_identifier("doggwas.org/HGVST5");
warn "ds:$ds, new_ident:$new_identifier, server:".($dynamic_ds->current_server || 'undef');

my $rs = $dynamic_ds->get_resultset_by_identifier("HGVRS102");
warn "rs:".$rs->identifier;

$rs = $dynamic_ds->get_resultset_by_identifier("gwascentral.localhost/HGVRS102");
warn "rs:".$rs->{identifier};
