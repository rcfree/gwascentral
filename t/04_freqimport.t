#!perl -w

# $Id: 04_freqimport.t 700 2008-06-04 14:09:42Z rob $

# Test genotype import pipeline

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/";
use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use FindBin;
use Data::Dumper qw(Dumper);
#use HGVbaseG2P::Test::DataImport::Frequency::WTCCC;
use HGVbaseG2P::Test::DataImport::Frequency::Broad;
use HGVbaseG2P::Test::DataImport::Frequency::CGEMS;
use HGVbaseG2P::Test::DataImport::Frequency::dbGaP;
HGVbaseG2P::Test::DataImport::Frequency::Broad->runtests;

#HGVbaseG2P::Test::DataImport::Frequency::CGEMS->runtests;
#sleep 5;
#HGVbaseG2P::Test::DataImport::Frequency::dbGaP->runtests;