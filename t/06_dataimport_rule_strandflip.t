#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - strandflips

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::DataImport::DataElement;
use HGVbaseG2P::DataImport::Rule::StrandFlip::Field;
use HGVbaseG2P::DataImport::Rule::StrandFlip::File;
use HGVbaseG2P::DataImport::Rule::StrandFlip::Custom;
use Bio::SeqFeature::Generic;


my $neg_marker = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '-1',
	-source => 'dbSNP',
	-score => '123',
	-display_name => 'rs123',
	-tag          => {chr => '3', alleles=>[qw(T G)], upstream30bp =>'', downstream30bp =>'' }
);

my $pos_marker = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '1',
	-source => 'dbSNP',
	-score => '123',
	-display_name => 'rs123',
	-tag          => {chr => '3', alleles=>[qw(A C)], upstream30bp =>'', downstream30bp =>'' }
);


test_field_flip();
test_file_flip();
test_custom_flip();

sub test_field_flip {
	#field flip
	my $r1 = HGVbaseG2P::DataImport::Rule::StrandFlip::Field->new;
	
	
	#test same strand (-)
	my $neg_de = get_neg_de();
	$neg_de->marker($neg_marker);
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'T', "Field Strands equivalent (- and -1) allele 1 is same (T)";
	ok $neg_de->line->{allele2} eq 'G', "Field Strands equivalent (- and -1) allele 2 is same (G)";
	
	$neg_de->line->{strand}='-1';
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'T', "Field Strands equivalent (-1 and -1) allele 1 is same (T)";
	ok $neg_de->line->{allele2} eq 'G', "Field Strands equivalent (-1 and -1) allele 2 is same (G)";
	
	#test same strand (- and -1)
	my $pos_de = get_pos_de();
	$pos_de->marker($pos_marker);
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'A', "Field Strands equivalent (+ and 1) allele 1 is same (A)";
	ok $pos_de->line->{allele2} eq 'C', "Field Strands equivalent (+ and 1) allele 2 is same (C)";
	
	$pos_de->line->{strand}='+1';
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'A', "Field Strands equivalent (+1 and 1) allele 1 is same (A)";
	ok $pos_de->line->{allele2} eq 'C', "Field Strands equivalent (+1 and 1) allele 2 is same (C)";
	
	#test different strands (+1 and -1)
	$pos_de = get_pos_de();
	$pos_de->marker($neg_marker);
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'T', "Field Strands diff (+1 and -1) allele 1 is flipped (T)";
	ok $pos_de->line->{allele2} eq 'G', "Field Strands diff (+1 and -1) allele 2 is flipped (G)";
	
	#test different strands (+ and -1)
	$pos_de = get_pos_de();
	$pos_de->line->{strand}='+';
	$pos_de->marker($neg_marker);
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'T', "Field Strands diff (+ and -1) allele 1 is flipped (T)";
	ok $pos_de->line->{allele2} eq 'G', "Field Strands diff (+ and -1) allele 2 is flipped (G)";
	
	#test different strands (-1 and +1)
	$neg_de = get_neg_de();
	$neg_de->marker($pos_marker);
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'A', "Field Strands diff (-1 and 1) allele 1 is flipped (A)";
	ok $neg_de->line->{allele2} eq 'C', "Field Strands diff (-1 and 1) allele 2 is flipped (C)";
	
	#test different strands (- and +1)
	$neg_de = get_neg_de();
	$neg_de->line->{strand}='-';
	$neg_de->marker($pos_marker);
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'A', "Field Strands diff (- and 1) allele 1 is flipped (A)";
	ok $neg_de->line->{allele2} eq 'C', "Field Strands diff (- and 1) allele 2 is flipped (C)";
}

sub test_file_flip {
	#file flip
	my $r1 = HGVbaseG2P::DataImport::Rule::StrandFlip::File->new;
	
	
	#test same strand (-1)
	my $neg_de = get_neg_de();
	$neg_de->marker($neg_marker);
	$r1->settings->{strand}='-1';
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'T', "File Strands equivalent (-1 and -1) allele 1 is same (T)";
	ok $neg_de->line->{allele2} eq 'G', "File Strands equivalent (-1 and -1) allele 2 is same (G)";
	
	#test same strand (1)
	my $pos_de = get_pos_de();
	$pos_de->marker($pos_marker);
	$r1->settings->{strand}='1';
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'A', "File Strands equivalent (1 and 1) allele 1 is same (A)";
	ok $pos_de->line->{allele2} eq 'C', "File Strands equivalent (1 and 1) allele 2 is same (C)";

	
	#test different strands (1 and -1)
	$pos_de = get_pos_de();
	$pos_de->marker($pos_marker);
	$r1->settings->{strand}='-1';
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'T', "File Strands diff (1 and -1) allele 1 is flipped (T)";
	ok $pos_de->line->{allele2} eq 'G', "File Strands diff (1 and -1) allele 2 is flipped (G)";
	
	#test different strands (-1 and 1)
	$pos_de = get_pos_de();
	$pos_de->marker($neg_marker);
	$r1->settings->{strand}='1';
	$r1->run($pos_de);
	ok $pos_de->line->{allele1} eq 'T', "File Strands diff (-1 and 1) allele 1 is flipped (T)";
	ok $pos_de->line->{allele2} eq 'G', "File Strands diff (-1 and 1) allele 2 is flipped (G)";
}

sub test_custom_flip {
	my $r1 = HGVbaseG2P::DataImport::Rule::StrandFlip::Custom->new;
		
	#test same strand (-1)
	my $neg_de = get_neg_de();
	$neg_de->marker($neg_marker);
	$neg_de->flip(0);
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'T', "Custom Flip set to 0 - allele 1 is same (T)";
	ok $neg_de->line->{allele2} eq 'G', "Custom Flip set to 0 - allele 2 is same (G)";
	
	#test different strands (1 and -1)
	$neg_de = get_neg_de();
	$neg_de->marker($neg_marker);
	$neg_de->flip(1);
	$r1->run($neg_de);
	ok $neg_de->line->{allele1} eq 'A', "Custom Flip set to 1 - allele 1 is flipped (A)";
	ok $neg_de->line->{allele2} eq 'C', "Custom Flip set to 1 - allele 2 is flipped (C)";
}

sub get_pos_de {
	my $pos_de = HGVbaseG2P::DataImport::DataElement->new;
	$pos_de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'A','allele2'=>'C','strand'=>'+'});
	$pos_de;
}

sub get_neg_de {
	my $neg_de = HGVbaseG2P::DataImport::DataElement->new;
	$neg_de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000', 'allele1'=>'T','allele2'=>'G','strand'=>'-'});
	$neg_de;
}
