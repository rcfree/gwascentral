#!perl -w

# $Id$

# Test parts of the web application which return raw JSON data structures, whether
# for Exhibit-enabled pages or other purposes.

use strict;
use warnings;


use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use HGVbaseG2P::Test::Web::JSON;
HGVbaseG2P::Test::Web::JSON->runtests();

