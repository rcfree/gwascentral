#!perl -w

# $Id: 10_webapp_pages.t 1274 2009-08-21 15:15:08Z rcf8 $

# Test most or all full pages of the web application, mainly to see if they work at
# all (as in return 200=successful) and do basic screen-scraping to see if they contain
# the expected content.

use strict;
use warnings;


use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;

use HGVbaseG2P::Test::Web::Pages;
HGVbaseG2P::Test::Web::Pages->runtests();

