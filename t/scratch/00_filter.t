#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use MockTextFilter;
use MockListFilter;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);

test_generic_text_filter();
test_generic_list_filter();
test_generic_checkbox_filter();
test_specific_filters()
#specific filters
#ok $filter->does("HGVbaseG2P::Search::Filter::Fieldable"),"Field filter has the correct role";

sub test_specific_filters {
	#my $filter = HGVbaseG2P::Search::Filter::->new({conf_file=>'conf/hgvbase.conf'});
}

sub test_generic_text_filter {
	my $filter = MockTextFilter->new({conf_file=>'conf/hgvbase.conf'});

	throws_ok {
	$filter->populate({'test' => []});
	} qr/Text\/checkbox filter .+ value cannot be an array/, "Throws exception if text filter value is array";
	
	lives_ok {
	$filter->populate({'test' => undef});
	} "Lives if text filter value is undef";
	
	lives_ok {
	$filter->populate({'test' => "1.2.3.."});
	} "Lives if text filter value is actual value";
	
	ok $filter->value eq "1.2.3..","Value has been set in filter";
	
}

sub test_generic_checkbox_filter {
	my $filter = MockCheckboxFilter->new({conf_file=>'conf/hgvbase.conf'});

	throws_ok {
	$filter->populate({'test' => []});
	} qr/Text\/checkbox filter .+ value cannot be an array/, "Throws exception if text filter value is array";
	
	lives_ok {
	$filter->populate({'test' => undef});
	} "Lives if text filter value is undef";
	
	lives_ok {
	$filter->populate({'test' => "1.2.3.."});
	} "Lives if text filter value is actual value";
	
	ok $filter->value eq "1.2.3..","Value has been set in filter";
	
}
sub test_generic_list_filter {
	my $filter = MockListFilter->new({conf_file=>'conf/hgvbase.conf'});

	throws_ok {
	$filter->populate({'list' => []});
	} qr/List filter .+ value cannot be an array/, "Throws exception if list filter value is array";
	
	throws_ok {
	$filter->populate({'list' => 'NONE'});
	} qr/List filter .+ value .+ is not in the available list/, "Throws exception if value is not in list";
	
	lives_ok {
	$filter->populate({'list' => "ZERO"});
	} "Lives if value is in list";
	ok $filter->value eq "ZERO","Value has been set in filter";
	$filter->keys_as_values(1);
	throws_ok {
	$filter->populate({'list' => "ZERO"});
	} qr/List filter .+ value .+ is not in the available list/, "Throws exception when 'keys_as_values' if key not in list";
	lives_ok {
	$filter->populate({'list' => "-log p &ge; 0"});
	} "Lives if key is in list";
}