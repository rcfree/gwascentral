#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);

use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);

my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";
my $query = new_Query( 'Phenotypes', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'page'=>1, 'page_size'=>20, 'q'=>undef, 't'=>'ZERO', 'o'=>'Identifier (ascending)', 'format'=>'json', 'rt'=>1});
$query->prepare_Filters;
print $query->output;