use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib";
use lib "$FindBin::Bin/../../lib";
use SearchObjectMaker;

use Test::More;
use Test::Exception;
use Test::Deep;
my $som = SearchObjectMaker->new({conf_file=>'conf/hgvbase.conf'});
use HGVbaseG2P::Search::Detect::Auto;
use MockQuery;

test_performer_retrieves_none();
test_performer_retrieves_keyword();
test_performer_retrieves_annotation();
test_performer_retrieves_marker();
test_performer_retrieves_concept_if_pid();
test_performer_restrict_to_annotation_works();

sub test_performer_retrieves_none {
	my $d = get_Detect();

	is $d->perform, "none", "Query uses 'retrieve_none for empty query";
}


sub test_performer_retrieves_concept_if_pid {
	my $performer = get_mock_performer();
	my $query = $performer->query;
	$query->params({'q'=>undef, 'pid' => 'D1313423'});
	$query->prepare_filters;
	is $performer->perform, "concept", "Query uses 'retrieve_ct' for concept specified";
}

sub test_performer_retrieves_keyword {
	my $d = get_Detect();
	$d->Query->params({'q'=>'cancer'});
	$d->Query->prepare_filters;
	is $d->perform, "keyword", "Query uses 'retrieve_keyword for keyword 'cancer'";
}

sub test_performer_retrieves_annotation {
	my $d = get_Detect();
	$d->Query->params({'q'=>'cancer'});
	$d->Query->prepare_filters;
	is $d->perform, "keyword", "Query uses 'retrieve_keyword for keyword 'cancer'";
}

sub test_performer_retrieves_marker {
	my $performer = get_mock_performer('rs699');
	is $performer->perform, "marker", "Query uses 'retrieve_marker for marker 'rs699'";
	
	$performer = get_mock_performer('HGVM510');
	is $performer->perform, "marker", "Query uses 'retrieve_marker for marker 'HGVM510'";
}

sub test_performer_restrict_to_annotation_works {
	my $performer = get_mock_performer();
	my $query = $performer->query;
	$query->params({'rt'=>1, 'q'=>'diabetes'});
	$query->prepare_filters;
	is $performer->perform, "none", "Query uses 'retrieve_none' for non-ontology term 'diabetes' when restricted";
	
	$performer = get_mock_performer();
	$query = $performer->query;
	$query->params({'rt'=>0, 'q'=>'diabetes'});
	$query->prepare_filters;
	is $performer->perform, "keyword", "Query uses 'retrieve_keyword' for non-ontology term 'diabetes' when not restricted";

	$performer = get_mock_performer();
	$query = $performer->query;
	$query->params({'rt'=>1, 'q'=>'rs699'});
	$query->prepare_filters;
	is $performer->perform, "none", "Query uses 'retrieve_none' for non-ontology term 'rs699' when restricted";
	
	$performer = get_mock_performer();
	$query = $performer->query;
	$query->params({'rt'=>0, 'q'=>'rs699'});
	$query->prepare_filters;
	is $performer->perform, "marker", "Query uses 'retrieve_marker' for non-ontology term 'rs699' when not restricted";
	
}

sub get_Detect {
	my ($querystring) = @_;
	my $ds = GwasCentral::DataSource->new({conf_file=>'conf/hgvbase.conf'});
	#
	
#	return $som->make_Performer({
#		name => 'AutoDetect',
#		querystring => $querystring,
#		Retriever => MockRetriever->new,
#		filter_list => [qw(PhenotypeID QueryString RestrictToAnnotation Concept)]
#	});
	my $q = MockQuery->new({conf_file=>'conf/hgvbase.conf'});
	$q->DataSources( $ds->new_DataSources_from_config() );
	my $d = HGVbaseG2P::Search::Detect::Auto->new({conf_file=>'conf/hgvbase.conf'});
	$d->Query($q);
	return $d;
	#$q->populate_list;
}



