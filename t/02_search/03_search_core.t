#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use MockQuery;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::Database;
use HGVbaseG2P::Search;
use HGVbaseG2P::DataSource::Study::Dynamic;
use HGVbaseG2P::DataSource::Browser::Dynamic;
use HGVbaseG2P::DataSource::Xapian::Database;
use HGVbaseG2P::Search::Formatter;

#use MockStudyDataSource;
use HGVbaseG2P::AccessControl::None;

my $db = HGVbaseG2P::Database->new({conf_file=>'conf/hgvbase.conf'});

my $study_source = HGVbaseG2P::DataSource::Study::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
$study_source->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));
$study_source->is_controlled(1);

my $browser_source = HGVbaseG2P::DataSource::Browser::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
$browser_source->AccessControl(HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'}));
$browser_source->is_controlled(1);

my $xapian_source = HGVbaseG2P::DataSource::Xapian::Database->new({conf_file=>'conf/hgvbase.conf'});

my %databases = (
	OntologyDB => $db->new_instance('Ontology'),
	HpoDB => $db->new_instance('Hpo'),
	MarkerDB => $db->new_instance('Marker'),
	SeqFeature => $db->new_instance('Feature'),
	StudyDB => $study_source,
	Xapian => $xapian_source,
	BrowserDB => $browser_source,
);

#test_json_search();
#test_html_search();

my $search = HGVbaseG2P::Search->new({conf_file=>'conf/hgvbase.conf'});
my ( $filename, $qry ) = $search->search_single({ 
	type => 'Studies', 
	params => {'t'=>3,'page_size'=>'all'}, 
	models => \%databases,
});
my $results = $qry->results;
ok ref($results) =~ qr/ARRAY/, "Results for page size all are an array";
cmp_ok scalar(@{$results}),">",300, "Number of results returned is large!";

sub test_json_search {
	my $search = HGVbaseG2P::Search->new({conf_file=>'conf/hgvbase.conf'});
	my ( $filename, $qry ) = $search->search_single({ 
		type => 'Studies', 
		params => {'q'=>'cancer', 't'=>8}, 
		models => \%databases,
		content_type => 'text/json',
	});
	
	my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
	$json_coder->space_after(1);
	lives_ok { $json_coder->decode($qry->output) } "JSON formatted text";
	is $qry->content_type, 'text/json', "JSON content-type";
	is $qry->format->value, 'json', "JSON format value";
	is $qry->is_html,undef,"Output is not HTML";
	is $filename, "studies.txt","Output filename is correct";
}

sub test_html_search {
	my $search = HGVbaseG2P::Search->new({conf_file=>'conf/hgvbase.conf'});
	my ( $filename, $qry ) = $search->search_single({ 
		type => 'Studies', 
		params => {'q'=>'DMD', 't'=>3,'m'=>'0,1000000'}, 
		models => \%databases,
	});
	is $qry->is_html,1,"Output is HTML";
	ok ref($qry->results) =~ qr/ARRAY/, "Results are an array";
	is $qry->content_type, 'application/x-www-form-urlencoded', "HTML content-type";
	is $qry->format->value, 'html', "HTML format value";
}
