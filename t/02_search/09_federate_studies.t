use Test::More;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use GwasCentral::Search::Filter::ResultSet;
use GwasCentral::Search::Util qw(new_Federate);
use GwasCentral::Base qw(load_config);

use Data::Dumper qw(Dumper);
use GwasCentral::Connect;
use GwasCentral::AccessControl::Simple;

my %config = load_config('conf/main.conf');

my $ac = GwasCentral::AccessControl::Simple->new({conf_file=>\%config});
my $connect = GwasCentral::Connect->new({conf_file=>\%config, local_url => "http://192.168.56.4" });
my $cdb = HGVbaseG2P::DataSource::Connect->new({conf_file=>\%config});
$connect->db($cdb);
$ac->Connect($connect);
my $fed = new_Federate('Studies',{DataSources_from_config=>1, AccessControl=>$ac, conf_file=>\%config, Connect => $connect});
$fed->params({'r[]'=>'gwascentral.resultset.HGVRS4,gwascentral.resultset.HGVRS5', 'iDisplaySize'=>50,'iDisplayStart'=>'50'});
$fed->prepare_Filters;
warn Dumper($fed->results);
