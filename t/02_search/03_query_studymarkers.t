#!/usr/bin/perl
use FindBin;
use lib ( "$FindBin::Bin/../../lib", "$FindBin::Bin/lib" );
use Test::More;
use Test::Exception;

#use Test::Base;
use Data::Dumper qw(Dumper);
$Data::Dumper::Maxdepth=2;
use GwasCentral::Search::Util qw(new_Query);
use GwasCentral::DataSource::Util qw( new_AccessControl);
my $ac = new_AccessControl('conf/main.conf');
warn "ac:$ac";

my $query = new_Query( 'StudyMarkers', { DataSources_from_config => 1, conf_file => 'conf/main.conf', AccessControl => $ac } );

$query->params({'sid' => 'HGVST200', 'r[]' => 'HGVRS303'});
$query->prepare_Filters;
$query->results;
print Dumper($query->data);
#foreach my $q(values %{$query->Queries}) {
#	print Dumper($q->data);
#}
#print $query->output;
#print "filename:".$query->Format->filename;

#test_search_recognised_as 'input' => 'expected','studies';
#
#__END__
#
#=== Test 1 (Query string 'cancer' recognised as 'Keyword')
#--- input lines chomp hash
#q
#cancer
#t
#6
#
#--- expected chomp
#Keyword
#
#=== Test 2 (Query string 'fgfr2' recognised as 'GeneRegion')
#--- input lines chomp hash
#q
#chr6
#t
#6
#
#--- expected chomp
#GeneRegion
#
#=== Test 3 (Query string 'rs699' recognised as 'Marker')
#--- input lines chomp hash
#q
#rs549
#t
#3
#
#--- expected chomp
#Marker
#
#=== Test 3 (Empty string recognised as 'None')
#--- ONLY
#--- input lines chomp hash
#q
#
#t
#3
#
#--- expected chomp
#None
#
#=== Test 4 (Undef string recognised as 'None')
#--- ONLY
#--- input lines chomp hash
#q
#undef
#t
#3
#
#--- expected chomp
#None

