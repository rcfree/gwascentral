#!/usr/bin/perl
use FindBin;
use lib ("$FindBin::Bin/../../lib","$FindBin::Bin/lib");
use MockQuery;
use Test::More;
use Test::Exception;
use Data::Dumper qw(Dumper);
use HGVbaseG2P::Database;
use HGVbaseG2P::Search::Query::Markers;
use HGVbaseG2P::DataSource::Study::Dynamic;
use HGVbaseG2P::DataSource::Browser::Dynamic;
use HGVbaseG2P::DataSource::Marker::Dynamic;
use HGVbaseG2P::DataSource::Xapian::Database;
use HGVbaseG2P::Search::Formatter;
use JSON::XS;

#use MockStudyDataSource;
use HGVbaseG2P::AccessControl::None;

my $db = HGVbaseG2P::Database->new({conf_file=>'conf/hgvbase.conf'});

my $ac = HGVbaseG2P::AccessControl::None->new({conf_file=>'conf/hgvbase.conf'});
my $study_source = HGVbaseG2P::DataSource::Study::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
$study_source->AccessControl($ac);
$study_source->local_server("gwascentral.localhost");
my $browser_source = HGVbaseG2P::DataSource::Browser::Dynamic->new({conf_file=>'conf/hgvbase.conf'});
$browser_source->AccessControl($ac);

my $xapian_source = HGVbaseG2P::DataSource::Xapian::Database->new({conf_file=>'conf/hgvbase.conf'});
my $marker_source = HGVbaseG2P::DataSource::Marker::Dynamic->new({conf_file=>'conf/hgvbase.conf'});

my %databases = (
	Ontology => $db->new_instance('Ontology'),
	HPO => $db->new_instance('Hpo'),
	Marker => $marker_source,
	Feature => $db->new_instance('Feature'),
	Study => $study_source,
	Xapian => $xapian_source,
	Browser => $browser_source
);

my $query = HGVbaseG2P::Search::Query::Markers->new({conf_file=>'conf/hgvbase.conf'});
$query->databases(\%databases);
$query->params({'q'=>'cancer', 't'=>'8','m'=>'All', 'o' => 'Identifier (descending)', 'page_size'=>50, 'chr' => 2, 'page' => 2 });
$query->prepare_filters;
#is($query->content_type, 'text/json',"JSON content-type is correct when given 'format'");
my @results = @{$query->results};
my @trimmed = map { $_->{identifier}.",".$_->{accession}.",".$_->{coords} } @results;
foreach my $line(@trimmed) {
	print $line."\n";
}
warn "pager:".Dumper($query->pager);
#$query = HGVbaseG2P::Search::Query::Studies->new({conf_file=>'conf/hgvbase.conf'});
#$query->databases(\%databases);
#$query->params({'q'=>'Pancreatic cancer'});
#$query->prepare_filters;
#is($query->content_type('text/json'), 'text/json',"JSON content-type is correct when given 'content-type'");
#is($query->content_type, 'text/json',"JSON content-type is correct when retrieved again");
#is($query->is_html,undef,"JSON type is NOT HTML");
#$query->Formatter(HGVbaseG2P::Search::Formatter->new({conf_file=>$query->config}));
#
#my $json_coder = JSON::XS->new->ascii->pretty->allow_nonref;
#$json_coder->space_after(1);
#my $output = $query->output;
#lives_ok { $json_coder->decode($output) } "JSON formatted text";
#
#$query = HGVbaseG2P::Search::Query::Studies->new({conf_file=>'conf/hgvbase.conf'});
#$query->databases(\%databases);
#$query->params({'q'=>'DMD', 't'=>'ZERO'});
#$query->prepare_filters;
#$query->results;

