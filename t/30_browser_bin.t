#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - allelematch rule

use strict;
use warnings;

use FindBin;
use lib ( "$FindBin::Bin/../lib" );
my $path = "$FindBin::Bin/../lib/";
use Data::Dumper qw(Dumper);

use HGVbaseG2P::Browser::Analyse::Bin;

use Test::More;
use Array::Compare;
plan tests=>56;

	
	#deals with zero start by switching to 1 and marker at position 0 is ignored
	test_binning({
		text => "deals with zero start by switching to 1 and marker at position 0 is ignored",
		bin_size => 1000,
		bin_start => 0,
		bin_stop =>  100000000,
		length => 100000000,
		expected_markers => [ [2,3,4,5,6,7,8],[9],[],[],[],[],[],[],[],[10,11] ],
		first_start => 1,
		first_stop => 1000,
		last_start => 99999001,
		last_stop => 100000000,
		number_bins => 100000
	});
	
	#deals with simple size same as length and small bins (1Kb)
	test_binning({
		text => "deals with simple size same as length and small bins (1Kb)",
		bin_size => 1000,
		bin_start => 1,
		bin_stop =>  100000000,
		length => 100000000,
		expected_markers => [ [2,3,4,5,6,7,8],[9],[],[],[],[],[],[],[],[10,11] ],
		first_start => 1,
		first_stop => 1000,
		last_start => 99999001,
		last_stop => 100000000,
		number_bins => 100000
	});
	
	#deals with bigger bins (10Kb)
	test_binning({
		text => "deals with bigger bins (10Kb)",
		bin_size => 10000,
		bin_start => 1,
		bin_stop =>  100000000,
		length => 100000000,
		expected_markers => [ [2,3,4,5,6,7,8,9,10,11],[12,13],[14],[15] ],
		first_start => 1,
		first_stop => 10000,
		last_start => 99990001,
		last_stop => 100000000,
		number_bins => 10000
	});

#check only markers within region are binned
	test_binning({
		text => "deal with binning between 10001 and 50Kb region with 10Kb binsize",
		bin_size => 10000,
		bin_start => 10001,
		bin_stop =>  50000,
		length => 100000000,
		expected_markers => [ [12,13], [14], [15],[] ],
		first_start => 10001,
		first_stop => 20000,
		last_start => 40001,
		last_stop => 50000,
		number_bins => 4
	});

	test_binning({
		text => "deal with binning between 10Kb and 50Kb region with 10Kb binsize",
		bin_size => 10000,
		bin_start => 10000,
		bin_stop =>  50000,
		length => 100000000,
		expected_markers => [ [11,12],[13],[14], [15],[] ],
		first_start => 10000,
		first_stop => 19999,
		last_start => 50000,
		last_stop => 59999,
		number_bins => 5
	});
	
	test_binning({
		text => "deal with binning between 50Kb and 150Kb region with 10Kb binsize",
		bin_size => 10000,
		bin_start => 50000,
		bin_stop =>  150000,
		length => 100000000,
		expected_markers => [ [], [], [], [], [16],[17,18],[] ],
		first_start => 50000,
		first_stop => 59999,
		last_start => 150000,
		last_stop => 159999,
		number_bins => 11
	});
	
	#test with large bins (1Mb)
	test_binning({
		text => 'deal with large bins(1Mb)',
		bin_size => 1000000,
		bin_start => 1000001,
		bin_stop =>  10000000,
		length => 100000000,
		expected_markers => [ [21], [], [], [], [],[],[], [], [22,23] ],
		first_start => 1000001,
		first_stop => 2000000,
		last_start => 9000001,
		last_stop => 10000000,
		number_bins => 9
	});
	
sub test_binning {	
	my ($attrs)=shift;
	my $length = $attrs->{'length'};
	my $bin_size = $attrs->{bin_size};
	my $bin_start = $attrs->{bin_start};
	my $bin_stop = $attrs->{bin_stop};
	my $analyse = HGVbaseG2P::Browser::Analyse::Bin->new({ data => generate_test_data() } );
	print $attrs->{text}."\n";
    my $binned = $analyse->bin(
				{
					position_field => 'start',
					bin_size       => $bin_size,
					total_length   => $length,
					value_field    => 'pvalue',
					bin_content    => 'array',
					bin_start      => $bin_start,
					bin_stop       => $bin_stop,
					calc_ranges    => 1,
				}
			);

	is $binned->[0]->{bin_start},$attrs->{first_start}, "Expected first bin_start returned";
 	is $binned->[0]->{bin_stop},$attrs->{first_stop}, "Expected first bin_stop returned";
	
	is $binned->[-1]->{bin_start},$attrs->{last_start}, "Expected last bin_start returned";
	is $binned->[-1]->{bin_stop},$attrs->{last_stop}, "Expected last bin_stop returned";
	
	is scalar(@{$binned}),$attrs->{number_bins}, "Expected number of bins returned";
    
    my $start_failures=0;
    my $stop_failures=0;
    
    foreach my $item(@{$binned}) {
		
		my $bin_start = $item->{bin_start};
		my $bin_stop = $item->{bin_stop};
		my $start_fail=0;
		my $stop_fail=0;
		foreach my $content(@{$item->{bin_content}}) {
			if ($content->{start} < $item->{bin_start}) {
				$start_fail=1;
				print $content->{start}." < ".$item->{bin_start}."\n";
			}
			if ($content->{start} > $item->{bin_stop}) {
				$stop_fail=1;
			}
		}
		$start_failures+=$start_fail;
		$stop_failures+=$stop_fail;
	}
	is $start_failures,0, "No failed binning for bin_start";
	is $stop_failures,0, "No failed binning for bin_stop";
    
	my $expected_markers = $attrs->{expected_markers};
	foreach my $row(@{$expected_markers}) {
		foreach my $col(@{$row}) {
			$col="HGVM$col";
		}
	}
	my $bin_counter=0;
	my $find_failures=0;
	foreach my $item(@{$binned}) {
		
		my @markers =();
		foreach my $content(@{$item->{bin_content}}) {
			push @markers, $content->{markerid};
		}
		
		my $comp = Array::Compare->new();
		last unless $expected_markers->[$bin_counter];
		if (!$comp->perm(\@markers, $expected_markers->[$bin_counter])) {
			$find_failures++;
			print "FAILURE $find_failures - Actual:".join(",",@markers)." vs Expected:".join(",",@{$expected_markers->[$bin_counter]})."\n";
			
		}
		
		
		$bin_counter++;
	}
	
	is $find_failures,0,"Markers found in correct bins";
}	

sub generate_test_data {
    my @positions=('0','1','10','99','100','101','999','1000','1001','9999','10000','10001','20000','30000','40000','99999','100000',
    '100001','999999','1000000','1000001','9999999','10000000','10000001','99999999','100000000','100000001');
    my @data = ();
    my $marker=0;
    my $value = 1;
    foreach(@positions) {
    	$marker++;
    	$value++;
    	push @data, {
    		pvalue=>$value,
            markerid=>"HGVM$marker",
            start=>"$_",
            stop=>"$_"
    	};
    	$value==6 and $value=1;
    }
    return \@data;
}