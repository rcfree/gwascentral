#!perl
use strict;
use warnings;

use FindBin;
use lib ( "$FindBin::Bin/../lib" );
my $path = "$FindBin::Bin/../";

use Data::Dumper qw(Dumper);

use HGVbaseG2P::Browser::Core;
use HGVbaseG2P::Database::Browser;
use Test::More;
use Array::Compare;
plan tests=>1;

#autoconnection works
my $browser = HGVbaseG2P::Browser::Core->new({
				  conf_file =>
					$path."/conf/hgvbase.conf",
	  });
$browser->viewtype('genome');
$browser->chr('1');
#$browser->resultsetid(['3','5','6','8','12','15']);
$browser->resultsetid(['6']);


#simulate marker data retrieved from database
#SELECT Marker_Identifier, Marker_Accession, Chr, Start, Stop, NegLogPValue, Strand, SignificanceID, ResultsetID
#my $markers = [
#	['3','999','1','2600000','5','3'],
#	['6','994','1','2400000','5','5'],
#	['4','92','1','2200000','5','5'],
#	['4','92','1','2200000','6','6'],
#	['1','994','1','2100000','5','6'],
#	['2','991','1','2100000','6','6'],
#	['2','991','1','3500000','6','6'],
#];

my $data = mock_marker_data(
[
	['3','999','1','2600000','5','6'],
	['6','994','1','2400000','5','6'],
	['4','92','1','2200000','6','6'],
 	['2','991','1','2100000','6','6'],
 	['3','991','1','3534000','6','6'],
 	['5','991','1','7043000','6','6'],
 	['7','991','1','9100000','6','6'],
]
);


print Dumper($data);


test_binned_marker_data('3000000', '0','12000000', '3000000',$data, '1','3000000',4);
test_binned_marker_data('2000000','12000000','2000000', $data, '1','2000000',4);

#is scalar(@{$sorted[0]->[1]}),2,"Correct number of markers in marker content";

#my ( $max_and_counts, $sig_markers, $max_pvalue, $max_count ) =
#		  $browser->calculate_max_and_sig_counts( { data => $binned_data } );
		  
			
#print Dumper($max_and_counts);			

#$browser->add_sig_counts_glyph( {height => 80} );
#
#		my $chr_max_count = $browser->add_sig_counts(
#			{
#				data           => $max_and_counts,
#				bin_size       => $browser->major_bin,
#			}
#		);

sub test_binned_marker_data {
	my ($test_binsize, $data, $bin_start, $bin_stop, $number)=@_;
	my $browser = HGVbaseG2P::Browser::Core->new({
				  conf_file =>
					$path."/conf/hgvbase.conf",
	  });
	$browser->viewtype('genome');
	$browser->chr('1');
	$browser->seg_start('2432607');
	$browser->seg_stop('11453534');
	$browser->setting('lowres_binsize',$test_binsize);
	$browser->round_positions;
	ok $browser->round_stop eq '12000000',"Rounded up sequence stop to 12Mb";
	ok $browser->round_start eq '0',"Rounded down sequence stop to 0";
	my ($binned_data) =
		  $browser->calculate_binned_data(
			{ bin_size => $test_binsize, data => $data } );
			
	print Dumper($binned_data);
	
	#test calculated bin data as expected for 15Kb, 30Kb and 45Kb
	##check some values in bin 0
	my $bin = $binned_data->[0];
	is $bin->{'bin_start'},$bin_start,"Bin start correct";
	is $bin->{'bin_stop'},$bin_stop,"Bin stop correct";
	is scalar(@{$bin->{'bin_content'}}),$number,"Correct number (4) of marker types in bin";
}

sub mock_marker_data {
	my ($markers)=@_;
	
	my $data = [];
	#test calculated bin data as expected for 1Mb, 2Mb and 3Mb
	foreach my $marker(@{$markers}) {
		push @{$data},{
			Marker_Identifier => "HGVM".$marker->[0],
			Marker_Accession => "rs".$marker->[1],
			Chr => $marker->[2],
			Start => $marker->[3],
			Stop => $marker->[3],
			NegLogPValue => $marker->[4],
			Strand => '-1',
			ResultsetID => $marker->[5],
		}	
	}
	return $data;
}

sub test_binned_features {
	#tests for genome binned data
my $data = [
	{ bin_start => 1,
	  bin_stop  => 3000000,
	  bin_counts => '1|2|3|-1|-1|-1',
	  bin_content => [],
	},
	{ bin_start => 3000001,
	  bin_stop  => 6000000,
	  bin_counts => '2|-1|2|3|2|2',
	  bin_content => [],
	},
	{ bin_start => 6000001,
	  bin_stop  => 9000000,
	  bin_counts => '3|1|-1|2|-1|3',
	  bin_content => [],
	},
	{ bin_start => 9000001,
	  bin_stop  => 12000000,
	  bin_counts => '3|1|-1|2|3|-1',
	  bin_content => [],
	}
];
$browser->add_sig_counts_glyph;
my $chr_max_count =
			  $browser->add_sig_counts(
				{ data => $data, chr => 1 } );

#check basic features are correct (sort by start to ensure in correct order!)
my @unsorted_features = $browser->features->features('s');
my @features = sort { $a->start <=> $b->start } @unsorted_features; 

is scalar(@features),4,"Correct number of features created";
is $features[0]->score,'1|2|3|0|0|0',"Correct scores for feature 0";
is $features[1]->score,'2|0|2|3|2|2',"Correct scores for feature 1";
is $features[2]->score,'3|1|0|2|0|3',"Correct scores for feature 2";
is $features[3]->score,'3|1|0|2|3|0',"Correct scores for feature 3";
is $features[0]->ref,1,"Correct ref for feature 0";

$browser->add_bins_present_glyph;
$browser->add_bins_present( { data => $data, chr => 1 } );
my @featurel3 = $browser->features->features('l3');
is scalar(@featurel3),1,"Correct number of features created";
is $featurel3[0]->start,'1',"Correct start for feature l3";
is $featurel3[0]->stop,'12000000',"Correct stop for feature l3";

my @featurel5 = $browser->features->features('l5');
is scalar(@featurel5),2,"Correct number of features created";
is $featurel5[0]->start,'1',"Correct start for feature 0 l5";
is $featurel5[0]->stop,'3000000',"Correct stop for feature 0 l5";
is $featurel5[1]->start,'6000001',"Correct start for feature 1 l5";
is $featurel5[1]->stop,'12000000',"Correct stop for feature 1 l5";


my @featurel6 = $browser->features->features('l6');
is scalar(@featurel6),1,"Correct number of features created";
is $featurel6[0]->start,'1',"Correct start for feature 0 l6";
is $featurel6[0]->stop,'6000000',"Correct stop for feature 0 l6";

my @featurel8 = $browser->features->features('l8');
is scalar(@featurel8),1,"Correct number of features created";
is $featurel8[0]->start,'3000001',"Correct start for feature 0 l8";
is $featurel8[0]->stop,'12000000',"Correct stop for feature 0 l8";

my @featurel12 = $browser->features->features('l12');
is scalar(@featurel12),2,"Correct number of features created";
is $featurel12[0]->start,'3000001',"Correct start for feature 0 l12";
is $featurel12[0]->stop,'6000000',"Correct stop for feature 0 l12";
is $featurel12[1]->start,'9000001',"Correct start for feature 1 l12";
is $featurel12[1]->stop,'12000000',"Correct stop for feature 1 l12";

my @featurel15 = $browser->features->features('l15');
is scalar(@featurel15),1,"Correct number of features created";
is $featurel15[0]->start,'3000001',"Correct start for feature 0 l15";
is $featurel15[0]->stop,'9000000',"Correct stop for feature 0 l15";
}

