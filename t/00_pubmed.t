use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper qw(Dumper);
use HGVbaseG2P::DataImport::Tool::PubmedSearch;
my $search = HGVbaseG2P::DataImport::Tool::PubmedSearch->new({conf_file=>'conf/hgvbase.conf'});
my $article = $search->pubmed_search(17554300);
print Dumper($article);