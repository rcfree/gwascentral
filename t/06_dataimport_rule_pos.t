#!perl -w

# $Id: 03_markerimport.t 463 2008-03-27 13:02:59Z mummi $

# Test data import - positions match rule

use strict;
use warnings;

use Test::More  qw/no_plan/; # NB increment this as you add more tests!
use Test::Exception;
use Data::Dumper qw(Dumper);
use List::Compare;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Validation::DataElement;
use HGVbaseG2P::Validation::Rule::PositionsMatch;
use Bio::SeqFeature::Generic;

my $marker = Bio::SeqFeature::Generic->new(
	-start => '3000',
	-stop => '3000',
	-strand => '-1',
	-source => 'dbSNP',
	-score => '123',
	-display_name => 'rs123',
	-tag          => {chr => '3', alleles=>[qw(A T)], upstream30bp =>'', downstream30bp =>'' }
);
my $de = HGVbaseG2P::Validation::DataElement->new;
$de->line({'accession'=>'rs123','chromosome'=>'3','position'=>'3000'});
$de->status({});
$de->marker($marker);

my $r1 = HGVbaseG2P::Validation::Rule::PositionsMatch->new;
$r1->settings->{position}=1;
$r1->run($de);
print $de->dumper;