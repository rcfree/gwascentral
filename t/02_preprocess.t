#!perl -w

#$Id: 02_preprocess.t 1072 2008-11-03 11:32:17Z mummi $

# Test Preprocess liftover

use strict;
use warnings;
use Data::Dumper;
use Test::More 'no_plan';
use Test::Exception;
use FindBin;
use lib "$FindBin::Bin/../lib";
use HGVbaseG2P::Util qw(openInfile);

my $path='/Users/rob/Documents/workspace/hgvbaseG2P/trunk/perl/t/data/PreProcess/';
my $infile = '/Users/rob/Documents/workspace/hgvbaseG2P/trunk/perl/t/data/PreProcess/broad.txt';
my $chainfile = '/Users/rob/Documents/workspace/hgvbaseG2P/trunk/perl/t/data/PreProcess/hg17ToHg18.over.chain';
my $source_class = "Broad";
my $conf = '/Users/rob/Documents/workspace/hgvbaseG2P/trunk/perl/conf/hgvbase.conf';

lives_ok{system("rm ".$path."broad_*");}"old files removed ok";

ok !-f $path."broad_liftover.liftover",$path."broad_liftover.liftover deleted";
ok !-f $path."broad_liftover.mapped",$path."broad_liftover.mapped deleted";
ok !-f $path."broad_liftover.unmapped",$path."broad_liftover.unmapped deleted";
ok !-f $path."broad_liftover.txt",$path."broad_liftover.txt deleted";
ok !-f $path."broad_liftover_unmapped.txt",$path."broad_liftover_unmapped.txt deleted";

# Initialize importer class
my $preprocess;

my $preprocessclass = 'HGVbaseG2P::PreProcess::LiftOver::Broad';
eval "require $preprocessclass";
#print Dumper (\@INC);
lives_ok {
 $preprocess = $preprocessclass->new(conf_file => $conf,
				 source    => $source_class,
				 				 argv      => \%ARGV );
} "Preprocess class created ok";


isa_ok($preprocess,"HGVbaseG2P::PreProcess::LiftOver","Class is LiftOver class");


lives_ok {
	$preprocess->run_liftover($infile,$chainfile);
	
} "Liftover ran ok";


ok -f $path."broad_liftover.liftover","Broad liftover input file created";
ok -f $path."broad_liftover.txt","Broad liftover final output file created";
ok -f $path."broad_liftover.mapped","Broad liftover mapped file created";
ok -f $path."broad_liftover.unmapped","Broad liftover unmapped file created";

my ($unmapped_sourcefile,$mapped_sourcefile,$openmappedfile,$openunmappedfile);
my (@mappedarray_ok,@unmappedarray_ok,@mapped_sourceoutfile_ok,@unmapped_sourceoutfile_ok); 


lives_ok { 
	$mapped_sourcefile = openInfile($path."broad_liftover.txt");
	isa_ok $mapped_sourcefile, 'IO::File',"Filehandle is of the proper class"; 
}"outfile ".$path."broad_liftover.txt ok";;

@mapped_sourceoutfile_ok = <$mapped_sourcefile>;
$mapped_sourcefile->close;

    
lives_ok{
	$openmappedfile = openInfile($path."broad_liftover.mapped");
	isa_ok $openmappedfile, 'IO::File',"Filehandle is of the proper class";

}"outfile ".$path."broad_liftover.mapped ok";
@mappedarray_ok = <$openmappedfile>;
$openmappedfile->close;
#

lives_ok{
	$openunmappedfile = openInfile($path."broad_liftover.unmapped");
	isa_ok $openunmappedfile, 'IO::File',"Filehandle is of the proper class";
}"outfile ".$path."broad_liftover.unmapped ok";
@unmappedarray_ok = <$openunmappedfile>;
$openunmappedfile->close;

lives_ok{
		 $unmapped_sourcefile = openInfile($path."broad_unmapped_liftover.txt");
		 isa_ok $unmapped_sourcefile, 'IO::File',"Filehandle is of the proper class";
		 
}"outfile ".$path."broad_unmapped_liftover.txt ";
@unmapped_sourceoutfile_ok= <$unmapped_sourcefile>;
$unmapped_sourcefile->close;







#check mappped file ok
my @mappedarray = ("chrX	141008946	141008947	rswrong\n",
"chrX	5031491	5031492	rs1037215\n",
"chrX	18670167	18670168	rs5909220\n",
"chr3	9249803	9249804	rs845445\n",
"chrX	38755183	38755184	rs909379\n",
"chrX	144936519	144936520	rs4827683\n",
"chrX	148124049	148124050	SNP_A-1787762\n",
"chrX	148124049	148124050	rs000000\n");




is_deeply (\@mappedarray , \@mappedarray_ok, "mapped file is ok");

#check unmappped file as expected
my @unmappedarray = ("#Deleted in new\n",
"chr22	129504521	129504522	rs5975213\n",
"#Deleted in new\n",
"chr1	-1	0	rs123\n",
"#Deleted in new\n",
"chrY	148021903	148021904	i_r12589\n",
"#Deleted in new\n",
"chrY	-1	0	i_r12589\n");
is_deeply (\@unmappedarray , \@unmappedarray_ok, "unmapped file ok");
#

#
###test to see if source file has been generated corrrectly
my @mapped_sourceoutfile = (
"CHR POS SNP Z_PVAL ZSCORE OR_CMH L95 U95 A1 A2 MAF F_A F_U GENO GENO_A GENO_U P_HWD P_HWD_A P_HWD_U F_MISS F_MISS_A F_MISS_U P_MISS GENE_LIST\n",
"X 141008946 rswrong 0.5611 0.5812 1.045 0.9041 1.207 C G 0.4479 0.445 0.4393 308/707/454 154/341/223 154/366/231 0.2919 0.2914 0.7123 0.00716479 0.006148 0.00818 0.5142 -9\n",
"X 5031491 rs1037215 0.5751 -0.5605 0.9476 0.8074 1.112 C G 0.2523 0.2669 0.2625 97/555/816 53/271/393 44/284/423 0.8369 0.5007 0.7689 0.00921187 0.01025 0.00818 0.5583 -9\n",
"X 18670167 rs5909220 0.02328 2.269 1.2 1.036 1.39 A G 0.4386 0.4336 0.4121 257/698/453 134/340/204 123/358/249 0.7037 0.7564 0.819 0.0467417 0.05464 0.03885 0.04288 ppef1\n",
"3 9249803 rs845445 0.4273 -0.7938 0.9306 0.7984 1.085 G C 0.3169 0.3197 0.3275 158/657/666 72/318/331 86/339/335 0.8598 0.7975 1 0.00102354 0.001366 0.0006817 0.5623 tbl1x\n",
"X 38755183 rs909379 0.4207 0.8052 1.07 0.9122 1.255 T C 0.2851 0.2884 0.2837 144/508/774 79/238/375 65/270/399 1.836e-05 4.053E-05 0.05086 0.0218356 0.02322 0.02045 0.6074 -9\n",
"X 144936519 rs4827683 0.4883 0.6931 1.054 0.9127 1.216 T G 0.4456 0.453 0.4452 279/736/453 131/369/214 148/367/239 0.5257 0.2248 0.7675 0.00955305 0.01161 0.007498 0.2523 -9\n",
"X 148124049 SNP_A-1787762 0.2686 -1.106 0.7975 0.5609 1.134 T C 0.04526 0.03541 0.04643 3/116/1335 0/51/655 3/65/680 0.7382 1 0.228 0.0143296 0.01639 0.01227 0.3476 -9\n",
"X 148124049 rs000000 0.2686 -1.106 0.7975 0.5609 1.134 T C 0.04526 0.03541 0.04643 3/116/1335 0/51/655 3/65/680 0.7382 1 0.228 0.0143296 0.01639 0.01227 0.3476 -9\n"
);

is_deeply (\@mapped_sourceoutfile , \@mapped_sourceoutfile_ok, "new source file ok");


####test to see if source file has been generated corrrectly
my @unmapped_sourceoutfile = (
"CHR POS SNP Z_PVAL ZSCORE OR_CMH L95 U95 A1 A2 MAF F_A F_U GENO GENO_A GENO_U P_HWD P_HWD_A P_HWD_U F_MISS F_MISS_A F_MISS_U P_MISS GENE_LIST\n",
"22 129504521 rs5975213 0.8715 0.1618 1.012 0.8425 1.217 C T 0.1926 0.1873 0.1882 56/431/995 29/204/489 27/227/506 0.2971 0.2091 0.8098 0.00102354 0.001366 0.0006817 0.5623 cova1\n",
"1 -1 rs123 0.2686 -1.106 0.7975 0.5609 1.134 T C 0.04526 0.03541 0.04643 3/116/1335 0/51/655 3/65/680 0.7382 1 0.228 0.0143296 0.01639 0.01227 0.3476 -9\n",
"Y 148021903 i_r12589 0.2686 -1.106 0.7975 0.5609 1.134 T C 0.04526 0.03541 0.04643 3/116/1335 0/51/655 3/65/680 0.7382 1 0.228 0.0143296 0.01639 0.01227 0.3476 -9\n",
"Y -1 i_r12589 0.2686 -1.106 0.7975 0.5609 1.134 T C 0.04526 0.03541 0.04643 3/116/1335 0/51/655 3/65/680 0.7382 1 0.228 0.0143296 0.01639 0.01227 0.3476 -9\n");

is_deeply (\@unmapped_sourceoutfile , \@unmapped_sourceoutfile_ok, "new unmapped source file ok");




exit 0;